<?php
// part of orsee. see orsee.org
ob_start();

$menu__area="my_registrations";
include("header.php");

$checkkey_length=14;

if (isset($_REQUEST['s']) && $_REQUEST['s']) $session_id=url_cr_decode_session($_REQUEST['s']); else $session_id="";
if(empty($initial_cpid) && !empty($_REQUEST['p'])) $initial_cpid=$_REQUEST['p'];
if (!empty($initial_cpid)) $participant_id=url_cr_decode($initial_cpid); else $participant_id="";
$proceed_unregister=false;
if(!empty($_REQUEST['k']) && !empty($_REQUEST['c'])) {
	$id=str_replace(' &#x2F;','/',$_REQUEST['k']);
	$id_nmatches=preg_match_all("/[.\/0-9A-Za-z]/",$id);
	// var_dump($id_nmatches,strlen($id),$id,urldecode($id),strlen(urldecode($id)),str_replace(' &#x2F;','/',$id));
	if($id_nmatches===false || $id_nmatches==0 || $id_nmatches!=strlen($id)) exit(lang('not_authorized'));
	$w=(strtolower(PHP_OS)=="linux")?" WHERE ENCRYPT(log_id,'un')='$id' ":" WHERE log_id='$id'";
	$q="SELECT * FROM ".table("participants_log").$w;
	$line=orsee_query($q);
	if($line === false) exit(lang("incorrect_or_expired_link"));
	// var_dump($line);
	$atarget=preg_split("/\n|\r\n/",$line['target']); $targets=array();
	foreach($atarget as $stinfo) {
		// echo "\nstinfo:$stinfo\n";
		$atinfo=explode(":",$stinfo);
		if(count($atinfo)>1) {
			$targets[trim($atinfo[0])]=trim($atinfo[1]);
		}
	}
	$session_id=$targets["session_id"];
	$experiment_id=$targets["experiment_id"];
	$participant_id=$line["id"];
	$keybase=$participant_id."-".$experiment_id."-".$session_id."-".$line["timestamp"];
	$proceed_unregister=hash_equals(tinymd5($keybase,$checkkey_length),$_REQUEST['c']);
	if(!$proceed_unregister) exit(lang("incorrect_or_expired_link").".");
	// var_dump($_REQUEST['k'],$_REQUEST['c'],$line); exit;
}
if(!empty($session_id))
{
	$session=orsee_db_load_array("sessions",$session_id,"session_id");
	$session_name=session__build_name($session).' ('.lang('experiment',false).' '.experiment__get_public_name($session['experiment_id']).')';
	$session_start_minute=$session['session_start_minute']; if(strlen($session_start_minute)==1) $session_start_minute="0".$session_start_minute;
	$session_start_month=$session['session_start_month']; if(strlen($session_start_month)==1) $session_start_month="0".$session_start_month;
	$session_date_time=$session['session_start_year']."-".$session_start_month."-".$session['session_start_day']." ".$session['session_start_hour'].":".$session_start_minute;
	$unregister_end_time=sessions__get_unregister_end($session);
	$unregister_end=time__format($lang['lang'],'',false,false,true,false,$unregister_end_time);
}
$now=time();
$rdl=$GLOBALS['settings__public_folder']."/participant_show.php";
if(!empty($participant_id)) $rdl.="?p=".url_cr_encode($participant_id);
if ( empty($session_id) || empty($participant_id) ) {
	redirect($rdl);
}
if(!empty($_REQUEST['no_sorry'])) redirect($rdl);

$return_button='
	<br/>
	<FORM action="participant_show.php">
	
	<center>
	<INPUT type=hidden name="p" value="'.url_cr_encode($participant_id).'">
	<INPUT type=hidden name="s" value="'.unix_crypt($session_id).'">
	<INPUT type=submit name="my_registrations" value="'.lang('my_registrations').'">
	</center>
	</FORM>';
	
$registered=expregister__check_registered_session($participant_id,$session_id);

if($registered && $now>$unregister_end_time) {
	echo '<center><br/>';
	echo lang('too_late_to_unsubscribe_from_this_session').'.';
	echo '</center>';
	if(!isset($_REQUEST['p'])) echo $return_button;
	// var_dump($now,$unregister_end,$unregister_end_time);
	exit;
}
if(!$registered) {
	echo '<center><br/>';
	echo lang("you_are_not_registered_for_this_session").'.';
	echo '</center>';
	if(!isset($_REQUEST['p'])) echo $return_button;
	exit;
}

if($proceed_unregister) {
	expregister__unregister($participant_id,$session_id);
	$registered=expregister__check_registered_session($participant_id,$session_id);
	if($registered) exit(lang("an_error_has_occured please_contact_the_experimentator"));
	echo '<center><br/>';
	echo lang('you_are_unregistered_from_the_session').' <strong>'.$session_name.'</strong>.';
	echo '</center>';
	log__participant("unregister",$participant_id,"experiment_id:".$session['experiment_id']."\nsession_id:".$session_id,true);
	if(!isset($_REQUEST['p'])) echo $return_button;
	exit;
}

$unregister_index=log__get_participant_index($participant_id,'unregister',$session['experiment_id'],$session_id);
$unreg_ask_index=log__get_participant_index($participant_id,'unreg_ask',$session['experiment_id'],$session_id);
if($unregister_index < 0 && $unreg_ask_index < 0 && !empty($_REQUEST['yes_unregister'])) {
	
	
	$log_arr=log__participant("unreg_ask",$participant_id,"experiment_id:".$session['experiment_id']."\nsession_id:".$session_id,true);
	$cid=(strtolower(PHP_OS)=="linux")?crypt($log_arr["log_id"],"un"):$log_arr["log_id"];
	$keybase=$participant_id."-".$session['experiment_id']."-".$session_id."-".$log_arr["timestamp"];
	$key=tinymd5($keybase,$checkkey_length);
	$unreg_link=$settings__root_url."/".$GLOBALS['settings__public_folder']."/participant_desinscription.php?k=$cid&c=$key";
	$unreg_ask_index = 0;
	experimentmail__confirmation_mail_session($participant_id,$session_id,$unreg_link,lang('unregistering_from_an_experimental_session_need_confirmation'),"session_unregister_confirmation_mail");
	// var_dump($log_arr,$keybase,$unreg_link);
}
if($unreg_ask_index>=0) {
	echo '<center><br/>';
	echo lang('the_confirmaiton_link has_been_sent_to_you').'. <br/>'.lang('in_order_to_unregister_connect_to_your_mailbox_and_click_on_the_link_before').' <strong>'.$unregister_end.'</strong>.';
	echo '</center>';
	exit;
}

$yes_text=lang('yes_send_me_the_link'); $no_text=$lang['no_sorry'];
			echo '
			<br/>
			<FORM action="participant_desinscription.php">
			
			<center>
			'.lang('do_you_really_want_to_unregister_from_session').' <strong>'.$session_name.'</strong>?<br/><br/>
			'.lang('if_you_click_yes you_will_receive_a_confirmation_link_by_mail').'. <em>'.lang('you_will_be_unregistered_only_if_you_click_on_it').'</em>.<br/><br/>
			<INPUT type=hidden name="p" value="'.url_cr_encode($participant_id).'">
			<INPUT type=hidden name="s" value="'.unix_crypt($session_id).'">
			<INPUT type=submit name="yes_unregister" value="'.$yes_text.'">
			<INPUT type=submit name="no_sorry" value="'.$no_text.'">
			</center>
			</FORM>';
?>
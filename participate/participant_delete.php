<?php
// part of orsee. see orsee.org
ob_start();

$menu__area="my_data";
include("header.php");

	if (isset($_REQUEST['betternot'])) {redirect($GLOBALS['settings__public_folder']."/participant_edit.php?p=".url_cr_encode($participant['participant_id'])); exit;};

	$form=true;

	if (isset($_REQUEST['reallydelete']) && $_REQUEST['reallydelete']=="12345" && isset($_REQUEST['doit'])) {
		
		query_makecolumns(table('participants'),array("last_delete_time","last_delete_admin","can_undelete"),array("INT(20)","VARCHAR(20)","tinyint(1)"));
		$canund="0";
		if(!empty($_REQUEST['can_undelete'])) $canund="1";
 

                $query="UPDATE ".table('participants')." 
		 	SET deleted='y', last_delete_time=UNIX_TIMESTAMP(), last_delete_admin='-', can_undelete=$canund
                 	WHERE participant_id='".mysqli_real_escape_string($GLOBALS['mysqli'],$participant_id)."'";
		$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
		log__participant("delete",$participant_id,$canund?'resubscribe_possible':'definetely');
		$form=false;
		message ($lang['removed_from_invitation_list']);
		redirect($GLOBALS['settings__public_folder']."/");
		}	

	if ($form) {

		echo '<BR><BR>
			<center>
			<h4>'.$lang['delete_participant'].'</h4>

			<FORM action="participant_delete.php" name="unsubscribe">
			<INPUT type=hidden name="p" value="';
		echo url_cr_encode($participant_id);
		echo '">
			<TABLE cellpadding=10>
			<TR>
			<TD colspan=3 style="text-align:center"><INPUT name=reallydelete type=hidden value="12345">
			'.lang('do_you_really_want_to_unsubscribe').'<BR></TD>
			</TR>
			<TR><TD>
			<INPUT type=submit name=doit style="width:300px" value="'.lang('desactivate_only ( resubscribe_possible )').'">
			</TD><TD>
			<INPUT type=hidden name=doit id=doit_h value=0">
			<INPUT type=hidden name=can_undelete id=can_undelete value=0">
			<INPUT type=button name=doit style="width:300px" onclick="definetely_unsubscribe()" value="'.lang('definetely_unsubscribe').'">
			</TD>
			<TD><INPUT type=submit name=betternot value="'.$lang['no_sorry'].'">
			</TD>
			</TR>
			</TABLE>
			</FORM>
			</center>
			<script>
			function definetely_unsubscribe() {
				if(confirm("'.lang('you_will_not_be_able_to_resubscribe_after_this_action ; do_you_want_to_continue ?').'")) {document.getElementById("can_undelete").value=0; document.getElementById("doit_h").value=1; document.forms[0].submit();}
			}
			</script>';
		}

include("footer.php");

?>




<?php
// part of orsee. see orsee.org
ob_start();

$menu__area="my_registrations";
include("header.php");
if(!empty($settings__stop_subscriptions) && $settings__stop_subscriptions != "n") {
	echo content__get_content("error_temporary_disabled");
	exit;
}

	if (isset($_REQUEST['s']) && $_REQUEST['s']) $session_id=url_cr_decode_session($_REQUEST['s']); else $session_id="";
	if(!empty($session_id))
	{
		$session=orsee_db_load_array("sessions",$session_id,"session_id");
		$session_start_minute=$session['session_start_minute']; if(strlen($session_start_minute)==1) $session_start_minute="0".$session_start_minute;
		$session_start_month=$session['session_start_month']; if(strlen($session_start_month)==1) $session_start_month="0".$session_start_month;
		$session_date_time=$session['session_start_year']."-".$session_start_month."-".$session['session_start_day']." ".$session['session_start_hour'].":".$session_start_minute;
	}

	if (isset($_REQUEST['register']) && $_REQUEST['register']) {

		$continue=true;

		$suppage=false; $suppageoption=""; $suppageoption2="";

		if (isset($_REQUEST['betternot']) && $_REQUEST['betternot']) {
			redirect($GLOBALS['settings__public_folder']."/participant_show.php?p=".url_cr_encode($participant_id));
			}
			
		 
		if (!empty($_REQUEST['notpaypal'])) {
			//print_r($_REQUEST);
			$paypalinfotable=$_REQUEST['paypalinfotable'];
			$suppageoption=$_REQUEST['suppageoption'];
			$query="UPDATE ".$paypalinfotable." SET explicit_answer_paypal=-1 WHERE participant_id='$participant_id' AND session_id='$session_id'";
			if(!mysqli_query($GLOBALS['mysqli'],$query)) {echo "$query<br>Error n°".mysqli_errno($GLOBALS['mysqli']).":".mysqli_error($GLOBALS['mysqli']);}
			$supinputs='
				<INPUT type=hidden name="s" value="'.$_REQUEST['s'].'">
				<INPUT type=hidden name="p" value="'.url_cr_encode($participant_id).'">
				<INPUT type=hidden name="register" value="true">
				<INPUT type=hidden name="paypalinfotable" value="'.$paypalinfotable.'">
				<INPUT type=hidden name="suppageoption" value="'.$suppageoption.'">';
			$qu='Vous avez choisi de ne pas vous inscrire à cette expérience. Afin  de pouvoir analyser correctement les résultats de notre etude, pourriez vous s’il vous plait nous en expliquer les raisons?';
			$query_string = "sendvalue=".urlencode('Envoyez et/ou revenir sur la page d’inscription')."&sendname=notpaypal_answer&sendaction=".$_SERVER['PHP_SELF']."&qu=".urlencode($qu)."&supinputs=".urlencode($supinputs);
			echo "<iframe frameBorder='0' style='border:none;width:100%;height:1024px' src='../../scripts/simplequestion.php?".htmlentities($query_string)."' />";
			if(!in_array($suppageoption,array("paypal","lydia","FNAC","rib"))) {redirect($GLOBALS['settings__public_folder']."/participant_show.php?p=".url_cr_encode($participant_id));} //if($suppageoption=="documentaire")
			exit;
			}
			
		if (!empty($_REQUEST['notpaypal_answer'])) {
			//print_r($_REQUEST);
			function supertrim($tempword) {
				$tempword=preg_replace('/'.PHP_EOL.'/'," ",$tempword);
				$tempword=trim($tempword);
				$tempword=preg_replace('/\'/',"’",$tempword);
				//$tempword=preg_replace('/"/',"”",$tempword);
				return $tempword;
			}
			$openanswer=""; if(isset($_POST["openanswer"])) $openanswer=$_POST["openanswer"]; $openanswer=supertrim($openanswer);
			$paypalinfotable=$_REQUEST['paypalinfotable'];
			$suppageoption=$_REQUEST['suppageoption'];
			if($openanswer!="") {
				$openanswer=strip_tags($openanswer,"<span><strong><em><img>"); //<ul><ol><li>
				$openanswer=stripslashes($openanswer);
				$openanswer = preg_replace('/<img[^<]+alt="([^"]+?)"[^<]*>/is', '|Image:$1|', $openanswer, -1, $pregcount); //$openanswer = preg_replace('/<img[^>]+alt="([^"]+?)"[^>]*>/is', '|Image:$1|', $openanswer, -1, $pregcount); ??
				//echo $openanswer."<br>pregcount=$pregcount<br>";
				$query="SELECT * FROM $paypalinfotable WHERE participant_id='$participant_id' AND session_id='$session_id'";
				if(!mysqli_query($GLOBALS['mysqli'],$query)) {echo "$query<br>Error n°".mysqli_errno($GLOBALS['mysqli']).":".mysqli_error($GLOBALS['mysqli']); exit;}
				$cresult=mysqli_query($GLOBALS['mysqli'],$query); $openanswer_prefix=""; $already_tried=mysqli_num_rows($cresult); while ($already_tried && $crow = mysqli_fetch_array($cresult, MYSQLI_ASSOC)) {$prevanswer=$crow['openanswer']; echo"prevanswer=$prevanswer;<br>"; if(trim($prevanswer)!="") $openanswer_prefix=$prevanswer." <strong>Another message:</strong> ";}
				//echo "cresul_query=$query;<br>openanswer_prefix=$openanswer_prefix;<br>already_tried=$already_tried<br>;"; 
				$openanswer=$openanswer_prefix.$openanswer;
				$query="UPDATE $paypalinfotable SET openanswer='$openanswer' WHERE participant_id='$participant_id' AND session_id='$session_id'";
				if(!mysqli_query($GLOBALS['mysqli'],$query)) {echo "$query<br>Error n°".mysqli_errno($GLOBALS['mysqli']).":".mysqli_error($GLOBALS['mysqli']); }; 
			}
			redirect($GLOBALS['settings__public_folder']."/participant_show.php?p=".url_cr_encode($participant_id)); 
			}
			

		if (empty($session_id)) {
			$continue=false;
			log__participant("interfere",$participant_id);
			message($lang['error_session_id_register']);
			redirect($GLOBALS['settings__public_folder']."/participant_show.php?p=".url_cr_encode($participant_id));
			}


		$experiment_id=sessions__get_experiment_id($session_id);
		$registered=expregister__check_registered($participant_id,$experiment_id);
		if ($registered) {
			$continue=false;
			message($lang['error_already_registered']);
			redirect($GLOBALS['settings__public_folder']."/participant_show.php?p=".url_cr_encode($participant_id));
			}	

		$registration_end=sessions__get_registration_end("",$session_id);
		// $full=sessions__session_full($session_id);
		$thissession=orsee_db_load_array("sessions",$session_id,"session_id"); $thissession["participant_id"]=$participant_id;
		// $full=sessions__session_full($session_id);
		$full=sessions__session_full("",$thissession);
		// var_dump($thissession); var_dump($full); exit;

		$now=time();
		// $register_after_start_val=sessions__get_remark_list_elem_by_name($session_id,"register_after_start");
		// $register_after_start=!empty($register_after_start_val) && $register_after_start_val!='n';
		// if($register_after_start) { $sessions_endtime=sessions__get_session_end_time(array(),$session_id); if(time()>=$sessions_endtime) $register_after_start=false; }
		if ($registration_end < $now) { // && !$register_after_start - already in sessions__get_registration_end()
			$continue=false;
                	message($lang['error_registration_expired']);
                        redirect($GLOBALS['settings__public_folder']."/participant_show.php?p=".url_cr_encode($participant_id));
			}
		$list_sessions=expregister__list_registered_for($participant_id,"","exp_sess_only");
		$thissess_starttime=sessions__get_session_time(array(),$session_id);
		$thissess_endttime=sessions__get_session_end_time(array(),$session_id);
		$n_sametime_sess=0; $sametime_sess=array();
		$start_at_any_time_thissession_val=sessions__get_remark_list_elem_by_name($session_id,"start_at_any_time");
		$start_at_any_time_thissession=!empty($start_at_any_time_thissession_val) && $start_at_any_time_thissession_val!='n';
		if(!$start_at_any_time_thissession) foreach($list_sessions as $csl) {
			$csess_id=$csl[1]; $csess_starttime=sessions__get_session_time(array(),$csess_id); $csess_endtime=sessions__get_session_end_time(array(),$csess_id);
			if($thissess_starttime<$csess_endtime && $csess_starttime<$thissess_endttime) {
				$n_sametime_sess++; $sametime_sess[]=$csl;
			}
		}
		if($n_sametime_sess>0) {
			$mess=lang('you_are_already_registered_for_the_following_session').': ';
			$sessnames=array();
			$start_at_any_time_sessions=array();
			foreach($sametime_sess as $ss) {
				$csessiondata=orsee_db_load_array("sessions",$ss[1],"session_id");
				$start_at_any_time_val=sessions__get_remark_list_elem_by_name($ss[1],"start_at_any_time");
				$start_at_any_time=!empty($start_at_any_time_val) && $start_at_any_time_val!='n';
				if($start_at_any_time) $start_at_any_time_sessions[]=$ss[1];
				else $sessnames[]=session__build_name($csessiondata)." (".experiment__get_public_name($ss[0]).")";
			}
			// var_dump($sessnames); exit;
			if(count($sessnames)>0) {
				$continue=false;
				$mess.=implode(", ",$sessnames);
				$mess.=". ";
				$mess.=lang('you_cannot_participate_in_these_sessions_simultaneously')."!";
				message($mess);
				redirect($GLOBALS['settings__public_folder']."/participant_show.php?p=".url_cr_encode($participant_id));
			}
		}

		if ($full) {
			 $continue=false;
                         message($lang['error_session_complete']);
                         redirect($GLOBALS['settings__public_folder']."/participant_show.php?p=".url_cr_encode($participant_id)); exit($lang['error_session_complete']);
                         }

		$experimentpublicname=experiment__get_public_name($experiment_id);
		$expepublicarr=explode("(",$experimentpublicname);
		$expepublicarr2=explode("_",trim($expepublicarr[0]));
		$reservation_minutes=sessions__get_remark_list_elem_by_name($session_id,"reservation_minutes");
		$pass_reservation=false;
		$sess_remark_list=sessions__get_remark_list($session_id);
		$sess_sup_subscr_message=""; $sess_sup_confirmpage_message="";
		foreach($sess_remark_list as $srem) {
			$asrem=explode(":",$srem);
			if($asrem[0]=="restrictions" && count($asrem)>1) $expepublicarr2=array_merge($expepublicarr2,explode("_",trim($asrem[1])));
			if($asrem[0]=="confirmpage_message" && count($asrem)>1) $sess_sup_confirmpage_message=str_replace("<comma>",",",implode(":",array_slice($asrem, 1)));
			if($asrem[0]=="subscription_message" && count($asrem)>1) $sess_sup_subscr_message=str_replace("<comma>",",",implode(":",array_slice($asrem, 1)));
		}
		// if($experimentpublicname=="expe39") {$suppage=true; $suppageoption="paypal";}
		// if($experimentpublicname=="expe78 (sur 2 jours)") {$suppage=true; $suppageoption="paypal";}
		if($experimentpublicname=="expe59d") {$suppage=true; $suppageoption="documentaire";}
		if($experimentpublicname=="expe97") {$suppage=true; $suppageoption="FNAC";}
		// if($experimentpublicname=="expe124_eeg") {$suppage=true; $suppageoption="supquest";}
		// if($experimentpublicname=="expe144") {$suppage=true; $suppageoption="supquest";}
		// if($experimentpublicname=="expe110 (sur 2 jours)") {$suppage=true; $suppageoption="supquest";}
		if($experimentpublicname=="expe129") {$suppage=true; $suppageoption="partanglais";}
		$a_expepublicarr1=(count($expepublicarr)>1)?preg_split("/[\s,]+/",trim($expepublicarr[1])):array();
		if(!$suppage && count($expepublicarr)>1 && count($a_expepublicarr1)==3 && $a_expepublicarr1[0]=="sur" && $a_expepublicarr1[2]=="jours)" && is_numeric($a_expepublicarr1[1])) {$suppage=true; $suppageoption="2days"; $suppageoption2.="_".$a_expepublicarr1[1]."days";}
		if(!$suppage && count($expepublicarr2)>1 && trim($expepublicarr2[1])=="paypal") {$suppage=true; $suppageoption="paypal";}
		if(!$suppage && count($expepublicarr2)>1 && (trim($expepublicarr2[1])=="jourdan" || trim($expepublicarr2[1])=="assas" || trim($expepublicarr2[1])=="pulv")) {$suppage=true; $suppageoption=$expepublicarr2[1];}
		if(!$suppage && count($expepublicarr2)>1 && (trim($expepublicarr2[1])=="anglais" || trim($expepublicarr2[1])=="english")) {$suppage=true; $suppageoption=$expepublicarr2[1];}
		if(!empty($reservation_minutes) && is_numeric($reservation_minutes) && $reservation_minutes>0) {
			$reservation_link=sessions__get_remark_list_elem_by_name($session_id,"reservation_link");
			$reservation_autoload=sessions__get_remark_list_elem_by_name($session_id,"reservation_autoload");
			$crf="SELECT id FROM ".table('participants_log')." WHERE action='book_end' AND target LIKE '".mysqli_real_escape_string($GLOBALS['mysqli'],"experiment_id:".$experiment_id."\n%")."' ORDER BY log_id";
			$pidscrf=orsee_query($crf,"return_first_elem");
			$mycrfindex=-1;
			if($pidscrf!==false) foreach(array_values(array_unique($pidscrf)) as $pk=>$cpid) if($cpid==$participant['participant_id']) $mycrfindex=$pk;
			if($mycrfindex>=0) $pass_reservation=true;
			if(!$suppage && !$pass_reservation) {$suppage=true; $suppageoption="reservation";}
		}
		if(!$suppage && count($expepublicarr2)>1 && trim($expepublicarr2[1])=="lydia") {$suppage=true; $suppageoption="lydia";}
		if(!$suppage && count($expepublicarr2)>1 && trim($expepublicarr2[1])=="rib") {$suppage=true; $suppageoption="rib";}
		if(!$suppage && count($expepublicarr2)>1 && (trim(strtolower($expepublicarr2[1]))=="win" || trim(strtolower($expepublicarr2[1]))=="windows")) {$suppage=true; $suppageoption="forcewin"; if(count($expepublicarr2)>2 && trim(strtolower($expepublicarr2[2])=="lydia")) $suppageoption="forcewinlydia"; if(count($expepublicarr2)>2 && trim(strtolower($expepublicarr2[2])=="paypal")) $suppageoption="forcewinpaypal"; }
		$ndays_arr=preg_grep("/^\d{1,2}days$/",$sess_remark_list);
		if($ndays_arr!==false && count($ndays_arr)>0) {if(!$suppage) {$suppage=true; $suppageoption="2days";}; foreach($ndays_arr as $ndelem) $suppageoption2.="_".$ndelem;}
		if(!empty(experiment__get_value($experiment_id,'add_step_subscr_info')) || !empty($sess_sup_subscr_message)) {if(!$suppage) {$suppage=true; $suppageoption="info";}; $suppageoption2.="_info";}
		if($suppage && in_array("visio",$expepublicarr2) && !in_array("zoom",$expepublicarr2) && !in_array("zoomreg",$expepublicarr2)) $suppageoption2.="_visioconference";
		if($suppage && in_array("zoom",$expepublicarr2) && !in_array("zoomreg",$expepublicarr2)) $suppageoption2.="_zoom";
		if($suppage && in_array("zoomreg",$expepublicarr2)) $suppageoption2.="_zoomreg";
		if(experiment__check_option($experiment_id,'by_couples')) {$suppage=true; $suppageoption="by_couples";}
		if(experiment__check_option($experiment_id,'supquest')) {$suppage=true; $suppageoption="supquest";}
		// if($suppage && in_array("lydia",$expepublicarr2)) $suppageoption2.="_lydia";
		if (isset($_REQUEST['reallyregister']) && $_REQUEST['reallyregister']) {


			// if all checks are done, register ...
			if ($continue) {
				query_makecolumns(table('participate_at'),"external_code","varchar(255)","");
				$externalcodes=sessions__get_remark_list_elem_by_name($session_id,"external_codes");
				if(!empty($externalcodes)) {
					$unique=false;
					$aecodes=preg_split("/\s+/",$externalcodes);
					if(count($aecodes)==count(array_unique($aecodes))) $unique=true;
					log__participant("take_code",$participant['participant_id'],"experiment_id:".$experiment_id."\nsession_id:".$session_id);
					$plq="SELECT id FROM ".table('participants_log')." WHERE action='take_code' AND target='".mysqli_real_escape_string($GLOBALS['mysqli'],"experiment_id:".$experiment_id."\nsession_id:".$session_id)."' ORDER BY log_id";
					$pids=orsee_query($plq,"return_first_elem");
					$myindex=-1;
					foreach(array_values(array_unique($pids)) as $pk=>$cpid) if($cpid==$participant['participant_id']) $myindex=$pk;
					$external_code='';
					if(!empty($pass_reservation)) {						
						$other_session_id=log__get_participant_targets($participant['participant_id'],"book_end","session_id","experiment_id:".$experiment_id);
						if(!empty($other_session_id)) {
							$my_other_index=log__get_participant_index($participant['participant_id'],'take_code',$exmeriment_id,$other_session_id);
							if($my_other_index>=0) {
								$other_externalcodes=sessions__get_remark_list_elem_by_name($other_session_id,"external_codes");
								if(!empty($other_externalcodes)) {
									$other_aecodes=preg_split("/\s+/",$other_externalcodes);
									if($my_other_index<count($other_aecodes)) {
										$external_code=$other_aecodes[$my_other_index]; $myindex=0;
									}
								}
								else { $pass_reservation=false; /*$suppage=true; $suppageoption="reservation"; - have no effect*/ }
							}
						}						
						// var_dump($external_code,$other_session_id,$myindex,$experiment_id,"pass_reservation:",$pass_reservation,$reservation_minutes); exit;
					}
					if($myindex>=0 && $myindex<count($aecodes)) {
						$mycode=empty($external_code)?$aecodes[$myindex]:$external_code;
						$cw=" WHERE payghost=0 AND experiment_id=$experiment_id";
						if($unique && empty($external_code)) {
							$makeuniquecheck=true;
							while($makeuniquecheck && $myindex<count($aecodes)) {
								$utq="SELECT external_code FROM ".table('participate_at').$cw. " AND `external_code`='".$mycode."' AND participant_id<>".$participant['participant_id']."";
								$fec=orsee_query($utq);
								if(!empty($fec)) {
									$myindex++; $mycode=$aecodes[$myindex];
								}
								else $makeuniquecheck=false;
							}
							if($makeuniquecheck) $continue=false;
						}
						$q="UPDATE ".table('participate_at')." ";
						$q.="SET `external_code`='".$mycode."'";
						$q.=$cw." AND participant_id=".$participant['participant_id']."";
						if($continue) orsee_query($q);
					}
					else $continue=false;
					
				}
				// var_dump($externalcodes,$continue,$mycode,$myindex,$pids,array_unique($pids),array_values(array_unique($pids)),count($aecodes)); exit;
				if(!$continue) {
					message(lang("an_error_occured_the_session_is_probably_full"));
					redirect($GLOBALS['settings__public_folder']."/participant_show.php?p=".url_cr_encode($participant_id)."&s=".url_cr_encode($session_id)); exit(lang("an_error_occured_the_session_is_probably_full"));
				}
				
				if(!empty($reservation_minutes) && is_numeric($reservation_minutes) && $reservation_minutes>0 && !$pass_reservation) {
					if(!empty($reservation_link)) {
						$participate_at_array=rasee_db_load_array("participate_at",array("participant_id"=>$participant['participant_id'],"experiment_id"=>$experiment_id));
						$participant_combined=$participant+$participate_at_array;
						foreach($participant_combined as $pck=>$pcv) $reservation_link=str_ireplace("#".$pck."#",$pcv,$reservation_link);
						$suppresmess="";
						if(!empty($reservation_autoload)) $suppresmess=js_redirect_code($reservation_link);
						message(lang('please_visit_the_following_link_and_finish_the_questionnaire in_order_to_confirm_your_registration').":<br><p style='background-color:#ffcfa0'><a href='$reservation_link'>$reservation_link</a></p>".lang('your_have')." ".$reservation_minutes." ".lang('minutes',false).". ".lang('after_that_time_your_registration_is_not_guaranteed').". ".$suppresmess);
					}
					
					log__participant("booking",$participant['participant_id'],"experiment_id:".$experiment_id."\nsession_id:".$session_id);
					
					// var_dump($reservation_minutes,$reservation_link,$pass_reservation); exit;
					
				}
				else {
					expregister__register($participant_id,$session_id);
					log__participant("register",$participant['participant_id'],"experiment_id:".$experiment_id."\nsession_id:".$session_id);
					$supmess="";
					query_makecolumns(table('experiments'),"confirm_message_supinfo","TEXT NULL");
					if(!empty(experiment__get_value($experiment_id,'confirm_message_supinfo')) || !empty($sess_sup_confirmpage_message)) $supmess="<hr>".experiment__get_value($experiment_id,'confirm_message_supinfo').$sess_sup_confirmpage_message;
					message($lang['successfully_registered_to_experiment_xxx']." ".
						experiment__get_public_name($experiment_id).", ".
						time__format_session_time($session_id).". ".
						$lang['this_will_be_confirmed_by_an_email'].$supmess);
				}
				if(!empty($_REQUEST['paypalinfotable'])) {
					$paypallogtable=$_REQUEST['paypalinfotable'];
					$query="UPDATE $paypallogtable SET explicit_answer_paypal=1 WHERE participant_id='$participant_id' AND session_id='$session_id'";
					if(!mysqli_query($GLOBALS['mysqli'],$query)) {echo"$query<br>Error n°".mysqli_errno($GLOBALS['mysqli']).":".mysqli_error($GLOBALS['mysqli']); exit;}
					if($suppageoption=="supquest") {
						$supquesttable_arr=explode(" ",experiment__get_public_name($experiment_id));
						$query="UPDATE "."supquest_".$supquesttable_arr[0]." SET registered=1, session_id='$session_id', session_date_time='$session_date_time' WHERE participant_id='$participant_id'";
						if(!mysqli_query($GLOBALS['mysqli'],$query)) {echo"$query<br>Error n°".mysqli_errno($GLOBALS['mysqli']).":".mysqli_error($GLOBALS['mysqli']); exit;}							
					}
				}
				redirect($GLOBALS['settings__public_folder']."/participant_show.php?p=".url_cr_encode($participant_id)."&s=".url_cr_encode($session_id));
			}
		}
		else {
			
			// $session=orsee_db_load_array("sessions",$session_id,"session_id"); //déplacé en haut
			
			$textpaypal=""; $normalsubmit="reallyregister"; $normaldecline="betternot";
			$show_yes_button=true; $show_no_button=true;
			$yes_text=$lang['yes_i_want']; $no_text=$lang['no_sorry']; $do_you_really_want_text=$lang['do_you_really_want_to_register_for experiment'];
			$already_tried_tosubstruct=0;
			if($suppage) {
				if (empty($_REQUEST["reallyregisterbeforepaypal"])) $normalsubmit="reallyregisterbeforepaypal";
				if (!empty($_REQUEST["reallyregisterbeforepaypal"])) {
					$supzoomreg=""; $text2days="";
					$asuppageoption2=explode("_",$suppageoption2);
					if(in_array("visioconference",$asuppageoption2)) { //preg_match("/_visioconference/i",$suppageoption2)
						$supzoomreg="<p><hr></p>Par ailleurs, il sera nécessaire de participer à une <strong>visoconférence en ligne</strong> au début de la session expérimentale.";
					}
					if(in_array("zoom",$asuppageoption2)) { //preg_match("/_zoom/i",$suppageoption2
						$supzoomreg="<p><hr></p>Par ailleurs, il sera nécessaire de participer à une <strong>visoconférence zoom</strong> au début de la session expérimentale.";
					}
					if(in_array("zoomreg",$asuppageoption2)) { //preg_match("/_zoomreg/i",$suppageoption2)
						$supzoomreg="<p><hr></p>Par ailleurs, il sera nécessaire de participer à une <strong>visoconférence zoom</strong> au début de la session expérimentale. Cette visioconférence sera <b>enregistrée</b> et conservée par le chercheur jusqu'à la publication des résultats de l'étude (ou pendant 3 ans au maximum) afin de garantir dans le processus de publication scientifique que les instructions ont bien été lues aux participants. Cet enregistrement ne sera en aucune façon appariée avec les résultats de l'expérience.";
					}
					// var_dump($expepublicarr2,$suppageoption2,$supzoomreg);
					// var_dump($suppageoption2,$asuppageoption2,in_array("info",$asuppageoption2));
					if($suppageoption!="info" && in_array("info",$asuppageoption2)) $supzoomreg.="<p><hr></p><p>".experiment__get_value($experiment_id,'add_step_subscr_info').$sess_sup_subscr_message."</p>";
					$normaldecline="notpaypal";
					$paypallogtable="expe_paypal_log";
					$textpaypal="";
					if($suppageoption=="paypal") $textpaypal="<table ><tr><td style='font-weight:normal;font-size: 14px;'><p align=center>INFORMATION IMPORTANTE :</p>
					<p>Pour des raisons scientifiques, cette exp&eacute;rience se d&eacute;roule selon des modalit&eacute;s particuli&egrave;res. La somme d'argent en euros que vous&nbsp; gagnerez dans cette exp&eacute;rience vous sera vers&eacute;e sur un <strong>compte PayPal</strong> (et non en liquide). Vous pourrez soit utiliser directement cette somme pour faire des achats en ligne, soit la transf&eacute;rer gratuitement&nbsp; et facilement sur votre compte bancaire.</p>
					<ul>
					<li><strong>Si vous poss&eacute;dez d&eacute;j&agrave; un compte PayPal:</strong> nous vous demanderons, &agrave; la fin de l'exp&eacute;rience, de saisir l'adresse e-mail qui y est associ&eacute;e afin que nous puissions vous verser vos gains finaux.&nbsp;</li>
					<li><strong>Si vous ne poss&eacute;dez pas de compte PayPal:</strong> nous vous demanderons de saisir l'adresse e-mail qui sera utilis&eacute;e pour la cr&eacute;ation de votre compte. A la fin de l'exp&eacute;rience, vous recevrez un e-mail de PayPal vous guidant dans les quelques rapides &eacute;tapes amenant &agrave; la cr&eacute;ation de votre compte et la r&eacute;ception de vos gains finaux.</li>
					</ul>
					<p><strong>En aucun cas nous ne vous demanderons d'information bancaire personnelle afin de proc&eacute;der au paiement: une adresse e-mail valide suffit! </strong></p>
					$supzoomreg<p><hr></p></td></tr></table>";
					$ndays_suparr=preg_grep("/^\d{1,2}days$/",$asuppageoption2);
					if($suppageoption=="2days" || ($ndays_suparr!==false && count($ndays_suparr)>0)) 
					{ //in_array("2days",$asuppageoption2)
						$delaydays=0;
						if(trim($expepublicarr[0])=="expe47")$delaydays=3;
						if(trim($expepublicarr[0])=="expe142")$delaydays=14;
						foreach($sess_remark_list as $srem) {
							$asrem=explode(":",$srem);
							if(($asrem[0]=="daysdiff" || $asrem[0]=="diffdays" || $asrem[0]=="delaydays" || $asrem[0]=="daysdelay") && count($asrem)>1 && is_numeric($asrem[1])) $delaydays=$asrem[1];
						}
						$start_at_any_time_val=sessions__get_remark_list_elem_by_name($session_id,"start_at_any_time");
						$start_at_any_time=!empty($start_at_any_time_val) && $start_at_any_time_val!='n';
						if($delaydays>0) {
							$delaymonth=round($delaydays/30);
							$session_now=strtotime($session_date_time);
							$weekdays=array("lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche");
							$date_shown_start=date("d/m/Y à H:i",$session_now);
							if($start_at_any_time) {
								$date_shown_start=date("d/m/Y",$session_now).' entre '.date("H:i",$session_now).' et '.date("H:i",sessions__get_session_end_time($session));
							}
							$session_now_text="le ".$weekdays[date("N",$session_now)-1]." <strong>".$date_shown_start."</strong>";
							// echo "<hr>",$session_now,"<br>",$session_date_time,"<hr>";
							$session_timenext=$session_now; $session_timenext_text="";
							$nexpdays=2;
							if($ndays_suparr!==false && count($ndays_suparr)>0) foreach($ndays_suparr as $ndelem) {$cndays=(int)str_replace("days","",$ndelem); if($cndays>$nexpdays) $nexpdays=$cndays;}
							for($ndk=2; $ndk<=$nexpdays; $ndk++) {
								$session_timenext=strtotime("+ $delaydays days",$session_timenext);
								$session_timenext_text.=($ndk<$nexpdays)?", ":" et ";
								$date_shown_end=date("d/m/Y à H:i",$session_timenext);
								if($start_at_any_time) {
									$date_shown_end=date("d/m/Y",$session_timenext).' entre '.date("H:i",$session_timenext).' et '.date("H:i",sessions__get_session_end_time($session));
								}
								$session_timenext_text.="le ".$weekdays[date("N",$session_timenext)-1]." <strong>".$date_shown_end."</strong>";
							}
							// $excess_month=$session['session_start_month']+$delaymonth-12;
							// $switch_year=($excess_month>0);
							// $secondsessyear=($switch_year)?$session['session_start_year']+1:$session['session_start_year'];
							// $secondsessmonth=($switch_year)?$excess_month:$session['session_start_month']+$delaymonth;
						}
						$text2days.="<p align=center><p align=justify>Nous vous rappelons que cette expérience est composée de $nexpdays sessions";
						if($delaydays>0) $text2days.="&nbsp;: $session_now_text".$session_timenext_text;
						$text2days.=". VOUS DEVEZ POUVOIR PARTICIPER AUX $nexpdays SESSIONS pour vous inscrire";
						if($start_at_any_time_val=='y') $text2days.=' (pour chacune de ces sessions, vous pourrez vous connecter au moment de votre choix dans la plage horaire indiqu&eacute;e)';
						if($start_at_any_time_val=='n') $text2days.=' (notez aussi que chacune des sessions doit &ecirc;tre commenc&eacute;e <strong>&agrave; l&#39;heure</strong>)';
						$text2days.=".</p>";
					}
					if($suppageoption!="2days" && $ndays_suparr!==false && count($ndays_suparr)>0) $supzoomreg = "<p><hr></p>".$text2days.$supzoomreg;
					$textLydiaPartial="exp&eacute;rience se d&eacute;roule selon des modalit&eacute;s particuli&egrave;res. La somme d'argent en euros que vous&nbsp; gagnerez dans cette exp&eacute;rience vous sera vers&eacute;e sur un <strong>compte Lydia</strong> (<a href='https://lydia-app.com/' target=_blank>https://lydia-app.com/</a>). Vous pourrez soit utiliser directement cette somme pour faire des achats en ligne, soit la transf&eacute;rer gratuitement et facilement sur votre compte bancaire. ";
					if(in_array("lydia_short_version",$sess_remark_list)) $textLydiaPartial.="</p><p><b>Vous devez avoir un smartphone sous Android ou iPhone pour pouvoir ouvrir et utiliser un compte Lydia.</b></p>";
					else $textLydiaPartial.="La création du compte Lydia de base est gratuit (<a href='https://lydia-app.com/fr/about/price.html' target=_blank>voir ce lien pour plus de détails</a>).</p>
						<p><b>Vous devez avoir un smartphone sous Android ou iPhone pour pouvoir ouvrir un compte Lydia si vous ne le possédez pas.</b></p>
						<ul>
						<li><strong>Si vous poss&eacute;dez d&eacute;j&agrave; un compte Lydia:</strong> nous vous demanderons, &agrave; la fin de l'exp&eacute;rience, de saisir le numéro de téléphone ou email qui y est associ&eacute; afin que nous puissions vous verser vos gains finaux.&nbsp;</li>
						<li><strong>Si vous ne poss&eacute;dez pas de compte Lydia:</strong> nous vous demanderons de saisir le numéro de téléphone ou email qui sera utilis&eacute; pour la cr&eacute;ation de votre compte.</li>
						</ul>
						";					
					if($suppageoption=="lydia") {
						$textLydiaPartial.=$supzoomreg;
						$paypallogtable="expe_lydia_log";
						$textpaypal.="<table ><tr><td style='font-weight:normal;font-size: 14px;'><p align=center>INFORMATION IMPORTANTE :</p>
						<p>Cette $textLydiaPartial<p><hr></p></td></tr></table>";
					}
					if($suppageoption!="lydia" && $suppageoption!="forcewinlydia" && in_array("lydia",$expepublicarr2)) $supzoomreg.="<p><hr></p><p>Veuillez également noter que cette $textLydiaPartial";
					
					$textRibPartial="exp&eacute;rience sera r&eacute;mun&eacute;r&eacute;e par un <strong>virement bancaire</strong>. &Agrave; la fin de l'exp&eacute;rience, vous devrez <strong>renseigner vos nom, pr&eacute;nom, mail et coordonn&eacute;es bancaires (IBAN fran&ccedil;ais)</strong><!-- et signer (&eacute;lectroniquement pour les &eacute;xp&eacute;riences en ligne) le re&ccedil;u de paiement -->. Vos informations personnelles (identit&eacute;, coordonn&eacute;es bancaires) sont anonymis&eacute;es et n&rsquo;appara&icirc;tront jamais dans les publications issues de cette &eacute;tude.";
					if($suppageoption=="rib") {
						$textRibPartial.=$supzoomreg;
						$paypallogtable="expe_rib_log";
						$textpaypal.="<table ><tr><td style='font-weight:normal;font-size: 14px;'><p align=center>INFORMATION IMPORTANTE :</p>
						<p>Cette $textRibPartial<p><hr></p></td></tr></table>";
					}
					if($suppageoption!="rib" && in_array("rib",$expepublicarr2)) $supzoomreg.="<p><hr></p><p>Veuillez également noter que cette $textRibPartial";
					if($suppageoption=="documentaire") {
						$paypallogtable="expe_documentaire_log";
						$textpaypal="<table style='text-align:justify'><tr><td style='font-weight:normal;font-size: 14px;'><p align=center>INFORMATION IMPORTANTE :</p><p>L’exp&eacute;rience est organis&eacute;e pour les besoins d’un tournage d’un documentaire portant sur la recherche en &eacute;conomie exp&eacute;rimentale. Si vous participez &agrave; cette exp&eacute;rience, il vous sera demand&eacute; de signer une autorisation de diffusion pour les personnes film&eacute;es. 
						<br /> <br /> La session&nbsp; est pr&eacute;vue pour durer environ 2h tout compris : l’exp&eacute;rience proprement dite dure 1h15 environ et il est ensuite pr&eacute;vu d’&eacute;ventuelles prises de vue suppl&eacute;mentaires. 
						<br /> <br /> Cette exp&eacute;rience est r&eacute;mun&eacute;r&eacute;e et vous b&eacute;n&eacute;ficierez d’une gratification forfaitaire major&eacute;e.</p>
						$supzoomreg<p><hr></p></td></tr></table>";
					}
					if($suppageoption=="FNAC") {
						$paypallogtable="expe_fnac_log";
						$textpaypal="<table style='text-align:justify'><tr><td style='font-weight:normal;font-size: 14px;'><p align=center>INFORMATION IMPORTANTE :</p><p>Pour des raisons de gestion, cette expérience sera payé essentiellement en cartes FNAC à utiliser rapidement.</p>
						$supzoomreg<p><hr></p></td></tr></table>";
					}
					if($suppageoption=="eeg") {
						$paypallogtable="expe_eeg_log";
						$textpaypal="<table style='text-align:justify'><tr><td style='font-weight:normal;font-size: 14px;'><p align=center>INFORMATION IMPORTANTE :</p><p>L&rsquo;exp&eacute;rience implique un enregistrement &eacute;lectroenc&eacute;phalographique (EEG) au moyen d&rsquo;un casque.<br />Cet examen non invasif et indolore n&eacute;cessite l&rsquo;application d&rsquo;un gel non gras sur le cuir chevelu.</p>
						<div>Il vous sera possible de vous laver les cheveux apr&egrave;s l&rsquo;exp&eacute;rience, shampoing et serviette seront fournis.<br /><br />Apr&egrave;s une pause, le participant r&eacute;pondra &agrave; des questionnaires et des tests psychologiques.<br /><br />Etant donn&eacute;s le travail et le temps n&eacute;cessaires &agrave; la pr&eacute;paration de l&rsquo;exp&eacute;rience, nous vous solliciterons par t&eacute;l&eacute;phone pour confirmation de votre pr&eacute;sence afin d&rsquo;&eacute;viter les absences inopin&eacute;es.</div>
						<div>&nbsp;</div>
						<div>Le participant doit &ecirc;tre <u>droitier</u>.</div>
						<div>&nbsp;</div>
						<div>Il est indispensable que le cuir chevelu des participants ne soient&nbsp;<u>pas chauve, fris&eacute;s ou cr&eacute;pus</u>&nbsp;pour des raison de qualit&eacute; d&rsquo;enregistrement EEG.<br /><br />Le rendez-vous aura lieu dans le hall de la Maison de l&rsquo;Economie.<br /><br />La r&eacute;mun&eacute;ration est de 10 &euro; par heure pour une dur&eacute;e totale de 3 heures à 4 heures, soit 30 &agrave; 40&euro;.<br /><br /></div>
						$supzoomreg<p><hr></p></td></tr></table>";
					}
					if($suppageoption=="partanglais") {
						$paypallogtable="expe_english_log";
						$textpaypal="<table style='text-align:justify'><tr><td style='font-weight:normal;font-size: 14px;'><p align=center>INFORMATION IMPORTANTE :</p>
						<p>L&rsquo;interface de cette exp&eacute;rience sera partiellement <strong>en anglais</strong>. Il est nécessaire de comprendre l&rsquo;anglais écrit afin de pouvoir y participer.</p>
						$supzoomreg<p><hr></p></td></tr></table>";
					}
					if($suppageoption=="anglais") {
						$paypallogtable="expe_english_log";
						$textpaypal="<table style='text-align:justify'><tr><td style='font-weight:normal;font-size: 14px;'><p align=center>INFORMATION IMPORTANTE :</p>
						<p>L&rsquo;interface et les instructions de cette exp&eacute;rience seront <strong>en anglais</strong>. Une bonne compréhension de l'anglais est requis afin de pouvoir y participer. Un test de compréhension de l'anglais aura lieu avant le début de l'expérience.</p>
						<hr style='width:50%'><p>Please note that the experimental instructions and questions will be <strong>in English</strong>. English comprehension is required in order to participate in this experiment. An English comprehension test will be conducted before the experiment starts.</p>
						$supzoomreg<p><hr></p></td></tr></table>";
					}
					if($suppageoption=="assas") {
						$paypallogtable="expe_address_log";
						$textpaypal="<table style='text-align:justify'><tr><td style='font-weight:normal;font-size: 14px;'><p align=center>INFORMATION IMPORTANTE :</p>
						<p>L'expérience aura lieu à l'adresse suivante : <b>92 rue d'Assas 75006 Paris</b> (<a target=_blank href='https://www.u-paris2.fr/fr/universite/linstitution/implantations/centre-assas'>plan d'accès</a>)</p>
						$supzoomreg<p><hr></p></td></tr></table>";
					}
					if($suppageoption=="pulv") {
						$paypallogtable="expe_address_log";
						$textpaypal="<table style='text-align:justify'><tr><td style='font-weight:normal;font-size: 14px;'><p align=center>INFORMATION IMPORTANTE :</p>
						<p>L'expérience aura lieu à l'adresse suivante : <br><b>Pôle Léonard de Vinci<br>2/12 avenue Léonard de Vinci<br>92400 COURBEVOIE<br>(métro La Défense)</b> <br>(<a target=_blank href='https://leep.univ-paris1.fr/external/pulv.html'>plan d'accès</a>)</p>
						$supzoomreg<p><hr></p></td></tr></table>";
					}
					if($suppageoption=="jourdan") {
						$paypallogtable="expe_address_log";
						$textpaypal="<table style='text-align:justify'><tr><td style='font-weight:normal;font-size: 14px;'><p align=center>INFORMATION IMPORTANTE :</p>
						<p>L'expérience aura lieu à l'adresse suivante : <b>48 boulevard Jourdan, 75014 Paris</b> (<a target=_blank href='https://www.parisschoolofeconomics.eu/fr/acces/'>plan d'accès</a>)</p>
						$supzoomreg<p><hr></p></td></tr></table>";
					}
					if($suppageoption=="info") {
						$paypallogtable="expe_supinfo_log";
						$textpaypal="<table style='text-align:justify'><tr><td style='font-weight:normal;font-size: 14px;'><p align=center>INFORMATION IMPORTANTE :</p>
						<p>".experiment__get_value($experiment_id,'add_step_subscr_info').$sess_sup_subscr_message."</p>
						$supzoomreg<p><hr></p></td></tr></table>";
					}
					if($suppageoption=="2days")
					{
						$paypallogtable="expe_2days_log";
						$already_tried_tosubstruct=0;
						$textpaypal="<table style='text-align:justify'><tr><td style='font-weight:normal;font-size: 14px;'><p align=center>INFORMATION IMPORTANTE</p>";
						$textpaypal.=$text2days;
						$textpaypal.="$supzoomreg<p><hr></p></td></tr></table>";
					}
					if($suppageoption=="reservation")
					{
						$paypallogtable="expe_reservation_log";
						$already_tried_tosubstruct=0;
						$textpaypal="<table style='text-align:justify'><tr><td style='font-weight:normal;font-size: 14px;'><p align=center>INFORMATION IMPORTANTE</p>";
						$textpaypal.="<p>";
						$textpaypal.=lang("it_will_be_necessary to_answer_a_questionnaire in_order_to_confirm_your_registration").".<br>";
						if(empty($reservation_autoload))$textpaypal.=lang("the_link_to_the_questionnaire_will_be_shown_on_the_next_page if_you_accept_to_participate").". ";
						else $textpaypal.=lang("if_you_accept_to_participate you_will_be_redirected_to_the_questionnaire").". ";
						$textpaypal.=lang('your_will_have')." ".$reservation_minutes." ".lang('minutes',false).". ".lang('after_that_time_your_registration_is_not_guaranteed').". ";
						$textpaypal.="</p>";
						$textpaypal.="$supzoomreg<p><hr></p></td></tr></table>";
						$participant_not_eligible=false;
						$crf="SELECT id FROM ".table('participants_log')." WHERE action='book_fail' AND target LIKE '".mysqli_real_escape_string($GLOBALS['mysqli'],"experiment_id:".$experiment_id."\n%")."' ORDER BY log_id";
						$pidscrf=orsee_query($crf,"return_first_elem");
						$mycrfindex=-1;
						if($pidscrf!==false) foreach(array_values(array_unique($pidscrf)) as $pk=>$cpid) if($cpid==$participant['participant_id']) $mycrfindex=$pk;
						if($mycrfindex>=0) $participant_not_eligible=true;
						if($participant_not_eligible)
						{
							$text_denial="<p align=center>".lang("the_participants_of_this_experiment_must_correspond_to_a_particular_profile").". ".lang("you_dont_have_this_profile or we_have_already_enough_participants_of_your_profile and unforunately_we_cannot_benefit_from_your_participation").".</p>";
							// "Pour des raisons scientifiques, les participants à cette expérience doivent avoir un profil particulier.</p><p align=center>Vous n'avez pas ce profil et nous ne pouvons malheureusement pas bénéficier de votre participation."
							// if($questionnaire->quota) $text_denial="<p align=center>Pour des raisons scientifiques, nous cherchons des participants avec des profils particuliers pour cette expérience.</p><p align=center>Nous avons déjà un nombre suffisant des participants avec votre profil et nous ne pouvons malheureusement pas bénéficier de votre participation en ce moment.</p>";
							$textpaypal="<table style='text-align:justify'><tr><td style='font-weight:normal;font-size: 14px;'>$text_denial
							<p><hr></p></td></tr></table>";
							$show_yes_button=false;
							$no_text="Retour"; $normaldecline="betternot";
							$do_you_really_want_text="";
						}
					}
					if($suppageoption=="supquest") {
						$paypallogtable="supquestlogtable";
						include_once("supquest.php");
						$participant_not_eligible=false;
						$participant_eligible=false;
						$registeredEligibility=$questionnaire->eligibilityDone();
						$already_tried_tosubstruct=1;
						if($registeredEligibility!==false)
						{
							if($registeredEligibility==1) $participant_eligible=true;
							if($registeredEligibility==0) $participant_not_eligible=true;
						}
						$questresult_eligible=$questionnaire->eligible();
						if($questresult_eligible!==false) {
							if($questresult_eligible==1) {
								$participant_eligible=true;
							}
							else {
								$participant_not_eligible=true;
							}
							$normalsubmit="reallyregister";
							$questionnaire->save($questresult_eligible);
						}
						elseif(!$participant_eligible && !$participant_not_eligible) {
							$normalsubmit="reallyregisterbeforepaypal";
							if(!$questionnaire->alreadyQuest) $already_tried_tosubstruct=0;							
							$textpaypal="<table style='text-align:justify'><tr><td style='font-weight:normal;font-size: 14px;'>";
							$textpaypal.="<p>Pour cette expérience, nous avons besoin des participants qui correspondent à des critères spécifiques. Vous devez remplir le questionnaire ci-dessous pour savoir si vous pouvez y participer.</p>";
							$textpaypal.="<p>".$questionnaire->makeQuestions()."</p>
							<p><hr></p></td></tr></table>";
							$yes_text="OK"; $no_text="Annuler";
							$do_you_really_want_text="Appuyez sur $yes_text pour soumettre vos réponses";
						}
						if($participant_not_eligible)
						{
							$text_denial="<p align=center>Pour des raisons scientifiques, les participants à cette expérience doivent avoir un profil particulier.</p><p align=center>Vous n'avez pas ce profil et nous ne pouvons malheureusement pas bénéficier de votre participation.</p>";
							if($questionnaire->quota) $text_denial="<p align=center>Pour des raisons scientifiques, nous cherchons des participants avec des profils particuliers pour cette expérience.</p><p align=center>Nous avons déjà un nombre suffisant des participants avec votre profil et nous ne pouvons malheureusement pas bénéficier de votre participation en ce moment.</p>";
							$textpaypal="<table style='text-align:justify'><tr><td style='font-weight:normal;font-size: 14px;'>$text_denial
							<p><hr></p></td></tr></table>";
							$show_yes_button=false;
							$no_text="Retour"; $normaldecline="betternot";
							$do_you_really_want_text="";
						}
						if($participant_eligible)
						{
							$already_tried_tosubstruct=0;
							$textpaypal="<table style='text-align:justify'><tr><td style='font-weight:normal;font-size: 14px;'><p align=center>".lang('we_are_happy_to_invite_you_to_participate_in_this_experiment').".</p>";
							if($experimentpublicname=="expe110 (sur 2 jours)") {
								$delaydays=28;
								$delaymonth=round($delaydays/30);
								$session_now=strtotime($session_date_time);
								// echo "<hr>",$session_now,"<br>",$session_date_time,"<hr>";
								$session_timenext=strtotime("+ $delaydays days",$session_now);
								$weekdays=array("lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche");
								$session_now_text="le ".$weekdays[date("N",$session_now)-1]." <strong>".date("d/m/Y à H:i",$session_now)."</strong>";
								$session_timenext_text="le ".$weekdays[date("N",$session_timenext)-1]." <strong>".date("d/m/Y à H:i",$session_timenext)."</strong>";
								$excess_month=$session['session_start_month']+$delaymonth-12;
								$switch_year=($excess_month>0);
								$secondsessyear=($switch_year)?$session['session_start_year']+1:$session['session_start_year'];
								$secondsessmonth=($switch_year)?$excess_month:$session['session_start_month']+$delaymonth;
								$textpaypal.="<p align=center>INFORMATION IMPORTANTE :<p align=justify>Lors de cette expérience, les données anthropométriques (votre poids et taille) peuvent être mesurées. La pesée doit s'effectuer selon les conditions suivantes&nbsp;: vous devez enlever les vetêments lourds et les chaussures. Ces données resteront strictement confidentielles et à usage exclusif des investigateurs concernés.</p></p>";
								$textpaypal.="<p align=center><p align=justify>Par ailleurs, nous vous rappelons que cette expérience est composée de 2 sessions&nbsp;: $session_now_text et $session_timenext_text. VOUS DEVEZ POUVOIR VENIR LES 2 JOURS pour y participer.</p></p>";
							}
							if($experimentpublicname=="expe124_eeg") {
								$paypallogtable="expe_eeg_log";
								$textpaypal.="<table style='text-align:justify'><tr><td style='font-weight:normal;font-size: 14px;'><p align=center>INFORMATION IMPORTANTE :</p><p>L&rsquo;exp&eacute;rience implique un enregistrement &eacute;lectroenc&eacute;phalographique (EEG) au moyen d&rsquo;un casque.<br />Cet examen non invasif et indolore n&eacute;cessite l&rsquo;application d&rsquo;un gel non gras sur le cuir chevelu.</p>
								<div>Il vous sera possible de vous laver les cheveux apr&egrave;s l&rsquo;exp&eacute;rience, shampoing et serviette seront fournis.<br /><br />Apr&egrave;s une pause, le participant r&eacute;pondra &agrave; des questionnaires et des tests psychologiques.<br /><br />Etant donn&eacute;s le travail et le temps n&eacute;cessaires &agrave; la pr&eacute;paration de l&rsquo;exp&eacute;rience, nous vous solliciterons par t&eacute;l&eacute;phone pour confirmation de votre pr&eacute;sence afin d&rsquo;&eacute;viter les absences inopin&eacute;es.</div>
								<div>&nbsp;</div>
								<div>Le rendez-vous aura lieu dans le hall de la Maison de l&rsquo;Economie.<br /><br />La r&eacute;mun&eacute;ration est de 10 &euro; par heure pour une dur&eacute;e totale de 3 heures à 4 heures, soit 30 &agrave; 40&euro;.<br /><br /></div>
								<p><hr></p></td></tr></table>";
							}
							$textpaypal.="$supzoomreg<p><hr></p></td></tr></table>";
						}
					}
					if($suppageoption=="by_couples") {
						$paypallogtable="bycoupleslogtable";
						include_once("bycouples.php");
						$bchelper = new ByCouplesHelper();
						$participant_not_eligible=false;
						$participant_eligible=false;
						$registeredEligibility=$bchelper->eligibilityDone();
						// var_dump($registeredEligibility);
						if($registeredEligibility!==false)
						{
							if($registeredEligibility==0 || $registeredEligibility=='no' || $registeredEligibility=='n') $participant_not_eligible=true;
							else $participant_eligible=true;
						}
						$questresult_eligible=$bchelper->eligible();
						// var_dump($questresult_eligible);
						if($questresult_eligible!==false) {
							if($questresult_eligible==1) {
								$participant_eligible=true;
							}
							else {
								$participant_not_eligible=true;
							}
							$normalsubmit="reallyregister";
							$bchelper->save($questresult_eligible);
							
						}
						elseif(!$participant_eligible && !$participant_not_eligible) {
							$normalsubmit="reallyregisterbeforepaypal";					
							$textpaypal="<table style='text-align:justify'><tr><td style='font-weight:normal;font-size: 14px;'>";
							// $textpaypal.=$text2days.$supzoomreg;
							// $textpaypal.="<p>".lang("please_enter_your_partner's_mail")."</p>";
							$iminthepool=true;
							if(!empty($bchelper->restricttotype)) {
								$me=rasee_db_load_array('participants',array('participant_id'=>$participant_id));
								$subscriptions=explode(",",$me['subscriptions']);
								// var_dump($subscriptions);
								if(!in_array($bchelper->restricttotype[0],$subscriptions)) $iminthepool=false;
							}
							$yes_text="OK"; $no_text=lang('cancel');
							if($iminthepool){
								$textpaypal.="<p align=center>".lang("IMPORTANT_INFORMATION")." :<p align=justify>".lang('this_is_a_by_couples_experiment').". ".lang("you_should_register_for_the_same_session_with_your_partner");
								if($bchelper->just_registered) $textpaypal.='. '.$bchelper->emailForm(false,true);
								$textpaypal.=".</p></p>";
								$textpaypal.="<p>".$bchelper->emailForm()."</p>";
							}
							else {
								$consent=lang("I_agree_to_participate_in_this_experiment_and_come_with_my_partner");
								$special_subject="couples";
								if(!empty(content__get_content($special_subject."_consentment"))) {
									$consentment=explode("|",content__get_content($special_subject."_consentment"));
									$consentment0=explode('. ',$consentment[0]);
									$consent=(count($consentment0)>1)?$consentment0[1]:$consentment;
								}
								$yes_text=lang('yes'); $no_text=lang('no');
								$textpaypal.="<p style='font-weight:bold'>".$consent."</p>";
								$textpaypal.="<input type=hidden value='yes_i_consent' name='".$special_subject."_consentment'/>";
							}
							$textpaypal.="<p><hr></p></td></tr></table>";
							$do_you_really_want_text="";
						}
						if($participant_not_eligible)
						{
							// $text_denial="<p align=center>Pour des raisons scientifiques, les participants à cette expérience doivent avoir un profil particulier.</p><p align=center>Vous n'avez pas ce profil et nous ne pouvons malheureusement pas bénéficier de votre participation.</p>";
							$text_denial=$bchelper->getMessage();
							$textpaypal="<table style='text-align:justify'><tr><td style='font-weight:normal;font-size: 14px;'><strong>$text_denial</strong>
							<p><hr></p></td></tr></table>";
							$show_yes_button=false;
							$no_text=lang('back'); $normaldecline="betternot"; //$no_text="Retour"
							$do_you_really_want_text="";
						}
						if($participant_eligible)
						{
							$textpaypal="<table style='text-align:justify'><tr><td style='font-weight:normal;font-size: 14px;'><p align=center>".lang('we_are_happy_to_invite_you_to_participate_in_this_experiment').".</p>";
							$textpaypal.=$bchelper->getMessage();
							$show_yes_button=true;
							$do_you_really_want_text="";
							$show_no_button=false;
							$yes_text="OK";
							$textpaypal.=$text2days."$supzoomreg<p><hr></p></td></tr></table>";
							if(empty($supzoomreg) && empty($text2days)) $textpaypal.=$bchelper->eligibleHtml($yes_text);
						}
					}
					$WrongOS="";
					if(preg_match("/^forcewin/i",$suppageoption)) {
						$paypallogtable="winchecklogtable";
						$participant_not_eligible=false;
						$participant_eligible=false;
						$os=helpers_get_os(); $win=(strtolower(substr($os,0,3)) == "win");
						$registeredEligibility=1;
						if (!in_array("winlight",$expepublicarr2) && trim($expepublicarr2[1])!="windows" && !($win && !helpers_detect_mobile())) {
							$WrongOS=$os;
							$registeredEligibility=0;
						}			
						$already_tried_tosubstruct=1;
						if($registeredEligibility!==false)
						{
							if($registeredEligibility==1) $participant_eligible=true;
							if($registeredEligibility==0) $participant_not_eligible=true;
						}
						$normalsubmit="reallyregister";
						if($participant_not_eligible)
						{
							$text_denial="<p align=center>Pour participer à cette expérience, vous devrez télécharger un programme qui ne fonctionnera que sur un ordinateur sous <strong>Windows</strong>. Pour cette raison, nous vous demandons de vous inscrire avec un ordinateur sous Windows qui sera utilisé pour l'expérience.</p>";
							$textpaypal="<table style='text-align:justify'><tr><td style='font-weight:normal;font-size: 14px;'>$text_denial
							</td></tr></table>";
							$show_yes_button=false;
							$no_text="Retour"; $normaldecline="betternot";
							$do_you_really_want_text="";
						}
						if($participant_eligible)
						{
							$already_tried_tosubstruct=0;
							$textpaypal="<table style='text-align:justify'><tr><td style='font-weight:normal;font-size: 14px;'><p align=center>Veuillez noter que pour cette expérience, vous devrez <strong>télécharger et éxécuter un programme</strong> qui ne fonctionnera que sur un ordinateur sous <strong>Windows</strong>.</p>";
							if($suppageoption=="forcewinpaypal") {
								$textpaypal.="<table ><tr><td style='font-weight:normal;font-size: 14px;'><p align=center>INFORMATION IMPORTANTE :</p>
								<p>Cette exp&eacute;rience se d&eacute;roule selon des modalit&eacute;s particuli&egrave;res. La somme d'argent en euros que vous&nbsp; gagnerez dans cette exp&eacute;rience vous sera vers&eacute;e sur un <strong>compte PayPal</strong> (<a href='https://www.paypal.com' target=_blank>https://www.paypal.com/</a>). Vous pourrez soit utiliser directement cette somme pour faire des achats en ligne, soit la transf&eacute;rer gratuitement&nbsp; et facilement sur votre compte bancaire.</p>
								<ul>
								<li><strong>Si vous poss&eacute;dez d&eacute;j&agrave; un compte PayPal:</strong> nous vous demanderons, &agrave; la fin de l'exp&eacute;rience, de saisir l'adresse e-mail qui y est associ&eacute;e afin que nous puissions vous verser vos gains finaux.&nbsp;</li>
								<li><strong>Si vous ne poss&eacute;dez pas de compte PayPal:</strong> nous vous demanderons de saisir l'adresse e-mail qui sera utilis&eacute;e pour la cr&eacute;ation de votre compte. A la fin de l'exp&eacute;rience, vous recevrez un e-mail de PayPal vous guidant dans les quelques rapides &eacute;tapes amenant &agrave; la cr&eacute;ation de votre compte et la r&eacute;ception de vos gains finaux.</li>
								</ul>
								<p><strong>En aucun cas nous ne vous demanderons d'information bancaire personnelle afin de proc&eacute;der au paiement: une adresse e-mail valide suffit! </strong></p>
								$supzoomreg</td></tr></table>";
							}
							elseif($suppageoption=="forcewinlydia") {
								$textpaypal.="<table ><tr><td style='font-weight:normal;font-size: 14px;'><p align=center>INFORMATION IMPORTANTE :</p>
								<p>Cette exp&eacute;rience se d&eacute;roule selon des modalit&eacute;s particuli&egrave;res. La somme d'argent en euros que vous&nbsp; gagnerez dans cette exp&eacute;rience vous sera vers&eacute;e sur un <strong>compte Lydia</strong> (<a href='https://lydia-app.com/' target=_blank>https://lydia-app.com/</a>). Vous pourrez soit utiliser directement cette somme pour faire des achats en ligne, soit la transf&eacute;rer gratuitement&nbsp; et facilement sur votre compte bancaire. La création du compte Lydia de base est gratuit (<a href='https://lydia-app.com/fr/about/price.html' target=_blank>voir ce lien pour plus de détails</a>).</p>
								<p><b>Vous devez avoir un smartphone sous Android ou iPhone pour pouvoir ouvrir un compte Lydia si vous ne le possédez pas.</b></p>
								<ul>
								<li><strong>Si vous poss&eacute;dez d&eacute;j&agrave; un compte Lydia:</strong> nous vous demanderons, &agrave; la fin de l'exp&eacute;rience, de saisir le numéro de téléphone ou email qui y est associ&eacute; afin que nous puissions vous verser vos gains finaux.&nbsp;</li>
								<li><strong>Si vous ne poss&eacute;dez pas de compte Lydia:</strong> nous vous demanderons de saisir le numéro de téléphone ou email qui sera utilis&eacute; pour la cr&eacute;ation de votre compte.</li>
								</ul>
								$supzoomreg</td></tr></table>";
							}
							else $textpaypal.=$supzoomreg;
							$textpaypal.="<p><hr></p></td></tr></table>";
						}
					}
					$textpaypal.="<input type=hidden name='paypalinfotable' value='$paypallogtable' >
					<input type=hidden name='suppageoption' value='$suppageoption' >";
					$query="CREATE TABLE IF NOT EXISTS `$paypallogtable` (
					`participant_id` text NULL,
					`session_id` text NULL,
					`session_date_time` text NULL,
					`date_time` datetime NULL,
					`gender` char(1) NULL,
					`field_of_studies` text NULL,
					`begin_of_studies` int(5) NULL,
					`profession` text NULL,
					`number_reg` int(5) NULL,
					`number_noshowup` int(5) NULL,
					`already_tried` smallint NULL,
					`explicit_answer_paypal` tinyint NOT NULL DEFAULT 0,
					`openanswer` longtext NULL
					)";
					if(!mysqli_query($GLOBALS['mysqli'],$query)) {echo"$query<br>Error n°".mysqli_errno($GLOBALS['mysqli']).":".mysqli_error($GLOBALS['mysqli']);}
					$langstudies=orsee_db_load_array("lang",$participant['field_of_studies'],"content_name");
					$langprofession=orsee_db_load_array("lang",$participant['profession'],"content_name");
					//echo "participantslangstudycode=".$participant['field_of_studies']; print_r($langstudies);
					$query="SELECT * FROM $paypallogtable WHERE participant_id='$participant_id' AND session_id='$session_id'";
					$cresult=mysqli_query($GLOBALS['mysqli'],$query); $already_tried=mysqli_num_rows($cresult); $prevanswer=""; while ($already_tried && $crow = mysqli_fetch_array($cresult, MYSQLI_ASSOC)) {$already_tried+=$crow['already_tried']-$already_tried_tosubstruct; $prevanswer=$crow['openanswer'];}
					if(empty($prevanswer) && !empty($WrongOS)) $prevanswer="Wrong OS: $WrongOS";
					$query="DELETE FROM $paypallogtable WHERE participant_id='$participant_id' AND session_id='$session_id'";
					if(!mysqli_query($GLOBALS['mysqli'],$query)) {echo"$query<br>Error n°".mysqli_errno($GLOBALS['mysqli']).":".mysqli_error($GLOBALS['mysqli']);}
					$query="INSERT INTO $paypallogtable (participant_id, session_id, session_date_time, date_time, gender, field_of_studies, begin_of_studies, profession, number_reg, number_noshowup, already_tried, openanswer) VALUES ('$participant_id', '$session_id', '$session_date_time', '".date("Y-m-d H:i:s")."', '".$participant['gender']."', '".str_replace("'"," ",$langstudies['fr'])."', '".((int)$participant['begin_of_studies'])."', '".str_replace("'"," ",$langprofession['fr'])."', '".$participant['number_reg']."', '".$participant['number_noshowup']."', '$already_tried', '$prevanswer') ";
					if(!mysqli_query($GLOBALS['mysqli'],$query)) {echo"$query<br>Error n°".mysqli_errno($GLOBALS['mysqli']).":".mysqli_error($GLOBALS['mysqli']);}
				}
			}

			echo '
                		<center><BR><BR>
                		<H4>'.$lang['experiment_registration'].'</H4>
                		<BR><BR>

				<form action="participant_show.php">
				<INPUT type=hidden name="s" value="'.$_REQUEST['s'].'">
				<INPUT type=hidden name="p" value="'.url_cr_encode($participant_id).'">
				<INPUT type=hidden name="register" value="true">

				<TABLE width=80%>
				<TR>
					<TD colspan=2 align=center>
						'.$textpaypal.'
						'.$do_you_really_want_text.'
					</TD>
				</TR>
				<TR>';
				if (empty($_REQUEST["reallyregisterbeforepaypal"])) echo '

					<TD align=right width=50%>
						'.$lang['experiment'].':
					</TD>
					<TD width=50%>
						'.experiment__get_public_name($experiment_id).'
					</TD>
				</TR>
				<TR>
					<TD align=right>
						'.$lang['date_and_time'].':
					</TD>
					<TD>
						'.time__format_session_time($session_id).'
					</TD>
				</TR>
				<TR>
					<TD align=right>
						'.$lang['laboratory'].':
					</TD>
					<TD>
						'.laboratories__get_laboratory_name($session['laboratory_id']).'
					</TD>
				</TR>
				<TR>
					<TD colspan=2>&nbsp;</TD>
				</TR>';
				echo '
				<TR>
					<TD align=center colspan=2>';
					if($show_yes_button) echo '	<INPUT type=submit name="'.$normalsubmit.'" value="'.$yes_text.'">';
					if($show_yes_button && $show_no_button) echo '	&nbsp;&nbsp;&nbsp;&nbsp;';


					if($show_no_button) echo '	<INPUT type=submit name="'.$normaldecline.'" value="'.$no_text.'">';
				echo '
					</TD>
				</TR>
				</TABLE>
				</FORM>
				</center>

				';
			}
	   	}
   else {

	echo '<SCRIPT LANGUAGE="JavaScript">
		function openprint() {
		printwin=open("participant_show_print.php?p='.$_REQUEST['p'].'", "faq", "width=700,height=500,location=no,toolbar=yes,menubar=no,status=no,directories=no,scrollbars=yes,resizable=yes") 
		}
		</SCRIPT>
		';

	echo '
		<center>
		<BR><BR>
		<H4>'.$lang['experiment_registration'].'</H4>
		<BR><BR>


		<TABLE width=80%>
		<TR><TD colspan=3 bgcolor="'.$color['list_title_background'].'">';
	echo $lang['experiments_you_are_invited_for'];
	echo '</TD>
		</TR>
		<TR><TD colspan=3>
		'.$lang['please_check_availability_before_register'].'
		</TD></TR>';

	$labs=expregister__list_invited_for($participant_id,"expregister__list_invited_for_format",true);

	$registered_list=expregister__list_registered_for($participant_id,"","exp_sess_only",true);
	// echo "\n <!-- "; echo "labs registered list: \n"; print_r($registered_list); echo " --> ";
	if(count($registered_list)>0) echo '<TR><TD colspan=3>&nbsp;</TD></TR>
		<TR><TD colspan=3 bgcolor="'.$color['list_title_background'].'">
		'.$lang['experiments_already_registered_for'].'
		(<A HREF="javascript:openprint()">'.$lang['print_version'].'</A>)
		</TD>
		</TR>';

	$labs2=expregister__list_registered_for($participant_id,$session_id);

	echo '<TR><TD colspan=3>&nbsp;</TD></TR>';

	$laboratories=array_unique(array_merge($labs,$labs2));
	// echo "\n <!-- "; echo "labs invited: \n"; print_r($labs); echo "\n labs registered: \n"; print_r($labs2); echo "\n all labs: \n"; print_r($laboratories); echo " --> ";
	$labs_empty=empty($laboratories); //(count($laboratories)==0);
	if(!$labs_empty) {$labs_empty=true; foreach($laboratories as $lab) if(!empty($lab)) $labs_empty=false;}
	// var_dump(empty($labs),empty($labs2),empty($laboratories),$labs_empty);
	if (!$labs_empty) {
		echo '<TR><TD colspan=3>';

		echo '<TABLE rules=all cellpadding=3 width=100%>
			<TR bgcolor="white"><TD colspan=2>';
		if (count($laboratories)==1) echo $lang['laboratory_address']; else echo $lang['laboratory_addresses'];
		echo '</TD></TR>';
		
		// $sess_remark_list=sessions__get_remark_list($session_id);

		foreach ($laboratories as $laboratory_id) {
			$labname=laboratories__get_laboratory_name($laboratory_id);
			$address=laboratories__get_laboratory_address($laboratory_id);
			if(empty($labname) && empty($address)) continue;
			$c=0;
			$address=preg_replace("/\b(http[s]?[:]\/\/.+)(\s|$)/","<a target=_blank href='$1'>$1</a>$2",$address,-1,$c);
			$suptdstyle="";
			// if(in_array("!less_important!",$sess_remark_list)) $suptdstyle=' style="color:#777"';
			echo '<TR><TD valign=top'.$suptdstyle.'>';
			echo $labname;//."-".$c;
			echo '</TD>
				<TD'.$suptdstyle.'>';
			echo str_replace("\n","<BR>",$address);
			echo '</TD></TR>';
			}
		echo '</TABLE></TD></TR>';
		echo '<TR><TD colspan=3>&nbsp;</TD></TR>';
		}


	echo '<TR><TD colspan=3 bgcolor="'.$color['list_title_background'].'">
		'.$lang['experiments_you_participated'].'
		</TD></TR>
		<TR><TD colspan=3>
		'.$lang['registered_for'].' '.$participant['number_reg'].'<BR>
		'.$lang['not_shown_up'].' '.$participant['number_noshowup'].'
		</TD>
		</TR>';
	expregister__list_history($participant_id);
	echo '</TABLE>';

	echo '<BR><BR><A HREF="participant_edit.php?p='.url_cr_encode($participant_id).'">'.$lang['edit_your_profile'].'</A>';

	echo '</center>';

	}

include("footer.php");


?>
<?php
// part of rasee
if(!isset($_REQUEST["exptc"]) || empty($_REQUEST["sesscodelen"]) || empty($_REQUEST["sesstc"])) exit("- You are not authorised -");
if(empty($_REQUEST["exptc"]) && empty($_REQUEST["cpid"]) && empty($_REQUEST["external_code"])) exit("- You are not authorised -");
$strtocheck=".".$_REQUEST["exptc"].".".$_REQUEST["sesscodelen"].".".$_REQUEST["sesstc"];
if(preg_match("/\s|SELECT|UPDATE|DELETE|DBMS_PIPE/i",$strtocheck)) {
	// var_dump($strtocheck);
	exit("- You are not authorised.");
}
if(str_replace("||","",$strtocheck)!=$strtocheck) exit("- You are not authorised!");
include("nonoutputheader.php");
$res="- notfound -";
$res=0;

$pid=0; $flname=""; $participant=array(); $sess_ids=array();
if(!empty($_REQUEST["cpid"])) {
	$pid=url_cr_decode($_REQUEST["cpid"],true);
	if(empty($pid)) result(2);
	$participant=orsee_db_load_array("participants",$pid,"participant_id");
	$flname=$participant["fname"].",".$participant["lname"];
}
if(!empty($_REQUEST["cpid"]) || !empty($_REQUEST["external_code"])) {
	$where=" WHERE registered='y'";
	if(!empty($_REQUEST["external_code"])) $where.=" AND external_code='".$_REQUEST["external_code"]."'";
	if(!empty($pid)) $where.=" AND participant_id=".$pid."";
	if(!empty($_REQUEST["exptc"])) $where.=" AND experiment_id='".$_REQUEST["exptc"]."'";
	$where.=" AND payghost=0";
	$q="SELECT session_id FROM ".table("participate_at").$where;
	$q.=" ORDER BY participate_id";
	$sess_ids=orsee_query($q,"return_first_elem");
}

$expidquery=""; 
if(!empty($_REQUEST["exptc"])) $expidquery=sprintf(" experiment_id='%s' AND",mysqli_real_escape_string($GLOBALS['mysqli'],$_REQUEST["exptc"]));
$tquery=sprintf("SELECT session_id FROM ".table("sessions")." WHERE".$expidquery." session_remarks like '%%#t%dmd5-%d#%%' AND session_remarks NOT LIKE '%%!second_day!%%' AND session_remarks NOT LIKE '%%!third_day!%%' AND session_remarks NOT LIKE '%%!api_invisible!%%' AND session_finished='n'",mysqli_real_escape_string($GLOBALS['mysqli'],$_REQUEST["sesscodelen"]),mysqli_real_escape_string($GLOBALS['mysqli'],$_REQUEST["sesstc"]));
$tquery.=" ORDER BY session_start_year,session_start_month,session_start_day,session_start_hour,session_start_minute";
$sessfound=false;
$lines=orsee_query($tquery,"return_first_elem");
// var_dump($sess_ids,$_REQUEST);
if(!empty($lines)) {
	if(!empty($sess_ids)) {
		$sess_inters=array_intersect($sess_ids,$lines);
		if(!empty($sess_inters)) {
			$csess_id=end($sess_inters);
			$sessfound=true;
		}
	}
	if(!$sessfound && !empty($_REQUEST["exptc"])) {
		$csess_id=end($lines);
		$sessfound=true;
	}
}
elseif(!empty($_REQUEST["exptc"]) && count($sess_ids)==1) {
	$csess_id=end($sess_ids);
	$sessfound=true;	
}
if($sessfound) {
	$csess_starttime=sessions__get_session_time(array(),$csess_id); $csess_endtime=sessions__get_session_end_time(array(),$csess_id);
	$res=$csess_starttime."-".$csess_endtime;
}
echo $res;
// echo "<br>".date_default_timezone_get();
?>

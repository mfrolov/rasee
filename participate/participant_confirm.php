<?php
// part of orsee. see orsee.org
ob_start();
$temp__nosession=true;
include("header.php");

	echo '<BR><BR>
		<center><BR><BR><h4>'.$lang['confirm_registration'].'</h4>
		';

	$p=empty($initial_cpid)?$_REQUEST['p']:$initial_cpid;
	// var_dump($p,$_REQUEST['p']);var_dump($initial_cpid); exit;
	$geschickt__p=$p;
	$p=url_cr_decode($p,true);
	$continue=true;

	if (!$p) {
		message($lang['confirmation_error']);
		redirect($GLOBALS['settings__public_folder']."/");
		}

	$already_confirmed=participant__participant_id_exists($p);

        if ($already_confirmed) {
                message($lang['already_confirmed_error']);
                redirect($GLOBALS['settings__public_folder']."/?p=".url_cr_encode($p));
				exit;
        }


	// copy from participants_temp to participants
	// orsee_query("ALTER TABLE ".table('participants_temp')." DROP COLUMN last_delete_time, DROP COLUMN last_delete_admin, DROP COLUMN can_undelete, DROP COLUMN twinto");
	// orsee_query("ALTER TABLE ".table('participants')." DROP COLUMN last_delete_time, DROP COLUMN last_delete_admin, DROP COLUMN can_undelete, DROP COLUMN twinto");
	orsee_query("SET SESSION innodb_strict_mode=OFF");
	query_makecolumns(table('participants_temp'),array("last_delete_time","last_delete_admin","can_undelete","twinto"),array("INT(20)","VARCHAR(20)","tinyint(1)","TEXT NOT NULL"),array(0,"''",0,"''"));
	query_makecolumns(table('participants'),array("last_delete_time","last_delete_admin","can_undelete","twinto"),array("INT(20)","VARCHAR(20)","tinyint(1)","TEXT NOT NULL"),array(0,"''",0,"''"));
	$query="INSERT INTO ".table('participants')." SELECT * FROM ".table('participants_temp')." WHERE participant_id='".mysqli_real_escape_string($GLOBALS['mysqli'],$p)."'";
	$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error at participant_confirm.php: " . mysqli_error($GLOBALS['mysqli']));

	if (!$done) {
		message($lang['database_error']);
		redirect($GLOBALS['settings__public_folder']."/");
		}

	// delete old entry

        $query="DELETE FROM ".table('participants_temp')." WHERE participant_id='".mysqli_real_escape_string($GLOBALS['mysqli'],$p)."'";
        $done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

	// load participant package
	$participant=orsee_db_load_array("participants",$p,"participant_id");

	log__participant("confirm",$p,$participant['subscriptions']);



	$mess=$lang['registration_confirmed'].'<BR><BR>
		'.$lang['you_will_be_invited_to'].':<BR><BR>
			<UL>';

		$exptypes=explode(",",$participant['subscriptions']);
		$typenames=load_external_experiment_type_names();

		foreach ($exptypes as $type) {
			$mess.='<LI>'.$typenames[$type].'</LI>';
			}
			$mess.='</UL>
				<BR>
				'.$lang['thanks_for_registration'];
		$mess=str_replace("#my_data#",'<a href="'.$settings__root_url.'/'.$GLOBALS['settings__public_folder'].'/participant_edit.php?p='.url_cr_encode($p).'">'.lang("my_data")."</a>",$mess);

	message($mess);
	show_message();

	echo '</center>';

include("footer.php");

include_once("confirm_include_doublons.php");


?>

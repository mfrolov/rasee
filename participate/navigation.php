// navigation.php. part of orsee. see orsee.org. 
//
// menu entry format:
//     0         1          2      3   4     5     6        7          8
// entrytype|menu__area|lang_item|url|icon|target|addp?|showonlyifp?|hideifp?
// headlink|welcome|welcome|../../index.htm|||true
// head|pipe|pipe|
headlink|mainpage|mainpage|/public/index.php|||true
head|pipe|pipe|
headlink|public_register|register|/public/participant_create.php|||||true
head|pipe|pipe||||||true
headlink|my_data|my_data|/public/participant_edit.php|||true|true
head|pipe|pipe||||true|true
headlink|my_registrations|my_registrations|/public/participant_show.php|||true|true
head|pipe|pipe||||true|true
headlink|calendar|calendar|/public/show_calendar.php|||true|true
head|pipe|pipe||||true|true
headlink|rules|rules|/public/rules.php|||true
head|pipe|pipe|
headlink|privacy|privacy_policy|/public/privacy.php|||true
head|pipe|pipe|
headlink|faqs|faqs|/public/faq.php|||true
head|pipe|pipe|
headlink|contact|contact|/public/contact.php|||true


//headlink|internet_exp|internet_experiments|/public/ie.php|||true||true
//head|||
//link|impressum|impressum|/public/impressum.php|||true



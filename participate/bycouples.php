<?php

// $bchelper = new ByCouplesHelper();

// if(!class_exists("ByCouplesHelper")) {
class ByCouplesHelper
{
	public $criteria="";
	public $tablename="";
	public $alreadyQuest=false;
	public $quota=false;
	public $quotaBySession=false;
	public $mailtablefield="external_code";
	public $mailuserfield="partnersmail";
	public $restricttotype=[];
	public $just_registered=false;
	private $by_couples_link="";
	private $message="";
	private $eligibility_code=0;
	
	function __construct() {
		global $experiment_id;
		$by_couples_link=experiment__get_value($experiment_id,'by_couples_link');
		if(empty($by_couples_link)) $by_couples_link=$GLOBALS['settings__public_folder'];
		$by_couples_exptype=experiment__get_value($experiment_id,'by_couples_exptype');
		if(!empty($by_couples_exptype)) {$temp=explode(",",$by_couples_exptype); $restricttype=array_map(fn($value): string => trim($value),$temp); $this->restricttotype=array_values(array_intersect($restricttype,load_external_experiment_types()));} // if(!empty($restricttype,load_external_experiment_types())) $this->restricttotype=$restricttype;
		$this->by_couples_link=$GLOBALS['settings__root_url'].'/'.$by_couples_link;
		$this->tablename=table('participate_at');
		if(!empty($_REQUEST[$this->mailuserfield])) $this->alreadyQuest=true;
		//var_dump($this->alreadyQuest);
	}

	
	public function doneCorrectly()
	{
		return true;
	}
	
	public function emailForm($no_warning=true,$only_warning=false)
	{
		$supmessage="";
		$newcouplesallowed=$this->countCouples(2);
		if (!$newcouplesallowed) {
			if($newcouplesallowed === null) {
				$supmessage="<span style='color:red'>".lang("no_more_places_in_this_session")."</span>";
			}
			else {
				$supmessage="<span style='color:blue'>".lang("places_in_this_session_avalable_for_partners_of_already_registered_persons_only").". </span>";
			}
		}
		$result=$supmessage.'
		  <div class="container" style="max-width:600px">
			<label for="exampleInputEmail1" class="form-label">'.lang("please_enter_your_partner's_mail_used_to_register_in_our_database");
		if(!$no_warning && !empty($this->restricttotype) && $this->just_registered) {
			$warning=lang('please_note_that your_partner_should_have_consented_to_participate_in_this_experiment');
			$warning.=' <strong>'.lang('as_you_have_juste_done', false).'</strong> '.lang('or',false);
			$warning.=' '.lang('by_subscribing_using_the_link_below', false);
			if($only_warning) return $warning;
			$result.='. '.$warning;
		}
		$result.='. </label>
			<input type="email" name="'.$this->mailuserfield.'" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
			<div id="emailHelp" class="form-text">'.lang('the_address_to_register_for_this_experiment').': <a target=_blank style="color:#777" href="'.$this->by_couples_link.'">'.$this->by_couples_link.'</a></div>
		  </div><br><br>
		';
		return $result;
	}
	
	public function getMessage()
	{
		$result=$this->message;
		//if($this->eligibility_code==0) {}
		return $result;
	}
	
	public function eligibleHtml()
	{
		$result="";
		if($this->eligibility_code==1) {
			$result='<script>
				$(document).ready(function() {
					$("input[name=reallyregister]").click();
					$("input[name=reallyregister]").attr("disabled",true);
					$("input[name=reallyregister]").val("'.lang('please_wait').'");
				});
			</script>';
		}
		return $result;
	}
	
	public function registerMeToRestrictedType()
	{
		if(empty($this->restricttotype)) return;
		global $participant_id;
		$me=rasee_db_load_array('participants',array('participant_id'=>$participant_id));
		$prevsubscriptions=explode(',',$me["subscriptions"]);
		$newparticipant=$me;
		$newparticipant["remarks"]=empty($me["remarks"])?"":$me["remarks"]; //remarks may be null, which (probably) provokes warning message in php 8 if the orsee_db_save_array function
		if(!in_array($this->restricttotype[0],$prevsubscriptions)) $prevsubscriptions[]=$this->restricttotype[0];
		$newparticipant["subscriptions"]=$prevsubscriptions;
		$done=orsee_db_save_array($newparticipant,"participants",$newparticipant['participant_id'],"participant_id");
		if($done) $this->just_registered=true;
	}
	
	public function eligible($force=false)
	{
		global $participant_id, $experiment_id, $session_id;
		if(!$force && !$this->alreadyQuest) $_REQUEST[$this->mailuserfield]=$this->registeredData();
		if(!empty($this->restricttotype) && !empty($_REQUEST['couples_consentment']) && mb_strtolower(trim($_REQUEST['couples_consentment'])) == 'yes_i_consent')  $this->registerMeToRestrictedType(); //var_dump($_REQUEST['couples_consentment']);
		if(empty($_REQUEST[$this->mailuserfield])) return false;
		$this->eligibility_code=1;
		$pform=participantform__define();
		$mailform=array();
		foreach($pform as $pf) if($pf['mysql_column_name']=='email') $mailform=$pf;
		if(!preg_match($mailform['perl_regexp'],trim($_REQUEST[$this->mailuserfield]))) {
			$this->message=lang($mailform['error_message_if_no_regexp_match_lang']);
			$this->eligibility_code=-9;
			return $this->eligibility_code;
		}
		$me=rasee_db_load_array('participants',array('participant_id'=>$participant_id));
		$query="SELECT * FROM ".table('participants')." WHERE email='".trim($_REQUEST[$this->mailuserfield])."' AND deleted='n' AND participant_id<>$participant_id";
		$query_init=$query;
		if(!empty($this->restricttotype)) {
			$query.=" AND (";
			$rts=[];
			foreach($this->restricttotype as $rt) $rts[]="subscriptions LIKE '%".$rt."%'";
			$query.=implode(" OR ",$rts);
			$query.=")";
		}
		$lines=orsee_query($query,"return_same");
		if(empty($lines)) {
			if(mb_strtolower(trim($_REQUEST[$this->mailuserfield]))==mb_strtolower(trim($me['email']))) {
				$this->message=lang('you_should_enter_the_email_of_your_partner_not_yours').'.';
				$this->eligibility_code=0;
				log__participant("cple_err".str_replace("-","_",$this->eligibility_code),$me['participant_id'],"experiment_id:".$experiment_id."\nsession_id:".$session_id);
				return $this->eligibility_code;
			}
			$this->message=lang("sorry_email_not_found").'. ';
			$use_by_couples_link=true;
			$by_couples_link_capitalise=true;
			if(!empty($this->restricttotype)) {
				$lines_query_init=orsee_query($query_init,"return_same");
				if(!empty($lines_query_init)) {
					$this->message.=lang('your_partner_should_have_consented_to_participate_in_this_experiment');
					$this->message.=' '.lang('after_clicking_of_the_invitation_link_for_this_experiment_recieved_by_mail',false);
					$use_by_couples_link=true;
					if($use_by_couples_link) {
						$this->message.=' '.lang('or otherwise',false).' ';
						$by_couples_link_capitalise=false;
					}
				}
			}
			if($use_by_couples_link) $this->message.=lang("your_partner_should_register_at",$by_couples_link_capitalise).' <a href="'.$this->by_couples_link.'">'.$this->by_couples_link.'</a>';
			$this->eligibility_code=-1;
			log__participant("cple_err".str_replace("-","_",$this->eligibility_code),$me['participant_id'],"experiment_id:".$experiment_id."\nsession_id:".$session_id);
			return $this->eligibility_code;
		}
		foreach($lines as $line) {
			$at_line=rasee_db_load_array('participate_at',array('experiment_id'=>$experiment_id,'participant_id'=>$line['participant_id']));
			if($at_line!==false) {
				if(!empty(trim($at_line[$this->mailtablefield])) && mb_strtolower(trim($at_line[$this->mailtablefield]))!=mb_strtolower(trim($me['email']))) {
					// var_dump(at_line[$this->mailtablefield],$me['email']); exit;
					$this->message=lang("impossible_to_register_for_this_experiment");
					$this->eligibility_code=-2;
				}
				elseif($at_line['session_id']!=0 && $at_line['session_id']!=$session_id) {
					if(sessions__get_session_end_time(array(),$at_line['session_id'])<time()) {
						$this->message=lang("impossible_to_register_for_this_experiment");
						$this->eligibility_code=-3;
					}
					else {
						$this->message=lang("your_partner_is_registered_to_another_session").'. '.lang("please_register_to_the_same_session_as_your_partner");
						$this->eligibility_code=-4;
					}
				}
				if($this->eligibility_code<1) {
					log__participant("cple_err".str_replace("-","_",$this->eligibility_code),$me['participant_id'],"experiment_id:".$experiment_id."\nsession_id:".$session_id);
					return $this->eligibility_code;
				}
			}
		}
		$newcouplesallowed=$this->countCouples(2);
		if (!$newcouplesallowed) {
			if($newcouplesallowed === null) {
				$this->message="<span style='color:red'>".lang("no_more_places_in_this_session")."</span>";
				$this->eligibility_code=-5;
				return $this->eligibility_code;
			}
			if(!in_array(trim($_REQUEST[$this->mailuserfield]),$this->countCouples(3))) {
				$this->message="<span style='color:brown'>".lang("places_in_this_session_avalable_for_partners_of_already_registered_persons_only").". ".lang("your_partner_is_not_registered_for_this_session")."</span>";
				$this->eligibility_code=-6;
				log__participant("cple_err".str_replace("-","_",$this->eligibility_code),$me['participant_id'],"experiment_id:".$experiment_id."\nsession_id:".$session_id);
				return $this->eligibility_code;
				
			}
		}
		return $this->eligibility_code;
	}
	
	public function save($eligible=null)
	{
		global $participant_id, $experiment_id;
		// var_dump($this->registeredData(),$eligible);
		if(!empty($this->registeredData())) return false;
		if ($eligible===null) $eligible=($this->eligible()>0);
		if ($eligible===false) return false;
		if ($this->eligibility_code<1 && $this->eligibility_code!=-4) return false;
		$query="UPDATE ".$this->tablename." SET ".$this->mailtablefield."='".$_REQUEST[$this->mailuserfield]."' WHERE participant_id='$participant_id' AND experiment_id='$experiment_id'";
		if(!mysqli_query($GLOBALS['mysqli'],$query)) {echo "$query<br>Error n�".mysqli_errno($GLOBALS['mysqli']).":".mysqli_error($GLOBALS['mysqli']); exit; return false;}
		// var_dump($query);
		return true;
	}
	
	public function registeredData($varname="")
	{
		global $participant_id, $experiment_id;
		if($varname=="") $varname=$this->mailtablefield;
		$mysqltable=$this->tablename;
		$query="SELECT * FROM $mysqltable WHERE participant_id='$participant_id' AND experiment_id='$experiment_id'";
		$cresult=mysqli_query($GLOBALS['mysqli'],$query); $cnlines=mysqli_num_rows($cresult);
		if($varname=="#n_lines") return $cnlines;
		if($cnlines==0) return false;
		$res="";
		while ($crow = mysqli_fetch_array($cresult, MYSQLI_ASSOC)) {$res=$crow[$varname]; }
		// var_dump($crow,$varname,$res,$query,$cresult,$cnlines);
		return $res;
	}
	
	public function countCouples($mode=0,$pid=0,$sid=-1,$eid=-1)
	{
		$session_id=($sid==-1)?$GLOBALS['session_id']:$sid;
		$experiment_id=($eid==-1)?$GLOBALS['experiment_id']:$eid;
		$mysqltable=$this->tablename;
		$query="SELECT * FROM $mysqltable as mt LEFT JOIN ".table('sessions')." as st ON mt.session_id=st.session_id WHERE mt.experiment_id='$experiment_id'";
		if(!empty($session_id) && $session_id>0) $query.="AND mt.session_id='$session_id'";
		else $query.="AND registered='y'";
		$query.="ORDER BY session_start_year,session_start_month,session_start_day,session_start_hour,session_start_minute DESC, participate_id ASC";
		$at_lines=orsee_query($query,"return_same");
		$pmails=array(); $mails=array(); $pids=array(); $pidmails=array(); $selfpidmails=array(); $pidsessid=array(); $couplemails_sessid=array();
		// var_dump($at_lines,$query);
		if($at_lines!==false) foreach($at_lines as $atline) {
			$pmails[]=mb_strtolower($atline[$this->mailtablefield]);
			$pids[]=$atline['participant_id'];
			$pidmails[$atline['participant_id']]=mb_strtolower($atline[$this->mailtablefield]);
			if(!empty($atline['session_id'])) $pidsessid[$atline['participant_id']]=$atline['session_id'];
		}
		if($mode==1) {
			if($pid==0) $pid=$GLOBALS['participant_id'];
			$me=rasee_db_load_array('participants',array('participant_id'=>$pid));
			return in_array(mb_strtolower($me['email']),$pmails);
		}
		$couplemails=array();
		foreach($pidmails as $cpid=>$mail) {
			$cme=rasee_db_load_array('participants',array('participant_id'=>$cpid));
			$mails[]=mb_strtolower(trim($cme['email']));
			$selfpidmails[$cme['participant_id']]=mb_strtolower(trim($cme['email']));
			if(!in_array(mb_strtolower(trim($cme['email'])).'+'.mb_strtolower(trim($mail)),$couplemails) && !in_array(mb_strtolower(trim($mail)).'+'.mb_strtolower(trim($cme['email'])),$couplemails)) {
				$ccm=mb_strtolower(trim($cme['email'])).'+'.mb_strtolower(trim($mail));
				if(!empty($pidsessid[$cpid])) $couplemails_sessid[$ccm]=$pidsessid[$cpid];
				$couplemails[]=$ccm;
			}
		}
		if($mode==0) return count($couplemails);
		if(!empty($session_id) && $session_id>0) {
			$thissession=orsee_db_load_array("sessions",$session_id,"session_id");
			$nplaces = $thissession['part_needed'] + $thissession['part_reserve'];
		}
		// var_dump("session_id:",$session_id);
		if($mode>=2 && $mode<=3) {
			// if($session_id>0) var_dump($pidmails,$couplemails,$nplaces,$session_id);
			if($mode==2 && (empty($session_id) || $session_id<=0)) return null;
			if($mode==2 && count($pids)>=$nplaces) return null;
			if($mode==2) return (count($couplemails) < $nplaces/2);
			// if($pid==0) $pid=$GLOBALS['participant_id'];
			return $mails;
		}
		if($mode==4) return $pmails;
		if($mode==5) return $couplemails;
		if($mode==6) return $pidmails;
		if($mode==7) return $selfpidmails;
		if($mode==8) return $couplemails_sessid;
		$assigned_pids=orsee_query("SELECT participant_id FROM $mysqltable WHERE experiment_id='$experiment_id'","return_first_elem");
		if($mode==9) return $assigned_pids;
		$registered_info_base=orsee_query("SELECT participant_id,registered,participated FROM $mysqltable WHERE experiment_id='$experiment_id' AND registered='y'","return_key_vals");
		$registered_info=[];
		foreach($registered_info_base as $rib) {
			// var_dump("new",$rib,$registered_info); echo "<hr>";
			$registered_info+=$rib;
		}
		// exit;
		if($mode==10) return $registered_info;
		$me=rasee_db_load_array('participants',array('participant_id'=>$pid));
		return array(
			count($couplemails),
			in_array(mb_strtolower($me['email']),$pmails),
			(empty($session_id) || $session_id<0 || count($pids)>=$nplaces)?null:(count($couplemails) < $nplaces/2),
			$mails,
			$pmails,
			$couplemails,
			$pidmails,
			$selfpidmails,
			$couplemails_sessid,
			$assigned_pids,
			$registered_info
		);
	}
	
	
	public function eligibilityDone()
	{
		$qr=$this->registeredData($this->mailtablefield);
		if($qr===false || $qr==="") return false;
		return $qr;
	}
	
	public function writeCouplesTable($sid=-1,$eid=-1) {
		$session_id=($sid==-1)?$GLOBALS['session_id']:$sid;
		$experiment_id=($eid==-1)?$GLOBALS['experiment_id']:$eid;
		$registered_mails=$this->countCouples(3,0,$session_id,$experiment_id);
		// var_dump($registered_mails);
		$couple_mails=$this->countCouples(5,0,$session_id,$experiment_id);
		$pidmails=$this->countCouples(6,0,0,$experiment_id);
		$selfpidmails=$this->countCouples(7,0,$session_id,$experiment_id);
		$couple_mails_session=$this->countCouples(8,0,$session_id,$experiment_id);
		$show_id_column=false;
		// var_dump($pidmails);
		echo '<center><strong>'.count($couple_mails).' '.lang('couple(s)', false).' : </strong></center><br>';
		echo '<table class="table ">';
		foreach($couple_mails as $cmk=>$cm) {
		echo '<tr><td>';
		echo $cmk+1;
		echo '. <em>'.$cm.'</em>';
		if(empty($session_id) && !empty($couple_mails_session[$cm])) {
			$tsession=orsee_db_load_array("sessions",$couple_mails_session[$cm],"session_id");
			echo '. <a href="'.thisdoc().'?'.$_SERVER['QUERY_STRING'].'&session_id='.$couple_mails_session[$cm].'">'.session__build_name($tsession).'</a>';
		}
		echo ' :<br>';
		echo '<table class="table table-light">';
		if($show_id_column) echo '<th>'.lang('id').'</th>';
		echo '<th>'.lang('lastname').'</th>'.'<th>'.lang('firstname').'</th>'.'<th>'.lang('email').'</th>'.'<th>'.lang("partner's_email").'</th>'.'<th>'.lang('registered').'</th>';
		$ccouple=explode('+',$cm);
		foreach($ccouple as $cpk=>$cpm) {
			$q="SELECT * FROM ".table('participants')." WHERE email='$cpm' AND deleted='n'";
			if(!empty($this->restricttotype)) {
				$q.=" AND (";
				$rts=[];
				foreach($this->restricttotype as $rt) $rts[]="subscriptions LIKE '%".$rt."%'";
				$q.=implode(" OR ",$rts);
				$q.=")";
			} //$q.=" AND subscriptions LIKE '%".$this->restricttotype."%'";
			// $q="SELECT * FROM ".table('participants')." as p,".table('participate_at')." as pat WHERE p.participant_id=pat.participant_id AND pat.experiment_id=$experiment_id AND pat.session_id=$session_id p.email='$cpm' AND p.deleted='n'";
			$clines=orsee_query($q,"return_same");
			if($clines===false) {echo lang('empty_result').' : <code>'.$q.'</code><br>'; }
			if($clines!==false) {
				foreach($clines as $cl) {
					$iamregistered=in_array(mb_strtolower($cl['email']),$registered_mails);
					$other_k=count($ccouple)-1-$cpk;
					$other_mail=$ccouple[$other_k];
					$other_pid=array_search($other_mail,$selfpidmails);
					$supstyle="";
					if(!empty($pidmails[$cl['participant_id']]) && $pidmails[$cl['participant_id']]!=$other_mail) $supstyle.="text-decoration:line-through";
					$subscriptions=explode(',',$cl["subscriptions"]);
					if(!in_array(experiment__get_value($experiment_id,'experiment_ext_type'),$subscriptions)
						|| (!empty($this->restricttotype) && !in_array($this->restricttotype[0],$subscriptions))
					) $supstyle.=";color:#AAA";
					if(!empty($supstyle)) $supstyle="style='$supstyle'";
					echo '<tr '.$supstyle.'>';
					if($show_id_column) echo '<td>'.$cl['participant_id'].'</td>';
					echo '<td>'.$cl['lname'].'</td>'.'<td>'.$cl['fname'].'</td>'.'<td>'.$cl['email'].'</td><td>';
					if(in_array($cl['participant_id'],array_keys($pidmails))) {
						
						$partner=rasee_db_load_array('participants',array('email'=>$pidmails[$cl['participant_id']]));
						$psupstyle="";
						if(!empty($partner)) {
							$psubscriptions=explode(',',$partner["subscriptions"]);
							if(!in_array(experiment__get_value($experiment_id,'experiment_ext_type'),$psubscriptions)
								|| (!empty($this->restricttotype) && !in_array($this->restricttotype[0],$psubscriptions))
							) $psupstyle.=";color:#AAA";
							else $psupstyle.=";color:black";
						}
						if(!empty($psupstyle)) $psupstyle="style='$psupstyle'";
						echo '<em '.$psupstyle.'>' . $pidmails[$cl['participant_id']] . '</em>';
					}
					else {
						echo "--";
					}
					echo '</td><td>';
					if($iamregistered) {
						echo lang('yes');
					}
					else {
						echo lang('no');
						if(!empty($session_id) && $session_id>0) echo ' (<a class="btn btn-danger" href="'.thisdoc().'?'.$_SERVER['QUERY_STRING'].'&bcsubscribe='.$cl['participant_id'].'&bcpartner='.$other_pid.'">'.lang('register').'</a>, <a target=_blank href="participants_edit.php?participant_id='.$cl['participant_id'].'">'.lang('edit').'</a> )';
					}
					echo '</td>';
					echo '</tr>';
				}
			}
		}
		echo '</table>';
		echo '</td></tr>';
		}
		echo '</table>';
	}
	
	public function subscribePartner($me,$partner_id,$sid=-1,$eid=-1,$admin=true) {
		$session_id=($sid==-1)?$GLOBALS['session_id']:$sid;
		$experiment_id=($eid==-1)?$GLOBALS['experiment_id']:$eid;
		$registered_mails=$this->countCouples(3,0,$session_id,$experiment_id);
		$selfpidmails=$this->countCouples(7,0,$session_id,$experiment_id);
		$pidmails=$this->countCouples(6,0,0,$experiment_id);
		$assigned_pids=$this->countCouples(9,0,$session_id,$experiment_id);
		if(in_array($me,array_keys($selfpidmails))) {
			if($admin) message(lang('already_registered'));
			return false;
		}
		$myfuturepartner=rasee_db_load_array('participants',array('participant_id'=>$partner_id));
		$mearr=rasee_db_load_array('participants',array('participant_id'=>$me));
		// var_dump($me,array_keys($pidmails));
		if(!in_array($me,$assigned_pids)) {
			$instring=$me;
			$query="INSERT INTO ".table('participate_at')." (participant_id,experiment_id,session_id,registered,".$this->mailtablefield.") 
							SELECT participant_id, '".$experiment_id."', '".$session_id."', 'y', '".$myfuturepartner['email']."'
							FROM ".table('participants')." 
				WHERE participant_id IN ('".$instring."') AND deleted='n'";
			if(!empty($this->restricttotype)) {
				$query.=" AND (";
				$rts=[];
				foreach($this->restricttotype as $rt) $rts[]="subscriptions LIKE '%".$rt."%'";
				$query.=implode(" OR ",$rts);
				$query.=")";
			} //$query.=" AND subscriptions LIKE '%".$this->restricttotype."%'";
			$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']) . "query: ".$query);
			$target="experiment:".$experiment['experiment_name'];
			if (!empty($session_id)) $target.="\nsession_id:".$session_id;
			if($admin) log__admin("couples_assign_external",$target);
		}
		else {
			$query="UPDATE ".table('participate_at')." SET session_id=$session_id, ".$this->mailtablefield."='".$myfuturepartner['email']."', registered='y' 
				WHERE experiment_id=$experiment_id AND participant_id=$me";
			
			$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']) . "query: ".$query);
			$target="experiment:".$experiment['experiment_name'];
			if (!empty($session_id)) $target.="\nsession_id:".$session_id;
			if($admin) log__admin("couples_assign_participants",$target);
		}
		if($admin) message($mearr['fname'].' '.$mearr['lname'].' '.lang('registered',false));
		return true;
		
	}
}
// }

?>
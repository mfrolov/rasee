<?php
ob_start();
$header=true;
if(!empty($_REQUEST['headers']) && $_REQUEST['headers']=="no") $header=false;
if($header) {
	include("header.php");
}
else {
	include("nonoutputheader.php");
	html__header();
	@include ("../style/".$settings['style']."/html_header_frame.php");
}
date_default_timezone_set('Europe/Paris');


$participant_id=empty($initial_cpid)?"":url_cr_decode($initial_cpid);
if(empty($initial_cpid) && !empty($_REQUEST['p'])) $participant_id=url_cr_decode($_REQUEST['p']);
$participant=array(); $langtousestudies=array(); $langtouseprofession=array();
$participant=empty($participant_id)?array():orsee_db_load_array("participants",$participant_id,"participant_id");
$langtouse=$lang['lang'];
if(empty($participant_id))
{
	$langtousestudies[$langtouse]=''; $langtouseprofession[$langtouse]='';
	$participant['gender']=''; $participant['begin_of_studies']=0; $participant['number_reg']=0; $participant['number_noshowup']=0;
}
else{
	$langtouse=$participant['language'];
	$participant=orsee_db_load_array("participants",$participant_id,"participant_id");
	$langtousestudies=orsee_db_load_array("lang",$participant['field_of_studies'],"content_name");
	$langtouseprofession=orsee_db_load_array("lang",$participant['profession'],"content_name");
}
$experiments=array();
$expe='';
if(!empty($_REQUEST['expe'])) {$expe=$_REQUEST['expe']; $experiments=orsee_db_load_full_array("experiments",$_REQUEST['expe'],"experiment_public_name");}
if(!empty($_REQUEST['experiment_id'])) {
	$expfromid=orsee_db_load_array("experiments",$_REQUEST['experiment_id'],"experiment_id");
	if($expfromid!==false && !empty($expfromid['experiment_id']) && ($expe=='' || $expfromid['experiment_public_name']!=$expe)) $experiments[]=$expfromid;
}
$exp_ids='';
$descriptions=array();
foreach($experiments as $k=>$exp)
{
	if($k>0) $exp_ids.=",";
	if(!empty($exp['suppl_exp_description']) && !in_array($exp['suppl_exp_description'],$descriptions)) $descriptions[]=$exp['suppl_exp_description'];
	$exp_ids.=$exp['experiment_id'];
}
$descr=implode("<br>",$descriptions);
if(count($experiments)==0 || count($descriptions)==0) {
	$descr=lang('no_description_for_this_experiment');
	if(count($experiments)==0) $descr=lang('no_experiment_found');
	if(empty($participant_id)) die($descr);
}


$logtable='descriptionlogtable';
$query="CREATE TABLE IF NOT EXISTS `$logtable` (
`participant_id` text NOT NULL,
`experiment_id` text NOT NULL,
`expe` text NOT NULL,
`date_time` datetime NOT NULL,
`gender` char(1) NOT NULL,
`field_of_studies` text NOT NULL,
`begin_of_studies` int(5) NOT NULL,
`profession` text NOT NULL,
`number_reg` int(5) NOT NULL,
`number_noshowup` int(5) NOT NULL,
`client_info` text NOT NULL,
`computer_name` text NOT NULL,
`ntrials` int(5) NOT NULL,
`ntrial_comp_expe` int(5) NOT NULL
)";
$ntrials=1;
$ntrial_comp_expe=1;
if(!mysqli_query($GLOBALS['mysqli'],$query)) {echo"$query<br>Error n°".mysqli_errno($GLOBALS['mysqli']).":".mysqli_error($GLOBALS['mysqli']);}
//echo "participantslangstudycode=".$participant['field_of_studies']; print_r($langtousestudies);
if(!empty($participant_id)) {
	$query="SELECT * FROM $logtable WHERE participant_id='$participant_id'";
	$cresult=mysqli_query($GLOBALS['mysqli'],$query); $ntrials+=mysqli_num_rows($cresult);
}
$query="SELECT * FROM $logtable WHERE expe='$expe' AND computer_name='".get_client_name()."'";
$cresult=mysqli_query($GLOBALS['mysqli'],$query); $ntrial_comp_expe+=mysqli_num_rows($cresult);

$query="INSERT INTO $logtable (participant_id, experiment_id, expe, date_time, gender, field_of_studies, begin_of_studies, profession, number_reg, number_noshowup, client_info, computer_name, ntrials, ntrial_comp_expe) VALUES ('$participant_id', '$exp_ids', '$expe', '".date("Y-m-d H:i:s")."', '".$participant['gender']."', '".$langtousestudies[$langtouse]."', '".$participant['begin_of_studies']."', '".$langtouseprofession[$langtouse]."', '".$participant['number_reg']."', '".$participant['number_noshowup']."', '".get_userinfo_line()."', '".get_client_name()."', '$ntrials', '$ntrial_comp_expe') ";
if(!mysqli_query($GLOBALS['mysqli'],$query)) {echo"$query<br>Error n°".mysqli_errno($GLOBALS['mysqli']).":".mysqli_error($GLOBALS['mysqli']);}

$link="";
if(!empty($participant_id))
{
	$reg_link=experimentmail__build_lab_registration_link($participant_id);
	$fsprs=110;
	$link="<p>".lang("if_you_are_still_ready_please_click_on_the_link")."</p><p align=center style='font-size:$fsprs%'><a href='$reg_link' style='font-size:$fsprs%'>".lang('CONTINUE')."</a></p>";
}
$todisplay="<table align=center style='text-align:justify'><tr><td style='font-weight:normal;font-size: 14px;'>".$descr.$link."</td></tr></table>";

echo $todisplay;

if($header) include("footer.php");
else @include ("../style/".$settings['style']."/html_footer_frame.php");
?>

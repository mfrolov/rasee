<?php
// part of orsee. see orsee.org
ob_start();
$menu__area="privacy";
include ("header.php");

echo '<BR><BR>
	<center>
	<h4>'; echo $lang['privacy_policy'].' '.lang('and_data_protection',false);
	echo '</h4>
		<BR>
		<TABLE width=70%><TR><TD>';
	if(empty($settings__stop_subscriptions) || $settings__stop_subscriptions == "n")
		echo content__get_content("privacy_policy");
	elseif(!empty(content__get_content("error_temporary_disabled_privacy")) )
		echo content__get_content("error_temporary_disabled_privacy");
	else
		echo content__get_content("privacy_policy");
	echo '
		</TD></TR></TABLE>

		</center>';

include ("footer.php");

?>

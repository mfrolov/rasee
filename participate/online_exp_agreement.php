<?php 
ob_start();
$menu__area="public_register";
$title="Exp�riences en ligne sans r�mun�ration";
$oo=(!empty($_REQUEST['oo']) || !empty($_REQUEST['onlineonly']) || !empty($_REQUEST['onlyonline']) || !empty($_REQUEST['textonly']) || !empty($_REQUEST['p']));
include ("header.php");

if (isset($_REQUEST['no']) && !empty($_REQUEST['oo'])) {redirect("../"); exit;}
if (isset($_REQUEST['no'])) redirect($GLOBALS['settings__public_folder']."/");
echo '<BR><BR>';

if (!(isset($_REQUEST['s'])) && !(isset($_REQUEST['r'])) && !(isset($_REQUEST['p'])) && !(isset($_REQUEST['textonly']))) {
	echo '<BR><BR>
		<center>';
	echo $lang['please_choose_subgroup'];
	echo '<BR><BR>
		';

function subpool__save_all_pool_ids($alist) {
global $all_pool_ids;
$all_pool_ids[]=$alist['subpool_id'];
}

$all_pool_ids=NULL;
$query="SELECT * FROM ".table('subpools')." WHERE subpool_id > 1 AND show_at_registration_page='y'
 		ORDER BY subpool_id";
orsee_query($query,"subpool__save_all_pool_ids");

if (count($all_pool_ids)==1) redirect ($GLOBALS['settings__public_folder']."/".thisdoc()."?s=".$all_pool_ids[0]);

if (count($all_pool_ids)==0) redirect ($GLOBALS['settings__public_folder']."/".thisdoc()."?s=1");

if (count($all_pool_ids)<=1 && $settings['subpool_default_registration_id']) redirect($GLOBALS['settings__public_folder']."/".thisdoc()."?s=".$settings['subpool_default_registration_id']);
elseif (count($all_pool_ids)==1 && !$settings['subpool_default_registration_id']) redirect($GLOBALS['settings__public_folder']."/".thisdoc()."?s=".$all_pool_ids[0]);
elseif (count($all_pool_ids)==0 && !$settings['subpool_default_registration_id']) redirect ($GLOBALS['settings__public_folder']."/".thisdoc()."?s=1");


////////////////////////////////////////
// show subpools
function subpool__show_subpool_list($alist) {
	global $lang;

        echo '<A HREF="'.thisdoc().'?s='.$alist['subpool_id'].'">';
        echo stripslashes($alist['description']);
        echo '</A>
              <BR><BR>';
}

$query="SELECT *, ".table('lang').".".$lang['lang']." AS description
	FROM ".table('subpools').", ".table('lang')." 
	WHERE subpool_id > 0
	AND ".table('subpools').".subpool_id=".table('lang').".content_name 
	AND ".table('lang').".content_type='subjectpool' 
	AND show_at_registration_page='y' ORDER BY subpool_id";

orsee_query($query,"subpool__show_subpool_list");

echo '</center>';
}
$action="participant_create.php";
if(!empty($_REQUEST['p'])) $action="participant_edit.php";
$presentation='L�inscription pour les exp�riences en ligne vous permettra d��tre inform� par mail d��tudes en ligne non r�mun�r�es. L�objectif de ces �tudes est d��tudier les comportements pendant la p�riode de confinement. Pour vous inscrire, il vous suffit de nous donner votre nom, pr�nom et mail dans le formulaire d�inscription. Une personne ne peut avoir qu�un seul compte sur le site du LEEP.
Les donn�es collect�es dans les exp�riences en ligne le sont de mani�re anonyme.';
if(!$oo) $presentation='<h2>Souhaitez-vous �galement participer � des exp�riences en ligne pendant l��pid�mie de COVID 19?</h2>
<p>L�inscription pour les exp�riences en ligne vous permettra d��tre inform� par mail d��tudes en ligne non r�mun�r�es. L�objectif de ces �tudes est d��tudier les comportements pendant la p�riode de confinement. Pour vous inscrire, il vous suffit dans le formulaire d�inscription au LEEP d�indiquer que vous souhaitez recevoir des informations sur les �tudes en ligne r�alis�es pendant la p�riode de confinement.
</p><p>A tout moment, vous avez la possibilit� de ne plus recevoir ces informations pour des �tudes en ligne en vous d�sabonnant sur votre page personnelle en envoyant un mail �
<span id="nojs_content1">l�adresse mail du laboratoire</span><script src="../../mailto.php?id=0"></script>.</p>';
	echo '<center>
	      <FORM action='.$action.'>';
	if(!empty($_REQUEST['s'])) echo ' <INPUT type=hidden name=s value="'.$_REQUEST['s'].'">';
	if(!empty($_REQUEST['p'])) echo ' <INPUT type=hidden name=p value="'.$_REQUEST['p'].'">';
	if($oo) echo ' <INPUT type=hidden name=oo value="1">';
	echo '
	      	<TABLE width=70%>
			
			<tr>
				<td>
					<table width=100% border=1>
					<tr><td>
						<table>
							<TR><TD align="center" bgcolor="'.$color['list_title_background'].'">'.$presentation.'</TD></TR>
						</table>
					</td></tr>
					</table>
					<br><br>
				</td>
			</tr>	
			
		<TR><TD bgcolor="'.$color['list_title_background'].'">'.$lang['privacy_policy'].' '.lang('and_data_protection',false).'</TD></TR>
		<TR><TD>';
if($oo) echo '<p>La base de donn�es de participants</p>
<ul><li>Les donn�es collect�es � travers les inscriptions sont uniquement utilis�es dans le but d�informer les participants sur une nouvelle �tude en ligne r�alis�e pendant la p�riode de confinement.
</li><br><li>La base l�gale du traitement (base de donn�es de participants) est l�ex�cution d�une mission d�int�r�t public.
</li><br><li>Aucun transfert de donn�es n�est r�alis�. Elles ne sont pas divulgu�es � d�autres institutions ou individus. Elles ne sont pas transf�r�es aux chercheurs qui m�nent les exp�riences.
</li><br><li>A tout moment, vous avez la possibilit� de d�sactiver votre compte pour ne plus recevoir d�invitation aux exp�riences. Pour solliciter une d�sactivation et/ou une r�activation, envoyez un mail � un mail � <span id="nojs_content1">l�adresse mail du laboratoire</span><script src="../../mailto.php?id=0"></script>
</li><br><li>� tout moment, vous avez la possibilit� de vous d�sabonner d�finitivement de notre liste d�envoi afin de ne plus recevoir d�invitation aux exp�riences. Pour solliciter votre d�sabonnement, envoyez un mail � un mail � <span id="nojs_content2">l�adresse mail du laboratoire</span><script src="../../mailto.php?id=0"></script>. Il est interdit de se r�inscrire apr�s un d�sabonnement.
</li><br><li>Vous disposez des droits suivants pour l�utilisation qui est faite de vos donn�es :<ul>
<li>Le droit d�opposition : vous pouvez � tout moment vous opposer au traitement de vos donn�es et disposez du droit de retirer votre consentement
</li><br><li>Le droit d�acc�s et de rectification de vos donn�es
</li><br><li>Le droit d�effacement
</li><br><li>Le droit � une utilisation restreinte lorsque vos donn�es ne sont pas n�cessaires ou ne sont plus utiles
</li><br><li>Le droit � la portabilit� : communiquer vos donn�es � la personne de votre choix
</li><br></ul></li><br><li>Vous pouvez exercer ces droits en vous adressant au responsable du LEEP Mr <span id="nojs_content3">Jean-Christophe Vergnaud</span><script src="../../mailto.php?id=1"></script>, chercheur au Centre d�Economie de la Sorbonne.</li><br><BR>

<li>Vous pouvez contacter �galement votre DPD � l�adresse suivante :
<ul><li>DPD � 17 rue Notre Dame des Pauvres � 54519 � Vandoeuvre l�s Nancy Cedex - <span id="nojs_content4">dpd.demandes _at_ cnrs.fr</span><script src="../../mailto.php?id=2"></script></li><br></ul></li><br>
</ul></li><br><li>Si vous estimez que vos droits Informatique et Libert�s ne sont pas respect�s, vous avez la possibilit� d�introduire une r�clamation en ligne aupr�s de la CNIL ou par courrier postal.
</li><br></ul><br>';
else echo 'En cas d�inscription pour les exp�riences en ligne, la protection de vos donn�es sur la base du LEEP ne change pas. Les donn�es collect�es dans les exp�riences en ligne sont anonymes.<br><br>';

$agreementtext=$lang['do_you_agree_privacy'];
if(!$oo) $agreementtext='Je reconnais avoir pris connaissance des <a target=_blank href="'.thisdoc().'?textonly=1">conditions d�inscription et de la d�claration de confidentialit� pour les exp�riences en ligne</a> et je souhaite m�inscrire pour celles-ci';
if(empty($_REQUEST['textonly'])) { echo '</TD></TR>
		<TR><TD bgcolor="'.$color['list_title_background'].'">'.$agreementtext.'</TD></TR>
		<TR><TD align=center>';
		if(!isset($_REQUEST['dr']) && empty($_REQUEST['p'])) {
			echo '<INPUT type=submit name=dr value="'.$lang['yes'].'">';
			if(!isset($_REQUEST['onlinedr'])) echo '<INPUT type=hidden name=onlinedr value="1">';
		}
		else {
			echo '<INPUT type=submit name=onlinedr value="'.$lang['yes'].'">';
			if(isset($_REQUEST['dr'])) echo '<INPUT type=hidden name=dr value="'.$_REQUEST['dr'].'">';
		}
		echo '&nbsp;&nbsp;&nbsp;';
		if($oo && empty($_REQUEST['p'])) echo '<INPUT type=submit name=no value="'.$lang['no'].'">';
		else echo '<INPUT type=submit name=otherthanlab value="'.$lang['no'].'">';
}
	echo '</TD></TR>
		</TABLE>
		</FORM>
		</center>';
		
include("footer.php");
?>
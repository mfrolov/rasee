<?php
$quests=array(); $criteria="";
if(experiment__get_public_name($experiment_id)=="example_weight") {
	$quests=array("age"=>"1. Quel �ge avez-vous ?|int_99_18|ans","facebook"=>"2. Utilisez-vous les r�seaux sociaux (Facebook, Twitter, Instagram, Snapchat�) ?|radio|Oui:Non","internetBuyer"=>"3. Faites-vous r�guli�rement des achats sur Internet ?|radio|Oui:Non","amazon"=>"4. Accepteriez �vous de participer � une exp�rience si une partie de votre r�mun�ration se fera sous forme de bons d�achats pour le site Amazon ?|radio|Oui:Non","paypal"=>"5. Accepteriez �vous de participer � une exp�rience si une partie de votre r�mun�ration se fera par Paypal ?|radio|Oui:Non","association"=>"6. Faites-vous partie d�une association ?|radio|Oui:Non","internetConsumer"=>"7. �tes-vous un consommateur de biens culturels sur internet ? (Cin�ma, S�ries, Musique�)|radio|Oui:Non","culutre"=>"8. �tes-vous all� voir r�cemment une repr�sentation culturelle (th��tre, cin�ma, concert�)|radio|Oui:Non","health"=>"9. Comment jugez-vous votre �tat de sant� g�n�ral ?|radio|tr�s bon:bon satisfaisant:assez mauvais:tr�s mauvais","weight"=>"12. Quelle est votre poids ?|int_250_10|kg","height"=>"13. Quelle est votre taille ?|int_250_50|cm");
	$criteria='(#amazon#=="Oui" || #paypal#=="Oui") && #age#<=100 && ($imc_index=#weight#/((#height#/100,2)*(#height#/100,2)))>=0 && $imc_index<=100';
}
if(experiment__get_public_name($experiment_id)=="example_eeg") {
	$quests=array("age"=>"1. Quel �ge avez-vous ?|int_99_18|ans","study_level"=>"2. Quel est le plus haut niveau d'�tudes que vous ayez atteint ?|radio_values|Moins que le Bac:Bac:Bac+1:Bac+2:Bac+3:Bac+4:Bac+5 et plus","cheveux"=>"3. Avez-vous des cheveux cr�pus, fris�s ou un cr�ne chauve ?|radio|Oui:Non","gd"=>"4. Etes-vous ?|radio|Gaucher:Droitier","epilepsie"=>"5. Avez-vous des ant�c�dents d��pilepsie ?|radio|Oui:Non","maladie"=>"6. Avez-vous une maladie neurologique ou psychiatrique chronique ?|radio|Oui:Non");
	$criteria='#age#>18 && #age#<100 && #study_level#<=6 && #cheveux#=="Non" && #gd#=="Droitier" && #epilepsie#=="Non" && #maladie#=="Non"';
}

if(experiment__get_public_name($experiment_id)=="example_quota") {
	$quests=array("q1"=>"Pr�f�rez-vous gauche ou droite ?|radio|gauche:droite:milieu","q2"=>"Pr�f�rez-vous haut ou bas ?|radio_values|haut:bas");
	$criteria='quota:q2=#q2#:1x120,2x30';//2x50
	// $criteria='quota:session:q2=#q2#:1x14,2x7';
}

if(!empty(experiment__get_value($experiment_id,"supquest_qc"))) { //for brackets in text use &#40; &#41;, dont use functions in criteria and questions.
	$qcbase=experiment__get_value($experiment_id,"supquest_qc");
	if(preg_match('/\w+\s*[\(]/',$qcbase) ) exit(lang('unautorized_syntax_error_please_report_to').' '. $settings['support_mail'].'.');
	$qclines=preg_split('/((\r)?\n)+/',trim($qcbase)); 
	// var_dump($qclines);
	if(count($qclines)!=2) exit(lang('syntax_error_please_report_to').' '. $settings['support_mail'].'.');
	eval('$quests=['.$qclines[0].'];');
	$criteria=$qclines[1];
}
$questionnaire= new Questionnaire($quests,$criteria);


class Questionnaire
{
	public $criteria="";
	public $tablename="";
	public $alreadyQuest=false;
	public $quota=false;
	public $quotaBySession=false;
	private $questions=array();
	private $imc_index="";
	
	function __construct($quests, $criteria="") {
		global $experiment_id;
		$supquesttable_arr=explode(" ",experiment__get_public_name($experiment_id));
		$this->tablename="supquest_".$supquesttable_arr[0];
		$this->questions=$quests;
		$this->criteria=$criteria;
		$quotadeb="quota:";
		if(substr($criteria,0,strlen($quotadeb))==$quotadeb) {
			$this->criteria=substr($criteria,strlen($quotadeb));
			$sessdeb="session:";
			if(substr($this->criteria,0,strlen($sessdeb))==$sessdeb) {
				$this->criteria=substr($this->criteria,strlen($sessdeb));
				$this->quotaBySession=true;
			}
			$this->quota=true;
		}
		if(!empty($_REQUEST["questionnaire"])) $this->alreadyQuest=true;
		//var_dump($this->alreadyQuest);
	}

    public function __set($name, $value)
    {
        $this->questions[$name] = $value;
    }
	
    public function __get($name)
    {
        // echo "Getting '$name'\n";
        if (array_key_exists($name, $this->questions)) {
            return $this->questions[$name];
        }
		
		// if($name=="alreadyQuestDone") return $this->alreadyQuest;
		
		return false;

        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $name .
            ' in ' . $trace[0]['file'] .
            ' on line ' . $trace[0]['line'],
            E_USER_NOTICE);
        return null;
    }

    /**  As of PHP 5.1.0  */
    public function __isset($name)
    {
        // echo "Is '$name' set?\n";
        return isset($this->questions[$name]);
    }

    /**  As of PHP 5.1.0  */
    public function __unset($name)
    {
        // echo "Unsetting '$name'\n";
        unset($this->questions[$name]);
    }
	
	public function doneCorrectly()
	{
		$ok=true;
		foreach($this->questions as $var=>$q)
		{
			$quest=explode("|",$q);
			$question=new Question($var,$quest);
			if($question->error()) $ok=false;
			if(!$ok) break;
		}
		return $ok;
	}
	
	public function makeQuestions()
	{
		$result="";
		if($this->alreadyQuest && !$this->doneCorrectly()) $result.="<table cellpadding=5 align=center width=80% style='text-align:left'><tr><td><span style='color:brown'>Vous n'avez pas r�pondu � toutes les questions ou bien certaines de vos r�ponses ne sont pas correctes :</span></td></tr></table>";
		$result.="<table cellpadding=5 align=center width=80% style='text-align:left'>";
		foreach($this->questions as $var=>$q)
		{
			$quest=explode("|",$q);
			$question=new Question($var,$quest);
			$result.="<tr><td style='font-weigh:bold'>";
			$result.=$question->qu;
			if($this->alreadyQuest && $question->error())
			{
				$result.="<p style='color:red'>".$question->error(true)." :</p>";
			}
			$result.="</td></tr>";
			$result.="<tr><td style=''>".$question->makeInput()."</td></tr>";
			if($question->type!="empty") $result.="<tr><td style=''>&nbsp;</td></tr>";
		}
		$result.="</table>";
		$result.="<input type=hidden name=questionnaire id=questionnaire value=1 >";
		return $result;
	}
	
	public function eligible($force=false)
	{
		if(!$force && (!$this->doneCorrectly() || $this->criteria=="")) return false;
		$tocheck=$this->criteria;
		foreach($this->questions as $var=>$q)
		{
			$quest=explode("|",$q);
			$question=new Question($var,$quest);
			if($force && empty($question->value)) $question->value=$this->registeredData($var);
			$supguim='"';
			if(is_numeric($question->value)) $supguim='';
			$tocheck=str_replace('#'.$var.'#',$supguim.$question->value.$supguim,$tocheck);
		}
		// echo "<hr>tocheck='$tocheck'<hr>";
		if(!$this->quota)
		{
			eval('$eligible=('.$tocheck.');');
			if(isset($imc_index)) $this->imc_index=$imc_index;
		}
		else
		{
			$eligible=true;
			$criterias=explode(";",$tocheck);
			foreach($criterias as $cr)
			{
				$acr=explode(":",$cr);
				$varbundle=explode("=",str_replace('"','',$acr[0]));
				$varname=$varbundle[0]; $varvalue=$varbundle[1];
				$forquotas=explode(",",$acr[1]);
				$quotas=array();
				foreach($forquotas as $fq)
				{
					$afq=explode("x",$fq);
					$quotas[$afq[0]]=$afq[1];
				}
				$n_current=$this->countQuota($varname,$varvalue);
				if($n_current+1>$quotas[$varvalue]) $eligible=false;
				// var_dump($quotas);var_dump($varname);var_dump($varvalue);var_dump($quotas[$varvalue]);var_dump($n_current);
			}
		}
		// var_dump($eligible);
		// var_dump($imc_index);
		$res=($eligible)?1:0;
		return $res;
	}
	
	public function save($eligible=false)
	{
		if($this->registeredData("#n_lines")>0) return false;
		global $participant,$session,$participant_id,$experiment_id,$session_id;
		$this->createMysqlTable();
		$mysqltable=$this->tablename;
		$session_start_minute=$session['session_start_minute']; if(strlen($session_start_minute)==1) $session_start_minute="0".$session_start_minute;
		$session_start_month=$session['session_start_month']; if(strlen($session_start_month)==1) $session_start_month="0".$session_start_month;
		$session_date_time=$session['session_start_year']."-".$session_start_month."-".$session['session_start_day']." ".$session['session_start_hour'].":".$session_start_minute;
		$langstudies=orsee_db_load_array("lang",$participant['field_of_studies'],"content_name");
		$langprofession=orsee_db_load_array("lang",$participant['profession'],"content_name");
		//echo "participantslangstudycode=".$participant['field_of_studies']; print_r($langstudies);
		$supvarnames=""; $supvalues="";
		foreach($this->questions as $var=>$q)
		{
			$quest=explode("|",$q);
			$question=new Question($var,$quest);
			$supvarnames.=", `$var`";
			$supvalues.=", '".$question->value."'";
		}
		if($this->imc_index!="") {
			$checkquery="SELECT `imc_index` FROM $mysqltable";
			$query="ALTER TABLE $mysqltable ADD `imc_index`  float(10, 6)";
			if(!mysqli_query($GLOBALS['mysqli'],$checkquery)) if(!mysqli_query($GLOBALS['mysqli'],$query)) {echo "$query<br>Error n�".mysqli_errno($GLOBALS['mysqli']).":".mysqli_error($GLOBALS['mysqli']); return false;}
			$supvarnames.=", `imc_index`";
			$supvalues.=", '".$this->imc_index."'";
		}
		if ($eligible===false) $eligible=$this->eligible();
		if ($eligible===false) return false;
		$query="INSERT INTO $mysqltable (participant_id, nom, prenom, experiment_id, session_id, session_date_time, date_time, gender, field_of_studies, begin_of_studies, profession, number_reg, number_noshowup, eligible".$supvarnames.") VALUES ('$participant_id', '".$participant['lname']."', '".$participant['fname']."', '$experiment_id', '$session_id', '$session_date_time', '".date("Y-m-d H:i:s")."', '".$participant['gender']."', '".str_replace("'"," ",$langstudies['fr'])."', '".$participant['begin_of_studies']."', '".str_replace("'"," ",$langprofession['fr'])."', '".$participant['number_reg']."', '".$participant['number_noshowup']."', '$eligible'".$supvalues.") ";
		if(!mysqli_query($GLOBALS['mysqli'],$query)) {echo "$query<br>Error n�".mysqli_errno($GLOBALS['mysqli']).":".mysqli_error($GLOBALS['mysqli']); return false;}
		return true;
	}
	
	public function registeredData($varname="")
	{
		global $participant_id;
		$this->createMysqlTable();
		$mysqltable=$this->tablename;
		$query="SELECT * FROM $mysqltable WHERE participant_id='$participant_id'";
		$cresult=mysqli_query($GLOBALS['mysqli'],$query); $cnlines=mysqli_num_rows($cresult);
		if($varname=="#n_lines") return $cnlines;
		if($cnlines==0) return false;
		$res="";
		while ($crow = mysqli_fetch_array($cresult, MYSQLI_ASSOC)) $res=$crow[$varname];
		return $res;
	}
	
	public function countQuota($varname,$varvalue)
	{
		global $participant_id,$experiment_id,$session_id;
		$this->createMysqlTable();
		$mysqltable=$this->tablename;
		$query="SELECT $varname from $mysqltable as qt join ".table('sessions')." as st on (qt.experiment_id=st.experiment_id and qt.session_id=st.session_id) join ".table('participate_at')." as pt on (qt.participant_id=pt.participant_id and qt.experiment_id=pt.experiment_id) where qt.experiment_id=$experiment_id and ((st.session_finished='y' and pt.participated='y') or (st.session_finished='n' and pt.registered='y')) and qt.$varname='$varvalue' ";
		if($this->quotaBySession) $query="SELECT $varname from $mysqltable as qt join ".table('sessions')." as st on (qt.experiment_id=st.experiment_id and qt.session_id=st.session_id) join ".table('participate_at')." as pt on (qt.participant_id=pt.participant_id and qt.experiment_id=pt.experiment_id and qt.session_id=pt.session_id) where qt.session_id=$session_id and ((st.session_finished='y' and pt.participated='y') or (st.session_finished='n' and pt.registered='y')) and qt.$varname='$varvalue' ";
		$cresult=mysqli_query($GLOBALS['mysqli'],$query); $cnlines=mysqli_num_rows($cresult);
		return $cnlines;
	}
	
	public function eligibilityDone()
	{
		$qr=$this->registeredData("eligible");
		if($qr===false || $qr==="") return false;
		if($this->quota) 
		{
			$qr=$this->eligible(true);
			if($qr===false || $qr==="") return false;
		}
		return $qr;
	}
	
	private function createMysqlTable()
	{
		$mysqltable=$this->tablename;
		$query="CREATE TABLE IF NOT EXISTS `$mysqltable` (
		`participant_id` text,
		`nom` text,
		`prenom` text,
		`experiment_id` text,
		`session_id` text,
		`session_date_time` text,
		`date_time` datetime,
		`gender` char(1),
		`field_of_studies` text,
		`begin_of_studies` varchar(10),
		`profession` text,
		`number_reg` int(5),
		`number_noshowup` int(5),
		`eligible` smallint,
		`registered` smallint";
		foreach($this->questions as $var=>$q)
		{
			$quest=explode("|",$q);
			$question=new Question($var,$quest);
			$ctype="text";
			if($question->type=="int") $ctype="int(11)";
			$query.=",\n`$var` $ctype";
		}		
		$query.="\n)";
		if(!mysqli_query($GLOBALS['mysqli'],$query)) {echo"$query<br>Error n�".mysqli_errno($GLOBALS['mysqli']).":".mysqli_error($GLOBALS['mysqli']);}
		query_makecolumns($mysqltable, "experiment_id");
	}
	
	
	
}

class Question
{
	private $_quest=array();
	public $qu="";
	public $variable="";
	public $value="";
	public $types=array();
	public $type="empty";
	public $soustypes=array();
	public $soustype1="";
	public $soustype2="";
	public $minval=0;
	public $maxval=INF;
	public $options=" ";
	public $emptyMessage="Veuiller r�pondre � cette question";
	public $radiosep="<br>";//" &nbsp;&nbsp;&nbsp; ";
	
	
	function __construct($varname,$quest) {
		$this->variable=$varname;
		$this->_quest=$quest;
		if(isset($_REQUEST[$this->variable])) $this->value=trim($_REQUEST[$this->variable]);
		if(count($quest)>0) $this->qu=$quest[0];
		if(count($quest)>1) 
		{
			$types=explode("_",$quest[1]);
			$this->types=$types;
			$this->soustypes=$types;
			$this->type=strtolower(array_shift($this->soustypes));
			if(count($this->soustypes)>0) $this->soustype1=$this->soustypes[0];
			if(count($this->soustypes)>1) $this->soustype2=$this->soustypes[1];
		}
		if($this->type=="int")
		{
			if(count($this->soustypes)>0) $this->maxval=$this->soustypes[0];
			if(count($this->soustypes)>1) $this->minval=$this->soustypes[1];
			if($this->value!="" && is_numeric($this->value)) $this->value=round($this->value);
		}
		if(count($quest)>2) 
		{
			$this->options=$quest[2];
		}
	}
	
	public function error($returnMessage=false,$onlyIfVarExists=false)
	{
		if($this->type=="empty") return ($returnMessage)?"":false;
		if(!$onlyIfVarExists && !isset($_REQUEST[$this->variable])) return ($returnMessage)?$this->emptyMessage:true;
		if($onlyIfVarExists && !isset($_REQUEST[$this->variable])) return ($returnMessage)?"":false;
		$val=$_REQUEST[$this->variable];
		if($val=="") return ($returnMessage)?$this->emptyMessage:true;
		$message=""; $resp=false;
		if($this->type=="int")
		{
			if(!$resp && !is_numeric($val)) {$message="La r�ponse doit �tre num�rique"; $resp=true;}
			if(!$resp && $this->type=="int") $val=intval($val);
			if(!$resp && $val<$this->minval) {$message="La r�ponse doit �tre sup�rieure ou �gale � ".$this->minval; if($this->variable=="age" && $val<18) $message.=". Vous devez �tre majeur(e) pour participer aux exp�riences&nbsp;! "; $resp=true;}
			if(!$resp && $val>$this->maxval) {$message="La r�ponse doit �tre inf�rieure ou �gale � ".$this->maxval; $resp=true;}
			$message.=" (vous avez choisi \"".$_REQUEST[$this->variable]."\")";
		}
		return ($returnMessage)?$message:$resp;
	}
	
	public function makeInput()
	{
		$res="";
		$initval="";
		if(!$this->error())$initval=$this->value;
		if($this->type=="int")
		{
			$maxsize=($this->maxval==INF)?10:strlen($this->maxval);  $maxsizemun=($maxsize>1)?$maxsize-1:1; 
			$res="<INPUT type='text' ask=TEX  name='".$this->variable."'  id='".$this->variable."' size='".$maxsizemun."' maxlength='".$maxsize."'  value='".$initval."' > ".$this->options;
		}

		if($this->type=="radio")
		{
			$plist=explode(":",$this->options);
			foreach ($plist as $i=>$option)
			{
				$opval=$option;
				$n=$i+1;
				if($this->soustype1=="values") $opval=$n;
				$checked="";
				if($initval!="" && $initval==$opval) $checked="checked";
				$res.=$option." ";
				$res.="<INPUT type='radio' ask=TEX $checked name='".$this->variable."'  id='".$this->variable."".$n."'  value='".$opval."' > ";
				$res.=$this->radiosep;
			}
		}
		return $res;
	}
	
	public function makeQuery($table)
	{
		if($this->error()) return false;
		$val=$_REQUEST[$this->variable];
	}

}

?>
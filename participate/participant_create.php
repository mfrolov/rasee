<?php
// part of orsee. see orsee.org
ob_start();
$menu__area="public_register";
include ("header.php");

	$php_gt533=version_compare(phpversion(), '5.3.3', '>=');
	require_once '../jcryption/include/sqAES.php';
	require_once '../jcryption/include/JCryption.php';
	if(!empty($_POST) && !isset($_POST[JCryption::POST_KEY])) exit(lang("an_error_is_occured or_you_are_not_authorized please_contact_the_site_support"));
	if(!$php_gt533) include_once '../jcryption/include/Crypt/AES.php';
	$postBefore = $_POST;
	if(!empty($_POST)) {
		JCryption::decrypt();
		$jcdebug=false;
		$php_gt74=version_compare(phpversion(), '7.4.0', '>=');
		$encodingfile="../scripts/Encoding.php";
		if($jcdebug) {$postBeforeFix = $_POST;}
		if(file_exists($encodingfile) && $php_gt74) {
			include_once($encodingfile);
			$_POST=\ForceUTF8\Encoding::fixUTF8($_POST);
			$_REQUEST=\ForceUTF8\Encoding::fixUTF8($_REQUEST);
		}
		if($jcdebug) {
			var_dump($php_gt74);
			print_r($postBefore);
			print_r($postBeforeFix);
			print_r($_POST);
			exit;
		}
	}
	
	echo '
		<script type="text/javascript">
			var jCryptionLocation="../jcryption/";
		</script>
		<script type="text/javascript" src="../jcryption/jquery.jcryption.3.1.0.js"></script>
		<script type="text/javascript">
			window.only_get_form=false;
			$(function() {
				if(!window.only_get_form) $("form").jCryption();
			});
		</script>';
		
// session_start(); var_dump($_SESSION);
if (isset($_REQUEST['no']) && !empty($_REQUEST['oo'])) {redirect("../"); exit;}
if (isset($_REQUEST['no'])) redirect($GLOBALS['settings__public_folder']."/");

if(!empty($settings__stop_subscriptions) && $settings__stop_subscriptions != "n") {
	echo content__get_content("error_temporary_disabled");
	exit;
}
echo '<BR><BR>';

if (!(isset($_REQUEST['s'])) && !(isset($_REQUEST['r']))) {
	echo '<BR><BR>
		<center>';
	echo $lang['please_choose_subgroup'];
	echo '<BR><BR>
		';

function subpool__save_all_pool_ids($alist) {
global $all_pool_ids;
$all_pool_ids[]=$alist['subpool_id'];
}

$all_pool_ids=array(); //NULL
$query="SELECT * FROM ".table('subpools')." WHERE subpool_id > 1 AND show_at_registration_page='y'
 		ORDER BY subpool_id";
orsee_query($query,"subpool__save_all_pool_ids");

if (count($all_pool_ids)==1) redirect ($GLOBALS['settings__public_folder']."/".thisdoc()."?s=".$all_pool_ids[0]);

if (count($all_pool_ids)==0) redirect ($GLOBALS['settings__public_folder']."/".thisdoc()."?s=1");

if (count($all_pool_ids)<=1 && $settings['subpool_default_registration_id']) redirect($GLOBALS['settings__public_folder']."/".thisdoc()."?s=".$settings['subpool_default_registration_id']);
elseif (count($all_pool_ids)==1 && !$settings['subpool_default_registration_id']) redirect($GLOBALS['settings__public_folder']."/".thisdoc()."?s=".$all_pool_ids[0]);
elseif (count($all_pool_ids)==0 && !$settings['subpool_default_registration_id']) redirect ($GLOBALS['settings__public_folder']."/".thisdoc()."?s=1");


////////////////////////////////////////
// show subpools
function subpool__show_subpool_list($alist) {
	global $lang;

        echo '<A HREF="'.thisdoc().'?s='.$alist['subpool_id'].'">';
        echo stripslashes($alist['description']);
        echo '</A>
              <BR><BR>';
}

$query="SELECT *, ".table('lang').".".$lang['lang']." AS description
	FROM ".table('subpools').", ".table('lang')." 
	WHERE subpool_id > 0
	AND ".table('subpools').".subpool_id=".table('lang').".content_name 
	AND ".table('lang').".content_type='subjectpool' 
	AND show_at_registration_page='y' ORDER BY subpool_id";

orsee_query($query,"subpool__show_subpool_list");

echo '</center>';
}

if (isset($_REQUEST['s']) && !(isset($_REQUEST['dr']))) {
	$action=thisdoc();
	$q='SELECT * FROM '.table('subpools').' WHERE subpool_id="'.$_REQUEST['s'].'"';
	$qline=orsee_query($q);
	if(str_replace("online_","",$qline["experiment_types"])!=$qline["experiment_types"]) $action="online_exp_agreement.php";
	echo '<center>
	      <FORM action='.$action.'>
	      <INPUT type=hidden name=s value="'.$_REQUEST['s'].'">
	      	<TABLE width=75%>
			
			<tr>
				<td>
					<table width=100% border=1>
					<tr><td style="text-align:center">
						<table align=center>
							<TR><TD align="center" bgcolor="'.$color['list_title_background'].'">'.lang('please_carefully_read_the_participation_rules').'.<br>'.lang('you_need_to_accept_the_conditions_at_the_bottom_in_order_to_subscribe').'.</TD></TR>
						</table>
					</td></tr>
					</table>
					<br><br>
				</td>
			</tr>	
			
		<TR><TD bgcolor="'.$color['list_title_background'].'">'.$lang['rules'].'</TD></TR>
		<TR><TD>';
	echo content__get_content("rules");
	echo '</TD></TR>
		<TR><TD bgcolor="'.$color['list_title_background'].'">'.$lang['privacy_policy'].' '.lang('and_data_protection',false).'</TD></TR>
		<TR><TD>';
	echo content__get_content("privacy_policy");
	echo '</TD></TR>
		<TR><TD bgcolor="'.$color['list_title_background'].'">'.$lang['do_you_agree_privacy'].'</TD></TR>
		<TR><TD align=center>
		<INPUT type=submit name=dr value="'.$lang['yes'].'">&nbsp;&nbsp;&nbsp;
		<INPUT type=submit name=no value="'.$lang['no'].'">
		</TD></TR>
		</TABLE>
		</FORM>
		</center>';
	echo '
		<script type="text/javascript">
			window.only_get_form=true;
		</script>';
}



echo '<center>';

$form=true; $errors__dataform=array();

if (isset($_REQUEST['add'])) {
		// var_dump($_REQUEST); exit;


		if (!$_REQUEST['subpool_id']) $_REQUEST['subpool_id']=$settings['subpool_default_registration_id'];
        $subpool=orsee_db_load_array("subpools",$_REQUEST['subpool_id'],"subpool_id");
        $_REQUEST['subpool_id'] = $subpool['subpool_id']; 
        if (!$subpool['subpool_id']) {
			$subpool=orsee_db_load_array("subpools",$settings['subpool_default_registration_id'],"subpool_id");
			$_REQUEST['subpool_id'] = $subpool['subpool_id'];
		}
		
		if(!isset($postBefore[JCryption::POST_KEY]) || isset($_GET["email"])) exit(lang("an_error_is_occured or_you_are_not_authorized, please_contact_the_site_support"));


		$continue=true;
		
		// checks and errors
		foreach ($_REQUEST as $k=>$v) {
			if(!is_array($v)) $_REQUEST[$k]=trim($v);
		}
		$errors__dataform=participantform__check_fields($_REQUEST,false);		
        $error_count=count($errors__dataform);
        if ($error_count>0) $continue=false;
		
		if($continue) {
			$response=participantform__check_unique($_REQUEST,"create");
			if ($response['disable_form']) { $continue=false; $form=false; show_message(); }
			elseif($response['problem']) { $continue=false; }
			// var_dump($errors__dataform, $continue, $response); exit;
		}
		$securimage="";
		if(file_exists("../../securimage/securimage.php")) $securimage="../../securimage/securimage.php";
		if(file_exists("../securimage/securimage.php")) $securimage="../securimage/securimage.php";
		
		if($continue && !empty($securimage)) {
			$audiopath=str_replace("securimage.php","audio",$securimage);
			require_once $securimage;
			$securimage = new Securimage();
			$ask_captcha=false;
			query_makecolumns(table('participants_log'),"ipaddr","varchar(255)","''");
			$logs=orsee_db_load_full_array('participants_log',get_client_ip(),'ipaddr');
			$ctime=time();
			if($logs !== false)
				foreach($logs as $log) {
					$ctimediff=$ctime-$log['timestamp'];
					if($ctimediff<600) $ask_captcha=true;
				}
			// var_dump(get_client_ip(),$ask_captcha,$logs,$ctimediff); exit;
			if($ask_captcha) {
				echo "<h5>".lang('please_copy_the_image_code_to_continue')."</h5>";
				if(!empty($_REQUEST['rasee_captcha'])) {
					$_CURRENT_SESSION=$_SESSION;
					$_SESSION=$_DEFAULT_SESSION;
					if($securimage->check($_REQUEST['rasee_captcha'])) $ask_captcha=false;
					else echo "<h4>".lang('incorrect_code').', '.lang('please_retry',false)."</h4>";
					$_SESSION=$_CURRENT_SESSION;
				}
				if($ask_captcha) {
					$options = array();
					$options['input_name'] = 'rasee_captcha'; // change name of input element for form post
					$options['disable_flash_fallback'] = false; // allow flash fallback
					$options['input_text'] = lang("type_the_text").' : ';
					$options['show_text_input']=true;
					$options['show_audio_button']=file_exists($audiopath);

					$action=thisdoc();
					// var_dump($_REQUEST['rasee_captcha'],$securimage->check($_REQUEST['rasee_captcha']),$_SESSION,$_DEFAULT_SESSION);
					echo '<FORM method=POST action='.$action.'>';
					$formfields=participantform__load();
					foreach ($formfields as $f) { 
					if($f['subpools']=='all' | in_array($subpool['subpool_id'],explode(",",$f['subpools']))) {
						if ($f['type']=='invitations') {
							if (isset($_REQUEST[$f['mysql_column_name']]) && !is_array($_REQUEST[$f['mysql_column_name']])) $_REQUEST[$f['mysql_column_name']]=explode(",",$_REQUEST[$f['mysql_column_name']]);
						}
					}}
					foreach($_REQUEST as $rk=>$rv) {
						if(!is_array($rv)) echo '<INPUT type=hidden name="'.$rk.'" value="'.$rv.'">'.PHP_EOL;
						else foreach($rv as $rvk=>$rvv) echo '<INPUT type=hidden name="'.$rk.'['.$rvk.']" value="'.$rvv.'">'.PHP_EOL;
					}
					echo "<table><tr><td><div style='white-space:nowrap' id='captcha_container_1'>";
					echo Securimage::getCaptchaHtml($options);
					echo "</div></td></tr></table>\n";
					echo "
					<p>
					<br>
					<input type='submit' value='".lang('submit')."'>
					</p>
					</FORM>";	  
					exit;
				}
			}
		}

		
	if ($continue) {
        $participant=$_REQUEST;
		$participant['participant_id']=participant__create_participant_id();
		$participant['participant_id_crypt']=unix_crypt($participant['participant_id']);
		$participant['creation_time']=time();
		if (isset($_REQUEST['subpool_id']) && $_REQUEST['subpool_id']) 
				$participant['subpool_id']=$_REQUEST['subpool_id'];
			else  	$participant['subpool_id']=$settings['subpool_default_registration_id'];
		if (empty($participant['language'])) $participant['language']=$settings['public_standard_language'];
		$geschafft=orsee_db_save_array($participant,"participants_temp",$participant['participant_id'],"participant_id");
		// var_dump($participant); exit;
		$p=$participant['participant_id_crypt'];

	   if ($geschafft) {
		log__participant("subscribe",$participant['lname'].', '.$participant['fname'].' ('.$participant['email'].')',$participant['participant_id']);
		$form=false;
		experimentmail__confirmation_mail($participant['participant_id']);
		redirect ($GLOBALS['settings__public_folder']."/".thisdoc()."?r=t&p=".urlencode($p));
		}
	 	else {	
	    	echo $lang['database_error'].'<BR>';
	  	} 
	}
}


if (isset($_REQUEST['r']) && $_REQUEST['r']=="t") {
	$form=false;
	message($lang['successfully_registered']);
	redirect ($GLOBALS['settings__public_folder']."/fin.php");
	}


if (isset($_REQUEST['s']) && $_REQUEST['s'] && isset($_REQUEST['dr']) && $_REQUEST['dr']) {
	$_REQUEST['subpool_id']=$_REQUEST['s'];  
	
	$suptypes=""; //"construction"
	if(!empty($_REQUEST['onlylab']) || !empty($_REQUEST['labonly']) || !empty($_GET['otherthanlab'])) $suptypes="onlylab";
	if(!empty($_REQUEST['notlab']) || !empty($_REQUEST['nolab'])) $suptypes="nolab";
	if(!empty($_REQUEST['onlineonly']) || !empty($_REQUEST['oo'])) $suptypes="onlineonly";

	
	// var_dump($_REQUEST);

	if ($form) participant__show_form($_REQUEST,$lang['submit'],$lang['registration_form'],$errors__dataform,false,false,$suptypes);

	}

echo '</center>';

include("footer.php");

?>

<?php
// part of orsee. see orsee.org
ob_start();
$menu__area="faqs";

include("header.php");
if(!empty($settings__stop_subscriptions) && $settings__stop_subscriptions == "y" && !empty(content__get_content("error_temporary_disabled_faq"))) {
	echo content__get_content("error_temporary_disabled_faq");
	exit;
}

	echo '
		<BR><BR>
		<center>
			<h4>'.$lang['faq_long'].'</h4>
		<BR>
		<TABLE width=70%>
			<TR>
				<TD>
				</TD>
				<TD width=20% class="small">
					'.$lang['this_faq_answered_questions_of_xxx'].'
				</TD>
			</TR>';

     	$query="SELECT * FROM ".table('faqs').", ".table('lang')."
	     	WHERE ".table('lang').".content_name=".table('faqs').".faq_id
	     	AND ".table('lang').".content_type='faq_question'
	     	ORDER BY ".table('faqs').".evaluation DESC, ".table('lang').".".$lang['lang'];
    	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

	$shade=false;
	while ($line=mysqli_fetch_assoc($result)) {

		if ($shade) $shade=false; else $shade=true;
  		echo '<TR>
			<TD>
        			<A HREF="javascript:popupfaq(\'faq_show.php?id='.$line['faq_id'].'\')">';
  				if ($shade) echo '<FONT COLOR="#539EBD">'; else echo '<FONT color="#4A4A4A">';
  				echo stripslashes($line[$lang['lang']]);
  				echo '</FONT></A>
        		</TD>
			<TD>
  				'.$line['evaluation'].' '.$lang['persons'].'
			</TD>
		      </TR>';
		}
	echo '	</TABLE>
		</center>';

include ("footer.php");
?>

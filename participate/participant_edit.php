<?php
// part of orsee. see orsee.org
ob_start();

$menu__area="my_data";
include("header.php");
if(!empty($settings__stop_subscriptions) && $settings__stop_subscriptions != "n") {
	echo content__get_content("error_temporary_disabled");
	exit;
}
	$form=true;
	
	$errors__dataform=array();

	if (!empty($_REQUEST['resubscribe']) && $participant['deleted']=='y') {
		if(!empty($participant['can_undelete'])) {
				query_makecolumns(table('participants'),array("last_delete_time","last_delete_admin","can_undelete"),array("INT(20)","VARCHAR(20)","tinyint(1)"));
                $query="UPDATE ".table('participants')."
                        SET deleted='n', excluded='n', can_undelete=0
                        WHERE participant_id='".$participant['participant_id']."'";
                $result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
				if ($result) {
					log__participant("resubscribe",$participant['participant_id']);
					message (lang('account_reactivated'));
					redirect($GLOBALS['settings__public_folder']."/participant_edit.php?p=".url_cr_encode($participant['participant_id']));
				}
                else {
					message(lang('impossible_to_reactivate_your_account').". ".$lang['database_error']." ".lang('please_write_to')." ".support_mail_link());
					redirect($GLOBALS['settings__public_folder']."/");
				}
 		
		}
		else {
			message (lang('impossible_to_reactivate_your_account').". ".
                        $lang['if_you_have_questions_write_to']." ".support_mail_link());
			redirect($GLOBALS['settings__public_folder']."/");
		}
	}
	
	if (!empty($_REQUEST['subscribe']) && $participant['deleted']=='n') {
		$id=strtr($_REQUEST['subscribe'],'_','/');
		$id_nmatches=preg_match_all("/[.\/0-9A-Za-z]/",$id);
		// var_dump($id_nmatches,strlen($id),$id,urldecode($id),strlen(urldecode($id)),str_replace(' &#x2F;','/',$id));
		if($id_nmatches===false || $id_nmatches==0 || $id_nmatches!=strlen($id)) exit(lang('not_authorized'));
		$w=(strtolower(PHP_OS)=="linux")?" WHERE ENCRYPT(log_id,'su')='$id' ":" WHERE log_id='$id'";
		$q="SELECT * FROM ".table("participants_log").$w;
		$line=orsee_query($q);
		if($line === false) exit(lang("incorrect_or_expired_link"));
		// var_dump($line);
		if($line['id']!=$participant['participant_id']) {
			message(lang('incorrect_or_expired_link'));
			redirect($GLOBALS['settings__public_folder']."/");
		}
		$atarget=explode("|",$line['target']); $targets=array();
		$prevparticipant=orsee_db_load_array("participants",$participant['participant_id'],"participant_id");
		$newparticipant=$prevparticipant;
		$prevsubscriptions=explode(',',$prevparticipant["subscriptions"]);
		$newsubscriptions=[];
		$exp_type_names=load_external_experiment_type_names(true,false,true);
		foreach(explode(',',$atarget[0]) as $ct) {
			if(!in_array(trim($ct),$prevsubscriptions) && array_key_exists(trim($ct),$exp_type_names)) {
				$newsubscriptions[]=trim($ct);
				$prevsubscriptions[]=trim($ct);
			}
		}
		if(empty($newsubscriptions)) {
			message(lang('acrion_already_done'));
			redirect($GLOBALS['settings__public_folder']."/participant_edit.php?p=".url_cr_encode($participant['participant_id']));
		}
		$newparticipant["subscriptions"]=$prevsubscriptions;
		$prevremarks=empty($prevparticipant["remarks"])?"":$prevparticipant["remarks"];
		if(count($atarget)>1 && !empty($atarget[1]) && strpos($prevremarks,$atarget[1])===false) {
			$newremarks=empty($prevparticipant["remarks"])?$atarget[1]:$prevparticipant["remarks"].PHP_EOL.$atarget[1];
			$newparticipant["remarks"]=$newremarks;
		}
		$done=orsee_db_save_array($newparticipant,"participants",$newparticipant['participant_id'],"participant_id");
		$exp_type_names_selected=[];
		foreach($newsubscriptions as $etname) $exp_type_names_selected[]=$exp_type_names[$etname];
		// var_dump($newsubscriptions,$exp_type_names_selected,$atarget,$prevremarks,$newremarks,$newparticipant["remarks"]); exit;
		if ($done) {
			log__participant("add_subscriptions",$participant['participant_id'],implode(',',$newsubscriptions));
			message(lang('you_are_now_subscribed_to').' '.implode(', ',$exp_type_names_selected));
			redirect($GLOBALS['settings__public_folder']."/participant_edit.php?p=".url_cr_encode($participant['participant_id']));
		}
		else {
			message(lang('impossible_to_subscribe_to')." ".implode(', ',$exp_type_names_selected).'<hr>'.$lang['database_error']." ".lang('please_write_to')." ".support_mail_link());
			redirect($GLOBALS['settings__public_folder']."/");
		}
 		
	}
	
	if (isset($_REQUEST['add']) && $_REQUEST['add']) {
		// var_dump($_REQUEST); exit;
		$continue=true;
		$_REQUEST['participant_id']=$participant['participant_id'];
            
		// checks and errors
		foreach ($_REQUEST as $k=>$v) {
			if(!is_array($v)) $_REQUEST[$k]=trim($v);
		}
		$errors__dataform=participantform__check_fields($_REQUEST,false);		
        $error_count=count($errors__dataform);
        if ($error_count>0) {
			$continue=false;
			if($error_count==1 && in_array('subscriptions',$errors__dataform)) {
				$_SESSION['message_text']='';
				redirect($GLOBALS['settings__public_folder']."/participant_delete.php?p=".url_cr_encode($participant['participant_id']));
			}
		}

		$response=participantform__check_unique($_REQUEST,"edit",$_REQUEST['participant_id']);
		if($response['problem']) { $continue=false; }
        
        if ($continue) {
           	$participant=$_REQUEST;

			
			$prevparticipant=orsee_db_load_array("participants",$participant['participant_id'],"participant_id");
			$updatedkeys=array(); $updatedkeys1=array();
			$pform=participantform__define();
			$checkboxkeys=array();
			$warning=false;
			foreach($pform as $pf) if(!empty($pf['checkbox_type']) && $pf['checkbox_type']!='n') $checkboxkeys[]=$pf['mysql_column_name'];
			foreach($prevparticipant as $ppk=>$ppv) {
				if(isset($participant[$ppk])) {
					$comp1=is_array($participant[$ppk])?implode(",",$participant[$ppk]):$participant[$ppk];
					$comp2=is_array($ppv)?implode(",",$ppv):$ppv;
					if($comp1!=$comp2) {
						$updatedkeys[]=$ppk;
						$updatedkeys1[]=$ppk.'('.str_replace("'","’",$ppv).')';
					}
				}
				if(!isset($participant[$ppk])) {
					$keyinform=false;
					if(in_array($ppk,$checkboxkeys)) $participant[$ppk]=null;
				}
			}
			if(in_array('fname',$updatedkeys) || in_array('lname',$updatedkeys)) {
				message(lang('in_order_to_modify_your_first_or_last_name please_write_to').' '.$settings['support_mail']);
				if(in_array('fname',$updatedkeys)) { unset($participant['fname']); unset($updatedkeys[array_search('fname',$updatedkeys)]); }
				if(in_array('lname',$updatedkeys)) { unset($participant['lname']); unset($updatedkeys[array_search('lname',$updatedkeys)]); }
				$warning=true;
			}
			if(in_array('email',$updatedkeys)) {
				$list_sessions=expregister__list_registered_for($participant_id,"","exp_sess_only");
				$ok_modify_email=true;
				foreach($list_sessions as $csl) {
					$csess_id=$csl[1]; //$csess_starttime=sessions__get_session_time(array(),$csess_id); 
					$csess_endtime=sessions__get_session_end_time(array(),$csess_id);
					$csession=orsee_db_load_array("sessions",$csess_id,"session_id");
					if(time()<$csess_endtime && $csession['session_finished']!='y') {
						$ok_modify_email=false;
						break;
					}
				}
				if(!$ok_modify_email) {
					message(lang('you_cannot_modify_your_email_while_registered_to_a_non_finished_session').'. ');
					unset($participant['email']);
					unset($updatedkeys[array_search('email',$updatedkeys)]);
					$warning=true;
				}
			}

			$done=orsee_db_save_array($participant,"participants",$participant['participant_id'],"participant_id",false,true);

	   		if ($done) {
				if(!($warning && empty($updatedkeys))) {
					// var_dump($updatedkeys,$participant,$prevparticipant);exit;
					message($lang['changes_saved']);
					
					query_makecolumns(table('participants'),array("last_update_time","last_update_fileds"),array("int(20)","text"),array("0","null"));
					query_makecolumns(table('participants_temp'),array("last_update_time","last_update_fileds"),array("int(20)","text"),array("0","null"));
					$query="UPDATE ".table('participants')."
							SET last_update_time=UNIX_TIMESTAMP(), last_update_fileds='".implode(", ",$updatedkeys1)."' WHERE participant_id='".$participant['participant_id']."'";
					$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
					
					log__participant("edit",$participant['participant_id'],implode(",",$updatedkeys));
				}
				redirect($GLOBALS['settings__public_folder']."/participant_edit.php?p=".url_cr_encode($participant['participant_id']));
			} else {
				message($lang['database_error']); //var_dump($done); exit;
             	redirect ($GLOBALS['settings__public_folder']."/participant_edit.php?p=".url_cr_encode($participant['participant_id']));
	  		} 
		}
	} else {
    	$_REQUEST=$participant;
	}


// form

	if ($form) {
		

		$suptypes="";
		// $suptypes="construction";
		if(!empty($_GET['onlylab']) || !empty($_GET['labonly']) || !empty($_GET['otherthanlab'])) $suptypes="onlylab";
		// var_dump($suptypes); var_dump($_REQUEST);var_dump($_GET);
		if(!empty($_GET['onlinedr']) && $suptypes!="onlylab") {
			$likelines=orsee_db_load_full_array_like('experiment_types','internet','exptype_mapping');
			if(count($likelines)==1) {
				$subpool_exptypes=explode(",",rasee_db_load_value("subpools",$participant['subpool_id'],"subpool_id","experiment_types"));
				$participant_subscriptions=explode(",",$participant["subscriptions"]);
				if(in_array($likelines[0]["exptype_name"],$subpool_exptypes) && !in_array($likelines[0]["exptype_name"],$participant_subscriptions)) {
					$participant_subscriptions[]=$likelines[0]["exptype_name"];
					orsee_db_save_array(array("subscriptions"=>implode(",",$participant_subscriptions)),"participants",$participant['participant_id'],"participant_id");
					$q="SELECT * FROM ".table('lang')." as tlang WHERE tlang.content_name='".$likelines[0]["exptype_id"]."' AND tlang.content_type='experiment_type'";
					$qline=orsee_query($q);
					$forwhat=lang('online_experiments');
					if(!empty($qline[$lang['lang']])) $forwhat=strip_tags($qline[$lang['lang']]);
					message (lang('you_have_registered_for').' '.$forwhat);
					redirect($GLOBALS['settings__public_folder']."/participant_edit.php?p=".$participant['participant_id_crypt']);
					//$_REQUEST['subscriptions']=$participant_subscriptions;
				}
			}
		}
		participant__show_form($_REQUEST,$lang['save'],$lang['edit_participant_data'],$errors__dataform,false,true,$suptypes);

		echo '<CENTER>
			<BR><BR>
			<A HREF="participant_show.php?p='.url_cr_encode($_REQUEST['participant_id']).'">'.
			$lang['click_to_experiment_registrations'].'</A>
			<BR><BR>
			<FORM action=participant_delete.php>
			<INPUT type=hidden name="p" value="'.url_cr_encode($_REQUEST['participant_id']).'">
			<TABLE>
			<TR>
			<TD>
			'.$lang['i_want_to_delete_my_data'].'<BR></TD>
			</TR>
			<TR><TD>
			<center><INPUT type=submit name=delete value="'.$lang['unsubscribe'].'"></center>
			</TD>
			</TR>
			</TABLE>
			</FORM>
			</center>';

		}

	echo '</CENTER>';

include("footer.php");

?>

<?php
// part of rasee
$input=$_REQUEST;
$algo="aes-128-cbc";
$input["iv"]=bin2hex(openssl_random_pseudo_bytes(openssl_cipher_iv_length($algo)/2));
// if(empty($input["iv"]) || empty($input["data"])) exit("- You are not authorised -");

include("nonoutputheader.php");
include_once("../config/participant_form.php");
error_reporting(E_ERROR | E_PARSE);
$sk=get_security_key();
// $algo="aes-128-cbc"; if(!in_array($algo,openssl_get_cipher_methods(true))) $algo=openssl_get_cipher_methods(true)[0];
// $jsondata=openssl_decrypt($input["data"],$algo,$sk,0,$input["iv"]);
// echo $input["data"]."\r\n".$sk."\r\n".$algo."\r\n".$input["iv"]."\r\n-\r\n";
// if($jsondata===false) exit("- You are not authorised!");

$rec=array("session"=>'6', "experiment_id"=>'11712918', "action"=>"change_session", "cpid"=>"cduxN3Qzj8Xmk", "add_minutes"=>'20160'); //json_decode($jsondata,true);

if(empty($rec["external_code"]) && empty($rec["cpid"])) exit("- Incorrect data error.");
if(empty($rec["action"])) exit("- Forbidden");
$actions=explode(",",$rec["action"]);
if(empty($rec["cpid"]) && empty($rec["experiment_id"]) && empty($rec["session"]) && !in_array("confirm_reservation",$actions) && !in_array("fail_reservation",$actions)) result(2);

// var_dump($actions);
	

$session_ids=array(); $sesscodelen=3;
if(!empty($rec["sesscode"])) $sesscodelen=strlen($rec["sesscode"]);
if(!empty($rec["session_id"])) $session_ids=explode(",",$rec["session_id"]);

$errlevel=0;

$pid=0; $flname=""; $participant=array();
if(!empty($rec["cpid"])) {
	$pid=url_cr_decode($rec["cpid"],true);
	if(empty($pid)) result(2);
	$participant=orsee_db_load_array("participants",$pid,"participant_id");
	$flname=$participant["fname"].",".$participant["lname"];
}
if(empty($rec["cpid"]) || !empty($rec["session"]) || !empty($rec["experiment_id"])) {
	if(!empty($rec["session"])) {
		$sessq="SELECT session_id FROM ".table("sessions")." WHERE session_id>0 AND session_finished='n' AND session_remarks like '%#t".$sesscodelen."md5-".$rec["session"]."#%'";
		// $sessq.=" AND session_remarks not like '%!second_day!%'";
		// $sessq.=" AND session_remarks not like '%!third_day!%'";
		$sessq.=" AND session_remarks not like '%!api_invisible!%'";
		if(!empty($rec["experiment_id"])) $sessq.=" AND experiment_id='".$rec["experiment_id"]."'";
		$sessq.=" ORDER BY session_start_year,session_start_month,session_start_day,session_start_hour,session_start_minute";
		$ids=orsee_query($sessq,"return_first_elem");
		// echo $rec["sesscode"]."\r\n".$sessq."\r\n"; print_r($ids);
		if($ids!==false && count($ids)>0) $session_ids=array_merge($session_ids,$ids);
		else result(1,$flname);
	}

	$where=" WHERE 1";
	if(!in_array("confirm_reservation",$actions)) $where.=" AND registered='y'";
	$sess_id_cond=array_map(function($el) {return "session_id=".$el."";}, $session_ids);
	if(!empty($rec["external_code"])) $where.=" AND external_code='".$rec["external_code"]."'";
	if(!empty($pid)) $where.=" AND participant_id=".$pid."";
	if(!empty($sess_id_cond)) $where.=" AND (".implode(" OR ",$sess_id_cond).")";
	if(!empty($rec["experiment_id"])) $where.=" AND experiment_id='".$rec["experiment_id"]."'";
	$where.=" AND payghost=0";
	$q="SELECT * FROM ".table("participate_at").$where;
	$q.=" ORDER BY participate_id";

	$lines=orsee_query($q,"return_same");
	// echo $flname."\r\n".$rec["sesscode"]."\r\n".$q."\r\n"; if($lines===false) echo $lines."\r\n"; else print_r($lines);
	if($lines===false || count($lines)==0) result(1,$flname);
	$line=end($lines);

	if(empty($sess_id_cond)) {
		$session_id=$line["session_id"]; $where.=" AND session_id=".$session_id."";
	}

	$pid=$line["participant_id"];
}
else $where= "WHERE 1";
$participant=orsee_db_load_array("participants",$pid,"participant_id");
$flname=$participant["fname"].",".$participant["lname"];
$where.=" AND participant_id=".$pid."";

$supdata=array();
var_dump("pid=$pid",$rec);
if(!empty($rec["action"]) && !empty($pid) ) {
	$sets=array();
	// $modify_participate_at=false;
	if(in_array("check_participation",$actions)) { $sets[]="shownup='y',participated='y'"; }
	elseif(in_array("check_presence",$actions)) {  $sets[]="shownup='y'"; }
	if(in_array("change_session",$actions) && !empty($rec["add_minutes"])){
		$session_id=$line["session_id"];
		$sessarr=orsee_db_load_array("sessions",$session_id,"session_id");
		var_dump("sessarr",$sessarr);
		$session_unixtime=sessions__get_session_time($sessarr);
		$newsession_unixtime=$session_unixtime+$rec["add_minutes"]*60;
		$sameexp_sessids=experiment__list_session_id($sessarr["experiment_id"],true);
		foreach($sameexp_sessids as $newsessid) {
			$csesstime=sessions__get_session_time(array(),$newsessid);
			var_dump("csesstime=$csesstime , newsession_unixtime=$newsession_unixtime");
			if($csesstime == $newsession_unixtime) {
				$sets[]="session_id=$newsessid";
				break;
			}
		}
		var_dump("sets",$sets);
		if(empty($sets)) result(3,$flname);
	}
	exit;
	
	if(!empty($sets)) {
		$uq="UPDATE ".table("participate_at")." SET ".implode(",",$sets).$where;
		orsee_query($uq);
	}
	$psets=array();
	if(in_array("subscribe_to_lab",$actions) && !empty($rec["lab"])) {
		$subscriptions=explode(",",$participant["subscriptions"]);
		$labs=explode(",",$rec["lab"]);
		$lab_added=false;
		$new_subscriptions=array();
		foreach($labs as $lab) {
			if(!in_array($lab,$subscriptions)) {
				$subscriptions[]=$lab;
				$new_subscriptions[]=$lab;
			}
		}
		if(!empty($new_subscriptions)) {
			sort($subscriptions);
			$psets[]="subscriptions='".implode(",",$subscriptions)."'";
			log__participant("external_subscribe",$pid,implode(",",$new_subscriptions));
		}
	}
	if(!empty($psets)) {
		$uq="UPDATE ".table("participants")." SET ".implode(",",$psets)." WHERE participant_id=".$pid."";
		$ores=orsee_query($uq);
		// if($ores ===  false) result(3,$uq);
	}
	if(in_array("confirm_reservation",$actions) || in_array("fail_reservation",$actions)) {
		$crf="SELECT * FROM ".table('participants_log')." WHERE action='booking' AND `id`='".$pid."'";
		$pidcrf=orsee_query($crf);
		if($pidcrf!==false) {
			$atarget=explode("\n",$pidcrf['target']);
			$targets=array();
			foreach($atarget as $stinfo) {
				// echo "\nstinfo:$stinfo\n";
				$atinfo=explode(":",$stinfo);
				if(count($atinfo)>1) {
					$targets[trim($atinfo[0])]=trim($atinfo[1]);
				}
			}
			// print_r($targets); exit;
			$uq="UPDATE ".table('participants_log')." SET action='book_start' WHERE action='booking' AND `id`='".$pid."'";
			orsee_query($uq);
			$whattolog=in_array("fail_reservation",$actions)?"book_fail":"book_end";
			log__participant($whattolog,$pid,"experiment_id:".$targets['experiment_id']."\nsession_id:".$targets['session_id']);
			$supdata['message']="";
			$supdata['registration_error_code']="0";
			$supdata['registration_warning']="";
			$reservation_minutes=sessions__get_remark_list_elem_by_name($targets['session_id'],"reservation_minutes");
			if(!empty($reservation_minutes) && is_numeric($reservation_minutes) && $reservation_minutes>0) {
				$now=time();
				$supdata['reservation_minutes']=$reservation_minutes;
				$min_time=$now-($reservation_minutes*60);
				$session_unixtime=sessions__get_session_time(array(),$targets['session_id']);
				$registration_unixtime=sessions__get_registration_end(array(),$targets['session_id']);
				$session_full=sessions__session_full($targets['session_id']);
				$supdata['session_name']=time__format_session_time($targets['session_id']);
				if(!in_array("fail_reservation",$actions)) {
					$registration_limittime=$registration_unixtime;
					if(!empty($pidcrf['timestamp']) && ($registration_unixtime-$pidcrf['timestamp'] < ($reservation_minutes*60))) {
						$registration_limittime = $pidcrf['timestamp'] + ($reservation_minutes*60);
					}
					if($registration_limittime < $now || $session_unixtime < $now) {
						/// trop tard
						$supdata['registered']=0; $supdata['registration_error']="too_late"; $supdata['registration_warning']=$supdata['registration_error'];
						$supdata['registration_error_code']="1";
					}
					elseif((empty($pidcrf['timestamp']) || $pidcrf['timestamp']<$min_time) && $session_full) {
						$supdata['registered']=0; $supdata['registration_error']="session_full"; $supdata['registration_warning']='reservation_time_passed';
						$supdata['registration_error_code']="2";
						if(empty($pidcrf['timestamp'])) {
							$supdata['registration_error_code']="2.5";
							$supdata['message']=lang("an_error_has_occured");
						}
					}
					else {
						try {
							expregister__register($pid,$targets['session_id']);
							log__participant("register",$pid,"experiment_id:".$targets['experiment_id']."\nsession_id:".$targets['session_id']);
							$supmess="";
							query_makecolumns(table('experiments'),"confirm_message_supinfo","TEXT NULL");
							if(!empty(experiment__get_value($targets['experiment_id'],'confirm_message_supinfo'))) $supmess="<hr>".experiment__get_value($targets['experiment_id'],'confirm_message_supinfo');
							$supdata['message']=$lang['successfully_registered_to_experiment_xxx']." ".
								experiment__get_public_name($targets['experiment_id']).", ".
								time__format_session_time($targets['session_id']).". ".
								$lang['this_will_be_confirmed_by_an_email'].$supmess;
							$supdata['registered']=1; $supdata['registration_error']="";
						}
						catch (Exception $ex) {
							$supdata['registered']=0; $supdata['registration_error']=$ex->getMessage();
							$supdata['message']=lang("an_error_has_occured");
							$supdata['registration_error_code']="3";
						}
						if($pidcrf['timestamp']<$min_time && !$session_full) $supdata['registration_warning']='reservation_time_passed';
					}
				}
				$supdata["participant_link"]=$settings__root_url."/".$GLOBALS['settings__public_folder']."/participant_show.php?p=".url_cr_encode($pid);
				
			}
		}
		else {
			$supdata['registered']=0; $supdata['registration_error']="already_done"; $supdata['registration_warning']='';
			$supdata['registration_error_code']="4";			
		}
	}
		
}

result(0,$flname,$supdata);

function result($error_level,$flname="",$supdata=array()) {
	global $algo, $sk, $input;
	$encodingfile="../scripts/Encoding.php";
	if(file_exists($encodingfile)) {
		include_once($encodingfile);
		$flname=\ForceUTF8\Encoding::fixUTF8($flname);
	}
	else $flname=iconv(mb_detect_encoding($flname, mb_detect_order(), true), "UTF-8", $flname);
	$postdata=array("error_level"=>$error_level,"flname"=>$flname);
	foreach($supdata as $k=>$v) $postdata[$k]=file_exists($encodingfile)?\ForceUTF8\Encoding::fixUTF8($v):iconv(mb_detect_encoding($v, mb_detect_order(), true), "UTF-8", $v);
	$encdata=openssl_encrypt(json_encode($postdata),$algo,$sk,0,$input["iv"]);
	// print_r($postdata); echo "-".$flname."-".json_encode($postdata)."- ".$sk."\r\n<br>\r\n";
	header('Access-Control-Allow-Origin: https://s2ch-experiences.huma-num.fr');
	// header('Access-Control-Allow-Origin: http://localhost');
	echo $encdata;
	exit;
}

?>

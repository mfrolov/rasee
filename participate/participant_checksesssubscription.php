<?php
// part of rasee
$input=$_REQUEST;
if(empty($input["iv"]) || empty($input["data"])) exit("- You are not authorised -");

include("nonoutputheader.php");

$sk=get_security_key();
$algo="aes-128-cbc"; if(!in_array($algo,openssl_get_cipher_methods(true))) $algo=openssl_get_cipher_methods(true)[0];
$jsondata=openssl_decrypt($input["data"],$algo,$sk,0,$input["iv"]);
// echo $input["data"]."\r\n".$sk."\r\n".$algo."\r\n".$input["iv"]."\r\n-\r\n";
if($jsondata===false) exit("- You are not authorised!");

$rec=json_decode($jsondata,true);
if(empty($rec["external_code"]) && empty($rec["cpid"])) exit("- Incorrect data error.");
if(empty($rec["session"]) && empty($rec["experiment_id"])) result(2);

	

$session_ids=array(); $sesscodelen=3;
if(!empty($rec["sesscode"])) $sesscodelen=strlen($rec["sesscode"]);
if(!empty($rec["session_id"])) $session_ids=explode(",",$rec["session_id"]);

$errlevel=0;

$pid=0; $flname=""; $participant=array();
if(!empty($rec["cpid"])) {
	$pid=url_cr_decode($rec["cpid"],true);
	if(empty($pid)) result(2);
	$participant=orsee_db_load_array("participants",$pid,"participant_id");
	$flname=$participant["fname"].",".$participant["lname"];
}

if(!empty($rec["session"])) {
	$sessq="SELECT session_id FROM ".table("sessions")." WHERE session_id>0 AND session_finished='n' AND session_remarks like '%#t".$sesscodelen."md5-".$rec["session"]."#%'";
	// $sessq.=" AND session_remarks not like '%!second_day!%'";
	// $sessq.=" AND session_remarks not like '%!third_day!%'";
	$sessq.=" AND session_remarks not like '%!api_invisible!%'";
	if(!empty($rec["experiment_id"])) $sessq.=" AND experiment_id='".$rec["experiment_id"]."'";
	$sessq.=" ORDER BY session_start_year,session_start_month,session_start_day,session_start_hour,session_start_minute";
	$ids=orsee_query($sessq,"return_first_elem");
	// echo $rec["sesscode"]."\r\n".$sessq."\r\n"; print_r($ids);
	if($ids!==false && count($ids)>0) $session_ids=array_merge($session_ids,$ids);
	else result(1,$flname);
}

$where=" WHERE registered='y'";
$sess_id_cond=array_map(function($el) {return "session_id=".$el."";}, $session_ids);
if(!empty($rec["external_code"])) $where.=" AND external_code='".$rec["external_code"]."'";
if(!empty($pid)) $where.=" AND participant_id=".$pid."";
if(!empty($sess_id_cond)) $where.=" AND (".implode(" OR ",$sess_id_cond).")";
if(!empty($rec["experiment_id"])) $where.=" AND experiment_id='".$rec["experiment_id"]."'";
$where.=" AND payghost=0";
$q="SELECT * FROM ".table("participate_at").$where;
$q.=" ORDER BY participate_id";

$lines=orsee_query($q,"return_same");
// echo $flname."\r\n".$rec["sesscode"]."\r\n".$q."\r\n"; if($lines===false) echo $lines."\r\n"; else print_r($lines);
if($lines===false || count($lines)==0) result(1,$flname);
$line=end($lines);

if(empty($sess_id_cond)) {
	$session_id=$line["session_id"]; $where.=" AND session_id=".$session_id."";
}
if(empty($pid)) {
	$pid=$line["participant_id"];
	$participant=orsee_db_load_array("participants",$pid,"participant_id");
	$flname=$participant["fname"].",".$participant["lname"];
	$where.=" AND participant_id=".$pid."";
}
if(!empty($rec["action"])) {
	$actions=explode(",",$rec["action"]);
	$sets=array();
	if(in_array("check_participation",$actions)) $sets[]="shownup='y',participated='y'";
	elseif(in_array("check_presence",$actions)) $sets[]="shownup='y'";
	$uq="UPDATE ".table("participate_at")." SET ".implode(",",$sets).$where;
	orsee_query($uq);
}

result(0,$flname);

function result($error_level,$flname="",$supdata=array()) {
	global $algo, $sk, $input;
	$encodingfile="../scripts/Encoding.php";
	if(file_exists($encodingfile)) {
		include_once($encodingfile);
		$flname=\ForceUTF8\Encoding::fixUTF8($flname);
	}
	else $flname=iconv(mb_detect_encoding($flname, mb_detect_order(), true), "UTF-8", $flname);
	$postdata=array("error_level"=>$error_level,"flname"=>$flname);
	foreach($supdata as $k=>$v) $postdata[$k]=file_exists($encodingfile)?\ForceUTF8\Encoding::fixUTF8($v):iconv(mb_detect_encoding($v, mb_detect_order(), true), "UTF-8", $v);
	$encdata=openssl_encrypt(json_encode($postdata),$algo,$sk,0,$input["iv"]);
	// print_r($postdata); echo "-".$flname."-".json_encode($postdata)."- ".$sk."\r\n<br>\r\n";
	header('Access-Control-Allow-Origin: https://s2ch-experiences.huma-num.fr');
	// header('Access-Control-Allow-Origin: http://localhost');
	echo $encdata;
	exit;
}

?>

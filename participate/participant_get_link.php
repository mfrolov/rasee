<?php
// part of orsee. see orsee.org
ob_start();

$menu__area=empty($_REQUEST['menu'])?"":$_REQUEST['menu'];
include("header.php");

if(!empty($_REQUEST['rp'])) {
	if(strlen($_REQUEST['rp'])==11) $_REQUEST['rp']='cd'.$_REQUEST['rp'];
	$participant_id=url_cr_decode($_REQUEST['rp']);
}
else redirect($GLOBALS['settings__public_folder']."/");
if(empty($participant_id) && strlen($_REQUEST['rp'])<13) redirect($GLOBALS['settings__public_folder']."/");

$logname='askforlink';

$valid_pages=array("participant_edit.php",
		"participant_delete.php",
		"participant_show.php",
		"participant_show_print.php",
		"participant_desinscription.php",
		"show_calendar.php",
		"participant_confirm.php");

$page="participant_edit.php";
if(!empty($_REQUEST['page']) && in_array(trim($_REQUEST['page']),$valid_pages)) $page=trim($_REQUEST['page']);

$participant=array();
if(!empty($participant_id)){
	$participant=orsee_db_load_array("participants",$participant_id,"participant_id");
}
elseif(isset($_POST['email'])) {
	$pform=participantform__define();
	$mailform=array();
	foreach($pform as $pf) if($pf['mysql_column_name']=='email') $mailform=$pf;
	if(!preg_match($mailform['perl_regexp'],$_REQUEST['email'])) {
		message(lang($mailform['error_message_if_no_regexp_match_lang']));
		$supexpired="";
		if(!empty($_REQUEST['expired'])) $supexpired="&expired=1";
		redirect($GLOBALS['settings__public_folder']."/".thisdoc()."?rp=".$_REQUEST['rp']."&page=".$_REQUEST['page'].$supexpired);
	}
	$participant=rasee_db_load_array("participants",array('email'=>$_POST['email'],'deleted'=>'n'));
	if(empty($participant)) {
		log__participant($logname,'email_not_found ('.$_POST['email'].')',$page,false);
		$messtext=lang('email_address_not_found');
		$messtext.='<br>';
		$messtext.=$lang['if_you_have_questions_write_to']." ";
		$messtext.=support_mail_link();
		message($messtext);
		redirect($GLOBALS['settings__public_folder']."/");
	}
}

$securimage="";
if(file_exists("../../securimage/securimage.php")) $securimage="../../securimage/securimage.php";
if(file_exists("../securimage/securimage.php")) $securimage="../securimage/securimage.php";

if(!empty($securimage) && !empty($_REQUEST['yes_get_link'])) {
	$audiopath=str_replace("securimage.php","audio",$securimage);
	require_once $securimage;
	$securimage = new Securimage();
	$ask_captcha=false;
	query_makecolumns(table('participants_log'),"ipaddr","varchar(255)","''");
	$logs=orsee_db_load_full_array('participants_log',get_client_ip(),'ipaddr');
	$ctime=time();
	if($logs!==false) foreach($logs as $log) {
		if($log['action']==$logname) {
			$ctimediff=$ctime-$log['timestamp'];
			if($ctimediff<3600) $ask_captcha=true;
		}
	}
	// var_dump(get_client_ip(),$ask_captcha,$logs,$ctimediff); exit;
	if($ask_captcha) {
		if(!empty($_REQUEST['rasee_captcha'])) {
			$_CURRENT_SESSION=$_SESSION;
			$_SESSION=$_DEFAULT_SESSION;
			if($securimage->check($_REQUEST['rasee_captcha'])) $ask_captcha=false;
			else echo "<h4>".lang('incorrect_code').', '.lang('please_retry',false)."</h4>";
			$_SESSION=$_CURRENT_SESSION;
		}
		if($ask_captcha) {
			echo "<center><h5>".lang('please_copy_the_image_code_to_continue')."</h5>";
			$options = array();
			$options['input_name'] = 'rasee_captcha'; // change name of input element for form post
			$options['disable_flash_fallback'] = false; // allow flash fallback
			$options['input_text'] = lang("type_the_text").' : ';
			$options['show_text_input']=true;
			$options['show_audio_button']=file_exists($audiopath);

			$action=thisdoc();
			// var_dump($_REQUEST['rasee_captcha'],$securimage->check($_REQUEST['rasee_captcha']),$_SESSION,$_DEFAULT_SESSION);
			echo '<FORM method=POST action='.$action.'>';
			/*$formfields=participantform__load();
			foreach ($formfields as $f) { 
			if($f['subpools']=='all' | in_array($subpool['subpool_id'],explode(",",$f['subpools']))) {
				if ($f['type']=='invitations') {
					if (isset($_REQUEST[$f['mysql_column_name']]) && !is_array($_REQUEST[$f['mysql_column_name']])) $_REQUEST[$f['mysql_column_name']]=explode(",",$_REQUEST[$f['mysql_column_name']]);
				}
			}}*/
			foreach($_REQUEST as $rk=>$rv) {
				if(!is_array($rv)) echo '<INPUT type=hidden name="'.$rk.'" value="'.$rv.'">'.PHP_EOL;
				else foreach($rv as $rvk=>$rvv) echo '<INPUT type=hidden name="'.$rk.'['.$rvk.']" value="'.$rvv.'">'.PHP_EOL;
			}
			echo "<table><tr><td><div style='white-space:nowrap' id='captcha_container_1'>";
			echo Securimage::getCaptchaHtml($options);
			echo "</div></td></tr></table>\n";
			echo "
			<p>
			<br>
			<input type='submit' value='".lang('submit')."'>
			</p>
			</FORM>";
			echo "</center>";
			exit;
		}
	}
}

if (!empty($participant) && $participant['deleted']=="y") {
	$messtext=empty($participant['can_undelete'])?$lang['error_sorry_you_are_deleted']:lang('your_account_is_now_desactivated');
	$messtext.=". ";
	if(!empty($participant['can_undelete'])) $messtext.='<br><form action="participant_edit.php"><input type=hidden name="p" value="'.url_cr_encode($participant['participant_id']).'"><input type=hidden name=resubscribe value=1><input type=submit style="margin-left:50px" value="'.lang('reactivate_your_account').'"></form>';
	// else $messtext.="-".$participant['can_undelete']."-";
	$messtext.=$lang['if_you_have_questions_write_to']." ";
	if(!empty($participant['can_undelete'])) $messtext.='<br>';
	$messtext.=support_mail_link();
	message($messtext);
	redirect($GLOBALS['settings__public_folder']."/");
}

$expiration_deadline_edit=get_general_option("expiration_link_edit_hours");

$log_index=-1;//log__get_participant_index($participant_id,$logname);

$logs=orsee_db_load_full_array('participants_log',$participant_id,'id');
// var_dump($logs);
$ctime=time();
$last_log_time=0;
if($logs !== false) foreach($logs as $log) {
	// var_dump($log,$logname,$page,$log['action'],$log['target']);
	if($log['action']==$logname && $log['target']==$page) {
		$ctimediff=$ctime-$log['timestamp'];
		if($ctimediff<3600*24) {
			$log_index++;
			if($log['timestamp']>$last_log_time) $last_log_time=$log['timestamp'];
		}
	}
}

if($log_index < 0 && !empty($_REQUEST['yes_get_link'])) {

	$supmailmessage='';
	$subject="#".lang('the_new_link to_access_your_personal_space');
	if(!empty($expiration_deadline_edit) && $page=="participant_edit.php") {
		$supmailmessage.=lang("this_link_will_be_valid_for $expiration_deadline_edit hours").". ";
	}
	experimentmail__mail_new_link($participant['participant_id'],$supmailmessage,'',$page,$subject);
	
	log__participant($logname,$participant['participant_id'],$page,false);

	$log_index=0;
}
if($log_index>=0 && !empty($_REQUEST['yes_get_link'])) {
	// echo '<center><br/>';
	message(lang('the_new_link has_been_sent_to_you'));
	// echo '</center>';
	redirect($GLOBALS['settings__public_folder']."/");
	// exit;
}

$yes_text=lang('send_me_the_new_link'); //$no_text=$lang['no_sorry'];
$text="";
if(!empty($expiration_deadline_edit) && !empty($_REQUEST['page']) && $_REQUEST['page']=="participant_edit.php") {
	$text.="<p>".lang("in_order_to_protect_your_personal_data the_link_to_access_this_area should_be_renewed_each $expiration_deadline_edit hours").".</p>";
}
elseif (!empty($_REQUEST['expired'])) $text.="".lang('this_link_is_expired').". ";
else  $text.="<br/>";
if(empty($participant_id)) $emailfield='
  <div class="container" style="max-width:600px">
    <label for="exampleInputEmail1" class="form-label">'.lang('please_enter_your_registered_email_address in_order_to_receive the_link_to_access_this_area').'</label>
    <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
    <div id="emailHelp" class="form-text">'.lang('we_will_send_your_the_new_link_if_this_address_exists_in_our_database').'</div>
  </div><br><br>
';
if(empty($participant_id)) $text.="<p>".lang('incorrect_or_expired_link').".</p>".$emailfield;
// var_dump($participant_id,$log_index,$page,$last_log_time,$logs);
if($log_index>=0) {
	$text.=lang("the_new_link has_been_already_sent_to_you by mail on_the_date")." ".time__format($lang['lang'],'',false,false,false,false,$last_log_time).". ";
	echo '<br><br><center>'.$text.'</center>';
}
else {
	$text.="".lang('to_receive the_new_link please_click_on_the_next_button')."<br/><br/>";
	echo '
	<br/>
	<FORM action="'.thisdoc().'">

	<center>
	'.$text.'
	<INPUT type=hidden name="rp" value="'.$_REQUEST['rp'].'">
	<INPUT type=hidden name="page" value="'.$_REQUEST['page'].'">
	<INPUT type=submit name="yes_get_link" value="'.$yes_text.'">
	</center>
	</FORM>';
}
include ("footer.php");
?>
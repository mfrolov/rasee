<?php
if(!function_exists("redirect") && (empty($menu__area) || $menu__area!="contact")) header("Location: contact.php");
$external_source_deb="";
$external_source_class="entry-content";
?>
<div class="container">
<?php
$lab_list=["test"];
$lab_names=["test"=>"Test lab"];
$lab_urls=$lab_list;

$limitto=false;
if(isset($_REQUEST["lab"]) && in_array(mb_strtolower($_REQUEST["lab"]),$lab_list)) $limitto=mb_strtolower($_REQUEST["lab"]);
?>
<ul class="nav nav-tabs" id="myTab" role="tablist">
<?php
foreach($lab_list as $k=>$lab) {
	echo '
	<li class="nav-item" role="presentation">
    <a class="nav-link';
	if((!$limitto && $k==0) || (!empty($limitto) && $limitto == $lab)) echo ' active';
	echo '" id="'.$lab.'-tab" ';
	if(!$limitto || $lab == $limitto) echo 'data-bs-toggle="tab" ';
	else echo 'href="'.$_SERVER['PHP_SELF'].'?lab='.$lab.'"';
	$area_selected="false";
	if((!$limitto && $k==0) || (!empty($limitto) && $limitto == $lab)) $area_selected="true";
	$lab_name=empty($lab_names[$lab])?mb_strtoupper($lab):$lab_names[$lab];
	echo ' data-bs-target="#'.$lab.'" type="button" role="tab" aria-controls="'.$lab.'" aria-selected="'.$area_selected.'">'.$lab_name.'</a>';
	echo '
	   </li>
	';
}
?>
</ul>
  <div class="tab-content" id="myTabContent">
<?php
foreach($lab_list as $k=>$lab) {
	echo '<div class="tab-pane fade';
	if((!$limitto && $k==0) || (!empty($limitto) && $limitto == $lab)) echo ' show active';
	echo '" id="'.$lab.'" role="tabpanel" aria-labelledby="'.$lab.'-tab">
	';
	if(file_exists("../labs/".$lab."/contact.php")) include("../labs/".$lab."/contact.php");
elseif(!$limitto || $lab == $limitto) echo load_elem_by_class($external_source_class,$external_source_deb.$lab_urls[$k]."/");
	if(file_exists("../labs/".$lab."/contact_plus.php")) include("../labs/".$lab."/contact_plus.php");
	echo '
	</div>
	';
	
}
?>

  </div>
</div>


<?php
function get_web_page( $url, $cookiesIn = '', $post = array() ){
        $options = array(
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => true,     //return headers in addition to content
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLINFO_HEADER_OUT    => true,
            CURLOPT_SSL_VERIFYPEER => true,     // Validate SSL Cert
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_COOKIE         => $cookiesIn
        );
		$options[CURLOPT_SSL_VERIFYHOST]=false;
		$options[CURLOPT_SSL_VERIFYPEER]=false;

		
		if(!empty($post)) {
			$options[CURLOPT_POST] = 1;
			$options[CURLOPT_POSTFIELDS] = $post;
		}

        $ch      = curl_init( $url );
        curl_setopt_array( $ch, $options );
        $rough_content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );

        $header_content = substr($rough_content, 0, $header['header_size']);
        $body_content = trim(str_replace($header_content, '', $rough_content));
        $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m"; 
        preg_match_all($pattern, $header_content, $matches); 
        $cookiesOut = implode("; ", $matches['cookie']);

        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['headers']  = $header_content;
        $header['content'] = $body_content;
        $header['cookies'] = $cookiesOut;
    return $header;
}

function load_elem_by_class($classname, $pageurl) {
	$labpage=get_web_page($pageurl);
	$dom = new DOMDocument;
	$internalErrors = libxml_use_internal_errors(true);
	$dom->encoding = 'UTF-8';
	$dom->loadHTML($labpage["content"]);

	$xpath = new DOMXPath($dom);
	foreach( $xpath->query('//div[contains(attribute::class, "et_pb_section_0")]') as $e ) {
		$e->parentNode->removeChild($e);
	}
	foreach( $xpath->query('//div[contains(attribute::class, "et_pb_image")]') as $e ) {
		$e->parentNode->removeChild($e);
	}
	$results = $xpath->query("//*[@class='" . $classname . "']");
	$n=$results->item(0);
	// var_dump($n->hasChildNodes());


	if ($results->length > 0) {
		$d = new DOMDocument('1.0');
		$b = $d->importNode($n->cloneNode(true),true);
		$d->appendChild($b); $h = $d->saveHTML();
		$h = substr($h,strpos($h,'>')+1,-(strlen($n->nodeName)+4));
		return $h	;
	}
	else return "-";
}
?>
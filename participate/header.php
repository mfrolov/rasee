<?php
// part of orsee. see orsee.org
session_start();
$_DEFAULT_SESSION=$_SESSION;
session_write_close();

include ("../config/settings.php");
include ("../config/system.php");
include ("../config/requires.php");
include ("../config/participant_form.php");

site__database_config();

$settings=load_settings();
$settings['style']=$settings['orsee_public_style'];
$color=load_colors();

session_set_save_handler("orsee_session_open", "orsee_session_close", "orsee_session_read", "orsee_session_write", "orsee_session_destroy", "orsee_session_gc");

session_start();
if (isset($_SESSION['authdata'])) $authdata=$_SESSION['authdata']; else $authdata=array();

$_REQUEST=strip_tags_array($_REQUEST);

if (isset($_REQUEST['language'])) {
	$langarray=lang__get_public_langs();
	if (in_array($_REQUEST['language'],$langarray)) {
		$authdata['language']=$_REQUEST['language'];
		$_SESSION['authdata']=$authdata;
		}
	else unset($authdata['language']);
}

if (!empty($_REQUEST['p'])) {
	$initial_cpid_decrypted=link_en_de_crypt($_REQUEST['p'],2);
	$initial_p=$_REQUEST['p'];
	// var_dump($_REQUEST['p'],$initial_cpid);
	if($initial_cpid_decrypted!==false && !is_array($initial_cpid_decrypted)) {
		$initial_cpid_array=explode('_',$initial_cpid_decrypted);
		$initial_cpid=$initial_cpid_array[0];
		// $_REQUEST['p']=$initial_cpid;
		// var_dump($_REQUEST['p'],$initial_cpid,$initial_cpid_array); exit;
		if(count($initial_cpid_array)>1) {
			$expiration_deadline_general=get_general_option("expiration_link_general_days");
			$link_time=end($initial_cpid_array);
			if(!empty($expiration_deadline_general)) {
				if(time()-$link_time>$expiration_deadline_general*3600*24 && thisdoc()!="participant_get_link.php") {
					if(!empty($if_expired_redirect_to)) redirect($if_expired_redirect_to);
					$ma=empty($menu__area)?"":"&menu=".$menu__area;
					redirect($GLOBALS['settings__public_folder']."/participant_get_link.php?p=$initial_p&rp=$initial_cpid&page=".thisdoc()."&expired=1".$ma);
				}
			}
		}
	}
	else {
		// $lang=load_language($authdata['language']); die("<br><h2 align=center>".lang('incorrect_link')."</h2>"); //.' '.link_en_de_crypt($_REQUEST['p']));
	}
}

// load and check participant data
$part_load=array("participant_edit.php",
		"participant_delete.php",
		"participant_show.php",
		"participant_show_print.php",
		"participant_desinscription.php",
		"show_calendar.php");
if(!empty($part_load_sup)) $part_load=array_merge($part_load,(is_array($part_load_sup)?$part_load_sup:explode(",",$part_load_sup)));
if (in_array(thisdoc(),$part_load)) {

	// fix the uuencode malformed url issue
	if (empty($_REQUEST['p'])) {
		foreach ($_REQUEST as $key=>$value) {
			if (substr($key,0,1)=='p') $_REQUEST['p']=''.substr($key,strlen($key)-strlen(link_en_de_crypt('cd1234567890A')));
			}
	}
	if (!empty($_REQUEST['p'])) {
		$initial_cpid_decrypted=link_en_de_crypt($_REQUEST['p'],2);
		$initial_p=$_REQUEST['p'];
		// var_dump($_REQUEST['p'],$initial_cpid);
		if($initial_cpid_decrypted!==false && !is_array($initial_cpid_decrypted)) {
			$initial_cpid_array=explode('_',$initial_cpid_decrypted);
			$initial_cpid=$initial_cpid_array[0];
			if(count($initial_cpid_array)>1) {
				$expiration_deadline_general=get_general_option("expiration_link_general_days");
				$link_time=end($initial_cpid_array);
				if(!empty($expiration_deadline_general)) {
					if(time()-$link_time>$expiration_deadline_general*3600*24 && thisdoc()!="participant_get_link.php") {
						if(!empty($if_expired_redirect_to)) redirect($if_expired_redirect_to);
						$ma=empty($menu__area)?"":"&menu=".$menu__area;
						redirect($GLOBALS['settings__public_folder']."/participant_get_link.php?p=$initial_p&rp=$initial_cpid&page=".thisdoc()."&expired=1".$ma);
					}
				}
				if(thisdoc() == $part_load[0] || !empty($i_have_confident_information)) {
					$expiration_deadline_edit=get_general_option("expiration_link_edit_hours");
					if(!empty($expiration_deadline_edit)) {
						if(time()-$link_time>$expiration_deadline_edit*3600 && thisdoc()!="participant_get_link.php") {
							if(!empty($if_expired_redirect_to)) redirect($if_expired_redirect_to);
							$ma=empty($menu__area)?"":"&menu=".$menu__area;
							redirect($GLOBALS['settings__public_folder']."/participant_get_link.php?p=$initial_p&rp=$initial_cpid&page=".thisdoc()."&expired=1".$ma);
						}
					}
				}
			}
			$_REQUEST['p']=$initial_cpid;
			// var_dump($_REQUEST['p'],$initial_cpid,$initial_cpid_array); exit;
		}
		else {
			$rp=$initial_p;
			if(is_array($initial_cpid_decrypted) && count($initial_cpid_decrypted)>1) $rp=explode('_',$initial_cpid_decrypted[1])[0];
			if(strlen($rp)>10 && thisdoc()!="participant_get_link.php") {
				if(!empty($if_expired_redirect_to)) redirect($if_expired_redirect_to);
				$ma=empty($menu__area)?"":"&menu=".$menu__area;
				if((is_array($initial_cpid_decrypted) && count($initial_cpid_decrypted)>1) || !empty(url_cr_decode($initial_p))) $ma.='&expired=1';
				redirect($GLOBALS['settings__public_folder']."/participant_get_link.php?p=$initial_p&rp=$rp&page=".thisdoc().$ma);
			}
			$lang=load_language($authdata['language']); die("<br><h2 align=center>".lang('incorrect_link')."</h2>"); //.' '.link_en_de_crypt($_REQUEST['p']));
		}
	}

	// fix the uuencode malformed url issue
	if (empty($_REQUEST['p'])) {
		foreach ($_REQUEST as $key=>$value) {
			if (substr($key,0,1)=='p') $_REQUEST['p']='cd'.substr($key,strlen($key)-11);
			}
	}
	$pass_pages_witout_p=array("show_calendar.php","participant_desinscription.php");
	if(!in_array(thisdoc(),$pass_pages_witout_p) || !empty($_REQUEST['p'])) {
        if (empty($_REQUEST['p'])) redirect($GLOBALS['settings__public_folder']."/");
        $participant_id=url_cr_decode($_REQUEST['p']);
		if(!empty($initial_p)) $_REQUEST['p']=$initial_p;
        if (!$participant_id) redirect($GLOBALS['settings__public_folder']."/");
		if (thisdoc()=="participant_confirm.php")
			$participant=orsee_db_load_array("participants_temp",$participant_id,"participant_id");
		else
        	$participant=orsee_db_load_array("participants",$participant_id,"participant_id");

		$authdata['language']=$participant['language'];
		$_SESSION['authdata']=$authdata;
	}
}



if (!isset($authdata['language'])) $authdata['language']=$settings['public_standard_language'];
$lang=load_language($authdata['language']);

        if (isset($participant) && $participant['excluded']=="y") {
                message ($lang['error_sorry_you_are_excluded']." ".
                        $lang['if_you_have_questions_write_to']." ".support_mail_link());
                redirect($GLOBALS['settings__public_folder']."/");
                }

        if (isset($participant) && $participant['deleted']=="y" && (thisdoc()!="participant_edit.php" || empty($_REQUEST['resubscribe']))) {
				$messtext=empty($participant['can_undelete'])?$lang['error_sorry_you_are_deleted']:lang('your_account_is_now_desactivated');
				$messtext.=". ";
				if(!empty($participant['can_undelete'])) $messtext.='<br><form action="participant_edit.php"><input type=hidden name="p" value="'.url_cr_encode($participant['participant_id']).'"><input type=hidden name=resubscribe value=1><input type=submit style="margin-left:50px" value="'.lang('reactivate_your_account').'"></form>';
				// else $messtext.="-".$participant['can_undelete']."-";
                $messtext.=$lang['if_you_have_questions_write_to']." ";
				if(!empty($participant['can_undelete'])) $messtext.='<br>';
				$messtext.=support_mail_link();
				message($messtext);
                redirect($GLOBALS['settings__public_folder']."/");
                }


if ($settings['stop_public_site']=="y" && !isset($expadmindata['adminname']) && !(thisdoc()=="disabled.php")) redirect($GLOBALS['settings__public_folder']."/disabled.php");


$pagetitle=$settings['default_area'];
if (isset($title)) $pagetitle=$pagetitle.': '.$title;


html__header();
include ("../style/".$settings['style']."/html_header.php");

echo "<center>";

show_message();

echo "</center>";

// var_dump($_SESSION,$expadmindata,empty($_SESSION['expadmindata']['adminname']));
if (isset($participant) && $participant['deleted']=="n" && empty($_SESSION['expadmindata']['adminname'])) {
	query_makecolumns(table('participants'),array("last_visit_time","last_visit_page"),array("int(20)","text"),array("0","null"));
	query_makecolumns(table('participants_temp'),array("last_visit_time","last_visit_page"),array("int(20)","text"),array("0","null"));
	$query="UPDATE ".table('participants')."
			SET last_visit_time=UNIX_TIMESTAMP(), last_visit_page='".thisdoc()."' WHERE participant_id='".$participant['participant_id']."'";
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
}

if (isset($participant) && $participant['deleted']=="n" && thisdoc()!="participant_edit.php" && thisdoc()!="participant_create.php") {
	echo "<center style='position:relative;top:20px'>";

	echo lang("welcome_").str_replace("?","",$participant['fname'])." ".str_replace("?","",$participant['lname']);

	echo "</center>";
}

?>

<?php
// part of orsee. see orsee.org
session_start();
$_DEFAULT_SESSION=$_SESSION;
session_write_close();

include ("../config/settings.php");
include ("../config/system.php");
include ("../config/requires.php");

site__database_config();

$settings=load_settings();
$settings['style']=$settings['orsee_public_style'];
$color=load_colors();

session_set_save_handler("orsee_session_open", "orsee_session_close", "orsee_session_read", "orsee_session_write", "orsee_session_destroy", "orsee_session_gc");

session_start();
if (isset($_SESSION['authdata'])) $authdata=$_SESSION['authdata'];

$_REQUEST=strip_tags_array($_REQUEST);

if (isset($_REQUEST['language'])) {
	$authdata['language']=$_REQUEST['language'];
	$_SESSION['authdata']=$authdata;
}

if (!isset($authdata['language'])) {
	$authdata['language']=$settings['public_standard_language'];
	$_SESSION['authdata']=$authdata;
	}
if (!empty($_REQUEST['p'])) {
	$initial_cpid_decrypted=link_en_de_crypt($_REQUEST['p'],2);
	$initial_p=$_REQUEST['p'];
	// var_dump($_REQUEST['p'],$initial_cpid);
	if($initial_cpid_decrypted!==false && !is_array($initial_cpid_decrypted)) {
		$initial_cpid_array=explode('_',$initial_cpid_decrypted);
		$initial_cpid=$initial_cpid_array[0];
		// $_REQUEST['p']=$initial_cpid;
		// var_dump($_REQUEST['p'],$initial_cpid,$initial_cpid_array); exit;
	}
	else {
		if(is_array($initial_cpid_decrypted) && count($initial_cpid_decrypted)>1) $initial_cpid=explode('_',$initial_cpid_decrypted[1])[0];
		else $initial_cpid=$_REQUEST['p'];
	}
}
// load and check participant data
$part_load=array("participant_edit.php",
                "participant_delete.php",
                "participant_show.php",
                "participant_show_print.php");

if (in_array(thisdoc(),$part_load)) {

        if (!$_REQUEST['p']) redirect($GLOBALS['settings__public_folder']."/");
        $participant_id=url_cr_decode($_REQUEST['p']);
        if (!$participant_id) redirect($GLOBALS['settings__public_folder']."/");
        if (thisdoc()=="participant_confirm.php")
                $participant=orsee_db_load_array("participants_temp",$participant_id,"participant_id");
          else
                $participant=orsee_db_load_array("participants",$participant_id,"participant_id");

        $authdata['language']=$participant['language'];
        $_SESSION['authdata']=$authdata;
}

if (!isset($authdata['language'])) $authdata['language']=$settings['public_standard_language'];
$lang=load_language($authdata['language']);

        if (isset($participant) && $participant['excluded']=="y") {
                message ($lang['error_sorry_you_are_excluded']." ".
                        $lang['if_you_have_questions_write_to']." ".support_mail_link());
                redirect($GLOBALS['settings__public_folder']."/");
                }

        if (isset($participant) && $participant['deleted']=="y") {
				$messtext=$lang['error_sorry_you_are_deleted']." ";
				if(!empty($participant['can_undelete'])) $messtext.='<br><form action="participant_edit.php"><input type=hidden name="p" value="'.url_cr_encode($participant['participant_id']).'"><input type=hidden name=resubscribe value=1><input type=submit style="margin-left:50px" value="'.lang('reactivate_your_account').'"></form>';
				// else $messtext.="-".$participant['can_undelete']."-";
                $messtext.=$lang['if_you_have_questions_write_to']." ";
				if(!empty($participant['can_undelete'])) $messtext.='<br>';
				$messtext.=support_mail_link();
				message($messtext);
                redirect($GLOBALS['settings__public_folder']."/");
        }

if ($settings['stop_public_site']=="y" && !isset($umadmindata['username']) && !(thisdoc()=="error_temporaly_disabled.php")) redirect("errors/error_temporaly_disabled.php");

if (!isset($title)) $title="";
$pagetitle=$settings['default_area'].': '.$title;

?>

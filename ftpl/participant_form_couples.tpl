
		<tr #error_fname#>
			<td><label class="form-label" for="input_fname">lang[firstname]</label></td>
			<td>#fname#</td>
		</tr>
		<tr #error_lname#>
                        <td><label class="form-label" for="input_lname">lang[lastname]</label></td>
                        <td>#lname#</td>
                </tr>
		<tr #error_email#>
                        <td><label class="form-label" for="input_email">lang[e-mail-address]</label></td>
                        <td>#email#</td>
                </tr>
        { #multiple_participant_languages_exist# 
		<tr #error_language#>
                        <td><label class="form-label" for="input_language">lang[language]</label></td>
                        <td>#language#</td>
                </tr> }
		<tr #error_subscriptions#>
                        <td>
				{ #is_not_admin# En plus des invitations pour l'expérience sur les décisions de santé des couples, je souhaite aussi recevoir des invitations <strong>individuelles</strong> pour }
				{ #is_admin# lang[invitations] }
			</td>
                        <td>#subscriptions#
						<div class="form-check" style="visibility:hidden"><label class="form-check-label" for="subscriptions_cple"><input type="checkbox" checked class="form-check-input" id="subscriptions_cple" name="subscriptions[cple]" value="cple" onchange="if(this.checked) document.getElementById(this.name+0).style.textShadow='1px 0px 0px black'; else document.getElementById(this.name+0).style.textShadow=''"><span id="subscriptions[cple]0" style="text-shadow:">participer aux exp&eacute;riences du <a name="CPLE_inscr"></a><a href="#CPLE_inscr" class="exptype_hint" data-html="true" data-bs-html="true" data-bs-placement="auto" data-toggle="tooltip" title="" data-bs-original-title="Cette inscription est r&eacute;serv&eacute;e aux professionnels en gestion des ressources humaines ou en recrutement">CPLE</a> (expérience sur les décisions de santé des couples)</span></label></div>
						<input type="hidden" name="remarks" id="remarks" value="exp_couples_2023">
						</td>
                </tr>

		{ #is_not_admin#
		<tr>
			<td colspan="2"><center><em>lang[optional_fields_follow]</em></center></td>
		</tr> }

		<tr #error_gender#>
                        <td><label class="form-label" for="input_gender">lang[gender]</label></td>
                        <td>#gender#</td>
        </tr>
		<tr>
			<td #error_birth_year#><label class="form-label" for="input_birth_year">lang[birth_year]</label></td>
			<td colspan="2">#birth_year#</td>
		</tr>

		<tr>
			<td #error_professional_status#><label class="form-label" for="input_profession">lang[professional_status]</label></td>
			<td colspan="2">#profession#</td>
		</tr>
		{ #is_subpool_type_b#
			<tr>
				<td #error_activity_area#><label class="form-label" for="input_activity_area">lang[activity_area_question]</label></td>
				<td colspan="2">#activity_area#</td>
			</tr>
			<tr>
				<!-- <td></td><td><table><tr> -->
				<td #error_profession#><label class="form-label" for="input_field_of_studies">lang[studies_question]</label></td>
				<!-- </tr><tr> -->
				<td colspan="2">#field_of_studies#</td>	
				

				<!-- </tr></table></td> -->
			</tr>

		}

		{ #is_subpool_type_s#
                <tr #error_field_of_studies#>
                        <td><label class="form-label" for="input_field_of_studies">lang[studies_question]</label></td>
                        <td>#field_of_studies#</td>
		</tr>


		}
		{ #is_subpool_type_w#
			<tr>
				<td #error_activity_area#><label class="form-label" for="input_activity_area">lang[activity_area_question]</label></td>
				<td colspan="2">#activity_area#</td>
			</tr>
		}


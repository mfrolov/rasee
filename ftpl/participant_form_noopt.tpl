
		<tr #error_fname#>
			<td>lang[firstname]</td>
			<td>#fname#</td>
		</tr>
		<tr #error_lname#>
                        <td>lang[lastname]</td>
                        <td>#lname#</td>
                </tr>
		<tr #error_email#>
                        <td>lang[e-mail-address]</td>
                        <td>#email#</td>
                </tr>
        { #multiple_participant_languages_exist# 
		<tr #error_language#>
                        <td>lang[language]</td>
                        <td>#language#</td>
                </tr> }
		<tr #error_subscriptions#>
                        <td>
				{ #is_not_admin# lang[i_want_invitations_for] }
				{ #is_admin# lang[invitations] }
			</td>
                        <td>#subscriptions#</td>
                </tr>




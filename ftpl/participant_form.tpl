
		<tr #error_fname#>
			<td><label class="form-label" for="input_fname">lang[firstname]</label></td>
			<td>#fname#</td>
		</tr>
		<tr #error_lname#>
                        <td><label class="form-label" for="input_lname">lang[lastname]</label></td>
                        <td>#lname#</td>
                </tr>
		<tr #error_email#>
                        <td><label class="form-label" for="input_email">lang[e-mail-address]</label></td>
                        <td>#email#</td>
                </tr>
        { #multiple_participant_languages_exist# 
		<tr #error_language#>
                        <td><label class="form-label" for="input_language">lang[language]</label></td>
                        <td>#language#</td>
                </tr> }
		<tr #error_subscriptions#>
                        <td>
				{ #is_not_admin# lang[i_want_invitations_for] }
				{ #is_admin# lang[invitations] }
			</td>
                        <td>#subscriptions#</td>
                </tr>

		{ #is_not_admin#
		<tr>
			<td colspan="2"><center><em>lang[optional_fields_follow]</em></center></td>
		</tr> }


		<tr #error_phone_number#>
                        <td><label class="form-label" for="input_phone_number">lang[phone_number]
			{ #is_not_admin# <BR><FONT class="small">lang[phone_number_remark]</FONT> }</label>
			</td>
                        <td>#phone_number#</td>
        </tr>

		<tr #error_gender#>
                        <td><label class="form-label" for="input_gender">lang[gender]</label></td>
                        <td>#gender#</td>
        </tr>
		<tr>
			<td #error_birth_year#><label class="form-label" for="input_birth_year">lang[birth_year]</label></td>
			<td colspan="2">#birth_year#</td>
		</tr>
		<tr>
			<td #error_professional_status#><label class="form-label" for="input_profession">lang[professional_status]</label></td>
			<td colspan="2">#profession#</td>
		</tr>
		{ #is_subpool_type_b#
			<tr>
				<td #error_activity_area#><label class="form-label" for="input_activity_area">lang[activity_area_question]</label></td>
				<td colspan="2">#activity_area#</td>
			</tr>
			<tr>
				<!-- <td></td><td><table><tr> -->
				<td #error_profession#><label class="form-label" for="input_field_of_studies">lang[studies_question]</label></td>
				<!-- </tr><tr> -->
				<td colspan="2">#field_of_studies#</td>	
				
				<tr #hide_begin_of_studies#>
				<td ></td><td #error_begin_of_studies#><label class="form-label" for="input_begin_of_studies">lang[begin_of_studies]<br />#begin_of_studies#</label></td>
				</tr>
			<tr #hide_study_level# #error_study_level#>
				<td><label class="form-label" for="input_study_level">lang[study_level_question]</label></td>
				<td>#study_level#</td>
			</tr>

				<!-- </tr></table></td> -->
			</tr>

		}

		{ #is_subpool_type_s#
                <tr #error_field_of_studies#>
                        <td><label class="form-label" for="input_field_of_studies">lang[studies_question]</label></td>
                        <td>#field_of_studies#</td>
		</tr>
		<tr #hide_begin_of_studies# #error_begin_of_studies#>
			<td><label class="form-label" for="input_begin_of_studies">lang[begin_of_studies]</label></td>
			<td>#begin_of_studies#</td>
		</tr>
		<tr #hide_study_level# #error_study_level#>
			<td><label class="form-label" for="input_study_level">lang[study_level_question]</label></td>
			<td>#study_level#</td>
		</tr>

		}
		{ #is_subpool_type_w#
			<tr>
				<td #error_activity_area#><label class="form-label" for="input_activity_area">lang[activity_area_question]</label></td>
				<td colspan="2">#activity_area#</td>
			</tr>
		}
		<tr>
			<td><label class="form-label" for="input_unwanted_pay_systems_intro">lang[unwanted_pay_systems_intro] lang[unwanted_pay_systems]</label></td>
			<td colspan="2">#unwanted_pay_systems#</td>
		</tr>


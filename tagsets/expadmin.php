<?php
// part of orsee. see orsee.org

// login form
function admin__login_form() {
	global $lang;
	echo '<form name="login" action="admin_login.php" method=post>
		<INPUT type=hidden name=redirect value="';
		if (isset($_REQUEST['redirect']) && $_REQUEST['redirect']) echo $_REQUEST['redirect'];
		elseif(!empty($_GET['document']) && preg_match("/^[a-z_]+\.[a-z]+$/",$_GET['document'])) {
			$redirectto=$GLOBALS['settings__admin_folder']."/".$_GET['document'];
			$other_vars=$_GET; unset($other_vars['document']);
			if(!empty($other_vars)) $redirectto.='?'.urlencode(http_build_query($other_vars));
			echo $redirectto;
		}
		else echo $GLOBALS['settings__admin_folder'].'/index.php';
	echo '">
		'.$lang['username'].':
		<input type=integer size=20 maxlength=20 name=adminname><BR>
		'.$lang['password'].':
		<input type=password size=20 maxlength=20 name=password><BR>
		<input type=submit name=login value="'.$lang['login'].'">
		</form>';  // onChange="gotoPassword()" // onChange="sendForm()"
}

// checks username and password
// if ok, redirect
function admin__check_login($username,$password) {
	global $lang;
        $query="SELECT * FROM ".table('admin')." 
                WHERE adminname='".mysqli_real_escape_string($GLOBALS['mysqli'],$username)."'"; // AND password='".mysqli_real_escape_string($GLOBALS['mysqli'],$password)."'"
        $result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
        $num_rows = mysqli_num_rows($result);
		// var_dump($num_rows);

	if ($num_rows==1) {
		$expadmindata=mysqli_fetch_assoc($result);
		if(!password_verify($password,$expadmindata["password"])) {
			$expadmindata=array();
			return false;
		}
		// load admin rights
		$expadmindata['rights']=admin__load_admin_rights($expadmindata['admin_type']);
		if ($expadmindata['rights']['login']) {
			$_SESSION['expadmindata']=$expadmindata;
			return true;
			} else {
			message($lang['error_not_allowed_to_login']);
			return false;
			}
		}
	   else {
		return false;
		}
}

function admin__load_names_by_id($ids,$realnames=true,$idname="admin_id") {
	if(!is_array($ids)) $ids=explode(",",$ids);
	$admins=array();
	foreach($ids as $id){
		$query="SELECT lname,fname,adminname,email FROM ".table("admin")." WHERE $idname='$id'";
		$res=orsee_query($query);
		if($res!==false) $admins[$id]=($realnames)?$res['fname'].' '.$res['lname']:$res['adminname'];
		// var_dump($admins,$id,$res);
		if($realnames===-1 && $res!==false) $admins[$id]=$res['email'];
	}
	return $admins;
}

function admin__load_name_by_val($ids,$links=false,$realnames=true,$idname="adminname") {
	if($links) {
		$admins=admin__load_names_by_id($ids,$realnames,$idname);
		$admin_ids=admin__load_names_by_id($ids,false);
		$emails=admin__load_names_by_id($ids,-1,$idname);
		$ret=array();
		foreach($admins as $k=>$admin) {
			$link=$realnames?'mailto:'.$emails[$k]:'admin_edit.php?admin_id='.array_search($k,$admin_ids);
			$ret[]='<a target=_blank href="'.$link.'"><small>'.$admin.'</small></a>';
		}
		return implode(", ",$ret);
	}
	return implode(", ",admin__load_names_by_id($ids,$realnames,$idname));
}


function admin__load_id_by_rights($rights,$and_or="AND",$idname="admin_id") {
	if(!is_array($rights)) $rights=explode(",",$rights);
	$query="SELECT $idname,admin_type FROM ".table("admin")."";
	$admins=orsee_query($query,"return_same");
	$ids=array();
	foreach($admins as $admin)
	{
		$admrights=admin__load_admin_rights($admin['admin_type']);
		$ok=($and_or=="AND");
		foreach ($rights as $right) {if($and_or=="AND") $ok=($ok && !empty($admrights[$right])); else $ok=($ok || !empty($admrights[$right]));}
		if($ok) $ids[]=$admin[$idname];
	}
	return $ids;
}

function admin__load_names_by_rights($rights,$and_or="AND",$realnames=true,$idname="adminname") {
	return admin__load_names_by_id(admin__load_id_by_rights($rights,$and_or="AND",$idname),$realnames,$idname);
}

function admin__load_admin_rights($admin_type) {
	$admin_type=orsee_db_load_array("admin_types",$admin_type,"type_name");
	$trights=explode(",",$admin_type['rights']);
	$rights=array();
	foreach ($trights as $right) $rights[$right]=true;
	$rights=update_admin_rights_with_new_fields($rights);
	return $rights;
}

function update_admin_rights_with_new_fields($rights) {
	global $default_new_rights;
	foreach($default_new_rights as $m) {
		if(isset($rights[$m['old']]) && $rights[$m['old']] && (!isset($rights[$m['new']]))) $rights[$m['new']]=true;
	}
	return $rights;
}

function check_allow($right,$redirect="",$allow_if_not_set=false) {
	global $expadmindata, $lang;
	// var_dump($expadmindata['rights']);
	if($allow_if_not_set) {
		global $system__admin_rights ;
		$notset=true;
		foreach ($system__admin_rights as $systemright) {
			$line=explode(":",$systemright);
			if($line[0]==$right) {
				$notset=false;
				break;
			}
		}
		// var_dump($right.":".($notset?"notset":""));
		if($notset) return true;
	}
	if (isset($expadmindata['rights'][$right]) && $expadmindata['rights'][$right]) return true;
	   else {
		if ($redirect) {
			message ($lang['error_not_authorized_to_access_this_function']);
			redirect($GLOBALS['settings__admin_folder']."/".$redirect);
			}
		return false;
		}
}


function admin__logout() {
	global $expadmindata;
	$expadmindata=array();
	$SESSION['expadmindata']=$expadmindata;
	session_destroy();
}


// Updating password for admin
function admin__set_password($password,$userid) {
	$query="UPDATE ".table('admin')." 
         	SET password='".mysqli_real_escape_string($GLOBALS['mysqli'],password_hash($password,PASSWORD_BCRYPT))."'
         	WHERE admin_id='".$userid."'";
	$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
}

// admin type selection list
function admin__select_admin_type($fieldname,$selected="") {
	$query="SELECT * from ".table('admin_types')."
		ORDER by type_name";
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
	echo '<SELECT name="'.$fieldname.'">';
	while ($line=mysqli_fetch_assoc($result)) {
		echo '<OPTION value="'.$line['type_name'].'"';
		if ($line['type_name']==$selected) echo ' SELECTED';
		echo '>'.$line['type_name'].'</OPTION>';
		}
	echo '</SELECT>';
}

?>

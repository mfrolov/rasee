<?php
// part of orsee. see orsee.org


function date__skip_months($count,$time=0) {
	if ($time==0) $time=time();
	$td=getdate($time);

	$tmonth=$td['mon']+$count;
	$newtimestamp=mktime(0,0,1,$tmonth,1,$td['year']);
	return $newtimestamp;
}

function date__skip_years($count,$time=0) {
	if ($time==0) $time=time();
	$td=getdate($time);

	$newyear=$td['year']+$count;
	$newtimestamp=mktime(0,0,1,1,1,$newyear);
	return $newtimestamp;
}

function date__year_start_time($time=0) {
        if ($time==0) $time=time();
        $td=getdate($time);
        $newtimestamp=mktime(0,0,1,1,1,$td['year']);
        return $newtimestamp;
}

function calendar__remember_sessions($alist) {
  global $calendar__session_data; 
  $calendar__session_data[]=$alist;
}

function calendar__days_in_month($month, $year)
{
// calculate number of days in a month
return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
}

function calendar__month_table_inner($time=0,$admin=false,$print=false,$limittoday=array()) {
	if ($time==0) $time=time();

	global $lang, $color, $settings;
	static $expcolor_count=0, $expcolors=array(), $lscolor_count=0, $lscolors=array();

  	$start_date=date__skip_months(0,$time);
  	$date=getdate($start_date);
  	$limit=calendar__days_in_month($date['mon'],$date['year']);
  	$first_day=$date['wday'];
  	if ($first_day==0) $first_day=7; $first_day=$first_day-1;
  	$day="01";
  	$month=helpers__pad_number($date['mon'],2);
  	$year=$date['year'];
 	$i=0;
  	$j=0;
	$month_names=explode(",",$lang['month_names']);
	
	$oneday=false;
	if(!empty($limittoday)) {$oneday=true;}

	// prepare day array
	$days=array();
	for ($i=1; $i<=$limit; $i++) {
		$days[$i]=array();
		}

	// get session times
	if ($admin && isset($color['calendar_admin_experiment_sessions']))
                $expcolors_poss=explode(",",$color['calendar_admin_experiment_sessions']);
        elseif ((!$admin) && isset($color['calendar_public_experiment_sessions']))
                $expcolors_poss=explode(",",$color['calendar_public_experiment_sessions']);
        else
                $expcolors_poss=explode(",",$color['calendar_experiment_sessions']);

	$expcolors_max=count($expcolors_poss)-1;

	$query="SELECT * FROM ".table('sessions').", ".table('experiments').", ".table('lang')."
        	WHERE ".table('sessions').".experiment_id=".table('experiments').".experiment_id
        	AND ".table('sessions').".laboratory_id=".table('lang').".content_name
        	AND ".table('lang').".content_type='laboratory'";
	if (!$admin && empty($_REQUEST['p'])) $query.=" AND ".table('experiments').".hide_in_cal='n' ";
	if (!$admin && !empty($_REQUEST['p'])) {
		$participant_id=url_cr_decode($_REQUEST['p']);
		$query.=" AND ( ".table('experiments').".hide_in_cal='n' OR '".$participant_id."' IN (SELECT participant_id FROM ".table('participate_at')." WHERE ".table('participate_at').".experiment_id=".table('experiments').".experiment_id))";
	}
	$query.=" AND session_start_year='".$year."'
                 AND session_start_month='".$date['mon']."'";
	// if($oneday) $query.=" AND session_start_day='".$limittoday['mday']."'";
    $query.=" ORDER BY session_start_day, session_start_hour, session_start_minute";
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
	while ($line=mysqli_fetch_assoc($result)) {
			$days[$line['session_start_day']][]=$line;
			if (!isset($expcolors[$line['experiment_id']])) {
				$expcolors[$line['experiment_id']]=$expcolors_poss[$expcolor_count];
				if ($expcolor_count==$expcolors_max) $expcolor_count=0; else $expcolor_count++;
				}
			}
			//var_dump($query);


        // get maintenance times
	$monthstring=$year.$month;
	$lscolors_poss=explode(",",$color['calendar_lab_space_reservation']);
	$lscolors_max=count($lscolors_poss)-1;

        $query="SELECT *, (space_start_year*100+space_start_month) as space_start,
			(space_stop_year*100+space_stop_month) as space_stop
		FROM ".table('lab_space').", ".table('lang')."
                WHERE ".table('lab_space').".laboratory_id=".table('lang').".content_name
                AND ".table('lang').".content_type='laboratory'
                HAVING space_start<='".$monthstring."'
                	AND space_stop>='".$monthstring."'
                ORDER BY space_start_day, space_start_hour, space_start_minute";
        $result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
        while ($line=mysqli_fetch_assoc($result)) {
		$maintstart = ($line['space_start']==$monthstring) ? $line['space_start_day'] : 1;
		$maintstop = ($line['space_stop']==$monthstring) ? $line['space_stop_day'] : $limit;
		if (!isset($lscolors[$line['space_id']])) {
			$lscolors[$line['space_id']]=$lscolors_poss[$lscolor_count];
			if ($lscolor_count==$lscolors_max) $lscolor_count=0; else $lscolor_count++;
			}
		for ($i=$maintstart; $i<=$maintstop; $i++) $days[$i][]=$line;
                }


	$colspan=7;
	if($oneday) $colspan=1;
	echo '
    		<tr>
      			<td bgcolor="'.$color['calendar_month_background'].'" colspan='.$colspan.' align=center>
        			<font color="'.$color['calendar_month_font'].'"><b>';
					if($oneday) echo helpers__pad_number($limittoday['mday'],2).' ';
					echo $month_names[$date['mon']-1].' '.$year;
				echo '</b></font>
      			</td>
    		</tr>
    		<tr valign=top>';

	$calendar__weekdays=explode(",",$lang['weekdays_abbr']);
	foreach ($calendar__weekdays as $wk=>$weekday) {
			if($oneday) {
				$wday_to_wk=(($limittoday["wday"]-1)%7);
				if($wday_to_wk<0) $wday_to_wk=6;
			}
      		if(!$oneday || $wk==$wday_to_wk) echo '<td align=right> <b>'.$weekday.'</b> </td>';
	}
	echo '</tr>
    		<tr valign=top>';
	$i=0;
	// write empty fields in days array
  	while ($i<$first_day) {
    		if(!$oneday) echo "<td>&nbsp;</td>";
    		$i++; $j++;
  		}


	// write day numbers
  	$i=0; $pointer=0;
  	while ($i<$limit) {
          	if (!$oneday && ($j % 7)==0) {
           		echo "</tr>
                		<tr valign=top>";
           		}
        	$i++; $j++;
			$today=getdate();
			$daybgcolor=$color['calendar_day_background'];
			if($today['year']==$date['year'] && $today["mon"]==$date["mon"] && $today["mday"]==$i) {
				if(!empty($color['calendar_day_background_today'])) $daybgcolor=$color['calendar_day_background_today'];
				else $daybgcolor='pink';
			}
			
			if($oneday && !($limittoday['year']==$date['year'] && $limittoday["mon"]==$date["mon"] && $limittoday["mday"]==$i)) continue;
        	echo '<td height=50>
                	<table border=0 width=100%>';
					echo '
						<TR><TD align=right bgcolor="'.$daybgcolor.'">';
						 if(!$oneday) {
							 $ctime=mktime(0,0,1,$date['mon'],$i,$date['year']);
							 //var_dump($ctime);
							 if($admin && !$print)echo '<a href="'.$_SERVER['SCRIPT_NAME'].'?time='.$ctime.'&oneday=true" style="color:'.$color['body_text'].'">';
							 echo helpers__pad_number($i,2); 
							 if($admin && !$print)echo '</a>';
						 }
						 if($oneday) echo '&nbsp;';
					echo '</TD></TR>';
			
			$n_different_exp=0; $prev_exp_id="";
        	foreach ($days[$i] as $entry) {
				if (isset($entry['session_start_day'])) {
					if (!$admin && !empty($_REQUEST['p'])) {
						// $participant_id=url_cr_decode($_REQUEST['p']);
						if($entry['hide_in_cal']=='y') {
							$exp_sess_history=expregister__list_history($participant_id,"exp_sess_only");
							$registered_session_finished=false;
							foreach($exp_sess_history as $elem) {if($elem[0]==$entry['experiment_id'] && $elem[2]=='y') $registered_session_finished=true;}
							if($entry['session_finished']=='y' || $registered_session_finished) {
								// $exp_sess_registered=expregister__list_registered_for($participant_id,"","exp_sess_only");
								$registered_for_this_session=false;
								// var_dump($exp_sess_history);
								foreach($exp_sess_history as $elem) {if($elem[0]==$entry['experiment_id'] && $elem[1]==$entry['session_id']) $registered_for_this_session=true;}
								if(!$registered_for_this_session) continue;
							}

						}
						$sess_remark_list=sessions__get_remark_list($entry['session_id']);
						if(!empty(preg_grep("/^hidden(:[1y])?\s*$/",$sess_remark_list)) || in_array('!hidden!',$sess_remark_list)) {
							$exp_sess_registered=expregister__list_registered_for($participant_id,"","exp_sess_only");
							$registered_for_this_session=false;
							foreach($exp_sess_registered as $elem) {if($elem[0]==$entry['experiment_id'] && $elem[1]==$entry['session_id']) $registered_for_this_session=true;}
							if(!$registered_for_this_session) continue;
						}
					}
					if($entry['experiment_id']!= $prev_exp_id) {$n_different_exp++; $prev_exp_id=$entry['experiment_id'];}
					echo '<TR><TD  style="background-clip: padding-box;border-radius: 10px;border: 1px solid white; padding:2px; background-color:'.$expcolors[$entry['experiment_id']].'">'; // bgcolor="'.$expcolors[$entry['experiment_id']].'"
					$start_time=time__get_timepack_from_pack($entry,"session_start_");
					$duration=time__get_timepack_from_pack($entry,"session_duration_");
					$end_time=time__add_packages($start_time,$duration);

					echo time__format($lang['lang'],$start_time,true,false,true,true).'-'.
													time__format($lang['lang'],$end_time,true,false,true,true);

						echo '<BR><FONT class="small">'.
							laboratories__strip_lab_name(stripslashes($entry[$lang['lang']])).'</FONT>';
					if ($admin || (!isset($settings['public_calendar_hide_exp_name'])) ||
							$settings['public_calendar_hide_exp_name']!='y') {
						echo '<BR>';  
							if ((!$print) && $admin && check_allow('experiment_show')) 
										echo '<A HREF="experiment_show.php?experiment_id='.
								$entry['experiment_id'].'">';
							echo '<FONT color="'.$color['calendar_experiment_name'].'">';
							if ($admin) echo $entry['experiment_name'];
									else echo $entry['experiment_public_name'];
							echo '</FONT>';
							if ((!$print) && $admin && check_allow('experiment_show')) echo '</A>';
					}


						if ($admin) echo '<BR><FONT class="small">'.str_replace(",",", ",$entry['experimenter']).'</FONT>';

						$cs__reg=experiment__count_participate_at($entry['experiment_id'],$entry['session_id']);

						if ($cs__reg<$entry['part_needed']) $cs__status="not_enough_participants";
						elseif ($cs__reg<$entry['part_needed']+$entry['part_reserve']) $cs__status="not_enough_reserve";
						else $cs__status="complete";
						// var_dump($cs__status);
						if (!$admin && !empty($_REQUEST['p'])) {
							$entry['participant_id']=$participant_id;
							if(sessions__session_full("-",$entry)) $cs__status="complete";
							$unfinished_reservation=false;
							$unfinished_reservation_my_session=false;
							$reservation_minutes=sessions__get_remark_list_elem_by_name($entry['session_id'],"reservation_minutes");
							if(!empty($reservation_minutes) && is_numeric($reservation_minutes) && $reservation_minutes>0) {
								$crf="SELECT id FROM ".table('participants_log')." WHERE action='booking' AND target LIKE '".mysqli_real_escape_string($GLOBALS['mysqli'],"experiment_id:".$entry['experiment_id']."\n%")."' ORDER BY log_id";
								$pidscrf=orsee_query($crf,"return_first_elem");
								$mycrfindex=-1;
								if($pidscrf!==false) foreach(array_values(array_unique($pidscrf)) as $pk=>$cpid) if($cpid==$participant_id) $mycrfindex=$pk;
								if($mycrfindex>=0) $unfinished_reservation=true;
								$crfcs="SELECT count(*) FROM ".table('participants_log')." WHERE action='booking' AND id='".$participant_id."' AND target='".mysqli_real_escape_string($GLOBALS['mysqli'],"experiment_id:".$entry['experiment_id']."\nsession_id:".$entry['session_id'])."'";
								$cscrf=orsee_query($crfcs,"return_first_elem");
								// var_dump($crfcs,$cscrf);
								if($cscrf!==false && $cscrf[0]>0) $unfinished_reservation_my_session=true;
								// else $unfinished_reservation_my_session=false;
							}
						}

						echo '<BR>';

						if ($admin) {
							$color_varname="session_".$cs__status;
							echo '<FONT color="'.$color[$color_varname].'">'.$cs__reg.
							' ('.$entry['part_needed'].','.$entry['part_reserve'].')</FONT>';
							if ($entry['session_finished']=="y")
								echo ' <font style="white-space:nowrap;font-size:85%" color="'.$color['session_finished'].'">
									- '.$lang['session_finished'].'</font>';
						}

						$reg_end=sessions__get_registration_end($entry);
						$register_after_start_val=sessions__get_remark_list_elem_by_name($entry['session_id'],"register_after_start");
						$register_after_start=!empty($register_after_start_val) && $register_after_start_val!='n';
						if($register_after_start) { $sessions_endtime=sessions__get_session_end_time($entry); if(time()>=$sessions_endtime) $register_after_start=false; }

						if (time()>$reg_end ) $cs__status="expired"; // && !$register_after_start - already in sessions__get_registration_end()
						// var_dump($cs__status,time(),$reg_end,time()>$reg_end);
						if (!($admin)) {
							switch ($cs__status) {
									case "not_enough_participants":
									case "not_enough_reserve":
														$text=$lang['free_places'];
														$thcolor=$color['session_public_free_places'];
														break;
									case "complete":
														$text=$lang['complete'];
														$thcolor=$color['session_public_complete'];
														break;
									case "expired":
														$text=$lang['expired'];
														$thcolor=$color['session_public_expired'];
														break;
								}
						$textinnit=$text;
						if(empty($_REQUEST['p']) /*|| $textinnit!=$lang['free_places']*/) echo '<FONT color="'.$thcolor.'">'.$text.'</FONT>';
							else {
								global $settings;
								$public_lang=$settings['public_standard_language'];
								$exp_sess_history=expregister__list_history($participant_id,"exp_sess_only");
								$exp_sess_registered=expregister__list_registered_for($participant_id,"","exp_sess_only");
								foreach($exp_sess_registered as $elem) {if($elem[0]==$entry['experiment_id']) $text='-'; if($elem[1]==$entry['session_id']) $text='<span style="color:blue">'.$lang['registered'].'</span>';}
								foreach($exp_sess_history as $elem) {if($elem[0]==$entry['experiment_id'] && $text==$lang['free_places']) {$text='-'; if($public_lang=="fr") $text='<small style="color:gray">d&eacute;j&agrave; particip&eacute;(e)</small>'; }}
								if($textinnit==$lang['complete'] && str_replace($lang['registered'],"",$text)==$text) $text=$textinnit;
								if($entry['session_finished']=='y') $text="<span style='color:gray'>".$lang['finished']."</span>";
								if($text!=$lang['complete'] && $text!='-') echo '<A style="color:'.$thcolor.'" HREF="participant_show.php?p='.$_REQUEST['p'].'">'.$text.'</A>';
								else echo '<span style="color:'.$thcolor.'">'.$text.'</span>';
								$sessid_buttons=expregister__list_invited_for(url_cr_decode($_REQUEST['p']),"expregister__list_session_register_buttons");
								// var_dump($sessid_buttons); // echo "\n <!-- "; echo "sessid_buttons: \n"; print_r($sessid_buttons); echo " entry_session:"; echo $entry['session_id']; echo "\n --> ";
								// var_dump($cs__status);
								foreach($sessid_buttons as $elem) if($elem[0]==$entry['session_id']) {
									if(empty($unfinished_reservation)) echo $elem[1];
									elseif(!empty($unfinished_reservation_my_session)) {
										$reservation_link=sessions__get_remark_list_elem_by_name($entry['session_id'],"reservation_link");
										if(!empty($reservation_link) && $unfinished_reservation_my_session) {
											$participate_at_array=rasee_db_load_array("participate_at",array("participant_id"=>$participant_id,"experiment_id"=>$entry['experiment_id']));
											$participant_combined=rasee_db_load_array("participants",array("participant_id"=>$participant_id))+$participate_at_array;
											//var_dump($reservation_link);
											foreach($participant_combined as $pck=>$pcv) {
												// var_dump($reservation_link,$pck,$pcv);
												if(is_null($pcv)) $pcv="";
												$reservation_link=str_ireplace("#".$pck."#",$pcv,$reservation_link);
											}
											echo "<br><a class='small button' href='$reservation_link'>".lang("questionnaire")."</a>";
											// $reservation_link_shown=1;
										}
									}
								}
							}
						}

						if ((!$print) && $admin) {
								if(check_allow('experiment_show_participants')) echo '<BR><A class="small" HREF="experiment_participants_show.php?experiment_id='.
									$entry['experiment_id'].'&focus=registered&session_id='.$entry['session_id'].
									'">['.$lang['participants'].']</A>';
								if(check_allow('session_edit')) echo ' <A class="small" HREF="session_edit.php?session_id='.
									$entry['session_id'].
									'">['.$lang['edit'].']</A>';
								}
						echo '<BR><BR>
								</TD></TR>';
				}
				else {
					if ($admin) {
						echo '<TR><TD bgcolor="'.$lscolors[$entry['space_id']].'">';
						if ($entry['space_start_day']==$i) {
							$from['hour']=$entry['space_start_hour'];
							$from['minute']=$entry['space_start_minute'];
							}
						   else {
							$from['hour']=$settings['laboratory_opening_time_hour'];
							$from['minute']=$settings['laboratory_opening_time_minute'];
							}
						if ($entry['space_stop_day']==$i) { 
													$to['hour']=$entry['space_stop_hour'];
													$to['minute']=$entry['space_stop_minute'];
							}
											   else {
							$to['hour']=$settings['laboratory_closing_time_hour'];
													$to['minute']=$settings['laboratory_closing_time_minute'];
							}
						echo time__format($lang['lang'],$from,true,false,true,true).'-'.
							time__format($lang['lang'],$to,true,false,true,true);
						echo '<BR><FONT class="small">'.
												laboratories__strip_lab_name(stripslashes($entry[$lang['lang']])).'</FONT><BR>';
						echo $entry['reason'].'<BR><FONT class="small">'.str_replace(",",", ",$entry['experimenter']).'</FONT><BR>';
						if (check_allow('lab_space_edit'))
							echo '<A HREF="lab_space_edit.php?space_id='.$entry['space_id'].'">'.
								'<FONT class="small" align=right>'.$lang['edit'].'</FONT></A><BR>';
						echo '<BR></TD></TR>';
						}

				}
        		$pointer++;
        	}
				if($admin && $n_different_exp>0 && check_allow('experiment_show_participants') )
				{
					echo '<TR><TD bgcolor="" style="text-align:center">';
					$explink=($n_different_exp>1)?'':'experiment_id='.$prev_exp_id;
					echo '<A class="small" HREF="experiment_participants_show.php?'.$explink.'
					&focus=atdate:'.$i.'/'.$date['mon'].'/'.$date['year'].'
                				">['.lang('all_subjects',true,false,true).']</A>';
					echo ' &nbsp;&nbsp;';
					echo '<A class="small" HREF="experiment_participants_show.php?'.$explink.'
					&focus=absentatdate:'.$i.'/'.$date['mon'].'/'.$date['year'].'
                				">['.lang('all_absent_subjects',true,false,true).']</A>';
					
        			echo '<BR>
                			</TD></TR>';
				}
        	echo '</table>
                	</td>';
  		}

  	$i=$i+$first_day;
  	while (($i % 7)!=0) {
    		if(!$oneday) echo '<td>&nbsp;</td>';
    		$i++;
  		}
  	echo '</tr>';
}



function calendar__month_table($time=0,$forward=0,$admin=false,$print=false) {
	if ($time==0) $time=time();
	$i=0;
	echo '<table border=1 width="80%">';
	while ($i< $forward+1) {
		calendar__month_table_inner($time,$admin,$print);
		if ($forward>0) {
			echo '<TR><TD colspan=7 height=50>&nbsp;</TD></TR>';
			}
		$time=date__skip_months(1,$time);
		$i++;
		}
	echo '</table>';
}

function calendar__day_table($time=0,$admin=false,$print=false) {
	if ($time==0) $time=time();
	$i=0;
	echo '<table border=1 width="80%">';
	calendar__month_table_inner($time,$admin,$print,getdate($time));
	echo '</table>';
}


function calendar__show_year($time=0,$admin=false,$print=false) {
if ($time==0) $time=time();
$yearstarttime=date__year_start_time($time);
calendar__month_table($yearstarttime,11,$admin,$print);
}

?>

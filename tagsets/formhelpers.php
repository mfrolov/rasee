<?php
// part of orsee. see orsee.org


// select field for numbers from begin to end by steps
function helpers__select_numbers($name,$prevalue,$begin,$end,$fillzeros=2,$steps=1,$none=false) {

	$i=$begin;
	echo '<select name="'.$name.'">';
	if ($none) echo '<option value="">-</option>';
	while ($i<=$end) {
		echo '<option value="'.$i.'"';
		if ($i == (int) $prevalue) echo ' SELECTED';
                echo '>';
		echo helpers__pad_number($i,$fillzeros);
		echo '</option>
			';
		$i=$i+$steps;
		}
	echo '</select>';
}

function helpers__select_number($name,$prevalue,$begin,$end,$fillzeros=2,$steps=1,$none=false) {
	$out='';
	$i=$begin;
	$out.='<select name="'.$name.'">';
	if ($none) $out.='<option value="">-</option>';
	while ($i<=$end) {
		$out.='<option value="'.$i.'"';
		if ($i == (int) $prevalue) $out.=' SELECTED';
        $out.='>';
		$out.=helpers__pad_number($i,$fillzeros);
		$out.='</option>
			';
		$i=$i+$steps;
		}
	$out.='</select>';
	return $out;
}


// select field for text array
function helpers__select_text($tarray,$name,$prevalue,$none=false) {
	global $lang;
        echo '<select name="'.$name.'">';
        if ($none) echo $tarray=array(""=>"-")+$tarray;//'<option value=""></option>';
        foreach ($tarray as $k=>$text) {
                echo '<option value="'.$k.'"';
                if ($k == $prevalue) echo ' SELECTED';
                echo '>';
                if (isset($lang[$text])) echo $lang[$text];
				else echo $text;
                echo '</option>
                        ';
                }
        echo '</select>';
}

function helpers__select_text_out($tarray,$name,$prevalue,$none=false,$disabled=false,$supselect="") {
		$out="";
        $out.= '<select ';
		if(true || empty($GLOBALS['expadmindata']['adminname'])) $out.='class="form-select" ';
		$out.= ''.$supselect.' name="'.$name.'" id="input_'.$name.'"';
		if($disabled) $out.=' DISABLED';
		$out.='>';
        if ($none) $tarray=array(""=>"-")+$tarray;//'<option value=""></option>';
        foreach ($tarray as $k=>$text) {
                $out.= '<option value="'.$k.'"';
                if ($k == $prevalue) $out.= ' SELECTED';
                $out.= '>'; //echo "<hr>text=$text<hr>";
                $out.= lang($text);
                $out.= '</option>
                        ';
                }
        $out.= '</select>';
		return $out;
}


// select field for values for reminder time and registration end
function helpers__select_numbers_relative($name,$prevalue,$begin,$end,$fillzeros=2,$steps=1,$current_time=0,$asso=array()) {
	global $authdata;
	$i=$begin;
	echo '<select name="'.$name.'">';
	while ($i <= $end) {
		echo '<option value="'.$i.'"';
		if ($i== (int) $prevalue) echo ' SELECTED';
                echo '>';
		if(array_key_exists($i,$asso)) echo $asso[$i]; else {
			echo helpers__pad_number($i,$fillzeros); 
			if ($current_time > 0) {
				$utime=$current_time - ($i * 60 * 60); 
				echo ' ('.time__format($authdata['language'],"",false,false,true,true,$utime).')';
				}
		}
		echo '</option>
			';
		$i=$i+$steps;
		}
	echo '</select>';
}

function updown_control($funcname,$elemname)
{
		return
		'<table cellpadding="0" cellspacing="0" border="0" style="display:inline">
		<tr>
		<td><input type="button" value=" /\ " onclick="'.$funcname.'(this.form.'.$elemname.',1)" style="font-size:7px;margin:0;padding:0;width:20px;height:13px;" ></td>
		</tr>
		<tr>
		<td><input type=button value=" \/ " onclick="'.$funcname.'(this.form.'.$elemname.',-1)" style="font-size:7px;margin:0;padding:0;width:20px;height:12px;" ></td>
		</tr>
		</table>';
}

function helpers__select_pay_accounts($name,$dossier='',$prevalue="",$disabled=false,$supselect="",$include_empty=true) {

	return helpers__select_text_out(helpers_pay_accounts($dossier,$include_empty),$name,$prevalue,false,$disabled,$supselect);
}


function helpers__select_payment_file($experiment_id='',$session_id='',$elemname='dossier',$restrict_to_session=false) {
	global $site__database_database;
	// var_dump($array);

	// find out which fields i can save
	$query="SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS 
			WHERE table_name='".table("payment_files")."' 
			AND table_schema = '".$site__database_database."'";
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$query);
	if(mysqli_num_rows($result)==0) {echo lang("no_payment_files"); return false;}
	mysqli_free_result($result);
	$out='';
	$query="SELECT id FROM ".table("payment_files")." WHERE closed=0 ORDER BY year DESC, number DESC";
	$lines=orsee_query($query,"return_same");
	$choosed_value="0";
	$restricted_files=array();
	if($session_id!='' || $experiment_id!=''){
		foreach($lines as $kl=>$vl) if($vl['id']=='1') {unset($lines[$kl]); break;}
		$line=array();
		if($session_id!='') $query="SELECT payment_file FROM ".table("sessions")." WHERE session_id=$session_id";
		$line=orsee_query($query);
		if(isset($line['payment_file'])) {$choosed_value=$line['payment_file']; if($restrict_to_session && !empty($choosed_value)) $restricted_files[]=$choosed_value;}
		if((empty($choosed_value) || $restrict_to_session) &&  $experiment_id!='') {
			$query="SELECT payment_file FROM ".table("experiments")." WHERE experiment_id=$experiment_id";
			$line=orsee_query($query);
			if(isset($line['payment_file'])) {if(empty($choosed_value)) $choosed_value=$line['payment_file']; if($restrict_to_session && !empty($line['payment_file'])) $restricted_files[]=$line['payment_file'];}
		}
		if(!empty($choosed_value)) {
			$lines_contain_choosen=false;
			foreach($lines as $l) {if($l['id']==$choosed_value) {$lines_contain_choosen=true; break;}}
			if(!$lines_contain_choosen) $lines[]=array("id"=>$choosed_value);
		}
	}
	$out.= '
		<select name='.$elemname.'>
			';
	array_unshift($lines, array("id"=>"0"));
	// var_dump($restricted_files,$lines); exit;
	if($restrict_to_session && count($restricted_files)==0) return false;
	// var_dump($restricted_files);
	foreach($lines as $l) {
		if($restrict_to_session && $experiment_id && $session_id && !in_array($l['id'],$restricted_files)) continue;
		$out.= '<option value="'.$l['id'].'"';
		if($l['id']==$choosed_value) $out.= ' SELECTED';
		$out.= '>'.dossier_name($l['id'],1);
	}
	$out.= '</select>';
	return $out;
}



function experiment_ext_types__checkboxes($postvarname,$showvar,$checked="",$var="") {
	if (!$checked) $checked=array();
	if (!$var) $var="exptype_name";

        $query="SELECT *
                FROM ".table('experiment_types')." as texpt, ".table('lang')." as tlang
                WHERE texpt.exptype_id=tlang.content_name
                AND tlang.content_type='experiment_type'
		AND texpt.enabled='y' 
                ORDER BY exptype_id";

        $result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
        while ($line = mysqli_fetch_assoc($result)) {
                echo '<INPUT class="form-check-input" type="checkbox" id="'.$postvarname.'_'.$line[$var].'" name="'.$postvarname.'['.$line[$var].']" 
					value="'.$line[$var].'"';
                if (isset($checked[$line[$var]]) && $checked[$line[$var]]) echo " CHECKED";
                echo '><label class="form-check-label" for="'.$postvarname.'_'.$line[$var].'" >'.$line[$showvar];
                echo '</label><BR>
                                ';
                }

}

function participant__subscription_checkboxes($postvarname,$subpool_id=-1,$checked="") {
	global $settings, $lang;

        if (!$checked) $checked=array();
	if ($subpool_id==-1) $subpool_id=$settings['subpool_default_registration_id'];

        $query="SELECT *
                FROM ".table('experiment_types')." as texpt, ".table('lang')." as tlang, ".table('subpools')." as tsub
                WHERE texpt.exptype_id=tlang.content_name
                AND tlang.content_type='experiment_type'
                AND texpt.enabled='y'
		AND tsub.experiment_types LIKE concat('%',texpt.exptype_name ,'%') 
		AND tsub.subpool_id='".$subpool_id."' 
                ORDER BY exptype_id";
        $result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
        while ($line = mysqli_fetch_assoc($result)) {
                echo '<INPUT class="form-check-input" type="checkbox" id="'.$postvarname.'_'.$line['exptype_name'].'" name="'.$postvarname.'['.$line['exptype_name'].']"
                                        value="'.$line['exptype_name'].'"';
                if (isset($checked[$line['exptype_name']]) && $checked[$line['exptype_name']]) echo " CHECKED";
                echo '><label class="form-check-label" for="'.$postvarname.'_'.$line['exptype_name'].'" >'.$line[$lang['lang']];
                echo '</lable><BR>
                                ';
                }

}


// select field for sessions
function select__sessions($preval,$varname,$exp_id,$hide_nosession,$supsess_criteria="",$supselectoptions="",$startwithdash=false,$restrictionsbycheckbox=false) {
	global $lang, $expadmindata;
	// var_dump($supsess_criteria);

	if (!$preval) $preval=$startwithdash?-1:0; 
	if (!$varname) $varname="session";
	if (!empty($exp_id)) {  
			$query="SELECT *
                    FROM ".table('sessions')."
					WHERE experiment_id='".$exp_id."'
					OR session_id=0
                    ORDER BY session_start_year, session_start_month, session_start_day, 
                    session_start_hour, session_start_minute";
			$with_exp=false;
			}
		else {
			$query="SELECT *
                    FROM ".table('sessions').", ".table('experiments')."
                    WHERE ".table('sessions').".experiment_id=".table('experiments').".experiment_id
					";
					if(empty($supsess_criteria)) $query.="AND ".table('sessions').".session_finished='n'
					";
					else $query.=$supsess_criteria;
					$query.="
                    ORDER BY session_start_year, session_start_month, session_start_day, 
                    session_start_hour, session_start_minute";
			$with_exp=true;
			}

	$out='';
    $out.='<SELECT '.$supselectoptions.' name="'.$varname.'" id="select'.$varname.'">';
	if ($startwithdash) {
		$out.='<OPTION value="-1"';
		if ($preval==-1) $out.=" SELECTED";
		$out.='>--</OPTION>';
	}
	if ($with_exp && !$hide_nosession) {
		$out.='<OPTION value="0"';
		if ($preval==0) $out.=" SELECTED";
		$out.='>'.$lang['no_session'].'</OPTION>';
	}
	if ($restrictionsbycheckbox) {
		$out.='<OPTION value="-2"';
		if ($preval==-2) $out.=" SELECTED";
		$out.='>-'.lang('check_to_restrict').'-</OPTION>';
	}

	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

	while ($line = mysqli_fetch_assoc($result)) {
    	$out.='<OPTION value="'.$line['session_id'].'"';
        	if ($preval==$line['session_id']) $out.=" SELECTED";
		$out.='>';
		// var_dump($preval,$line['session_id']);
		if ($line['session_id']==0) {
			$out.=$lang['no_session'];
		} else {
			if ($with_exp) $out.=$line['experiment_name'].' - ';
				$tarr=array('day'=>$line['session_start_day'],
						'month'=>$line['session_start_month'],
						'year'=>$line['session_start_year'],
						'hour'=>$line['session_start_hour'],
						'minute'=>$line['session_start_minute']);
				$out.=time__format($expadmindata['language'],$tarr,false,false,true,false);
			}
        	$out.='</OPTION>';
		}
        $out.='</SELECT>';
        return $out;
}

function headcell($value,$sort="",$focus="",$supvals="") {
	global $color;
	if (!isset($_REQUEST['focus'])) $_REQUEST['focus']="";
	if (!isset($_REQUEST['experiment_id'])) $_REQUEST['experiment_id']="";
	if (!isset($_REQUEST['session_id'])) $_REQUEST['session_id']="";
	echo '
        <TD class=small';
	if ($_REQUEST['focus']==$focus && $focus) echo ' bgcolor="'.$color['list_highlighted_table_head_background'].'"';
	echo '>';
        if ($sort) {
		echo '<A HREF="'.thisdoc().'?sort='.urlencode($sort).'&show=true';
		if ($_REQUEST['experiment_id']) echo '&experiment_id='.$_REQUEST['experiment_id'];
		if ($_REQUEST['session_id']) echo '&session_id='.$_REQUEST['session_id'];
		if ($_REQUEST['focus']) echo '&focus='.$_REQUEST['focus'];
		echo $supvals;
		echo '">';
		}
	echo '<FONT class="small"';
	if ($_REQUEST['focus']==$focus && $focus) echo ' color="'.$color['list_highlighted_table_head_text'].'"';
	echo '>';
        echo $value;
	echo '</FONT>';
        if ($sort) echo '</A>';
        echo '</TD>';
}

function strip_tags_content($text, $tags = '', $invert = FALSE) {

  preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags);
  $tags = array_unique($tags[1]);
   
  if(is_array($tags) AND count($tags) > 0) {
    if($invert == FALSE) {
      return preg_replace('@<(?!(?:'. implode('|', $tags) .')\b)(\w+)\b.*?>.*?</\1>@si', '', $text);
    }
    else {
      return preg_replace('@<('. implode('|', $tags) .')\b.*?>.*?</\1>@si', '', $text);
    }
  }
  elseif($invert == FALSE) {
    return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
  }
  return $text;
}


function helpers__select_date($name,$prevalue='') {

		echo '<link rel="stylesheet" href="../jcryption/jquery-ui.min.css">
		  <script src="../jcryption/jquery-ui.min.js"></script>
		  ';
		$dpreg='';
		if(lang('lang')=='fr') {
			echo '
			  <script src="../jcryption/datepicker-fr.js"></script>
			  ';
			$dpreg='fr';
		}
		echo '
		  <script>
		  $( function() {
			$.datepicker.setDefaults( $.datepicker.regional[ "'.$dpreg.'" ] );
			$( ".datepicker" ).datepicker({
			  dateFormat: "yy-mm-dd",
			  onSelect: function() {}
			});
		  } );
		  
		  </script>
		
		<input type="text" id="datepicker_'.$name.'" name="'.$name.'" class="datepicker" value="'.$prevalue.'">
		';
}

?>

<?php
// part of rasee. See http://leep.univ-paris1.fr/outils/telecharger.php?file=rasee

class operation
{
	protected $_debit="";
	protected $_credit="";
	protected $_tables;
	protected $_time;
	protected $_admname="";
	protected $_dossier="";
	protected $_data=array();
	public $amounts=array();
	public $dossier=0;
	public $remarks="";
	public $instead_of="";
	public $id="";
	public static $n_mix_accounts=1;
	
	function __construct($deb, $cred, $file="0")
	{
		global $expadmindata;
		$this->_debit=$deb;
		$this->_credit=$cred;
		$this->_dossier=$file;
		$this->_admname=$expadmindata['adminname'];
		$this->_time=time();
		operation::operation_tables();
	}
	
	function add($type,$nominal,$n_nominals)
	{
		$this->data[]=array("unit"=>$type,"nominal"=>$nominal,"n_units"=>$n_nominals,"amount"=>$nominal*$n_nominals);
	}
	
	function total_amount()
	{
		$res=0;
		foreach($this->data as $td) $res+=$td["amount"];
		return $res;
	}
	
	function make()
	{
		$first=true;
		if(empty($this->id)) $this->id=$this->get_new_id();
		$this->_time=time();
		foreach($this->data as $i=>$d)
		{
			$q="INSERT INTO ".table("pay_operations")." SET id='".$this->id."', datetime='".$this->_time."', admin='".$this->_admname."'";
			$q.=", debit_account='".$this->_debit."', credit_account='".$this->_credit."', payment_file='".$this->_dossier."'";
			if(!empty($this->instead_of)) $q.=", instead_of_file='".$this->instead_of."'";
			foreach($d as $k=>$val) $q.=", $k='$val'";
			if($first) {
				if(!empty($this->remarks)) $q.=", remarks='".$this->remarks."'";
			}
			$first=false;
			// echo $q; exit;
			$result=mysqli_query($GLOBALS['mysqli'],$q) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$q);
		}
		$this->update_accounts();
	}
	
	function update_accounts()
	{
		$accounts=array("debit"=>$this->_debit,"credit"=>$this->_credit);
		$payment_present=false; $cash_deleted=false;
		$regidavance_positive_budget=(helpers__regiedavance_budget()>0 || helpers__regiedavance_amount_left()>0);
		$labsafe_line=orsee_query("SELECT SUM(amount) as labsafe_amount FROM ".table("pay_accounts")." WHERE shortname='lab' AND payment_file='".$this->_dossier."'");
		$am_labsafe_dossier=0; if($labsafe_line!==false && !empty($labsafe_line['labsafe_amount'])) $am_labsafe_dossier=$labsafe_line['labsafe_amount'];
		$totamount=$this->total_amount();
		foreach($accounts as $t=>$an) {
			$cdossier=$this->_dossier;
			if($an=="lab" && $regidavance_positive_budget) {
				if($am_labsafe_dossier==0 || ($t=="debit" && $am_labsafe_dossier<$totamount) || ($t=="credit" && ($am_labsafe_dossier>0 || $totamount>abs($am_labsafe_dossier))))
				{
					$cdossier=1;
				}
			}
			$wherebase=" WHERE shortname='$an' AND payment_file='".$cdossier."'";
			// $af_new=(orsee_query("SELECT * FROM ".table("pay_accounts").$wherebase)===false);
			$dumquery="";
			foreach($this->data as $i=>$d) {
				$type=($d["unit"]=="cash" || $d["unit"]=="nominal")?"cash":"noncash";
				$unit=$d["unit"];
				$typearr=explode("_",$d["unit"]);
				$basetype=array_shift($typearr);
				if($basetype=="payment") {
					$sectype=implode("_",$typearr);
					// $type=$basetype.'_'.(($sectype=="cash" || $sectype=="nominal")?"cash":"noncash");
					$type=($sectype=="cash" || $sectype=="nominal")?"cash":"noncash";
					// $unit=$sectype;
					$payment_present=true;
				}
				// if($an=="participants") continue;
				// if($an=="bank") continue;
				$where=$wherebase." AND type='$type' AND unit='$unit' AND nominal='".$d["nominal"]."'";//$this->setimplode(" AND ",$d,array("unit","nominal"));
				// $line=false;
				// if(!$af_new) {
				$query="SELECT * FROM ".table("pay_accounts").$where;
				$line=orsee_query($query);
				//if(true) {var_dump($line,$query,$type,$unit,$basetype,$i); echo "<hr>"; if($i==count($this->data)-1) exit;}
				// }
				$new=($line===false);
				$prev=array(); $newv=array();
				$setvals=array("n_units","amount");
				foreach($d as $k=>$val) if(in_array($k,$setvals)) {$prev[$k]=($new)?0:$line[$k]; $newv[$k]=($t=="debit")?$prev[$k]-$val:$prev[$k]+$val;}
				if($new) foreach($d as $k=>$val) if($k=="nominal") $newv[$k]=$val;
				$q=($new)?"INSERT INTO ":"UPDATE ";
				$q.=table("pay_accounts")." SET ";
				if($new) {
					//if($type=="cash" && (lang('money_symbol')!="")) $unit=lang('money_symbol'); //should be same as unit in pay_operations table which is "cash" for cash values
					$longname=helpers__pay_account_longname($an);
					$q.="type='$type', unit='$unit', shortname='$an', name='$longname', payment_file='".$cdossier."'";
					$q.=", ";
				}
				$q.=$this->setimplode(", ",$newv);
				if(!$new) {
					if($newv["amount"]==0) $q="DELETE FROM ".table("pay_accounts");
					$q.=$where;
				}
				$result=mysqli_query($GLOBALS['mysqli'],$q) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$q);
				// $dumquery.=$q."<hr>";
			}
				// var_dump($dumquery); exit
			$check_delete_types=array("cash","noncash");
			foreach($check_delete_types as $cdt) {
				$doesnt_exist=(orsee_query("SELECT * FROM ".table("pay_accounts").$wherebase." AND type='$cdt'")===false);
				if(!$doesnt_exist) {
					$line=orsee_query("SELECT SUM(amount) as somme FROM ".table("pay_accounts").$wherebase. " AND type='$cdt'");
					if($line["somme"]==0) {
						$q="DELETE FROM ".table("pay_accounts").$wherebase. " AND type='$cdt'";
						$result=mysqli_query($GLOBALS['mysqli'],$q) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$q);
						$cash_deleted=true;
					}
				}
			}
		}
		if(!$cash_deleted && $payment_present)
		{
			//ajust cash nominals
			$an=$this->_debit;
			$cdossier=$this->_dossier;
			if($an=="lab" && $regidavance_positive_budget) {
				if($am_labsafe_dossier==0 || $am_labsafe_dossier<$totamount)
				{
					$cdossier=1;
				}
			}
			$wherebase=" WHERE shortname='$an' AND payment_file='".$cdossier."' AND type='cash'";
			$payment_sum=0; $cash_sum=0;
			$line=orsee_query("SELECT SUM(amount) as somme FROM ".table("pay_accounts").$wherebase. " AND unit='payment_cash'");
			if($line!==false) $payment_sum=$line["somme"];
			$q_cash="SELECT * FROM ".table("pay_accounts").$wherebase. " AND unit='cash' ORDER BY nominal DESC";
			if(orsee_query($q_cash)===false) return;
			$line=orsee_query("SELECT SUM(amount) as somme FROM ".table("pay_accounts").$wherebase. " AND unit='cash'");
			if($line!==false) $cash_sum=$line["somme"];
			$sum_left=$cash_sum-abs($payment_sum);
			$new_payment_sum=abs($payment_sum);
			$cashlines=orsee_query($q_cash,"return_same");
			$n_nonzero=0; $nonzero_nunits=0; $nonzero_nominal=0;
			for($i=0; $i<count($cashlines); $i++) {
				$cl=$cashlines[$i];
				if($cl["amount"]>$sum_left) {
					$todeductbase=$cl["amount"]-$sum_left;
					$todeduct_nunits=ceil($todeductbase/$cl["nominal"]);
					$todeduct=$todeduct_nunits*$cl["nominal"];
					while($cl["amount"]<$todeduct) {
						$todeduct_nunits--;
						$todeduct=$todeduct_nunits*$cl["nominal"];
					}
					$new_payment_sum-=$todeduct;
					$cashlines[$i]["new_amount"]=$cl["amount"]-$todeduct;
					$cashlines[$i]["n_units"]=$cl["n_units"]-$todeduct_nunits;
					if($cashlines[$i]["new_amount"]==0) $q="DELETE FROM ".table("pay_accounts").$wherebase. " AND unit='cash' AND nominal='".$cl["nominal"]."'";
					else {$q="UPDATE ".table("pay_accounts")." SET amount='".$cashlines[$i]["new_amount"]."', n_units='".$cashlines[$i]["n_units"]."' ".$wherebase. " AND unit='cash' AND nominal='".$cl["nominal"]."'";}
					mysqli_query($GLOBALS['mysqli'],$q) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$q);
					$cashlines[$i]["amount"]=$cashlines[$i]["new_amount"];
				}
				if($cashlines[$i]["amount"]>0) {$n_nonzero++; $nonzero_nominal=$cashlines[$i]["nominal"]; $nonzero_nunits=$cashlines[$i]["n_units"];}
			}
			if($n_nonzero==1 && $new_payment_sum>0) {
				if($new_payment_sum % $nonzero_nominal == 0) {
					$nonzero_nunits+=($new_payment_sum/$nonzero_nominal);
					$nonzero_amount=$nonzero_nominal*$nonzero_nunits;
					$q="UPDATE ".table("pay_accounts")." SET amount='".$nonzero_amount."', n_units='".$nonzero_nunits."' ".$wherebase. " AND unit='cash' AND nominal='".$nonzero_nominal."'";
					mysqli_query($GLOBALS['mysqli'],$q) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$q);
					$new_payment_sum=0;
				}
			}
			if($new_payment_sum==0) $q="DELETE FROM ".table("pay_accounts");
			else {$q="UPDATE ".table("pay_accounts")." SET amount='".-$new_payment_sum."', n_units='".-$new_payment_sum."'";}
			$q.=" ".$wherebase. " AND unit='payment_cash'";
			mysqli_query($GLOBALS['mysqli'],$q) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$q);
		}
	}
	
	function setimplode($join,$arr,$setvals=array()) {
		$res=""; $first=true;
		foreach($arr as $k=>$v) if(empty($setvals) || in_array($k,$setvals)) {
			if(!$first) $res.=$join;
			$res.="$k='$v'";
			$first=false;
		}
		return $res;
	}
	
	static function get_new_id()
	{
		operation::operation_tables();
		return query_new_id(table("pay_operations"));
	}
	
	public static function operation_tables()
	{
		$tables=array();
		$tables["permanent_pay_accounts"]=array("id"=>"INT(20)", "shortname"=>"varchar(150)", "name"=>"varchar(250)");
		$tables["pay_accounts"]=array("type"=>"varchar(25)","shortname"=>"varchar(150)","name"=>"varchar(250)","payment_file"=>"varchar(250)","unit"=>"varchar(25)","nominal"=>"DECIMAL(6,2)","n_units"=>"INT","amount"=>"DECIMAL(10,2)");
		$tables["pay_operations"]=array("id"=>"int(20)","admin"=>"varchar(150)","datetime"=>"varchar(250)","payment_file"=>"varchar(250)","debit_account"=>"varchar(250)","credit_account"=>"varchar(250)","unit"=>"varchar(25)","nominal"=>"DECIMAL(6,2)","n_units"=>"INT","amount"=>"DECIMAL(10,2)","remarks"=>"text","instead_of_file"=>"varchar(250)","archived"=>"tinyint");
		// print_r($tables); exit;
		foreach($tables as $table=>$columns) {
			$first=true;
			foreach($columns as $colname=>$coltype){
				if($first) {
				$query="CREATE TABLE IF NOT EXISTS ".table($table)." ($colname $coltype NOT NULL)";
				$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysql_error($GLOBALS['mysqli']).'<br> query: '.$query);
				$first=false;
				break;
				}
			}
			query_makecolumns(table($table),array_keys($columns),array_values($columns));
			$query="SELECT * FROM ".table("permanent_pay_accounts")."";
			$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysql_error($GLOBALS['mysqli']));
			if(mysqli_num_rows($result)==0) {
				$perm_base_accounts=array("bank"=>"bank/bookkeeper","lab"=>"lab");
				foreach($perm_base_accounts as $kpa=>$pa) {
					$q="INSERT INTO ".table("permanent_pay_accounts")." SET id='".query_new_id(table("permanent_pay_accounts"),'id')."', name='$pa'";
					if(is_string($kpa))$q.=", shortname='$kpa'";
					orsee_query($q);
				}
			}
		}
		$static = !(isset($this) && get_class($this) == __CLASS__);
		if(!$static) $this->_tables=$tables;

	}
}


?>

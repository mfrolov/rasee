<?php 
// part of orsee. see orsee.org

function experimentmail__mail($recipient,$subject,$message,$headers,$env_sender="") {
	global $settings, $settings__charset;

	if(preg_match("/^\s*#html#/",$subject)) {
		$subject=preg_replace("/^\s*#html#/","",$subject);
		$headers ='MIME-Version: 1.0' . "\r\n" . "Content-type: text/html; charset=$settings__charset" . "\r\n". $headers;
		$contains_a=preg_match('#\<a.*?\>(.*?)\<\/a\>#si',$message);
		$stripped_message=strip_tags($message, '<strong><b><em><i><u><span><font><hr><a><table><thead><tbody><tfoot><tr><td><ol><ul><li>');
		if($stripped_message!=$message) $message=str_replace('-------','<br>-------<br>',$message);
		else $message=str_replace("\n"," <br>\n",str_replace("\r\n","\n",$message));
		if(!$contains_a) {
			$url_pattern='%(?i)\b(((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’])))%';
			$url_replacement='<a href="$2" >$2</a>';
			$message=preg_replace($url_pattern,$url_replacement,$message);
		}
	}
	else $headers .= "Content-Type: text/plain; charset=\"$settings__charset\"\r\n";
	$message=html_entity_decode($message,ENT_COMPAT,$settings__charset); //,'UTF-8' or 'ISO-8859-1'
	$subject=html_entity_decode($subject,ENT_COMPAT,$settings__charset); //,'UTF-8' or 'ISO-8859-1'
	$subject='=?'.$settings__charset.'?B?'.base64_encode($subject).'?=';
	$done=experimentmail__send($recipient,$subject,$message,$headers,$env_sender);
	return $done;
}


function experimentmail__send($recipient,$subject,$message,$headers,$env_sender="") {
	global $settings;
	if ($settings['email_sendmail_type']=="indirect") {
		if (empty($env_sender)) $env_sender=$settings['support_mail'];
		if ($settings['email_sendmail_path']) $sendmail_path=$settings['email_sendmail_path'];
			else $sendmail_path="/usr/sbin/sendmail";

		$sendmail = $sendmail_path." -t -i -f $env_sender";

		$fd = popen($sendmail, "w");
		fputs($fd, "To: $recipient\r\n");
		fputs($fd, $headers);
		fputs($fd, "Subject: $subject\r\n");
		fputs($fd, "X-Mailer: orsee\r\n\r\n");
		fputs($fd, $message);
		pclose($fd);
		$done=true;
		}
	   else {
	    $headers="Errors-To: ".$settings['support_mail']."\r\n".$headers;
		// $done=mail($recipient,$subject,$message,$headers);
		$done=mail(stripslashes($recipient),stripslashes($subject),stripslashes($message),stripslashes($headers),"-r ".$settings['support_mail']);
		}
	return $done;
}

function load_mail($mail_name,$lang) {
	global $authdata;

	$query="SELECT * FROM ".table('lang')." 
		WHERE content_type='mail'
		AND content_name='".$mail_name."'";
	$marr=orsee_query($query);
	if($marr === false) return false;
	if (isset($marr[$lang])) $mailtext=$marr[$lang];
		else $mailtext=$marr[$authdata['language']];
	return $mailtext;
}

function experimentmail__load_invitation_text($experiment_id,$tlang="") {
	global $settings;
	if (!$tlang) $tlang=$settings['public_standard_language'];
	$query="SELECT * from ".table('lang')."
                WHERE content_type='experiment_invitation_mail'
                AND content_name='".$experiment_id."'";
        $experiment_mail=orsee_query($query);
	return $experiment_mail[$tlang];
}

function experimentmail__load_bulk_mail($bulk_id,$tlang="") {
        global $settings;
        if (!$tlang) $tlang=$settings['public_standard_language'];
        $query="SELECT * from ".table('bulk_mail_texts')."
                WHERE bulk_id='".$bulk_id."'
                AND lang='".$tlang."'";
        $bulk_mail=orsee_query($query);
	if (!isset($bulk_mail['bulk_subject'])) {
		$query="SELECT * from ".table('bulk_mail_texts')."
                WHERE bulk_id='".$bulk_id."'
                AND lang='".$settings['public_standard_language']."'";
        	$bulk_mail=orsee_query($query);
		}
        return $bulk_mail;
}


function experimentmail__gc_bulk_mail_texts() {
	$active_bulks=array();
        $query="SELECT ".table('bulk_mail_texts').".bulk_id from ".table('bulk_mail_texts').", ".table('mail_queue')." 
		WHERE ".table('bulk_mail_texts').".bulk_id=".table('mail_queue').".bulk_id 
                ORDER BY ".table('bulk_mail_texts').".bulk_id";
        $result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
	while ($line=mysqli_fetch_assoc($result)) {
		$active_bulks[]=$line['bulk_id'];
		}
	$bulk_string=implode("','",$active_bulks);
        $query="DELETE from ".table('bulk_mail_texts')."
                WHERE bulk_id NOT IN ('".$bulk_string."')";
        $done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
        return $done;
}

function process_mail_template($template,$vararray) {

        $output=explode("\n",$template);
		
		if(!empty($vararray["lname"]) && !empty($vararray["fname"])) {
			if(empty($vararray["flname"])) $vararray["flname"]=$vararray["fname"].",".$vararray["lname"];
			if(!empty($vararray["email"]) && empty($vararray["flnamemail"])) $vararray["flnamemail"]=$vararray["flname"]."|".$vararray["email"];
			if(!empty($vararray["email"]) && !empty($vararray["session_id"]) && empty($vararray["flnamemailsid"])) $vararray["flnamemailsid"]=$vararray["flname"]."|".$vararray["email"]."|".$vararray["session_id"];
		}
		
		if(!empty($vararray["participant_id"])) {
			if(empty($vararray["participant_id_crypt_new"])) $vararray["participant_id_crypt_new"]=url_cr_encode($vararray["participant_id"]);
		}

		if(!empty($vararray["experiment_id"]) && !empty($vararray["participant_id"])) {
			if(!array_key_exists("external_code",$vararray)) {
				query_makecolumns(table('participate_at'),"external_code","varchar(255)","");
				$tquery="SELECT external_code FROM ".table("participate_at")." WHERE experiment_id='".mysqli_real_escape_string($GLOBALS['mysqli'],$vararray["experiment_id"])."' AND participant_id='".mysqli_real_escape_string($GLOBALS['mysqli'],$vararray["participant_id"])."' AND payghost=0";
				$line=orsee_query($tquery);
				if($line!==false && !empty($line["external_code"])) $vararray["external_code"]=$line["external_code"];
			}
		}
		if(!empty($vararray["session_id"])) {
			if(empty($vararray["session_start_date"])) $vararray["session_start_date"]=time__format($GLOBALS["settings"]['public_standard_language'],time__unixtime_to_time_package(sessions__get_session_time(array(),$vararray["session_id"])));
			if(empty($vararray["session_end_date"])) $vararray["session_end_date"]=time__format($GLOBALS["settings"]['public_standard_language'],time__unixtime_to_time_package(sessions__get_session_end_time(array(),$vararray["session_id"])));
		}


        $vars=array_keys($vararray);
		
		$supinfokey=array_search("supinfo",$vars);
		if($supinfokey !== false) {unset($vars[$supinfokey]); array_unshift($vars,"supinfo");}

        foreach ($vars as $key) {
            $i=0;
            foreach ($output as $outputline) {
				if(($key=="lname" || $key=="fname") && isset($vararray[$key])) $vararray[$key]=str_replace("?","",$vararray[$key]);
                if(isset($vararray[$key])) $output[$i]=str_replace("#ub64gz-".$key."#",urlencode(preg_replace("/^H4sIAAAAAAA/","",base64_encode(gzencode($vararray[$key],9)))),$output[$i]);
                // var_dump("#".$key."#",$vararray[$key],$output[$i],isset($vararray[$key])); 
				if(isset($vararray[$key])) $output[$i]=str_replace("#".$key."#",$vararray[$key],$output[$i]);
                $i++;
                }
           }
	$result="";
        foreach($output as $outputline) {
			$modoutpline=$outputline;
			if(!empty($vararray["session_id"])){
				if(preg_match_all("/#session_remarks_parameter(\d)#/",$modoutpline,$sessremarksmatches,PREG_SET_ORDER)) {
					$sess_remark_list=sessions__get_remark_list($vararray["session_id"]);
					foreach($sess_remark_list as $srem) {
						$asrem=explode(":",$srem); $nasrem=count($asrem);
						$asrem_left=array_shift($asrem);
						foreach($sessremarksmatches as $sessremarksm)
							if(($asrem_left=="param".$sessremarksm[1] || $asrem_left=="parameter".$sessremarksm[1]) && $nasrem>1) {
								$modoutpline=str_replace("#session_remarks_parameter".$sessremarksm[1]."#",implode(":",$asrem),$modoutpline);
						}
					}
				}
			}
			$modoutpline=preg_replace("/#session_remarks_parameter(\d)#/","",$modoutpline);
			$modoutpline=preg_replace_callback("/#t(\d)md5-(\w+)#/", function($matches) {return tinymd5($matches[2],$matches[1]);}, $modoutpline);
			$modoutpline=str_replace("<line>","\r\n",$modoutpline);
              // $result=$result.trim($outputline)."\n";
               // $result=$result.$outputline."\n";
            $result=$result.$modoutpline."\n";
		}
       return $result;
}


function experimentmail__mail_attach($to, $from, $subject, $message, $filename, $filecontent,$lb="\n") {
   // $to Recipient
   // $from Sender (like "email@domain.com" or "Name <email@domain.com>")
   // $subject Subject
   // $message Content
   // $file File (on server) to attach
   $mime_boundary = "<<<:" . md5(uniqid(mt_rand(), 1));
   $data = chunk_split(base64_encode($filecontent),60,"\r\n");
   $header = "From: ".$from.$lb;
   //$header.= "To: ".$to.$lb;
   $header.= "MIME-Version: 1.0".$lb;
   $header.= "Content-Type: multipart/mixed;".$lb;
   $header.= " boundary=\"".$mime_boundary."\"".$lb;
   
   $content = "This is a multi-part message in MIME format.".$lb.$lb;
   $content.= "--".$mime_boundary.$lb;
   $content.= "Content-Type: text/plain; charset=\"iso-8859-1\"".$lb;
   $content.= "Content-Transfer-Encoding: 7bit".$lb.$lb;
   $content.= $message.$lb;
   $content.= "--".$mime_boundary.$lb;
   $content.= "Content-Disposition: attachment;".$lb;
   $content.= "Content-Type: Application/Octet-Stream; name=\"".$filename."\"".$lb;
   $content.= "Content-Transfer-Encoding: base64".$lb.$lb;
   $content.= $data.$lb;
   $content.= "--" . $mime_boundary . $lb;
   $subject='=?ISO-8859-1?B?'.base64_encode($subject).'?=';
   if(experimentmail__send($to, $subject, $content, $header)) {
       return TRUE;
   }
   return FALSE;
}


// lists possible sessions for given experiment
// only sessions, which registration end is in the future and which 
// are not full are listed
function experimentmail__get_session_list($experiment_id,$tlang="") {
	global $settings, $lang;
	$savelang=$lang;

	if (!$tlang) $tlang=$settings['public_standard_language'];

	if ($lang['lang']!=$tlang) 
		$lang=load_language($tlang);

	$query="SELECT *
      		FROM ".table('sessions')."
        	WHERE experiment_id='".$experiment_id."'
		AND session_finished!='y' 
      		ORDER BY session_start_year, session_start_month, session_start_day,
		 	session_start_hour, session_start_minute";
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
	$list="";
	while ($s=mysqli_fetch_assoc($result)) {
        	$registration_unixtime=sessions__get_registration_end($s);
        	$session_full=sessions__session_full('',$s,true);
		$now=time();
		$register_after_start_val=sessions__get_remark_list_elem_by_name($s['session_id'],"register_after_start");
		$register_after_start=!empty($register_after_start_val) && $register_after_start_val!='n';
		if($register_after_start) { $sessions_endtime=sessions__get_session_end_time($s); if(time()>=$sessions_endtime) $register_after_start=false; }
		if (($registration_unixtime > $now || $register_after_start) && !$session_full) {
        		$list.=session__build_name($s,$lang['lang']).' '.
        			laboratories__get_laboratory_name($s['laboratory_id']).', '.
        			$lang['registration_until'].' '.
        			time__format($lang['lang'],'',false,false,true,false,$registration_unixtime);
			$list.="\n";
			}
		}
	$lang=$savelang;
	return $list;
}

function experimentmail__send_invitations_to_queue($experiment_id,$whom="not-invited",$starttime=0) {

	switch ($whom) {
		case "not-invited": 	$aquery=" AND invited='n' "; break;
		case "all":		$aquery=""; break;
		default:		$aquery=" AND ".table('participants').".participant_id='0' ";

		}
	mt_srand((double)microtime()*1000000);
        $now=mt_rand();
        $order="ORDER BY rand(".$now.") ";
	$qsupvar=""; $qsupval="";
	if($starttime>0) {$qsupvar=",start_at"; $qsupval=", ".$starttime; query_makecolumns(table('mail_queue'),array("start_at"),array("int(20)"),array("0"));}
	$query="INSERT INTO ".table('mail_queue')." (timestamp,mail_type,mail_recipient,experiment_id".$qsupvar.") 
		SELECT UNIX_TIMESTAMP(),'invitation', ".table('participants').".participant_id, experiment_id".$qsupval." 
		FROM ".table('participants').", ".table('participate_at')." 
		WHERE experiment_id='".$experiment_id."' 
		AND ".table('participants').".participant_id=".table('participate_at').".participant_id ".
		$aquery." 
		AND registered = 'n' AND deleted='n'".$order;

	$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
	$count=mysqli_affected_rows($GLOBALS['mysqli']);
	query_makecolumns(table('participants'),array("last_invitation_time","last_invitation_exp"),array("int(20)","int(20)"),array("0","0"));
	query_makecolumns(table('participants_temp'),array("last_invitation_time","last_invitation_exp"),array("int(20)","int(20)"),array("0","0"));
	return $count;
}

function experimentmail__send_bulk_mail_to_queue($bulk_id,$part_array,$starttime=0) {

	$return=false;
	if (is_array($part_array)) {
		$now=time(); $done=shuffle($part_array);
		$qsup="";
		if($starttime>0) {$qsup=", start_at='".$starttime."'"; query_makecolumns(table('mail_queue'),array("start_at"),array("int(20)"),array("0"));}
		foreach ($part_array as $participant_id) {
        		$query="INSERT INTO ".table('mail_queue')." 
				SET timestamp='".$now."',
				mail_type='bulk_mail',
				mail_recipient='".$participant_id."',";
				if(!empty($_REQUEST["experiment_id"])) $query.="
				experiment_id='".$_REQUEST['experiment_id']."',
				";
				if(!empty($_REQUEST["session_id"])) $query.="
				session_id='".$_REQUEST['session_id']."',
				";
				$query.="
				bulk_id='".$bulk_id."'".$qsup;
        		$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
			}
			$return=true;
		}
	return $return;
}


function experimentmail__send_session_reminders_to_queue($session,$absentonly=false) {

        $query="INSERT INTO ".table('mail_queue')." (timestamp,mail_type,mail_recipient,experiment_id,session_id)
                SELECT UNIX_TIMESTAMP(),'session_reminder', participant_id, experiment_id, session_id
                FROM ".table('participate_at')."
                WHERE experiment_id='".$session['experiment_id']."'
                AND session_id='".$session['session_id']."'";
		if($absentonly) $query.=" AND shownup='n' AND participated='n'";
        $done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
        $count=mysqli_affected_rows($GLOBALS['mysqli']);

	// update session table : reminder_sent
	$query="UPDATE ".table('sessions')." SET reminder_sent='y' WHERE session_id='".$session['session_id']."'";
	$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: ".mysqli_error($GLOBALS['mysqli']));
        return $count;
}


function experimentmail__send_noshow_warnings_to_queue($session) {

        $query="INSERT INTO ".table('mail_queue')." (timestamp,mail_type,mail_recipient,experiment_id,session_id)
                SELECT UNIX_TIMESTAMP(),'noshow_warning', participant_id, experiment_id, session_id
                FROM ".table('participate_at')."
                WHERE experiment_id='".$session['experiment_id']."'
                AND session_id='".$session['session_id']."'
		AND shownup='n'";
        $done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
        $count=mysqli_affected_rows($GLOBALS['mysqli']);
        return $count;
}

function experimentmail__set_reminder_checked($session_id) {
	// update session table : reminder_checked
        $query="UPDATE ".table('sessions')." SET reminder_checked='y' WHERE session_id='".$session_id."'";
        $done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: ".mysqli_error($GLOBALS['mysqli']));
	return $done;
}

function experimentmail__set_noshow_warnings_checked($session_id) {
        // update session table : reminder_checked
        $query="UPDATE ".table('sessions')." SET noshow_warning_sent='y' WHERE session_id='".$session_id."'";
        $done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: ".mysqli_error($GLOBALS['mysqli']));
        return $done;
}

function experimentmail__mails_in_queue($type="",$experiment_id="",$session_id="") {

	if ($type) $tquery=" AND mail_type='".$type."' "; else $tquery="";
	if ($experiment_id) $equery=" AND experiment_id='".$experiment_id."' "; else $equery="";
	if ($session_id) $squery=" AND session_id='".$session_id."' "; else $squery="";

	$query="SELECT count(mail_id) as number FROM ".table('mail_queue')."
		WHERE mail_id>0 ".$tquery.$equery.$squery;
	$line=orsee_query($query);
	$number=$line['number'];
	return $number;
}

function experimentmail__remove_from_queue($type="",$experiment_id="",$session_id="") {

	if ($type) $tquery=" AND mail_type='".$type."' "; else $tquery="";
	if ($experiment_id) $equery=" AND experiment_id='".$experiment_id."' "; else $equery="";
	if ($session_id) $squery=" AND session_id='".$session_id."' "; else $squery="";

	$query="DELETE FROM ".table('mail_queue')."
		WHERE mail_id>0 ".$tquery.$equery.$squery;
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$query." <hr> ".lang('technical_information').":<br>".print_r(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS),true));
	$number=mysqli_affected_rows($GLOBALS["mysqli"]);
	return $number;
}


function experimentmail__send_mails_from_queue($number=0,$type="",$experiment_id="",$session_id="") {

	if ($number>0) $limit=" LIMIT ".$number; else $limit="";
        if ($type) $tquery=" AND mail_type='".$type."' "; else $tquery="";
        if ($experiment_id) $equery=" AND experiment_id='".$experiment_id."' "; else $equery="";
        if ($session_id) $squery=" AND session_id='".$session_id."' "; else $squery="";
	
	$satquery="";
	if(query_column_exist(table('mail_queue'),"start_at")) $satquery=" AND start_at<=".time();
	

	$smails=array(); $smails_ids=array();

	$invitations=array(); $reminders=array(); $bulks=array(); $errors=array(); 
	$reminder_text=array(); $warning_text=array();
        $pform_fields=array(); $professions=array(); $fields_of_studies=array(); $genders=array(); 
        $warnings=array(); $exps=array(); $sesss=array(); $parts=array(); $inv_texts=array(); $slists=array();
        $labs=array();

	// first get mails to send
	$query="SELECT * FROM ".table('mail_queue')."
                WHERE mail_id>0 ".
		$tquery.
		$equery.
		$squery.
		$satquery."
		ORDER BY timestamp, mail_id ".
		$limit;
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

	while ($line=mysqli_fetch_assoc($result)) {
		$smails[]=$line;
		$smails_ids[]=$line['mail_id'];
	}
	$smails_ids_string=implode("','",$smails_ids);
	$query="DELETE FROM ".table('mail_queue')." 
		WHERE mail_id IN ('".$smails_ids_string."')";
	$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

	foreach ($smails as $line) {
		$texp=$line['experiment_id'];
		$tsess=$line['session_id'];
		$tpart=$line['mail_recipient'];
		$ttype=$line['mail_type'];
		$tbulk=$line['bulk_id'];
		$continue=true;

		// well, if experiment_id, session_id, recipient, footer or inv_text, add to array
		if (!isset($exps[$texp]) && $texp) 
			$exps[$texp]=orsee_db_load_array("experiments",$texp,"experiment_id");
		if (!isset($sesss[$tsess]) && $tsess)
                        $sesss[$tsess]=orsee_db_load_array("sessions",$tsess,"session_id");
		if (!isset($parts[$tpart]) && $tpart)
                        $parts[$tpart]=orsee_db_load_array("participants",$tpart,"participant_id");
		$tlang=$parts[$tpart]['language'];
		if (!isset($footers[$tlang]))
			$footers[$tlang]=load_mail("public_mail_footer",$tlang);
		if ($ttype=="session_reminder" && !isset($reminder_text[$tlang])) {
                        $reminder_text[$tlang]['text']=load_mail("public_session_reminder",$tlang);
			$reminder_text[$tlang]['subject']=load_language_symbol('email_session_reminder_subject',$tlang);
			}
		if ($ttype=="noshow_warning" && !isset($warning_text[$tlang])) {
                        $warning_text[$tlang]['text']=load_mail("public_noshow_warning",$tlang);
                        $warning_text[$tlang]['subject']=load_language_symbol('email_noshow_warning_subject',$tlang);
                        }
		if (($ttype=="session_reminder" || $ttype=="noshow_warning") && !isset($labs[$tsess][$tlang])) {
                        $labs[$tsess][$tlang]=laboratories__get_laboratory_text($sesss[$tsess]['laboratory_id'],$tlang);
                        }
        if (!isset($pform_fields[$tlang])) $pform_fields[$tlang]=participant__load_result_table_fields('email',$tlang);
		
		if ($ttype=="invitation" && !isset($inv_texts[$texp][$tlang]))
			$inv_texts[$texp][$tlang]=experimentmail__load_invitation_text($texp,$tlang);
		if ($ttype=="invitation" && !isset($slists[$texp][$tlang]))
			$slists[$texp][$tlang]=experimentmail__get_session_list($texp,$tlang);
		if ($ttype=="bulk_mail" && !isset($bulk_mails[$tbulk][$tlang])) {
                        $bulk_mails[$tbulk][$tlang]=experimentmail__load_bulk_mail($tbulk,$tlang);
		}

		// check for missing values ...
		if (!isset($parts[$tpart]['participant_id'])) {
			$continue=false;
			// email error: no recipient
			$line['error'].="recipient:";
		} elseif($parts[$tpart]['deleted'] == 'y') {
			$continue=false;
			// email error: no recipient
			$line['error'].="recipient:";
		}
		else {
		   	foreach ($pform_fields[$tlang] as $f) {
		   		if(preg_match("/(radioline|select_list|select_lang)/",$f['type']) && isset($f['lang'][$parts[$tpart][$f['mysql_column_name']]]))
					$parts[$tpart][$f['mysql_column_name']]=$f['lang'][$parts[$tpart][$f['mysql_column_name']]];
			}
		}

                if (!isset($exps[$texp]['experiment_id']) && ($ttype=="invitation" || $ttype=="session_reminder" 
				|| $ttype=="noshow_warning")) {
                        $continue=false;
                        // email error: no experiment id given
			$line['error'].="experiment:";
                        }

                if (!isset($sesss[$tsess]['session_id']) && ($ttype=="session_reminder" || $ttype=="noshow_warning")) {
                        $continue=false;
                        // email error: no session id given
			$line['error'].="session:";
                        }

		if (!isset($inv_texts[$texp][$tlang]) && $ttype=="invitation") {
                        $continue=false;
                        // email error: no inv_text given
			$line['error'].="inv_text:";
                        }

		if (!isset($bulk_mails[$tbulk][$tlang]) && $ttype=="bulk_mail") {
                        $continue=false;
                        // email error: no bulk_mail given
                        $line['error'].="bulk_mail:";
                        }

		// fine, if no errors, add to arrays
		if ($continue) {

			switch ($line['mail_type']) {
				case "invitation": 
					$invitations[]=$line;
					break;
				case "session_reminder":
					$reminders[]=$line;
					break;
				case "noshow_warning":
                                        $warnings[]=$line;
                                        break;
				case "bulk_mail": 
					$line['bulk_mails']=$bulk_mails;
					$bulks[]=$line;
					break;
				}
			}
		   else {
			$errors[]=$line;
			}
		}

	// fine now we have everything we want, and we can proceed with sending the mails

	$mails_sent=0; $mails_errors=0;

	// reminders
	foreach ($reminders as $mail) {
                $tlang=$parts[$mail['mail_recipient']]['language'];
                $done=experimentmail__send_session_reminder_mail($mail,$parts[$mail['mail_recipient']],
                                                $exps[$mail['experiment_id']],$sesss[$mail['session_id']],
                                                $reminder_text[$tlang],$labs[$mail['session_id']][$tlang],
						$footers[$tlang]);
                if ($done) {
                        $mails_sent++;
                        $deleted=experimentmail__delete_from_queue($mail['mail_id']);
                        }
                   else {
                        $mail['error']="sending";
                        $errors[]=$mail;
                        }
                }


	// noshow warnings
        foreach ($warnings as $mail) {
                $tlang=$parts[$mail['mail_recipient']]['language'];
                $done=experimentmail__send_noshow_warning_mail($mail,$parts[$mail['mail_recipient']],
                                                $exps[$mail['experiment_id']],$sesss[$mail['session_id']],
                                                $warning_text[$tlang],$labs[$mail['session_id']][$tlang],
                                                $footers[$tlang]);
                if ($done) {
                        $mails_sent++;
                        $deleted=experimentmail__delete_from_queue($mail['mail_id']);
                        }
                   else {
                        $mail['error']="sending";
                        $errors[]=$mail;
                        }
                }



	// invitations
	foreach ($invitations as $mail) {
		$tlang=$parts[$mail['mail_recipient']]['language'];
		// var_dump($tlang,$exps,$slists);
    	if ($exps[$mail['experiment_id']]['experiment_type']=='laboratory' && (!trim($slists[$mail['experiment_id']][$tlang]))) {
    		$done=true;
		} else {
    		$done=experimentmail__send_invitation_mail($mail,$parts[$mail['mail_recipient']],
        		$exps[$mail['experiment_id']],$inv_texts[$mail['experiment_id']][$tlang],
        		$slists[$mail['experiment_id']][$tlang],$footers[$tlang]); // var_dump($done);
		}
		if ($done) {
			$mails_sent++;
			$deleted=experimentmail__delete_from_queue($mail['mail_id']);
			}
		   else {
			$mail['error']="sending";
			$errors[]=$mail;
			}
		}
				
	// bulks
	foreach ($bulks as $mail) {
				$tbulk=$mail['bulk_id'];
                $tlang=$parts[$mail['mail_recipient']]['language'];
				$exp4bulk=array();
				if(!empty($mail['experiment_id']) && !empty($exps[$mail['experiment_id']])) $exp4bulk=$exps[$mail['experiment_id']];
                $done=experimentmail__send_bulk_mail($mail,$parts[$mail['mail_recipient']],$mail['bulk_mails'][$tbulk][$tlang],$footers[$tlang],$exp4bulk);
                if ($done) {
                        $mails_sent++;
                        $deleted=experimentmail__delete_from_queue($mail['mail_id']);
                        }
                   else {
                        $mail['error']="sending";
                        $errors[]=$mail;
                        }
                }
	$done=experimentmail__gc_bulk_mail_texts();


	// handle errors
	foreach ($errors as $mail) {
		$query="UPDATE ".table('mail_queue')." 
			SET error='".$mail['error']."' 
			WHERE mail_id='".$mail['mail_id']."'";
		$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

		}
	$mess['mails_sent']=$mails_sent;
	$mess['mails_errors']=$mails_errors;
	return $mess;

}

function experimentmail__delete_from_queue($mail_id) {

	$query="DELETE FROM ".table('mail_queue')."
		WHERE mail_id='".$mail_id."'";
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
	return $result;
}

function experimentmail__send_session_reminder_mail($mail,$part,$exp,$session,$reminder_text,$lab,$footer) {
        global $settings;


        $part['edit_link']=experimentmail__build_edit_link($part['participant_id']);
        $part['experiment_name']=$exp['experiment_public_name'];
        $part['session_date']=session__build_name($session,$part['language']);
	$part['lab_name']=laboratories__strip_lab_name($lab);
	$part['lab_address']=laboratories__strip_lab_address($lab);


        $mailtext=stripslashes($reminder_text['text']);
        $subject=$reminder_text['subject'];
        $recipient=$part['email'];

		if(empty($part['experiment_id']) && !empty($exp['experiment_id'])) $part['experiment_id']=$exp['experiment_id'];
		if(empty($part['session_id']) && !empty($session['session_id'])) $part['session_id']=$session['session_id'];
		$part['supinfo']="";
		if(!empty($exp['reminder_mail_supinfo'])) {
			$part['supinfo']="\r\n".str_replace("<line>","\r\n",$exp['reminder_mail_supinfo'])."\r\n";
		}
		$part['unregister_info']="\n";
		$unregister_end_time=sessions__get_unregister_end($session);
		if(time()<$unregister_end_time) {
			$unregister_end=time__format($lang['lang'],'',false,false,true,false,$unregister_end_time);
			$part_show_link=$GLOBALS['settings__root_url']."/".$GLOBALS['settings__public_folder']."/participant_show.php?p=".url_cr_encode($part['participant_id']);
			$part['unregister_info'].=lang('you_may_unsubscribe_from_this_session_before').' '.$unregister_end.'. ';
			$part['unregister_info'].=lang('in_order_to_do_it_visit_the_following_link').': '.$part_show_link.' '.PHP_EOL;
		}
		$sess_remark_list=sessions__get_remark_list($part['session_id']);
		$confirmmail_sess_message="";
		foreach($sess_remark_list as $srem) {
			$asrem=explode(":",$srem);
			if($asrem[0]=="confirmmail_message" && count($asrem)>1) $confirmmail_sess_message="\r\n".str_replace("<line>","\r\n",str_replace("<comma>",",",implode(":",array_slice($asrem, 1))))."\r\n";
			if($asrem[0]=="remindermail_message" && count($asrem)>1) $part['supinfo'].=(($asrem[1]=="!confirmmail_message!")?$confirmmail_sess_message:"\r\n".str_replace("<line>","\r\n",str_replace("<comma>",",",implode(":",array_slice($asrem, 1))))."\r\n");
		}
        $message=process_mail_template($mailtext,$part)."\n".process_mail_template($footer,$part);

        if ($exp['sender_mail']) $sender=$exp['sender_mail']; else {
			$sender="";
			if(!empty($settings['sender_name'])) $sender.=$settings['sender_name']." <";
			$sender.=$settings['support_mail'];
			if(!empty($settings['sender_name'])) $sender.=">";
		}

        $headers="From: ".$sender."\r\n";

        //echo $headers.$recipient."\n".$subject."\n\n".$message;

        $done=experimentmail__mail($recipient,$subject,$message,$headers);
        return $done;

}

function experimentmail__send_noshow_warning_mail($mail,$part,$exp,$session,$warning_text,$lab,$footer) {
        global $settings;


        $part['edit_link']=experimentmail__build_edit_link($part['participant_id']);
        $part['experiment_name']=$exp['experiment_public_name'];
        $part['session_date']=session__build_name($session,$part['language']);
        $part['lab_name']=laboratories__strip_lab_name($lab);
        $part['lab_address']=laboratories__strip_lab_address($lab);
	$part['max_noshows']=$settings['automatic_exclusion_noshows'];


        $mailtext=stripslashes($warning_text['text']);
        $subject=$warning_text['subject'];
        $recipient=$part['email'];

		if(empty($part['experiment_id']) && !empty($exp['experiment_id'])) $part['experiment_id']=$exp['experiment_id'];
        $message=process_mail_template($mailtext,$part)."\n".process_mail_template($footer,$part);


        $sender="";
		if(!empty($settings['sender_name'])) $sender.=$settings['sender_name']." <";
		$sender.=$settings['support_mail'];
		if(!empty($settings['sender_name'])) $sender.=">";

        $headers="From: ".$sender."\r\n";

        //echo $headers.$recipient."\n".$subject."\n\n".$message;

        $done=experimentmail__mail($recipient,$subject,$message,$headers);
        return $done;

}

function experimentmail__send_participant_exclusion_mail($part) {
        global $settings;

	$mailtext=stripslashes(load_mail("public_participant_exclusion",$part['language']));
        $subject=load_language_symbol('participant_exclusion_mail_subject',$part['language']);
        $recipient=$part['email'];
        $message=process_mail_template($mailtext,$part)."\n".experimentmail__get_mail_footer($part['participant_id']);
        $sender="";
		if(!empty($settings['sender_name'])) $sender.=$settings['sender_name']." <";
		$sender.=$settings['support_mail'];
		if(!empty($settings['sender_name'])) $sender.=">";
        $headers="From: ".$sender."\r\n";

        $done=experimentmail__mail($recipient,$subject,$message,$headers);
        return $done;
}

function experimentmail__send_reminder_notice($line,$number,$sent,$disclaimer="") {
	global $settings;

	$experimenters=explode(",",$line['experimenter_mail']);

	foreach ($experimenters as $experimenter) {

		$mail=orsee_db_load_array("admin",$experimenter,"adminname");

		$tlang= ($mail['language']) ? $mail['language'] : $settings['admin_standard_language'];
		$lang=load_language($tlang);

		$mail['session_name']=session__build_name($line,$tlang);
		$mail['experiment_name']=$line['experiment_name'];
		$mail['nr_participants'] = ($sent) ? $number : 0;
	
		switch ($disclaimer) {
			case 'part_needed': 
				$mail['disclaimer']=load_language_symbol('reminder_not_sent_part_needed',$tlang);
				break;
			case 'part_reserve':
				$mail['disclaimer']=load_language_symbol('reminder_not_sent_part_reserve',$tlang);
				break;
			default:
				$mail['disclaimer']="";
			}

		if ($mail['disclaimer']) $sub_notice=load_language_symbol('subject_for_session_reminder_error_notice',$tlang);
			else $sub_notice=load_language_symbol('subject_for_session_reminder_ok_notice',$tlang);

		$recipient=$mail['email'];

		$subject=$sub_notice.' '.$mail['experiment_name'].' '.$mail['session_name'];

		$mailtext=load_mail("admin_session_reminder_notice",$tlang);
			if(empty($mail['experiment_id']) && !empty($line['experiment_id'])) $mail['experiment_id']=$line['experiment_id'];
        	$message=process_mail_template($mailtext,$mail);

        	$headers="From: ".$settings['support_mail']."\r\n";

        	$done=experimentmail__mail($recipient,$subject,$message,$headers);
		}

	return $done;

}

function experimentmail__send_invitation_mail($mail,$part,$exp,$inv_text,$slist,$footer) {
	global $settings;
	// echo "experimentmail__send_invitation_mail\n";

	$part['edit_link']=experimentmail__build_edit_link($part['participant_id']);
	$part['experiment_name']=$exp['experiment_public_name'];
	$part['sessionlist']=$slist;
        $part['link']=experimentmail__build_lab_registration_link($part['participant_id']);
		query_makecolumns(table('experiments'),"suppl_exp_description","TEXT NOT NULL");
		if(!empty(experiment__get_value($exp['experiment_id'],'suppl_exp_description'))) $part['link']=str_replace("participant_show.php?","description.php?expe=".$exp['experiment_public_name']."&",$part['link']);

	// split in subject and text
        $subject=stripslashes(str_replace(strstr($inv_text,"\n"),"",$inv_text));
        $mailtext=stripslashes(substr($inv_text,strpos($inv_text,"\n")+1,strlen($inv_text)));
	$recipient=$part['email'];
	if(empty($part['experiment_id'])) $part['experiment_id']=$exp['experiment_id'];

        $message=process_mail_template($mailtext,$part)."\n".process_mail_template($footer,$part);

	if ($exp['sender_mail']) $sender=$exp['sender_mail']; else {
		$sender="";
		if(!empty($settings['sender_name'])) $sender.=$settings['sender_name']." <";
		$sender.=$settings['support_mail'];
		if(!empty($settings['sender_name'])) $sender.=">";
	}

        $headers="From: ".$sender."\r\n";

	// echo $headers.$recipient."\n".$subject."\n\n".$message;

        $done=experimentmail__mail($recipient,$subject,$message,$headers);
	$done2=experimentmail__update_invited_flag($mail);
	$query="UPDATE ".table('participants')."
			SET last_invitation_time=UNIX_TIMESTAMP(), last_invitation_exp='".$part['experiment_id']."' WHERE participant_id='".$part['participant_id']."'";
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
	// var_dump($done,$done2);
	return $done;

}


function experimentmail__send_bulk_mail($mail,$part,$bulk_mail,$footer,$exp=[]) {
        global $settings;

        $part['edit_link']=experimentmail__build_edit_link($part['participant_id']);
		
		if(empty($part['experiment_id']) && !empty($mail['experiment_id'])) $part['experiment_id']=$mail['experiment_id'];
		if(empty($part['session_id']) && !empty($mail['session_id'])) $part['session_id']=$mail['session_id'];
		
		if(!empty($part['session_id'])) {
			$session=orsee_db_load_array("sessions",$part['session_id'],"session_id");
			$part['session_date']=session__build_name($session,$part['language']);
		}

        // split in subject and text
        $subject=stripslashes($bulk_mail['bulk_subject']);
        $mailtext=stripslashes($bulk_mail['bulk_text']);
        $recipient=$part['email'];

        $message=process_mail_template($mailtext,$part)."\n".process_mail_template($footer,$part);

        $sender="";
		if(!empty($exp['sender_mail'])) $sender=$exp['sender_mail']; else {
			if(!empty($settings['sender_name'])) $sender.=$settings['sender_name']." <";
			$sender.=$settings['support_mail'];
			if(!empty($settings['sender_name'])) $sender.=">";
		}

        $headers="From: ".$sender."\r\n";

        $done=experimentmail__mail($recipient,$subject,$message,$headers);
        return $done;
}

function experimentmail__update_invited_flag($mail) {

        $query="UPDATE ".table('participate_at')."
		SET invited='y' 
                WHERE participant_id='".$mail['mail_recipient']."'
		AND experiment_id='".$mail['experiment_id']."'";
        $result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
        return $result;
}

function experimentmail__build_edit_link($participant_id,$page="participant_edit.php") {
	global $settings__root_url;
	$edit_link=$settings__root_url."/".$GLOBALS['settings__public_folder']."/".$page."?p=".url_cr_encode($participant_id);
	return $edit_link;
}

function experimentmail__build_lab_registration_link($participant_id) {
        global $settings__root_url;
        $reg_link=$settings__root_url."/".$GLOBALS['settings__public_folder']."/participant_show.php?p=".url_cr_encode($participant_id);
        return $reg_link;
}

function experimentmail__mail_edit_link($participant_id,$supmailmessage='',$addtolink='',$page='',$subject='') {
	global $lang, $authdata, $settings;

	if(empty($page)) $page="participant_edit.php";
	if(empty($subject)) $subject=$lang['subject_for_edit_link_mail'];
	
	$participant=orsee_db_load_array("participants",$participant_id,"participant_id");
	$participant['edit_link']=experimentmail__build_edit_link($participant_id,$page);
	// if(!empty($addtolink)) $participant['edit_link'].="&".$addtolink;

	if (isset($authdata['language']) && $authdata['language']) $maillang=$authdata['language'];
                        else $maillang=$participant['language'];
        $mailtext=load_mail("public_mail_footer",$maillang);
		$message="";
		if(!empty($supmailmessage)) {
			$message.=$supmailmessage.PHP_EOL;
			if(!empty($addtolink)) $message.=$participant['edit_link']."&".$addtolink.PHP_EOL;
		}
		elseif(!empty($addtolink)) $participant['edit_link'].="&".$addtolink;
        $message.=process_mail_template($mailtext,$participant);
		
		$sendermail="";
		if(!empty($settings['sender_name'])) $sendermail.=$settings['sender_name']." <";
		$sendermail.=$settings['support_mail'];
		if(!empty($settings['sender_name'])) $sendermail.=">";

        $headers="From: ".$sendermail."\r\n";
		
		// var_dump($participant['email'],$lang['subject_for_edit_link_mail'],$subject,$addtolink,$supmailmessage,$message,$headers); exit;

        experimentmail__mail($participant['email'],$subject,$message,$headers);

}

function experimentmail__mail_new_link($participant_id,$supmailmessage='',$addtolink='',$page='',$subject='') {
	global $lang, $authdata, $settings;

	if(empty($page)) $page="participant_edit.php";
	if(!empty($subject) && substr($subject,0,1)=="#") {
		if(!empty($lang['subject_for_new_link_mail'])) $subject=$lang['subject_for_new_link_mail'];
		else $subject=substr($subject,1);
	}
	if(empty($subject) && !empty($lang['subject_for_new_link_mail'])) $subject=$lang['subject_for_new_link_mail'];
	
	$participant=orsee_db_load_array("participants",$participant_id,"participant_id");
	$participant['new_link']=experimentmail__build_edit_link($participant_id,$page);
	$participant['supinfo']=$supmailmessage;
	// if(!empty($addtolink)) $participant['new_link'].="&".$addtolink;

	if (isset($authdata['language']) && $authdata['language']) $maillang=$authdata['language'];
                        else $maillang=$participant['language'];
        $mailtext=load_mail("public_newlink_mail",$maillang);
		$message="";
		if(!empty($addtolink)) $participant['new_link'].="&".$addtolink;
        $message.=process_mail_template($mailtext,$participant);
		$message=$message."\n".experimentmail__get_mail_footer($participant_id);
		
		$sendermail="";
		if(!empty($settings['sender_name'])) $sendermail.=$settings['sender_name']." <";
		$sendermail.=$settings['support_mail'];
		if(!empty($settings['sender_name'])) $sendermail.=">";

        $headers="From: ".$sendermail."\r\n";
		
		// var_dump($participant['email'],$lang['subject_for_edit_link_mail'],$subject,$addtolink,$supmailmessage,$message,$headers); exit;

        experimentmail__mail($participant['email'],$subject,$message,$headers);

}

function experimentmail__get_language($partlang) {
	global $authdata;
        if (isset($authdata['language']) && $authdata['language']) $maillang=$authdata['language'];
                        else $maillang=$partlang;
	return $maillang;	
}

function experimentmail__get_admin_language($adminlang) {
        global $authdata, $settings;

        if (isset($authdata['language']) && $authdata['language']) $maillang=$authdata['language'];
                elseif ($adminlang) $maillang=$adminlang;
		else $maillang=$settings['admin_standard_language'];
	return $maillang;
}

function experimentmail__get_mail_footer($participant_id) {
        global $lang, $settings;

        $participant=orsee_db_load_array("participants",$participant_id,"participant_id");
		if($participant===false) $participant=array();
        $participant['edit_link']=experimentmail__build_edit_link($participant_id);
		// var_dump($lang,$settings);
        $maillang=empty($participant['language'])?$lang["lang"]:experimentmail__get_language($participant['language']);

        $mailtext=load_mail("public_mail_footer",$maillang);
        $footer=process_mail_template($mailtext,$participant);
	return $footer;
}


function experimentmail__get_admin_footer($maillang='',$admin_id=0) {

	if ($admin_id>0) 
		$admin=orsee_db_load_array("admin",$admin_id,"admin_id");
		else $admin=array();

        if (!$maillang) $maillang=experimentmail__get_admin_language($admin['language']);

        $mailtext=load_mail("admin_mail_footer",$maillang);
        $footer=process_mail_template($mailtext,$admin);
        return $footer;
}

function experimentmail__confirmation_mail($participant_id) {
	global $authdata, $lang, $settings__root_url, $settings;

	$participant=orsee_db_load_array("participants_temp",$participant_id,"participant_id");

	$pform_fields=participant__load_result_table_fields('email');
	foreach ($pform_fields as $f) {
		if(preg_match("/(radioline|select_list|select_lang)/",$f['type']) && isset($f['lang'][$participant[$f['mysql_column_name']]]))
			$participant[$f['mysql_column_name']]=$f['lang'][$participant[$f['mysql_column_name']]];
	}

	$exptypes=explode(",",$participant['subscriptions']);
        $typenames=load_external_experiment_type_names(true,false,true);
	$invnames=array();
        foreach ($exptypes as $type) $invnames[]=$typenames[$type];
	$participant['invitations']=implode(", ",$invnames);

        $participant['registration_link']=$settings__root_url."/".$GLOBALS['settings__public_folder']."/participant_confirm.php?p=".url_cr_encode($participant_id);


	$maillang=experimentmail__get_language($participant['language']);

	$mailtext=load_mail("public_system_registration",$maillang);
	$message=process_mail_template($mailtext,$participant);

	$sendermail="";
	if(!empty($settings['sender_name'])) $sendermail.=$settings['sender_name']." <";
	$sendermail.=$settings['support_mail'];
	if(!empty($settings['sender_name'])) $sendermail.=">";
	$headers="From: ".$sendermail."\r\n";

        experimentmail__mail($participant['email'],$lang['registration_email_subject'],$message,$headers);
}

function experimentmail__confirmation_mail_session($participant_id,$session_id,$confirmation_link,$subject="",$template_name="",$session_name="") {
	global $authdata, $lang, $settings__root_url, $settings;

	$participant=orsee_db_load_array("participants",$participant_id,"participant_id");
	$session=orsee_db_load_array("sessions",$session_id,"session_id");
	$experiment=orsee_db_load_array("experiments",$session['experiment_id'],"experiment_id");

    $participant['confirmation_link']=$confirmation_link;
	if(empty($template_name)) $template_name="session_unregister_confirmation_mail";
	if(empty($subject)) $subject=lang('confirmation_needed');
	if(empty($session_name)) $session_name=session__build_name($session).' ('.lang('experiment',false).' '.experiment__get_public_name($session['experiment_id']).')';
    $participant['session_name']=$session_name;
	


	$maillang=experimentmail__get_language($participant['language']);

	$mailtext=load_mail($template_name,$maillang);
	$message=process_mail_template($mailtext,$participant);

	$sendermail="";
	if ($experiment['sender_mail']) $sendermail=$experiment['sender_mail']; else {
		$sendermail="";
		if(!empty($settings['sender_name'])) $sendermail.=$settings['sender_name']." <";
		$sendermail.=$settings['support_mail'];
		if(!empty($settings['sender_name'])) $sendermail.=">";
	}	
	$headers="From: ".$sendermail."\r\n";

       experimentmail__mail($participant['email'],$subject,$message,$headers);
}


function experimentmail__experiment_registration_mail($participant_id,$session_id,$experiment_id) {
	global $lang, $settings;

        // load experiment
        $experiment=orsee_db_load_array("experiments",$experiment_id,"experiment_id");

        // load participant
        $participant=orsee_db_load_array("participants",$participant_id,"participant_id");
        // load session
        $session=orsee_db_load_array("sessions",$session_id,"session_id");

	$maillang=experimentmail__get_language($participant['language']);

	$experimentmail=$participant;

	$pform_fields=participant__load_result_table_fields('email');
	foreach ($pform_fields as $f) {
		if(preg_match("/(radioline|select_list|select_lang)/",$f['type']) && isset($f['lang'][$participant[$f['mysql_column_name']]]))
			$participant[$f['mysql_column_name']]=$f['lang'][$participant[$f['mysql_column_name']]];
	}

        // define some more shortcuts
        $experimentmail['laboratory']=laboratories__get_laboratory_name($session['laboratory_id']);
        $experimentmail['location']=laboratories__get_laboratory_address($session['laboratory_id']);
        $experimentmail['session']=session__build_name($session,$maillang);
        $experimentmail['experiment']=$experiment['experiment_public_name'];
		$experimentmail['duration']=$session['session_duration_hour'].":".$session['session_duration_minute'];
		$experimentmail['session_id']=$session_id;
		$experimentmail['supinfo']="";
		if(!empty($experiment['confirm_mail_supinfo'])) {
			$experimentmail['supinfo']="\r\n".str_replace("<line>","\r\n",$experiment['confirm_mail_supinfo'])."\r\n";
		}
		$experimentmail['unregister_info']="\n";
		$unregister_end_time=sessions__get_unregister_end($session);
		if(time()<$unregister_end_time) {
			$unregister_end=time__format($lang['lang'],'',false,false,true,false,$unregister_end_time);
			$part_show_link=$GLOBALS['settings__root_url']."/".$GLOBALS['settings__public_folder']."/participant_show.php?p=".url_cr_encode($participant_id);
			$experimentmail['unregister_info'].=lang('you_may_unsubscribe_from_this_session_before').' '.$unregister_end.'. ';
			$experimentmail['unregister_info'].=lang('in_order_to_do_it_visit_the_following_link').': '.$part_show_link.' '.PHP_EOL;
		}
		$sess_remark_list=sessions__get_remark_list($experimentmail['session_id']);
		foreach($sess_remark_list as $srem) {
			$asrem=explode(":",$srem);
			if($asrem[0]=="confirmmail_message" && count($asrem)>1) $experimentmail['supinfo'].="\r\n".str_replace("<line>","\r\n",str_replace("<comma>",",",$asrem[1]))."\r\n";
		}
		if(empty($experimentmail['experiment_id'])) $experimentmail['experiment_id']=$experiment_id;
        $mailtext=load_mail("public_experiment_registration",$maillang);
        $message=process_mail_template($mailtext,$experimentmail);

	$message=$message."\n".experimentmail__get_mail_footer($participant_id);

	if ($experiment['sender_mail']) $sendermail=$experiment['sender_mail']; else {
			$sendermail="";
			if(!empty($settings['sender_name'])) $sendermail.=$settings['sender_name']." <";
			$sendermail.=$settings['support_mail'];
			if(!empty($settings['sender_name'])) $sendermail.=">";
		}

        $headers="From: ".$sendermail."\r\n";

        experimentmail__mail($participant['email'],$lang['registration_email_subject'],$message,$headers);
}


function experimentmail__send_registration_notice($line) {
        global $settings;

	$reg=experiment__count_participate_at($line['experiment_id'],$line['session_id']);

	$experimenters=explode(",",$line['experimenter_mail']);

        foreach ($experimenters as $experimenter) {

                $mail=orsee_db_load_array("admin",$experimenter,"adminname");

                $tlang= ($mail['language']) ? $mail['language'] : $settings['admin_standard_language'];
                $lang=load_language($tlang);

                $mail['session_name']=session__build_name($line,$tlang);
                $mail['experiment_name']=$line['experiment_name'];
                $mail['registered'] = $reg;
		$mail['status']=session__get_status($line,$tlang,$reg);
		$mail['needed']=$line['part_needed'];
		$mail['reserve']=$line['part_reserve'];
		if(empty($mail['experiment_id'])) $mail['experiment_id']=$line['experiment_id'];

                $subject=load_language_symbol('subject_for_registration_notice',$tlang);
		$subject.=' '.$mail['experiment_name'].', '.$mail['session_name'];

                $recipient=$mail['email'];

                $mailtext=load_mail("admin_registration_notice",$tlang)."\n".
                                        experimentmail__get_admin_footer($tlang)."\n";
                $message=process_mail_template($mailtext,$mail);

		$now=time();
		$list_name=$lang['participant_list_filename'].' '.date("Y-m-d",$now);
        	$list_filename=str_replace(" ","_",$list_name).".pdf";
        	$list_file=pdfoutput__make_part_list($line['experiment_id'],$line['session_id'],'registered','lname,fname',true,$tlang);

		$done=experimentmail__mail_attach($recipient,$settings['support_mail'],$subject,$message,$list_filename,$list_file);
                }

	// update session table : reg_notice_sent
        $query="UPDATE ".table('sessions')." SET reg_notice_sent='y' WHERE session_id='".$line['session_id']."'";
        $done2=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: ".mysqli_error($GLOBALS['mysqli']));

        return $done;

}


function experimentmail__send_calendar() {
	global $lang, $settings;

        $now=time();

	if (isset($settings['emailed_calendar_included_months']) && $settings['emailed_calendar_included_months']>0) 
			$number_of_months=$settings['emailed_calendar_included_months']-1;
	else $number_of_months=1;

        $cal_name=$lang['experiment_calendar'].' '.date("Y-m-d",$now);
        $cal_filename=str_replace(" ","_",$cal_name).".pdf";

	// save old lang
	$old_lang=$lang['lang'];
	$maillang=$old_lang;
	$cal_name=$lang['experiment_calendar'].' '.date("Y-m-d",$now);
        $cal_filename=str_replace(" ","_",$cal_name).".pdf";
	// var_dump($cal_name,$cal_filename,$number_of_months);
	$cal_file=pdfoutput__make_calendar($now,false,true,$number_of_months,true);
	// var_dump($cal_name,$from,$cal_file);
        $from=$settings['support_mail'];


	// get experimenters who want to receive the calendar
     	$query="SELECT *
      		FROM ".table('admin')."
      		WHERE get_calendar_mail='y'
		ORDER BY language";

	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

	$i=0; $rec_count=mysqli_num_rows($result);
	while ($admin = mysqli_fetch_assoc($result)) {
			if ($admin['language'] != $maillang) {
				$maillang=$admin['language'];
				$lang=load_language($maillang);
				$cal_name=$lang['experiment_calendar'].' '.date("Y-m-d",$now);
        			$cal_filename=str_replace(" ","_",$cal_name).".pdf";
				$cal_file=pdfoutput__make_calendar($now,false,true,$number_of_months,true);
				}

			$mailtext=load_mail("admin_calendar_mailtext",$maillang).
					"\n".
					experimentmail__get_admin_footer($maillang)."\n";
			$message=process_mail_template($mailtext,$admin);
			$done=experimentmail__mail_attach($admin['email'],$from,$cal_name,$message,$cal_filename,$cal_file);
			if (!$done) {
				echo "cannot send mail to ".$admin['email'].", from: ".$from.", cal_name: ".$cal_name.", message: ".$message.", cal_filename: ".$cal_filename;
			}
			$i++;
                        }

	mysqli_free_result($result);
	
	if ($maillang!=$old_lang) $lang=load_language($old_lang);

	return $cal_name." sent to ".$i." out of ".$rec_count." administrators\n";
}



function experimentmail__send_participant_statistics() {
	global $lang, $settings;
	$now=time();

        // save old lang
        $old_lang=$lang['lang'];
        $maillang=$old_lang;
	$statistics=stats__textstats_all();
	$subject=load_language_symbol('participant_statistics',$maillang).' '.
                                        time__format($maillang,"",false,true,true,false,$now);

        $from=$settings['support_mail'];
	$headers="From: ".$from."\r\n";

        // get experimenters who want to receive the statistics
        $query="SELECT *
                FROM ".table('admin')."
                WHERE get_statistics_mail='y'
                ORDER BY language";

        $result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

        $i=0; $rec_count=mysqli_num_rows($result);
        while ($admin = mysqli_fetch_assoc($result)) {
                        if ($admin['language'] != $maillang) {
                                $maillang=$admin['language'];
                                $lang=load_language($maillang);
				$statistics=stats__textstats_all();
				$subject=load_language_symbol('participant_statistics',$maillang).' '.
					time__format($maillang,"",false,true,true,false,$now);
                                }

                        $mailtext=load_mail("admin_participant_statistics_mailtext",$maillang).
                                        "\n\n".
					$statistics."\n".
                                        experimentmail__get_admin_footer($maillang)."\n";
                        $message=process_mail_template($mailtext,$admin);
                        $done=experimentmail__mail($admin['email'],$subject,$message,$headers);
                        if ($done) $i++;
                        }

        mysqli_free_result($result);

        if ($maillang!=$old_lang) $lang=load_language($old_lang);

        return "statistics sent to ".$i." out of ".$rec_count." administrators\n";
}

function experimentmail__bulk_mail_form($experiment_id="",$session_id="") {
	global $lang;
	$href="participants_bulk_mail.php";
	$hrefsearch=[];
	if(!empty($experiment_id)) $hrefsearch[]="experiment_id=".$experiment_id;
	if(!empty($session_id)) $hrefsearch[]="session_id=".$session_id;
	if(count($hrefsearch)>0) $href.="?".implode("&",$hrefsearch);

	echo '<A HREF="'.$href.'">'.$lang['send_mail_to_listed_participants'].'</A>';
}



?>

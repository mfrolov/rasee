<?php
// part of orsee. see orsee.org

function getmicrotime()
{
   list($usec, $sec) = explode(" ",microtime());
   return ((float)$usec + (float)$sec);
}

function support_mail_link() {
	global $settings;
	$sml='<A HREF="mailto:'.$settings['support_mail'].'">'.$settings['support_mail'].'</A>';
	return $sml;
}

function multi_array_sort(&$data, $sortby) {
   	static $sort_funcs = array();

   	if (is_array($sortby)) {
       		$sortby = join(',',$sortby);
   		}

   	if (empty($sort_funcs[$sortby])) {
       		$code = "\$c=0;";
       		$sortby_arr=explode(',', $sortby);
       		foreach ($sortby_arr as $key) {
           		$code .= "\$c1 = strcasecmp(\$a['$key'],\$b['$key']);  if ( \$c1 != 0 && !empty(\$a['$key'])  && !empty(\$b['$key']) ) {\$c = \$c1; return \$c;}\n";
       			}
       		//$code .= 'return $c;';
       		$sort_func = $sort_funcs[$sortby] = function($a,$b) use($code) {eval($code); return $c; }; //create_function('$a, $b', $code);
   	 } else {
       		$sort_func = $sort_funcs[$sortby];
   		}
   	$sort_func = $sort_funcs[$sortby];
	// var_dump($sortby,$code,array_map(function($a) use($sortby) {return $a[$sortby];},$data));
   	uasort($data, $sort_func);
	// var_dump(array_map(function($a) use($sortby) {return $a[$sortby];},$data),$sort_func);
}


function time__unixtime_to_time_package($unixtime=-1) {
	if ($unixtime<0) $unixtime=time();
	$tarr=getdate($unixtime);
        $ta['day']=$tarr['mday'];
        $ta['month']=$tarr['mon'];
        $ta['year']=$tarr['year'];
        $ta['minute']=$tarr['minutes'];
        $ta['hour']=$tarr['hours'];
        $ta['second']=$tarr['seconds'];
	return $ta;
}

// unused?
function time__build_session_timestring($unixtime="") {
	if (!$unixtime) $unixtime=time();
	$timestring=date("YmdHi",$unixtime);
	return $timestring;
}


function time__get_timepack_from_pack ($from_package,$prefix="session_start_") {
	$vars=array('day','month','year','minute','hour');
	$to_package=array();
	foreach ($vars as $var) { if (isset($from_package[$prefix.$var])) $to_package[$var]=$from_package[$prefix.$var]; else $to_package[$var]=0; }
        $to_package['second']='0';
	return $to_package;
}


function time__load_session_time($from_package) {
	return time__get_timepack_from_pack ($from_package,'session_start_');
}

function time__add_packages($orig,$plus,$return_unixtime=false) {
	$orig_pack=array('year'=>0,'month'=>0,'day'=>0,'hour'=>0,'minute'=>0,'second'=>0);
	$plus_pack=array('day'=>0,'hour'=>0,'minute'=>0,'second'=>0);
	foreach ($orig as $key=>$value) $orig_pack[$key]=$value;
	foreach ($plus as $key=>$value) $plus_pack[$key]=$value;

	$orig_time=time__time_package_to_unixtime($orig_pack);
	$plus_time=$plus_pack['day']*24*60*60+$plus_pack['hour']*60*60+$plus_pack['minute']*60+$plus_pack['second'];
	$new_time=$orig_time+$plus_time;
	if ($return_unixtime) return $new_time;
	else return time__unixtime_to_time_package($new_time);

}


function time__format_session_time($session_id,$language="",$package="") {
	global $authdata, $settings;
        if (!$language) {
                if (isset($authdata['language']) && $authdata['language']) $thislang=$authdata['language'];
                        else $thislang=$settings['public_standard_language'];
                }
                else {
                $thislang=$language;
                }
	if ($session_id)
        	$pack=orsee_db_load_array("sessions",$session_id,"session_id");
	   else $pack=$package;

        $session_time=time__load_session_time($pack);
	$timestring=time__format($thislang,$session_time,false,false,true,false);
        return $timestring;
}

function time__time_package_to_unixtime($tarray) {
extract($tarray);
$unixtime=mktime($hour,$minute,$second,$month,$day,$year);
return $unixtime;
}


function time__format($lang,$ttime,$hide_date=false,$hide_time=false,$hide_second=true,$hide_year=false,$unixtime="") { 


	if ($unixtime) $ta=time__unixtime_to_time_package($unixtime);
		else $ta=$ttime;

	$tvars=array('day','month','year','hour','minute','second');
	foreach ($tvars as $t) { if (!isset($ta[$t])) $ta[$t]=0; }

	$ta['day']=helpers__pad_number($ta['day'],2);
	$ta['month']=helpers__pad_number($ta['month'],2);
	$ta['year']=helpers__pad_number($ta['year'],4);
    if (isset($ta['minute']) && (string) $ta['minute']!="" && isset($ta['hour']) && (string) $ta['hour']!="") { 
					  $time_ex=true;
					  $ta['hour']=helpers__pad_number($ta['hour'],2);
					  $ta['minute']=helpers__pad_number($ta['minute'],2);
					  }

	if (isset($ta['second'])) $ta['second']=helpers__pad_number($ta['second'],2);

	if (!$lang) {
		global $expadmindata, $settings;
		if ($expadmindata['language']) $lang=$expadmindata['language'];
			else $lang=$settings['public_standard_language'];
		}

	$datestring="";

	$ready=false;

	// de - german
	if ($lang=="de") {
		$ready=true;
		if (!$hide_date) {
			$datestring.=$ta['day'].'.'.$ta['month'].'.';
			if (!$hide_year) $datestring.=$ta['year'];
			$datestring.=" ";
		}
		if ((!$hide_time) && $time_ex) {
			$datestring.=$ta['hour'].':'.$ta['minute'];
			if (!$hide_second && $ta['second']) $datestring.=':'.$ta['second'];
		}
	}

	// es - spanish
	if ($lang=="es" || $lang=="fr") {
		$ready=true;
		if (!$hide_date) {
			global $expadmindata;
			if($lang=="fr" && empty($expadmindata['language'])) {
				$weekdays=array("dimanche","lundi","mardi","mercredi","jeudi","vendredi","samedi");
				$cwdindex=date('w',time__time_package_to_unixtime($ta));
				$datestring.=$weekdays[$cwdindex].' '; //'le '.
			}
			$datestring.=$ta['day'].'/'.$ta['month'];
			if (!$hide_year) $datestring.='/'.$ta['year'];
			$datestring.=" ";
		}
		if (!$hide_time && $time_ex) {
			$datestring.=$ta['hour'].':'.$ta['minute'];
			if (!$hide_second && $ta['second']) $datestring.=':'.$ta['second'];
		}
	}

	// default en - english as AM/PM
	if (!$ready) {
		$ready=true;
		if (!$hide_date) {
		
			// US-ENGLISH MM/DD/YYYY
			$datestring.=$ta['month'].'/'.$ta['day'];
			if (!$hide_year) $datestring.='/'.$ta['year'];
			
			// OTHER ENGLISH DD/MM/YYYY (comment/uncomment as needed)
			//$datestring.=$ta['month'].'/'.$ta['day'];
			//if (!$hide_year) $datestring.='/'.$ta['year'];
			
			$datestring.=" ";
		}
		if (!$hide_time && $time_ex) {
		
			// 12-hour AM/PM format
			if ($ta['hour']>12) $hour=$ta['hour']-12; elseif ($ta['hour']==0) $hour=12; else $hour=$ta['hour'];
			if ($ta['hour']>=12) $ampm='pm'; else $ampm='am';
			$datestring.=$hour.':'.$ta['minute'];
			if (!$hide_second && $ta['second']) $datestring.=':'.$ta['second'];
			$datestring.=$ampm;
			
			// 24-hour military format (comment/uncomment as needed)
			//$datestring.=$ta['hour'].':'.$ta['minute'];
			//if (!$hide_second && $ta['second']) $datestring.=':'.$ta['second'];
		}
	}


	return $datestring;
}

 
function helpers__pad_number() {

if (func_num_args()>0) { $help=func_get_arg(0); $number="$help"; } else { $number="1"; }
if (func_num_args()>1) $fillzeros=func_get_arg(1); else $fillzeros=2;

$padnumber="";
$length=strlen($number);
while ($length<$fillzeros) {
	$padnumber=$padnumber."0";
	$length++;
	}
$padnumber=$padnumber.$number;
return $padnumber;
}


// Url-Coding-Functions

function get_unique_id($table,$idcol) {
           $exists=true;
           srand ((double)microtime()*1000000);
           while ($exists) {
                $crypt_id = "/";
		while (preg_match("/(\/|\\.)/",$crypt_id)) {
                        $id = rand();
                        $crypt_id=unix_crypt($id);
                        }

                $query="SELECT ".$idcol." FROM ".table($table)."
                        WHERE ".$idcol."='".$id."'";
                $line=orsee_query($query);
                if (isset($line[$idcol])) $exists=true; else $exists=false;
                }
        return $id;
}


// encryption
function unix_crypt($value,$sid="cd") {
	$encrypted=crypt($value,$sid);
	return $encrypted;
}

// Url-Encode
function url_cr_encode($var,$sid="cd") {
	$t=unix_crypt($var,$sid);
	// $encoded=urlencode($t);
	return link_en_de_crypt($t.'_'.time());
	}

// Url-Decode
function url_cr_decode($value,$temp=false,$sid="cd") {
	$decoded="";
	if(strlen($value)>13) {
		$initial_cpid_decrypted=link_en_de_crypt($value,2);
		if($initial_cpid_decrypted!==false) {
			if(is_array($initial_cpid_decrypted) && count($initial_cpid_decrypted)>1) $value=explode('_',$initial_cpid_decrypted[1])[0];
			else {
				$initial_cpid_array=explode('_',$initial_cpid_decrypted);
				$value=$initial_cpid_array[0];
			}
			// var_dump($value,$initial_cpid_decrypted);
		}
	}
	if (substr($value,0,2)!=$sid) $value=$sid.substr($value,1);
	if ($temp) {
		$query="SELECT participant_id FROM ".table('participants_temp')." 
                	WHERE participant_id_crypt='".mysqli_real_escape_string($GLOBALS['mysqli'],$value)."'";
		$decarray=orsee_query($query);
		if($decarray!==false) $decoded=$decarray['participant_id'];
	}

	if (!$decoded) {
		$query="SELECT participant_id FROM ".table('participants')." 
                 	WHERE participant_id_crypt='".mysqli_real_escape_string($GLOBALS['mysqli'],$value)."'";
		$decarray=orsee_query($query);
		if($decarray!==false) $decoded=$decarray['participant_id'];
		}
	return $decoded;
}

// Url-Decodec session id
function url_cr_decode_session($value) {
	$query="SELECT session_id FROM ".table('sessions')."
                WHERE session_id_crypt='".mysqli_real_escape_string($GLOBALS['mysqli'],$value)."'";
	$decarray=orsee_query($query);
        $decoded=$decarray['session_id']??0;
	return $decoded;
}


function helpers__update_encrypted($table) {

	$ids=array();

	switch ($table) {

		case "sessions": $idvar="session_id";
			 	break;
		case "participants": $idvar="participant_id";
				break;
		case "participants_temp": $idvar="participant_id";
				break;
		default: return 0;
	}

	$query="SELECT ".$idvar." FROM ".table($table);
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
	while ($line=mysqli_fetch_assoc($result)) {
		$ids[$line[$idvar]]=$line[$idvar];
		}

	foreach ($ids as $id) {
		$query="UPDATE ".table($table)." SET ".$idvar."_crypt='".unix_crypt($id)."'
			WHERE ".$idvar."='".$id."'";
		$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
		}
}

function helpers__scramblemail($address) { 
 	$address = "<a class=\"small\" href=\"mailto:$address\">";
 	$temp =  chunk_split($address,3,"##"); 
 	$temp_array =  explode("##",$temp);
 	$scrambled="";
 	
 	foreach($temp_array as $piece) 
 		{ $scrambled.="+'$piece'"; } 
 	$scrambled =  substr($scrambled,1, strlen($scrambled));
 
 	$result = "<script type='text/javascript'>";
 	$result.="<!--\n"; 
 	$result.= "document.write($scrambled);\n"; 
 	$result.="-->"; 
 	$result.="</SCRIPT>"; 
 	echo $result; 
} 

function strip_tags_array($array,$exempt=array()) {
	foreach($array as $k=>$v) {
		if (!in_array($k,$exempt)) {
	   		if (is_array($v)) $array[$k]=strip_tags_array($v);
    		else {
    			$v=strip_tags($v);
    			$v=str_replace(array('&','<','>','"',"'",'/'),
    						array('&amp;','&lt;','&gt;','&quot;','&#x27;',' &#x2F;'),
    						$v);
    			$array[$k]=$v;
    		}
    	}
	}
    return $array;
}

function dossier_name($id,$long=0)
{
		global $lang;
		if(empty($id)) return '-';
		$name="";
		$query="SELECT * FROM ".table('payment_files')."
						WHERE `id`=".$id;
		$line=orsee_query($query);
		if($line===false) {
			return lang('payment_file_deleted').' (id='.$id.')';
		}
		// echo "</option></select>"; var_dump($line);
		$name.=$line['year'];
		$name.=($long>0 && $lang['lang']=='fr')?' N°':'_n';
		$name.=$line['number'];
		if(!empty($line['tranche'])) {
			if($long<2) $name.='_'.$line['tranche'];
			elseif($lang['lang']=='fr') {
				if($line['tranche']<11) {
					$numerals=array("première","deuxième","troisième","quatrième","cinqième","sixième","septième","huitième","neuvième","dixième");
					$name.=', '.$numerals[$line['tranche']-1].' demande de fond';
				}
				else $name.=', demande de fond n°'.$line['tranche'];
			}
			else  $name.=' / '.$line['tranche'];
		}
		if(!empty($line['surname']) && $long>0) {
			 $name.=' ['.$line['surname'].']';
		}
		return $name;
}

function dossier_closed($id)
{
	if(empty($id)) return null;
	$name="";
	$query="SELECT closed FROM ".table('payment_files')."
					WHERE `id`=".$id;
	$closed=true;
	$line=orsee_query($query);
	if($line!==false && isset($line['closed'])) $closed=($line['closed']!=0);
	return $closed;
}

function table_fields($tablenames)
{
	global $site__database_database;
	if(!is_array($tablenames)) $tablenames=explode(",",$tablenames);
	$existing_cols=array();
	foreach($tablenames as $tablename) {
		$query="SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS 
				WHERE table_name='".$tablename."' 
				AND table_schema = '".$site__database_database."'";
		$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$query);
		if(mysqli_num_rows($result)==0) {continue;}
		while ($line=mysqli_fetch_assoc($result)) $existing_cols[]=$line['COLUMN_NAME'];
		// var_dump($existing_cols);
		mysqli_free_result($result);
	}
	return $existing_cols;
}

function helpers_pay_accounts($dossier='',$include_empty=false)
{
	operation::operation_tables();
	$q="SELECT shortname,name FROM ".table("permanent_pay_accounts")."";
	$res=array();
	if($include_empty) $res+=array(""=>"-");
	$lines=orsee_query($q,"return_key_val");
	foreach($lines as $cval) $res+=$cval;
	if($dossier=='1') return $res;
	if(!empty($dossier)) {
		$q="SELECT money_responsible FROM ".table("payment_files")." WHERE id='$dossier'";
		$line=orsee_query($q);
		$res+=array($line["money_responsible"]=>admin__load_name_by_val($line["money_responsible"]));
	}
	$q="SELECT money_responsible FROM ".table("payment_files")."";
	$lines=orsee_query($q,"return_first_elem");
	foreach($lines as $cval) $res+=array($cval=>admin__load_name_by_val($cval));
	return $res;
}

function helpers__pay_account_longname($colval)
{
	$rname=admin__load_name_by_val($colval); if(!empty($rname)) $colval=$rname; else {$xline=orsee_query("SELECT name FROM ".table("permanent_pay_accounts")." WHERE shortname='$colval'"); if($xline!==false) $colval=lang($xline["name"]); else {$xline=orsee_query("SELECT name FROM ".table("pay_accounts")." WHERE shortname='$colval'"); if($xline!==false) $colval=lang($xline["name"]);}}
	return $colval;
}

function helpers__regiedavance_budget()
{
	$res=0;
	$q="SELECT budget FROM ".table('payment_files')." WHERE id=1 AND closed='0'";
	$line=orsee_query($q);
	if($line!==false && !empty($line['budget'])) $res=$line['budget'];
	return $res;
}
function helpers__regiedavance_amount_left()
{
	$q="SELECT closed FROM ".table('payment_files')." WHERE id=1";
	$closed=true;
	$line=orsee_query($q);
	if($line!==false && isset($line['closed'])) $closed=($line['closed']!=0);
	$acline=orsee_query("SELECT SUM(amount) as labsafe_amount FROM ".table("pay_accounts")." WHERE shortname='lab' AND payment_file='1'");
	// var_dump($closed); var_dump($acline);
	$am=0; if(!$closed && $acline!==false && !empty($acline['labsafe_amount'])) $am=$acline['labsafe_amount'];
	return $am;
}

function return_same($x)
{
	return $x;
}

function return_first_elem($x)
{
	if(!is_array($x)) $x=array($x);
	$res=null;
	foreach($x as $elem) {$res=$elem; break;}
	return $res;
}

function return_key_val($x)
{
	if(!is_array($x)) $x=array($x);
	$key=""; $aval=array();
	foreach($x as $elem) {if(empty($key)) $key=$elem; else $aval[]=$elem;}
	return array($key=>implode(" ",$aval));
}

function return_key_vals($x)
{
	if(!is_array($x)) $x=array($x);
	$key=""; $aval=array();
	foreach($x as $initkey=>$elem) {if(empty($key)) $key=$elem; else $aval[$initkey]=$elem;}
	return array($key=>$aval);
}

if(!function_exists("mb_ucfirst")) { 
	function mb_ucfirst($str) {
		$fc = mb_strtoupper(mb_substr($str, 0, 1));
		return $fc.mb_substr($str, 1);
	}
}

function helpers_get_os($user_agent = '') {
	//https://stackoverflow.com/questions/18070154/get-operating-system-info

	if(empty($user_agent)) $user_agent = $_SERVER['HTTP_USER_AGENT'];

    $os_platform  = "Unknown OS Platform";

    $os_array     = array(
                          '/windows nt 10/i'      =>  'Windows 10',
                          '/windows nt 6.3/i'     =>  'Windows 8.1',
                          '/windows nt 6.2/i'     =>  'Windows 8',
                          '/windows nt 6.1/i'     =>  'Windows 7',
                          '/windows nt 6.0/i'     =>  'Windows Vista',
                          '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
                          '/windows nt 5.1/i'     =>  'Windows XP',
                          '/windows xp/i'         =>  'Windows XP',
                          '/windows nt 5.0/i'     =>  'Windows 2000',
                          '/windows me/i'         =>  'Windows ME',
                          '/win98/i'              =>  'Windows 98',
                          '/win95/i'              =>  'Windows 95',
                          '/win16/i'              =>  'Windows 3.11',
                          '/macintosh|mac os x/i' =>  'Mac OS X',
                          '/mac_powerpc/i'        =>  'Mac OS 9',
                          '/linux/i'              =>  'Linux',
                          '/ubuntu/i'             =>  'Ubuntu',
                          '/iphone/i'             =>  'iPhone',
                          '/ipod/i'               =>  'iPod',
                          '/ipad/i'               =>  'iPad',
                          '/android/i'            =>  'Android',
                          '/blackberry/i'         =>  'BlackBerry',
                          '/webos/i'              =>  'Mobile'
                    );

    foreach ($os_array as $regex => $value)
        if (preg_match($regex, $user_agent))
            $os_platform = $value;

    return $os_platform;
}

function helpers_detect_mobile($useragent='') {
	if(empty($useragent)) $useragent=$_SERVER['HTTP_USER_AGENT'];
	if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
		return true;
	return false;

}

function tinymd5($str, $length) { // $length 20-22 not advised unless $remove = ''; //https://stackoverflow.com/questions/3763728/shorter-php-cipher-than-md5
    // remove vowels to prevent undesirable words and + / which may be problematic
    $remove = array('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U', '+', '/');
    $salt = $str;
    do { // re-salt and loop if rebase removes too many characters
        $salt = $base64 = base64_encode(md5($salt, TRUE));
        $rebase = substr(str_replace($remove, '', $base64), 0, $length);
    } while ($length < 20 && substr($rebase, -1) == '=');
    return str_pad($rebase, min($length, 22), '='); // 22 is max possible length
}

function lang($wholeterm,$firstupper=true,$forcefirsttrue=false,$unbraked=false) {
	global $lang, $settings__charset;
	$fr=(strtolower($lang['lang'])=='fr');
	$en=(strtolower($lang['lang'])=='en');
	$aphrase=explode(" ",$wholeterm);
	$newaphrase=array();
	$implodeby=$unbraked?"&nbsp;":" ";
	foreach ($aphrase as $k=>$term) {
	if (isset($lang[$term])) $existed=true;
	elseif($term=='money_symbol') $lang[$term]=($lang['lang']=='fr' || $lang['lang']=='es' || $lang['lang']=='de')?"€":"";
	elseif($term=='loans_between_files') $lang[$term]=$fr?"Prêts entre les dossiers ":"Loans between files";
	elseif($term=='hide') $lang[$term]=$fr?"cacher":"hide";
	elseif($term=='show') $lang[$term]=$fr?"Afficher":"Show";
	elseif($term=='no_loans') $lang[$term]=$fr?"Pas de prêts":"No loans";
	elseif($term=='nonliquid_funds_name') $lang[$term]=$fr?"Actif non liquide":"Non-cash asset name";
	elseif($term=='nonliquid_value') $lang[$term]=$fr?"Valeur non liquide":"Non-cash value";
	elseif($term=='liquid_value') $lang[$term]=$fr?"Valeur liquide":"Cash";
	elseif($term=='spent') $lang[$term]=$fr?"Dépensé":"Spent";
	elseif($term=='left_money') $lang[$term]=$fr?"Reste":"Left";
	elseif($term=='payment_files') $lang[$term]=$fr?"Dossiers paiement":"Payment files";
	elseif($term=='show_also_closed_files') $lang[$term]=$fr?"Montrer aussi les dossiers ferm&eacute;s":"Show also the closed files";
	elseif($term=='hide_closed_files') $lang[$term]=$fr?"Cacher les dossiers ferm&eacute;s":"Hide the closed files";
	elseif($term=='without_loans') $lang[$term]=$fr?"sans prêts":"without loans";
	elseif($term=='loans_substructed') $lang[$term]=$fr?"prêts déduit":"loans substructed";
	elseif($term=='payment_num') $lang[$term]=$fr?"N paiement":"N payment";
	elseif($term=='number_count') $lang[$term]=$fr?"Nombre":"Number";
	elseif($term=='nonparticipated') $lang[$term]=$fr?"Enregistr&eacute;s mais pas particip&eacute;":"Registered but not participated";
	elseif($term=='subjects_absent') $lang[$term]=$fr?"Enregistr&eacute;s mais ni particip&eacute;s ni pr&eacute;sents":"Registered but not present nor participated";
	elseif($term=='participats_registered_but_not_participated_nor_present' && $fr) $lang[$term]="Participants enregistrés mais ni participés ni présents";
	elseif($term=='delete_or_undelete_several_participants_by_id_or_email' && $fr) $lang[$term]="Désabonner ou réabonner plusieurs participants par ids ou emails";
	elseif($term=='|s' && !$fr) $lang[$term]="";
	elseif($term=='number' && $fr) $lang[$term]="Numéro";
	elseif($term=='year' && $fr) $lang[$term]="Année";
	elseif($term=='n_part_needed' && $fr) $lang[$term]="N Partic. Requis";
	elseif($term=='payment' && $fr) $lang[$term]="Paiement";
	elseif($term=='payments' && $fr) $lang[$term]="Paiements";
	elseif($term=='payment_file' && $fr) $lang[$term]="Dossier";
	elseif($term=='account' && $fr) $lang[$term]="Compte";
	elseif($term=='edit_payment_files' && $fr) $lang[$term]="Modifier les dossiers de paiement";
	elseif($term=='initial_budget' && $fr) $lang[$term]="Demande de fonds ";
	elseif($term=='residual' && $fr) $lang[$term]="R&eacute;sidu ";
	elseif($term=='total_payment' && $fr) $lang[$term]="Paiement total ";
	elseif($term=='no_payment_files' && $fr) $lang[$term]="Aucun dossier de paiement";
	elseif($term=='closed' && $fr) $lang[$term]="Ferm&eacute:";
	elseif($term=='links' && $fr) $lang[$term]="Liens ";
	elseif($term=='assigned_experiments' && $fr) $lang[$term]="Expériences affiliées ";
	elseif($term=='experiments_used_it' && $fr) $lang[$term]="Expériences l'ayant utilisé ";
	elseif($term=='loans' && $fr) $lang[$term]="Prêts ";
	elseif($term=='given_loans' && $fr) $lang[$term]="Prêts donnés ";
	elseif($term=='negative_for_borrowings' && $fr) $lang[$term]="valeurs négatives pour emprunts";
	elseif($term=='payment_file_deleted' && $fr) $lang[$term]="Dossier supprimé";
	elseif($term=='assign_payment_file' && $fr) $lang[$term]="Attribuer un dossier";
	elseif($term=='choose_payment_file' && $fr) $lang[$term]="Attribuer un dossier";
	elseif($term=='payment_file_choosen' && $fr) $lang[$term]="Dossier attribu&eacute;";
	elseif($term=='manage_payment_files' && $fr) $lang[$term]="G&eacute;rer les dossiers";
	elseif($term=='see_experiments_in_the_calendar' && $fr) $lang[$term]="Voir les experiences dans le calendrier";
	elseif($term=='payment_responsable' && $fr) $lang[$term]="Responsable de paiements";
	elseif($term=='payment_report' && $fr) $lang[$term]="Récapitulatif de paiements";
	elseif($term=='money_responsible' && $fr) $lang[$term]="Responsable d'argent";
	elseif($term=='then' && $fr) $lang[$term]="Puis";
	elseif($term=='reload_the_page' && $fr) $lang[$term]="recharger la page";
	elseif($term=='should_not_be_empty' && $fr) $lang[$term]="Ne doit pas être vide ";
	elseif($term=='nominal_values' && $fr) $lang[$term]="Valeurs nominales";
	elseif($term=='make' && $fr) $lang[$term]="Effectuer";
	elseif($term=='no_lines' && $fr) $lang[$term]="Aucune ligne";
	elseif($term=='no_accounts_found' && $fr) $lang[$term]="Aucun compte trouvé";
	elseif($term=='payment_operations' && $fr) $lang[$term]="Opérations de paiement";
	elseif($term=='financial_operations' && $fr) $lang[$term]="Opérations de comptabilité";
	elseif($term=='new_operation' && $fr) $lang[$term]="Nouvelle opération";
	elseif($term=='debit_account_from' && $fr) $lang[$term]="Débiter un compte<br>(de)";
	elseif($term=='credit_account_to' && $fr) $lang[$term]="Créditer un compte<br>(vers)";
	elseif($term=='debit_account' && $fr) $lang[$term]="Compte à débiter";
	elseif($term=='credit_account' && $fr) $lang[$term]="Compte à créditer";
	elseif($term=='payment_accounts' && $fr) $lang[$term]="Comptes de paiement";
	elseif($term=='state_of_accounts' && $fr) $lang[$term]="États des comptes";
	elseif($term=='lab_safe' && $fr) $lang[$term]="Coffre fort du laboratoire";
	elseif($term=='bank/bookkeeper' && $fr) $lang[$term]="Banque/comptable";
	elseif($term=='total_sum' && $fr) $lang[$term]="Somme totale";
	elseif($term=='sum' && $fr) $lang[$term]="Somme";
	elseif($term=='amount' && $fr) $lang[$term]="Montant";
	elseif($term=='date_time' && $fr) $lang[$term]="Date et heure";
	elseif($term=='where_cash' && $fr) $lang[$term]="Dont cash ";
	elseif($term=='choose_another' && $fr) $lang[$term]="Choisir un autre";
	elseif($term=='both_accounts_should_be_choosen' && $fr) $lang[$term]="Les deux comptes doivent être choisis";
	elseif($term=='accounts_should_be_different' && $fr) $lang[$term]="Les comptes doivent être différents";
	elseif($term=='or_the_total_amount_should_be' && $fr) $lang[$term]="ou le montant total doit être ";
	elseif($term=='there_is_an_error_in_amounts' && $fr) $lang[$term]="Il y a une erreur dans les montants";
	elseif($term=='at_least_one_amount_should_be_positif' && $fr) $lang[$term]="Au moins un montant doit être positif";
	elseif($term=='details' && $fr) $lang[$term]="Détailles";
	elseif($term=='details_operation_id' && $fr) $lang[$term]="Détailles de l'opération dont id=";
	elseif($term=='this_operation_does_not_exist' && $fr) $lang[$term]="Cette opération n'existe pas";
	elseif($term=='made_by' && $fr) $lang[$term]="Effectuée par";
	elseif($term=='make_a' && $fr) $lang[$term]="Effectuer une";
	elseif($term=='registered_by' && $fr) $lang[$term]="Enregistrée par";
	elseif($term=='please_choose_the_payment_file' && $fr) $lang[$term]="Veuillez choisir le dossier de paiement ";
	elseif($term=='for_this_file' && $fr) $lang[$term]="Pour ce dossier";
	elseif($term=='financial_operations_main_page' && $fr) $lang[$term]="Page principale d'opérations de comptabilité";
	elseif($term=='payment_files_list' && $fr) $lang[$term]="Liste des dossiers de paiement";
	elseif($term=='payment_files_main_page' && $fr) $lang[$term]="Page principale des dossiers de paiement";
	elseif($term=='state_of_accounts_main_page' && $fr) $lang[$term]="Page principale d'états des comptes";
	elseif($term=='this_file_replaces_another?' && $fr) $lang[$term]="Ce dossier « remplace » un autre ?";
	elseif($term=='please_choose' && $fr) $lang[$term]="Veuillez choisir";
	elseif($term=='you_should_choose_the_replacement_file_as_the_experimenter_choosen_is_not_responsible_for_this_file' && $fr) $lang[$term]="Vous devez choisir le dossier « remplacé » car l'expérimentateur choisi n'est pas responsable pour ce dossier";
	elseif($term=='accounting_record_not_made_as_no_money_available_for_this_file' && $fr) $lang[$term]="Opération comptable n'est pas effectuée car pas de fonds disponibles pour ce dossier";
	elseif($term=='replaces_the_file' && $fr) $lang[$term]="Remplace le dossier ";
	elseif($term=='available_money' && $fr) $lang[$term]="Argent disponible";
	elseif($term=='the_accounts' && $fr) $lang[$term]="Les comptes";
	elseif($term=='include' && $fr) $lang[$term]="Inclure";
	elseif($term=='dont_include' && $fr) $lang[$term]="Ne pas inclure";
	elseif($term=='transpose' && $fr) $lang[$term]="Transposer";
	elseif($term=='in_total' && $fr) $lang[$term]="Au total";
	elseif($term=='no_operation_will_be_made' && $fr) $lang[$term]="Aucune opération ne sera effectuée";
	elseif($term=='payment_report_all_files_for_dates' && $fr) $lang[$term]="Rapport des paiements (tous dossiers) pour les dates ";
	elseif($term=='used_payment_files' && $fr) $lang[$term]="Dossiers utilisés ";
	elseif($term=='surname' && $fr) $lang[$term]="Surnom";
	elseif($term=='of_budget' && $fr) $lang[$term]="du budget";
	elseif($term=='left_at' && $fr) $lang[$term]="Reste au";
	elseif($term=='file_closed' && $fr) $lang[$term]="Dossier fermé";
	elseif($term=='at' && $fr) $lang[$term]="au";
	elseif($term=='all' && $fr) $lang[$term]="Tous";
	elseif($term=='everyone' && $fr) $lang[$term]="Tout le monde";
	elseif($term=='not_participated' && $fr) $lang[$term]="non participé(e)";
	elseif($term=='comments' && $fr) $lang[$term]="Commentaires";
	elseif($term=='apply' && $fr) $lang[$term]="Appliquer";
	elseif($term=='reload' && $fr) $lang[$term]="Recharger";
	elseif($term=='press' && $fr) $lang[$term]="Appuyez sur";
	elseif($term=='all_experiments' && $fr) $lang[$term]="Toutes les expériences";
	elseif($term=='budget_information' && $fr) $lang[$term]="Infomration sur le budget";
	elseif($term=='to_save_the_changes' && $fr) $lang[$term]="Pour enregistrer les changements";
	elseif($term=='to_undo_the_modifications' && $fr) $lang[$term]="Pour annuler les modifications";
	elseif($term=='confirm_mail_suppl_text' && $fr) $lang[$term]="Texte supplémentaire dans le <br>mail de confirmation  :";
	elseif($term=='reminder_mail_suppl_text' && $fr) $lang[$term]="Texte supplémentaire dans le <br>mail de rappel  :";
	elseif($term=='confirm_webmessage_suppl_text' && $fr) $lang[$term]="Texte supplémentaire dans le mesage web de confirmation :";
	elseif($term=='additional_subscription_step_text' && $fr) $lang[$term]="Texte de l'étape supplémetaire <br> lors de l'inscription  (si pas vide) :";
	elseif($term=='supplementary_experiment_description_for_participants' && $fr) $lang[$term]="Description supplémentaire de l'expérience<br> pour les participants :";
	elseif($term=='no_description_for_this_experiment' && $fr) $lang[$term]="Il n'y a pas de description pour cette expérience";
	elseif($term=='if_you_are_still_ready_please_click_on_the_link' && $fr) $lang[$term]="Si vous êtes toujours partant pour participer, nous vous invitons à cliquer sur le lien suivant pour vous inscrire à une session de votre choix :";
	elseif($term=='CONTINUE' && $fr) $lang[$term]="CONTINUER";
	elseif($term=='no_experiment_found' && $fr) $lang[$term]="Aucune expérience trouvée";
	elseif($term=='absent_subjects' && $fr) $lang[$term]="Participants absents";
	elseif($term=='participants_for_date(s)' && $fr) $lang[$term]="Participants pour la/les date(s)";
	elseif($term=='absent_subjects_only' && $fr) $lang[$term]="Uniqument les 'absents'";
	elseif($term=='all_subjects' && $fr) $lang[$term]="Tous les participants";
	elseif($term=='all_absent_subjects' && $fr) $lang[$term]="Tous les 'absents'";
	elseif($term=='today' && $fr) $lang[$term]="Aujourd'hui";
	elseif($term=='and_data_protection' && $fr) $lang[$term]="et protection des données d’inscription";
	elseif($term=='impossible_to_reactivate_your_account' && $fr) $lang[$term]="Impossible de réactiver votre compte";
	elseif($term=='please_write_to' && $fr) $lang[$term]="Veuillez écrire à";
	elseif($term=='account_reactivated' && $fr) $lang[$term]="Compte réactivé";
	elseif($term=='reactivate_your_account' && $fr) $lang[$term]="Réactiver votre compte";
	elseif($term=='can_undelete' && $fr) $lang[$term]="Peut se réinscrire";
	elseif($term=='desactivate_only' && $fr) $lang[$term]="Seulement désactiver";
	elseif($term=='resubscribe_possible' && $fr) $lang[$term]="réactivation possible";
	elseif($term=='definetely_unsubscribe' && $fr) $lang[$term]="Se désabonner définitivement";
	elseif($term=='you_will_not_be_able_to_resubscribe_after_this_action' && $fr) $lang[$term]="Vous ne pourrez plus vous réabonner après cette action";
	elseif($term=='do_you_want_to_continue' && $fr) $lang[$term]="Voulez-vous continuer";
	elseif($term=='your_account_is_now_desactivated' && $fr) $lang[$term]="Votre compte est actuellement désactivé";
	elseif($term=='twin_to' && $fr) $lang[$term]="Jumeau à :";
	elseif($term=='twin' && $fr) $lang[$term]="Jumeau";
	elseif($term=='separate_by_comma' && $fr) $lang[$term]="Séparez par une virgule";
	elseif($term=='if_more_than_one' && $fr) $lang[$term]="Si plusieurs";
	elseif($term=='enter_ids' && $fr) $lang[$term]="Entrez l'(les) id(s)";
	elseif($term=='mix_account' && $fr) $lang[$term]="RégieAvance";
	elseif($term=='for_mix_account' && $fr) $lang[$term]="pour la Régie d’avance";
	elseif($term=='creation_of' && $fr) $lang[$term]="Création de";
	elseif($term=='in_the' && $fr) $lang[$term]="Dans le";
	elseif($term=='for_not_using' && $fr) $lang[$term]="Pour ne pas utiliser";
	elseif($term=='close_this_file' && $fr) $lang[$term]="Fermez ce dossier";
	elseif($term=='you_are_already_registered_for_the_following_session' && $fr) $lang[$term]="Vous êtes déjà enregistré(s) à la session suivante";
	elseif($term=='you_cannot_participate_in_these_sessions_simultaneously' && $fr) $lang[$term]="Vous ne pouvez pas participer aux deux sessions en même temps ";
	elseif($term=='accounting' && $fr) $lang[$term]="Comptabilité";
	elseif($term=='organise_a_session' && $fr) $lang[$term]="Organiser une session";
	elseif($term=='step_by_step_tutorial' && $fr) $lang[$term]="Tutoriel pas à pas";
	elseif($term=='optionally' && $fr) $lang[$term]="Optionnellement";
	elseif($term=='create_new_session' && $fr) $lang[$term]="Créer une nouvelle session";
	elseif($term=='check/modify' && $fr) $lang[$term]="Vérifier/modifier";
	elseif($term=='check' && $fr) $lang[$term]="Vérifier";
	elseif($term=='check_the' && $fr) $lang[$term]="Vérifier les";
	elseif($term=='experiment_details' && $fr) $lang[$term]="Les détails de l'expérience";
	elseif($term=='select_experiment' && $fr) $lang[$term]="Sélectionner une expérience";
	elseif($term=='and_then' && $fr) $lang[$term]="et puis";
	elseif($term=='if_there_are_any' && $fr) $lang[$term]="s'il y en a";
	elseif($term=='create_new_experiment' && $fr) $lang[$term]="Créer une nouvelle expérience";
	elseif($term=='select_only_from' && $fr) $lang[$term]="Sélectionner seulement de";
	elseif($term=='find_it' && $fr) $lang[$term]="Trouvez-la";
	elseif($term=='in_the_calendar' && $fr) $lang[$term]="Dans le calendrier";
	elseif($term=='click_on_its_name' && $fr) $lang[$term]="Cliquez sur son nom";
	elseif($term=='repeat_for_several_sessions' && $fr) $lang[$term]="Répéter pour créer plusieurs sessions";
	elseif($term=='some_days_before_the_experiment' && $fr) $lang[$term]="Quleques jours avant l'expérience";
	elseif($term=='return_back_to_the_experiment' && $fr) $lang[$term]="Retourner à l'expérience";
	elseif($term=='please_wait' && $fr) $lang[$term]="Veuillez pateinter";
	elseif($term=='back_to_introduction' && $fr) $lang[$term]="Retour à l'introduction";
	elseif($term=='current_page_address' && $fr) $lang[$term]="Adresse de la page en cours";
	elseif($term=='pelase_select_experiment_first' && $fr) $lang[$term]="Veuillez d’abord sélectionner une expérience";
	elseif($term=='whole_month' && $fr) $lang[$term]="MOIS ENTIER";
	elseif($term=='you_have_registered_for' && $fr) $lang[$term]="Vous vous êtes enregistré(e) pour";
	elseif($term=='add_external_code' && $fr) $lang[$term]="Ajouter un code externe";
	elseif($term=='move_selected_participants_to' && $fr) $lang[$term]="Déplacer les participants sélectionnés au";
	elseif($term=='select_participants' && $fr) $lang[$term]="Sélectionner les participants";
	elseif($term=='reach_the_sum' && $fr) $lang[$term]="Atteindre la somme";
	elseif($term=='reminder' && $fr) $lang[$term]="Rappel";
	elseif($term=='the_current_payment_file_is' && $fr) $lang[$term]="Le dossier courant et le";
	elseif($term=='make_transfer' && $fr) $lang[$term]="Effectuer le transfert";
	elseif($term=='n_selected' && $fr) $lang[$term]="Nombre des participants sélectionnés";
	elseif($term=='sum_selected' && $fr) $lang[$term]="Somme correspondante";
	elseif($term=='check_for_transfer' && $fr) $lang[$term]="Cocher pour déplacer";
	elseif($term=='move_subjects' && $fr) $lang[$term]="Déplacer des sujets";
	elseif($term=='move_subjects_to_another_payment_file' && $fr) $lang[$term]="Déplacer des sujets vers un autre dossier";
	elseif($term=='the_appointment_is_5_minutes_before_the_experiment' && $fr) $lang[$term]="Le rendez-vous est prévu 5 minutes avant le début de l'expérience.";
	elseif($term=='please_carefully_read_the_participation_rules' && $fr) $lang[$term]="Veuillez prendre connaissance des r&egrave;gles de participation aux exp&eacute;riences avant de vous inscrire";
	elseif($term=='you_need_to_accept_the_conditions_at_the_bottom_in_order_to_subscribe' && $fr) $lang[$term]="Merci d'accepter ces conditions au bas de la page pour commencer l'inscription";
	elseif($term=='please_note_the_ids_or_emails_of_participants_to_unsubscribe_or_resubscribe' && $fr) $lang[$term]="Notez les Ids ou emails des participants à désabonner ou réabonner";
	elseif($term=='one_per_line*_or_separated_by_comma' && $fr) $lang[$term]="un par ligne* (ou séparés par virgules) ";
	elseif($term=='form_in_contruction' && $fr) $lang[$term]="Formulaire en contruction";
	elseif($term=='birth_year' && $fr) $lang[$term]="Ann&eacute;e de naissance";
	elseif($term=='please_copy_the_image_code_to_continue' && $fr) $lang[$term]="Pour continuer, veuillez recopier le code ci-dessous";
	elseif($term=='incorrect_code' && $fr) $lang[$term]="Code incorrect";
	elseif($term=='please_retry' && $fr) $lang[$term]="veuillez réessayer";
	elseif($term=='type_the_text' && $fr) $lang[$term]="Entrez le texte";
	elseif($term=='an_error_is_occured' && $fr) $lang[$term]="Une erreur est survenue";
	elseif($term=='or_you_are_not_authorized' && $fr) $lang[$term]="ou vous n'avez pas d'autorisations nécessaires";
	elseif($term=='please_contact_the_site_support' && $fr) $lang[$term]="Veuillez contacter les administrateurs du site";
	elseif($term=='create_participant' && $fr) $lang[$term]="Créer un participant";
	elseif($term=='create_participants' && $fr) $lang[$term]="Créer plusieurs participants";
	elseif($term=='create_several_participants' && $fr) $lang[$term]="Créer plusieurs participants";
	elseif($term=='one_value_per_line' && $fr) $lang[$term]="Une valeur par ligne";
	elseif($term=='or_one_line_only_if_same_except_email' && $fr) $lang[$term]="Ou une seule ligne sauf pour email";
	elseif($term=='create_just_one_participant' && $fr) $lang[$term]="Créer un seul participant";
	elseif($term=='please_visit_the_following_link_and_finish_the_questionnaire' && $fr) $lang[$term]="Veuillez cliquer sur le lien suivant et terminer le questionnaire";
	elseif($term=='it_is_necessary_to_answer_a_questionnaire' && $fr) $lang[$term]="Il est nécessaire de répondre à un questionnaire";
	elseif($term=='it_will_be_necessary' && $fr) $lang[$term]="Il sera nécessaire";
	elseif($term=='to_answer_a_questionnaire' && $fr) $lang[$term]="De répondre à un questionnaire";
	elseif($term=='in_order_to_confirm_your_registration' && $fr) $lang[$term]="Afin de confirmer votre inscription";
	elseif($term=='your_have' && $fr) $lang[$term]="Vous avez";
	elseif($term=='your_will_have' && $fr) $lang[$term]="Vous aurez";
	elseif($term=='the_link_to_the_questionnaire_will_be_shown_on_the_next_page' && $fr) $lang[$term]="Le lien vers le questionnaire vous sera affiché sur la page suivante";
	elseif($term=='if_you_accept_to_participate' && $fr) $lang[$term]="Si vous acceptez de participer";
	elseif($term=='you_will_be_redirected_to_the_questionnaire' && $fr) $lang[$term]="Vous allez être directement redirigé(e) vers le questionnaire";
	elseif($term=='after_that_time_your_registration_is_not_guaranteed' && $fr) $lang[$term]="Après ce temps votre inscription ne sera plus garantie";
	elseif($term=='questionnaire_not_finished' && $fr) $lang[$term]="Questionnaire non termin&eacute;";
	elseif($term=='finish_the_questionnaire_with_the_followin_link' && $fr) $lang[$term]="Terminez le questionnaire en utilisant le lien suivant";
	elseif($term=='selected_session' && $fr) $lang[$term]="Session s&eacute;lectionn&eacute;e";
	elseif($term=='the_participants_of_this_experiment_must_correspond_to_a_particular_profile' && $fr) $lang[$term]="Pour des raisons scientifiques, les participants à cette expérience doivent avoir un profil particulier";
	elseif($term=='you_dont_have_this_profile' && $fr) $lang[$term]="Vous n'avez pas ce profil";
	elseif($term=='we_have_already_enough_participants_of_your_profile' && $fr) $lang[$term]="Nous avons déjà un nombre suffisant des participants avec votre profil";
	elseif($term=='unforunately_we_cannot_benefit_from_your_participation' && $fr) $lang[$term]="Nous ne pouvons malheureusement pas bénéficier de votre participation en ce moment";
	elseif($term=='if_your_are_not_redirected' && $fr) $lang[$term]="Si vous n'&ecirc;tes pas redirig&eacute;(e) vers une autre page";
	elseif($term=='please_click_on_the_next_button' && $fr) $lang[$term]="Veuillez appuyez sur le bouton suivant";
	elseif($term=='unregister_hours_before' && $fr) $lang[$term]="D&eacute;sincription possible (heures avant le d&eacute;but)";
	elseif($term=='unregister' && $fr) $lang[$term]="Se d&eacute;sincrire";
	elseif($term=='yes_unregister' && $fr) $lang[$term]="Oui, me d&eacute;sincrire";
	elseif($term=='yes_send_me_the_link' && $fr) $lang[$term]="Oui, recevoir le lien";
	elseif($term=='send_me_the_new_link' && $fr) $lang[$term]="Recevoir le nouveau lien par mail";
	elseif($term=='do_you_really_want_to_unregister_from_session' && $fr) $lang[$term]="Voulez-vous vraiment vous d&eacute;sincrire de la session du";
	elseif($term=='if_you_click_yes' && $fr) $lang[$term]="Si vous cliquez sur oui,";
	elseif($term=='you_will_receive_a_confirmation_link_by_mail' && $fr) $lang[$term]="Vous allez recevoir un lien de confirmation par courrier &eacute;lectronique";
	elseif($term=='you_will_be_unregistered_only_if_you_click_on_it' && $fr) $lang[$term]="Vous ne serez d&eacute;sincrit(e) qu'apr&egrave;s avoir cliqu&eacute; sur ce lien";
	elseif($term=='the_confirmaiton_link' && $fr) $lang[$term]="Le lien de confirmation";
	elseif($term=='in_order_to_unregister_connect_to_your_mailbox_and_click_on_the_link_before' && $fr) $lang[$term]="Afin de vous d&eacute;sinscrire, connectez-vous &agrave; votre boite mail et cliquez sur le lien avant le";
	elseif($term=='incorrect_or_expired_link' && $fr) $lang[$term]="Lien incorrect ou expir&eacute;";
	elseif($term=='too_late_to_unsubscribe_from_this_session' && $fr) $lang[$term]="Il est trop tard pour se d&eacute;sinscrire de cette session";
	elseif($term=='you_are_not_registered_for_this_session' && $fr) $lang[$term]="Vous n'&ecirc;tes pas inscrit(e) &agrave; cette session";
	elseif($term=='an_error_has_occured' && $fr) $lang[$term]="Une erreur est survenue";
	elseif($term=='please_contact_the_experimentator' && $fr) $lang[$term]="Veuillez contacter l'exp&eacute;rimentateur";
	elseif($term=='you_are_unregistered_from_the_session' && $fr) $lang[$term]="Vous vous &ecirc;tes bien d&eacute;sinscrit(e) de la session du";
	elseif($term=='unregistering_from_an_experimental_session_need_confirmation' && $fr) $lang[$term]="Confirmez votre demande de désinscription de la session expérimentale";
	elseif($term=='you_may_unsubscribe_from_this_session_before' && $fr) $lang[$term]="Vous pouvez vous désinscrire de cette session avant le";
	elseif($term=='in_order_to_do_it_visit_the_following_link' && $fr) $lang[$term]="Pour le faire, rendez-vous sur le lien suivant";
	elseif($term=='in_order_to_modify_your_first_or_last_name' && $fr) $lang[$term]="Pour modifier votre nom ou pr&eacute;nom";
	elseif($term=='you_cannot_modify_your_email_while_registered_to_a_non_finished_session' && $fr) $lang[$term]="Vous ne pouvez pas modifier votre adresse de courriel tant que vous &ecirc;tes enregistr&eacute;(e) &agrave; une session exp&eacute;rimentale non termin&eacute;e";
	elseif($term=='dates_and_hours_of_the_first_session_below' && $fr) $lang[$term]="Ci-dessous les dates et horaires de la <strong>premi&egrave;re</strong> session";
	elseif($term=='restrict_participant_subscriptions_dates' && $fr) $lang[$term]="Restreindre les dates d'inscription des participants";
	elseif($term=='please_choose_the_lab' && $fr) $lang[$term]="Veuillez s&eacute;lectionner le laboratoire";
	elseif($term=='select_the' && $fr) $lang[$term]="S&eacute;lectionner le";
	elseif($term=='you_have_to' && $fr) $lang[$term]="Vous devez";
	elseif($term=='experiment_type' && $fr) $lang[$term]="Type d'exp&eacuterience";
	elseif($term=='session_cancelled' && $fr) $lang[$term]="Session annulée";
	elseif($term=='not_yet_registered' && $fr) $lang[$term]="n'ayant pas encore confirm&eacute;";
	elseif($term=='the_link_that_we_have_sent_you_will_allow_you_to_reactivate_your_account' && $fr) $lang[$term]="Le lien que nous vous avons envoyé va vous permettre de réactiver votre compte";
	elseif($term=='click_on_the_received_link_to_subscribe_to_this_experiment_type' && $fr) $lang[$term]="Cliquez sur ce lien afin de vous inscrire à ce type d'expérience";
	elseif($term=='please_click_on_the_link_below_to_subscribe_to' && $fr) $lang[$term]="Veuillez cliquer sur le lien ci-dessous afin de vous inscrire pour";
	elseif($term=='impossible_to_subscribe_to' && $fr) $lang[$term]="Impossible de vous inscrire pour";
	elseif($term=='you_are_now_subscribed_to' && $fr) $lang[$term]="Vous êtes maintenant inscrit(e) pour";
	elseif($term=='acrion_already_done' && $fr) $lang[$term]="Vous avez déjà fait cette action";
	elseif($term=='incorrect_link' && $fr) $lang[$term]="Lien incorrect";
	elseif($term=='welcome_' && $fr) $lang[$term]="Bienvenue ";
	elseif($term=='email_address_not_found' && $fr) $lang[$term]="L'adresse mail n'a pas été trouvé";
	elseif($term=='this_link_will_be_valid_for' && $fr) $lang[$term]="Ce lien sera valable pendant";
	elseif($term=='in_order_to_protect_your_personal_data' && $fr) $lang[$term]="Dans le but de protéger vos données personnelles";
	elseif($term=='the_link_to_access_this_area' && $fr) $lang[$term]="Le lien pour accéder à cet espace";
	elseif($term=='should_be_renewed_each' && $fr) $lang[$term]="Doit être renouvelé chaque";
	elseif($term=='in_order_to_receive' && $fr) $lang[$term]="Afin de recevoir";
	elseif($term=='to_receive' && $fr) $lang[$term]="Pour recevoir";
	elseif($term=='please_enter_your_registered_email_address' && $fr) $lang[$term]="Veuillez entrer l'adresse mail que vous avez utiliser pour vous inscrire";
	elseif($term=='the_new_link' && $fr) $lang[$term]="Le nouveau lien";
	elseif($term=='please_click_on_the_next_button' && $fr) $lang[$term]="Veuillez cliquer sur le bouton suivant";
	elseif($term=='we_will_send_your_the_new_link_if_this_address_exists_in_our_database' && $fr) $lang[$term]="Nous vous enverrons le nouveau lien si cette adresse existe dans notre base de données";
	elseif($term=='has_been_sent_to_you' && $fr) $lang[$term]="Vous a &eacute;t&eacute; envoy&eacute;";
	elseif($term=='has_been_already_sent_to_you' && $fr) $lang[$term]="Vous a déjà été envoyé";
	elseif($term=='on_the_date' && $fr) $lang[$term]="à la date du";
	elseif($term=='hours' && $fr) $lang[$term]="heures";
	elseif($term=='this_link_is_expired' && $fr) $lang[$term]="Ce lien est expir&eacute;";
	elseif($term=='to_access_your_personal_space' && $fr) $lang[$term]="Pour accéder à votre espace personnel";
	elseif($term=='by' && $fr) $lang[$term]="Par";
	elseif($term=='participants_who_change_session' && $fr) $lang[$term]="Les participants qui changent de session";
	elseif($term=='should_be_marked_as' && $fr) $lang[$term]="Doivent &ecirc;tre marqu&eacute;s comme";
	elseif($term=='in_the_new_session' && $fr) $lang[$term]="Dans la nouvelle session";
	elseif($term=='sessions_closed' && $fr) $lang[$term]="Session(s) ferm&eacute;e(s)";
	elseif($term=='close_outdated_sessions' && $fr) $lang[$term]="Fermer les sessions avec les dates d&eacute;pass&eacute;es";
	elseif($term=='IMPORTANT_INFORMATION' && $fr) $lang[$term]="INFORMATION IMPORTANTE";
	elseif($term=='we_are_happy_to_invite_you_to_participate_in_this_experiment' && $fr) $lang[$term]="Nous sommes heureux de pouvoir vous inviter à participer à cette expérience";
	elseif($term=='this_is_a_by_couples_experiment' && $fr) $lang[$term]="Dans cette expérience les participants doivent venir en couple";
	elseif($term=='you_should_register_for_the_same_session_with_your_partner' && $fr) $lang[$term]="Vous devez vous inscrire à la même session que votre conjoint";
	elseif($term=='no_more_places_in_this_session' && $fr) $lang[$term]="Il n'y a plus de places dans cette session";
	elseif($term=='places_in_this_session_avalable_for_partners_of_already_registered_persons_only' && $fr) $lang[$term]="Les places dans cette sessions sont disponibles uniquement pour les conjoints des personnes déjà inscrites";
	elseif($term=="please_enter_your_partner's_mail_used_to_register_in_our_database" && $fr) $lang[$term]="Merci d'entrer l'adresse mail que <u>votre conjoint</u> a utilisé pour s'inscrire sur notre site";
	elseif($term=="the_address_to_register_for_this_experiment" && $fr) $lang[$term]="L'adresse pour s'inscrire à l'expérience";
	elseif($term=="you_should_enter_the_email_of_your_partner_not_yours" && $fr) $lang[$term]="Vous devez entrer l'adresse mail de votre conjoint, pas la vôtre";
	elseif($term=="sorry_email_not_found" && $fr) $lang[$term]="Désolé, l'adresse mail non trouvé";
	elseif($term=="your_partner_should_register_at" && $fr) $lang[$term]="Votre conjoint doit s'inscrire sur";
	elseif($term=="impossible_to_register_for_this_experiment" && $fr) $lang[$term]="Désolé, l'inscription à cette expérience n'est pas possible";
	elseif($term=="your_partner_is_registered_to_another_session" && $fr) $lang[$term]="Votre conjoint s'est inscrit à une autre session";
	elseif($term=="please_register_to_the_same_session_as_your_partner" && $fr) $lang[$term]="Veuillez vous inscrire à la même session que votre conjoint";
	elseif($term=="your_partner_is_not_registered_for_this_session" && $fr) $lang[$term]="Votre conjoint n'est pas inscrit &agrave; cette session";
	elseif($term=="please_note_that" && $fr) $lang[$term]="Veuillez noter que";
	elseif($term=="your_partner_should_have_consented_to_participate_in_this_experiment" && $fr) $lang[$term]="Votre conjoint doit avoir donn&eacute; sont consentement pour participer &agrave; cette exp&eacute;rience";
	elseif($term=="as_you_have_juste_done" && $fr) $lang[$term]="Comme vous venez de le faire";
	elseif($term=="by_subscribing_using_the_link_below" && $fr) $lang[$term]="En s'inscrivant avec le lien ci-dessous";
	elseif($term=="partners_only" && $fr) $lang[$term]="Session complète, sauf si votre conjoint y est inscrit";
	elseif($term=="after_clicking_of_the_invitation_link_for_this_experiment_recieved_by_mail" && $fr) $lang[$term]="Après avoir cliqué sur le lien d'invitation reçu par mail pour cette expérience";
	elseif($term=="otherwise" && $fr) $lang[$term]="Sinon";
	elseif($term=="remove_them_from_queue" && $fr) $lang[$term]="Retirer-les de la boite d'envoi";
	elseif($term=="mails_removed_from_queue" && $fr) $lang[$term]="e-mail(s) retiré(s) de la boite d'envoi";
	elseif($term=="from_temp_table" && $fr) $lang[$term]="De la table temporaire";
	elseif($term=="delete_unsubscribed_participants" && $fr) $lang[$term]="Supprimer des participants d&eacute;sabonn&eacute;s";
	elseif($term=="use_saved_request" && $fr) $lang[$term]="Requêtes sauvegardées";
	elseif($term=="last_used" && $fr) $lang[$term]="Dernière utilisée";
	elseif($term=="the_last_used" && $fr) $lang[$term]="Les dernières";
	elseif($term=="loaded_request" && $fr) $lang[$term]="Requête chargée";
	elseif($term=="save_as" && $fr) $lang[$term]="Enregistrer sous";
	elseif($term=="request_name" && $fr) $lang[$term]="Nom de la requête";
	elseif($term=="other_experiments" && $fr) $lang[$term]="Autres expériences";
	elseif($term=="other_users" && $fr) $lang[$term]="Autres utilisateurs";
	elseif($term=="my_account" && $fr) $lang[$term]="Mon compte";
	elseif($term=="saved" && $fr) $lang[$term]="Sauvegardé";
	elseif($term=="deleted" && $fr) $lang[$term]="Supprimé";
	

	

	else $lang[$term]=str_replace("_"," ",mb_ucfirst($term));
	
	// var_dump($term,$lang[$term],$fr);
	
	if(!$firstupper || $k>0) $lang[$term]=mb_strtolower($lang[$term],$settings__charset);
	$newaphrase[]=$forcefirsttrue?mb_ucfirst($lang[$term]):$lang[$term];
	}
	if($unbraked) $lang[$term]=str_replace(" ",$implodeby,$lang[$term]);
	
	return str_replace(" |","",implode($implodeby,$newaphrase));
}

?>

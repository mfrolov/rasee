<?php
// part of orsee. see orsee.org

function participants__count_participants($constraint="",$inc_deleted=false) {
	if ($constraint) $where_clause=" AND ".$constraint; else $where_clause="";
     $query="SELECT COUNT(participant_id) as pcount
      	FROM ".table('participants')." 
    	WHERE participant_id IS NOT NULL ";
    if (!$inc_deleted) $query.=" AND deleted='n' ";
    $query.=$where_clause;
	$line=orsee_query($query);
	return $line['pcount'];
}

function participants__count_participants_temp($constraint="") {
        if ($constraint) $where_clause=" AND ".$constraint;
                else $where_clause="";
        $query="SELECT COUNT(participant_id) as pcount
                FROM ".table('participants_temp')."
                WHERE deleted='n' ".$where_clause;
        $line=orsee_query($query);
        return $line['pcount'];
}

// check if participant_id exists in participants table
function participant__participant_id_exists($pid) {
                $query="SELECT participant_id FROM ".table('participants')." 
                        WHERE participant_id=".$pid;
                $line=orsee_query($query);
                if (isset($line['participant_id'])) $exists=true; else $exists=false;
        return $exists;
}

function participant__deleted($pid) {
       $query="SELECT deleted
               FROM ".table('participants')." 
               WHERE participant_id='".$pid."'";
	$line=orsee_query($query);
        if ($line['deleted']=="y") $result=true; else $result=false;
	return $result;
}

function participant__excluded($pid) {
        $query="SELECT excluded
                FROM ".table('participants')." 
                WHERE participant_id='".$pid."'";
        $line=orsee_query($query);
        if ($line['excluded']=="y") $result=true; else $result=false;
        return $result;
}

function participant__can_undelete($pid) {
        $query="SELECT can_undelete
                FROM ".table('participants')." 
                WHERE participant_id='".$pid."'";
        $line=orsee_query($query);
        if ($line['can_undelete']>0) $result=true; else $result=false;
        return $result;
}

function participant__exclude_participant($participant) {
	global $settings, $lang;

	if ($lang['lang']) $notice=$lang['automatic_exclusion_by_system_due_to_noshows'];
		else $notice=load_language_symbol('automatic_exclusion_by_system_due_to_noshows',$settings['admin_standard_language']);

	$notice=$participant['remarks']."\n".$notice.' '.$participant['number_noshowup'];

        $query="UPDATE ".table('participants')."
		SET excluded='y', deleted='y',
		remarks='".mysqli_real_escape_string($GLOBALS['mysqli'],$notice)."' 
                WHERE participant_id='".$participant['participant_id']."'";
        $done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

	$result='excluded';

	if ($settings['automatic_exclusion_inform']=='y') {
		$done=experimentmail__send_participant_exclusion_mail($participant);
		$result='informed';
		}

        return $result;
}



function participants__get_statistics($participant_id) {
	global $lang;

	echo '<TABLE width=90%>
		<TR>
			<TD>
				'.$lang['part_statistics_for_lab_experiments'].'
			</TD>
		</TR>';
	echo '  <TR>
			<TD>';
			participants__stat_laboratory($participant_id);
	echo '	     </TD>
                </TR>';
	echo '</TABLE>';

}

function participants__stat_laboratory($participant_id,$onlyarray=false,$regprespart="") {
	global $lang, $color;

	$query="SELECT *
		FROM ".table('experiments').", ".table('sessions').", ".table('participate_at')."
        	WHERE ".table('participate_at').".session_id=".table('sessions').".session_id 
		AND ".table('experiments').".experiment_id=".table('participate_at').".experiment_id
		AND participant_id = '".$participant_id."'
		AND experiment_type='laboratory'";
		if(strlen($regprespart)>1) {
			$firsttwoletters=substr($regprespart,0,2); $firstletter=substr($regprespart,0,1);
			if($firstletter=="r" || $firstletter=="s" || $firstletter=="p") $query.=" AND registered='y'";
			if($firstletter=="s" || $firstletter=="p") $query.=" AND shownup='y'";
			if($firsttwoletters=="pa") $query.=" AND participated='y'";
		}
		$query.="
      		ORDER BY registered, session_finished, session_start_year DESC, session_start_month DESC, session_start_day DESC,
                 	session_start_hour DESC, session_start_minute DESC"; //GROUP BY ".table('experiments').".experiment_id
	if($onlyarray) return orsee_query($query,"return_same");
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

	$now=time();

	$shade=false;

	echo '<TABLE width=90% border=0>';
	$headerline= '<TR>
		<TD>
			'.$lang['experiment'].'
		</TD>
		<TD>
			'.$lang['type'].'
		</TD>
		<TD>
			'.$lang['date_and_time'].'
		</TD>
		<TD>
			'.$lang['registered'].'
		</TD>
		<TD>
			'.$lang['location'].'
		</TD>
		<TD>
			'.$lang['shownup'].'
		</TD>
		<TD>
			'.$lang['participated'].'
		</TD>
		<TD>
			'.lang('Gain').'
		</TD>
	     </TR>';
	echo $headerline;
	$pay_tot=0;
	while ($p=mysqli_fetch_assoc($result)) {
		$last_reg_time=0;
		if ($p['registered']!='y') $last_reg_time=sessions__get_registration_end("","",$p['experiment_id']);
		if ($p['registered']=='y' || $last_reg_time > $now) {
			echo '<TR';
				if ($shade) echo ' bgcolor="'.$color['list_shade1'].'"'; 
						else echo ' bgcolor="'.$color['list_shade2'].'"';
			echo '>
				<TD>
					<A href="experiment_show.php?experiment_id='.$p['experiment_id'].'">'.
						$p['experiment_name'].'</A>
				</TD>
				<TD>
					'.$p['experiment_ext_type'].'
				</TD>
				<TD>';
				if ($p['registered']=='y') {
					if(empty($p['payghost'])) echo '<A HREF="experiment_participants_show.php?experiment_id='.
                				$p['experiment_id'].'&focus=registered&session_id='.
                				$p['session_id'].'">';
								echo session__build_name($p);
								if(empty($p['payghost'])) '</A>';
					}
				   else echo $lang['from'].' '.sessions__get_first_date($p['experiment_id']).' '.
                        			$lang['to'].' '.sessions__get_last_date($p['experiment_id']);
			echo '	</TD>
				<TD>';
				if ($p['registered']=='y') echo $lang['yes']; else echo $lang['no'];
			echo '	</TD>
				<TD>';
				if ($p['registered']=='y') echo laboratories__get_laboratory_name($p['laboratory_id']);
                                        else echo '-';
			echo '</TD>
				<TD>';
				if ($p['registered']=='y') {
					if ($p['session_finished']=='y') {
							if ($p['shownup']=='n')
								echo '<FONT color="'.$color['shownup_no'].'">'.$lang['no'].'</FONT>';
							   else echo '<FONT color="'.$color['shownup_yes'].'">'.$lang['yes'].'</FONT>';
							}
						else echo $lang['three_questionmarks'];
					}
				   else echo '-';
			echo '	</TD>
				<TD>';
				if ($p['registered']=='y') {
                                        if ($p['session_finished']=='y') {
							if ($p['participated']=='y') echo $lang['yes']; else echo $lang['no'];
						}
                                                else echo $lang['three_questionmarks'];
                                        }
				   else echo '-';
			echo '	</TD>
				<TD>';
				if(!empty($p['payment']) || !empty($p['payment_nlf'])) echo '<A HREF="experiment_participants_show.php?experiment_id='.
							$p['experiment_id'].'&focus=registered&pay=1&payment_mode=view&session_id='.
							$p['session_id'].'" target=_blank>';
				echo $p['payment'];
				if(!empty($p['payment_nlf'])) echo '+'.$p['payment_nlf'];
				if(!empty($p['payment']) || !empty($p['payment_nlf'])) '</A>';
				$pay_tot+=(int)$p['payment']+(int)$p['payment_nlf']; //var_dump($p);
			echo '	</TD>
			      </TR>';
			if ($shade) $shade=false; else $shade=true;
			}
		}
	$ahl=explode('<TD>',$headerline);
	$n_lines=count($ahl)-1;
	for($n=1; $n<=$n_lines-2; $n++) echo '<TD></TD>';
	echo '<TD style="text-align:right">'.lang('Total').':</TD>';
	echo '<TD>'.$pay_tot.'</TD>';
	echo '</TABLE>';
}



// Create unique participant id
function participant__create_participant_id() {
           $gibtsschon=true;
	   srand ((int)((double)microtime()*1000000));
           while ($gibtsschon) {
           	$crypt_id = "/";
		while (preg_match("/(\/|\\.)/",$crypt_id)) {
           		$participant_id = rand();
           		$crypt_id=unix_crypt($participant_id);
           		}

                $query="SELECT participant_id FROM ".table('participants')." 
                 	WHERE participant_id=".$participant_id." OR participant_id_crypt='$crypt_id'";
		$line=orsee_query($query);
                if (isset($line['participant_id'])) $gibtsschon=true; else $gibtsschon=false;

                if (!$gibtsschon) {
                	$query="SELECT participant_id FROM ".table('participants_temp')." 
                 		WHERE participant_id=".$participant_id." OR participant_id_crypt='$crypt_id'";
                	$line=orsee_query($query);
                	if (isset($line['participant_id'])) $gibtsschon=true; else $gibtsschon=false;	
                	}
          	}
	return $participant_id; 
}

// create new id and replace old id with it
function participant__create_new_participant_id($participant_id) {
	//participants,participants_temp
	$tables=array(  "participants"=>"participant_id",
			"participants_temp"=>"participant_id",
			"mail_queue"=>"mail_recipient",
			"participate_at"=>"participant_id",
			);
	$crypt_tables=array("participants","participants_temp");
	foreach ($tables as $table=>$column) {
		$query="LOCK TABLES ".table($table);
		$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
		}
	$new_id=participant__create_participant_id();
	foreach ($tables as $table=>$column) {
                $query="UPDATE ".table($table)." SET ".$column."='".$new_id."' WHERE ".$column."='".$participant_id."'";
                $done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
                }
	foreach ($crypt_tables as $table) {
		$query="UPDATE ".table($table)." SET participant_id_crypt='".unix_crypt($new_id)."' WHERE participant_id='".$new_id."'";
		$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
	}
	$query="UNLOCK TABLES";
	$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
	return $new_id;
}



// CHECKS
// check unique
function participantform__check_unique($edit,$formtype,$participant_id=0,$exp_types="",$remarks="") {
	global $lang, $settings, $errors__dataform;
	
	$disable_form=false; $problem=false; $nonunique_fields=array();
    if (!isset($edit['subpool_id']) || !$edit['subpool_id']) $edit['subpool_id']=$settings['subpool_default_registration_id'];
	$subpool=orsee_db_load_array("subpools",$edit['subpool_id'],"subpool_id");
	if (!$subpool['subpool_id']) $subpool=orsee_db_load_array("subpools",1,"subpool_id");
	$edit['subpool_id']=$subpool['subpool_id'];

	$nonunique=participantform__get_nonunique($edit,($formtype=='create')?0:$participant_id);	
	// var_dump($nonunique);
	if ($formtype=='create') {
		foreach ($nonunique as $f) {
			// conditions for check: 1) subpool must fit, 2) must be a public field, 3) form not yet disabled, 4) requires unique in creation form
			if(($f['subpools']=='all' || in_array($subpool['subpool_id'],explode(",",$f['subpools']))) && 
				($f['admin_only']!='y') &&
				$disable_form==false && 
				$f['require_unique_on_create_page']=='y'
					) {
			
				if ($f['nonunique_participants']) {
					$errors__dataform[]=$f['mysql_column_name'];
					$deleted=participant__deleted($f['nonunique_participants_list'][0]);
					$excluded=participant__excluded($f['nonunique_participants_list'][0]);
					$can_undelete=participant__can_undelete($f['nonunique_participants_list'][0]);

					if($excluded && $f['unique_on_create_page_tell_if_deleted']=='y') {
						message($lang['error_sorry_you_are_excluded']);
						message($lang['if_you_have_questions_write_to'].' '.support_mail_link());
						$disable_form=true;
					} elseif($deleted && $f['unique_on_create_page_tell_if_deleted']=='y' && !$can_undelete) {
						message($lang['error_sorry_you_are_deleted']);
						if($can_undelete) message('<br><form action="participant_edit.php"><input type=hidden name="p" value="'.url_cr_encode($f['nonunique_participants_list'][0]).'"><input type=hidden name=resubscribe value=1><input type=submit style="margin-left:50px" value="'.lang('reactivate_your_account').'"></form>');
						message($lang['if_you_have_questions_write_to'].' '.support_mail_link());
						$disable_form=true;		
					} else {
						$problem=true;
						if($deleted && $can_undelete) {
							message($lang['error_sorry_you_are_deleted']."<hr>".lang('the_link_that_we_have_sent_you_will_allow_you_to_reactivate_your_account'));
						}
						else {
							if (isset($lang[$f['unique_on_create_page_error_message_if_exists_lang']])) message($lang[$f['unique_on_create_page_error_message_if_exists_lang']]);
							else message($f['unique_on_create_page_error_message_if_exists_lang']);
						}
						if($f['unique_on_create_page_email_regmail_confmail_again']=='y') {
							$disable_form=true;
							$resend_mail=true;
							$logs=array();
							foreach($f['nonunique_participants_list'] as $nonunique_pid) 
								$logs=array_merge($logs,orsee_db_load_full_array('participants_log',$nonunique_pid,'id'));
							if(!empty($participant_id) && !in_array($participant_id,$f['nonunique_participants_list']))
								$logs=array_merge($logs,orsee_db_load_full_array('participants_log',$participant_id,'id'));
							$ctime=time();
							foreach($logs as $log) {
								$ctimediff=$ctime-$log['timestamp'];
								if($log['action'] == "resubscribe_attempt" && $ctimediff<3600*24 && (empty($participant_id) || $participant_id==$log['id'])) $resend_mail=false;
							}
							// var_dump($participant_id,$f,$resend_mail,$logs,$ctimediff); exit;
							if($resend_mail) {
								$supmess="";
								$cpid=empty($participant_id)?$f['nonunique_participants_list'][0]:$participant_id;
								if(!empty($exp_types)) {
									$supmess='<br><br><strong>'.lang('click_on_the_received_link_to_subscribe_to_this_experiment_type').'</strong>';
									$log_arr=log__participant("resubscribe_attempt",$cpid,$exp_types.'|'.$remarks,true);
									$cid=(strtolower(PHP_OS)=="linux")?strtr(crypt($log_arr["log_id"],"su"),'/','_'):$log_arr["log_id"];
									$exp_type_names=load_external_experiment_type_names(true,false,true);
									$exp_type_names_selected=[];
									foreach(explode(",",$exp_types) as $etname) $exp_type_names_selected[]=$exp_type_names[$etname];
									$supmailmessage=lang('please_click_on_the_link_below_to_subscribe_to').' '.implode(', ',$exp_type_names_selected);
									$addtolink='subscribe='.$cid;
									$done=experimentmail__mail_edit_link($cpid,$supmailmessage,$addtolink);
								}
								else {
									$done=experimentmail__mail_edit_link($cpid);
									log__participant("resubscribe_attempt",$cpid);
								}
								message($lang['message_with_edit_link_mailed'].$supmess);
							}							
						}
					}
				} elseif($f['nonunique_participants_temp']) {
						$problem=true;
						$errors__dataform[]=$f['mysql_column_name'];
						if($f['unique_on_create_page_email_regmail_confmail_again']=='y') {
							$mess=$lang['already_registered_but_not_confirmed'];
							$resend_mail=true;
							$logs=orsee_db_load_full_array('participants_log',$f['nonunique_participants_temp_list'][0],'target');
							$ctime=time();
							foreach($logs as $log) {
								$ctimediff=$ctime-$log['timestamp'];
								if($ctimediff<3600*24) $resend_mail=false;
							}
							// var_dump($f,$resend_mail,$logs,$ctimediff); exit;
							if($resend_mail) {
								foreach($logs as $log) {
									orsee_db_save_array(['timestamp'=>time()],"participants_log",$log['log_id'],'log_id');
								}
								$mess.=' '.$lang['confirmation_message_mailed_again'];
								$done=experimentmail__confirmation_mail($f['nonunique_participants_temp_list'][0]);
							}
							message($mess);
							$disable_form=true;
						}
						else {
							if (isset($lang[$f['unique_on_create_page_error_message_if_exists_lang']])) message($lang[$f['unique_on_create_page_error_message_if_exists_lang']]);
							else message($f['unique_on_create_page_error_message_if_exists_lang']);
						}
				}
			}
		}
	$response=array(); $response['disable_form']=$disable_form; $response['problem']=$problem;
	// var_dump($response);exit;
	return $response;
	} elseif ($formtype=='edit') {
		foreach ($nonunique as $f) {
			// var_dump($f);
			// conditions for check: 1) subpool must fit, 2) must be a public field, 3) requires unique in edit form
			if(($f['subpools']=='all' || in_array($subpool['subpool_id'],explode(",",$f['subpools']))) && 
				($f['admin_only']!='y') && $f['check_unique_on_edit_page']=='y') {
			
				if ($f['nonunique_participants'] || $f['nonunique_participants_temp']) {
					$errors__dataform[]=$f['mysql_column_name'];
					$problem=true;
					if (isset($lang[$f['unique_on_edit_page_error_message_if_exists_lang']])) message($lang[$f['unique_on_edit_page_error_message_if_exists_lang']]);
					else message($f['unique_on_edit_page_error_message_if_exists_lang']);
				} 
			}
		}
	}
	$response=array(); $response['problem']=$problem;
	return $response;
}


function participantform__get_nonunique($edit,$participant_id=0) {
	$nonunique_fields=array(); 
	$formfields=participantform__load();
	foreach ($formfields as $f) { 
	$f['nonunique_participants']=false; $f['nonunique_participants_list']=array();
	$f['nonunique_participants_temp']=false; $f['nonunique_participants_temp_list']=array();
	if(($f['require_unique_on_create_page']=='y' || $f['check_unique_on_edit_page']=='y') &&
		(isset($edit[$f['mysql_column_name']]) && $edit[$f['mysql_column_name']]) ) { 
		$query="SELECT participant_id FROM ".table('participants')." 
           		WHERE ".$f['mysql_column_name']."='".mysqli_real_escape_string($GLOBALS['mysqli'],$edit[$f['mysql_column_name']])."'";
        if ($participant_id) $query.=" AND participant_id!='".mysqli_real_escape_string($GLOBALS['mysqli'],$participant_id)."'";
		$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$query);
       	while ($line = mysqli_fetch_assoc($result)) $f['nonunique_participants_list'][]=$line['participant_id'];
		if (count($f['nonunique_participants_list'])>0) $f['nonunique_participants']=true;
		else {
			$query="SELECT participant_id FROM ".table('participants_temp')." 
           			WHERE ".$f['mysql_column_name']."='".mysqli_real_escape_string($GLOBALS['mysqli'],$edit[$f['mysql_column_name']])."'";
           	if ($participant_id) $query.=" AND participant_id!='".mysqli_real_escape_string($GLOBALS['mysqli'],$participant_id)."'";
			$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$query);
       		while ($line = mysqli_fetch_assoc($result)) $f['nonunique_participants_temp_list'][]=$line['participant_id'];
			if (count($f['nonunique_participants_temp_list'])>0) $f['nonunique_participants_temp']=true;
		}
		if ($f['nonunique_participants'] || $f['nonunique_participants_temp']) $nonunique_fields[$f['mysql_column_name']]=$f; 	
	}}
	return $nonunique_fields;
}


// check fields
function participantform__check_fields($edit,$admin,$showmessage=true,$check_invitations=true) {
	global $lang, $settings;
	$errors_dataform=array();
	
    if (!isset($edit['subpool_id']) || !$edit['subpool_id']) $edit['subpool_id']=$settings['subpool_default_registration_id'];
	$subpool=orsee_db_load_array("subpools",$edit['subpool_id'],"subpool_id");
	if (!$subpool['subpool_id']) $subpool=orsee_db_load_array("subpools",1,"subpool_id");
	$edit['subpool_id']=$subpool['subpool_id'];
	
	$formfields=participantform__load();
	foreach ($formfields as $f) { 
	// var_dump($f);
	if($f['subpools']=='all' | in_array($subpool['subpool_id'],explode(",",$f['subpools']))) {
		if ($f['type']=='invitations' && $check_invitations) {
			if (!isset($_REQUEST[$f['mysql_column_name']]) || !is_array($_REQUEST[$f['mysql_column_name']])) $_REQUEST[$f['mysql_column_name']]=array();
			$_REQUEST[$f['mysql_column_name']]=implode(",",$_REQUEST[$f['mysql_column_name']]);
			$edit[$f['mysql_column_name']]=$_REQUEST[$f['mysql_column_name']];
  		}
		if ($admin || $f['admin_only']!='y') {
			if ($f['compulsory']=='y') {
				if(!isset($edit[$f['mysql_column_name']]) || !$edit[$f['mysql_column_name']]) {
					$errors_dataform[]=$f['mysql_column_name'];
					if (isset($lang[$f['error_message_if_empty_lang']]) && $showmessage) message($lang[$f['error_message_if_empty_lang']]);
					elseif($showmessage) message($f['error_message_if_empty_lang']);
				}
			}
			if ($f['perl_regexp']!='' && isset($edit[$f['mysql_column_name']])) {
				if(!preg_match($f['perl_regexp'],$edit[$f['mysql_column_name']])) {
					$errors_dataform[]=$f['mysql_column_name'];
					if (isset($lang[$f['error_message_if_no_regexp_match_lang']]) && $showmessage) message($lang[$f['error_message_if_no_regexp_match_lang']]);
					elseif($showmessage) message($f['error_message_if_no_regexp_match_lang']);
				}
			}
		}
	}}
	return $errors_dataform;
}



// form fields
function participantform__allvalues() {
$array=array(
'admin_only'=>'n',
'allow_sort_in_session_participants_list'=>'n',
'check_unique_on_edit_page'=>'n',
'cols'=>'40',
'compulsory'=>'n',
'default_value'=>'',
'error_message_if_empty_lang'=>'',
'error_message_if_no_regexp_match_lang'=>'',
'include_in_statistics'=>'n',
'include_none_option'=>'n',
'link_as_email_in_lists'=>'n',
'list_in_session_participants_list'=>'n',
'list_in_session_pdf_list'=>'n',
'maxlength'=>'100',
'option_values_lang'=>'',
'option_values'=>'',
'perl_regexp'=>'',
'require_unique_on_create_page'=>'n',
'rows'=>'3',
'search_include_in_experiment_assign_query'=>'n',
'search_include_in_participant_query'=>'n',
'search_result_allow_sort'=>'n',
'search_result_sort_order'=>'',
'searchresult_list_in_experiment_assign_results'=>'n',
'searchresult_list_in_participant_results'=>'n',
'size'=>'40',
'subpools'=>'all',
'unique_on_create_page_email_regmail_confmail_again'=>'n',
'unique_on_create_page_error_message_if_exists_lang'=>'',
'unique_on_create_page_tell_if_deleted'=>'n',
'unique_on_edit_page_error_message_if_exists_lang'=>'',
'value_begin'=>'0',
'value_end'=>'1',
'value_step'=>'0',
'values_reverse'=>'n',
'wrap'=>'virtual'
);
return $array;
}

function participantform__load() {
	$pform=participantform__define();
	foreach ($pform as $k=>$f) {
		$t=participantform__allvalues();
		if(!empty($GLOBALS['SUP_COMPULSORY_FIELDS']) && is_array($GLOBALS['SUP_COMPULSORY_FIELDS']) && in_array($f['mysql_column_name'],$GLOBALS['SUP_COMPULSORY_FIELDS'])) { $t['compulsory']='y'; $t['error_message_if_empty_lang']='please_fill_all_cumpulsory_fields';  }
		foreach ($f as $kf=>$vf) {
			$t[$kf]=$vf;
		}
		$pform[$k]=$t;
	}
	return $pform;
}

function template_replace_callbackA(array $m) {
	global $tempout;
	if ($m[1]=='!') {
        if ((!(isset($tempout[$m[2]])) || (!$tempout[$m[2]]))) return $m[3];
        else return '';
    } else {
        if (isset($tempout[$m[2]]) && $tempout[$m[2]]) return $m[3];
        else return '';
    }
}

function template_replace_callbackB(array $m) {
	global $lang;
	return lang($m[1]);
}


// processing the template
function load_form_template($tpl_name,$out) {
	global $lang, $settings__root_to_server, 
		$settings__root_directory, $settings;
		global $tempout;
		$tempout = $out;

	// get the template
    //    $tpl=file_get_contents($settings__root_to_server.
    //                            $settings__root_directory.
    //                            '/ftpl/'.$tpl_name.'.tpl');
     $tpl=file_get_contents('../ftpl/'.$tpl_name.'.tpl');
	// process conditionals
	$pattern="/\{[^#\}]*#(!?)([^#!\}]+)#([^\}]+)\}/i";
    $replacement = "($1\$out['$2'])?\"$3\":''";
    $tpl=preg_replace_callback($pattern, 
    	'template_replace_callbackA', 
    	$tpl);


	// fill in the vars
	foreach ($out as $k=>$o) $tpl=str_replace("#".$k."#",$o,$tpl);

	// fill in language terms
        $pattern="/lang\[([^\]]+)\]/i";
        $replacement = "\$lang['$1']";
        $tpl=preg_replace_callback($pattern, 
    	'template_replace_callbackB', 
    	$tpl);
                                       
	return $tpl;
}


function form__replace_funcs_in_field($f) {
	global $lang, $settings;
    foreach ($f as $o=>$v) {
		if (substr($f[$o],0,5)=='func:') eval('$f[$o]='.substr($f[$o],5).';');
    }
	return $f;
}


// generic fields
function form__render_field($f, $notnew=false) {
	$out='';
	switch($f['type']) {
		case 'textline': $out=($notnew && substr($f['mysql_column_name'],1)=='name')?form__render_textline_nochange($f):form__render_textline($f); break;
		case 'textarea': $out=form__render_textarea($f); break;
		case 'radioline': $out=form__render_radioline($f); break;
		case 'select_list': $out=form__render_select_list($f); break;
		case 'select_numbers': $out=form__render_select_numbers($f); break;
		case 'select_lang': $out=form__render_select_lang($f); break;
		case 'hidden': $out=form__render_hidden($f); break;
	}
	return $out;
}

function form__render_hidden($f) {
	$out='<INPUT  type="hidden" name="'.$f['mysql_column_name'].'" value="'.$f['value'].'">';
	return $out;
}
function form__render_textline($f) {
	$out='<INPUT  class="form-control" type="text" id="input_'.$f['mysql_column_name'].'" name="'.$f['mysql_column_name'].'" value="'.$f['value'].'" size="'.
		$f['size'].'" maxlength="'.$f['maxlength'].'">';
	return $out;
}

function form__render_textline_nochange($f) {
	$shwn_value=$f['value'];
	if($f['mysql_column_name']=="fname" || $f['mysql_column_name']=="lname") $shwn_value=str_replace("?","",$shwn_value);
	$out='<INPUT class="form-control" type="text" disabled="disabled" id="input_'.$f['mysql_column_name'].'" name="'.$f['mysql_column_name'].'_nochange" style="background-color:buttonface" value="'.$shwn_value.'" size="'.
		$f['size'].'" maxlength="'.$f['maxlength'].'">'.
		'<INPUT  type="hidden" name="'.$f['mysql_column_name'].'" value="'.$f['value'].'">';
	return $out;
}

function participant__form_lname_nochange() {
	global $lang;
	tpr("lname");
	echo '<TD>'.$lang['lastname'].':</TD>
	      <TD><INPUT name=lname_nochange type=text disabled size=40 maxlength=50 value="'.str_replace("?","",$_REQUEST['lname']).'"><INPUT name=lname type=hidden value="'.$_REQUEST['lname'].'"></TD>
	      </TR>';
}

function form__render_textarea($f) {
        $out='<textarea  class="form-control" id="input_'.$f['mysql_column_name'].'" name="'.$f['mysql_column_name'].'" cols="'.$f['cols'].'" rows="'.
                $f['rows'].'"';
		if(!empty($GLOBALS['expadmindata']['adminname'])) $out.=' style="resize:both" ';		
		$out.=' wrap="'.$f['wrap'].'">'.$f['value'].'</textarea>';
        return $out;
}

function form__render_radioline($f,$where='',$show_count=false,$experiment_id=0,$no_empty=false) {
	global $lang;
	$optionvalues=explode(",",$f['option_values']);
	$optionnames=explode(",",$f['option_values_lang']);
	if($show_count && !in_array('?',$optionvalues) && !$no_empty) {
		if(count($optionvalues)==count($optionnames)) $optionnames[]='?';
		$optionvalues[]='?';
	}
	$items=array();
	foreach($optionvalues as $k=>$v) {
		if (isset($optionnames[$k])) $items[$v]=$optionnames[$k];
	}
	$out='';
	foreach ($items as $val=>$text) {
		if($show_count) {
			$query="SELECT count(*) as tf_count, ".$f['mysql_column_name']." as tf_value 
					FROM ".table('participants')."  
					WHERE ".table('participants').".participant_id IS NOT NULL ";
			if($experiment_id!=0)  $query.=" AND participant_id IN (SELECT participant_id FROM ".table('participate_at')." WHERE experiment_id=$experiment_id )";
			if(!empty($where)) $query.=" AND ".$where;
			$query.=" AND ".$f['mysql_column_name']."='".$val."' ";
			$query.=" GROUP BY ".$f['mysql_column_name']." 
					ORDER BY ".$f['mysql_column_name'];
			$line=orsee_query($query);
			// echo $query;
		}
		$out.='<div class="form-check" style="display:inline-block"><INPUT class="form-check-input" id="radioinput_'.$f['mysql_column_name'].'_'.$val.'" name="'.$f['mysql_column_name'].'" type="radio" value="'.$val.'"';
        if ($f['value']==$val) $out.=" CHECKED";
       	$out.='><label class="form-check-label" for="radioinput_'.$f['mysql_column_name'].'_'.$val.'">';
       	if (isset($lang[$text])) $out.=$lang[$text]; else $out.=$text;
		if ($show_count && $line !== false) $out.=' ('.$line['tf_count'].')';
       	$out.='</label></div>&nbsp;&nbsp;&nbsp;';
	}
	// $out.='';
    return $out;
}

function form__render_select_list($f) {
	global $lang;
	$optionvalues=explode(",",$f['option_values']);
	$optionnames=explode(",",$f['option_values_lang']);
	if ($f['include_none_option']=='y') $incnone=true; else $incnone=false;
	$items=array();
	foreach($optionvalues as $k=>$v) {
		if (isset($optionnames[$k])) $items[$v]=$optionnames[$k];
	}
	$out='';
	$out=helpers__select_text_out($items,$f['mysql_column_name'],$f['value'],$incnone);
    return $out;
}

function form__render_select_lang($f) {
	if ($f['include_none_option']=='y') $incnone=true; else $incnone=false;
	// var_dump($f['checkbox_type'], $f['mysql_column_name'], ( empty($f['checkbox_type']) || $f['checkbox_type']=='n' ));
	$out=( empty($f['checkbox_type']) || $f['checkbox_type']=='n' )  ? language__selectfield_item($f['mysql_column_name'],$f['mysql_column_name'],$f['value'],$incnone) : language__checkboxfield_item($f['mysql_column_name'],$f['mysql_column_name'],$f['value'],$incnone,'',false,'',false,0,$f);
	return $out;
}


function form__render_select_numbers($f) {
		if ($f['include_none_option']=='y') $incnone=true; else $incnone=false;
		if ($f['values_reverse']=='y') $reverse=true; else $reverse=false;
        $out=participant__select_numbers($f['mysql_column_name'],$f['value'],$f['value_begin'],$f['value_end'],0,$f['value_step'],$reverse,$incnone);
        return $out;
}

// special fields
function participant__invitations_form_field($subpool_id,$varname,$value,$suptypes=array(),$f=array(),$participant_id='',$forceall=false,$maintable="participants") {
	global $settings, $lang;
	if (!$subpool_id) $subpool_id=$settings['subpool_default_registration_id'];
	if(!is_array($suptypes)) $suptypes=explode(",",$suptypes);
    $checked=explode(",",$value);
    $query="SELECT *
            FROM ".table('experiment_types')." as texpt, 
            ".table('lang')." as tlang, ".table('subpools')." as tsub";
			if(!empty($participant_id)) $query.=", ".table($maintable)." as tpart";
			$query.="
            WHERE texpt.exptype_id=tlang.content_name
            AND tlang.content_type='experiment_type'
            AND texpt.enabled='y'
			AND ";
			$pidquery=$forceall?"":" AND (tsub.experiment_types LIKE concat('%',texpt.exptype_name ,'%') OR tpart.subscriptions LIKE concat('%',texpt.exptype_name ,'%'))";
			$query.=(empty($participant_id)?"tsub.experiment_types LIKE concat('%',texpt.exptype_name ,'%') ":"tpart.participant_id=".$participant_id.$pidquery);
			$query.="
			AND tsub.subpool_id='".$subpool_id."' 
            ORDER BY exptype_id";
    $result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
    $out='';
	$hidden_exptypes=[];
	if(in_array("hide_some_exptypes",$suptypes)) {
		foreach($suptypes as $stel) {
			$astel=explode(":",$stel);
			if(($astel[0]=="hidden_exptypes") && count($astel)>1) $hidden_exptypes=explode(",",substr($stel,strlen($astel[0])+1));
		}
	}
    while ($line = mysqli_fetch_assoc($result)) {
		if(in_array($line['exptype_name'],$hidden_exptypes)) continue;
		$exptype_mapping=explode(',',$line['exptype_mapping']);
		if(in_array("nolab",$suptypes) && in_array('laboratory',$exptype_mapping)) continue;
		if(in_array("onlylab",$suptypes) && !in_array('laboratory',$exptype_mapping)) continue;
		$supout="";
    	$out.='<div class="form-check"><label class="form-check-label" for="'.$varname.'_'.$line['exptype_name'].'"><INPUT type="checkbox" class="form-check-input" id="'.$varname.'_'.$line['exptype_name'].'" name="'.$varname.'['.$line['exptype_name'].']"
                                        value="'.$line['exptype_name'].'"';
		if(!empty($f['negative_form']) && $f['negative_form']!='n') $out.='onchange="if(this.checked) document.getElementById(this.name+0).style.textDecoration=\'line-through\'; else document.getElementById(this.name+0).style.textDecoration=\'none\'" ';
		elseif(!empty($f['positive_form']) && $f['positive_form']!='n') $out.='onchange="if(this.checked) document.getElementById(this.name+0).style.textShadow=\'1px 0px 0px black\'; else document.getElementById(this.name+0).style.textShadow=\'\'" ';
        if (in_array($line['exptype_name'],$checked)) $out.=" CHECKED";
		else {
			$exptypes=explode(',',rasee_db_load_value('experiment_types',$line['exptype_name'],'exptype_name','exptype_mapping'));
			$alreadychecked=false;
			if(in_array("laboratory",$exptypes) && !empty($_REQUEST['dr'])) {
				$likelines=orsee_db_load_full_array_like('experiment_types','laboratory','exptype_mapping');
				if(count($likelines)==1) $out.=" CHECKED";
				$alreadychecked=true;
				if(in_array("onlylab",$suptypes)) $supout.='<INPUT type="hidden" name="onlylab" value=1>';
			}
			if(in_array("internet",$exptypes)) {
				if(!$alreadychecked && !empty($_REQUEST['onlinedr'])) {
					$likelines=orsee_db_load_full_array_like('experiment_types','internet','exptype_mapping');
					if(count($likelines)==1) $out.=" CHECKED";
					$alreadychecked=true;
				}
				if(in_array("nolab",$suptypes)) {
					$oo=(!empty($_REQUEST['oo']) || !empty($_REQUEST['onlineonly']) || !empty($_REQUEST['onlyonline']));
					$supinputname=$oo?"onlineonly":"notlab";
					$supout.='<INPUT type="hidden" name="'.$supinputname.'" value=1>';
				}
			}
		}
        $out.='>';
		if(!empty($f['negative_form']) && $f['negative_form']!='n') $prev_decoration=(in_array($line['exptype_name'],$checked))?"line-through":"none";
		elseif(!empty($f['positive_form']) && $f['positive_form']!='n') $prev_decoration=(in_array($line['exptype_name'],$checked))?"1px 0px 0px black":"";
		if(!empty($f['negative_form']) && $f['negative_form']!='n') $out.='<span id="'.$varname.'['.$line['exptype_name'].']0" style="text-decoration:'.$prev_decoration.'">';
		elseif(!empty($f['positive_form']) && $f['positive_form']!='n') $out.='<span id="'.$varname.'['.$line['exptype_name'].']0" style="text-shadow:'.$prev_decoration.'">';
		$out.=$line[$lang['lang']];
		if((!empty($f['negative_form']) && $f['negative_form']!='n')  || (!empty($f['positive_form']) && $f['positive_form']!='n')) $out.='</span>';
        $out.=$supout.'</label></div>
                     ';
    }
    return $out;
}

function participant__rules_signed_form_field($current_rules_signed="") {
	global $lang;
	$out='<input type=radio name=rules_signed value="y"';
	if ($current_rules_signed=="y") $out.=" CHECKED";
        $out.='>'.$lang['yes'].'&nbsp;&nbsp;&nbsp;
              <input type=radio name=rules_signed value="n"';
        if ($current_rules_signed!="y") $out.=" CHECKED";
        $out.='>'.$lang['no'];
	return $out;
}

function participant__remarks_form_field($current_remarks="") {
        global $lang;
        $out='<TEXTAREA name=remarks rows=3 cols=70 wrap=virtual>';
        $out.=$current_remarks;
        $out.='</textarea>';
	return $out;
}

function participant__add_to_session_checkbox() {
        $out='<INPUT type=checkbox name=register_session value="y">';
	return $out;
}
function participant__add_to_session_select($participant_id,$session_id="") {
        $out=select__sessions($session_id,"session_id","",true,$participant_id);
	return $out;
}

function participant__select_numbers($name,$prevalue,$begin,$end,$fillzeros=2,$steps=1,$reverse=false,$incnone=false,$existing=false,$where='',$show_count=false) {
	$out='';
	$out.= '<select ';
	if(empty($GLOBALS['expadmindata']['adminname'])) $out.='class="form-select" ';
	$out.= ' name="'.$name.'" id="input_'.$name.'">';
	if ($incnone) $out.='<option value="">-</option>';

	if (!$existing) {
		if ($begin<$end) { $lb=$begin; $ub=$end;} else { $lb=$end; $ub=$begin; }
		if ($reverse) $i=$ub; else $i=$lb;
		if ($steps<1) $steps=1;
		while (($reverse==false && $i<=$ub) || ($reverse==true && $i>=$lb)) {
			$out.='<option value="'.$i.'"'; if ($i == (int) $prevalue) $out.=' SELECTED'; $out.='>';
			$out.=helpers__pad_number($i,$fillzeros); $out.='</option>';
			if ($reverse) $i=$i-$steps; else $i=$i+$steps;
		}
	} else {
		$query="SELECT count(*) as tf_count, ".$name." as tf_value 
				FROM ".table('participants')." 
				WHERE ".table('participants').".participant_id IS NOT NULL ";
		if($where) $query.=" AND ".$where." ";
		$query.=" GROUP BY ".$name." 
				  ORDER BY ".$name;
		if ($reverse) $query.=" DESC ";
		$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
		$listitems=array();
        while ($line = mysqli_fetch_assoc($result)) {
			if(!isset($listitems[$line['tf_value']])) $listitems[$line['tf_value']]=$line;
            else $listitems[$line['tf_value']]['tf_count']=$listitems[$line['tf_value']]['tf_count']+$line['tf_count'];
        }
        foreach ($listitems as $line) {
            $out.='<option value="'.$line['tf_value'].'"'; if ($line['tf_value'] == (int) $prevalue) $out.=' SELECTED'; $out.='>';
            if ($line['tf_value']!='') $out.=helpers__pad_number($line['tf_value'],$fillzeros);
            else $out.='-';
            if ($show_count) $out.=' ('.$line['tf_count'].')';
        	$out.='</option>';
        }
                
		
		while ($line = mysqli_fetch_assoc($result)) {
			$out.='<option value="'.$line['tf_value'].'"'; if ($line['tf_value'] == (int) $prevalue) $out.=' SELECTED'; $out.='>';
			$out.=helpers__pad_number($line['tf_value'],$fillzeros); 
			if ($show_count) $out.=' ('.$line['tf_count'].')';
			$out.='</option>';
		}
	}
	$out.='</select>';
	return $out;
}

function participant__select_existing($name,$prevalue,$where='',$show_count=false) {
	$out='';
	$out.='<select name="'.$name.'">';
	$query="SELECT count(*) as tf_count, ".$name." as tf_value 
			FROM ".table('participants')." 
			WHERE ".table('participants').".participant_id IS NOT NULL ";
	if($where) $query.=" AND ".$where." ";
	$query.=" GROUP BY ".$name." 
			  ORDER BY ".$name;
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
	while ($line = mysqli_fetch_assoc($result)) {
		$out.='<option value="'.$line['tf_value'].'"'; if ($line['tf_value'] == $prevalue) $out.=' SELECTED'; $out.='>';
		$out.=$line['tf_value']; 
		if ($show_count) $out.=' ('.$line['tf_count'].')';
		$out.='</option>';
	}
	$out.='</select>';
	return $out;
}


/*function participant__checkbox_existing($name,$prevalue,$where='',$show_count=false) {
	global $lang, $settings, $color;
	$out='';
	$out.='<TABLE width=100% cellspacing=0 cellpadding=0><TR bgcolor="'.$color['list_shade1'].'">';
	$query="SELECT count(*) as tf_count, ".$name." as tf_value 
			FROM ".table('participants')." 
			WHERE ".table('participants').".participant_id IS NOT NULL ";
	if($where) $query.=" AND ".$where." ";
	$query.=" GROUP BY ".$name." 
			  ORDER BY ".$name;
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
	$cols=$settings['query_experiment_list_nr_columns'];
	$ccol=1;
	$i=0;
	while ($line = mysqli_fetch_assoc($result)) {
		$out.='<TD class="small">
							<INPUT class="small" type=checkbox name="'.$name.'['.$i.']" value="'.$line['tf_value'].'"';
			if ($line['tf_value'] == $prevalue) $out.=' CHECKED';
			$out.='>'.$line['tf_value'];
			if ($show_count) $out.=' ('.$line['tf_count'].')';
			$out.='</TD>';
			if ($ccol==$cols) { 
				$ccol=1; 
				$out.= '</TR><TR bgcolor="';
				if ($shade==true) $out.= $color['list_shade1']; else $out.= $color['list_shade2'];
				$out.= '">'; 
				if ($shade==true) $shade=false; else $shade=true;
				} else $ccol=$ccol+1; 
		$i++;
	}
	if ($ccol>1) {
		while ($ccol <= $cols) {
			$out.= '<TD></TD>';
			$ccol=$ccol+1;
			}
		$out.= '</TR><TR>';
		}
 		$out.= '<TD></TD></TR></TABLE>';
	return $out;
}
*/

// the participant form
function participant__show_form($edit,$button_title="",$form_title="",$errors=array(),$admin=false,$notnew=false,$suptypes=array(),$several=false,$maintable="participants",$openform=true,$closeform=true,$nameprefix="") {
	global $lang, $subpool, $settings, $color;
	if(!is_array($suptypes)) $suptypes=explode(",",$suptypes);
	$out=array(); $tout=array();
	echo '<CENTER><BR>
		<h4>'.$form_title.'</h4>
		';
	show_message();
	if($edit===false) $edit=array();
	if (!isset($edit['participant_id'])) $edit['participant_id']='';
    if (!isset($edit['subpool_id']) || !$edit['subpool_id']) $edit['subpool_id']=$settings['subpool_default_registration_id'];
	$subpool=orsee_db_load_array("subpools",$edit['subpool_id'],"subpool_id");
	if (!$subpool['subpool_id']) $subpool=orsee_db_load_array("subpools",1,"subpool_id");
	$edit['subpool_id']=$subpool['subpool_id'];
	
	$pools=subpools__get_subpools();
	foreach ($pools as $p) $out['is_subjectpool_'.$p]=false;
	$out['is_subjectpool_'.$subpool['subpool_id']]=true;
	$out['is_subpool_type_w']=false; $out['is_subpool_type_s']=false; $out['is_subpool_type_b']=false;
	$out['is_subpool_type_'.$subpool['subpool_type']]=true;
	if ($admin) { $out['is_admin']=true; $out['is_not_admin']=false; }
	else { $out['is_admin']=false; $out['is_not_admin']=true; }
	
	
	if($openform) echo '<FORM action="'.thisdoc().'" method="POST">';
	
    if ($admin) echo '<INPUT type="hidden" name="participant_id" value="'.$edit['participant_id'].'">';
	else echo '<INPUT type=hidden name=p value="'.url_cr_encode($edit['participant_id']).'">';
	if (!isset($edit['s'])) $edit['s']='';
    if (!isset($edit['dr'])) $edit['dr']='';
    echo '<INPUT type=hidden name=s value="'.$edit['s'].'">';
	echo '<INPUT type=hidden name=dr value="'.$edit['dr'].'">';
	if (!$admin) echo '<INPUT type=hidden name=subpool_id value="'.$edit['subpool_id'].'">'; 

	if ($admin) $nonunique=participantform__get_nonunique($edit,$edit['participant_id']);

	$formfields=participantform__load();
	
	$compulsory_fileds_only=false;
	if(in_array("onlineonly",$suptypes)) {$compulsory_fileds_only=true; $suptypes[]="nolab"; unset($suptypes[array_search("onlineonly",$suptypes)]);}
	$several_values_for_each_field=false;
	$hide_begin_of_studies=true;
	foreach ($formfields as $f) {
		// if(in_array("construction",$suptypes) && $f['mysql_column_name']=="begin_of_studies") continue;
		if($f['subpools']=='all' | in_array($subpool['subpool_id'],explode(",",$f['subpools']))) {
	
			$f=form__replace_funcs_in_field($f);
			if (isset($edit[$nameprefix.$f['mysql_column_name']])) $f['value']=$edit[$nameprefix.$f['mysql_column_name']];
			else $f['value']=$f['default_value'];
			

			if ($f['type']=='language') {
				$part_langs=lang__get_part_langs();
				if (count($part_langs)>1) {
					$out['multiple_participant_languages_exist']=true;
					$tout['multiple_participant_languages_exist']=true;
				} else {
					$out['multiple_participant_languages_exist']=false;
					$tout['multiple_participant_languages_exist']=false;
				}
				$field=lang__select_lang($f['mysql_column_name'],$f['value'],$type="part");
			} elseif ($f['type']=='invitations') {
				$forceadmin=false;
				if($admin && $f['mysql_column_name']=="subscriptions" && !empty($_GET['all_exptypes'])) {$forceadmin=true;}
				// var_dump($_GET);
				// var_dump($nameprefix.$f['mysql_column_name'],$f['value']);
				$field=participant__invitations_form_field($subpool['subpool_id'],$nameprefix.$f['mysql_column_name'],$f['value'],$suptypes,$f,$edit['participant_id'],$forceadmin,$maintable);
				if($admin && $f['mysql_column_name']=="subscriptions") {
					$thislink=thisdoc().'?participant_id='.$edit['participant_id'];
					if($maintable=="participants_temp") $thislink.='&temptable=1';
					$thislinktext=lang('show_only_relevant');
					if(empty($_GET['all_exptypes'])) {
						$thislink.='&all_exptypes=1';
						$thislinktext=lang('show_all');
					}
					$field.='<div class="container"><a class="btn btn-secondary" href="'.$thislink.'">'.$thislinktext.'</a></div>';
				}
			} else {
				// var_dump($compulsory_fileds_only);
				// if($compulsory_fileds_only && (empty($f['compulsory']) || $f['compulsory']!='y')) continue; 
				if($several) {$f['type']="textarea"; $several_values_for_each_field=true;}
				$field=form__render_field($f,$notnew);
				if(!empty($nameprefix)) $field=preg_replace("/\bname=(['\"])".$f['mysql_column_name']."['\"]/","name=$1".$nameprefix.$f['mysql_column_name']."$1",$field);
			}
			if ($admin) {
				if (isset($nonunique[$f['mysql_column_name']])) {
					if (isset($lang['not_unique'])) $note=$lang['not_unique'];
							else $note='not unique';
					$link='participants_show.php?new_query=true&deleted=b&use%5B0%5D=true&query_field='.urlencode($f['value']).'&field_bezug='.urlencode($f['mysql_column_name']).'&show=true';
					$field.=' <A HREF="'.$link.'"><FONT color="red">'.$note.'</FONT></A>';
				}		
			}
			
			if ($f['admin_only']=='y') $tout[$f['mysql_column_name']]=$field; else $out[$f['mysql_column_name']]=$field;
			if(in_array($f['mysql_column_name'],$errors)) { 
				$out['error_'.$f['mysql_column_name']]=' bgcolor="'.$color['missing_field'].'"'; 
				$tout['error_'.$f['mysql_column_name']]=' bgcolor="'.$color['missing_field'].'"'; 
			} else { 
				$out['error_'.$f['mysql_column_name']]=''; 
				$tout['error_'.$f['mysql_column_name']]=''; 
			}
			
			if($f['mysql_column_name'] == "begin_of_studies" && (empty($f["hide_for_participant_if_empty"]) || $f["hide_for_participant_if_empty"]=="n")) $hide_begin_of_studies=false;
		}
	}
	
	$tmplformname=$compulsory_fileds_only?'participant_form_noopt':'participant_form';
	if(in_array("own_template",$suptypes)) {
		foreach($suptypes as $stel) {
			$astel=explode(":",$stel);
			if(($astel[0]=="template" || $astel[0]=="tpl") && count($astel)>1) $tmplformname=substr($stel,strlen($astel[0])+1);
		}
	}
	$out['hide_begin_of_studies']=''; $tout['hide_begin_of_studies']=''; 
 	if(in_array("construction",$suptypes) && !$compulsory_fileds_only) {
		// $tmplformname='participant_form_nobeginstudies';
		if($hide_begin_of_studies) $out['hide_begin_of_studies']="style='display:none'";
	}
	if($hide_begin_of_studies && empty($edit["begin_of_studies"]) && !$admin) $out['hide_begin_of_studies']="style='display:none'";
	$formoutput=load_form_template($tmplformname,$out);
	
	$stack_by_css=true;
	
	if($stack_by_css) {
		//<link rel='stylesheet' id='bootstrap-css'  href='https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css' type='text/css' />
		//<link rel='stylesheet' id='bootstrap-css'  href='../style/".$settings['style']."/stacktable.css' />
		echo "
		
		<div class='container'>
		";
	}

	if(!in_array("notable",$suptypes)) echo '<table cellspacing=0 class="table table-responsive-stack" cellpadding="10em" border=0 width="90%">';
	echo $formoutput;

	if ($admin) {
		if (isset($edit['participant_id'])) $tout['participant_id']=$edit['participant_id']; else $tout['participant_id']='';
		if (isset($edit['participant_id_crypt'])) $tout['participant_id_crypt']=$edit['participant_id_crypt']; else $tout['participant_id_crypt']='';
		if (isset($edit['creation_time'])) $tout['creation_time']=lang("creation_time").": <strong>".time__format($lang['lang'],'',false,false,true,false,$edit['creation_time'])."</strong>";  else $tout['creation_time']='';
		$tout['creation_time'].=(empty($tout['creation_time'])?"":", ");
		$tout['creation_time'].=lang("last_modification").": ";
		$tout['creation_time'].='<em>'.(empty($edit['last_update_time'])?lang('never'):time__format($lang['lang'],'',false,false,true,false,$edit['last_update_time'])).'</em>';
		if(!empty($edit['last_update_fileds'])) $tout['creation_time'].='<BR>'.lang('last_update_fileds').': <em>'.$edit['last_update_fileds'].'</em>';
		$tout['creation_time'].='<br>'.'<em style="color:blue">'.lang("last_visit").": ";
		$tout['creation_time'].=(empty($edit['last_visit_time'])?lang('unknown',false):time__format($lang['lang'],'',false,false,true,false,$edit['last_visit_time'])).'</em>';
		if(!empty($edit['last_visit_page'])) $tout['creation_time'].='. <em style="color:blue">'.lang('last_visit_page').': '.$edit['last_visit_page'].'</em>';
		query_makecolumns(table($maintable),array("last_invitation_time","last_invitation_exp"),array("int(20)","int(20)"),array("0","0"));
		query_makecolumns(table('participants_temp'),array("last_invitation_time","last_invitation_exp"),array("int(20)","int(20)"),array("0","0"));
		$tout['creation_time'].='<br>'.'<em style="color:green">'.lang("last_invitation").": ";
		$tout['creation_time'].=(empty($edit['last_invitation_time'])?lang('unknown',false):time__format($lang['lang'],'',false,false,true,false,$edit['last_invitation_time'])).'</em>';
		if(!empty($edit['last_invitation_exp'])) $tout['creation_time'].='. <em style="color:green">'.lang('last_invited_to').': <a href="experiment_edit.php?experiment_id='.$edit['last_invitation_exp'].'" style="color:green">'.experiment__get_public_name($edit['last_invitation_exp']).'</a></em>';
		if(!empty($edit['deleted']) && $edit['deleted']=='y') {
			if($tout['creation_time']!="") $tout['creation_time'].=". ";
			$tout['creation_time'].=lang("Unsubscribed");
			if(empty($edit['last_delete_time'])) {
				$leep=($settings['support_mail']=="leep@univ-paris1.fr");
				$supp_mail_arr=explode('@',$settings['support_mail']);
				$s2ch=(count($supp_mail_arr)>1 && $supp_mail_arr[1]=="s2ch.fr");
				if($leep || $s2ch) $tout['creation_time'].=" ".lang("before",false)." ".lang("november",false)." 2019";
				else $tout['creation_time'].=" ".lang("at_unknown_past_date",false);
			}
			else {
				$tout['creation_time'].=" ".lang("on",false)." ".time__format($lang['lang'],'',false,false,true,false,$edit['last_delete_time'])." ".lang("by",false)." ";
				if(empty($edit['last_delete_admin']) || $edit['last_delete_admin']=='-') $tout['creation_time'].=lang("participant",false);
				else $tout['creation_time'].=$edit['last_delete_admin'];
				$tout['creation_time'].=". ";
				if(empty($edit['can_undelete'])) $tout['creation_time'].=lang("can_not_resubscribe");
				else $tout['creation_time'].=lang("can_resubscribe");
			}
			$tout['creation_time'].=". ";
		}
		if (!isset($edit['rules_signed'])) $edit['rules_signed']='';

		$tout['subpool_id']=subpools__select_field("subpool_id","subpool_id","subpool_name",$edit['subpool_id'],"");
		$tout['rules_signed']=participant__rules_signed_form_field($edit['rules_signed']);
		
		if (!$edit['participant_id']) {
			$tout['is_part_create_form']=true;
			$tout['checkbox_add_to_session']=participant__add_to_session_checkbox();
			$tout['select_add_to_session']=participant__add_to_session_select($edit['participant_id']);
		} else $tout['is_part_create_form']=false;
		
		$adminformoutput=load_form_template('participant_form_admin_addons',$tout);
		echo $adminformoutput;
		$f=array();
		$f['type']='textline'; $f['mysql_column_name']='twinto'; 
		$f['value']=isset($edit['twinto'])?$edit['twinto']:''; $f['size']=40; $f['maxlength']=250;
		$suptwininfo='';
		if(!empty($edit['twinto'])) {
			$asupinfo=array();
			$atwins=explode(",",$edit['twinto']);
			foreach($atwins as $at) {
				$participant_info=orsee_db_load_array($maintable,trim($at),"participant_id");
				if($participant_info!==false) {
					$asupinfo[]="<a target=_blank href='participants_edit.php?participant_id=$at'>".$participant_info["fname"]." ".$participant_info["lname"]."</a> [".$participant_info["email"]."]";
				}
			}
			$suptwininfo="(".implode(", ",$asupinfo).")";
		}
		if(in_array("notable",$suptypes)) echo "<table>";
		echo '<tr><td>'.lang('twin_to <br> <small>( enter_ids , <br> separate_by_comma <br> if_more_than_one )</small>').'</td>
			<td>'.form__render_field($f).'<br>'.$suptwininfo.'</td></tr>';
		if(in_array("notable",$suptypes)) echo "</table>";
		
	}

	if (!$button_title) $button_title=$lang['change'];
	
	$sbdisabled=""; $constructionphrase="";
	if(in_array("construction",$suptypes)) {
		$constructionphrase="<h1><em>".lang('form_in_contruction')."...</em></h1>";
		// var_dump($_REQUEST, $_GET);
		if(empty($_GET["force"])) {
			$button_title=""; $sbdisabled=" disabled";
		}
	}
	
	if(in_array("own_buttonphrase",$suptypes)) {
		foreach($suptypes as $stel) {
			$astel=explode(":",$stel);
			if($astel[0]=="phrase" && count($astel)>1) $constructionphrase=substr($stel,strlen($astel[0])+1);
		}
	}
	
	$submit_button='<INPUT name="add" type="submit"'.$sbdisabled.' value="'.$button_title.'">';
	if(in_array("own_button",$suptypes) || in_array("own_buttons",$suptypes)) {
		foreach($suptypes as $stel) {
			$astel=explode(":",$stel);
			if(($astel[0]=="button" || $astel[0]=="buttons") && count($astel)>1) $submit_button=substr($stel,strlen($astel[0])+1);
		}
	}

	if(!in_array("notable",$suptypes)) { if($closeform) echo '<tr>
			<td colspan=2 align="center"> '.$constructionphrase.' '.$submit_button.'</td>
                </tr>';
	echo '</table>';
	}
	if($several_values_for_each_field) echo '<input type="hidden" name=several value="1" />';
	if($stack_by_css) echo '</div>';
    if($closeform) echo '</form>';
}
 

function participant__load_result_table_fields($type='search',$tlang='',$paymode=0) { // type can be search, assign, session, email
	global $lang;
	$include_delete=false;
	if($type=='searchb') {$type='search'; $include_delete=true;}
	if($type=='assignd') {$type='assign'; $include_delete=true;}
	if($type=='drop') {$include_delete=true;}
	if (!$tlang) $tlang=$lang['lang'];
	$formfields=participantform__load(); $result_columns=array();
	// var_dump($include_delete);
	// var_dump($formfields); exit;
	$excludelist=array();
	if($paymode>0)
	{
		$excludelist=explode(",","phone_number,begin_of_studies,field_of_studies,profession,gender");
		if($paymode==1) $excludelist[]="email";
	}
	// var_dump(check_allow('experiment_show_access_personal_data','',true));
	if(!check_allow('experiment_show_access_personal_data','',true)) {
		$excludelist=array_merge($excludelist,explode(",","fname,lname,email,phone_number,postal_code,birth_year"));
	}
	if(!empty($excludelist)) {
		foreach ($formfields as &$ff) if(in_array($ff['mysql_column_name'],$excludelist)) {
			$ff['searchresult_list_in_participant_results']='n';
			$ff['list_in_session_participants_list']='n';
			$ff['list_in_session_pdf_list']='n';
			// var_dump($ff['mysql_column_name'],$ff['list_in_session_participants_list']); echo '<br>';
		}
		// echo '<hr>'; var_dump($formfields[2]);
	}
	if($include_delete) $formfields[]=array('mysql_column_name'=>'deleted','name_lang'=>'deleted', 'search_result_allow_sort'=>'y');
	// echo count($formfields)."|"; print_r($formfields); exit;
	$mysql_column_names=[];
	// foreach ($formfields as $k=>$f) {echo $k.":".print_r($f,true).". ";}
	// print_r($formfields);
	// for ($k=0; $k<count($formfields); $k++) {$ff=$formfields[$k]; echo $k.":".print_r($ff,true).". ";}
	foreach ($formfields as $f) {
		// var_dump($f);
		if(in_array($f['mysql_column_name'],$mysql_column_names)) echo $f['mysql_column_name']."!";
		$mysql_column_names[]=$f['mysql_column_name'];
		if(  ($type=='search' && !empty($f['searchresult_list_in_participant_results']) && $f['searchresult_list_in_participant_results']=='y') ||
			 ($include_delete && $f['mysql_column_name']=='deleted') ||
			 ($type=='assign' && $f['searchresult_list_in_experiment_assign_results']=='y') ||
			 ($type=='session' && $f['list_in_session_participants_list']=='y') ||
			 ($type=='sessionpdf' && $f['list_in_session_pdf_list']=='y') ||
			 ($type=='email' && $f['admin_only']!='y')  ) {
			 if(  ($type=='search' && $f['search_result_allow_sort']=='y') ||
			 	  ($type=='assign' && $f['search_result_allow_sort']=='y') ||
			 	  (($type=='session' || $type=='sessionpdf') && $f['allow_sort_in_session_participants_list']=='y')  ) {
			 	$f['allow_sort']=true;
			 	if (!empty($f['search_result_sort_order'])) $f['sort_order']=$f['search_result_sort_order'];
			 	else $f['sort_order']=$f['mysql_column_name'];	
			 } else {
			 	$f['allow_sort']=false;
			 	$f['sort_order']='';
			 }
			if(isset($lang[$f['name_lang']])) $f['column_name']=$lang[$f['name_lang']]; else $f['column_name']=$f['name_lang'];
			if(isset($f['type']) && preg_match("/(radioline|select_list)/",$f['type'])) {
				$optionvalues=explode(",",$f['option_values']);
				$optionnames=explode(",",$f['option_values_lang']);
				$f['lang']=array();
				foreach($optionvalues as $k=>$v) {
					if (isset($optionnames[$k])) {
						if ($tlang!=$lang['lang']) {
							$oname=language__get_item('lang',$optionnames[$k],$tlang);
							if ($oname) $f['lang'][$v]=$oname;
							else $f['lang'][$v]=$optionnames[$k];
						} else {
							if(isset($lang[$optionnames[$k]])) $f['lang'][$v]=$lang[$optionnames[$k]];
							else $f['lang'][$v]=$optionnames[$k];
						}
					}
				}
			
			} elseif (isset($f['type']) && $f['type']=='select_lang') {
				$f['lang']=lang__load_lang_cat($f['mysql_column_name'],$tlang);
			}
			
			$result_columns[]=$f;
		}
	}
	// var_dump($result_columns);
	return $result_columns;
}


?>

<?php
// part of orsee. see orsee.org

function expregister__list_invited_for_format($varray) {
	static $saved_experiment_id, $see_calendar_shown, $reservation_link_shown;
	global $color, $lang, $authdata;

	$done=extract($varray);
	
	$sess_remark_list=sessions__get_remark_list($session_id);
	
	if(!empty(preg_grep("/^hidden(:[1y])?\s*$/",$sess_remark_list)) || in_array('!hidden!',$sess_remark_list)) return array($laboratory_id,2);
	if(sessions__hide_conditionally($session_id,$participant_id)) return array($laboratory_id,2);
	$err_code=1;
	$session_unixtime=sessions__get_session_time($varray); 
        $registration_unixtime=sessions__get_registration_end($varray);
        $session_full=sessions__session_full("",$varray);
		$unfinished_reservation=false;
		$unfinished_reservation_my_session=false;
		$reservation_minutes=sessions__get_remark_list_elem_by_name($session_id,"reservation_minutes");
		if(!empty($reservation_minutes) && is_numeric($reservation_minutes) && $reservation_minutes>0) {
			$crf="SELECT id FROM ".table('participants_log')." WHERE action='booking' AND target LIKE '".mysqli_real_escape_string($GLOBALS['mysqli'],"experiment_id:".$experiment_id."\n%")."' ORDER BY log_id";
			$pidscrf=orsee_query($crf,"return_first_elem");
			$mycrfindex=-1;
			if($pidscrf!==false) foreach(array_values(array_unique($pidscrf)) as $pk=>$cpid) if($cpid==$participant_id) $mycrfindex=$pk;
			if($mycrfindex>=0) $unfinished_reservation=true;
			$crfcs="SELECT count(*) FROM ".table('participants_log')." WHERE action='booking' AND id='".$participant_id."' AND target='".mysqli_real_escape_string($GLOBALS['mysqli'],"experiment_id:".$experiment_id."\nsession_id:".$session_id)."'";
			$cscrf=orsee_query($crfcs,"return_first_elem");
			// var_dump($crfcs,$cscrf);
			if($cscrf!==false && $cscrf[0]>0) $unfinished_reservation_my_session=true;
			// else $unfinished_reservation_my_session=false;
		}
	$now=time();

	$register_after_start_val=sessions__get_remark_list_elem_by_name($session_id,"register_after_start");
	$register_after_start=!empty($register_after_start_val) && $register_after_start_val!='n';
	if($register_after_start) { $sessions_endtime=sessions__get_session_end_time(array(),$session_id); if(time()>=$sessions_endtime) $register_after_start=false; }
	if( $now < $session_unixtime || $register_after_start) {

		global $settings;
		$public_lang=$settings['public_standard_language'];
		if ($public_lang=="fr" && empty($see_calendar_shown) && ($registration_unixtime >= $now) && !empty($_REQUEST['p'])) { // && (!$session_full)
			echo '<TR><TD>&nbsp;&nbsp;&nbsp;</TD>
				<TD style="text-align:center" colspan=2 bgcolor="'.$color['list_title_background'].'">
					<a class="alert-link" href="show_calendar.php?p='.$_REQUEST['p'].'">'.mb_strtoupper(lang('see_experiments_in_the_calendar')).'</a>
				</TD></TR>'; //<div class="alert alert-info"></div>
								
				$see_calendar_shown=1;
		}
		if ($experiment_id != $saved_experiment_id) {
			$saved_experiment_id=$experiment_id;
			echo '<TR><TD>&nbsp;&nbsp;&nbsp;</TD>
				<TD colspan=2 "><em><strong style="text-shadow:0px 0px 4px '.$color['list_title_background'].';">
					'.$experiment_public_name.' 
				</strong></em>'; //bgcolor="'.$color['list_title_background'].'
				// $suppageoption=$_SESSION['suppageoption'];
				// var_dump($_SESSION);
				$deuxjours=false;
				$experimentpublicname=experiment__get_public_name($experiment_id);
				$expepublicarr=explode("(",$experimentpublicname);
				$expepublicarr2=explode("_",trim($expepublicarr[0]));
				$a_expepublicarr1=(count($expepublicarr)>1)?preg_split("/[\s,]+/",trim($expepublicarr[1])):array();
				if(count($expepublicarr)>1 && count($a_expepublicarr1)==3 && $a_expepublicarr1[0]=="sur" && $a_expepublicarr1[2]=="jours)" && is_numeric($a_expepublicarr1[1])) {$deuxjours=true; }
				$ndays_arr=preg_grep("/^\d{1,2}days$/",$sess_remark_list);
				if($ndays_arr!==false && count($ndays_arr)>0) {$deuxjours=true;  }
				if($deuxjours) echo '<em  style="text-shadow:0px 0px 4px '.$color['list_title_background'].';">, '.lang('dates_and_hours_of_the_first_session_below', false). '</em>';
				echo '</TD></TR>';
		}
	
		$suptdstyle=""; if(in_array("!less_important!",$sess_remark_list)) $suptdstyle=' style="color:#888"';
		echo '<TR bgcolor="'.$color['list_shade1'].'"><TD></TD><TD'.$suptdstyle.'><strong>';

			echo session__build_name($varray)."</strong>, "; 
			echo laboratories__get_laboratory_name($laboratory_id).",  ".$lang['registration_until']." ";
			echo time__format($authdata['language'],"",false,false,true,false,$registration_unixtime);
		echo '</TD>
			<TD>';
		$canceled=!empty(preg_grep("/^[!]?cancel+ed[!]?(:[1y])?\s*$/",$sess_remark_list));
		if ((!$session_full) && ($registration_unixtime >= $now) && (!$unfinished_reservation) && (!$canceled)) {  // || $register_after_start - already in sessions__get_registration_end
			echo '
				<FORM action="participant_show.php">
				<INPUT type=hidden name="p" value="'.url_cr_encode($participant_id).'">
				<INPUT type=hidden name="s" value="'.unix_crypt($session_id).'">
				<INPUT type=submit class="small" name="register" value="'.$lang['register'].'">
				</FORM>';
			if(experiment__check_option($experiment_id,'by_couples')) {
				include_once("bycouples.php");
				$bchelper = new ByCouplesHelper();
				// var_dump($session_id,$varray,$bchelper->countCouples(2));
				if(!$bchelper->countCouples(2,$participant_id,$session_id,$experiment_id)) echo '<FONT color="'.$color['session_public_expired'].'">('.lang('partners_only',false).')</FONT>';
			}
		} 
		elseif($canceled) {
			echo '<FONT color="'.$color['session_public_complete'].'">'.lang("session_cancelled").'</FONT>';
		}
		elseif ($registration_unixtime < $now ) { // && !$register_after_start - already in sessions__get_registration_end
			echo '<FONT color="'.$color['session_public_expired'].'">'.$lang['expired'].'</FONT>';
		}
		elseif($session_full) {
			echo '<FONT color="'.$color['session_public_complete'].'">'.$lang['complete'].'</FONT>';
		}
		else {
			echo '<FONT color="'.$color['session_public_expired'].'">'.lang('questionnaire_not_finished');
			if($unfinished_reservation_my_session) echo " (".lang('selected_session',false).")";
			echo '</FONT>';
		}
		echo '</TD>
			</TR>';
		if($unfinished_reservation && empty($reservation_link_shown)) {
			$reservation_link=sessions__get_remark_list_elem_by_name($session_id,"reservation_link");
			if(!empty($reservation_link) && $unfinished_reservation_my_session) {
				$participate_at_array=rasee_db_load_array("participate_at",array("participant_id"=>$participant_id,"experiment_id"=>$experiment_id));
				$participant_combined=rasee_db_load_array("participants",array("participant_id"=>$participant_id))+$participate_at_array;
				//var_dump($reservation_link);
				foreach($participant_combined as $pck=>$pcv) {
					// var_dump($reservation_link,$pck,$pcv);
					if(is_null($pcv)) $pcv="";
					$reservation_link=str_ireplace("#".$pck."#",$pcv,$reservation_link);
				}
				echo '<TR><TD colspan=3 bgcolor="#ffcfa0" style="text-align:center"><br>';
				echo lang('finish_the_questionnaire_with_the_followin_link')." :<br>";
				echo "<a href='$reservation_link'>$reservation_link</a>";
				echo '</TD></TR>';
				$reservation_link_shown=1;
			}
		}
		echo '
			<TR><TD colpan=3>&nbsp;</TD></TR>';
			$err_code=0;
	}
	return array($laboratory_id,$err_code);
}

function expregister__list_session_register_buttons($varray){
	static $saved_experiment_id;
	global $color, $lang, $authdata;

	$done=extract($varray);

	$session_unixtime=sessions__get_session_time($varray);
        $registration_unixtime=sessions__get_registration_end($varray);
        $session_full=sessions__session_full("",$varray);
	$now=time();
	$register_after_start_val=sessions__get_remark_list_elem_by_name($session_id,"register_after_start");
	$register_after_start=!empty($register_after_start_val) && $register_after_start_val!='n';
	if($register_after_start) { $sessions_endtime=sessions__get_session_end_time(array(),$session_id); if(time()>=$sessions_endtime) $register_after_start=false; }

	$res='';
	if( $now < $session_unixtime || $register_after_start) {

        	if ($experiment_id != $saved_experiment_id) {
        		$saved_experiment_id=$experiment_id;
        		
       			} 
	if ((!$session_full) && ($registration_unixtime >= $now)) {  /*|| $register_after_start - already in sessions__get_registration_end */
		$res= '
			<FORM action="participant_show.php">
			<INPUT type=hidden name="p" value="'.url_cr_encode($participant_id).'">
			<INPUT type=hidden name="s" value="'.unix_crypt($session_id).'">
			<INPUT type=submit class="small" name="register" value="'.$lang['register'].'">
			</FORM>';
		} 
		elseif ($registration_unixtime < $now) {
			
		}
		else {
			
		}

	}
	return array($session_id,$res);
}


function expregister__list_invited_for($participant_id,$back_function="expregister__list_invited_for_format",$only_shown=false) {
	$query="SELECT *
      		FROM ".table('participate_at').", ".table('experiments').", ".table('sessions')." 
        	WHERE ".table('experiments').".experiment_id=".table('sessions').".experiment_id
		AND ".table('experiments').".experiment_id=".table('participate_at').".experiment_id
		AND ".table('participate_at').".participant_id = '".$participant_id."'
		AND ".table('sessions').".session_finished = 'n'
		AND ".table('participate_at').".registered = 'n'
		AND ".table('experiments').".experiment_type='laboratory'
      		ORDER BY ".table('experiments').".experiment_id, session_start_year, session_start_month, session_start_day,
                 session_start_hour, session_start_minute";
	$labs0=orsee_query($query,$back_function);
	$labs=$labs0;
	if($back_function=="expregister__list_invited_for_format" && $labs!==false) {
		if($only_shown) $labs0=array_filter($labs0, function($val) {return (!is_array($val) || count($val)<2 || $val[1]==0);});
		$labs=array_map(function($val) {return $val[0];}, $labs0);
	}
	$arr_unique_labs=array();
	// var_dump($GLOBALS);
	// var_dump($labs);
	if($labs!==false) $arr_unique_labs=($back_function=="expregister__list_invited_for_format")?array_unique($labs):$labs;
	// var_dump($arr_unique_labs); exit;
	// $unique_labs=($back_function=="expregister__list_invited_for_format")?$arr_unique_labs:$labs;
	// return $unique_labs;
	return $arr_unique_labs;
}


function expregister__list_registered_for_format($varray,$reg_session_id="",$type="registered") {
        static $saved_experiment_id, $first_line=false, $first_hline=false, $shade=false;
        global $color, $lang, $authdata;

	if ($type=="print") $print=true; else $print=false;
	if ($type=="history") $history=true; else $history=false;

        $done=extract($varray);

        $session_unixtime=sessions__get_session_time($varray);
        $session_unregister_end_time=sessions__get_unregister_end($varray);
        $now=time();
		$register_after_start_val=sessions__get_remark_list_elem_by_name($session_id,"register_after_start");
		$register_after_start=!empty($register_after_start_val) && $register_after_start_val!='n';
		if($register_after_start) { $sessions_endtime=sessions__get_session_end_time(array(),$session_id); if(time()>=$sessions_endtime) $register_after_start=false; }

        if ((!$history && ($now < $session_unixtime || $register_after_start)) || ($history && $now >= $session_unixtime && !$register_after_start)) {

                if ((!$history && !$first_line) || ($history && !$first_hline)) {
                	echo '<TR';
			if (!$print) echo ' bgcolor="'.$color['list_title_background'].'"';
			echo '><th>
                        	'.$lang['experiment'].'
                    		</th>
                    		<th>
                        	'.$lang['date_and_time'].'
                    		</th>
                    		<th>
                        	'.$lang['location'].'
                    		</th>';
			if ($history) {
				echo '<th>'.$lang['showup?'].'</th>';
				$first_hline=true;
				}
			elseif (!$print && $session_unregister_end_time>$now) {
				echo '<th>'.'&nbsp;'.'</th>';
				$first_hline=true;
			}
			else $first_line=true;
	             	echo '</TR>';
                	}
		$sess_remark_list=sessions__get_remark_list($session_id);
		$canceled=!empty(preg_grep("/^[!]?cancel+ed[!]?(:[1y])?\s*$/",$sess_remark_list));
		$supstle="";
		if($canceled)  $supstle=" text-decoration-line: line-through; ";

		echo '<TR';
			if ($shade) $shade=false; else $shade=true;
			if ($session_id==$reg_session_id) 
				echo ' bgcolor="'.$color['just_registered_session_background'].'"';
			elseif ($print && $shade) 
				echo ' bgcolor="lightgrey"';
			elseif ($shade) 
				echo ' bgcolor="'.$color['list_shade1'].'"';
			else echo ' bgcolor="'.$color['list_shade2'].'"';

		echo ' style="vertical-align:middle" ><TD';
		if(!$print && !$history) echo ' style="text-shadow:0px 0px 1px; '.$supstle.'"';
		echo '>
                	'.$experiment_public_name.'
			</TD>
        		<TD';
		if(!$print && !$history) echo ' style="text-shadow:0px 0px 1px; '.$supstle.'"';
		echo '>';
		echo session__build_name($varray);
		echo '</TD>
			<TD';
		if(!$print && !$history) echo ' style="text-shadow:0px 0px 1px; '.$supstle.'"';
		echo '>';
		echo laboratories__get_laboratory_name($laboratory_id);
		if(!empty(experiment__get_value($experiment_id,'confirm_message_supinfo'))) echo "<hr>".experiment__get_value($experiment_id,'confirm_message_supinfo');
		echo '</TD>';
		if ($history) {
			echo '<TD>';
                	if ($session_finished=="y") {
				if ($shownup=="n") {
					$tcolor=$color['shownup_no'];
					$ttext=$lang['no'];
					}
				elseif ($shownup=="y") {
                                        $tcolor=$color['shownup_yes'];
                                        $ttext=$lang['yes'];
					}
				echo '<FONT color="'.$tcolor.'">'.$ttext.'</FONT>';
				}
			   else echo $lang['three_questionmarks'];
			echo '</TD>';
		}
		elseif (!$print && $session_unregister_end_time>$now && !$canceled) {
			echo '<TD>';
			echo '
			<FORM action="participant_desinscription.php">
			<INPUT type=hidden name="p" value="'.url_cr_encode($participant_id).'">
			<INPUT type=hidden name="s" value="'.unix_crypt($session_id).'">
			<INPUT type=submit class="small" name="unregister" value="'.lang('unregister').'">
			</FORM>';
			echo '</TD>';
		}
		
		elseif ($canceled) {
			echo '<TD style="color:red">';
			echo lang("session_cancelled");
			echo '</TD>';
		}
		
		echo '</TR>';

		if (!$print) {
			echo '<TR><TD colspan=';
			if ($history) echo "4"; else echo "3";
			echo '>&nbsp;</TD></TR>';
			}

		return $laboratory_id;
		}
}



function expregister__list_registered_for($participant_id,$reg_session_id="",$type="registered",$only_shown=false) {
	if($type!="exp_sess_only") {
		echo '<TR><TD colspan=3>
			<TABLE class="table" width="100%" border=';
		if ($type=="print") echo "1"; else echo "0";
		echo '>';
	}
	$query="SELECT *
      		FROM ".table('experiments').", ".table('sessions').", ".table('participate_at')."
        	WHERE ".table('experiments').".experiment_id=".table('sessions').".experiment_id
		AND ".table('experiments').".experiment_id=".table('participate_at').".experiment_id
		AND ".table('participate_at').".participant_id = '".$participant_id."'
		AND ".table('sessions').".session_id = ".table('participate_at').".session_id
		AND ".table('sessions').".session_finished = 'n'
		AND ".table('participate_at').".registered = 'y' 
		AND ".table('experiments').".experiment_type='laboratory' 
      		ORDER BY session_start_year, session_start_month, session_start_day,
                 	session_start_hour, session_start_minute";

        $result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
		$labs=array();
                while ($line = mysqli_fetch_assoc($result)) {
						$arr_exp_sess_only=array($line['experiment_id'],$line['session_id']);
						
                        $labs[]=($type!="exp_sess_only")?expregister__list_registered_for_format($line,$reg_session_id,$type):$arr_exp_sess_only;
						if($only_shown && $type=="exp_sess_only") {
							$session_unixtime=sessions__get_session_time(array(),$line['session_id']);
							$register_after_start_val=sessions__get_remark_list_elem_by_name($line['session_id'],"register_after_start");
							$register_after_start=!empty($register_after_start_val) && $register_after_start_val!='n';
							if($register_after_start) { $sessions_endtime=sessions__get_session_end_time(array(),$line['session_id']); if(time()>=$sessions_endtime) $register_after_start=false; }
							// echo "\n <!-- "; echo "session_unixtime: "; echo $session_unixtime; echo ", time:"; echo time(); echo " --> ";
							if(time() > $session_unixtime && !$register_after_start) array_pop($labs);
							
						}
                }
        mysqli_free_result($result);

	if($type!="exp_sess_only")
		echo '</TABLE>
			</TD></TR>';
	$unique_labs=($type!="exp_sess_only")?array_unique($labs):$labs;
	return $unique_labs;
}



function expregister__list_history($participant_id,$type="") {
	if($type!="exp_sess_only") {
		echo '<TR><TD colspan=3>
			<TABLE width=100% border=0>';
	}

     	$query="SELECT *
      		FROM ".table('experiments').", ".table('sessions').", ".table('participate_at')."
        	WHERE ".table('experiments').".experiment_id=".table('sessions').".experiment_id
        	AND ".table('experiments').".experiment_id=".table('participate_at').".experiment_id
        	AND ".table('participate_at').".participant_id = '".$participant_id."'
        	AND ".table('sessions').".session_id = ".table('participate_at').".session_id
        	AND ".table('participate_at').".registered = 'y'
		AND ".table('experiments').".experiment_type='laboratory' 
      		ORDER BY session_start_year DESC, session_start_month DESC, session_start_day DESC,
                 	session_start_hour DESC, session_start_minute DESC";
        $result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
		$labs=array();
                while ($line = mysqli_fetch_assoc($result)) {
                        $labs[]=($type!="exp_sess_only")?expregister__list_registered_for_format($line,"","history"):array($line['experiment_id'],$line['session_id'],$line['session_finished']);
                        }
        mysqli_free_result($result);

	if($type!="exp_sess_only") {
        echo '</TABLE>
                </TD></TR>';
	}
	else return $labs;
}


function expregister__check_registered($participant_id,$experiment_id) {
	$query="SELECT registered
      		FROM ".table('participate_at')."
      		WHERE experiment_id='".$experiment_id."'
		AND participant_id='".$participant_id."'
		AND payghost=0";
	$res=orsee_query($query);
	if (isset($res['registered']) && $res['registered']=="y") $reg=true; else $reg=false;
	return $reg;
}

function expregister__check_registered_session($participant_id,$session_id) {
	$session=orsee_db_load_array("sessions",$session_id,"session_id");
	$query="SELECT registered
      		FROM ".table('participate_at')."
      		WHERE experiment_id='".$session['experiment_id']."'
			AND session_id='".$session_id."'
			AND participant_id='".$participant_id."'";
	$res=orsee_query($query);
	if (isset($res['registered']) && $res['registered']=="y") $reg=true; else $reg=false;
	return $reg;
}



function expregister__register($participant_id,$session_id) {
	$experiment_id=sessions__get_experiment_id($session_id);
     	$query="UPDATE ".table('participate_at')."
      		SET registered='y', session_id='".$session_id."' 
      		WHERE experiment_id='".$experiment_id."'
        	AND participant_id='".$participant_id."' AND payghost=0";
	$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

	experimentmail__experiment_registration_mail($participant_id,$session_id,$experiment_id);

}

function expregister__unregister($participant_id,$session_id) {
	$experiment_id=sessions__get_experiment_id($session_id);
     	$query="UPDATE ".table('participate_at')."
      		SET registered='n', session_id='0' 
      		WHERE experiment_id='".$experiment_id."'
        	AND participant_id='".$participant_id."' AND payghost=0";
	$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

}



?>

<?php
// part of orsee. see orsee.org

function sessions__format_alist($session) {
	global $lang, $color;
  	extract($session);
	$session_time_array=time__load_session_time($session);
        $session_time=session__build_name($session);

	$reg=experiment__count_participate_at($experiment_id,$session_id);
	if ($reg < $part_needed) {
		$regfontcolor=$color['session_not_enough_participants'];
		}
	  elseif ($reg < $part_needed + $part_reserve) {
		$regfontcolor=$color['session_not_enough_reserve'];
		}
	   else {
		$regfontcolor=$color['session_complete'];
		}

        echo '	<tr>
        		<td>
				'.$session_time.'
        		</td>
			<td>
			</td>
			<td>
				'.laboratories__get_laboratory_name($laboratory_id).'
			</td>
        		<td>';
				if (check_allow('session_edit')) echo '
        			<A HREF="session_edit.php?session_id='.$session_id.'">
                			'.$lang['edit'].'
                		</A>';
        echo '		</td>
        	</tr>';

	$allow_sp=check_allow('experiment_show_participants');
	$sup_exp_part_show_link="";
	if(experiment__check_option($experiment_id,'by_couples')) {
		include_once("../".$GLOBALS['settings__public_folder']."/bycouples.php");
		$bchelper = new ByCouplesHelper();
		$sup_exp_part_show_link.="&".$bchelper->mailtablefield."=1";
	}
	echo '	<TR>
			<TD>';
				if ($allow_sp) echo '
					<A HREF="experiment_participants_show.php?experiment_id='.
						$experiment_id.$sup_exp_part_show_link.'&focus=registered&session_id='.
						$session_id.'">';
				echo $lang['registered_subjects'];
				if ($allow_sp) echo '</A>';
				echo ': 
				<FONT color="'.$regfontcolor.'">
				'.$reg.' ('.$part_needed.','.$part_reserve.')</FONT> '.
				help("shortcut_counts");
				if(!$allow_sp && check_allow("participants_bulk_mail")) {
					echo '<br><A class="small" HREF="participants_bulk_mail.php?experiment_id='.
						$experiment_id.$sup_exp_part_show_link.'&session_id='.
						$session_id.'">('.lang('send_bulk_mail',false).')</A>';
						
				}
				echo '
			</TD>
			<TD>';
				if ($session_finished!="y") {
					if ($reminder_sent=="y") {
						$state=$lang['session_reminder_state__sent'];
						$statecolor=$color['session_reminder_state_sent_text'];
						if(!$allow_sp && check_allow('session_send_reminder') && time() + 600 < sessions__get_session_end_time($session)) $state.='<br>(<a class="small" href="session_send_reminder.php?session_id='.$session_id.'">'.lang('session_reminder_send_again').'</a>)';
						}
					elseif ($reminder_checked=="y" && $reminder_sent=="n") {
						$state=$lang['session_reminder_state__checked_but_not_sent'];
						$statecolor=$color['session_reminder_state_checked_text'];
						if(!$allow_sp && check_allow('session_send_reminder') && time() + 600 < sessions__get_session_end_time($session)) $state.='<br>(<a class="small" href="session_send_reminder.php?session_id='.$session_id.'">'.lang('session_reminder_send').'</a>)';
						}
					else {
						$state=$lang['session_reminder_state__waiting'];
						$statecolor=$color['session_reminder_state_waiting_text'];
						// var_dump($allow_sp, check_allow('session_send_reminder'));
						if(!$allow_sp && check_allow('session_send_reminder') && time() + 600 < sessions__get_session_end_time($session)) $state.='<br>(<a class="small" href="session_send_reminder.php?session_id='.$session_id.'">'.lang('session_reminder_send_now').'</a>)';
						}
					echo '<FONT color="'.$statecolor.'">'.$lang['session_reminder'].': '.$state.'</FONT>';
					}
	echo '		</TD>
			<TD colspan=2>';
				if ($session_finished=="y")
					echo '<font color="'.$color['session_finished'].'">
						'.$lang['session_finished'].'</font>';
		echo '	</TD>
		</TR>';

        echo '	<TR>
			<TD colspan=3 class=small>
				&nbsp;
			</TD>
		</TR>';
}

function session__check_lab_time_clash($entry) {
        global $lang;

	if (isset($entry['session_start_year'])) {
		$notice=$lang['overlapping_sessions'];
		$thistimearray=time__load_session_time($entry);
                $this_start_time=time__time_package_to_unixtime($thistimearray);
                $this_end_time = $this_start_time +
                                    ( (int) $entry['session_duration_hour'] * 3600 ) +
                                    ( (int) $entry['session_duration_minute'] * 60);
		}
	   else {
		$notice=$lang['overlapping_lab_reservation'];
                $thistimearray=time__get_timepack_from_pack ($entry,"space_start_");
                $this_start_time=time__time_package_to_unixtime($thistimearray);
		$thisendtimearray=time__get_timepack_from_pack ($entry,"space_stop_");
                $this_end_time=time__time_package_to_unixtime($thisendtimearray);
		}

		if (!isset($entry['space_id'])) $entry['space_id']='';
		if (!isset($entry['session_id'])) $entry['session_id']='';

        $query="SELECT unix_timestamp(concat(session_start_year,'-',session_start_month,'-',session_start_day,' ',
					session_start_hour,':',session_start_minute,':0')) as start_time, 
			unix_timestamp(concat(session_start_year,'-',session_start_month,'-',session_start_day,' ',
                                        session_start_hour,':',session_start_minute,':0')) +
					session_duration_hour*3600 + session_duration_minute*60 as stop_time,
			 ".table('experiments').".*, ".table('sessions').".*
                FROM ".table('experiments').", ".table('sessions')."
                WHERE ".table('experiments').".experiment_id=".table('sessions').".experiment_id
                AND ".table('experiments').".experiment_type!='internet'
                AND session_id!='".$entry['session_id']."'
                AND laboratory_id='".$entry['laboratory_id']."' 
		HAVING NOT (start_time >= '".$this_end_time."' OR stop_time <= '".$this_start_time."')
		ORDER BY start_time";
        $result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

        while ($osession=mysqli_fetch_assoc($result)) {
        	message ('<UL><LI>'.
                	$notice.': <A HREF="session_edit.php?session_id='.$osession['session_id'].'">'.
                        experiment__get_title($osession['experiment_id']).' - '.
                        session__build_name($osession).'</A></UL>');
                }

	$query="SELECT unix_timestamp(concat(space_start_year,'-',space_start_month,'-',space_start_day,' ',
                                        space_start_hour,':',space_start_minute,':0')) as start_time,
                        unix_timestamp(concat(space_stop_year,'-',space_stop_month,'-',space_stop_day,' ',
                                        space_stop_hour,':',space_stop_minute,':0')) as stop_time,
                         ".table('lab_space').".*
                FROM ".table('lab_space')." 
                WHERE laboratory_id='".$entry['laboratory_id']."' 
			AND space_id!='".$entry['space_id']."' 
                HAVING NOT (start_time >= '".$this_end_time."' OR stop_time <= '".$this_start_time."')
                ORDER BY start_time";
        $result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

        while ($osession=mysqli_fetch_assoc($result)) {
		$ostart_time=time__get_timepack_from_pack ($osession,"space_start_");
		$ostop_time=time__get_timepack_from_pack ($osession,"space_stop_");
		$ostart_string=time__format($lang['lang'],$ostart_time);
		$ostop_string=time__format($lang['lang'],$ostop_time);
                message ('<UL><LI>'.
                        $notice.': <A HREF="calendar_main.php?time='.$this_start_time.'">'.
                        $osession['reason'].' - '.
                        $ostart_string.' - '.$ostop_string.'</A></UL>');
                }
}



function session__get_status($session,$tlang="",$reg="") {
	global $settings;
	if ($tlang=="") $tlang=$settings['admin_standard_language'];

	if ($reg=="") $reg=experiment__count_participate_at($session['experiment_id'],$session['session_id']);
        if ($reg < $session['part_needed']) {
                $status=load_language_symbol('not_enough_participants',$tlang);
                }
          elseif ($reg < $session['part_needed'] + $session['part_reserve']) {
                $status=load_language_symbol('not_enough_reserve',$tlang);
                }
           else {
                $status=load_language_symbol('complete',$tlang);
                }
	return $status;
}



function session__build_name($pack,$language="",$time_only=false) {
	global $lang, $settings;
	if (!$language) {
        	if (isset($lang['lang'])) $thislang=$lang['lang'];
                        else $thislang=$settings['public_standard_language'];
		}
		else {
		$thislang=$language;
		}

	$session_time=time__load_session_time($pack);
	$duration=time__get_timepack_from_pack($pack,"session_duration_");
        $end_time=time__add_packages($session_time,$duration);
	$session_time_string=time__format($thislang,$session_time,false,false,true,false).'-'.
			time__format($thislang,$end_time,true,false,true,true);
	if($time_only) $session_time_string=time__format($thislang,$session_time,true,false,true,true).'-'.
			time__format($thislang,$end_time,true,false,true,true);
	return $session_time_string;
}


function sessions__get_first_date($experiment_id) {
	global $expadmindata;
     	$query="SELECT *
      		FROM ".table('sessions')."
      		WHERE experiment_id='".$experiment_id."'
      		ORDER BY session_start_year, session_start_month, session_start_day
		LIMIT 1";
	$fsession=orsee_query($query);
	if (isset($fsession['session_start_year'])) {
		$ttime=time__load_session_time($fsession);
		$dstr=time__format($expadmindata['language'],$ttime,false,true,true,false);
		return $dstr;
		}
	   else {
		return "???";
		}
}

function sessions__get_last_date($experiment_id) {
        global $expadmindata;
        $query="SELECT session_start_year, session_start_month, session_start_day
      		FROM ".table('sessions')." 
      		WHERE experiment_id='".$experiment_id."'
      		ORDER BY session_start_year DESC, session_start_month DESC, session_start_day DESC
        	LIMIT 1";
        $fsession=orsee_query($query);
        if (isset($fsession['session_start_year'])) {
                $ttime=time__load_session_time($fsession);
                $dstr=time__format($expadmindata['language'],$ttime,false,true,true,false);
                return $dstr;
                }
           else {
                return "???";
                }
}

function sessions__get_registration_end($alist,$session_id="",$experiment_id="") {
	if ($session_id) {
		$query="SELECT * FROM ".table('sessions')." WHERE session_id='".$session_id."'";
		$alist=orsee_query($query);
		}
 	elseif ($experiment_id) {
		$query="SELECT ".table('sessions').".*,
			session_start_year*100000000 +
            session_start_month*1000000 +
            session_start_day*10000 +
            session_start_hour*100 +
            session_start_minute as time
            FROM ".table('experiments').", ".table('sessions')."
            WHERE ".table('experiments').".experiment_id=".table('sessions').".experiment_id
            AND experiment_type='laboratory'
			AND ".table('experiments').".experiment_id='".$experiment_id."'
			ORDER BY time DESC
			LIMIT 1";
		$alist=orsee_query($query);
		}
	$blist=time__load_session_time($alist);
	$session_unixtime=time__time_package_to_unixtime($blist);
	$diff = ($alist === false)? 0 : ((int) $alist['registration_end_hours']) * 3600;
	$registration_end_unixtime=$session_unixtime-$diff;
	if (!empty($alist['session_id'])) {
		$register_after_start_val=sessions__get_remark_list_elem_by_name($alist['session_id'],"register_after_start");
		$register_after_start=!empty($register_after_start_val) && $register_after_start_val!='n';
		if($register_after_start) {
			$sessions_endtime=sessions__get_session_end_time(array(),$alist['session_id']);
			if($diff==0) $diff=5*60;
			$registration_end_unixtime=$sessions_endtime-$diff;
			// var_dump($session_unixtime,$diff,$registration_end_unixtime);
		}
	}
	// var_dump($session_id, $session_unixtime,$diff,$registration_end_unixtime);
	return $registration_end_unixtime;
}

function sessions__get_reminder_time($alist,$session_id="") {
	if ($session_id) {
		$query="SELECT * FROM ".table('sessions')." WHERE session_id='".$session_id."'";
        	$alist=orsee_query($query);
		}
	$blist=time__load_session_time($alist);
	$session_unixtime=time__time_package_to_unixtime($blist);
	$diff = ((int) $alist['session_reminder_hours']) * 3600;
	$reminder_unixtime=$session_unixtime-$diff;
	return $reminder_unixtime;
}

function sessions__get_unregister_end($alist,$session_id="") {
	if ($session_id) {
        	$alist=orsee_db_load_array("sessions",$session_id,"session_id");
	}
	if(!isset($alist['unregister_hours_before'])) return 0;
	$blist=time__load_session_time($alist);
	$session_unixtime=time__time_package_to_unixtime($blist);
	$diff = ((int) $alist['unregister_hours_before']) * 3600;
	$unregister_unixtime=($diff<0)?0:$session_unixtime-$diff;
	return $unregister_unixtime;
}

function sessions__get_session_time($pack,$session_id="") {
	if ($session_id) 
        	$pack=orsee_db_load_array("sessions",$session_id,"session_id");

	$session_time=time__load_session_time($pack);
	$session_unixtime=time__time_package_to_unixtime($session_time);
	return $session_unixtime;
}

function sessions__get_session_end_time($pack,$session_id="") {
	if ($session_id) 
        	$pack=orsee_db_load_array("sessions",$session_id,"session_id");

	$session_time=time__load_session_time($pack);
	$session_unixtime=time__time_package_to_unixtime($session_time);
	$diff = (((int) $pack['session_duration_hour']) * 3600) + (((int) $pack['session_duration_minute']) * 60) ;
	return $session_unixtime+$diff;
}


function sessions__session_full($session_id,$thissession=array(),$formails=false,$return_nplaces_left=false) {
	// if($session_id=="-") var_dump($thissession);
	if (!isset($thissession['session_id'])) 
	 	$thissession=orsee_db_load_array("sessions",$session_id,"session_id");
	$reg=experiment__count_participate_at($thissession['experiment_id'],$thissession['session_id']);
	if ($reg < $thissession['part_needed'] + $thissession['part_reserve']) $session_full=false; else $session_full=true;
	$nplaces_left = $thissession['part_needed'] + $thissession['part_reserve'] - $reg;
	// var_dump($thissession); exit;
	if(!$session_full && !empty($thissession['participant_id'])) {
		$participant_info=orsee_db_load_array("participants",$thissession['participant_id'],"participant_id");
		if(!empty($participant_info['twinto'])){
			$atwins=explode(",",$participant_info['twinto']);
			$registered_exp=explode(",",query__get_registered_twins($thissession['experiment_id']));
			$registered_sess=explode(",",query__get_registered_twins($thissession['experiment_id'],$thissession['session_id']));
			// var_dump($registered_exp); var_dump($registered_sess); var_dump($atwins); print_r($participant_info); echo "<hr>\r\n"; print_r($thissession); //exit;
			if(in_array($thissession['participant_id'],$registered_exp) && !in_array($thissession['participant_id'],$registered_sess)) {$session_full=true; $nplaces_left=0;}
			/*foreach($atwins as $at) {
				if(in_array(trim($at),$registered_exp) && !in_array(trim($at),$registered_sess)) {
					$session_full=true;
					break;
				}
			}*/
		}
		if(!$session_full && !isset($thissession['gender_equality']) && !empty($thissession['experiment_id'])) $thissession['gender_equality']=experiment__get_value($thissession['experiment_id'],'gender_equality');
		// if($session_id=="-") var_dump($thissession, $session_full);
		if(!$session_full && !empty($thissession['gender_equality']) && $thissession['gender_equality']!="n" ) {
			if($participant_info['gender']=="?") {
				if($return_nplaces_left) return 0;
				return true;
			}
			$reg_gender=experiment__count_participate_at($thissession['experiment_id'],$thissession['session_id'],$participant_info['gender']);
			//var_dump($reg_gender); exit;
			$limit_number=$reg_gender*2;
			$sess_remark_list=sessions__get_remark_list($thissession['session_id']);
			foreach($sess_remark_list as $srem) {
				$asrem=explode(":",$srem);
				if($asrem[0]==$participant_info['gender'] && count($asrem)>1 && is_numeric($asrem[1])) $limit_number=$reg_gender-$asrem[1]+$thissession['part_needed'] + $thissession['part_reserve'];
			}
			// var_dump($limit_number);
			if($limit_number>=($thissession['part_needed'] + $thissession['part_reserve'])) $session_full=true;
			$nplaces_left = $thissession['part_needed'] + $thissession['part_reserve'] - $limit_number;
		}
		
	}
	if(!$session_full) {
		$reservation_minutes=sessions__get_remark_list_elem_by_name($thissession['session_id'],"reservation_minutes");
		if(!empty($reservation_minutes) && is_numeric($reservation_minutes) && $reservation_minutes>0) {
			$plq="SELECT count(*) FROM ".table('participants_log')." WHERE action='booking' AND target='".mysqli_real_escape_string($GLOBALS['mysqli'],"experiment_id:".$thissession['experiment_id']."\nsession_id:".$thissession['session_id'])."'";
			$min_time=time()-($reservation_minutes*60);
			$plq.=" AND timestamp >= $min_time";
			/*$reservation_stop_before_hours=sessions__get_remark_list_elem_by_name($thissession['session_id'],"reservation_stop_before_hours");
			if(!empty($reservation_stop_before_hours) && is_numeric($reservation_stop_before_hours) && $reservation_stop_before_hours>0) {
				$max_time=sessions__get_session_time($thissession)-($reservation_stop_before_hours*3600);
				$plq.=" AND timestamp < $max_time";
			}*/
			if(!empty($thissession['gender_equality']) && $thissession['gender_equality']!="n" ) $plq.=" AND `id` IN (SELECT participant_id FROM ".table("participants")." WHERE gender='".$participant_info['gender']."')";
			$n_bookings=orsee_query($plq,"return_first_elem");
			// var_dump($n_bookings,$plq);
			if($n_bookings!==false && !empty($thissession['gender_equality']) && $thissession['gender_equality']!="n" ) {
				$limit_number=($reg_gender+$n_bookings[0])*2;
				foreach($sess_remark_list as $srem) {
					$asrem=explode(":",$srem);
					if($asrem[0]==$participant_info['gender'] && count($asrem)>1 && is_numeric($asrem[1])) $limit_number=$reg_gender+$n_bookings[0]-$asrem[1]+$thissession['part_needed'] + $thissession['part_reserve'];
				}
				// var_dump($limit_number);
				if($limit_number>=($thissession['part_needed'] + $thissession['part_reserve'])) $session_full=true;
				$nplaces_left = $thissession['part_needed'] + $thissession['part_reserve'] - $limit_number;
			}
			elseif($n_bookings!==false) {
				if($reg+$n_bookings[0]>=$thissession['part_needed'] + $thissession['part_reserve']) $session_full=true;
				$nplaces_left = $thissession['part_needed'] + $thissession['part_reserve'] - ($reg+$n_bookings[0]);
			}
			// var_dump($n_bookings,$reg,$thissession['part_needed'] + $thissession['part_reserve'],$session_full); exit;
		}
	}
	if(!$session_full && !$formails && !empty(sessions__get_remark_list_elem_by_name($thissession['session_id'],"session_not_available"))) {
		if(strtolower(sessions__get_remark_list_elem_by_name($thissession['session_id'],"session_not_available"))!="n") {$session_full=true; $nplaces_left=0;}
	}
	if(!$session_full && $formails && sessions__hide_conditionally($thissession['session_id'],empty($thissession['participant_id'])?0:$thissession['participant_id'])) {$session_full=true; $nplaces_left=0;}
	// $participant_info=orsee_db_load_array("participants",$thissession['participant_id'],"participant_id");
	// if($participant_info['remarks']!=str_replace("over50","",$participant_info['remarks'])) $session_full=true;
	if($return_nplaces_left) return $nplaces_left;
	return $session_full;
}

function sessions__hide_conditionally_two_by_two($session_id,$participant_id=0) {
	$conditionally_hidden_on=sessions__get_remark_list_elem_by_name($session_id,'conditionally_hidden'); //preg_grep("/^conditionally_hidden:\s*(\d+)\s*$/",$sess_remark_list);
	if(!empty($conditionally_hidden_on)) {
		$sess_temp=orsee_db_load_array("sessions",$conditionally_hidden_on,"session_id");
		if(!empty($sess_temp)) {
			$sess_temp["participant_id"]=$participant_id;
			// var_dump($conditionally_hidden_on,sessions__session_full("",$sess_temp),$sess_temp);
			$conditionally_hidden_other=sessions__get_remark_list_elem_by_name($conditionally_hidden_on,'conditionally_hidden');
			$mutually=(!empty($conditionally_hidden_other) && $conditionally_hidden_other==$session_id);
			// var_dump($sess_temp);
			$other_sess_full=sessions__session_full("-",$sess_temp);
			if(!$mutually && !$other_sess_full) return true;
			$my_sess_temp=orsee_db_load_array("sessions",$session_id,"session_id"); $my_sess_temp["participant_id"]=$participant_id;
			$my_sess_full=sessions__session_full("",$my_sess_temp);
			if(!$mutually && $my_sess_full) return true;
			if($mutually) {
				$imprincipal=sessions__check_remark_list_option($session_id,"conditionally_principal");
				$otherprincipal=sessions__check_remark_list_option($conditionally_hidden_on,"conditionally_principal");
				if(!($imprincipal^$otherprincipal)) $imprincipal=($session_id<$conditionally_hidden_on);
				// var_dump($imprincipal,$participant_id,$my_sess_full,$other_sess_full);
				// if(!$imprincipal && !$other_sess_full) return true;
				if(!$my_sess_full && !$other_sess_full) {
					$my_sess_left_places=sessions__session_full("",$my_sess_temp,false,true);
					$other_sess_left_places=sessions__session_full("",$sess_temp,false,true);
					if($my_sess_left_places<$other_sess_left_places || (!$imprincipal && $my_sess_left_places==$other_sess_left_places)) return true;
				}
				if($my_sess_full && !$other_sess_full) return true;
				if(!$imprincipal && $my_sess_full && $other_sess_full) return true;
			}
		}
	}
	return false;
}

function sessions__hide_conditionally($session_id,$participant_id=0) {
	$conditionally_hidden_on_init=sessions__get_remark_list_elem_by_name($session_id,'conditionally_hidden'); //preg_grep("/^conditionally_hidden:\s*(\d+)\s*$/",$sess_remark_list);
	if(!empty($conditionally_hidden_on_init)) {
		$conditionally_hidden_on_arr=preg_split("/\W+/",$conditionally_hidden_on_init);
		$my_sess_temp=orsee_db_load_array("sessions",$session_id,"session_id"); $my_sess_temp["participant_id"]=$participant_id;
		$my_sess_full=sessions__session_full("",$my_sess_temp);
		$imprincipal=sessions__check_remark_list_option($session_id,"conditionally_principal");
		$my_priority=sessions__get_remark_list_elem_by_name($session_id,'conditionally_hidden_priority');
		if(empty($my_priority))$my_priority=$session_id;
		if($imprincipal) $my_priority=1;
		foreach($conditionally_hidden_on_arr as $conditionally_hidden_on)
		{
			$sess_temp=orsee_db_load_array("sessions",$conditionally_hidden_on,"session_id");
			if(!empty($sess_temp)) {
				$sess_temp["participant_id"]=$participant_id;
				// var_dump($conditionally_hidden_on,sessions__session_full("",$sess_temp),$sess_temp);
				$conditionally_hidden_other_init=sessions__get_remark_list_elem_by_name($conditionally_hidden_on,'conditionally_hidden');
				if(!empty($conditionally_hidden_other_init))$conditionally_hidden_other=preg_split("/\W+/",$conditionally_hidden_other_init);
				$mutually=(!empty($conditionally_hidden_other_init) && in_array($session_id,$conditionally_hidden_other));
				// var_dump($sess_temp);
				$other_sess_full=sessions__session_full("-",$sess_temp);
				if(!$mutually && !$other_sess_full) return true;
				if(!$mutually && $my_sess_full) return true;
				if($mutually) {
					$otherprincipal=sessions__check_remark_list_option($conditionally_hidden_on,"conditionally_principal");
					$other_priority=sessions__get_remark_list_elem_by_name($session_id,'conditionally_hidden_priority');
					if(empty($other_priority))$other_priority=$conditionally_hidden_on;
					if($otherprincipal) $other_priority=1;
					$imprincipal=($my_priority<$other_priority);
					if($my_priority==$other_priority) $imprincipal=($session_id<$conditionally_hidden_on);
					// var_dump($imprincipal,$participant_id,$my_sess_full,$other_sess_full);
					if(!$my_sess_full && !$other_sess_full) {
						$my_sess_left_places=sessions__session_full("",$my_sess_temp,false,true);
						$other_sess_left_places=sessions__session_full("",$sess_temp,false,true);
						if($my_sess_left_places<$other_sess_left_places || (!$imprincipal && $my_sess_left_places==$other_sess_left_places)) return true;
					}
					if($my_sess_full && !$other_sess_full) return true;
					if(!$imprincipal && $my_sess_full && $other_sess_full) return true;
				}
			}
		}
	}
	return false;
}

function sessions__get_experiment_id($session_id) {
	$query="SELECT experiment_id
      		FROM ".table('sessions')." 
      		WHERE session_id='".$session_id."'";
	$res=orsee_query($query);
	if (isset($res['experiment_id'])) $experiment_id=$res['experiment_id']; else $experiment_id="";
	return $experiment_id;
}

function sessions__get_remarks($session_id) {
	$query="SELECT session_remarks
      		FROM ".table('sessions')." 
      		WHERE session_id='".$session_id."'";
	$res=orsee_query($query);
	if (isset($res['session_remarks'])) $sess_remarks=$res['session_remarks']; else $sess_remarks="";
	return $sess_remarks;
}

function sessions__get_remark_list($session_id) {
	$sess_remarks_list=str_replace(array("\r\n","\n","\r"),array(",",",",","),sessions__get_remarks($session_id));
	return explode(",",$sess_remarks_list);
}

function sessions__get_remark_list_elem_by_name($session_id,$elemname) {
	$sess_remark_list=sessions__get_remark_list($session_id);
	foreach($sess_remark_list as $srem) {
		$asrem=explode(":",$srem);
		if(trim($asrem[0])==trim($elemname) && count($asrem)>1) return trim(implode(":",array_slice($asrem, 1)));
	}
	return null;	
}

function sessions__check_remark_list_option($session_id,$elemname) {
	$sess_remark_list=sessions__get_remark_list($session_id);
	foreach($sess_remark_list as $srem) {
		$asrem=explode(":",$srem);
		if(trim($asrem[0])==trim($elemname)) {
			if(count($asrem)==1) return true;
			if(count($asrem)>1 && !empty($asrem[1]) && $asrem[1]!='n') return true;
		}
	}
	return false;	
}

?>

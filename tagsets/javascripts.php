<?php
// part of orsee. see orsee.org

function script__part_list_checkall() {

echo '
<SCRIPT LANGUAGE="JavaScript">

<!-- Begin
	function checkAll(chname,chcount) {
                for (var j = 1; j <= chcount; j++) {
                box = eval("document.part_list." + chname + j);
                if (box.checked == false) box.checked = true;
                   }
                }

        function uncheckAll(chname,chcount) {
                for (var j = 1; j <= chcount; j++) {
                box = eval("document.part_list." + chname + j);
                if (box.checked == true) box.checked = false;
                   }
                }

//  End -->
</script>
';

}

function script__part_reg_show() {

echo '
<SCRIPT LANGUAGE="JavaScript">
<!-- Begin
function checkAll(chname,chcount,startfrom) {
   if(startfrom === undefined) startfrom=1;
   for (var j = startfrom; j <= chcount; j++) {
	box = eval("document.part_list." + chname + j);
	if (box.checked == false) box.checked = true;
   }
}

function uncheckAll(chname,chcount) {
for (var j = 1; j <= chcount; j++) {
box = eval("document.part_list." + chname + j);
if (box.checked == true) box.checked = false;
   }
}

function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

function checkCustomNumber(chname,chcount) {
	var n = document.getElementById("num_to_select").value;
	if(!isNaN(parseInt(n)) && isFinite(n) && parseInt(n)>0) {
		n=parseInt(n);
		uncheckAll(chname,chcount);
		var stype=document.getElementById("custom_number_select").value;
		if(stype>0) {
			checkAll(chname,(n<chcount)?n:chcount);
		}
		if(stype<0) {
			var startfrom=(n<chcount)?parseInt(chcount)-n+1:1;
			checkAll(chname,chcount,startfrom);
		}
		if(stype==0) {
			if(n>=chcount) checkAll(chname,chcount);
			else {
				var num_list = [];
				for (var i = 1; i <= chcount; i++) {
					num_list.push(i);
				}
				//num_list.sort(function(a,b) {return Math.random()-0.5;});
				shuffle(num_list);
				for (var i = 0; i < n; i++) {
					var j = num_list[i];
					box = eval("document.part_list." + chname + j);
					if (box.checked == false) box.checked = true;
				}
			}
		}
		if(n>chcount) alert("'.lang('the_number_choosen').', "+n+" '.lang('is_greater_than_the_number_of_subjects_in_the_list').', "+chcount+".");
		
	}
	else {
		alert("'.lang('please_enter_a_positive_number_to_select').'"); return;
	}
}

function checkpart(i) {
boxpart = eval("document.part_list.part" + i);
boxshup = eval("document.part_list.shup" + i);
if (boxpart.checked == true) {
boxshup.checked = true;
   }
}

function checkshup(i) {
boxpart = eval("document.part_list.part" + i);
boxshup = eval("document.part_list.shup" + i);
if (boxshup.checked == false) {
boxpart.checked = false;
   }
}
//  End -->
</script>
';

}

function script__open_help() {

echo '
<SCRIPT LANGUAGE="JavaScript">
function helppopup(topic) {
        helpaddress= "../'.$GLOBALS['settings__admin_folder'].'/help.php?" + topic;
     hlpfn=open(helpaddress,"help","width=300,height=400,location=no,toolbar=no,menubar=no,status=no,directories=no,scrollbars=yes,resizable=no")
        }
</SCRIPT>

';
}

function script__login_page() {

echo '
<script language=JavaScript>

    <!--
     function gotoUsername() { document.login.adminname.focus(); }
     function gotoPassword() { document.login.password.value=""; document.login.password.focus(); }
     function sendForm() { document.login.submit(); }
     // -->

</script>
';

}


function script__open_faq() {

echo '
<SCRIPT LANGUAGE="JavaScript">

function popupfaq(address) {
faqwin=open(address, "faq", "width=470,height=400,location=no,toolbar=no,menubar=no,status=no,directories=no,scrollbars=yes,resizable=yes") }

</SCRIPT>

';

}

function javascript__close_window() {
        echo '<SCRIPT LANGUAGE="JavaScript">
<!--
window.close()
  //-->
</SCRIPT>';

}
function script__update_responsable($form_name, $select_name) {
	echo '<SCRIPT LANGUAGE="JavaScript">
<!--
	var form_name="'.$form_name.'";
	var select_name="'.$select_name.'";
	document.forms[form_name].onsubmit=function(){
		if(!document.forms[form_name].elements[select_name].value) {
			alert("'.lang($select_name).' '.lang('should_not_be_empty',false).'!");
			return false;
		}
		return true;
	};
	function update_responsable(x,val) {
		var already_present=false;
		var select_elem=x.form.elements[select_name];
		for(var n=0; n<select_elem.childNodes.length; n++) {
			if(select_elem.childNodes[n].value==x.value) already_present=true;
		}
		if(!already_present && x.checked) {
			var option=document.createElement("option");
			option.value=x.value;
			var ot=document.createTextNode(val);
			option.appendChild(ot);
			select_elem.appendChild(option);
		}
	}
	function updown(me,mult){
		if(me.name=="year") {
			var valarr=me.value.split("/");
			var d = new Date();
			var initval = d.getFullYear();
			if(d.getMonth()<7) initval-=1;
			if(!isNaN(parseInt(valarr[0]))) initval=parseInt(valarr[0]);
			var newval1=initval+Number(mult), newval2=initval+Number(mult)+1;
			me.value=newval1+"/"+newval2;
			me.form.number.value="";
		}
		else {
			var initval=0;
			if(!isNaN(parseInt(me.value))) initval=parseInt(me.value);
			var newval=initval+mult;
			me.value=(newval>=0)?newval:"";
		}
		
	}
	</SCRIPT>';
}

function script__calculate_sum($nnominals,$nominal_pref,$noncash_present) {
	$money_symbol_rep=(lang('money_symbol')=='')?"\\s":"\\s|".lang('money_symbol');
	echo '<SCRIPT LANGUAGE="JavaScript">
<!--
	var nominal_pref="'.$nominal_pref.'";
	var noncash_present='.$noncash_present.';
	var nnominals='.$nnominals.';
	var nnoncash=noncash_present?1:0;
	var instead_of_ness=false;
	var instead_of_acc="";
	function id(x) {return document.getElementById(x);}
	function account_info(me) {
		check_instead_of(me);
		if(me.value==""){
			id(me.name+"_info").innerHTML="";
			id(me.name+"_init_amount").value="";
			return false;
		}
		amounts=id("acc_amounts").value.split(",");
		for(var i = 0; i<amounts.length; i++) {
			var keyval=amounts[i].split(":");
			if(keyval[0]==me.value) {
				var color="#333";
				if(me.value=="bank") color="#BBB";
				var innerinfo=keyval[1]+"&nbsp;'.lang('money_symbol').' ('.lang('for_this_file',false).')";
				if(keyval.length>2 && keyval[1]>0) innerinfo+=", ";
				if(keyval.length>2 && keyval[1]==0) innerinfo="";
				if(keyval.length>2) innerinfo+=keyval[2]+"&nbsp;'.lang('money_symbol').' ('.lang('for_mix_account',false).')";
				id(me.name+"_info").innerHTML="<small style=\'color:"+color+"\'>"+innerinfo+"</small>";
				id(me.name+"_init_amount").value=keyval[1];
				break;
			}
		}
	}
	function check_instead_of(me) {
		var myname=me.name;
		var othername=(myname=="debit")?"credit":"debit";
		var myval=me.value; var otherval=me.form.elements[othername].value;
		var proceed=false;
		var acc_base=id("instead_of_base").value;
		var acc_base_arr=acc_base.split(",");
		var my_arr = new Array(), other_arr = new Array(), my_fnames = new Array(), other_fnames = new Array(), my_nalt=0, other_nalt=0;
		var my_ness=false, other_ness=false;
		for(var i=0; i<acc_base_arr.length; i++) {
			var carr=acc_base_arr[i].split(":");
			if(carr[0]==myval && carr.length>2) {my_arr=carr[1].split("-"); if(my_arr.shift()=="n") my_ness=true;  my_nalt=my_arr.length; my_fnames=carr[2].split("-");}
			if(carr[0]==otherval && carr.length>2) {other_arr=carr[1].split("-"); if(other_arr.shift()=="n") other_ness=true; other_nalt=other_arr.length; other_fnames=carr[2].split("-");}
		}
		var proceed_arr= new Array(), proceed_nalt=0, proceed_ness=false, proceed_val="", proceed_name="";
		//alert("myval="+myval+"\r\notherval="+otherval+"\r\nmy_nalt="+my_nalt+"\r\nother_nalt="+other_nalt);
		if(myval=="lab" && otherval!="lab" && other_nalt>0) {
			proceed_arr=other_arr; proceed_nalt=other_nalt; proceed_ness=other_ness; proceed_val=otherval; proceed_name=othername; proceed_fnames=other_fnames;
			proceed=true;
		}
		if(otherval=="lab" && myval!="lab" && my_nalt>0) {
			proceed_arr=my_arr; proceed_nalt=my_nalt; proceed_ness=my_ness; proceed_val=myval; proceed_name=myname; proceed_fnames=my_fnames;
			proceed=true;
		}
		if(!proceed) {
			instead_of_ness=false;
			instead_of_acc="";
			id("for_instead_of").innerHTML="";
			return;
		}
		instead_of_ness=proceed_ness; instead_of_acc=proceed_name;
		var io_sel=document.createElement("select");
		io_sel.name="instead_of_file";
		io_sel.setAttribute("id","instead_of_file");
		io_sel.setAttribute("onchange","color_black(this)");
		if(proceed_nalt>1 || (proceed_nalt>0 && !proceed_ness)) {
			var pr_option=document.createElement("option");
			pr_option.value="";
			pr_option.selected="SELECTED";
			var text_empty=proceed_ness?"'.lang('please_choose').'":"'.lang('no').'";
			var ot=document.createTextNode(text_empty);
			pr_option.appendChild(ot);
			io_sel.appendChild(pr_option);
		}
		for(var i=0; i<proceed_arr.length; i++) {
			var pr_option=document.createElement("option");
			pr_option.value=proceed_arr[i];
			if(proceed_nalt==1) pr_option.selected="SELECTED";
			var ot=document.createTextNode(proceed_fnames[i]);
			pr_option.appendChild(ot);
			io_sel.appendChild(pr_option);
		}
		id("for_instead_of").innerHTML="<span id=\'instead_of_quest\' style=\'color:black\'>'.lang('this_file_replaces_another?').'</span>"+"<br>"+io_sel.outerHTML;
		// alert(io_sel.innerHTML);
	}
	function calculate_sum(me){
		var idarr=me.id.split("_");
		var n=idarr.pop();
		var cnominal=id("nn_"+n).value;
		var errm="";
		if(me.value!=="") {
			var regmatch=me.value.match(/[^0-9+\-\/*.]/g);
			if(regmatch!=null && regmatch.length>0) errm="Please wirte in numbers";
			regmatch=me.value.match(/[+\-\/*.]/g);
			if(errm=="" && regmatch!=null && regmatch.length>0) {
				try{me.value=eval(me.value);}
				catch(err) {errm="Error: "+err.message;}
			}
		}
		if(errm!="") {
			me.value=initpayvals[n-1];
			document.getElementById("total_"+n).innerHTML="<span style=\"cursor:help;color:red\" title="+errm.replace(/\s/g,"_")+" >error!</span>";
			id("total_all").innerHTML="";
			if(id("total_cash")!=null) id("total_cash").innerHTML="";
			//alert(errm);
			return false;
		}
		var initHTTMLval=id("total_"+n).innerHTML;
		id("total_"+n).innerHTML=initHTTMLval.replace(/error!/g,"");
		if(n<=nnominals) id("total_"+n).innerHTML=me.value*cnominal+" '.lang('money_symbol').'";
		sumcash=0;
		for(h=1; h<=nnominals; h++) {
			var cvalue=(id("total_"+h).innerHTML=="")?0:parseFloat(id("total_"+h).innerHTML);
			sumcash+=cvalue;
		}
		sumtotal=sumcash;
		if(noncash_present) sumtotal+=(id("n_"+npart)!=null && id("n_"+npart).value!="")?parseFloat(id("n_"+npart).value):0;
		id("total_all").innerHTML=sumtotal+" '.lang('money_symbol').'";
		if(id("total_cash")!=null) id("total_cash").innerHTML=sumcash+" '.lang('money_symbol').'";

		return false;
	}
	function color_black(me)
	{
		//alert(me.value);
		if(me.value!="") id("instead_of_quest").style.color="black";
	}
	function newoperation(f)
	{
		if(f.debit.value=="" || f.credit.value=="")
		{
			alert("'.lang('both_accounts_should_be_choosen').'!");
			return false;
		}
		var total_amount=id("total_all").innerHTML.replace(/'.$money_symbol_rep.'/g,"");
		if(isNaN(parseFloat(total_amount)))
		{
			alert("'.lang('there_is_an_error_in_amounts').'!");
			return false;
		}
		if(parseFloat(total_amount)<=0)
		{
			alert("'.lang('at_least_one_amount_should_be_positif').'!");
			return false;
		}
		if(f.debit.value==f.credit.value && parseFloat(f.debit_init_amount.value)!=parseFloat(total_amount))
		{
			alert("'.lang('accounts_should_be_different').' '.lang('or_the_total_amount_should_be').' "+f.debit_init_amount.value+"'.lang('money_symbol').' !");
			return false;
		}
		if(instead_of_ness && (id("instead_of_file")==null || id("instead_of_file").value==""))
		{
			alert("'.lang('you_should_choose_the_replacement_file_as_the_experimenter_choosen_is_not_responsible_for_this_file').'!");
			id("instead_of_quest").style.color="red";
			return false;
		}
		f.submit();
	}
	var ctrldown=false;
	var prevkey=0, prevnextid="", nprevequality=0;
	var npart=nnominals+nnoncash;
	var initpayvals=new Array();
	for(var i =1; i<=npart; i++) {
		var cnominal=id("nn_"+i).value;
		var cid=nominal_pref+"_"+i;
		initpayvals[i-1]=document.getElementById(cid).value;
		document.getElementById(cid).onkeydown = function(event) {
			var key;
			//IE uses this
			if(window.event) {
				key = window.event.keyCode;
			}
			//FF uses this
			else {
				key = event.which;
			} 
			var idarr=this.id.split("_");
			var n=idarr.pop();
			var nextid=idarr[0]+"_"+n;
			if(key==13 || key==40 || key==38) {
				event.preventDefault();
				var newn=1;
				var ndone=0;
				for(var h = 1; h<=npart; h++){
					var nnext=Number(n);
					nnext+=(key==38)?-h:h;
					var newn=(nnext<=npart)?nnext:(key!=40)?nnext-npart:(n<npart && ((prevkey==key && prevnextid==idarr.join("_")+"_"+n && nprevequality>0) || ctrldown))?Number(n)+1:n;
					if(newn<1) newn=(key==38 && n>1 && ((prevkey==key && prevnextid==idarr.join("_")+"_"+n && nprevequality>0) || ctrldown))?n-1:n;
					nextid=idarr[0]+"_"+newn;
					/*for(var hh=0; hh<nparts; hh++) {
						if(idarr.join("_")==payids[hh]) {
							if(key==13 && hh<payids.length-1) { 
								var nextid1=payids[hh+1]+"_"+n;
								if(document.getElementById(nextid1).value=="") nextid=nextid1;
							}
							if(key==40 || key==38) nextid=payids[hh]+"_"+newn;
						}
					}*/
					
					try {
					if(document.getElementById(nextid).value!=="" && (key==13 || this.value==="") && !(ctrldown && key!=13) ) ndone++;
					else { break;} // || !(prevkey==key && prevnextid==nextid)  alert("n="+n+"\r\nnnext="+nnext+"\r\nprevkey="+prevkey+", key="+key+"\r\nprevnextid="+prevnextid+", nextid="+nextid);
					} catch(err) {alert("nextid="+nextid+"\r\n"+err.message);}
				}
				

				if(key==13 && ndone==npart) document.getElementById("change").focus();
				else document.getElementById(nextid).focus();
				if(prevnextid==nextid && prevkey==key) nprevequality++; else nprevequality=0;
				prevkey=key; prevnextid=nextid;
				ctrldown=false;
				return false;
			}
			if(key==16 || key==17) ctrldown=true;
		};
	
	}
  //-->
</SCRIPT>';

}
function script__payment_num($initval,$npart,$idarray) {
	echo '<SCRIPT LANGUAGE="JavaScript">
<!--
	var initval='.$initval.';
	var npart='.$npart.';
	function payment_num(me){
		var idarr=me.id.split("_");
		var n=idarr.pop();
		var cvalue=document.getElementById(idarr[0]+"_num_"+n).value;
		var initnumHTTMLval=document.getElementById("td_"+idarr[0]+"_num_"+n).innerHTML;
		var initpayvals=initpayobj[idarr.join("_")];
		//if(idarr.length>1 && idarr[1]=="nlf") initpayvals=initpayobj[idarr[0]+"_"+idarr[1]];
		var errm="";
		if(me.value!=="") {
			var regmatch=me.value.match(/[^0-9+\-\/*.]/g);
			if(regmatch!=null && regmatch.length>0) errm="Please wirte in numbers";
			regmatch=me.value.match(/[+\-\/*.]/g);
			if(errm=="" && regmatch!=null && regmatch.length>0) {
				try{me.value=eval(me.value);}
				catch(err) {errm="Error: "+err.message;}
			}
		}
		if(errm!="") {
			me.value=initpayvals[n-1];
			document.getElementById("td_"+idarr[0]+"_num_"+n).innerHTML+="<span style=\"cursor:help;color:red\" title="+errm.replace(/\s/g,"_")+" >error!</span>";
			//alert(errm);
			return false;
		}
		document.getElementById("td_"+idarr[0]+"_num_"+n).innerHTML=initnumHTTMLval.replace(/error!/g,"");
		//alert("id="+idarr[0]+"_num_"+n+"\r\ncvalue="+cvalue);
		if(cvalue==="" && me.value!=="") {
			var maxnumber=initval;
			for(var i =1; i<=npart; i++)
			{
				//alert("id="+idarr[0]+"_num_"+i);
				var cnumval=document.getElementById(idarr[0]+"_num_"+i).value;
				if(cnumval!="" && cnumval>maxnumber) maxnumber=Number(cnumval);
			}
			var newval=parseFloat(maxnumber)+1;
			document.getElementById(idarr[0]+"_num_"+n).value=newval;
			document.getElementById("td_"+idarr[0]+"_num_"+n).innerHTML=newval;
		}
		if(me.value!=="" && me.value>0 && document.getElementById("shup"+n)!=null) document.getElementById("shup"+n).checked="CHECKED";
		if(me.value!=="" && me.value>0 && document.getElementById("pay_also_check_participations")!=null && document.getElementById("pay_also_check_participations").checked && document.getElementById("part"+n)!=null) document.getElementById("part"+n).checked="CHECKED";
		if(me.value==="" && cvalue!=="") {
			var allEmpty=true;
			for(var ii=1; ii<=payids.length; ii++) if(document.getElementById(payids[ii-1]+"_"+n).value!=="") {allEmpty=false; break;}
			//alert(allEmpty.toString()+"\r\n"+initNums[n-1]);
			if(allEmpty && initNums[n-1]==="") {
				for(var i=1; i<=npart; i++) {
					icvalue=document.getElementById(idarr[0]+"_num_"+i).value;
					if(icvalue!=="" && parseFloat(icvalue)>parseFloat(cvalue) && initNums[i-1]==="") {
						//alert(" icvalue="+icvalue+"\r\n cvalue="+cvalue+"\r\n icvalue>cvalue="+(icvalue>cvalue).toString());
						var newicvalue=icvalue-1;
						document.getElementById(idarr[0]+"_num_"+i).value=newicvalue;
						document.getElementById("td_"+idarr[0]+"_num_"+i).innerHTML=newicvalue;
					}
				}
				document.getElementById(idarr[0]+"_num_"+n).value="";
				document.getElementById("td_"+idarr[0]+"_num_"+n).innerHTML="";
			}
		}
		var somme=0;
		for(var i =1; i<=npart; i++)
		{
			var valueforsum=document.getElementById(idarr.join("_")+"_"+i).value;
			somme+=(valueforsum=="")?0:parseFloat(valueforsum);
		}
		var idsomme=(idarr.length>1 && idarr[1]=="nlf")?"total_noncash":"total_cash";
		if(document.getElementById(idsomme)!=null) document.getElementById(idsomme).innerHTML=somme;
		return false;
	}
	var ctrldown=false;
	var prevkey=0, prevnextid="", nprevequality=0;
	var payids="'.implode(',',$idarray).'".split(",");
	var initpayobj={};
	for(var z=0; z<payids.length; z++) initpayobj[payids[z]]=new Array();
	var initNums=new Array();
	for(pay_id in initpayobj) {
		var pay_id_deb=pay_id+"_";
		for(var i =1; i<=npart; i++) {
		document.getElementById(pay_id_deb+i).onkeydown = function(event) {
		var key;
		//IE uses this
		if(window.event) {
			key = window.event.keyCode;
		}
		//FF uses this
		else {
			key = event.which;
		} 
		var idarr=this.id.split("_");
		var n=idarr.pop();
		var nextid=idarr[0]+"_"+n;
		if(key==13 || key==40 || key==38) {
			event.preventDefault();
			var newn=1;
			var ndone=0;
			for(var h = 1; h<=npart; h++){
				var nnext=Number(n);
				nnext+=(key==38)?-h:h;
				var newn=(nnext<=npart)?nnext:(key!=40)?nnext-npart:(n<npart && ((prevkey==key && prevnextid==idarr.join("_")+"_"+n && nprevequality>0) || ctrldown))?Number(n)+1:n;
				if(newn<1) newn=(key==38 && n>1 && ((prevkey==key && prevnextid==idarr.join("_")+"_"+n && nprevequality>0) || ctrldown))?n-1:n;
				nextid=idarr[0]+"_"+newn;
				for(var hh=0; hh<payids.length; hh++) {
					if(idarr.join("_")==payids[hh]) {
						if(key==13 && hh<payids.length-1) { 
							var nextid1=payids[hh+1]+"_"+n;
							if(document.getElementById(nextid1).value=="") nextid=nextid1;
						}
						if(key==40 || key==38) nextid=payids[hh]+"_"+newn;
					}
				}
				try {
				if(document.getElementById(nextid).value!=="" && (key==13 || this.value==="") && !(ctrldown && key!=13) ) ndone++;
				else { break;} // || !(prevkey==key && prevnextid==nextid)  alert("n="+n+"\r\nnnext="+nnext+"\r\nprevkey="+prevkey+", key="+key+"\r\nprevnextid="+prevnextid+", nextid="+nextid);
				} catch(err) {alert("nextid="+nextid+"\r\n"+err.message);}
			}
			

			if(key==13 && ndone==npart) document.getElementById("change").focus();
			else document.getElementById(nextid).focus();
			if(prevnextid==nextid && prevkey==key) nprevequality++; else nprevequality=0;
			prevkey=key; prevnextid=nextid;
			ctrldown=false;
			return false;
		}
		if(payids.length>1) if(key==37 || key==39) {
			var curr_id=this.id, next_id=curr_id; 
			for(var h =0; h<payids.length; h++)
			{
				if(idarr.join("_")==payids[h])
				{
					if(key==39 && h<payids.length-1) next_id=payids[h+1]+"_"+n;
					if(key==37 && h>0) next_id=payids[h-1]+"_"+n;
				}
			}
			if(curr_id!=next_id) document.getElementById(next_id).focus();
			ctrldown=false;
			//event.preventDefault();
		}
		if(key==16 || key==17) ctrldown=true;
		};
		initpayobj[pay_id][i-1]=document.getElementById(pay_id_deb+i).value;
		if(pay_id==payids[0]) {
			var idarr=pay_id.split("_");
			initNums[i-1]=document.getElementById(idarr[0]+"_num_"+i).value;
		}
		
		}
	}
	
	function showhideautoinput() {
		var cdisp=document.getElementById("autoinputfield").style.display;
		var autoinputbutname=document.getElementById("autoinputbut").value;
		if(cdisp=="none") {
			document.getElementById("autoinputfield").style.display="block";
			document.getElementById("autoinputbut").value=autoinputbutname.replace(/Show/,"Hide");
		}
		else {
			document.getElementById("autoinputfield").style.display="none";
			document.getElementById("autoinputbut").value=autoinputbutname.replace(/Hide/,"Show");
		}
	}
	
	function proceedautoinput() {
		var areastring=document.getElementById("autoinputarea").innerHTML;
		if(areastring.replace(/^\s+|\s+$/,"")=="") areastring=document.getElementById("autoinputarea").value;
		// alert(areastring);
		if(areastring.replace(/^\s+|\s+$/,"")=="") return;
		areastring=areastring.replace("\r\n","\n");
		var arealines=areastring.split(/\n/);
		var headers=arealines[0].split(/[\t;]/);
		var mainform=document.forms["part_list"];
		var tables=mainform.getElementsByTagName("table");
		var maintable=tables[0];
		var mainrows=maintable.getElementsByTagName("tr");
		var i_fname,i_lname,i_gain;
		var theaders=mainrows[0].getElementsByTagName("td");
		for(i=0; i<theaders.length; i++) {
			if(theaders[i].innerHTML.replace(/'.lang('lastname').'/,"")!=theaders[i].innerHTML) i_lname=i;
			if(theaders[i].innerHTML.replace(/'.lang('firstname').'/,"")!=theaders[i].innerHTML) i_fname=i;
			if(theaders[i].innerHTML.replace(/'.lang('payment').'/,"")!=theaders[i].innerHTML) i_gain=i;
		}
		// alert("i_fname="+i_fname+", i_lname="+i_lname+", i_gain="+i_gain);
		for(var n=1; n<arealines.length; n++) {
			if(arealines[n].replace(/^\s+|\s+$/,"")=="") continue;
			var caline=arealines[n].split(/[\t;]/);
			var cdata={};
			for(i=0; i<caline.length; i++) cdata[headers[i]]=caline[i];
			var ei=("ExternalInfo" in cdata && cdata["ExternalInfo"].replace(/\s+/,"")!="");
			var nms=("noms" in cdata);
			var nomsei=""; if(ei) {
				var eia=cdata["ExternalInfo"].split("|");
				nomsei=eia[0].replace(/,/g," ").replace(/[^\x00-\x7F]+|\?+/gi,"").replace(/\s+/," ");
			}
			var nomsnoms=""; if(nms){
				nomsnoms=cdata["noms"].replace(/\s+/," ");
			}
			nomstocheck=new Array();
			if(nomsnoms.replace(/\s+/," ")!="") nomstocheck[nomstocheck.length]=nomsnoms;
			if(nomsei.replace(/\s+/," ")!="") nomstocheck[nomstocheck.length]=nomsei;
			if(nomsnoms.replace(/\s+/," ")!="") nomstocheck[nomstocheck.length]=nomsnoms.normalize("NFD").replace(/[^\x00-\x7F]+/gi,"").replace(/\W|_/g," ");
			if(nomsnoms.replace(/\s+/," ")!="") nomstocheck[nomstocheck.length]=nomsnoms;
			if(nomsei.replace(/\s+/," ")!="") nomstocheck[nomstocheck.length]=nomsei;
			if(nomsnoms.replace(/\s+/," ")!="") nomstocheck[nomstocheck.length]=nomsnoms.normalize("NFD").replace(/[^\x00-\x7F]+/gi,"").replace(/\W|_/g," ");
			if(nomstocheck.length==0) nomstocheck[nomstocheck.length]="-";
			var ncriterea=nomstocheck.length*2;
			var found=false;
			var nc=0;
			var i_found=-1;
			while(!found && nc<ncriterea) {
				for(i=1; i<mainrows.length-1; i++) {
					var crow=mainrows[i].getElementsByTagName("td");
					var cnames=(nc<=nomstocheck.length/2)?crow[i_fname].innerHTML+" "+crow[i_lname].innerHTML:crow[i_lname].innerHTML+" "+crow[i_fname].innerHTML;
					cnames=cnames.replace(/\s+/," ").replace(/\?/,"");
					if(nomsei.replace(/\s+/," ")!="") {
						if(nomsnoms.replace(/\s+/," ")=="" || nc%3==1) cnames=cnames.replace(/[^\x00-\x7F]+/gi,"");
					}
					if(nomsnoms.replace(/\s+/," ")!="") {
						if((nomsei.replace(/\s+/," ")=="" && nc%2==1) || (nomsei.replace(/\s+/," ")!="" && nc%3==2)) cnames=cnames.normalize("NFD").replace(/[^\x00-\x7F]+/gi,"").replace(/\W|_/g," ");
					}
					if(cnames.replace(/\s+/,"")!="" && nomstocheck.length>nc) {
						var rleft=new RegExp(cnames,"i");
						var rright=new RegExp(nomstocheck[nc],"i");
						if(nomstocheck[nc].replace(rleft,"")!=nomstocheck[nc] || cnames.replace(rright,"")!=cnames) {
							found=true;
							i_found=i;
							// alert("Found "+nomstocheck[nc]+" - "+cnames+", nc="+nc+", i_found="+i_found);
							break;
						}
						// else alert("Not found "+nomstocheck[nc]+" - "+cnames+", nc="+nc+", i="+i);
					}
					
				}
				nc++;
			}
			if(!found) document.getElementById("autoinputwarnings").innerHTML+=nomstocheck[0]+" (no "+n+") not found; ";
			else {
				var cid=document.getElementById("payment_"+i_found);
				if(cid===null) document.getElementById("autoinputwarnings").innerHTML+=nomstocheck[0]+" (no "+n+") is null, i_found="+i_found+"; ";
				else {
					if(cid.value!="") document.getElementById("autoinputwarnings").innerHTML+="the gain of "+nomstocheck[0]+" (no "+n+") was not empty; ";
					else {
						gaincolname=("gainEUR" in cdata)?"gainEUR":"gain";
						if(gaincolname in cdata && cdata[gaincolname]!="") {
							if(cdata[gaincolname].replace(/just_?presence|just_?check|just_?participation/gi,"")!=cdata[gaincolname]) {
								if(document.getElementById("shup"+i_found)!=null) document.getElementById("shup"+i_found).checked="CHECKED";
								if(document.getElementById("pay_also_check_participations")!=null && document.getElementById("pay_also_check_participations").checked && document.getElementById("part"+i_found)!=null) document.getElementById("part"+i_found).checked="CHECKED";							
							}
							else {
								cid.focus();
								cid.value=cdata[gaincolname].replace(/[^\x00-\x7F]+|EUR|\s/gi,"").replace(/,/,".");
								cid.blur();
							}
						}
						else document.getElementById("autoinputwarnings").innerHTML+="the gain of "+nomstocheck[0]+" (no "+n+") was not given; ";
					}
				}
			}
		}
	}
  //-->
</SCRIPT>';

}

?>

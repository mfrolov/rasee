<?php
// part of orsee. see orsee.org

function query__form_module($module="",$experiment_id="",$deleted='n') {
	global $lang, $settings, $title;
	if(!isset($_REQUEST['new'])) $_REQUEST['new']="";

	if(substr($module,0,6)=='pform:') {
		$fieldname=substr($module,6);
		$module='pform';
	}
if(isset($_REQUEST['deleted']) || $title=="drop participants") {
 if ($title=="drop participants" || $_REQUEST['deleted']=='b') $deleted="n' OR deleted='y";
 elseif(!empty($_REQUEST['deleted'])) $deleted=$_REQUEST['deleted'];
}
switch ($module) {

	case "all":
		break;

	case "experiment_classes":
		if (!isset($_REQUEST['expclass_not'])) $_REQUEST['expclass_not']="";
		echo '<TABLE width=90%>
                                <TR>
                                        <TD>
                                        <SELECT name="expclass_not">
                                        <OPTION value="NOT"';
                                        if ($_REQUEST['expclass_not']=="NOT" || $_REQUEST['new']) echo ' SELECTED';
                                        echo '>'.$lang['without'].'</OPTION>
                                        <OPTION value=""';
                                        if ($_REQUEST['expclass_not']!="NOT" && !$_REQUEST['new']) echo ' SELECTED';
                                        echo '>'.$lang['only'].'</OPTION>
                                        </SELECT>
                                        '.$lang['participants_participated_expclass'].'
                                        <BR>';
                query__experiment_classes_checkbox_list($experiment_id);
                echo '                  </TD>
                                </TR>
                        </TABLE>';
                break;

	case "experiment_assigned_or":
		if (!isset($_REQUEST['inex_ass'])) $_REQUEST['inex_ass']="";
               	echo '<TABLE width=90%>
				<TR>
					<TD>
					<SELECT name="inex_ass">
					<OPTION value="NOT"';
					if ($_REQUEST['inex_ass']=="NOT" || $_REQUEST['new']) echo ' SELECTED';
					echo '>'.$lang['without'].'</OPTION>
					<OPTION value=""';
					if ($_REQUEST['inex_ass']!="NOT" && !$_REQUEST['new']) echo ' SELECTED';
					echo '>'.$lang['only'].'</OPTION>
					</SELECT>
					'.$lang['participants_were_assigned_to'].'
					<BR>';
		query__current_other_experiments_checkbox_list($experiment_id,"exp_ass");
	        echo '			</TD>
				</TR>
	                </TABLE>';
		break;


	case "experiment_participated_and":
				if (!isset($_REQUEST['inex2'])) $_REQUEST['inex2']="";
                echo '<TABLE width=90%>
                                <TR>
                                        <TD>
                                        <SELECT name="inex2">
                                        <OPTION value="NOT"';
                                        if ($_REQUEST['inex2']=="NOT" || $_REQUEST['new']) echo ' SELECTED';
                                        echo '>'.$lang['without'].'</OPTION>
                                        <OPTION value=""';
                                        if ($_REQUEST['inex2']!="NOT" && !$_REQUEST['new']) echo ' SELECTED';
                                        echo '>'.$lang['only'].'</OPTION>
                                        </SELECT>
                                        '.$lang['participants_participated_all'].'
                                        <BR>';
		query__current_other_experiments_checkbox_list($experiment_id,$formname="exp_and");
                echo '                  </TD>
                                </TR>
                        </TABLE>';
                break;

	case "experiment_participated_or":
				if (!isset($_REQUEST['inex'])) $_REQUEST['inex']="";
                echo '<TABLE width=90%>
                                <TR>
                                        <TD>
                                        <SELECT name="inex">
                                        <OPTION value="NOT"';
                                        if ($_REQUEST['inex']=="NOT" || $_REQUEST['new']) echo ' SELECTED';
                                        echo '>'.$lang['without'].'</OPTION>
                                        <OPTION value=""';
                                        if ($_REQUEST['inex']!="NOT" && !$_REQUEST['new']) echo ' SELECTED';
                                        echo '>'.$lang['only'].'</OPTION>
                                        </SELECT>
                                        '.$lang['participants_have_participated_on'].'
                                        <BR>';
                query__current_other_experiments_checkbox_list($experiment_id,$formname="exp");
                echo '                  </TD>
                                </TR>
                        </TABLE>';
                break;

	case "experiment_registered_future_or":
				if (!isset($_REQUEST['inex3'])) $_REQUEST['inex3']="";
                echo '<TABLE width=90%>
                                <TR>
                                        <TD>
                                        <SELECT name="inex3">
                                        <OPTION value="NOT"';
                                        if ($_REQUEST['inex3']=="NOT" || $_REQUEST['new']) echo ' SELECTED';
                                        echo '>'.$lang['without'].'</OPTION>
                                        <OPTION value=""';
                                        if ($_REQUEST['inex3']!="NOT" && !$_REQUEST['new']) echo ' SELECTED';
                                        echo '>'.$lang['only'].'</OPTION>
                                        </SELECT>
                                        '.lang('participants_registered_for_future_sessions_of_one_of_the_following_experiments').'
                                        <BR>';
                query__current_other_experiments_checkbox_list($experiment_id,$formname="exp_reg_future");
                echo '                  </TD>
                                </TR>
                        </TABLE>';
                break;

	case "field":
	case "field2":
		if (isset($_REQUEST[''.$module.'_bezug']) && empty($_REQUEST['new'])) $bz=$_REQUEST[''.$module.'_bezug']; else $bz="";
		if (!isset($_REQUEST['query_'.$module.'']) || !empty($_REQUEST['new'])) $_REQUEST['query_'.$module.'']="";
		if (!isset($_REQUEST[''.$module.'_not']) || !empty($_REQUEST['new'])) $_REQUEST[''.$module.'_not']="";
		$formfields=participantform__load();
		$form_query_fields=array();
		foreach ($formfields as $f) {
			if( preg_match("/(textline|textarea)/i",$f['type']) &&
				((!$experiment_id && $f['search_include_in_participant_query']=='y')	||
				($experiment_id && 	$f['search_include_in_experiment_assign_query']=='y'))) {
					$tfield=array();
					$tfield['value']=$f['mysql_column_name'];
					if (isset($lang[$f['name_lang']])) $tfield['name']=$lang[$f['name_lang']];
					else $tfield['name']=$f['name_lang']; 
					$form_query_fields[]=$tfield;
				}
		} 
		echo '	<TABLE width=90%>
				<TR>
					<TD> 
							<SELECT name="'.$module.'_not">
							<OPTION value=""';
							if (empty($_REQUEST[''.$module.'_not'])) echo ' SELECTED';
							echo '>'.$lang['where'].'</OPTION>
							<OPTION value="NOT"';
							if ($_REQUEST[''.$module.'_not']=="NOT") echo ' SELECTED';
							echo '>'.$lang['without'].'</OPTION>
							<OPTION value="="';
							if ($_REQUEST[''.$module.'_not']=="=") echo ' SELECTED';
							echo '>'.'='.'</OPTION>
							<OPTION value="!="';
							if ($_REQUEST[''.$module.'_not']=="!=") echo ' SELECTED';
							echo '>'.$lang['not'].' =</OPTION>
							<OPTION value="LIKE"';
							if ($_REQUEST[''.$module.'_not']=="LIKE") echo ' SELECTED';
							echo '>'.lang('starts_by',false).'</OPTION>
							<OPTION value="NOT LIKE"';
							if ($_REQUEST[''.$module.'_not']=="NOT LIKE") echo ' SELECTED';
							echo '>'.lang('starts_not_by',false).'</OPTION>
							<OPTION value="ENDLIKE"';
							if ($_REQUEST[''.$module.'_not']=="ENDLIKE") echo ' SELECTED';
							echo '>'.lang('ends_by',false).'</OPTION>
							<OPTION value="NOT ENDLIKE"';
							if ($_REQUEST[''.$module.'_not']=="NOT ENDLIKE") echo ' SELECTED';
							echo '>'.lang('ends_not_by',false).'</OPTION>
							</SELECT>
							<INPUT type=text size=15 maxlength=700 name=query_'.$module.'
									value="'.$_REQUEST['query_'.$module.''].'">
							'.$lang['in'].'
        	                		<SELECT name="'.$module.'_bezug">
                	        	<OPTION value="all"'; if ($bz=="all" || $_REQUEST['new']) echo ' SELECTED'; echo '>'.
						$lang['anyone'].'</OPTION>';
						foreach($form_query_fields as $tf) {
                        	echo '<OPTION value="'.$tf['value'].'"'; 
                        	if ($bz==$tf['value']) echo ' SELECTED'; 
                        	echo '>'.$tf['name'].'</OPTION>';
						}
					echo '
                	        		</SELECT>
					</TD>
				</TR>
			</TABLE>';
		break;



	case "pform":
		$req_not_not_set=!isset($_REQUEST[$fieldname.'_not']);
		if (!isset($_REQUEST[$fieldname.'_not'])) $_REQUEST[$fieldname.'_not']="";
		if (!isset($_REQUEST[$fieldname.'_sign'])) $_REQUEST[$fieldname.'_sign']="";
		if (!isset($_REQUEST[$fieldname])) $_REQUEST[$fieldname]="";
		
		// $existing=true;
		//if ($experiment_id) $show_count=false; else $show_count=true;
		// needs to much time for queries. So  better:
		$existing=false; $show_count=false;
		
		$formfields=participantform__load(); $f=array();
		foreach ($formfields as $p) { 
			if($p['mysql_column_name']==$fieldname) $f=$p;
			elseif(!empty($p['search_include_double']) && $p['search_include_double']=='y' && substr($fieldname,-3)=="_x2" &&  substr($fieldname,0,-3)==$p['mysql_column_name']) {$f=$p;}
			// if($p['mysql_column_name']=="birth_year") var_dump($p['mysql_column_name'],$fieldname, substr($fieldname,-3),substr($fieldname,0,-3),empty($p['search_include_double']));
		}
		$f=form__replace_funcs_in_field($f);
		if(!(empty($f['negative_form']) || $f['negative_form']=='n') && $req_not_not_set) $_REQUEST[$fieldname.'_not']="!";
		if (isset($f['mysql_column_name'])) {
			$whereword=$lang['where'];
			if(!empty($lang['where_profession_is'])) {
				$wordsforwhere=explode(" ",$lang['where_profession_is']);
				$whereword=$wordsforwhere[0];
			}
			echo '<TABLE width=90%>
					<TR>
					<TD>'.$whereword.' ';
			if(isset($lang[$f['name_lang']])) echo  $lang[$f['name_lang']]; else echo $f['name_lang'];
			echo ' ';
			if ($f['type']=='select_numbers') {
				echo '<select name="'.$fieldname.'_sign">
                      <OPTION value="<="'; if ($_REQUEST[$fieldname.'_sign']=="<=") echo ' SELECTED'; echo '><=</OPTION>
					  <OPTION value="="'; if ($_REQUEST[$fieldname.'_sign']=="=" || $_REQUEST[$fieldname.'_sign']=="" || $_REQUEST['new']) echo ' SELECTED'; echo '>=</OPTION>
                      <OPTION value=">"'; if ($_REQUEST[$fieldname.'_sign']==">") echo ' SELECTED'; echo '>></OPTION>
					  </select>';							
			} elseif($f['type']=='invitations') {
				echo '<select name="'.$fieldname.'_not">
				 	<OPTION value=""'; if (empty($_REQUEST[$fieldname.'_not'])) echo ' SELECTED'; echo '>=</OPTION>
				 	<OPTION value="!"'; if ($_REQUEST[$fieldname.'_not']=='!') echo ' SELECTED';	echo '>'.$lang['not'].' =</OPTION>
				 	<OPTION value="LIKE"'; if ($_REQUEST[$fieldname.'_not']=='LIKE') echo ' SELECTED';	echo '>'.$lang['where'].'</OPTION>
				 	<OPTION value="NOT LIKE"'; if ($_REQUEST[$fieldname.'_not']=='NOT LIKE') echo ' SELECTED';	echo '>'.$lang['without'].'</OPTION>
				 	</select> ';
			}
			else {
				echo '<select name="'.$fieldname.'_not">
				 	<OPTION value=""'; if (!$_REQUEST[$fieldname.'_not']) echo ' SELECTED'; echo '>=</OPTION>
				 	<OPTION value="!"'; if ($_REQUEST[$fieldname.'_not']) echo ' SELECTED';	echo '>'.$lang['not'].' =</OPTION>
				 	</select> ';
			}

			if ($f['type']=='select_lang') {
				$existing_chkbx=true;
				if(!empty($f['checkbox_type']) && $f['checkbox_type']!='n') $existing_chkbx=false;
				if(empty($GLOBALS['admin_participants_studies_multiple_choice'])) echo language__selectfield_item($fieldname,$fieldname,$_REQUEST[$fieldname],false,"",$existing,"(deleted='".$deleted."')",$show_count);
				else echo language__checkboxfield_item($fieldname,$fieldname,$_REQUEST[$fieldname],false,"",$existing_chkbx,"(deleted='".$deleted."')",true,($title!="drop participants")?0:$experiment_id);
				// --not ready-- echo language__checkboxfield_item($fieldname,$fieldname,$_REQUEST[$fieldname],false,"",$existing,"(deleted='".$deleted."')",$show_count);
			} elseif ($f['type']=='select_numbers') {
				if ($f['values_reverse']=='y') $reverse=true; else $reverse=false;
        		echo participant__select_numbers($fieldname,$_REQUEST[$fieldname],$f['value_begin'],$f['value_end'],0,$f['value_step'],$reverse,true,$existing,"(deleted='".$deleted."')",$show_count);
			} elseif (preg_match("/(select_list|radioline)/i",$f['type']) && !$existing) {
				$f['value']=$_REQUEST[$fieldname];
				// echo  form__render_select_list($f);
				$cwhere="(deleted='".$deleted."')";
				$no_empty=false;
				if($f['mysql_column_name']=='gender' && $title!="drop participants") {
					$current_exp=orsee_query("SELECT * from ".table("experiments")." WHERE experiment_id='$experiment_id'");
					if(!empty($current_exp['gender_equality']) && $current_exp['gender_equality']!="n") $no_empty=true;
				}
				echo  form__render_radioline($f,$cwhere,true,($title!="drop participants")?0:$experiment_id,$no_empty);
			} else {
				echo participant__select_existing($fieldname,$_REQUEST[$fieldname],"(deleted='".$deleted."')",$show_count);
				// echo participant__checkbox_existing($fieldname,$_REQUEST[$fieldname],"(deleted='".$deleted."')",$show_count);
			}

	    	echo '			</TD>
					</TR>
	            	    </TABLE>';
	    }
		break;

	case "noshowups":
		$query="SELECT max(number_noshowup) as maxnoshow FROM ".table('participants')."
			WHERE deleted='n' ORDER BY number_noshowup DESC LIMIT 1";
		$line=orsee_query($query);
		if (!isset($_REQUEST['query_noshowups'])) $_REQUEST['query_noshowups']=0;
               	echo '<TABLE width=90%>
				<TR>
					<TD>
						'.$lang['where_nr_noshowups_is'].'
				 		<select name="query_noshowups_sign">
                                 			<OPTION value="<="'; 
						if (!isset($_REQUEST['query_noshowups_sign']) || $_REQUEST['query_noshowups_sign']!=">") echo ' SELECTED';
                                                        echo '><=</OPTION>
                                 			<OPTION value=">""'; 
						if (isset($_REQUEST['query_noshowups_sign']) && $_REQUEST['query_noshowups_sign']==">") echo ' SELECTED';
                                                        echo '>></OPTION>
                                 		</select> ';
				 		helpers__select_numbers("query_noshowups",
							$_REQUEST['query_noshowups'],
							0,$line['maxnoshow'],0);
	        echo '			</TD>
				</TR>
	                </TABLE>';
		break;

        case "nr_participations":
                $query="SELECT max(number_reg) as maxnumreg FROM ".table('participants')."
                        WHERE deleted='n' ORDER BY number_reg DESC LIMIT 1";
                $line=orsee_query($query);
                if (!isset($_REQUEST['query_nr_participations'])) $_REQUEST['query_nr_participations']=0;
                echo '<TABLE width=90%>
                                <TR>
                                        <TD>
                                                '.$lang['where_nr_participations_is'].'
                                                <select name="query_nr_participations_sign">
                                                        <OPTION value="<="';
                                           if (!isset($_REQUEST['query_nr_participations_sign']) || $_REQUEST['query_nr_participations_sign']!=">") echo ' SELECTED';
                                                        echo '><=</OPTION>
                                                        <OPTION value=">""';
                                           if (isset($_REQUEST['query_nr_participations_sign']) && $_REQUEST['query_nr_participations_sign']==">") echo ' SELECTED';
                                                        echo '>></OPTION>
                                                </select> ';
                                                helpers__select_numbers("query_nr_participations",
                                                        $_REQUEST['query_nr_participations'],
                                                        0,$line['maxnumreg'],0);
                echo '                  </TD>
                                </TR>
                        </TABLE>';
                break;

        case "last_activity":
                // $query="SELECT max(number_reg) as maxnumreg FROM ".table('participants')." WHERE deleted='n' ORDER BY number_reg DESC LIMIT 1";
                // $line=orsee_query($query);
                if (!isset($_REQUEST['query_last_activity'])) $_REQUEST['query_last_activity']='';
                echo '<TABLE width=90%>
                                <TR>
                                        <TD>
                                                '.lang('where_last_seen_date_is',false).'
                                                <select name="query_last_activity_sign">
                                                        <OPTION value=">="';
                                           if (!isset($_REQUEST['query_last_activity_sign']) || $_REQUEST['query_last_activity_sign']!="<") echo ' SELECTED';
                                                        echo '>>=</OPTION>
                                                        <OPTION value="<"';
                                           if (isset($_REQUEST['query_last_activity_sign']) && $_REQUEST['query_last_activity_sign']=="<") echo ' SELECTED';
                                                        echo '><</OPTION>
                                                </select> ';
                                                helpers__select_date("query_last_activity",
                                                        $_REQUEST['query_last_activity']
														);
                echo '                  </TD>
                                </TR>
                        </TABLE>';
                break;

        case "last_invitation":
                if (!isset($_REQUEST['query_last_invitation'])) $_REQUEST['query_last_invitation']='';
                echo '<TABLE width=90%>
                                <TR>
                                        <TD>
                                                '.lang('where_last_invitation_date_is',false).'
                                                <select name="query_last_invitation_sign">
                                                        <OPTION value="<"';
                                           if (!isset($_REQUEST['query_last_invitation_sign']) || $_REQUEST['query_last_invitation_sign']=="<") echo ' SELECTED';
                                                        echo '><</OPTION>
                                                        <OPTION value=">="';
                                           if (isset($_REQUEST['query_last_invitation_sign']) && $_REQUEST['query_last_invitation_sign']!="<") echo ' SELECTED';
                                                        echo '>>=</OPTION>
                                                </select> ';
                                                helpers__select_date("query_last_invitation",
                                                        $_REQUEST['query_last_invitation']
														);
                echo '                  </TD>
                                </TR>
                        </TABLE>';
                break;

	case "rand_subset":
		$query_limit = (!isset($_REQUEST['query_limit']) ||!$_REQUEST['query_limit']) ? $settings['query_random_subset_default_size'] : $_REQUEST['query_limit'];
                echo '	<TABLE width=90%>
				<TR>
					<TD>
						'.$lang['limit_to_randomly_drawn'].'<BR>
						<INPUT type=text name="query_limit" value="'.
							$query_limit.'" size=5 maxlength=6>
	                		</TD>
				</TR>
	                </TABLE>';
		break;

        case "subjectpool":
        		if (!isset($_REQUEST['subjectpool_not'])) $_REQUEST['subjectpool_not']="";
        		if (!isset($_REQUEST['query_subjectpool'])) $_REQUEST['query_subjectpool']="";
                echo '  <TABLE width=90%>
                                <TR>
                                        <TD>
                                                '.$lang['who_are_in_subjectpool'].'
                                                <select name="subjectpool_not">
                                                        <OPTION value=""';
                                                                if (!$_REQUEST['subjectpool_not']) 
                                                                        echo ' SELECTED';
                                                                echo '></OPTION>
                                                        <OPTION value="!"';
                                                                if ($_REQUEST['subjectpool_not']) 
                                                                        echo ' SELECTED';
                                                                echo '>'.$lang['not'].'</OPTION>
                                                </select> ';
						echo subpools__select_field('query_subjectpool','subpool_id',
								'subpool_name',$_REQUEST['query_subjectpool']);
                echo '                  </TD>
                                </TR>
                        </TABLE>';
                break;

	}

}


function query__where_clause_module($module="",$experiment_id="") {
	$where_clause="";
	if(substr($module,0,6)=='pform:') {
		$fieldname=substr($module,6);
		$module='pform';
	}

switch ($module) {

        case "all":
		$where_clause=" AND ".table('participants').".participant_id IS NOT NULL";
		break;

	case "experiment_classes":
                $class_posted=array(); $class_part=array(); $participants=array();
				// var_dump($_REQUEST['expclass']);
                $class_posted= (isset($_REQUEST['expclass']) && $_REQUEST['expclass']!=='') ? $_REQUEST['expclass'] : array();
                if (count($class_posted) > 0) {
                                foreach ($class_posted as $class) if ($class!=='') $class_part[]=$class;
                        } else {
                                $class_part=array();
                                }
				// var_dump($class_part);
                if (count($class_part) > 0) {
						$exp_extract=array();
						$query="SELECT DISTINCT ".table('experiments').".experiment_class AS class, ".table('experiments').".experiment_id AS id
								FROM ".table('participate_at').", ".table('experiments')."
								WHERE participated='y' 
								AND ".table('participate_at').".experiment_id=".table('experiments').".experiment_id 
								";
						$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
						$first=true;
						while ($row=mysqli_fetch_assoc($result)) {
								$exp_extract[]=$row;
								}
						// var_dump($exp_extract); exit;
                        $wclause="";
                        $first=true;
                        foreach ($class_part as $class) {
                                if ($first==true) { $wclause.=" AND ( "; $first=false; } else $wclause.=" OR ";
								$exp_id_listarr=array();
								$n_exp=0;
								foreach($exp_extract as $exp)
								{
									$expclasses=explode(",",$exp['class']);
									if(in_array($class,$expclasses)) $exp_id_listarr[]=$exp['id'];
								}
                                // $wclause.="experiment_class = '".$class."'";
                                $wclause.="".table('experiments').".experiment_id IN ('".implode("','",$exp_id_listarr)."')";
                                $part_class[$class]=array();
                                }
                        if ($wclause) $wclause.=" )";

                        $query="SELECT DISTINCT participant_id 
								FROM ".table('participate_at').", ".table('experiments')."
								WHERE participated='y' 
								AND ".table('participate_at').".experiment_id=".table('experiments').".experiment_id 
								".$wclause." ORDER BY participant_id";
						// var_dump($query);
                        $result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

                        while ($line = mysqli_fetch_assoc($result)) $participants[]=$line['participant_id'];
                 } else {
                        $participants=array();
                        }
                $in_phrase=implode("','",$participants);

                $where_clause=" ".table('participants').".participant_id ".$_REQUEST['expclass_not'].
                                " IN ('".$in_phrase."') ";
                break;

	case "experiment_assigned_or":
		$exp_posted=array(); $exp_part=array(); $participants=array();
		if(isset($_REQUEST['exp_ass'])) $exp_posted= ($_REQUEST['exp_ass']);
                if (count($exp_posted) > 0) {
                                foreach ($exp_posted as $exp) if ($exp) $exp_part[]=$exp;
                        } else {
                                $exp_part=array();
                                }
                if (count($exp_part) > 0) {
                        $wclause="";
                        $first=true;
                        foreach ($exp_part as $exp) {
                                if ($first==true) { $wclause.=" AND ( "; $first=false; } else $wclause.=" OR ";
                                $wclause.="experiment_id = '".$exp."'";
                                $part_exp[$exp]=array();
                                }
                        if ($wclause) $wclause.=" )";

                        $query="SELECT DISTINCT participant_id FROM ".table('participate_at')."
                                WHERE experiment_id IS NOT NULL ".$wclause." ORDER BY participant_id";
                        $result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

                        while ($line = mysqli_fetch_assoc($result)) $participants[]=$line['participant_id'];
                 } else {
                        $participants=array();
                        }
                $in_phrase=implode("','",$participants);

                $where_clause=" ".table('participants').".participant_id ".$_REQUEST['inex_ass'].
                                " IN ('".$in_phrase."') ";
                break;

	case "experiment_participated_and":
                $exp_posted=array(); $exp_part=array(); $participants=array();
                if(isset($_REQUEST['exp_and'])) $exp_posted=$_REQUEST['exp_and'];
		if (count($exp_posted) > 0) {
				foreach ($exp_posted as $exp) if ($exp) $exp_part[]=$exp;
			} else {
				$exp_part=array();
				}
		if (count($exp_part) > 0) {
                	$wclause="";
			$first=true;
                	foreach ($exp_part as $exp) {
				if ($first==true) { $wclause.=" AND ( "; $first=false; } else $wclause.=" OR ";
				$wclause.="experiment_id = '".$exp."'";
				$part_exp[$exp]=array();
				}
			if ($wclause) $wclause.=" )";

                	$query="SELECT DISTINCT participant_id, experiment_id FROM ".table('participate_at')."
                        	WHERE participated='y' ".$wclause." ORDER BY participant_id";
					// STR_TO_DATE(CONCAT(session_start_year,'-',LPAD(session_start_month,2,'00'),'-',LPAD(session_start_day,2,'00')),'%Y-%m-%d')
					// $session_where_clause="session_finished='n' AND NOW()<=STR_TO_DATE(CONCAT(session_start_year,'-',LPAD(session_start_month,2,'00'),'-',LPAD(session_start_day,2,'00')),'%Y-%m-%d')";
                	// $query="SELECT DISTINCT participant_id, experiment_id FROM ".table('participate_at')."
                        	// WHERE registered='y' AND session_id IN (SELECT session_id FROM ".table('sessions')." WHERE  $session_where_clause) ".$wclause." ORDER BY participant_id";
                	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
                	while ($line = mysqli_fetch_assoc($result)) $part_exp[$line['experiment_id']][]=$line['participant_id'];
		
			if (count($exp_part)<2) {
				$participants=$part_exp[$exp_part[0]];
			 } else {
				$intersect_function='$participants=array_intersect(';
				$first=true;
				foreach ($exp_part as $exp) {
					if ($first) $first=false; else $intersect_function.=',';
					$intersect_function.='$part_exp['.$exp.']';
					}
				$intersect_function.=');';
				eval($intersect_function);
				}
		 } else {
			$participants=array();
			}

                $in_phrase=implode("','",$participants);

                $where_clause=" ".table('participants').".participant_id ".$_REQUEST['inex2'].
                                " IN ('".$in_phrase."') ";
                break;

	case "experiment_participated_or":
		$exp_posted=array(); $exp_part=array(); $participants=array();
                if(isset($_REQUEST['exp'])) $exp_posted=$_REQUEST['exp'];
                if (count($exp_posted) > 0) {
                                foreach ($exp_posted as $exp) if ($exp) $exp_part[]=$exp;
                        } else {
                                $exp_part=array();
                                }
		if (count($exp_part) > 0) {
                	$wclause="";
                        $first=true;
                        foreach ($exp_part as $exp) {
                                if ($first==true) { $wclause.=" AND ( "; $first=false; } else $wclause.=" OR ";
                                $wclause.="experiment_id = '".$exp."'";
                                $part_exp[$exp]=array();
                                }
                        if ($wclause) $wclause.=" )";

                	$query="SELECT DISTINCT participant_id FROM ".table('participate_at')."
                        	WHERE participated='y' ".$wclause." ORDER BY participant_id";
                	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

                	while ($line = mysqli_fetch_assoc($result)) $participants[]=$line['participant_id'];
                 } else {
                        $participants=array();
                        }
                $in_phrase=implode("','",$participants);

                $where_clause=" ".table('participants').".participant_id ".$_REQUEST['inex'].
                                " IN ('".$in_phrase."') ";
								//var_dump($_REQUEST['inex']);
                break; 


	case "experiment_registered_future_or":
		$exp_posted=array(); $exp_part=array(); $participants=array();
                if(isset($_REQUEST['exp_reg_future'])) $exp_posted=$_REQUEST['exp_reg_future'];
                if (count($exp_posted) > 0) {
                                foreach ($exp_posted as $exp) if ($exp) $exp_part[]=$exp;
                        } else {
                                $exp_part=array();
                                }
		if (count($exp_part) > 0) {
                	$wclause="";
                        $first=true;
                        foreach ($exp_part as $exp) {
                                if ($first==true) { $wclause.=" AND ( "; $first=false; } else $wclause.=" OR ";
                                $wclause.="experiment_id = '".$exp."'";
                                $part_exp[$exp]=array();
                                }
                        if ($wclause) $wclause.=" )";

                	// $query="SELECT DISTINCT participant_id FROM ".table('participate_at')."
                        	// WHERE participated='y' ".$wclause." ORDER BY participant_id";
					$session_where_clause="session_finished='n' AND NOW()<=STR_TO_DATE(CONCAT(session_start_year,'-',LPAD(session_start_month,2,'00'),'-',LPAD(session_start_day,2,'00'),'-',LPAD(session_start_hour,2,'00'),'-',LPAD(session_start_minute,2,'00')),'%Y-%m-%d-%H-%i')";
                	$query="SELECT DISTINCT participant_id, experiment_id FROM ".table('participate_at')."
                        	WHERE registered='y' AND session_id IN (SELECT session_id FROM ".table('sessions')." WHERE  $session_where_clause) ".$wclause." ORDER BY participant_id";
                	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

                	while ($line = mysqli_fetch_assoc($result)) $participants[]=$line['participant_id'];
                 } else {
                        $participants=array();
                        }
                $in_phrase=implode("','",$participants);

                $where_clause=" ".table('participants').".participant_id ".$_REQUEST['inex3'].
                                " IN ('".$in_phrase."') ";
                break; 


	case "field":
	case "field2":
		$qval=trim($_REQUEST['query_'.$module.'']);
		$qval=str_replace("'","\\'",$qval);
		$qval_arr=explode('|',$qval);
		if(str_replace('|','',$qval)=='') $qval_arr=array($qval);
		if(!isset($_REQUEST[''.$module.'_not'])) $_REQUEST[''.$module.'_not']=''; 
		$implode_cond=($_REQUEST[''.$module.'_not']=='NOT')?"AND":"OR";
		$like_start_perc=''; $like_end_perc='';
		$request_module_not_val=$_REQUEST[''.$module.'_not'];
		if(str_replace("LIKE","",$_REQUEST[''.$module.'_not'])!=$_REQUEST[''.$module.'_not']) {
			if(str_replace("ENDLIKE","",$_REQUEST[''.$module.'_not'])!=$_REQUEST[''.$module.'_not']) {
				$like_start_perc='%';
				$request_module_not_val=str_replace("ENDLIKE","LIKE",$_REQUEST[''.$module.'_not']);
			}
			else $like_end_perc='%';
		}
		// var_dump($_REQUEST[''.$module.'_bezug']);
		if ($_REQUEST[''.$module.'_bezug']=='all') {
			$formfields=participantform__load();
			$form_query_fields=array();
			foreach ($formfields as $f) {
				if( preg_match("/(textline|textarea)/i",$f['type']) &&
					((!$experiment_id && $f['search_include_in_participant_query']=='y')	||
					($experiment_id && 	$f['search_include_in_experiment_assign_query']=='y'))) {
					$supp_null_cond=($request_module_not_val=='NOT')?" OR ".table('participants').".".$f['mysql_column_name']." IS NULL":"";
					if(count($qval_arr)<=1) {
						// var_dump("count(qval_arr)<=1",count($qval_arr),$f);
						$form_query_fields[]=(empty(trim($request_module_not_val)) || trim($request_module_not_val)=="NOT")?" ".table('participants').".".$f['mysql_column_name']." ".$request_module_not_val." like '%".$qval."%' $supp_null_cond":" ".table('participants').".".$f['mysql_column_name']." ".$request_module_not_val." '".$like_start_perc.$qval.$like_end_perc."' $supp_null_cond";
					}
					else {
						$newquery_arr=array();
						// var_dump("count(qval_arr)>1",count($qval_arr),$f);
						foreach($qval_arr as $qa) $newquery_arr[]=(empty(trim($request_module_not_val)) || trim($request_module_not_val)=="NOT")?" ".table('participants').".".$f['mysql_column_name']." ".$request_module_not_val." like '%".trim($qa)."%' $supp_null_cond":" ".table('participants').".".$f['mysql_column_name']." ".$request_module_not_val." '".$like_start_perc.trim($qa).$like_end_perc."' $supp_null_cond";
						$form_query_fields[]="(".implode(") ".$implode_cond." (",$newquery_arr).")"; 
					}
				}
			}
			// $where_clause="(".implode(" OR ",$form_query_fields).")"; 
			$where_clause="(".implode(") ".$implode_cond." (",$form_query_fields).")"; 
		} else {
				$supp_null_cond=($request_module_not_val=='NOT')?" OR ".table('participants').".".$_REQUEST[''.$module.'_bezug']." IS NULL":"";
				if (!$qval) {
					if($request_module_not_val!="NOT") {
						if($request_module_not_val=="!=" || str_replace("NOT "," ",$request_module_not_val)!=$request_module_not_val) $request_module_not_val="NOT";
						else $request_module_not_val="";
					}
					$where_clause=$request_module_not_val." (".table('participants').".".$_REQUEST[''.$module.'_bezug']."='' OR ".table('participants').".".$_REQUEST[''.$module.'_bezug']." IS NULL)";
				}
                elseif(count($qval_arr)<=1) $where_clause=(empty(trim($request_module_not_val)) || trim($request_module_not_val)=="NOT")?table('participants').".".$_REQUEST[''.$module.'_bezug']." ".$request_module_not_val." like '%".$qval."%' $supp_null_cond":table('participants').".".$_REQUEST[''.$module.'_bezug']." ".$request_module_not_val." '".$like_start_perc.$qval.$like_end_perc."' $supp_null_cond";
				else {
					$newquery_arr=array();
					// var_dump("count(qval_arr)>1",count($qval_arr),$f);
					foreach($qval_arr as $qa) $newquery_arr[]=(empty(trim($request_module_not_val)) || trim($request_module_not_val)=="NOT")?" ".table('participants').".".$_REQUEST[''.$module.'_bezug']." ".$request_module_not_val." like '%".trim($qa)."%' $supp_null_cond":" ".table('participants').".".$_REQUEST[''.$module.'_bezug']." ".$request_module_not_val." '".$like_start_perc.trim($qa).$like_end_perc."' $supp_null_cond";
					$where_clause="(".implode(") ".$implode_cond." (",$newquery_arr).")"; 
				}
		};
		// if(isset($request_module_not_val) && $request_module_not_val=='NOT') {
			// if(true /*substr($where_clause,0,1)!='('*/) $where_clause='('.$where_clause.')';
			// $where_clause='NOT '.$where_clause;
		// }
		break; 
		
		

	case "pform":
		if (!isset($_REQUEST[$fieldname.'_not'])) $_REQUEST[$fieldname.'_not']="";
		if (!isset($_REQUEST[$fieldname.'_sign'])) $_REQUEST[$fieldname.'_sign']="=";
		if (!isset($_REQUEST[$fieldname])) $_REQUEST[$fieldname]="";
		$formfields=participantform__load(); $f=array();
		$colname=$fieldname;
		foreach ($formfields as $p) { 
			if($p['mysql_column_name']==$fieldname) $f=$p; 
			elseif(!empty($p['search_include_double']) && $p['search_include_double']=='y' && substr($fieldname,-3)=="_x2" &&  substr($fieldname,0,-3)==$p['mysql_column_name']) {
				$f=$p;
				$colname=substr($fieldname,0,-3);
			}
		}
		if (isset($f['mysql_column_name'])) {
			if ($f['type']=='select_numbers') {
				$where_clause=table('participants').".".$colname." ".$_REQUEST[$fieldname.'_sign'].
					" '".mysqli_real_escape_string($GLOBALS['mysqli'],$_REQUEST[$fieldname])."'";
					if(empty($_REQUEST[$fieldname])) $where_clause=table('participants').".".$colname.($_REQUEST[$fieldname.'_sign']=='>'?" IS NOT NULL OR ":" IS NULL OR ").$where_clause;
			}
			elseif(!empty($GLOBALS['admin_participants_studies_multiple_choice']) && $f['type']=='select_lang') {
				if(!empty($f['checkbox_type']) && $f['checkbox_type']!='n') {
					$a_w_c=array();
					// var_dump($module,$fieldname,$_REQUEST[$fieldname]);
					$proceed_foreach=false;
					if(function_exists("is_iterable")) $proceed_foreach = is_iterable($_REQUEST[$fieldname]);
					else $proceed_foreach=(is_array($_REQUEST[$fieldname]) || is_object($_REQUEST[$fieldname]));
					if($proceed_foreach) foreach($_REQUEST[$fieldname] as $rfn) {
						$a_w_c[]=table('participants').".".$colname." ".(($_REQUEST[$fieldname.'_not']!='!')?$_REQUEST[$fieldname.'_not']:"NOT")." LIKE '%".$rfn."%'";
					}
					$where_clause=implode(" OR ",$a_w_c);
				}
				else $where_clause=table('participants').".".$colname." ".(($_REQUEST[$fieldname.'_not']!='!')?$_REQUEST[$fieldname.'_not']:"NOT")." IN ('".
					implode("','",$_REQUEST[$fieldname])."')";				
			}
			elseif(str_replace("LIKE","",$_REQUEST[$fieldname.'_not'])==$_REQUEST[$fieldname.'_not']) {
				$where_clause=table('participants').".".$colname." ".$_REQUEST[$fieldname.'_not']."= '".
					mysqli_real_escape_string($GLOBALS['mysqli'],$_REQUEST[$fieldname])."'";
			}
			else {
				$where_clause=table('participants').".".$colname." ".$_REQUEST[$fieldname.'_not']." '%".
					mysqli_real_escape_string($GLOBALS['mysqli'],$_REQUEST[$fieldname])."%'";
			}
	    } else $where_clause="";
		break;
		
	case "noshowups":
		$where_clause=table('participants').".number_noshowup ".$_REQUEST['query_noshowups_sign'].
					" '".$_REQUEST['query_noshowups']."'";
		break;

	case "nr_participations":
			$where_clause=table('participants').".number_reg ".$_REQUEST['query_nr_participations_sign'].
									" '".$_REQUEST['query_nr_participations']."'";
			break;

	case "last_activity":
			$where_clause=table('participants').".last_visit_time ".$_REQUEST['query_last_activity_sign'].
									" UNIX_TIMESTAMP('".$_REQUEST['query_last_activity']." 00:00:00')";
			$where_clause.=" OR ".table('participants').".creation_time ".$_REQUEST['query_last_activity_sign']." UNIX_TIMESTAMP('".$_REQUEST['query_last_activity']." 00:00:00')";
			// /* takes too much time */  $where_clause.=" OR ".table('participants').".participant_id IN (SELECT id FROM ".table('participants_log')." WHERE timestamp ".$_REQUEST['query_last_activity_sign']." UNIX_TIMESTAMP('".$_REQUEST['query_last_activity']." 00:00:00'))";
			break;

	case "last_invitation":
			$where_clause=table('participants').".last_invitation_time ".$_REQUEST['query_last_invitation_sign'].
									" UNIX_TIMESTAMP('".$_REQUEST['query_last_invitation']." 00:00:00')";
			break;

	case "rand_subset":
		// done in query__orderlimit_module
		break;	

        case "subjectpool":
                $where_clause=table('participants').".subpool_id ".$_REQUEST['subjectpool_not']."= '".
                                        $_REQUEST['query_subjectpool']."'";
                break;
	}

return $where_clause;

}

function query__orderlimit_module($module) {
	$orderlimit="";

	switch ($module) {

	case "rand_subset":
        	$limit = (int) $_REQUEST['query_limit'];
                if ($limit && $limit > 0) {
			mt_srand((int)((double)microtime()*1000000));
			$now=mt_rand();
        		$orderlimit="ORDER BY rand(".$now.") LIMIT ".$limit." ";
			}
		break;
	}
	return $orderlimit;
}

function query__join_assign_module($module) {
	$join_clause="";

	return $join_clause;
}

function query__get_participant_form_modules($query_modules,$experiment_id="") {
	global $lang;
	$return_array=array();
	foreach($query_modules as $module) {
		if ($module!='participant_form_fields') $return_array[]=$module;
		else {
			$formfields=participantform__load();
			foreach ($formfields as $f) {
				if( (!preg_match("/(textline|textarea)/i",$f['type'])) &&
					( ((!$experiment_id)	&& $f['search_include_in_participant_query']=='y') ||
					  ($experiment_id && $f['search_include_in_experiment_assign_query']=='y')
					)  ) {
						$return_array[]="pform:".$f['mysql_column_name'];
						if(!empty($f['search_include_double']) && $f['search_include_double']=='y') $return_array[]="pform:".$f['mysql_column_name'].'_x2';
					}
			}
		} 
	}
	return $return_array;
}

?>

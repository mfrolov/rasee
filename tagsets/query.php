<?php
// part of orsee. see orsee.org

function query__form_saver($experiment_id) {
	global $expadmindata;
	$query="CREATE TABLE IF NOT EXISTS ".table("experiment_queries")." (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(60),
	`experiment_id` int(20) NOT NULL DEFAULT 0,
	`user` VARCHAR(20) ,
	`json` TEXT,
	`date_mod` DATE DEFAULT CURRENT_DATE(),
	`description` VARCHAR(500) NOT NULL DEFAULT '',
	PRIMARY KEY (`id`),
	CONSTRAINT expid_name_user UNIQUE (id,experiment_id,user)
	)"; //	FOREIGN KEY (`user`) REFERENCES ".table('admin')."(adminname), does not work
	// var_dump($query);
 	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
	if(!empty($experiment_id)) $arr_myexp=rasee_db_load_full_array('experiment_queries',array("experiment_id"=>$experiment_id));
	$arr_myaccount=rasee_db_load_full_array('experiment_queries',array("experiment_id"=>"0", "user"=>$expadmindata['adminname']));
	$other_exps_notlast=orsee_query("SELECT * FROM ".table('experiment_queries')." WHERE experiment_id<>$experiment_id AND experiment_id<>0 AND `name`<>'last_used'","return_same");
	$other_last_used=orsee_query("SELECT * FROM ".table('experiment_queries')." WHERE experiment_id<>$experiment_id AND experiment_id<>0 AND `name`='last_used'","return_same");
	$other_users=orsee_query("SELECT * FROM ".table('experiment_queries')." WHERE user<>'".$expadmindata["adminname"]."' AND experiment_id=0","return_same");
	$findIds = function($arr) {
		$names=[];
		if(!is_array($arr)) return $names;
		foreach($arr as $ar) {
			if(array_key_exists("id",$ar)) $names[]=$ar["id"];
		}
		return $names;
	};
	$other_exps=($other_exps_notlast===false)?[]:$other_exps_notlast;
	// var_dump($other_exps);
	$other_exps["_ids"]=$findIds($other_exps);
	$other_exps["_groupname"]=($experiment_id==0)?lang("experiments"):lang("other_experiments");
	// var_dump($other_exps["_groupname"],$other_exps["_ids"]);
	
	if(!empty($other_last_used)) {
		$other_last_used["_ids"]=$findIds($other_last_used);
		$other_last_used["_groupname"]=lang("last_used");
		// var_dump($other_last_used["_groupname"],$other_last_used["_ids"]);
		$other_exps[]=$other_last_used;
		$other_exps["_ids"]=array_merge($other_exps["_ids"],$other_last_used["_ids"]);
	}
	// var_dump($other_exps["_groupname"],$other_exps["_ids"]);
	if(!empty($other_users)) {
		$other_users["_ids"]=$findIds($other_users);
		$other_users["_groupname"]=lang("other_users");
		$other_users["_type"]="other_users";
	}
	$arr_all=[];
	if(!empty($experiment_id) && $arr_myexp!==false) $arr_all=array_merge($arr_all,$arr_myexp);
	if($arr_myaccount!==false) {
		$arr_myaccount[0]["_type"]="my_account";
		$arr_myaccount[count($arr_myaccount)-1]["_endtype"]="my_account_ends";
		$arr_all=array_merge($arr_all,$arr_myaccount);
	}
	// if(empty($arr_all)) return "";
	$used_request=[]; $saved_name="";
	if(!empty($_REQUEST['use_saved_request'])) {
		$used_request=orsee_db_load_array('experiment_queries',$_REQUEST['use_saved_request'],'id','name,user,id,experiment_id');
		if($used_request === false) {
			unset($_REQUEST['use_saved_request']);
			$used_request=[];
		}
		else $saved_name=$used_request["name"];
	}
	$dropdown_name=lang('use_saved_request').':'; $dropdow_color="secondary";
	if(!empty($saved_name)) {
		$dropdown_name=$saved_name;
		if($dropdown_name=="last_used") $dropdown_name=lang($dropdown_name,false);
		$exp_publ_name=experiment__get_public_name($used_request["experiment_id"]);
		$exp_name=experiment__get_value($used_request["experiment_id"],'experiment_name').' ';  //(<a target=_blank href="experiment_show.php?experiment_id='.$used_request['experiment_id'].'">'.lang('show',false).'</a>)
		$tooltip_starts='<u class=\'exptype_hint\' data-html=\'true\' data-bs-html=\'true\' data-bs-placement=\'auto\' data-toggle=\'tooltip\' title=\''.$exp_name.'\'>'; //'<u>'; 
		$tooltip_ends='</u>';
		if(!empty($used_request["experiment_id"]) && $used_request["experiment_id"]!=$experiment_id) $dropdown_name.=' ('.$exp_publ_name.')';
		// var_dump(htmlspecialchars($dropdown_name));

		$dropdow_color="warning";
		$dropdown_name=lang('loaded_request').': <q>'.$dropdown_name.'</q>';
	}
	if(!empty($other_exps["_ids"])) $arr_all[]=$other_exps;
	if(!empty($other_users)) $arr_all[]=$other_users;
	$arr_all["_groupname"]=$dropdown_name;
	$bs_dropdown_menu = function($arr) use($dropdow_color,$experiment_id,$used_request,$expadmindata,&$bs_dropdown_menu) {
		// echo "<hr>"; var_dump($arr);
		static $hearder_myaccount_done=false;
		$id=str_replace(" ","_",$arr["_groupname"]);
		$classname=array_key_exists("_ids",$arr)?'dropdown-item':'btn btn-'.$dropdow_color.' btn-sm';
		if(array_key_exists("_ids",$arr) && array_key_exists("id",$used_request) && in_array($used_request["id"],$arr["_ids"])) $classname.=" active";
		// var_dump($arr["_groupname"],array_key_exists("_ids",$arr),array_key_exists("id",$used_request),(array_key_exists("_ids",$arr) && array_key_exists("id",$used_request))?in_array($used_request["id"],$arr["_ids"]):"none",$used_request["id"],$arr["_ids"]);
		$text='<div class="dropdown">
		  <a class="'.$classname.' dropdown-toggle" href="#" role="button" id="dropdownMenu'.$id.'" data-bs-toggle="dropdown"  data-bs-auto-close="outside">
			'.$arr["_groupname"].'
		  </a>

		  <ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$id.'">';
		foreach($arr as $ai=>$av) {
			// if(is_array($av) && array_key_exists("_type",$av)) echo "<hr>".$av["_type"];
			if(!is_numeric($ai)) continue;
			if(!empty($experiment_id) && array_key_exists("_type",$av) && $av["_type"]=="my_account" && !$hearder_myaccount_done) {
				$text.='<li><h6 class="dropdown-header"><em><small>'.lang('my_account',false).':</small></em></h6></li>';
				$hearder_myaccount_done=true;
			}
			$text.='<li>';
			if(array_key_exists("id",$av)) {
				$text.='<a class="dropdown-item';
				if(array_key_exists("id",$used_request) && $av["id"]==$used_request["id"]) $text.=' active';
			
				$text.='" href="'.thisdoc().'?use_saved_request='.$av['id'];
				if(!empty($_GET['deleted'])) $text.= '&deleted='.$_GET['deleted'];
				if(!empty($experiment_id)) $text.= '&experiment_id='.$experiment_id;
				$name="---";
				if(array_key_exists("name",$av)) $name=($av['name']=="last_used")?lang($av['name'],false):$av['name'];
				elseif(array_key_exists("_groupname",$av)) $name=($av['_groupname']=="last_used")?lang($av['_groupname'],false):$av['_groupname'];
				if(!empty($av["experiment_id"]) && $av["experiment_id"]!=$experiment_id) {
					$exp_publ_name=experiment__get_public_name($av["experiment_id"]);
					$exp_name=experiment__get_value($av["experiment_id"],'experiment_name')." (<a target=_blank href='experiment_show.php?experiment_id=".$av['experiment_id']."'>".lang('show',false).'</a>)'; //"<a style='color:white' target=_blank href='experiment_show.php?experiment_id=".$av['experiment_id']."'>"..'</a>';
					$tooltip_starts='<u class="exptype_hint" data-html="true" data-bs-html="true" data-bs-placement="auto" data-toggle="tooltip" title="'.$exp_name.'">';
					$tooltip_ends='</u>';
					if($av['name']=="last_used") $name=$tooltip_starts.$exp_publ_name.$tooltip_ends.' ('.$av['date_mod'].')'; //.', '.$av['user']
					else $name.=' ('.$tooltip_starts.$exp_publ_name.$tooltip_ends.')'; //.', '.$av['date_mod']
				}
				if(!empty($av["user"]) && $av["user"]!=$expadmindata['adminname'] && empty($av["experiment_id"])) {
					$name.=' ('.$av["user"].', '.$av['date_mod'].')';
				}
				$text.= '">'.$name.'</a>';
			}
			else $text.=$bs_dropdown_menu($av);
			if(!empty($experiment_id) && array_key_exists("_endtype",$av) && $av["_endtype"]=="my_account_ends") $text.='<li><hr width=50% class="dropdown-divider"></li>';
			$text.= '</li>';
		}
		if(array_key_exists("_ids",$arr)) $text.='</ul></div>';
		return $text;
	};
	$text=empty($arr_all)?"":$bs_dropdown_menu($arr_all);
	$cdoc=thisdoc();
	$add_controls=($cdoc=="experiment_add_participants.php" || $cdoc=="participants_show.php");
	// var_dump($cdoc,$add_controls);
	if($add_controls) {
		$use_save=true;
		$use_delete=((isset($used_request['user']) && $expadmindata['adminname']==$used_request['user'] && empty($experiment_id) && empty($used_request['experiment_id'])) || (check_allow('experiment_delete') && !empty($used_request['experiment_id']) && !empty($experiment_id) && $used_request['experiment_id']==$experiment_id));
		if($use_save) {
			$text.='<li class="divider"><hr class="dropdown-divider"></li>';
			$text.='<li id=li_save_as><a class="dropdown-item" href="javascript:void(document.getElementById(\'save_request_form\').style.display=\'block\');void(document.getElementById(\'li_save_as\').style.display=\'none\');void(document.getElementById(\'request_name\').click());void(document.getElementById(\'request_name\').focus())">'.lang('save_as').'...</a></li>';
		
		}
		if(!empty($_REQUEST['use_saved_request']) && $saved_name!="last_used" && $use_delete) {
			if(!$use_save) $text.='<li class="divider"><hr class="dropdown-divider"></li>';
			$text.='<li id=li_delete_current><a class="dropdown-item" href="javascript:void(document.getElementById(\'delete_request_form\').style.display=\'block\');void(document.getElementById(\'li_delete_current\').style.display=\'none\');void(document.getElementById(\'dropdownMenu'.str_replace(' ','_',$dropdown_name).'\').click())"><small class="text-danger">'.lang('delete').' <q>'.$saved_name.'</q></a></small></li>';
		}
		$text.='
		  </ul>
		</div>';
		$text.='<div id="save_request_form" class="container" style="display:none"><div class="input-group mb-3">
		<input type="text" id=request_name name=request_name class="form-control" placeholder="'.lang('request_name').'">
		<input type="submit" class="btn-outline" name="save_request" value="'.lang('save').'"/>
		</div></div>';
		if(!empty($_REQUEST['use_saved_request']) && $use_delete) {
			$text.='<div id="delete_request_form" class="container" style="display:none"><div class="input-group mb-3">
			<input type="hidden" id=delete_request_id name=delete_request_id value="'.$_REQUEST['use_saved_request'].'">
			<input type="submit" class="btn btn-danger" name="delete_request" value="'.lang('delete').' `'.$saved_name.'`"/>
			</div></div>';
		}
	}
	return $text;
}

function query__form($query_modules,$experiment=array(),$shadecolors=array()) {
	global $lang, $color;
	if(empty($shadecolors)) $shadecolors=array($color['list_item_background']); //array($color['list_item_background'],$color['list_list_background']); //array($color['list_item_background']);
	elseif(!is_array($shadecolors)) $shadecolors=explode(',',$shadecolors);
	if (is_array($experiment) && isset($experiment['experiment_id']) && $experiment['experiment_id']) $experiment_id=$experiment['experiment_id']; else $experiment_id="";
	echo '	<TABLE border=0 width=90%>
			<TR>
				<TD colspan=3 align=left>
					<TABLE width=100% border=0>
					<TR><TD align=left valign=bottom>
					'.$lang['query_select_all'].'
					</TD><TD align=right>
					<A HREF="'.thisdoc().'?new=true';
					if(!empty($_GET['deleted'])) echo '&deleted='.$_GET['deleted'];
					if ($experiment_id) echo '&experiment_id='.$experiment_id;
					echo '">'.$lang['reset_query_form'].'</A>';
					echo query__form_saver(empty($experiment_id)?0:$experiment_id);
					echo '
					</TD></TR>
					</TABLE>
				</TD>
			</TR>';
	$i=0;
	// var_dump($_REQUEST['use']);
	if (isset($_REQUEST['use'])) $lused=$_REQUEST['use']; else $lused=array();
	if (isset($_REQUEST['con'])) $lcons=$_REQUEST['con']; else $lcons=array();
	$shade=false;
	foreach ($query_modules as $module) {
		echo '	<TR bgcolor="'.($shade?$shadecolors[0]:end($shadecolors)).'">
				<TD valign=middle align=center>
					'.($i+1).'. 
					<INPUT type=checkbox name="use['.$i.']" value=true';
					if (isset($lused[$i]) && $lused[$i] && empty($_REQUEST['new'])) echo ' CHECKED';
					echo '>
				</TD>
				<TD>';
		if ($i != 0 && $module != "rand_subset") { 
			echo '		<SELECT name="con['.$i.']">
					<OPTION value="AND"';
						if (isset($lcons[$i]) && $lcons[$i] != "OR" && empty($_REQUEST['new'])) echo ' SELECTED'; echo '>'.$lang['and'].'</OPTION>
					<OPTION value="OR"';
                                                if (isset($lcons[$i]) && $lcons[$i] == "OR" && empty($_REQUEST['new'])) echo ' SELECTED'; echo '>'.$lang['or'].'</OPTION>
					</SELECT>';
					}
		echo '		</TD>
				<TD>';
					query__form_module($module,$experiment_id);
		echo '		</TD>
			</TR>';
		$i=$i+1;
		if ($shade) $shade=false; else $shade=true;
		}

	echo '		<TR>
				<TD colspan=3 align=right>
					<INPUT type=submit name="show" value="'.$lang['search_and_show'].'">
				</TD>
			</TR>
		</TABLE>';
}


function query__where_clause($query_modules,$use,$con,$experiment="") {
	$query__where_clause="";
	$first_where=true;
	$i=0;
	foreach ($query_modules as $module) {
		if (isset($use[$i]) && $use[$i]) {
			$current_where=query__where_clause_module($module,$experiment);
			if ($current_where) {
				$query__where_clause.=" ";
				if ($first_where) $query__where_clause.="AND ("; else $query__where_clause.=$con[$i];
				$query__where_clause.=" (".$current_where.") ";
				$first_where=false;
				}
			}
		$i=$i+1;
	}
	if($query__where_clause) $query__where_clause.=")";
	return $query__where_clause;
}

function query__join_assign($experiment,$query_modules,$use) {

	query_makecolumns(table('participate_at'),"payghost","TINYINT","0");
	$query__join_phrase="  LEFT JOIN ".table('participate_at')." ON
                               ".table('participants').".participant_id = ".table('participate_at').".participant_id
                               AND (".table('participate_at').".experiment_id ='".$experiment['experiment_id']."' ";
	$i=0;
	foreach ($query_modules as $module) {
		if (isset($use[$i]) && $use[$i]) $query__join_phrase.=query__join_assign_module($module);
		$i=$i+1;
        	}
	$suptwins=""; $twinstoexclude=query__get_participated_twins($experiment['experiment_id']);
	$twinstoexclude2=query__get_registered_twins_closed_sessions($experiment['experiment_id']);
	// var_dump($twinstoexclude); exit;
	if(!empty($twinstoexclude)) $suptwins=" AND ".table('participants').".participant_id NOT IN (".$twinstoexclude.")";
	if(!empty($twinstoexclude2)) $suptwins.=" AND ".table('participants').".participant_id NOT IN (".$twinstoexclude2.")";
	// var_dump($twinstoexclude2); exit;
	$query__join_phrase.=	") WHERE (".table('participate_at').".participant_id IS NULL OR (".table('participate_at').".payghost>0  AND ".table('participate_at').".participant_id NOT IN (SELECT participant_id FROM ".table('participate_at')." WHERE experiment_id ='".$experiment['experiment_id']."' AND payghost=0)))".$suptwins."";

	$query__join_phrase.=" AND ".table('participants').".subscriptions LIKE '%".
				$experiment['experiment_ext_type']."%'"; 
	$query__join_phrase.=" AND ".table('participants').".deleted='n'";
	if(!empty($experiment['gender_equality']) && $experiment['gender_equality']!="n") $query__join_phrase.=" AND ".table('participants').".gender<>'?'";
	return $query__join_phrase;
}

function query__orderlimit($query_modules,$use) {
        $query__orderlimit="ORDER BY lname, fname, participant_id";
		$i=0;
        foreach ($query_modules as $module) {
        	if (isset($use[$i]) && $use[$i]) {
        		$current_order=query__orderlimit_module($module);
              	if ($current_order) $query__orderlimit=$current_order;
           	}
		$i=$i+1;
        }
	return $query__orderlimit;
}

function query__experiment_byclasses_where($nonot=false,$postedvar='expclass',$unassigned_allowed=false)
{
               $class_posted=array(); $class_part=array(); $participants=array();
				// var_dump($_REQUEST[$postedvar]);
                $class_posted= (isset($_REQUEST[$postedvar]) && $_REQUEST[$postedvar]!=='') ? $_REQUEST[$postedvar] : array();
                if (is_array($class_posted) && count($class_posted) > 0) {
                                foreach ($class_posted as $class) if ($class!=='') $class_part[]=$class;
                        } else {
                                $class_part=array();
                                }
				if(!is_array($class_posted) && $class_posted!=="" && $class_posted!=="false" && $class_posted!==false && $class_posted!==null) $class_part[]=$class_posted;
				// var_dump($class_part);
                $wclause="";
                if (count($class_part) > 0) {
						$exp_extract=array();
						if($unassigned_allowed) $query="SELECT DISTINCT ".table('experiments').".experiment_class AS class, ".table('experiments').".experiment_id AS id
								FROM ".table('experiments')."
								";
						else $query="SELECT DISTINCT ".table('experiments').".experiment_class AS class, ".table('experiments').".experiment_id AS id
								FROM ".table('participate_at').", ".table('experiments')."
								WHERE participated='y' 
								AND ".table('participate_at').".experiment_id=".table('experiments').".experiment_id 
								";
						$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
						$first=true;
						while ($row=mysqli_fetch_assoc($result)) {
								$exp_extract[]=$row;
								}
						// var_dump($exp_extract); exit;
						$where_not="";
						if (!$nonot && !empty($_REQUEST['expclass_not'])) $where_not=$_REQUEST['expclass_not'];
                        $first=true;
                        foreach ($class_part as $class) {
                                if ($first==true) { $wclause.=" AND $where_not ( "; $first=false; } else $wclause.=" OR ";
								$exp_id_listarr=array();
								$n_exp=0;
								foreach($exp_extract as $exp)
								{
									$expclasses=explode(",",$exp['class']);
									if(in_array($class,$expclasses)) $exp_id_listarr[]=$exp['id'];
								}
                                // $wclause.="experiment_class = '".$class."'";
                                $wclause.="".table('experiments').".experiment_id IN ('".implode("','",$exp_id_listarr)."')";
                                $part_class[$class]=array();
                                }
                        if ($wclause) $wclause.=" )";
				}
				return $wclause;
}

function query__current_other_experiments_checkbox_list($experiment_id="",$formname="exp_ass") {
		global $lang, $settings, $color;

		$sort_order="time,experiment_name";

		$experiments=array(); $exp_ids=array();

		if (preg_match("/^(exp|exp_and)$/",$formname)) $add_wc=" AND participated='y' ";
			else $add_wc="";
		$wclause="";
		if(!empty($_REQUEST['show_exp_by_class_'.$formname])) $wclause=query__experiment_byclasses_where();
                $query="SELECT ".table('experiments').".*
                        FROM ".table('experiments').", ".table('participate_at')."
                        WHERE ".table('experiments').".experiment_id=".table('participate_at').".experiment_id
                        AND ".table('experiments').".experiment_id!='".$experiment_id."'
			".$wclause." 
			".$add_wc." 
                        GROUP BY experiment_id
                        ORDER BY experiment_id";	
                $result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
		while ($line=mysqli_fetch_assoc($result)) {
			$experiments[$line['experiment_id']]=$line;
			$experiments[$line['experiment_id']]['time']="";
			$exp_ids[]=$line['experiment_id'];
			}
		$exp_ids_string=implode("','",$exp_ids);

		// get session times
		$query="SELECT *,
			min(session_start_year*100000000 +
                	session_start_month*1000000 +
                	session_start_day*10000 +
                	session_start_hour*100 +
                	session_start_minute) as time
                        FROM ".table('sessions')."
                        WHERE experiment_id IN ('".$exp_ids_string."')
                        AND session_id>0
                        GROUP BY experiment_id
                        ORDER BY time";
                $result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
                while ($line=mysqli_fetch_assoc($result)) {
			$experiments[$line['experiment_id']]['time']=1000000000000-$line['time'];
			$experiments[$line['experiment_id']]['timearray']=time__load_session_time($line);
			}
		// var_dump(array_map(function($x){return $x['time'];},$experiments));
		multi_array_sort($experiments,$sort_order);
		// echo "<hr>";var_dump(array_map(function($x){return $x['time'];},$experiments));
		if(isset($_REQUEST[$formname]) && empty($_REQUEST['new'])) $posted=$_REQUEST[$formname]; else $posted="";
		if(!isset($_REQUEST['extended_'.$formname]) || !empty($_REQUEST['new'])) $_REQUEST['extended_'.$formname]="";
		$nr_experiments=count($experiments);

		if (isset($_REQUEST['restrict_'.$formname]) && $_REQUEST['restrict_'.$formname]) { $_REQUEST['extended_'.$formname]='';
							$_REQUEST['extend_'.$formname]='';
							$_REQUEST['restrict_'.$formname]='';
							$_SESSION['assign_request']['extended_'.$formname]='';
							$_SESSION['assign_request']['extend_'.$formname]='';
							$_SESSION['assign_request']['restrict_'.$formname]='';
							}
		if (isset($_REQUEST['extend_'.$formname]) && $_REQUEST['extend_'.$formname]) { $_REQUEST['extended_'.$formname]='true';
                                                        $_REQUEST['extend_'.$formname]='';
                                                        $_REQUEST['restrict_'.$formname]='';
                                                        $_SESSION['assign_request']['extended_'.$formname]='';
                                                        $_SESSION['assign_request']['extend_'.$formname]='';
                                                        $_SESSION['assign_request']['restrict_'.$formname]='';
                                                        }
		if ((!isset($_REQUEST['extended_'.$formname]) || !$_REQUEST['extended_'.$formname]) && empty($_REQUEST['show_exp_by_class_'.$formname])) {
			$i=0;
			foreach ($experiments as $key=>$value) {
				$i++;
				if ($i > $settings['query_number_exp_limited_view']) unset($experiments[$key]);
				}
			}
		// echo "<hr>";var_dump(array_map(function($x){return $x['time'];},$experiments));
                $i=0;

		if(isset($_REQUEST[$formname]) && empty($_REQUEST['new'])) $posted=$_REQUEST[$formname]; else $posted="";
		if(!isset($_REQUEST['extended_'.$formname])) $_REQUEST['extended_'.$formname]="";

		$cols=$settings['query_experiment_list_nr_columns'];
		$ccol=1;

		$shade=false;
		if(!empty($_REQUEST['show_exp_by_class_'.$formname])) echo "(<u><big>". count($experiments)."</big> experiments correspond to the class condition above</u>)";
		echo '<TABLE width=100% cellspacing=0 cellpadding=0><TR bgcolor="'.$color['list_shade1'].'">';
                foreach ($experiments as $exp) {
                        echo '<TD class="small">
							<INPUT class="small" type=checkbox id="id_'.$formname.'_'.$i.'" name="'.$formname.'['.$i.']" value="'.$exp['experiment_id'].'"';
			if (isset($posted[$i]) && $posted[$i]) echo " CHECKED";
			echo '><label style="display:inline;" for="id_'.$formname.'_'.$i.'">'.$exp['experiment_name'].' (';
			if ($exp['experimenter']) echo $exp['experimenter'].',';
			if ($exp['time']) {
				echo time__format($lang['lang'],$exp['timearray'],false,true,true,false); 
				// var_dump($exp['time']);
				}
			   else {
				echo '???';
				}
			echo ')';
			// var_dump($_REQUEST['extended_'.$formname],$experiment_id,$exp['experiment_id']);
			if((true || !empty($_REQUEST['show_exp_by_class_'.$formname]) || !empty($_REQUEST['extended_'.$formname])) && !empty($exp['experiment_id'])) 
						echo ', <a target=_blank href="experiment_edit.php?experiment_id='.$exp['experiment_id'].'">edit</a>';
			echo '
                                </label></TD>';
			if ($ccol==$cols) { 
				$ccol=1; 
				echo '</TR><TR bgcolor="';
				if ($shade==true) echo $color['list_shade1']; else echo $color['list_shade2'];
				echo '">'; 
				if ($shade==true) $shade=false; else $shade=true;
				} else $ccol=$ccol+1; 
                        $i=$i+1;
                        }
		if ($ccol>1) {
			while ($ccol <= $cols) {
				echo '<TD></TD>';
				$ccol=$ccol+1;
				}
			echo '</TR><TR>';
			}
		echo '<TD colspan='.$cols.' class="small" align=right>
			<INPUT type=hidden name="extended_'.$formname.'" value="'.$_REQUEST['extended_'.$formname].'">';
		if ($nr_experiments > $settings['query_number_exp_limited_view']) {
			if ($_REQUEST['extended_'.$formname])
				echo '<INPUT class="small" type=submit name="restrict_'.$formname.'" value="restrict list">';
			else
				echo '<INPUT class="small" type=submit name="extend_'.$formname.'" value="extend list">';
			}
		echo '</TD></TR></TABLE>';
}

function query__experiment_classes_checkbox_list($experiment_id="") {
                global $lang, $settings, $color;
				$formname="exp";
				if(!empty($_REQUEST['undo_exp_by_class_'.$formname])) {
																$_REQUEST['undo_exp_by_class_'.$formname]='';
																$_REQUEST['show_exp_by_class_'.$formname]='';
																$_SESSION['assign_request']['show_exp_by_class_'.$formname]='';
																$_SESSION['assign_request']['undo_exp_by_class_'.$formname]='';
																}
				if(!empty($_REQUEST['show_exp_by_class_'.$formname])) { //$_REQUEST['extended_'.$formname]='true';
																//$_REQUEST['show_exp_by_class_'.$formname]='';
																$_SESSION['assign_request']['show_exp_by_class_'.$formname]='';
																}
				$exp_classes_list=""; $exp_classes_exptable="";
                $query="SELECT experiment_class
                        FROM ".table('experiments')."
                        WHERE ".table('experiments').".experiment_id!='".$experiment_id."'
						";
                $result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
				$first=true;
                while ($row=mysqli_fetch_row($result)) {
						if($first) $first=false; else { $exp_classes_list.="','"; $exp_classes_exptable.="','";}
                        $exp_classes_list.=str_replace(",","','",$row[0]);
                        $exp_classes_exptable.=$row[0];
                        }
				$classes=array();
				// FROM ".table('lang').", ".table('experiments')."
                // AND ".table('experiments').".experiment_id!='".$experiment_id."'
                $query="SELECT ".table('lang').".*
                        FROM ".table('lang')."
                        WHERE ".table('lang').".content_name IN ('".$exp_classes_list."')
                        AND ".table('lang').".content_type='experimentclass' 
						GROUP BY content_name 
                        ORDER BY ".$lang['lang'];
                        //AND ".table('lang').".content_name!='0' 
                $result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
                while ($line=mysqli_fetch_assoc($result)) {
                        $classes[$line['content_name']]=$line;
                        }
						// var_dump($classes);

                $nr_classes=count($classes);
                $i=0;

                $cols=$settings['query_experiment_classes_list_nr_columns'];
                $ccol=1;

		if (isset($_REQUEST['expclass'])) $posted=$_REQUEST['expclass']; else $posted="";

                $shade=false;
                echo '<TABLE width=100% cellspacing=0 cellpadding=0><TR bgcolor="'.$color['list_shade1'].'">';
                foreach ($classes as $class) {
                        echo '<TD class="small"><div class="form-check">
                                <INPUT class="small form-check-input" type=checkbox id="expclass_'.$i.'" name="expclass['.$i.']" value="'.
					$class['content_name'].'"';
                        if (isset($posted[$i]) && $posted[$i]==$class['content_name']) echo " CHECKED";
                        echo '><label class="form-check-label" for="expclass_'.$i.'">'.$class[$lang['lang']];
                        echo '
                                </label></div></TD>';
                        if ($ccol==$cols) {
                                $ccol=1;
                                echo '</TR><TR bgcolor="';
                                if ($shade==true) echo $color['list_shade1']; else echo $color['list_shade2'];
                                echo '">';
                                if ($shade==true) $shade=false; else $shade=true;
                                } else $ccol=$ccol+1;
                        $i=$i+1;
                        }
                if ($ccol>1) {
                        while ($ccol <= $cols) {
                                echo '<TD></TD>';
                                $ccol=$ccol+1;
                                }
                        echo '</TR><TR>';
                        }
                echo '<TD colspan='.$cols.' class="small" align=right>';
				// echo '<INPUT type=hidden name="extended_'.$formname.'" value="true">';
				if (empty($_REQUEST['show_exp_by_class_'.$formname]))
					echo '<INPUT class="small" type=submit name="show_exp_by_class_'.$formname.'" value="show corresponding experiments below">';
				else
					echo '<INPUT class="small" type=submit name="undo_exp_by_class_'.$formname.'" value="show usual experiment list below">';
				echo '
						</TD></TR></TABLE>';
				// if (!empty($_REQUEST['show_exp_by_class_'.$formname]) && empty($_REQUEST['undo_exp_by_class_'.$formname])) unset($_REQUEST['show_exp_by_class_'.$formname]);
				// if (empty($_REQUEST['show_exp_by_class_'.$formname]) && !empty($_REQUEST['undo_exp_by_class_'.$formname])) unset($_REQUEST['undo_exp_by_class_'.$formname]);
}

function query__get_assigned_twins($experiment_id="",$session_id="",$participant_id="",$who="",$supsessionclause="")
{
	query_makecolumns(table('participants'),"twinto","TEXT NOT NULL");
	$twins=array();
	$wclause="";
	$noghost=true;
	if($noghost || !empty($experiment_id) || !empty($session_id) || !empty($participant_id) || !empty($who)) $wclause=" WHERE ";
	$wepty=true;
	if(!empty($experiment_id)) {if(!$wepty) {$wclause.=" AND ";} $wclause.="experiment_id='$experiment_id'"; $wepty=false;}
	if(!empty($session_id)) {if(!$wepty) {$wclause.=" AND ";} $wclause.="session_id='$session_id'"; $wepty=false;}
	if(!empty($participant_id)) {if(!$wepty) {$wclause.=" AND ";} $wclause.="participant_id='$participant_id'"; $wepty=false;}
	if(!$wepty) {$wclause.=" AND ";} { $wclause.="$who='y'"; $wepty=false;}
	if(!$wepty) {$wclause.=" AND ";} { $wclause.="payghost=0"; $wepty=false;}
	if(!empty($supsessionclause)) {
		if(!$wepty) {$wclause.=" AND ";} 
		$wclause.="session_id IN (SELECT session_id FROM ".table('sessions')." WHERE ".$supsessionclause.")"; $wepty=false;
	}
	$q="SELECT twinto FROM ".table('participants')." WHERE participant_id IN (SELECT participant_id FROM ".table('participate_at').$wclause.")";
	$lines=orsee_query($q,"return_first_elem");
	if($lines!==false) foreach($lines as $cval) {
		$cvalarr=explode(",",$cval);
		foreach($cvalarr as $cva) {
			$cv=trim($cva);
			if(!empty($cv) && !in_array($cv,$twins)) $twins[]=$cv;
		}
	};
	return implode(",",$twins);
}

function query__get_registered_twins($experiment_id="",$session_id="",$participant_id="")
{
	return query__get_assigned_twins($experiment_id,$session_id,$participant_id,"registered");
}

function query__get_registered_twins_closed_sessions($experiment_id="",$session_id="",$participant_id="")
{
	return query__get_assigned_twins($experiment_id,$session_id,$participant_id,"registered","session_finished='y'");
}

function query__get_participated_twins($experiment_id="",$session_id="",$participant_id="")
{
	return query__get_assigned_twins($experiment_id,$session_id,$participant_id,"participated");
}




function query_show_result($select_query,$sort="lname,fname",$type="edit",$shadecolors=array()) {
	global $lang, $color;
	
	if(empty($shadecolors)) $shadecolors=array($color['list_shade1'],$color['list_shade2']);
	elseif(!is_array($shadecolors)) $shadecolors=explode(',',$shadecolors);

	$allow_edit=check_allow('participants_edit');
	
	$searchtype='search';
	if($type=="editb") {$searchtype='searchb'; $type='edit';}

	$$type=true;
	$atypes=array('assign','drop','edit');
	foreach($atypes as $a) { if (!isset($$a)) $$a=false; }

	echo '  <P class="small">'.$lang['query'].': '.str_replace(",",", ",$select_query).'</P>';
	
	$result=mysqli_query($GLOBALS['mysqli'],$select_query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

        $shade=false; $participants=array();
        while ($line=mysqli_fetch_assoc($result)) {
                $participants[]=$line;
                }

        if ($sort) {
        	multi_array_sort($participants,$sort);
                }

	$count_results=count($participants);

	if ($assign) $columns=participant__load_result_table_fields($type='assign');
	elseif ($drop)  $columns=participant__load_result_table_fields($type='assignd');
	else $columns=participant__load_result_table_fields($type=$searchtype);
	
    echo ' 
                        <A HREF="'.thisdoc();
			if (isset($_REQUEST['experiment_id']) && $_REQUEST['experiment_id']) 
				echo "?experiment_id=".$_REQUEST['experiment_id'];
			echo '">'.$lang['new_query'].'</A>
                <BR><BR>
			'.$count_results.' '.$lang['xxx_participants_in_result_set'].'
                <BR><BR>';

    if(check_allow('participants_show')) {
		if ($assign) echo $lang['only_ny_assigned_part_showed'].'<BR>';
		if ($drop) echo $lang['only_assigned_part_ny_reg_shownup_part_showed'].'<BR>';

		echo ' <table border=0>
                        <TR>';
			if ($assign || $drop) echo '<TD></TD>';
                        headcell($lang['id'],"participant_id");
                        foreach($columns as $c) {
                        	if($c['allow_sort']) headcell($c['column_name'],$c['sort_order']);
                        	else headcell($c['column_name']);
                        }
                        headcell($lang['noshowup'],"number_noshowup,number_reg");
                        headcell($lang['rules'],"rules_signed,lname,fname");
			if ($drop) headcell ($lang['invited'],"invited");
                        if ($edit) echo '<TD></TD>';
                        echo '</TR>';
	}
	$i=0;
	$assign_ids=array(); 
        foreach ($participants as $p) {
			$assign_ids[]=$p['participant_id'];
			if(!check_allow('participants_show')) continue;
			$i=$i+1;
				echo '<tr class="small"';
						if ($shade) echo ' bgcolor="'.$shadecolors[0].'"';
								   else echo 'bgcolor="'.$shadecolors[1].'"';
					echo '>';
			if ($assign || $drop) echo '<td><INPUT type=checkbox name="p'.$i.'" 
							value="'.$p['participant_id'].'"></td>';
					echo '	<td class="small">'.$p['participant_id'].'</TD>';
					foreach($columns as $c) {
						echo '<td class="small">';
						if(isset($c['link_as_email_in_lists']) && $c['link_as_email_in_lists']=='y') echo '<A class="small" HREF="mailto:'.
							$p[$c['mysql_column_name']].'">';
						if(isset($c['type']) && preg_match("/(radioline|select_list|select_lang)/",$c['type']) && isset($c['lang'][$p[$c['mysql_column_name']]]))
							echo $c['lang'][$p[$c['mysql_column_name']]];
						else echo $p[$c['mysql_column_name']];
						if(isset($c['link_as_email_in_lists']) && $c['link_as_email_in_lists']=='y') '</A>';
						echo '</td>';
					}
					echo '
							<td class="small">'.$p['number_noshowup'].
													'/'.$p['number_reg'].'</td>
							<td class="small">'.$lang[$p['rules_signed']].'</td>';
			if ($drop) {
				echo '<TD class="small"';
				if ($p['invited']!='n') echo ' bgcolor="orange">'.$lang['yes'];
					else echo '>'.$lang['no'];
				echo '</TD>';
			}
			if ($edit || $allow_edit) {
				echo '<TD class="small">';
				if ($allow_edit) echo '
								<A HREF="participants_edit.php?participant_id='
													 .$p['participant_id'].'">
										  '.$lang['edit'].'</A>';
				echo 	'</TD>';
			}
			echo '
						 </tr>';
					if ($shade) $shade=false; else $shade=true;
        }

        if(check_allow('participants_show')) echo '  </table>';
		return $assign_ids;
}

function query_new_id($tablename, $varname="id")
{
           $gibtsschon=true;
           mt_srand (round((double)microtime()*1000000));
           while ($gibtsschon) {
                $crypt_id = "/";
			while (preg_match("/(\/|\.)/",$crypt_id)) {
                        $new_id = mt_rand();
                        $crypt_id=unix_crypt($new_id);
                        }

                $query="SELECT ".$varname." FROM ".$tablename."
                        WHERE ".$varname."=".$new_id;
                $line=orsee_query($query);
                if (isset($line[$varname])) $gibtsschon=true; else $gibtsschon=false;
                }
	return $new_id;
}

function query_makecolumns($tablename, $columns, $coltypes=array(), $defaults=array())
{
	if(!is_array($columns)) $columns=explode(",",$columns);
	if(!is_array($coltypes)) $coltypes=explode(",",$coltypes);
	if(!is_array($defaults)) $defaults=explode(",",$defaults);
	foreach($columns as $key=>$cname) {
		$ctype=(isset($coltypes[$key]))?$coltypes[$key]:"varchar(255)";
		if(isset($defaults[$key]) && $defaults[$key] === "") $defaults[$key] = "''";
		$cdefault=(isset($defaults[$key]))?" DEFAULT ".$defaults[$key]:"";
		// $query0="SELECT `$cname` FROM ".$tablename;
		// $result0=mysqli_query($GLOBALS['mysqli'],$query0);
		// if(!$result0) {
		if(!query_column_exist($tablename,$cname)) {
			$nnd=($cdefault!=="")?"NOT NULL".$cdefault:"";
			if(strtolower(trim($defaults[$key]))==="null") $nnd="";
			$query="ALTER TABLE ".$tablename." ADD `$cname` $ctype ".$nnd;
			$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli'])." query: ".$query);
		}
	}
}

function query_column_exist($tablenames, $columns, $or_condition=false)
{
	global $site__database_database;
	if(is_string($or_condition)) $or_condition=(strtoupper($or_condition)=="OR");
	if(!is_array($columns)) $columns=explode(",",$columns);
	if(!is_array($tablenames)) $tablenames=explode(",",$tablenames);
	$boolret=!$or_condition;
	foreach($tablenames as $tablename) {
		$query="SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS 
				WHERE table_name='".$tablename."' 
				AND table_schema = '".$site__database_database."'";
		$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$query);
		if(mysqli_num_rows($result)==0) {$boolret=($or_condition && $boolret); continue;}
		$existing_cols=array();
		while ($line=mysqli_fetch_assoc($result)) $existing_cols[]=$line['COLUMN_NAME'];
		// var_dump($existing_cols);
		mysqli_free_result($result);
		foreach($columns as $cname) {
			if(in_array($cname,$existing_cols)) $boolret=($or_condition || $boolret);
			else $boolret=($or_condition && $boolret);
			// var_dump($cname,$boolret);
		}
	}
	return $boolret;
		
}

?>

<?php
// part of orsee. see orsee.org


function table($table) {
	global $site__database_table_prefix;
	$tablename=$site__database_table_prefix.$table;
	return $tablename;
}

// very convenient functions translated from metahtml

function orsee_query($query,$funcname="") {
	
	static $backiter=0;

	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$query." <hr> ".lang('technical_information').":<br>".print_r(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS),true));
	if (!empty($funcname) && $result!==false && mysqli_num_rows($result)>0) {
		$back=array();
        	while ($line = mysqli_fetch_assoc($result)) {
					$backiter++;
					$funcres=$funcname($line);
					// echo "\n <!-- "; echo " backiter="; echo $backiter; echo ", back function is :"; echo $funcname; echo ", funcres is : "; echo $funcres; echo ", query is : "; echo preg_replace("/\n|\r/"," ",$query); echo " --> ";
                	$back[]=$funcres;
                	}
        	mysqli_free_result($result);
		return $back;

         } elseif($result!==false && $result!==true && mysqli_num_rows($result)>0) {
        	$line=mysqli_fetch_assoc($result);
        	mysqli_free_result($result);
        	return $line;
        	}
			else return false;
}

function orsee_db_load_array($table,$key,$keyname,$what="*") {
		if($key === null) return false;
		// var_dump($table,$key,$keyname);
        $query="SELECT ".$what." FROM ".table($table)." where ".$keyname."='".mysqli_real_escape_string($GLOBALS['mysqli'],$key)."'";
        $line=orsee_query($query);
        return $line;
}

function rasee_db_load_array($table,$keynameval,$what="*") {
	
        $query="SELECT ".$what." FROM ".table($table)." where ";
		$n_w=0;
		foreach($keynameval as $keyname=>$key) {
			if($n_w>0) $query.=" AND ";
			$query.=$keyname."='".mysqli_real_escape_string($GLOBALS['mysqli'],$key)."'";
			$n_w++;
		}
        $line=orsee_query($query);
        return $line;
}
function rasee_db_load_full_array($table,$keynameval,$what="*",$sign="=") {
	
        $query="SELECT ".$what." FROM ".table($table)." where ";
		$n_w=0;
		foreach($keynameval as $keyname=>$key) {
			if($n_w>0) $query.=" AND ";
			$query.=$keyname.$sign."'".mysqli_real_escape_string($GLOBALS['mysqli'],$key)."'";
			$n_w++;
		}
        $lines=orsee_query($query,($what=="*" || count(explode(",",$what))>1)?"return_same":"return_first_elem");
        return $lines;
}

function rasee_db_load_value($table,$key,$keyname,$what) {
        $query="SELECT ".$what." FROM ".table($table)." where ".$keyname."='".mysqli_real_escape_string($GLOBALS['mysqli'],$key)."'";
        $line=orsee_query($query);
		if($line===false) return false;
        return $line[$what];
}

function orsee_db_load_full_array($table,$key,$keyname) {
        $query="SELECT * FROM ".table($table)." where ".$keyname."='".mysqli_real_escape_string($GLOBALS['mysqli'],$key)."'";
        $lines=orsee_query($query,"return_same");
        return $lines;
}

function orsee_db_load_full_array_like($table,$key,$keyname) {
        $query="SELECT * FROM ".table($table)." where ".$keyname." LIKE '%".mysqli_real_escape_string($GLOBALS['mysqli'],$key)."%'";
        $lines=orsee_query($query,"return_same");
        return $lines;
}

function rasee_db_register_new_twins($table,$key,$prevval,$newval)
{
	$aprev=(empty($prevval))?array():explode(",",$prevval); $anew=(empty($newval))?array():explode(",",$newval);
	$toadd=array(); $todelete=array();
	foreach($anew as $an) if(!in_array($an,$aprev)) $toadd[]=$an;
	foreach($aprev as $ap) if(!in_array($ap,$anew)) $todelete[]=$ap;
	$mess="";
	$newtwins=array();
	foreach($toadd as $td) {
		$addres=rasee_db_add_twin($table,trim($td),$key);
		if($addres[0]!==false) $newtwins[]=$td;
		$mess.=$addres[1];
	}
	foreach($todelete as $td) {
		$addres=rasee_db_delete_twin($table,trim($td),$key);
		$mess.=$addres[1];
	}
	foreach($newtwins as $td) {
		$addres=rasee_db_add_twin($table,$key,trim($td));
		$mess.=$addres[1];
	}
	foreach($todelete as $td) {
		$addres=rasee_db_delete_twin($table,$key,trim($td));
		$mess.=$addres[1];
	}
	// echo "Message:".$mess; exit;
	if(!empty($mess)) message($mess);
}

function rasee_db_add_twin($table,$pid,$twin)
{
	$prevval=rasee_db_load_value($table,$pid,"participant_id","twinto");
	if($prevval===false) return array(false,"participant_id '".$pid."' ".lang("not_found")."<br>");
	if(trim($pid)==trim($twin)) return array(false,lang("twin's_id_shoud_be_different_from_subjects's_id<br>"));
	$mess="";
	$aprev=explode(",",$prevval);
	if(trim($prevval)=="") $aprev=array();
	$aprev[]=trim($twin);
	$q="UPDATE ".table($table)." SET twinto='".implode(",",$aprev)."' WHERE participant_id='".mysqli_real_escape_string($GLOBALS['mysqli'],$pid)."'";
	$result=mysqli_query($GLOBALS['mysqli'],$q) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$q);
	$nrowsaffected=mysqli_affected_rows($GLOBALS['mysqli']);
	if($nrowsaffected!=1) $mess.="$nrowsaffected ".lang('rows affected')." for participant_id ".$pid."<br>";
	return array(($nrowsaffected>0),$mess);
}

function rasee_db_delete_twin($table,$pid,$twin)
{
	$prevval=rasee_db_load_value($table,$pid,"participant_id","twinto");
	if($prevval===false) return array(false,"participant_id '".$pid."' ".lang("not_found")."<br>");
	$mess="";
	$aprev=explode(",",$prevval);
	if(!in_array($twin,$aprev)) $mess.=lang("twin")." ".$twin." ".lang("was_not_registered_for",false)." participant_id ".$pid."!<br>";
	$newtwins=array();
	foreach($aprev as $ap) if(trim($ap)!=trim($twin) && trim($ap)!="") $newtwins[]=trim($ap);
	$q="UPDATE ".table($table)." SET twinto='".implode(",",$newtwins)."' WHERE participant_id='".mysqli_real_escape_string($GLOBALS['mysqli'],$pid)."'";
	$result=mysqli_query($GLOBALS['mysqli'],$q) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$q);
	$nrowsaffected=mysqli_affected_rows($GLOBALS['mysqli']);
	if($nrowsaffected!=1) $mess.="$nrowsaffected ".lang('rows affected')." for participant_id ".$pid."<br>";
	return array(($nrowsaffected>0),$mess);
}

function orsee_db_save_array($array,$table,$key,$keyname,$ignore_empty=false,$set_defaults=false) {
	global $site__database_database;
	// var_dump($array);

	// find out which fields i can save
	$query="SELECT COLUMN_NAME,COLUMN_DEFAULT FROM INFORMATION_SCHEMA.COLUMNS 
			WHERE table_name='".table($table)."' 
			AND table_schema = '".$site__database_database."'";
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$query);
    while ($line = mysqli_fetch_assoc($result)) {
    	$columns[]=$line['COLUMN_NAME'];
		$defaults[$line['COLUMN_NAME']]=$line['COLUMN_DEFAULT'];
    }
    /*
	$fields = mysql_list_fields($site__database_database,table($table));
	$column_count = mysql_num_fields($fields);
	for ($i = 0; $i < $column_count; $i++) {
    		$columns[]=mysql_field_name($fields, $i);
	}
	*/

	// delete key
	if (isset($array[$keyname])) unset($array[$keyname]);
	$arraykeys=array_keys($array);
	// var_dump($arraykeys,$ignore_empty); exit;
	$fields_to_save=array_intersect($arraykeys,$columns);
	// build set phrase
	$first=true; $set_phrase="";
	foreach ($fields_to_save as $field) {
		if ($first) $first=false; elseif($field!='twinto' && isset($array[$field]) && (!$ignore_empty || !empty($array[$field]))) $set_phrase=$set_phrase.", ";
		if(is_array($array[$field])) $array[$field]=implode(",",$array[$field]);
		if($field!='twinto' && isset($array[$field]) && (!$ignore_empty || !empty($array[$field]))) $set_phrase=$set_phrase.$field."='".mysqli_real_escape_string($GLOBALS['mysqli'],$array[$field])."'";
		elseif($keyname=="participant_id" && isset($array[$field])) {
			$prevval=rasee_db_load_value($table,$key,$keyname,'twinto');
			if($prevval!==false && trim($prevval)!=trim($array[$field])) rasee_db_register_new_twins($table,$key,$prevval,$array[$field]);
		}
		elseif(!isset($array[$field]) && $set_defaults && isset($defaults[$field])) $set_phrase=$set_phrase.($first?"":", ").$field."=".$defaults[$field]."";
		// echo'<hr>field='.$field.'|$array[$field]='.$array[$field].' | isset(array[field])= '; var_dump(isset($array[$field]));echo ', default:'; var_dump($defaults[$field]); echo ', adding to set_phrase: '; var_dump(($field!='twinto' && isset($array[$field]) && (!$ignore_empty || !empty($array[$field]))));
		}
		// echo'<hr>set_phrase=` '.$set_phrase.' `'; exit;

	// check if already saved
	$query="SELECT ".$keyname." FROM ".table($table)." WHERE ".$keyname."='".mysqli_real_escape_string($GLOBALS['mysqli'],$key)."'";
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$query);
	$num_rows = mysqli_num_rows($result);

	if ($num_rows>0) {
		// update query
        	$query="UPDATE ".table($table)." SET ".$set_phrase." WHERE ".$keyname."='".mysqli_real_escape_string($GLOBALS['mysqli'],$key)."'";
         } else {
		// insert query
        	$query="INSERT INTO ".table($table)." SET ".$keyname."='".mysqli_real_escape_string($GLOBALS['mysqli'],$key)."', ".$set_phrase;
        }
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$query);
	// if($table=="lang") {var_dump($num_rows); var_dump($query); var_dump($result); exit;}
	return $result;
}

function rasee_db_save_array($array,$table,$keynameval,$ignore_empty=false,$set_defaults=false) {
	global $site__database_database;
	// var_dump($array);

	// find out which fields i can save
	$query="SELECT COLUMN_NAME,COLUMN_DEFAULT FROM INFORMATION_SCHEMA.COLUMNS 
			WHERE table_name='".table($table)."' 
			AND table_schema = '".$site__database_database."'";
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$query);
    while ($line = mysqli_fetch_assoc($result)) {
    	$columns[]=$line['COLUMN_NAME'];
		$defaults[$line['COLUMN_NAME']]=$line['COLUMN_DEFAULT'];
    }

	// delete key
	foreach($keynameval as $keyname=>$key) {
		if (isset($array[$keyname])) unset($array[$keyname]);
	}
	$arraykeys=array_keys($array);
	// var_dump($arraykeys);
	$fields_to_save=array_intersect($arraykeys,$columns);
	// build set phrase
	$first=true; $set_phrase="";
	foreach ($fields_to_save as $field) {
		if ($first) $first=false; elseif($field!='twinto' && isset($array[$field]) && (!$ignore_empty || !empty($array[$field]))) $set_phrase=$set_phrase.", ";
		if(is_array($array[$field])) $array[$field]=implode(",",$array[$field]);
		if($field!='twinto' && isset($array[$field]) && (!$ignore_empty || !empty($array[$field]))) $set_phrase=$set_phrase.$field."='".mysqli_real_escape_string($GLOBALS['mysqli'],$array[$field])."'";
		elseif($keyname=="participant_id" && isset($array[$field])) {
			$prevval=rasee_db_load_value($table,$key,$keyname,'twinto');
			if($prevval!==false && trim($prevval)!=trim($array[$field])) rasee_db_register_new_twins($table,$key,$prevval,$array[$field]);
		}
		elseif(!isset($array[$field]) && $set_defaults && isset($defaults[$field])) $set_phrase=$set_phrase.($first?"":", ").$field."=".$defaults[$field]."";
		// echo'<hr>field='.$field.'|$array[$field]='.$array[$field].' | isset(array[field])= '; var_dump(isset($array[$field]));echo ', default:'; var_dump($defaults[$field]); echo ', adding to set_phrase: '; var_dump(($field!='twinto' && isset($array[$field]) && (!$ignore_empty || !empty($array[$field]))));
		}
		// echo'<hr>set_phrase=` '.$set_phrase.' `'; exit;

	// check if already saved
	$qwhere="";
	$n_w=0;
	foreach($keynameval as $keyname=>$key) {
		if($n_w>0) $qwhere.=" AND ";
		$qwhere.=$keyname."='".mysqli_real_escape_string($GLOBALS['mysqli'],$key)."'";
		$n_w++;
	}
	$query="SELECT ".$keyname." FROM ".table($table)." WHERE ".$qwhere;
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$query);
	$num_rows = mysqli_num_rows($result);

	if ($num_rows>0) {
		// update query
        	$query="UPDATE ".table($table)." SET ".$set_phrase." WHERE ".$qwhere;
    } 
	else {
		// insert query
		$qset="";
		$n_s=0;
		foreach($keynameval as $keyname=>$key) {
			if($n_s>0) $qset.=", ";
			$qset.=$keyname."='".mysqli_real_escape_string($GLOBALS['mysqli'],$key)."'";
			$n_s++;
		}
		$query="INSERT INTO ".table($table)." SET ".$qset.", ".$set_phrase;
	}
	// var_dump($query);
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$query);
	// if($table=="lang") {var_dump($num_rows); var_dump($query); var_dump($result); exit;}
	return $result;
}

function dump_array($array,$lb="<BR>") {
	global $lang;

	echo '<TABLE border=0>';
	foreach ($array as $key => $value) {
		echo '<TR><TD align=right>';
		if (isset($lang[$key])) echo $lang[$key]; else echo stripslashes(isset($key)?$key:'');
		echo ':</TD><TD>&nbsp;</TD><TD align=left>';
		if (isset($lang[$value])) echo $lang[$value]; else echo stripslashes(isset($value)?$value:'');
		echo "</TD></TR>\n";
		}
	echo '</TABLE>';
}

?>

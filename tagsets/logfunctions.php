<?php
// part of orsee. see orsee.org


function log__participant($action,$participant_id,$target="",$return_logid=false) {
	$darr=getdate();	
	query_makecolumns(table('participants_log'),"ipaddr","varchar(255)","''");
	$query="INSERT INTO ".table('participants_log')." SET id='".mysqli_real_escape_string($GLOBALS['mysqli'],$participant_id)."',
		year='".$darr['year']."', 
		month='".$darr['mon']."', 
		day='".$darr['mday']."', 
		action='".$action."',
		target='".mysqli_real_escape_string($GLOBALS['mysqli'],$target)."',
		timestamp='".$darr[0]."',
		ipaddr='".get_client_ip()."'";
	$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']) . ", query : $query");
	if($done && $return_logid) return array("log_id"=>mysqli_insert_id($GLOBALS['mysqli']),"action"=>$action, "target"=>$target, "id"=>$participant_id,"timestamp"=>$darr[0], "year"=>$darr['year'], "month"=>$darr['mon'], "day"=>$darr['mday']);
}

function log__get_participant_index($participant_id,$action,$experiment_id='',$session_id='') {
	$crf="SELECT id FROM ".table('participants_log')." WHERE action='$action'";
	if(!empty($experiment_id) && !empty($session_id)) $crf.=" AND target='".mysqli_real_escape_string($GLOBALS['mysqli'],"experiment_id:".$experiment_id."\nsession_id:".$session_id)."' ORDER BY log_id";
	elseif(!empty($experiment_id)) $crf.=" AND target LIKE '".mysqli_real_escape_string($GLOBALS['mysqli'],"experiment_id:".$experiment_id."\n%")."' ORDER BY log_id";
	elseif(!empty($session_id)) $crf.=" AND target LIKE '".mysqli_real_escape_string($GLOBALS['mysqli'],"%\nsession_id:".$session_id."")."' ORDER BY log_id";
	$pidscrf=orsee_query($crf,"return_first_elem");
	$mycrfindex=-1;
	if($pidscrf!==false) foreach(array_values(array_unique($pidscrf)) as $pk=>$cpid) if($cpid==$participant_id) $mycrfindex=$pk;
	return $mycrfindex;
}

function log__get_participant_targets($pid,$action,$what="",$targetlikecondition="") {
	$crf="SELECT * FROM ".table('participants_log')." WHERE action='$action' AND `id`='".$pid."'";
	if(!empty($targetlikecondition)) {
		$crf.=" AND target LIKE '%".$targetlikecondition."%'";
	}
	$crf.=" ORDER BY timestamp DESC";
	$pidcrf=orsee_query($crf);
	$targets=array();
	if($pidcrf!==false) {
		$atarget=explode("\n",$pidcrf['target']);
		foreach($atarget as $stinfo) {
			// echo "\nstinfo:$stinfo\n";
			$atinfo=explode(":",$stinfo);
			if(count($atinfo)>1) {
				$targets[trim($atinfo[0])]=trim($atinfo[1]);
			}
		}
	}
	if(!empty($what)) {
		if(!empty($targets[$what])) return $targets[$what];
		else return false;
	}
	if(empty($targets) && !empty($pidcrf['target'])) return $pidcrf['target'];
	return $targets;
}

function log__admin($action="unknown",$target="",$ipaddr="",$id="") {
	global $expadmindata;
	if(empty($id) && !empty($expadmindata['admin_id'])) $id=$expadmindata['admin_id'];
        $darr=getdate();

        $query="INSERT INTO ".table('admin_log')." SET id='".mysqli_real_escape_string($GLOBALS['mysqli'],$id)."',
                year='".$darr['year']."',
                month='".$darr['mon']."',
                day='".$darr['mday']."',
                action='".$action."',
		target='".mysqli_real_escape_string($GLOBALS['mysqli'],$target)."',
                timestamp='".$darr[0]."'";
		if(!empty($ipaddr)) {
			query_makecolumns(table('admin_log'),"ipaddr","varchar(255)","''");
			$query.=", ipaddr='".get_client_ip()."'";
		}
        $done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
		// var_dump($query,$id); exit;
}

function log__cron_job($action="unknown",$target="",$now="",$id="") {
	if ($now=="") $now=time();
        $darr=getdate($now);

        $query="INSERT INTO ".table('cron_log')." 
		SET id='".$id."',
                year='".$darr['year']."',
                month='".$darr['mon']."',
                day='".$darr['mday']."',
                action='".$action."',
                target='".mysqli_real_escape_string($GLOBALS['mysqli'],$target)."',
                timestamp='".$now."'";
        $done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
	return $done;
}


function log__link() {
	$post=$_REQUEST;
	unset($post['SID']); unset ($post['PHPSESSID']);
	$arg_list=func_get_args();
	foreach ($arg_list as $arg) {
		$var=explode("=",$arg);
		$post[$var[0]]=$var[1];
		}
	$link='<A HREF="'.thisdoc().'?';
	foreach ($post as $key=>$value) {
		$link.=$key.'='.urlencode($value).'&';
		}
	$link.='">';
	return $link;
}

function log__restrict_link($varname,$varvalue) {
	global $lang;
	$link=log__link($varname.'='.$varvalue,'os=0');
	$link.='<FONT class="small">['.$lang['restrict'].']</FONT></A>';
	return $link;
}

function log__show_log($log) {
	global $limit;
	if (!$limit) {
		$limit=50;
	}
	if (isset($_REQUEST['limit']) && is_numeric($_REQUEST['limit'])) $limit=$_REQUEST['limit'];
	if (isset($_REQUEST['os']) && $_REQUEST['os']>0) $offset=$_REQUEST['os']; else $offset=0;

	global $lang, $color;


	if (isset($_REQUEST['action']) && $_REQUEST['action']) $aquery=" AND action='".$_REQUEST['action']."' ";
			else $aquery="";

	if (isset($_REQUEST['id']) && $_REQUEST['id']) $idquery=" AND id='".$_REQUEST['id']."' ";
			else $idquery="";

	if (isset($_REQUEST['target']) && $_REQUEST['target']) $tquery=" AND target LIKE '%".$_REQUEST['target']."%' ";
                        else $tquery="";

	$logtable=table('participants_log');
	switch ($log) {
		case "participant_actions":
			$logtable=table('participants_log');
			$secondtable=" LEFT JOIN ".table('participants')." ON id=participant_id ";
			break;
		case "experimenter_actions":
			$logtable=table('admin_log');
			$secondtable=" LEFT JOIN ".table('admin')." ON id=admin_id ";
			break;
		case "regular_tasks":
			$logtable=table('cron_log');
			$secondtable=" LEFT JOIN ".table('admin')." ON id=admin_id ";
			break;
		}

	if (isset($_REQUEST['delete']) && $_REQUEST['delete'] && isset($_REQUEST['days']) && $_REQUEST['days']) {

		$allow=check_allow('log_file_'.$log.'_delete','statistics_show_log.php?log='.$log);

		if (isset($_REQUEST['days']) && $_REQUEST['days']=="all") $where_clause="";
		   else {
                	$now=time();
                	$dsec= (int) $_REQUEST['days']*24*60*60;
                	$dtime=$now-$dsec;
			$where_clause=" WHERE timestamp < ".$dtime;
			}
                $query="DELETE FROM ".$logtable.$where_clause;
		$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
		$number=mysqli_affected_rows($GLOBALS['mysqli']);
		message ($number.' '.$lang['xxx_log_entries_deleted']);
		if ($number>0) log__admin("log_delete_entries","log:".$log."\ndays:".$_REQUEST['days']);
		redirect ($GLOBALS['settings__admin_folder']."/statistics_show_log.php?log=".$log);
                }



	$query="SELECT * FROM ".$logtable.$secondtable."
		WHERE id IS NOT NULL ".
		$aquery.$idquery.$tquery.
		" ORDER BY timestamp DESC 
		";
	if($limit>0) $query.="LIMIT ".$offset.",".$limit;

	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

	$num_rows=mysqli_num_rows($result);

	echo '<TABLE width=80% border=0>
		<TR><TD width=50%>';
	echo '<FONT class="small">'.$lang['query'].': '.$query.'</FONT><BR><BR>';
	echo '</TD>
		<TD align=right width=50%>';
	
	if (check_allow('log_file_'.$log.'_delete')) {
		echo '
			<FORM action="statistics_show_log.php">
			<INPUT type=hidden name="log" value="'.$log.'">
			'.$lang['delete_log_entries_older_than'].'
			<select name="days">
			<option value="all">'.$lang['all_entries'].'</option>';
			$ddays=array(1,7,30,90,180,360);
			if (isset($_REQUEST['days']) && $_REQUEST['days']) $selected=$_REQUEST['days']; else $selected=90;
			foreach ($ddays as $day) {
				echo '<option value="'.$day.'"';
				if ($day==$selected) echo ' SELECTED';
				echo '>'.$day.' ';
				if ($day==1) echo $lang['day']; else echo $lang['days'];
				echo '</option>
					';
				}
			echo '	</select><input type=submit name="delete" value="'.$lang['delete'].'">';
		}
	echo '</TD></TR></TABLE>';
	
	if($limit>0) {
		if ($offset > 0) echo '['.log__link('os='.($offset-$limit)).$lang['previous'].'</A>]';
				else echo '['.$lang['previous'].']';
		echo '&nbsp;&nbsp;';
		if ($num_rows >= $limit) echo '['.log__link('os='.($offset+$limit)).$lang['next'].'</A>]';
				else echo '['.$lang['next'].']';
	}
	echo '<TABLE width=90% border=1>';

	// header
	echo '<TR bgcolor="'.$color['list_header_background'].'">
		<TD>
			'.$lang['date_and_time'].'
		</TD>
		<TD>';
			if ($log=='participant_actions') {
				echo $lang['lastname'].', '.$lang['firstname'];
				}
			elseif ($log=='experimenter_actions' || $log=='regular_tasks') {
				echo $lang['experimenter'];
				}
			if (isset($_REQUEST['id']) && $_REQUEST['id']) 
				echo ' '.log__link('id=','os=0','limit=').'<FONT class="small">['.$lang['unrestrict'].']</FONT></A>';
	echo '	</TD>
		<TD>
			'.$lang['action'];
			if (isset($_REQUEST['action']) && $_REQUEST['action'])
                                echo ' '.log__link('action=','os=0','limit=').'<FONT class="small">['.$lang['unrestrict'].']</FONT></A>';
	echo '	</TD>
		<TD>
			'.$lang['target'];
			if (isset($_REQUEST['target']) && $_REQUEST['target'])
                                echo ' '.log__link('target=','os=0','limit=').'<FONT class="small">['.$lang['unrestrict'].']</FONT></A>';
	echo '	</TD>
	      </TR>
		';

	while ($line=mysqli_fetch_assoc($result)) {

		$target=stripslashes($line['target']);
		if(preg_match("/^experiment_id:(\d+)\nsession_id:(\d+)/",$target,$tmatches)) {
			$tsession=orsee_query("SELECT * FROM ".table('sessions')." WHERE session_id='".$tmatches[2]."'");
			$sesstext='<a style="color:black" href="experiment_participants_show.php?focus=registered&experiment_id='.
							$tmatches[1].'&session_id='.$tmatches[2].'">'.
                					session__build_name($tsession).'</a>';
			// $sesstext.=' <A style="color:black" HREF="session_edit.php?session_id='.$tmatches[2].'"><FONT class="small">['.$lang['edit'].']</FONT></A> ';
			$sesstext.=' (<A style="color:black" HREF="experiment_show.php?experiment_id='.$tmatches[1].'">'.experiment__get_public_name($tmatches[1]).'</A>)';
			$target=str_replace("experiment_id:".$tmatches[1]."\nsession_id:".$tmatches[2],$sesstext,$target);
		}
		echo '<TR>
			<TD>
				'.time__format($lang['lang'],'',false,false,false,false,$line['timestamp']).'
			</TD>
			<TD>';
				if ($log=='participant_actions') {
				   if ($line['participant_id']) {
					echo $line['lname'].', '.$line['fname'].
					' <A HREF="participants_edit.php?participant_id='.
					$line['participant_id'].'"><FONT class="small">['.$lang['edit'].']</FONT></A>';
					}
				   else echo $line['id'];
				   }
				elseif ($log=='experimenter_actions' || $log=='regular_tasks') {
					echo $line['adminname'];
					}
				if (!isset($_REQUEST['id']) || $_REQUEST['id']!=$line['id']) echo ' '.log__restrict_link('id',$line['id']);
		echo '	</TD>
			<TD>
				'.$line['action'];
				if (!isset($_REQUEST['action']) || $_REQUEST['action']!=$line['action']) 
					echo ' '.log__restrict_link('action',$line['action']);
		echo '	</TD>
			<TD>
                                '.nl2br(stripslashes($target));
				if (!isset($_REQUEST['target']) || $_REQUEST['target']!=$line['target'] && $log!='regular_tasks')
                                        echo ' '.log__restrict_link('target',$line['target']);
                echo '	</TD>
		      </TR>';
		}

	echo '</TABLE>';
	return $num_rows;
}

?>

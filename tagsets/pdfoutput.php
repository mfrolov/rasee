<?php

// pdf output functions. part of orsee. see orsee.org.

function pdfoutput__make_part_list($experiment_id,$session_id,$focus,$sort="",$file=false,$tlang="") {
	global $settings;
	$show_rules_signed=false;
	if ($tlang=="") {
		global $lang;
		}
	   else {
		$lang=load_language($tlang);
		}

	if ($experiment_id) $experiment=orsee_db_load_array("experiments",$experiment_id,"experiment_id");
		else $experiment=array();

	if ($session_id) $session=orsee_db_load_array("sessions",$session_id,"session_id");
		else $session=array();

	if ($sort) $order=$sort;
                else $order="session_start_year, session_start_month, session_start_day,
                        session_start_hour, session_start_minute, lname, fname, email";

		$focuses=array('assigned','invited','registered','shownup','participated','nonparticipated','notpresent','absent','absentold','absentnew','todayabsent','new','todaystarted','atdate','athours','absentatdate','atmonth','today','todaynew','tomorrow','todayopen','todayopenshownup','todayshownup');
		foreach($focuses as $f) { $$f=false; }
		$arr_focus=explode(":",$focus);
		$focus2part="";
		if(count($arr_focus)>1) {
			$focus=strtolower($arr_focus[0]);
			$focus2part=$arr_focus[1];
			if($focus=="athour") $focus="athours";
			if($focus=="atdates") $focus="atdate";
			if($focus=="absentatdates") $focus="absentatdate";
		}
        if (!$focus) $focus="assigned";
        $$focus=true;
		
		$supsess_criteria="";
        switch ($focus) {
                case "assigned":        $where_clause="AND registered='n'";
                                        $title=$lang['assigned_subjects_not_yet_registered'];
                                        break;
                case "invited":         $where_clause="AND invited='y' AND registered='n'";
                                        $title=$lang['invited_subjects_not_yet_registered'];
                                        break;
                case "registered":      $where_clause="AND registered='y'";
                                        $title=$lang['registered_subjects'];
                                        break;
                case "shownup":         $where_clause="AND shownup='y'";
                                        $title=$lang['shownup_subjects'];
                                        break;
                case "participated":    $where_clause="AND participated='y'";
                                        $title=$lang['subjects_participated'];
                                        break;
                case "nonparticipated":    $where_clause="AND registered='y' AND participated='n'";
                                        $title=lang('nonparticipated');
                                        break;
				case "notpresent":						
                case "absent":    $where_clause="AND registered='y' AND shownup='n' AND participated='n'";
                                        $title=lang('absent_subjects');
                                        break;
                case "absentold":    $where_clause="AND registered='y' AND shownup='n' AND participated='n' AND session_finished='y'";
                                        $title=lang('absent_subjects_in_closed_sessions');
                                        break;
                case "absentnew":    $where_clause="AND registered='y' AND shownup='n' AND participated='n' AND session_finished='n'";
                                        $title=lang('absent_subjects_in_opened_sessions');
                                        break;
                case "new":    $where_clause="AND registered='y' AND shownup='n' AND participated='n' AND session_finished='n'";
                                        $title=lang('subjects_new');
                                        break;
                case "todaystarted":    
										$where_clause="AND registered='y' AND session_start_year='".date('Y')."' AND session_start_month='".date('n')."' AND session_start_day='".date('j')."' AND CAST(session_start_hour AS UNSIGNED)<=".date('G')."";
                                        $title=lang('subjects').' '.date('d/m/Y').' '.lang('before',false).' '.date('G').lang('h',false);
                                        break;
                case "athours":    
										$cdate=empty($_REQUEST['date'])?date('H').".".date('j').".".date('n').".".date('Y'):$_REQUEST['date'];
										if(!empty($focus2part)) $cdate=$focus2part;
										$cdates=explode('-',$cdate);
										$where_clause="AND registered='y' ";
										$datetoshow="";
										for($i=0; $i<count($cdates); $i++)
										{
											$scdate=preg_replace("/\/|h\s*/",".",$cdates[$i],1);
											$acdate=explode('.',$scdate);
											// if(count($acdate)<2) $acdate=explode('/',$cdates[$i]);
											if(count($acdate)<2 || empty($acdate[1])) $acdate[1]=date('j');
											if(count($acdate)<3) $acdate[]=date('n');
											if(count($acdate)<4) $acdate[]=date('Y');
											$mysqlsessdate="STR_TO_DATE(CONCAT(session_start_year,'-',LPAD(session_start_month,2,'00'),'-',LPAD(session_start_day,2,'00'),'-',LPAD(session_start_hour,2,'00'),'-',LPAD(session_start_minute,2,'00')),'%Y-%m-%d-%H-%i')";
											$mysqldate1="STR_TO_DATE('".$acdate[3]."-".$acdate[2]."-".$acdate[1]."-".$acdate[0]."-00','%Y-%m-%d-%H-%i')";
											$mysqldate2="STR_TO_DATE('".$acdate[3]."-".$acdate[2]."-".$acdate[1]."-".$acdate[0]."-00','%Y-%m-%d-%H-%i')";
											if(count($cdates)==1) $where_clause.="AND session_start_year='".$acdate[3]."' AND session_start_month='".$acdate[2]."' AND session_start_day='".$acdate[1]."' AND session_start_hour='".$acdate[0]."'";
											elseif($i==0) {$supsess_criteria.="AND ".$mysqlsessdate.">=".$mysqldate1." "; $where_clause.=$supsess_criteria;}
											else {$supsess_criteria.="AND ".$mysqlsessdate."<=".$mysqldate2." "; $where_clause.=$supsess_criteria;}
											if($i>0) $datetoshow.=" - ";
											$cdtsh=implode("/",array_map("str_pad",$acdate,array_fill(0,count($acdate),2),array_fill(0,count($acdate),'0'),array_fill(0,count($acdate),STR_PAD_LEFT))); //str_pad($input, 10, "-=", STR_PAD_LEFT);
											$cdtsh=preg_replace("/\//","h ",$cdtsh,1);
											$datetoshow.=$cdtsh;
										}
										// "session_finished='n' AND NOW()<=STR_TO_DATE(CONCAT(session_start_year,'-',LPAD(session_start_month,2,'00'),'-',LPAD(session_start_day,2,'00'),'-',LPAD(session_start_hour,2,'00'),'-',LPAD(session_start_minute,2,'00')),'%Y-%m-%d-%H-%i')"
                                        $title=lang('subjects').' '.$datetoshow;
                                        break;
                case "atdate":    
										$cdate=empty($_REQUEST['date'])?date('j').".".date('n').".".date('Y'):$_REQUEST['date'];
										if(!empty($focus2part)) $cdate=$focus2part;
										$cdates=explode('-',$cdate);
										$where_clause="AND registered='y' ";
										$datetoshow="";
										for($i=0; $i<count($cdates); $i++)
										{
											$acdate=explode('.',$cdates[$i]);
											if(count($acdate)<2) $acdate=explode('/',$cdates[$i]);
											if(count($acdate)<2) $acdate[]=date('n');
											if(count($acdate)<3) $acdate[]=date('Y');
											$mysqlsessdate="STR_TO_DATE(CONCAT(session_start_year,'-',LPAD(session_start_month,2,'00'),'-',LPAD(session_start_day,2,'00'),'-',LPAD(session_start_hour,2,'00'),'-',LPAD(session_start_minute,2,'00')),'%Y-%m-%d-%H-%i')";
											$mysqldate1="STR_TO_DATE('".$acdate[2]."-".$acdate[1]."-".$acdate[0]."-00-01','%Y-%m-%d-%H-%i')";
											$mysqldate2="STR_TO_DATE('".$acdate[2]."-".$acdate[1]."-".$acdate[0]."-23-59','%Y-%m-%d-%H-%i')";
											if(count($cdates)==1) $where_clause.="AND session_start_year='".$acdate[2]."' AND session_start_month='".$acdate[1]."' AND session_start_day='".$acdate[0]."'";
											elseif($i==0) {$supsess_criteria.="AND ".$mysqlsessdate.">=".$mysqldate1." "; $where_clause.=$supsess_criteria;}
											else {$supsess_criteria.="AND ".$mysqlsessdate."<=".$mysqldate2." "; $where_clause.=$supsess_criteria;}
											if($i>0) $datetoshow.=" - ";
											$datetoshow.=implode("/",array_map("str_pad",$acdate,array_fill(0,count($acdate),2),array_fill(0,count($acdate),'0'),array_fill(0,count($acdate),STR_PAD_LEFT))); //str_pad($input, 10, "-=", STR_PAD_LEFT);
										}
										// "session_finished='n' AND NOW()<=STR_TO_DATE(CONCAT(session_start_year,'-',LPAD(session_start_month,2,'00'),'-',LPAD(session_start_day,2,'00'),'-',LPAD(session_start_hour,2,'00'),'-',LPAD(session_start_minute,2,'00')),'%Y-%m-%d-%H-%i')"
                                        $title=lang('subjects').' '.$datetoshow;
                                        break;
                case "absentatdate":    
										$cdate=empty($_REQUEST['date'])?date('j').".".date('n').".".date('Y'):$_REQUEST['date'];
										if(!empty($focus2part)) $cdate=$focus2part;
										$cdates=explode('-',$cdate);
										$where_clause="AND registered='y' AND shownup='n' AND participated='n' ";
										$datetoshow="";
										for($i=0; $i<count($cdates); $i++)
										{
											$acdate=explode('.',$cdates[$i]);
											if(count($acdate)<2) $acdate=explode('/',$cdates[$i]);
											if(count($acdate)<2) $acdate[]=date('n');
											if(count($acdate)<3) $acdate[]=date('Y');
											$mysqlsessdate="STR_TO_DATE(CONCAT(session_start_year,'-',LPAD(session_start_month,2,'00'),'-',LPAD(session_start_day,2,'00'),'-',LPAD(session_start_hour,2,'00'),'-',LPAD(session_start_minute,2,'00')),'%Y-%m-%d-%H-%i')";
											$mysqldate1="STR_TO_DATE('".$acdate[2]."-".$acdate[1]."-".$acdate[0]."-00-01','%Y-%m-%d-%H-%i')";
											$mysqldate2="STR_TO_DATE('".$acdate[2]."-".$acdate[1]."-".$acdate[0]."-23-59','%Y-%m-%d-%H-%i')";
											if(count($cdates)==1) $where_clause.="AND session_start_year='".$acdate[2]."' AND session_start_month='".$acdate[1]."' AND session_start_day='".$acdate[0]."'";
											elseif($i==0) {$supsess_criteria.="AND ".$mysqlsessdate.">=".$mysqldate1." "; $where_clause.=$supsess_criteria;}
											else {$supsess_criteria.="AND ".$mysqlsessdate."<=".$mysqldate2." "; $where_clause.=$supsess_criteria;}
											if($i>0) $datetoshow.=" - ";
											$datetoshow.=implode("/",array_map("str_pad",$acdate,array_fill(0,count($acdate),2),array_fill(0,count($acdate),'0'),array_fill(0,count($acdate),STR_PAD_LEFT))); //str_pad($input, 10, "-=", STR_PAD_LEFT);
										}
										$where_clause="AND registered='y' AND shownup='n' AND participated='n' AND session_start_year='".$acdate[2]."' AND session_start_month='".$acdate[1]."' AND session_start_day='".$acdate[0]."'";
                                        $title=lang('subjects_absent').' '.$datetoshow;
                                        break;
                case "atmonth":    
										$cdate=date('n').".".date('Y');
										if(!empty($_REQUEST['date'])) $cdate=$_REQUEST['date'];
										if(!empty($_REQUEST['month'])) $cdate=$_REQUEST['month'];
										if(!empty($focus2part)) $cdate=$focus2part;
										$acdate=explode('.',$cdate);
										if(count($acdate)<2) $acdate=explode('/',$cdate);
										if(count($acdate)<2) $acdate[]=date('Y');
										$where_clause="AND registered='y' AND session_start_year='".$acdate[1]."' AND session_start_month='".$acdate[0]."'";
                                        $title=lang('subjects').' '.implode("/",$acdate);
                                        break;
                case "today":    
										$where_clause="AND registered='y' AND session_start_year='".date('Y')."' AND session_start_month='".date('n')."' AND session_start_day='".date('j')."'";
                                        $title=lang('subjects').' '.date('d/m/Y');
                                        break;
                case "tomorrow":    
										$where_clause="AND registered='y' AND session_start_year='".date('Y', strtotime('+1 day'))."' AND session_start_month='".date('n', strtotime('+1 day'))."' AND session_start_day='".date('j', strtotime('+1 day'))."'";
                                        $title=lang('subjects').' '.date('d/m/Y', strtotime('+1 day'));
                                        break;
				case "todaynew":    
										$where_clause="AND registered='y' AND shownup='n' AND participated='n'  AND session_finished='n' AND session_start_year='".date('Y')."' AND session_start_month='".date('n')."' AND session_start_day='".date('j')."'";
                                        $title=lang('subjects_new').' '.date('d/m/Y');
                                        break;
				case "todayopen":    
										$where_clause="AND registered='y' AND session_finished='n' AND session_start_year='".date('Y')."' AND session_start_month='".date('n')."' AND session_start_day='".date('j')."'";
                                        $title=lang('subjects_new').' '.date('d/m/Y');
                                        break;
				case "todayopenshownup":    
										$where_clause="AND registered='y' AND shownup='y' AND session_finished='n' AND session_start_year='".date('Y')."' AND session_start_month='".date('n')."' AND session_start_day='".date('j')."'";
                                        $title=lang('subjects_new').' '.date('d/m/Y');
                                        break;
				case "todayshownup":    
										$where_clause="AND registered='y' AND shownup='y' AND session_start_year='".date('Y')."' AND session_start_month='".date('n')."' AND session_start_day='".date('j')."'";
                                        $title=lang('subjects_new').' '.date('d/m/Y');
                                        break;
				case "todayabsent":    
										$where_clause="AND registered='y' AND shownup='n' AND participated='n' AND session_start_year='".date('Y')."' AND session_start_month='".date('n')."' AND session_start_day='".date('j')."'";
                                        $title=lang('absent_subjects').' '.date('d/m/Y');
                                        break;
                }


        $select_query=" SELECT * FROM ".table('participants').", ".table('participate_at').",
                                        ".table('sessions')."
                        WHERE ".table('participants').".participant_id=".
                                        table('participate_at').".participant_id
                        AND ".table('sessions').".session_id=".table('participate_at').".session_id
						";
        if ($experiment_id) $select_query.="AND ".table('participate_at').".experiment_id='".$experiment_id."' ";
        if ($session_id) $select_query.=" AND ".table('participate_at').".session_id='".$session_id."' ";
        $select_query.=$where_clause." ORDER BY ".$order;


	// get result
        $result=mysqli_query($GLOBALS['mysqli'],$select_query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

        $participants=array();
        while ($line=mysqli_fetch_assoc($result)) {
                $participants[]=$line;
                }
        $result_count=count($participants);


	// determine table title
	// $table_title=$experiment['experiment_name'];
	if($experiment_id) $table_title=$experiment['experiment_public_name']; else $table_title=lang("all_experiments");
	if ($session_id) $table_title.=', '.$lang['session'].' '.session__build_name($session);
	$table_title.=' - '.$title;

	$allow_privite_info=check_allow('experiment_show_access_personal_data','',true);
	// determine table headings
	$table_headings=array();
	$table_headings[]="";
	if($allow_privite_info)$table_headings[]=$lang['lastname'];
	if($allow_privite_info)$table_headings[]=$lang['firstname'];
	if($allow_privite_info)$table_headings[]=$lang['e-mail-address'];
	if($allow_privite_info)$table_headings[]=$lang['phone_number'];
	$table_headings[]=$lang['gender'];
	$table_headings[]=$lang['studies'].'/'.$lang['profession'];
	$table_headings[]=$lang['noshowup'];
	if ($assigned || $invited) $table_headings[]=$lang['invited_abbr'];
	if ($registered || $shownup || $participated || $nonparticipated || $notpresent || $absent || $absentold || $absentnew || $todayabsent || $new || $todaystarted || $atdate || $athours || $absentatdate || $atmonth || $today || $todaynew || $tomorrow || $todayopen || $todayopenshownup || $todayshownup) {
		if (!$session_id) $table_headings[]=$lang['session'];
		$table_headings[]=$lang['shownup_abbr'];
		$table_headings[]=$lang['participated_abbr'];
		}
	if($show_rules_signed) $table_headings[]=$lang['rules_abbr'];

	// generate table content
	$studies=lang__load_studies();
        $professions=lang__load_professions();

	$table_data=array();
	$session_names=array();

	$pnr=0;
        foreach ($participants as $p) {

		$row=array();
		$pnr++;
		$row[]=$pnr;
        	if($allow_privite_info)$row[]=$p['lname'];
	        if($allow_privite_info)$row[]=$p['fname'];
        	if($allow_privite_info)$row[]=$p['email'];
		if($allow_privite_info)$row[]=$p['phone_number'];
		if ($p['gender']=='m') $row[]=$lang['gender_m_abbr'];
			elseif ($p['gender']=='f') $row[]=$lang['gender_f_abbr'];
			else $row[]="?";
		$row[]= ($p['field_of_studies']>0) ? 
				$studies[$p['field_of_studies']].' ('.$p['begin_of_studies'].')' .' ['.$p['birth_year'].']' : 
				$professions[$p['profession']].' ['.$p['birth_year'].']';
		$row[]=$p['number_noshowup'].'/'.$p['number_reg'];
		if ($assigned || $invited) $row[]=$p['invited'];
		if ( $registered || $shownup || $participated || $nonparticipated || $notpresent || $absent || $absentold || $absentnew || $todayabsent || $new || $todaystarted || $atdate || $athours || $absentatdate || $atmonth || $today || $todaynew || $tomorrow || $todayopen || $todayopenshownup || $todayshownup) {
			if (!$session_id) {
				if (!isset($session_names[$p['session_id']])) 
					$session_names[$p['session_id']]=session__build_name($p);
				$row[]=$session_names[$p['session_id']];
				}
			$row[]=$lang[$p['shownup']];
                        $row[]=$lang[$p['participated']];
			}
		if($show_rules_signed) $row[]= ($p['rules_signed']!='y') ? "X" : '';
		$table_data[]=$row;
		}

        // prepare pdf
        include_once('../tagsets/class.ezpdf.php');

        $pdf = new Cezpdf('a4','landscape');

        $pdf->selectFont('../tagsets/fonts/Times-Roman.afm');

	$fontsize= ($settings['participant_list_pdf_table_fontsize']) ? $settings['participant_list_pdf_table_fontsize'] : 10;
	$titlefontsize= ($settings['participant_list_pdf_title_fontsize']) ? $settings['participant_list_pdf_title_fontsize'] : 12;

	$y=$pdf->ezTable($table_data,
                                $table_headings,
                                $table_title,
                        array('showLines'=>2,
                                'showHeadings'=>1,
                                'shaded'=>2,
                                'shadeCol'=>array(1,1,1),
                                'shadeCol2'=>array(0.9,0.9,0.9),
                                'fontSize'=>$fontsize,
                                'titleFontSize'=>$titlefontsize,
                                'rowGap'=>1,
                                'colGap'=>3,
                                'innerLineThickness'=>0.5,
                                'outerLineThickness'=>1,
                                'maxWidth'=>800,
                                'width'=>800,
                                'protectRows'=>2));

	// debugging stuff
        if ($file) {
                $pdffilecode = $pdf->output();

                return $pdffilecode;
                } else {
                $pdf->ezStream(array('Content-Disposition'=>'participant_list.pdf',
                                'Accept-Ranges'=>0,
                                'compress'=>1));
                }

}


function pdfoutput__make_calendar($caltime=0,$calyear=false,$admin=false,$forward=0,$file=false) {
	global $settings;

	if(isset($_REQUEST['d'])) $debug=$_REQUEST['d']; else $debug=false;

	if ($caltime==0) $caltime=time();

	if ($calyear) {
		$caltime=date__year_start_time($caltime);
		$forward=11;
		}

	// prepare pdf
	include_once('../tagsets/class.ezpdf.php');

	$pdf = new Cezpdf('a4');

	$pdf->selectFont('../tagsets/fonts/Times-Roman.afm');

	$fontsize= ($settings['calendar_pdf_table_fontsize']) ? $settings['calendar_pdf_table_fontsize'] : 8;
        $titlefontsize= ($settings['calendar_pdf_title_fontsize']) ? $settings['calendar_pdf_title_fontsize'] : 12;

	$i=0;
	// var_dump($forward);
	while ($i< $forward+1) {
		$monthdata=pdfoutput__calendar_get_month_table($caltime,$admin);

		$title=$monthdata['table_title'];
		$headings=$monthdata['table_headings'];
		$data=$monthdata['table_data'];

		$y=$pdf->ezTable($data,
              			$headings,
				$title,
              		array('showLines'=>2,
                    		'showHeadings'=>1,
                    		'shaded'=>2,
                    		'shadeCol'=>array(1,1,1),
				'shadeCol2'=>array(0.9,0.9,1),
                    		'fontSize'=>$fontsize,
                    		'titleFontSize'=>$titlefontsize,
                    		'rowGap'=>1,
                    		'colGap'=>3,
                    		'innerLineThickness'=>0.5,
                    		'outerLineThickness'=>1,
                    		'maxWidth'=>500,
				'width'=>500,
				'protectRows'=>2));
		// var_dump($title,$headings,$data,$y);
		$pdf->ezSetDy(-20);
		$caltime=date__skip_months(1,$caltime);
		$i++;
	}
	// var_dump($file);
	// debugging stuff
	if ($file) {
		$pdffilecode = $pdf->output();

		return $pdffilecode;	
  		/*
  		$fname ="/apache/orsee/".$GLOBALS['settings__admin_folder']."/pdfdir/test.pdf";
  
  		$fp = fopen($fname,'w');
  			fwrite($fp,$pdffilecode);
  		fclose($fp);
		echo '<A HREF="pdfdir/test.pdf" target="_blank">pdf file</A><BR><BR>';
  		$pdfcode = str_replace("\n","\n<br>",htmlspecialchars($pdfcode));
  		echo trim($pdfcode);
		*/
		} else {
  		$pdf->ezStream(array('Content-Disposition'=>'calendar.pdf',
				'Accept-Ranges'=>0,
				'compress'=>1));
		}
}

function pdfoutput__calendar_get_month_table($time=0,$admin=false) {
	if ($time==0) $time=time();

	global $lang,$settings;

  	$start_date=date__skip_months(0,$time);
  	$date=getdate($start_date);
  	$limit=calendar__days_in_month($date['mon'],$date['year']);
  	$first_day=$date['wday'];
  	if ($first_day==0) $first_day=7; $first_day=$first_day-1;
  	$day="01";
  	$month=helpers__pad_number($date['mon'],2);
  	$year=$date['year'];
	$month_names=explode(",",$lang['month_names']);

	// prepare day array
        $days=array();
        for ($i=1; $i<=$limit; $i++) {
                $days[$i]=array();
                }

        // get session times
        $query="SELECT * FROM ".table('sessions').", ".table('experiments').", ".table('lang')."
                WHERE ".table('sessions').".experiment_id=".table('experiments').".experiment_id
                AND ".table('sessions').".laboratory_id=".table('lang').".content_name
                AND ".table('lang').".content_type='laboratory'";
        if (!$admin) $query.=" AND ".table('experiments').".hide_in_cal='n' ";
        $query.=" AND session_start_year='".$year."'
                 AND session_start_month='".$date['mon']."'
                 ORDER BY session_start_day, session_start_hour, session_start_minute";
        $result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
        while ($line=mysqli_fetch_assoc($result)) $days[$line['session_start_day']][]=$line;

        // get maintenance times
        $monthstring=$year.$month;

        $query="SELECT *, (space_start_year*100+space_start_month) as space_start,
                        (space_stop_year*100+space_stop_month) as space_stop
                FROM ".table('lab_space').", ".table('lang')."
                WHERE ".table('lab_space').".laboratory_id=".table('lang').".content_name
                AND ".table('lang').".content_type='laboratory'
                HAVING space_start<='".$monthstring."'
                        AND space_stop>='".$monthstring."'
                ORDER BY space_start_day, space_start_hour, space_start_minute";
        $result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
        while ($line=mysqli_fetch_assoc($result)) {
                $maintstart = ($line['space_start']==$monthstring) ? $line['space_start_day'] : 1;
                $maintstop = ($line['space_stop']==$monthstring) ? $line['space_stop_day'] : $limit;
                for ($i=$maintstart; $i<=$maintstop; $i++) $days[$i][]=$line;
                }



        // remember table title
        $table_title=$month_names[$date['mon']-1].' '.$year;

        // remember table heading
        $calendar__weekdays=explode(",",$lang['weekdays_abbr']);
	$wday=1;
        foreach ($calendar__weekdays as $weekday) {
                $table_headings[$wday]=$weekday;
		$wday++;
        }

	// write empty fields in days array
	$i=0;
 	$wday=1; $las1=array(); $las2=array();
  	while ($i<$first_day) {
    		$las1[$wday]=""; $las2[$wday]=""; 
    		$i++; $wday++;
  	}

	// write day numbers
  	$i=1; $pointer=0;
  	while ($i<=$limit) {
          	if ($wday==8) {
           		$table_data[]=$las1;
	   		$table_data[]=$las2;
	   		$las1=array(); $las2=array(); $wday=1;
           		}
        	$las1[$wday]=helpers__pad_number($i,2); $las2[$wday]="";
		$nonempty=false;

        	foreach ($days[$i] as $entry) {
		   $nonempty=true;
		   if (isset($entry['session_start_day'])) {
			$nonempty=true;

			$start_time=time__get_timepack_from_pack($entry,"session_start_");
                        $duration=time__get_timepack_from_pack($entry,"session_duration_");
                        $end_time=time__add_packages($start_time,$duration);

                        $las2[$wday].=time__format($lang['lang'],$start_time,true,false,true,true).'-'.
                                                time__format($lang['lang'],$end_time,true,false,true,true);

			$las2[$wday].="\n"."<i>".laboratories__strip_lab_name(stripslashes($entry[$lang['lang']]))."</i>\n";

        		if ($admin) 
				$las2[$wday].="<b>".$entry['experiment_name']."</b>";
                  		else $las2[$wday].="<b>".$entry['experiment_public_name']."</b>";
			$las2[$wday].="\n";

			if ($admin) $las2[$wday].=$entry['experimenter']."\n";

        		$cs__reg=experiment__count_participate_at($entry['experiment_id'],$entry['session_id']);

        		if ($cs__reg<$entry['part_needed']) $cs__status="not_enough_participants";
        		  elseif ($cs__reg<$entry['part_needed']+$entry['part_reserve']) $cs__status="not_enough_reserve";
        		  else $cs__status="complete";


        		if ($admin) $las2[$wday].=$cs__reg.' ('.$entry['part_needed'].','.$entry['part_reserve'].')';

        		$reg_end=sessions__get_registration_end($entry);

        		if (time()>$reg_end) $cs__status="complete";

        		if (!($admin)) {
        			switch ($cs__status) {
                			case "not_enough_participants":
                			case "not_enough_reserve":
                                        			        $text=$lang['free_places'];
                                                			break;
                			case "complete":
                                               				$text=$lang['complete'];
                                               				break;
        				}
        			$las2[$wday].=$text;
        			}
        		}
		    else {
			if ($admin) {
                                if ($entry['space_start_day']==$i) {
                                	$from['hour']=$entry['space_start_hour'];
                                        $from['minute']=$entry['space_start_minute'];
                                        }
                                   else {
                                        $from['hour']=$settings['laboratory_opening_time_hour'];
                                        $from['minute']=$settings['laboratory_opening_time_minute'];
                                        }
                                   if ($entry['space_stop_day']==$i) {
                                        $to['hour']=$entry['space_stop_hour'];
                                        $to['minute']=$entry['space_stop_minute'];
                                        }
                                   else {
                                        $to['hour']=$settings['laboratory_closing_time_hour'];
                                        $to['minute']=$settings['laboratory_closing_time_minute'];
                                        }
                                   $las2[$wday].=time__format($lang['lang'],$from,true,false,true,true).'-'.
                                                time__format($lang['lang'],$to,true,false,true,true);
                                   $las2[$wday].="\n"."<i>".laboratories__strip_lab_name(stripslashes($entry[$lang['lang']]))."</i>\n";

                                   $las2[$wday].="<b>".$entry['reason']."</b>\n".$entry['experimenter'];
                                        }
			}
		    $las2[$wday].="\n\n";
                    $pointer++;
		    }
		if (!$nonempty) $las2[$wday]="";
		$i++; $wday++;
  		}

  	while ($wday<8) {
    		$las1[$wday]=""; $las2[$wday]="";
    		$wday++;
  		}
  	$table_data[]=$las1;
  	$table_data[]=$las2;

	$alldata['table_title']=$table_title;
	$alldata['table_headings']=$table_headings;
	$alldata['table_data']=$table_data;

  	return $alldata;
}

?>

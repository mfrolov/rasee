<?php
  $protocol = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) ? 'https' : 'http';
  header('Location: '.$protocol.'://'.$_SERVER['SERVER_NAME']);
  exit;
?>
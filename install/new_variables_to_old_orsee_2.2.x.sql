
ALTER TABLE or_participants_log ADD COLUMN ipaddr varchar(255) collate utf8_unicode_ci default '';


ALTER TABLE or_participants ADD COLUMN birth_year mediumtext collate utf8_unicode_ci default NULL;
ALTER TABLE or_participants ADD INDEX birth_year_index (birth_year(4));
ALTER TABLE or_participants_temp ADD COLUMN birth_year mediumtext collate utf8_unicode_ci default NULL;
ALTER TABLE or_participants_temp ADD INDEX birth_year_index (birth_year(4));


ALTER TABLE or_participants_temp ADD COLUMN unwanted_pay_systems varchar(255) collate utf8_unicode_ci default '';
ALTER TABLE or_participants ADD COLUMN unwanted_pay_systems varchar(255) collate utf8_unicode_ci default '';
ALTER TABLE or_participants_temp ADD INDEX unwanted_pay_systems_index (unwanted_pay_systems);
ALTER TABLE or_participants ADD INDEX unwanted_pay_systems (unwanted_pay_systems);


ALTER TABLE or_participants_temp ADD COLUMN study_level varchar(255) collate utf8_unicode_ci default '';
ALTER TABLE or_participants ADD COLUMN study_level varchar(255) collate utf8_unicode_ci default '';
ALTER TABLE or_participants_temp ADD INDEX study_level_index (study_level);
ALTER TABLE or_participants ADD INDEX study_level (study_level);

ALTER TABLE or_participants_temp ADD COLUMN activity_area varchar(255) collate utf8_unicode_ci default '';
ALTER TABLE or_participants ADD COLUMN activity_area varchar(255) collate utf8_unicode_ci default '';
ALTER TABLE or_participants_temp ADD INDEX activity_area_index (activity_area);
ALTER TABLE or_participants ADD INDEX activity_area (activity_area);

UPDATE or_lang SET fr=CONCAT('<!-- 10 -->',fr) WHERE content_type='field_of_studies' AND fr NOT LIKE '<%';
UPDATE or_lang SET en=CONCAT('<!-- 10 -->',en) WHERE content_type='field_of_studies' AND en NOT LIKE '<%';
UPDATE or_lang SET de=CONCAT('<!-- 10 -->',de) WHERE content_type='field_of_studies' AND de NOT LIKE '<%';



-- INSERT INTO `or_lang` (`lang_id`, `enabled`, `content_type`, `content_name`, `en`, `de`, `fr`) VALUES
-- (170000,	'y',	'unwanted_pay_systems',	'1631544895',	'Amazon gift vouchers',	'Amazon-Geschenkgutscheine',	'Chèques cadeau amazon'),
-- (170001,	'y',	'unwanted_pay_systems',	'1631544913',	'Paypal',	'Paypal',	'Paypal'),
-- (170002,	'y',	'unwanted_pay_systems',	'1631544937',	'ApplePay',	'ApplePay',	'ApplePay'),
-- (170003,	'y',	'unwanted_pay_systems',	'1631544946',	'Wise',	'Wise',	'Wise'),
-- (170004,	'y',	'unwanted_pay_systems',	'1631544961',	'Revolute',	'Revolute',	'Revolute'),
-- (170005,	'y',	'unwanted_pay_systems',	'1631544968',	'Lydia',	'Lydia',	'Lydia'),
-- (170006,	'y',	'unwanted_pay_systems',	'1631545004',	'Bank transfer',	'Banküberweisung',	'Virement bancaire');


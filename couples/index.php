<?php
// part of orsee. see orsee.org
ob_start();

include_once ("../config/settings.php");
$special_subject=basename(__DIR__);
$menu__area="mainpage";
$navigation_disabled=true;
include "../".$GLOBALS['settings__public_folder']."/header.php";

echo "<center>";

show_message();

if(empty($settings__stop_subscriptions) || $settings__stop_subscriptions == "n") {
	if(isset($_REQUEST['ans']) && $_REQUEST['ans'] == 'no') {
		$consentement_found=false;
		if(!empty(content__get_content($special_subject."_consentment"))) {
			$consentment=explode("|",content__get_content($special_subject."_consentment"));
			// var_dump(content__get_content($special_subject."_consentment"),$consentment);
			if(count($consentment)>1) {
				echo '<hr>'.str_replace("#settings__public_folder#",$GLOBALS['settings__public_folder'],$consentment[1]).'<hr>';
				$consentement_found=true;
			}
		}
		if(!$consentement_found) echo '<hr><h6>Pour une inscription standard, veuillez <a class=button href="../'.$GLOBALS['settings__public_folder'].'/">cliquer ici</a></h6><hr>';
	}
	if(!empty($_REQUEST['p'])) {
		$participant_id=url_cr_decode($_REQUEST['p']);
		if(!empty($participant_id)) {
			$content=str_replace("participant_create.php?","participant_create.php?p=".$_REQUEST['p']."&",content__get_content($special_subject."_welcome"));
			echo $content;
		}
	}
	if( empty($_REQUEST['p']) || empty($participant_id)) echo content__get_content($special_subject."_welcome");
}
else 
	echo content__get_content("error_temporary_disabled");

if (!isset($addp)) $addp="";

if ($addp) $sign="&"; else $sign="?";

$langarray=lang__get_public_langs();

$lang_names=lang__get_language_names();

if  (count($langarray) > 1) {
echo '<BR><BR>
	switch to ';

	foreach ($langarray as $thislang) {
		if ($thislang != $authdata['language']) {
		echo '<A HREF="index.php'.$addp.$sign.'language='.$thislang.'">';
		$iconpath='../style/'.$settings['style'].'/icons/lang_'.$thislang.'.png';
		if (file_exists($iconpath)) echo icon('lang_'.$thislang);
		if ($lang_names[$thislang]) echo $lang_names[$thislang]; else echo $thislang;
		echo '</A>&nbsp;&nbsp;&nbsp;';
		}
	}
}

echo "</center>";

clearpixel();

include "../".$GLOBALS['settings__public_folder']."/footer.php";


?>

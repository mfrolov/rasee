<?php
/* example jCryption entry point - will handle handshake, getPublicKey,
 * decrypttest or a dump of posted form variables.
 * Key files specified below should be stored outside the web tree
 * but for the example they are not.
 *
 * To generate keys:
 *
 * openssl genrsa -out rsa_1024_priv.pem 1024
 * openssl rsa -pubout -in rsa_1024_priv.pem -out rsa_1024_pub.pem
 */
include_once ("../config/settings.php");
include_once ("../config/system.php");
include_once ("../config/requires.php");
session_set_save_handler("orsee_session_open", "orsee_session_close", "orsee_session_read", "orsee_session_write", "orsee_session_destroy", "orsee_session_gc");

$php_gt533=version_compare(phpversion(), '5.3.3', '>=');
// var_dump($php_gt533);
require_once 'include/sqAES.php';
require_once 'include/JCryption.php';
if(!$php_gt533) include_once 'include/Crypt/AES.php';

$dir = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];
$url = $_SERVER['REQUEST_URI']; //returns the current URL
$parts = explode('/',$url);
//$dir = $_SERVER['SERVER_NAME'];
for ($i = 0; $i < count($parts) - 1; $i++) {
 $dir .= $parts[$i] . "/";
}
//echo $dir.'rsa_1024_priv.php?k=pub'; exit;
$jc = new JCryption($dir.'rsa_1024_priv.php?k=pub', $dir.'rsa_1024_priv.php?k=jpWgh0a5Z2CShKO2lcYCXAyP');
$jc->go();
header('Content-type: text/plain');
print_r($_POST);

<basefont face="Tahoma,Arial,Helvetica,sans-serif">

<center>
<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 WIDTH="100%">
	<TR VALIGN=TOP ALIGN=LEFT BGCOLOR="#cc9966">
		<TD COLSPAN=2 WIDTH=100% HEIGHT=19 ALIGN=RIGHT VALIGN=BOTTOM>
		<P><FONT face="Arial" size=1 class="small" COLOR="#FFFFFF">RECRUITMENT AND ACCOUNTING SYSTEM FOR EXPERIMENTAL ECONOMICS</FONT>
		</TD>
	</TR>


	<TR bgcolor="#996600">
		<TD WIDTH=100% HEIGHT=70 VALIGN=MIDDLE ALIGN=LEFT rowspan=2>
			<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 WIDTH=300>
			<TR>
				<TD VALIGN=TOP>
				<IMG border=0 SRC="../style/orsee2/orsee_sign.gif">
				</TD>
			</TR>
			</TABLE>
		</TD>
                <TD ALIGN=RIGHT VALIGN=bottom WIDTH="100%">
                        <TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 WIDTH="100%" HEIGHT="100%">


                        <TR>
                                <TD valign=bottom align=right>
                                <IMG SRC="../style/orsee2/rasee_logo.gif" BORDER=0>&nbsp;
                                </TD>
                        </TR>
                        </TABLE>
                </TD>

	</TR>

	<TR bgcolor="#996600">
                <TD valign=top align=right>
                        <FONT face="Arial" size=1 class="small" COLOR="#FFFFFF">Based on <a target=_blank  style='color:#FFFFFF;text-decoration:underline' href="http://www.orsee.org">ORSEE</a> <a target=_blank  style='color:#FFFFFF;text-decoration:underline' href="https://github.com/orsee/orsee/releases/tag/orsee_2.2.5">2.2.5</a> THE EASE OF RECRUITMENT</FONT>
                </TD>

	</TR>

	<TR bgcolor="#cc9966" valign=top align=center>
		<TD valign=top align=left colspan=2>
		<?php navigation("horizontal"); ?>
		</TD>
	</TR>

	<TR>
		<TD VALIGN=TOP ALIGN=LEFT colspan=2>

			<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 WIDTH="100%" HEIGHT="100%">
			<TR>

			<TD width=100% valign=top>


# This is a color style file for orsee. see orsee.org.
#
# Empty lines or lines starting with # are ignored.
#
# color definition lines start with the color item name,
# followed by a : and then the color value,
# whitespaces are trimmed
#
# color values might be hex-numbers like #800080 or color names like blue



# color specifications for body tag of html page
body_bgcolor:	snow
body_text:	#4A4A4A
body_link:	#539EBD
body_alink      #904F39
body_vlink:	#904F39







# experiment calendar colors
calendar_month_font:		white
calendar_month_background:	black
calendar_experiment_name:	blue
calendar_day_background:	lightblue
calendar_day_background_today:	lightpink

# the following two are color lists, seperated by ","
calendar_admin_experiment_sessions:	burlywood,cadetblue,darkkhaki,darkturquoise,gold,lightcyan,lightsteelblue,mediumspringgreen,springgreen
calendar_public_experiment_sessions:   burlywood,cadetblue,darkkhaki,darkturquoise,gold,lightcyan,lightsteelblue,mediumspringgreen,springgreen
calendar_lab_space_reservation:	darksalmon,lightcoral,hotpink,lightpink,pink

# session state text colors
session_complete:	green
session_finished:	green
session_not_enough_participants:	red
session_not_enough_reserve:		orange

session_public_complete:	red
session_public_expired:		blue
session_public_free_places:	green

shownup_no:     red
shownup_yes:    green

# session reminder state text colors
session_reminder_state_sent_text:	#585
session_reminder_state_checked_text:	red
session_reminder_state_waiting_text:	#967

# menu colors
menu_title:	4DA0BE
menu_item:	#4DA0BE
menu_item_highlighted_background:	lightgrey

# item lists (experiments/sessions/options etc.)
#list_header_background:		#E0E0EA
list_header_background:		#C0E5F0
#list_title_background:		#A3A3C2

list_title_background:		#EAFFFF
list_item_background:		#DADAE7
list_item_emphasize_background:	white
#list_list_background: 		#E9E9F1
list_list_background: 		#EED
#list_shade1:			#BDCEEF
list_shade1:			#DDC
list_shade2:			#DEE
list_options_background:	lightgrey

list_highlighted_table_head_background:	blue
list_highlighted_table_head_text:	white

# notification message colors
message_background:	coral
message_border:		blue
message_text:		black

# admin type list: error backgrounds
admin_type_error_missing_required:	lightcoral
admin_type_required_by_error:		lightgreen

# background for fields not supplied in a form (used only sparly)
missing_field:	orange

# public area, experiment registration
just_registered_session_background:	sandybrown


# background for graphs in statistics section
stats_graph_background:	snow

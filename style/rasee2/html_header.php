<?php
?>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- <link rel='stylesheet'  href='https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css' type='text/css' /> -->
<link rel='stylesheet'  href='../style/rasee2/bootstrap.min.css' type='text/css' />
<link rel='stylesheet'  href='../style/rasee2/stacktable.css' />
<style type="text/css" id="custom-background-css">
body.custom-background { background-color: #CC9966; }
body .hor_navigation {line-height:1.7em;}
<?php
 if(isset($expadmindata['adminname'])) echo "textarea { resize:both; }";
?>
</style>
<script src='../style/rasee2/jquery-3.6.0.min.js'></script>
<script src='../style/rasee2/bootstrap.bundle.min.js'></script>
<script>
$(document).ready(function(){
  $('.exptype_hint').tooltip({
        delay: {show: 0, hide: 700}
    });   
});
</script>

<basefont face="Tahoma,Arial,Helvetica,sans-serif">


<center>
<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 WIDTH="100%">
	<TR VALIGN=TOP ALIGN=LEFT BGCOLOR="#cc9966">
		<TD COLSPAN=2 WIDTH=100% HEIGHT=19 ALIGN=RIGHT VALIGN=BOTTOM>
		<P><FONT face="Arial" size=1 class="small" COLOR="#FFFFFF">RECRUITMENT AND ACCOUNTING SYSTEM FOR EXPERIMENTAL ECONOMICS</FONT>
		</TD>
	</TR>


	<TR bgcolor="#89441C">
		<TD WIDTH=100% HEIGHT=70 VALIGN=MIDDLE ALIGN=LEFT rowspan=2>
			<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 WIDTH=300>
			<TR>
				<TD VALIGN=TOP>
				<IMG border=0 SRC="../style/rasee2/orsee_sign.gif">
				</TD>
			</TR>
			</TABLE>
		</TD>
                <TD ALIGN=RIGHT VALIGN=bottom WIDTH="100%">
                        <TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 WIDTH="100%" HEIGHT="100%">


                        <TR>
                                <TD valign=bottom align=right>
                                <IMG SRC="../style/rasee2/rasee_logo.gif" BORDER=0>&nbsp;
                                </TD>
                        </TR>
                        </TABLE>
                </TD>

	</TR>

	<TR bgcolor="#89441C">
                <TD valign=top align=right>
                        <FONT face="Arial" size=1 class="small" COLOR="#FFFFFF">Based on <a target=_blank  style='color:#FFFFFF;text-decoration:underline' href="http://www.orsee.org">ORSEE</a> <a target=_blank  style='color:#FFFFFF;text-decoration:underline' href="https://github.com/orsee/orsee/releases/tag/orsee_2.2.5">2.2.5</a> THE EASE OF RECRUITMENT</FONT>
                </TD>

	</TR>

	</TABLE>
</center>
<div id="page-container">
	<div class="menu_line" >
		<div class="hor_navigation et_pb_text_inner"><?php navigation("horizontal"); ?></div>
	</div>
</div>


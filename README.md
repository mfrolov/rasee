# RASEE (RECRUITMENT AND ACCOUNTING SYSTEM FOR EXPERIMENTAL ECONOMICS)

The project is derived from ORSEE (http://orsee.org/), version [2.2.5](https://github.com/orsee/orsee/releases/tag/orsee_2.2.5).
The name is changed according to the ORSEE license and takes into account the incorporation of a system wich facilitates the accounting when the experiment is paid in cash.
New features added to the ORSEE 2.2.5 version are independent of the ORSEE 3.

## Requirements
The system was tested on apache/php7.4 & php8.3 (linux server for production).

## License
The project is distributed as is, without any guarantee. Being based on ORSEE, the ORSEE licence applies (see install/LICENSE), including the obligation to cite the following publication (or an updated version) when the system is used for academic production:

         Ben Greiner: An Online Recruitment System for Economic Experiments.
         In: Kurt Kremer, Volker Macho (Eds.): 
         Forschung und wissenschaftliches Rechnen 2003. GWDG Bericht 63,
         Goettingen : Ges. fuer Wiss. Datenverarbeitung, 79-93, 2004.

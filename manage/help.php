<?php
// part of orsee. see orsee.org
$content_type=empty($_REQUEST['content'])?'help':trim($_REQUEST['content']);
$possible_contents=["help","public_content"];
if(!in_array($content_type,$possible_contents)) $content_type=$possible_contents[0];
$public_contents=["public_content"];

$header_prefix="";
include ("../config/settings.php");

if(in_array($content_type,$public_contents)) $header_prefix="../".$GLOBALS['settings__public_folder']."/";

include ($header_prefix."nonoutputheader.php");
$topic=isset($_REQUEST['topic'])?$_REQUEST['topic']:"";

$pagetitle.=$lang['help'].": ".preg_replace("/_/"," ",$topic);
html__header();


include ("../style/".$settings['style']."/help_html_header.php");

	if (empty($_REQUEST['topic'])) {
		echo '<h4>'.$lang['no_topic_choosed'].'</h4>';
		}
	   elseif ($_REQUEST['topic']=="login_names") {
		echo '<TABLE>
			<TR bgcolor="'.$color['list_title_background'].'">
				<TD>'.$lang['name'].'</TD>
				<TD>'.$lang['login_name'].'</TD>
			</TR>';

     		$query="SELECT *
      			FROM ".table('admin')." 
      			ORDER BY lname, fname, adminname";
		$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

		while ($line=mysqli_fetch_assoc($result)) {
        		echo '<TR><TD>';
			echo $line['fname'];
			echo ' ';
			echo $line['lname'];
			echo '</TD>
                		<TD align=right>';
			echo $line['adminname'];
			echo '</TD>
        			</TR>';
			}
		echo '</TABLE>';

		}
	   else {
		$query="SELECT * FROM ".table('lang')." WHERE content_type='$content_type'
			AND content_name='".$_REQUEST['topic']."'";

		$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));


		if (mysqli_num_rows($result)==0) {
			echo '<h4>'.$lang['no_help_available_for_topic'].'</h4>';
			}
		   else {
			$line = mysqli_fetch_assoc($result);
			if ($line[$expadmindata['language']]) $thislang=$expadmindata['language'];
				elseif ($line[$settings['admin_standard_language']]) $thislang=$settings['admin_standard_language']; 
				else $thislang="en";
					$toshow=str_replace("\n","<BR>",stripslashes($line[$thislang]));
					if(!empty($_REQUEST['hide_some_blocks'])) $toshow=preg_replace("/display:(list-item )?block/","display:none",$toshow);
            		echo $toshow;
			}

		mysqli_free_result($result);
		}

include ("../style/".$settings['style']."/help_html_footer.php");
html__footer();

?>

<?php
ob_start();

if (isset($_REQUEST['dossier'])) $dossier=$_REQUEST['dossier']; else $dossier="";

$menu__area="payment_ccounts";
$title="payment accounts";
include ("header.php");



	$done=false;
	$allow_cat="experimentclass";
    
	$allow=check_allow($allow_cat.'_edit','options_main.php');

	$header=lang('state_of_accounts');
	if(!empty($_REQUEST['addit'])) {
		$new=empty($_REQUEST["id"]) || empty($_REQUEST["details"]);
		// $new=true;
		if($new) $header=lang('new_operation'); else $header=lang('details_operation_id').''.$_REQUEST["id"];
	}
	// $where="";
	
	if (check_allow($allow_cat.'_add') && empty($_REQUEST["addit"])) {
	
		}
		
	operation::operation_tables();
	
	// $horizontal=(!empty($_REQUEST['horizontal']))?$_REQUEST['horizontal']:"payment_file";
	// $vertical=(!empty($_REQUEST['vertical']))?$_REQUEST['vertical']:"shortname";
	$limit_account=(!empty($_REQUEST['limacc']))?$_REQUEST['limacc']:"";
	$limit_file=(!empty($_REQUEST['limfile']))?$_REQUEST['limfile']:"";
	$where="WHERE 1";
	$use_nominals=false;
	if($limit_account || $limit_file) { $use_nominals=true;}
	if($limit_account) $where.=" AND shortname='$limit_account'";
	if($limit_file) $where.=" AND payment_file='$limit_file'";
	
	$showbank=false;
	if(!empty($_REQUEST['showbank'])) {$showbank=true; }
	$_SESSION['showbank']=(int)$showbank;
	$transpose=false;
	if(!empty($_REQUEST['transpose'])) $transpose=true;
	
	if($limit_account) $header.="<br>".lang('account').' '.helpers__pay_account_longname($limit_account);
	if($limit_file) $header.="<br>".lang('payment_file').' '.dossier_name($limit_file,1).' ('.'<a target=_blank href="dossiers_paiement_main.php?addit=1&show_statistics=1&id='.$limit_file.'">'.lang('edit',false).'</a>, <a target=_blank href="payment_operations.php?addit=1&dossier='.$limit_file.'">'.lang('new_operation',false).'</a>'.')';
	
	echo '<BR>
		<center><h4>'.$header.'</h4>';


	
	$query="SELECT * FROM ".table("pay_accounts")." ".$where." ORDER BY type,nominal DESC";
	$result=mysqli_query($GLOBALS['mysqli'],$query);
	if($result===false || mysqli_num_rows($result)==0) {
		echo lang('no_accounts_found').'.';
		echo '<BR><BR>
		<A href="javascript:history.back()">'.icon('back').' '.lang('back').'</A><BR><BR>
		</center>';
		include_once ("footer.php");
		exit;
	}
	$supw_pfile_limacc="";
	if(!empty($limit_account) && empty($_REQUEST["show_all"])) $supw_pfile_limacc=" AND shortname='$limit_account'";
	$supnobankcond=$showbank?"":"shortname<>'bank' AND shortname<>'participants' AND ";
	$supwhere_pfile="AND id IN (SELECT DISTINCT payment_file FROM ".table("pay_accounts")." WHERE ".$supnobankcond."amount<>0 $supw_pfile_limacc)";
	$notclosedcond="closed='0'";
	// $test=orsee_query("SELECT DISTINCT payment_file,shortname FROM ".table("pay_accounts")." WHERE ".$supnobankcond."amount<>0 and payment_file IN (SELECT DISTINCT id FROM ".table("payment_files")." WHERE $notclosedcond)","return_key_val"); print_r($test);
	if(!empty($_REQUEST["show_all"])) { $supwhere_pfile=""; $notclosedcond="1";}
	$p_files=($limit_file)?array():orsee_query("SELECT DISTINCT id FROM ".table("payment_files")." WHERE $notclosedcond ".$supwhere_pfile." ORDER BY year,number","return_first_elem"); //|| $limit_account
	if($limit_file) $p_files+=orsee_query("SELECT DISTINCT payment_file FROM ".table("pay_accounts")." ".$where,"return_first_elem");
	// print_r($p_files);
	$supwheretot=(!empty($limit_file) || !empty($_REQUEST["show_all"]))?"1":"payment_file IN (SELECT DISTINCT id FROM ".table("payment_files")." WHERE $notclosedcond)";
	$p_accs=orsee_query("SELECT DISTINCT shortname FROM ".table("pay_accounts")." ".$where." AND $supwheretot","return_first_elem");
	 // while ($line=mysqli_fetch_assoc($result)) {
		// $p_files[]=array($line['payment_file'],dossier_name($line['payment_file'],1));
		// $p_accs[]=array($line['shortname'],$line['name']);
	 // }
	if(array_search("bank",$p_accs)!==false) unset($p_accs[array_search("bank",$p_accs)]); 
	if(array_search("participants",$p_accs)!==false) unset($p_accs[array_search("participants",$p_accs)]); 
	if(!$use_nominals) {
		if(array_search("lab",$p_accs)!==false) unset($p_accs[array_search("lab",$p_accs)]);
		sort($p_accs); array_unshift($p_accs,"lab");
	}
	else sort($p_accs); 
	if($showbank) {array_unshift($p_accs,"bank","participants"); /*$p_accs[]="participants";*/}
	$hrows=$p_accs; $hcols=$p_files;
	if($transpose) {$hrows=$p_files; $hcols=$p_accs;}
	$hrows[]="_total_";
	// print_r($hrows);
	$suplink_base="";
	if($limit_account) $suplink_base.="&limacc=".$limit_account;
	if($limit_file) $suplink_base.="&limfile=".$limit_file;
	$suplink_bank=$showbank?"&showbank=1":""; $suplink_altbank=$showbank?"":"&showbank=1"; 
	$suplink_transp=$transpose?"&transpose=1":""; $suplink_alttransp=$transpose?"":"&transpose=1";
	$suplink=$suplink_base.$suplink_bank.$suplink_transp;

	echo '<table width=60% align=center><tr>';
	echo '<td style="width:50%;text-align:left">';
	if(!$limit_account) echo '<a href="'.thisdoc().'?one=1'.$suplink_base.$suplink_altbank.$suplink_transp.'">
	'.($suplink_altbank?lang('include'):lang('dont_include')).' '.lang('the_accounts',false).' '.helpers__pay_account_longname('bank').' '.lang('and',false).' '.helpers__pay_account_longname('participants').'
	</a>';
	echo '</td>';
	echo '<td style="text-align:right">
		<a href="'.thisdoc().'?one=1'.$suplink_base.$suplink_bank.$suplink_alttransp.'">
		'.lang('transpose').'\'
		</a></td>';
	echo '</tr></table>';
	if(!$use_nominals)
	{
		$rowwhere=(!$transpose)?"shortname":"payment_file";
		$colwhere=$transpose?"shortname":"payment_file";
		$rowname=($rowwhere=="shortname")?lang('account'):lang('payment_file');
		$colname=($colwhere=="shortname")?lang('account'):lang('payment_file');
		$rowlink=($rowwhere=="shortname")?'limacc':'limfile';
		$collink=($colwhere=="shortname")?'limacc':'limfile';
		
		$supwhere=$showbank?"":" AND shortname<>'bank' AND shortname<>'participants'";
		echo '<BR>
			<table border=1 style="border-collapse:collapse;text-align:center;vertical-align:middle">
				<TR>
						<td class="small">';
								echo $rowname.'\\'.$colname;
						echo '</td>';

					foreach ($hcols as $k=>$h)
						if(true) {
						echo '<td class="small">';
								if($transpose) echo helpers__pay_account_longname($h); else echo dossier_name($h,1);
						echo '</td>';
					}

		echo '			<TD>'.lang('total').'</TD>
				</TR>';
		foreach ($hrows as $kr=>$hr) {
			echo '<TR class="small"><TD>';
			if($hr!="_total_") {if(!$transpose) echo helpers__pay_account_longname($hr); else echo dossier_name($hr,1);}
			else echo lang('total');
			echo '</TD>';
					foreach ($hcols as $k=>$h)
						if(true) {
						echo '<td';
						if($hr!="_total_") echo ' class="small"';
						else echo ' style="border-top-width:2px"';
						echo'>';
						if($hr!="_total_") $cline=orsee_query("SELECT SUM(amount) as amount FROM ".table("pay_accounts")." WHERE $rowwhere='$hr' AND $colwhere='$h'".$supwhere);
						else $cline=orsee_query("SELECT SUM(amount) as amount FROM ".table("pay_accounts")." WHERE $colwhere='$h'".$supwhere);
						$amount=(empty($cline["amount"]))?0:$cline["amount"];
						if($hr=="_total_")  echo '<a href="'.thisdoc().'?'.$collink.'='.$h.$suplink_base.$suplink_bank.'&transpose=1'.'">';
						else echo '<a href="'.thisdoc().'?'.$collink.'='.$h.'&'.$rowlink.'='.$hr.'">';
						echo $amount."&nbsp;".lang('money_symbol');
						echo '</a>'; //if($hr=="_total_")  
						echo '</td>';
					}
			if($hr!="_total_") $rline=orsee_query("SELECT SUM(amount) as amount FROM ".table("pay_accounts")." WHERE $supwheretot AND $rowwhere='$hr'".$supwhere);
			else  $rline=orsee_query("SELECT SUM(amount) as amount FROM ".table("pay_accounts")." WHERE $supwheretot".$supwhere);
			echo '<TD>';
			if($hr!="_total_") echo '<a href="'.thisdoc().'?'.$rowlink.'='.$hr.$suplink_base.$suplink_bank.''.'">';
			$amount=(empty($rline["amount"]))?0:$rline["amount"];
			echo $amount."&nbsp;".lang('money_symbol');
			if($hr!="_total_") echo '</a>';
			echo '</TD></TR>';
			
		}
		echo '</table>';
		
	}
	else
	{
		$money_types=array(); $nominals=array(); $money_names=array(); 
		$use_p_files=(count($p_files)>count($p_accs) || (count($p_files)==count($p_accs) && $limit_account && !$limit_file));
		$p_data=array_map("helpers__pay_account_longname",$p_accs); $p_data_init=$p_accs; $limit_data=$limit_file?$limit_file:$p_files[0]; $p_data_col="shortname"; $p_data_altcol="payment_file"; $limlink='limacc'; $limaltlink='limfile'; $p_data_n=array(); $p_data_ntotals=array(); //$p_data_sums=array(); 
		$row_sums=array(); $col_sums=array(); //$p_data_amounts=array(); 
		if($use_p_files){$p_data_init=$p_files; $whatuseforlimit=(empty($p_accs[0]))?"":$p_accs[0]; if($whatuseforlimit==="" && empty($p_files[0])) $whatuseforlimit=$p_files[0];  $limit_data=$limit_account?$limit_account:$whatuseforlimit; $p_data=array_map(create_function('$x','return dossier_name($x,1);'),$p_files); $p_data_col0=$p_data_col; $p_data_col=$p_data_altcol; $p_data_altcol=$p_data_col0; unset($p_data_col0); $limlink0=$limlink; $limlink=$limaltlink; $limaltlink=$limlink0; unset($limlink0);}
		while ($pbl=mysqli_fetch_assoc($result)) {
			$ctype=($pbl["unit"]=="cash")?"nominal":$pbl["unit"];
			$cmoneyname=($pbl["unit"]=="cash")?$pbl["nominal"].'&nbsp;'.lang('money_symbol'):$pbl["unit"].'&nbsp;('.((float)$pbl["nominal"]).''.lang('money_symbol').')';
			if(!in_array($cmoneyname,$money_names) ) {
				$money_types[]=$ctype; $nominals[]=$pbl["nominal"]; 
				$money_names[]=$cmoneyname;
				foreach($p_data as $k=>$pdname) {
					$fi=$cmoneyname; $si=$pdname;
					if($transpose) {$fi=$pdname; $si=$cmoneyname;}
					$cquery="SELECT * FROM ".table("pay_accounts")." WHERE $p_data_col='".$p_data_init[$k]."' AND $p_data_altcol='$limit_data' AND unit='".$pbl["unit"]."' AND nominal='".$pbl["nominal"]."'";
					$cline=orsee_query($cquery);
					// var_dump($cline,$cquery);
					$cnunits=(empty($cline["n_units"]))?0:$cline["n_units"];
					$camount=(empty($cline["amount"]))?0:$cline["amount"];
					$p_data_n[$fi][$si]=$cline["n_units"];
					// $p_data_amounts[$fi][$si]=$cline["amount"];
					// $initsum=isset($p_data_sums[$pdname])?$p_data_sums[$pdname]:0;
					// $p_data_sums[$pdname]=$initsum+$camount;
					$initn=isset($p_data_ntotals[$cmoneyname])?$p_data_ntotals[$cmoneyname]:0;
					$p_data_ntotals[$cmoneyname]=$initn+$cnunits;
					$initrsum=isset($row_sums[$fi])?$row_sums[$fi]:0;
					$row_sums[$fi]=$initrsum+$camount;
					$initcsum=isset($col_sums[$si])?$col_sums[$si]:0;
					$col_sums[$si]=$initcsum+$camount;
				}
			}
		}
		// print_r($money_names);print_r($p_data); var_dump($transpose);
		// print_r($p_data_n);
		// print_r($hrows); echo '<br>hcols:'; print_r($hcols); var_dump(in_array('_total_',$hrows));
		$pluri_data=(count($p_data)>1 || !$limit_account || !$limit_file);
		$hrows=$money_names; $hcols=$p_data; $rowname=lang('nominal_values'); $colname=($p_data_col=="shortname")?lang('account'):lang($p_data_col);
		if($transpose) {$hrows=$p_data; $hcols=$money_names; $initrowname=$rowname; $rowname=$colname; $colname=$initrowname;}
		if($pluri_data) {if($transpose) $hrows[]='_total_'; else $hcols[]='_total_';} 
		$hcols[]='_totalsum_'; $hrows[]='_totalsum_';
		
		echo '<BR>
			<table border=1 style="border-collapse:collapse;text-align:center;vertical-align:middle">
				<TR>
						<td class="small">';
								echo $rowname.'\\'.$colname;
						echo '</td>';

					foreach ($hcols as $k=>$hc)
						if(true) {
						echo '<td class="small">';
						if($hc!="_total_" && $hc!="_totalsum_") {
							if($colname!=lang('nominal_values') && $pluri_data) echo '<a href="'.thisdoc().'?'.$limaltlink.'='.$limit_data.'&'.$limlink.'='.$p_data_init[$k].$suplink_transp.'">';
							if($pluri_data || $colname==lang('nominal_values')) echo $hc;
							elseif(!$pluri_data) echo lang('number_count');
							if($colname!=lang('nominal_values') && $pluri_data) echo '</a>';
						}
						elseif($hc=="_total_") echo lang('total');
						else echo lang('sum');
						echo '</td>';
					}
				echo '</TR>';
		foreach ($hrows as $kr=>$hr) {
			echo '<TR class="small"><TD>';
			if($rowname!=lang('nominal_values') && $pluri_data && substr($hr,0,6)!="_total") echo '<a href="'.thisdoc().'?'.$limaltlink.'='.$limit_data.'&'.$limlink.'='.$p_data_init[$kr].$suplink_transp.'">';
			if($hr!="_total_" && $hr!="_totalsum_") {if($pluri_data || $rowname==lang('nominal_values')) echo $hr; elseif(count($p_data)<=1) echo lang('number_count');}
			elseif($hr=="_total_") echo lang('total');
			else echo lang('sum');
			if($rowname!=lang('nominal_values') && $pluri_data && substr($hr,0,6)!="_total") echo '</a>';
			echo '</TD>';
					foreach ($hcols as $k=>$hc)
						if(true) {
						echo '<td';
						if(substr($hr,0,6)!="_total" && substr($hc,0,6)!="_total") echo ' class="small"';
						elseif(substr($hr,0,6)=="_total" ) echo ' style="border-top-width:2px"';
						echo'>';
						// if($hr=="_total_")  echo '<a href="'.thisdoc().'?'.$collink.'='.$h.'">';
						// var_dump(in_array('_total_',$hcols));
						if(substr($hr,0,6)!="_total" && substr($hc,0,6)!="_total") echo $p_data_n[$hr][$hc];
						elseif($hr=="_totalsum_"&& substr($hc,0,6)!="_total") {if($pluri_data || $colname==lang('nominal_values'))  echo $col_sums[$hc].''.lang('money_symbol');}
						elseif($hr=="_total_"&& substr($hc,0,6)!="_total") echo $p_data_ntotals[$hc];
						elseif($hc=="_total_"&& substr($hr,0,6)!="_total") echo $p_data_ntotals[$hr];
						elseif($hc=="_totalsum_"&& substr($hr,0,6)!="_total") {if($pluri_data ||  $rowname==lang('nominal_values')) echo $row_sums[$hr].''.lang('money_symbol');}
						elseif($hc=="_totalsum_" && $hr=="_totalsum_") echo "<u>".((array_sum($row_sums)+array_sum($col_sums))/2).''.lang('money_symbol')."</u>";
						// if($hr=="_total_")  echo '</a>';
						echo '</td>';
					}
			echo '</TR>';
			
		}
		
		echo '</table><BR><BR>
		<A href="javascript:history.back()">'.icon('back').' '.lang('back').'</A><BR><BR>
		<A href="'.thisdoc().'">'.icon('back').' '.lang('state_of_accounts_main_page').'</A><BR><BR>';

	}
	
/*

	
	
	
	echo '<BR>
		<table border=1 style="border-collapse:collapse">
			<TR>
 				';
        		foreach ($colnames as $colkey=>$colname) if($columns[$colkey]!="archived" || !empty($_REQUEST['all']))
					if(empty($not_in_summary[$columns[$colkey]])) {
                	echo '<td class="small">
							'.$colname.'
					</td>';
				}

	echo '			<TD></TD>
			</TR>';

	while ($line=mysqli_fetch_assoc($result)) {

   		echo '	<tr class="small"'; 
   		if ($shade) echo ' bgcolor="'.$color['list_shade1'].'"';
		else echo ' bgcolor="'.$color['list_shade2'].'"';
   		echo '>';
		foreach ($columns as $col) if($col!="archived" || !empty($_REQUEST['all'])) 
		if(empty($not_in_summary[$col])) {
			$colval=$line[$col];
			if(substr($col,-8)=="_account") {$colval=helpers__pay_account_longname($colval);}
			$supstart=''; $supend='';
			if($col=='amount') {$supstart= '<A HREF="'.thisdoc().'?addit=1&id='.$line['id'].'&details=1&dossier='.$line['payment_file'].'"><small>'; $supend='</small></A>';}
			if($col=="amount") { $colval.='&nbsp;'.lang('money_symbol');}
			if($col=="payment_file") $colval=dossier_name($colval,1);
			if($col=="remarks") { $colval=preg_replace("/^,+|,+$/","",$colval); if(!empty($line["replacement_file"])) {if(trim($colval)!="") $colval.="<br>"; $colval.=lang('replaces_the_file').' '.dossier_name($line["replacement_file"],1);} }
			if($col=="date_time") { $colval=date("d/m/Y H:i:s",$colval);}
			// if($col=="money_responsible" || $col=="experimenter") $line[$col]=admin__load_name_by_val($line[$col],true); //,$col!="experimenter"
        	echo '	<td class="small" valign=top';
            if ($shade) echo ' bgcolor="'.$color['list_shade1'].'"'; 
			else echo ' bgcolor="'.$color['list_shade2'].'"';
            echo '>';
			echo $supstart;
			if (isset($chnl2br) && $chnl2br) echo nl2br(stripslashes($colval));
            else echo stripslashes($colval);
			echo $supend;
            echo '</td>';
		}
   		echo '		<TD>
					<A HREF="'.thisdoc().'?addit=1&id='.$line['id'].'&details=1&dossier='.$line['payment_file'].'">'.
						lang('details').'</A>
				</TD>
   			</tr>';
   		if ($shade) $shade=false; else $shade=true;
		}
		
		}
	   else {
		echo '	<tr>
				<td style="text-align:center" colspan='.(count($columns)+1).'>
					'.lang('no_lines').'
				</td>
			</tr>';
		}
*/
	echo '

		</center>';

include_once ("footer.php");



?>

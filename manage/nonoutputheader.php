<?php
// part of orsee. see orsee.org
session_start();
$_DEFAULT_SESSION=$_SESSION;
session_write_close();

include ("../config/settings.php");
include ("../config/system.php");
include ("../config/requires.php");
include ("../config/participant_form.php");

	$document=thisdoc();
	if ($settings__stop_admin_site=="y" && $document!="error_temporaly_disabled.php") 
		redirect("errors/error_temporaly_disabled.php");

	site__database_config();

	$settings=load_settings();
	$settings['style']=$settings['orsee_admin_style'];
	$color=load_colors();

	session_set_save_handler("orsee_session_open", "orsee_session_close", "orsee_session_read", "orsee_session_write", "orsee_session_destroy", "orsee_session_gc");

	session_start();

	if (isset($_SESSION['expadmindata'])) $expadmindata=$_SESSION['expadmindata']; else $expadmindata=array();

	if(isset($_SESSION["userAgentSessInfo"])) {
		if($_SESSION["userAgentSessInfo"]!=get_userinfo_line()) {
			$expadmindata=array(); 
			$mailmessage="Possible attempt of an illegal access to the admin area is detected."."<br><br>";
			$mailmessage.="Accout : ".$expadmindata['adminname']."<br>";
			$mailmessage.="First connection data : ".$_SESSION["userAgentSessInfo"]."<br>";
			$mailmessage.="Second connection data : ".get_userinfo_line()."<br>";
			$mailmessage.="Date and time : ".date("d/m/Y H:i:s")."<br>";
			$mailmessage.="REQUEST_URI :".$_SERVER['REQUEST_URI']."<br>";
			$mailmessage.="Header used : nonoutputheader.php"."<br>";
			session_destroy();
			$to      = $settings['support_mail'];
			$subject = 'Possible attemt of an illegal connection to the admin area';
			$headers = 'From: '.$settings['support_mail'] . "\r\n" .
			"Content-type: text/html; charset=utf-8" . "\r\n".
			'Reply-To: '.$settings['support_mail'] . "\r\n" .
			'X-Mailer: PHP/' . phpversion();
			mail($to, $subject, $mailmessage, $headers);
		}
	}

	// Check for login
	if ((!(isset($expadmindata['adminname']) && $expadmindata['adminname'])) && $document!="admin_login.php") 
		redirect ($GLOBALS['settings__admin_folder']."/admin_login.php");

	if (isset($_REQUEST['new_language'])) {
		$expadmindata['language']=$_REQUEST['new_language'];
		$_SESSION['expadmindata']=$expadmindata;
		}

	if (!isset($expadmindata['language'])) 
		$expadmindata['language']=$settings['admin_standard_language'];

	$authdata['language']=$expadmindata['language'];
	$_SESSION['authdata']=$authdata;

	$lang=load_language($expadmindata['language']);

	if (!isset($title)) $title="";
	$pagetitle=$settings['default_area'].': '.$title;

?>

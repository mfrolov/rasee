<?php
// part of orsee. see orsee.org
ob_start();

$menu__area="experiments";
$title="assign participants";
$query_modules=array("field","field2","noshowups","nr_participations","subjectpool",
			"participant_form_fields",
			"experiment_classes","experiment_participated_or","experiment_participated_and","experiment_assigned_or","experiment_registered_future_or","last_invitation","last_activity","rand_subset");

include ("header.php");

	if ($_REQUEST['experiment_id']) $experiment_id=$_REQUEST['experiment_id'];
                else redirect($GLOBALS['settings__admin_folder']."/experiment_main.php");

	$allow=check_allow('experiment_assign_participants','experiment_show.php?experiment_id='.$experiment_id);

	$experiment=orsee_db_load_array("experiments",$experiment_id,"experiment_id");

	if (!check_allow('experiment_restriction_override'))
		check_experiment_allowed($experiment,$GLOBALS['settings__admin_folder']."/experiment_show.php?experiment_id=".$experiment_id);

	$query_modules=query__get_participant_form_modules($query_modules,$experiment_id);
	// var_dump($query_modules);
	echo '	<center>
		<BR><BR>
			<h4>'.$experiment['experiment_name'].'</h4>
			<h4>'.$lang['assign_subjects'].'</h4>
		';
    $unneeded_in_save=["show","request_name","save_request","delete_request","delete_request_id","new_query","deleted"];
	
	if(!isset($_REQUEST['use'])) $_REQUEST['use']=array();
	if(!isset($_REQUEST['con'])) $_REQUEST['con']=array();
	if(!isset($_REQUEST['new'])) $_REQUEST['new']=""; //var_dump($_REQUEST['use']);
	if ((isset($_REQUEST['addselected']) && $_REQUEST['addselected']) || (isset($_REQUEST['addall']) && $_REQUEST['addall'])) {
	
	
		// data base queries for assign ...

		$assign_ids=$_SESSION['assign_ids'];

		if (isset($_REQUEST['addall']) && $_REQUEST['addall']) {

		$assigned_count=count($assign_ids);
		$instring=implode("','",$assign_ids);

			}
		elseif (isset($_REQUEST['addselected']) && $_REQUEST['addselected']) {
			$selected_ids=array();
			$i=0;
			foreach ($assign_ids as $id) {
				$i++;
				if (isset($_REQUEST['p'.$i]) && $_REQUEST['p'.$i]==$id) $selected_ids[]=$id;
				}
                	$assigned_count=count($selected_ids);
                	$instring=implode("','",$selected_ids);
			}

		$query="INSERT INTO ".table('participate_at')." (participant_id,experiment_id) 
                        SELECT participant_id, '".$experiment_id."' 
                        FROM ".table('participants')." 
			WHERE participant_id IN ('".$instring."') ";
		$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

                log__admin("experiment_assign_participants","experiment:".$experiment['experiment_name']);

		$_SESSION['assign_ids']=array();
		message($assigned_count.' '.$lang['xxx_participants_assigned']);
		redirect ($GLOBALS['settings__admin_folder'].'/'.thisdoc().'?experiment_id='.$experiment_id);

		}

	elseif (!empty($_REQUEST['show'])) {

		$sort = (isset($_REQUEST['sort']) && $_REQUEST['sort']) ? $_REQUEST['sort']:"lname,fname,email";

		if ((isset($_REQUEST['new_query']) && $_REQUEST['new_query']) || (!isset($_REQUEST['assign_select_query']) || !$_SESSION['assign_select_query'])) {
			unset($_REQUEST['new_query']);
	       	$where_clause=query__where_clause($query_modules,
							  $_REQUEST['use'],
							  $_REQUEST['con'],$experiment_id);

			if (!$where_clause) $where_clause=query__where_clause_module("all");

			$join_phrase=query__join_assign($experiment,
							$query_modules,
							$_REQUEST['use']);

			$orderlimit=query__orderlimit($query_modules,$_REQUEST['use']);
			
			$where_clause.=" AND ((".table('participants').".remarks NOT LIKE '%never_invite%' AND ".table('participants').".remarks NOT LIKE '%never_".$experiment['experiment_ext_type']."%') OR ".table('participants').".remarks IS NULL)";

			$select_query="SELECT ".table('participants').".* 
                        	FROM ".table('participants')." ".
				$join_phrase." ".
                        	$where_clause." ".
				$orderlimit;

                	$_SESSION['assign_where_clause']=$where_clause;
			$_SESSION['assign_select_query']=$select_query;
			$_SESSION['assign_request']=$_REQUEST;
			// var_dump($_SESSION['assign_request']);
			$arrtosave=$_SESSION['assign_request'];
			foreach($unneeded_in_save as $us) {
				if(isset($arrtosave[$us])) unset($arrtosave[$us]);
			}
			$jsar=json_encode($arrtosave);
			rasee_db_save_array(array('json'=>$jsar,'user'=>$expadmindata['adminname']),'experiment_queries',array("experiment_id"=>$experiment_id,"name"=>"last_used"));
		}
	    else {
			$where_clause=$_SESSION['assign_where_clause'];
					$select_query=$_SESSION['assign_select_query'];
		}

		echo  '<FORM name="part_list" method=post action="'.thisdoc().'">
                <INPUT type=hidden name=experiment_id value="'.$experiment_id.'">';

		script__part_list_checkall();

		$assign_ids=query_show_result($select_query,$sort,"assign");
		$_SESSION['assign_ids']=$assign_ids;

		$count_results=count($assign_ids);

		if(check_allow('participants_show')) echo '<TABLE border=0>
			<TR>
				<TD align=left>
                		<input type=button value="'.$lang['select_all'].
					'" onClick="checkAll(\'p\','.$count_results.')">
                		<br>
                		<input type=button value="'.$lang['select_none'].
					'" onClick="uncheckAll(\'p\','.$count_results.')">
                		</TD>
				<TD colspan=2></TD>
			</TR>
                	<TR>
				<TD></TD>
				<TD align=center>
                		<INPUT type=submit name="addselected" 
					value="'.$lang['assign_only_marked_participants'].'">
                		</TD>
                		<TD align=center>
                		<INPUT type=submit name="addall" 
					value="'.$lang['assign_all_participants_in_list'].'">
                		</TD>
			</TR>
		     </TABLE>';
		else echo '<center><INPUT type=submit name="addall" value="'.$lang['assign_all_participants_in_list'].'"></center>';

		echo '</FORM>';

		}

	else 	{
		
		if (!empty($_REQUEST['use_saved_request'])) {
			$jssr=rasee_db_load_value('experiment_queries',$_REQUEST['use_saved_request'],'id','json');
			$_SESSION['assign_request']=json_decode($jssr,true);
		}
		elseif((!empty($_SESSION['assign_request']) || !empty($_REQUEST['request_name'])) && empty($_REQUEST['new'])) {
			if (!empty($_SESSION['assign_request'])) {
				$jsar=json_encode($_SESSION['assign_request']);
				$exp_queries=orsee_query("SELECT * FROM ".table('experiment_queries'). " WHERE experiment_id=$experiment_id OR experiment_id=0 ORDER BY id DESC", "return_same"); //(experiment_id=0 and `user`='".$expadmindata['adminname']."') 
				$used_request=[];
				if($exp_queries!==false) foreach($exp_queries as $eq) {
					$allequal=true;
					$prevsess=json_decode($eq["json"],true);
					foreach($_SESSION['assign_request'] as $kar=>$sar) if(!in_array($kar,$unneeded_in_save)) {
						if(!isset($prevsess[$kar]) || $prevsess[$kar]!=$sar) {
							$allequal=false;
							// var_dump("error:",$kar);
							break;
						}
					}
					if($allequal) {
						$_REQUEST['use_saved_request']=$eq["id"];
						$used_request=$eq;
						break;
					}
				}
			}
			if(!empty($_REQUEST['request_name']) && !empty($experiment_id) ) {
				$arrtosave=$_REQUEST;
				foreach($unneeded_in_save as $us) {
					if(isset($arrtosave[$us])) unset($arrtosave[$us]);
				}
				$jsar=json_encode($arrtosave);
				// var_dump($_SESSION['assign_request']);
				rasee_db_save_array(array('json'=>$jsar,'user'=>$expadmindata['adminname']),'experiment_queries',array("experiment_id"=>$experiment_id,"name"=>trim($_REQUEST['request_name'])));
				message("<q>".trim($_REQUEST['request_name']). "</q> ". lang("saved",false));
				redirect($GLOBALS['settings__admin_folder'].'/'.thisdoc().'?experiment_id='.$experiment_id);
			}
			elseif(!empty($_REQUEST['delete_request']) && !empty($_REQUEST['delete_request_id']) && isset($used_request['id']) && $_REQUEST['delete_request_id'] == $used_request['id'] && ((isset($used_request['user']) && $expadmindata['adminname']==$used_request['user']) || check_allow('experiment_delete')) ) {
				orsee_query("DELETE FROM ".table('experiment_queries')." WHERE `id`='".$used_request['id']."'");
				message("<q>".$used_request['name']. "</q> ". lang("deleted",false));
				redirect($GLOBALS['settings__admin_folder'].'/'.thisdoc().'?experiment_id='.$experiment_id);
				
			}

		}

		if (!isset($_SESSION['assign_request']) || !empty($_REQUEST['new'])) $_SESSION['assign_request']=array();
			else {
				if (!isset($_SESSION['assign_request']) || !is_array($_SESSION['assign_request'])) $_SESSION['assign_request']=array();
				if(isset($_SESSION['assign_request']['use']) && isset($_REQUEST['use']) && empty($_REQUEST['use'])) unset($_REQUEST['use']);
				if(isset($_SESSION['assign_request']['con']) && isset($_REQUEST['con']) && empty($_REQUEST['con'])) unset($_REQUEST['con']);
				if(isset($_SESSION['assign_request']['new']) && isset($_REQUEST['new']) && empty($_REQUEST['new'])) unset($_REQUEST['new']);
				if(isset($_SESSION['assign_request']['use_saved_request']) && isset($_REQUEST['use_saved_request']) && empty($_REQUEST['use_saved_request'])) unset($_REQUEST['use_saved_request']);
				$new_req=array_merge($_SESSION['assign_request'],$_REQUEST);
				// var_dump($new_req);
				// var_dump($_SESSION['assign_request']);
				$_REQUEST=$new_req;
				$_SESSION['assign_request']=$_REQUEST;
				if(isset($_SESSION['assign_request']['use_saved_request'])) unset ($_SESSION['assign_request']['use_saved_request']);
				}
		$_SESSION['assign_ids']=array();
		$exptypes=load_external_experiment_type_names(false);
		$wstring="subscriptions LIKE '%".$experiment['experiment_ext_type']."%'";
		echo participants__count_participants($wstring);
		echo ' '.$lang['xxx_part_in_db_for_xxx_exp'].' ';
		echo $exptypes[$experiment['experiment_ext_type']];
		echo '<BR><BR>';
		echo experiment__count_participate_at($experiment_id).' '.
        		$lang['participants_assigned_to_this_experiment'];
		echo '
			<BR><BR>
	
        		<FORM action="'.thisdoc().'" method="POST">
			<INPUT type=hidden name="new_query" value="true">
			<INPUT type=hidden name="experiment_id" value="'.$experiment_id.'">';
        		query__form($query_modules,$experiment);

        	echo '	</FORM>';

		}
	echo '	<A HREF="experiment_show.php?experiment_id='.$experiment_id.'">
			'.$lang['mainpage_of_this_experiment'].'</A><BR><BR>

		</CENTER>';

include ("footer.php");

?>

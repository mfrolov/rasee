<?php

ob_start();

$menu__area="experiments";
$title="Download Log Table";
define("NO_HTML_HEADER","1");
include("nonoutputheader.php");

define("no_utf8","1");
require_once 'excel_writer.php';

$table='expe_paypal_log'; 
if(isset($_GET['option']) && $_GET['option']=="documentaire") $table='expe_documentaire_log'; 
if(isset($_GET['option']) && $_GET['option']=="FNAC") $table='expe_fnac_log'; 
if(isset($_GET['option']) && $_GET['option']=="supquest") $table='supquestlogtable'; 
if(isset($_GET['option']) && $_GET['option']=="eeg") $table='expe_eeg_log'; 
if(isset($_GET['option']) && $_GET['option']=="address") $table='expe_address_log'; 
if(isset($_GET['option']) && $_GET['option']=="english") $table='expe_english_log'; 
if(isset($_GET['option']) && $_GET['option']=="info") $table='expe_supinfo_log'; 
if(isset($_GET['option']) && $_GET['option']=="2days") $table='expe_2days_log'; 
if(isset($_GET['option']) && $_GET['option']=="rib") $table='expe_rib_log'; 
if(isset($_GET['option']) && $_GET['option']=="description") $table='descriptionlogtable'; 
if(isset($_GET['option']) && $_GET['option']=="redirect") $table='redirectlogtable'; 
if(isset($_GET['option']) && $_GET['option']=="lydia") $table='expe_lydia_log'; 
if(isset($_GET['option']) && $_GET['option']=="forcewin") $table='winchecklogtable'; 
// if(isset($_GET['option']) && $_GET['option']=="redirect2") $table='redirect2logtable'; 
if(isset($_GET['option']) && substr($_GET['option'],0,9)=="supquest_") $table=$_GET['option']; 
$wclause=""; $mode="html";

$tablename=$table;

//if(isset($_POST["whereclause"]) && $_POST["whereclause"]!="") {$wclause="WHERE ".$_POST["whereclause"]; $tablename="searchresults";}
function contains($whole,$part) {
$w=strtolower($whole); $p=strtolower($part);
if(str_replace($p,"",$w)!=$w) {return TRUE;}
else {return FALSE;}
}

$nonauthorizedArray=array("update ","alter ","delete ","drop ","create table ","select ");
for($na=0; $na<count($nonauthorizedArray); $na++) {if(contains($wclause,$nonauthorizedArray[$na])) die("Not authorized");}

//$ERSHOW=true;
$result_table=mysqli_query($GLOBALS['mysqli'],"SHOW TABLES LIKE '$table'");
$nrows_table = mysqli_num_rows($result_table);
if($nrows_table==0)
{
	if($_GET['option']=="info") {$table='expe_rib_log'; $result_table=mysqli_query($GLOBALS['mysqli'],"SHOW TABLES LIKE '$table'"); $nrows_table = mysqli_num_rows($result_table);}
	if($nrows_table==0) die("Table not found");
}
$tablename=$table;
//$ERSHOW=true;
$result=mysqli_query($GLOBALS['mysqli'],"SELECT * FROM $table $wclause");
$nfields = mysqli_num_fields($result);

$fnames=array();

for ($i = 0; $i < $nfields; $i++){
    $fname=mysqli_fetch_field($result)->name;
	$fnames[$i]=$fname;
}


$xls = new Excel($tablename,$mode);
$xls->top();
$xls->home();
$xls->headers=true;
$xls->writeLine($fnames);

while ($row = mysqli_fetch_row($result)) {
$xls->writeLine($row);
}

$xls->send();

unset($xls);

?>
<?php
ob_start();

if (isset($_REQUEST['id'])) $id=$_REQUEST['id']; else $id="";

$menu__area="dossiers_paiement";
$title="dossiers paiement ";
include ("header.php");

$dossier_tablename=table("payment_files");

	$done=false;
	$allow_cat="experimentclass";
    
	$allow=check_allow($allow_cat.'_edit','options_main.php');

	$header=lang('payment_files');
	if(!empty($id)) $header=lang('payment_file').' '.dossier_name($id,1);
	// $where="";
	$where=" WHERE closed = 0 ";
	if(!empty($_REQUEST['all'])) $where="";
	$order='year DESC,number DESC';
	$show_part_stats=false;
	$show_loans=false; $loanlines=array();
	
	
	if(!empty($_REQUEST['show_loans']) || !empty($_REQUEST['show_statistics'])) {
	if(query_column_exist(table("participate_at"),"payment_file,payment,payment_nlf") && query_column_exist(table("sessions"),"payment_file")) {
		$show_loans=true;
		$query="SELECT DISTINCT ".table("participate_at").".payment_file as preteur, ".table("sessions").".payment_file as emprenteur,
		SUM( CAST(".table("participate_at").".payment AS DECIMAL(10,2))) as somme_cash, SUM( CAST(".table("participate_at").".payment_nlf AS DECIMAL(10,2))) as somme_noncash
		FROM ".table("participate_at").", ".table("sessions")." 
		WHERE ".table("participate_at").".session_id = ".table("sessions").".session_id
		AND ".table("participate_at").".payment_file<>'' AND ".table("sessions").".payment_file<>''
		AND ".table("participate_at").".payment_file<>".table("sessions").".payment_file
		GROUP BY ".table("participate_at").".payment_file, ".table("sessions").".payment_file";
		$loanlines=orsee_query($query,"return_same");
		if(empty($_REQUEST['addit'])) {
			echo '<center><h4>'.lang('loans_between_files');
			echo ' (<a href="'.thisdoc().'">'.lang('hide').'</a>)';
			echo '</h4>';
			$n_loans=0;
			if(count($loanlines)>0) foreach ($loanlines as $line) if(empty($where) || !dossier_closed($line['preteur']) || !dossier_closed($line['emprenteur']))
			{
				$n_loans++;
				echo "<br><u>".dossier_name($line['preteur'],1)."</u>&nbsp;&nbsp;<strong><big>&rarr;</big></strong>&nbsp;&nbsp;<u>".dossier_name($line['emprenteur'],1)."</u>&nbsp;&nbsp;:&nbsp;&nbsp;";
				if($line['somme_noncash']>0) echo 'cash:&nbsp;';
				echo $line['somme_cash'].'&nbsp;'.lang('money_symbol');
				if($line['somme_noncash']>0)  {
					echo ',&nbsp;&nbsp;non&nbsp;cash:&nbsp;'.$line['somme_noncash'].'&nbsp;'.lang('money_symbol');
					echo '.&nbsp;&nbsp;Total:&nbsp;&nbsp;'.($line['somme_noncash']+$line['somme_cash']).'&nbsp;'.lang('money_symbol');
				}
			}
			if($n_loans==0) echo lang('no_loans');
			echo '</center>';
		}
	} elseif(empty($_REQUEST['addit'])) echo '<center>'.lang('no_loans').'</center>';
	}
	elseif(empty($_REQUEST['addit'])) {
	
	echo '<p align=right style="margin-right:10%"><a href="'.thisdoc().'?show_loans=1';
	if(!empty($_REQUEST['all'])) echo '&all=1';
	echo '">'.lang('show').' '.lang('loans_between_files',false).'</a></p>';
	
	}

	$dossier_stat_headers=array('n_participants'=>'N '.lang('participants'),
	'spent_cash'=>lang('spent').' ('.lang('cash',false).')',
	'spent_noncash'=>lang('spent').' ('.lang('non_cash',false).')',
	'left_money'=>lang('left_money')
	);
	if($show_loans) $dossier_stat_headers['left_money_loans']='<span style="color:#555">'.lang('left_money').' ('.lang('loans_substructed',false).')</span>';
	$dossier_stat_headers['available_money']=lang('available_money');

	
	echo '<BR>
		<center><h4>'.$header.'</h4>';

	if (check_allow($allow_cat.'_add') && empty($_REQUEST["addit"])) {
		echo '	<BR>
			<form action="'.thisdoc().'">
			<INPUT type=submit name="addit" value="'.lang('create_new').'">
			</FORM>';
		}
		
		
	// load languages
	$columns=array("year", "number", "surname", "tranche", "budget", "n_part_needed", "experimenter", "money_responsible", "nominal_values", "nonliquid_funds_name", "remarks", "start_num_from", "closed");
	$column_types=array("varchar(11)", "int(11)", "text", "varchar(5)", "DECIMAL(10,2)", "varchar(15)", "varchar(50)", "varchar(50)", "varchar(250)", "varchar(250)", "text", "varchar(15)", "tinyint");
	$modifiable=array(1,1,1,1,1,1,3,4,1,1,1,1,2);
	$not_in_summary=array();
	$not_in_summary['nominal_values']=true;
	
	$query="CREATE TABLE IF NOT EXISTS $dossier_tablename (`id` INT(20) NOT NULL default 0)";
 	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
	
	query_makecolumns($dossier_tablename,$columns,$column_types);
	query_makecolumns(table('participate_at'),array("payment","payment_nlf","payment_num","payment_file"),array("varchar(20)","varchar(20)","varchar(20)","varchar(20)"), array("","","",""));
	$colnames=array();
	foreach ($columns as $col) $colnames[]=lang($col);
	
	if(!empty($_REQUEST["delete"]) && isset($_REQUEST["id"]) && $_REQUEST["id"]!=="") {

		log__admin("payment_files_delete","dossier ".dossier_name($id).", id: $id");
		$query="DELETE FROM $dossier_tablename WHERE `id`='$id'";
		$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
		$supmess="";
		if($id==1) $supmess="<br> ".lang('for_not_using').' '.lang('mix_account').' '.lang('close_this_file',false);
		message (lang('changes_saved').$supmess);
		redirect($GLOBALS['settings__admin_folder']."/".thisdoc());
		exit;
		// show_message();
	}
	$query001="SELECT `id` FROM ".$dossier_tablename." WHERE `id`=1";
	$checkline001=orsee_query($query001); $redirect_if_created=true;
	if(!isset($checkline001['id'])) {
		//create file for mixed accounts
		$_REQUEST["addoredit"]=true;
		$_REQUEST["new"]=true;
		$_REQUEST["id"]="1";
		$_REQUEST["year"]=lang('mix_account');
		$_REQUEST["number"]="1";
		$_REQUEST["surname"]=""; $_REQUEST["tranche"]="";
		$_REQUEST["budget"]="0";
		$_REQUEST["n_part_needed"]="";$_REQUEST["nonliquid_funds_name"]="";
		$_REQUEST["start_num_from"]="";
		$_REQUEST["nominal_values"]="200,100,50,20,10,5,2,1,0.5,0.2,0.1,0.05,0.02,0.01";
		$_REQUEST["remarks"]=lang("lab_safe for_mix_account");
		$_REQUEST["closed"]="0";
		$_REQUEST["experimenter"]=""; $_REQUEST["money_responsible"]="";
		$redirect_if_created=false;
		message(lang("creation_of lab_safe for_mix_account"));
		//var_dump($_REQUEST); exit;
	}
	
	
	if(!empty($_REQUEST["addoredit"])) {
		$expr=array();
		$dossier="";
		$new=!empty($_REQUEST['new']);
		if($new) {
			$query="SELECT `id` FROM ".$dossier_tablename."
							WHERE `id`=".$_REQUEST['id'];
			$checkline=orsee_query($query);
			$new=!isset($checkline['id']);
		}
		// var_dump($new);
		for($i=0; $i<count($columns); $i++) if($modifiable[$i]>0)
		{
			// $valnames[]=$columns[$i]; $values[]=$_REQUEST[$columns[$i]];
			$val=$_REQUEST[$columns[$i]];
			if($modifiable[$i]==2) $val=(int)$val;
			if(is_array($val)) $val=implode(",",$val);
			if($columns[$i]=="start_num_from" && trim($val)==="") $val=1;
			$expr[]="`".$columns[$i]."`='".$val."'";
			if($i<2) {if($i>0 && !empty($val)) $dossier.="_"; if($i==1) $dossier.="n"; $dossier.=$val;}
		}
		// var_dump($_REQUEST,$expr); exit;
		$query=($new)?"INSERT INTO ":"UPDATE ";
			// $valnames=array(); $values=array(); 
		$query.="$dossier_tablename SET ";
		if($new) $query.="`id`='".$_REQUEST['id']."', ";
		$query.=implode(", ",$expr);
		if(!$new) $query.="WHERE `id`='$id'";
		// var_dump($_REQUEST,$query);
		$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Query: $query <br> Database error: " . mysqli_error($GLOBALS['mysqli']));
		log__admin("payment_files_edit","dossier: $dossier");
		message ("Dossier ".dossier_name($_REQUEST["id"],1)." :<br>".lang('changes_saved'));
		if($redirect_if_created) {
			redirect($GLOBALS['settings__admin_folder']."/".thisdoc()); exit;
		}
		else {show_message();}
	}
	
	if(!empty($_REQUEST["id"])) {
	$show_stat=(!empty($_REQUEST["show_statistics"]));
	$margin_right="10%"; 
	$showhidephrase=($show_stat)?lang("hide_statistics"):lang("show_statistics");
	$statlinkplus="?id=".$_REQUEST["id"];
	if(!empty($_REQUEST["addit"])) $statlinkplus.="&addit=".$_REQUEST["addit"];
	if(!$show_stat) $statlinkplus.="&show_statistics=1";
	echo '<p align=right style="margin-right:'.$margin_right.'"><a href="'.thisdoc().$statlinkplus.'">'.$showhidephrase.'</a></p>';
	if($show_stat) {
		$extinfo_headers=array("assigned_exp"=>lang("assigned_experiments"),"used_exp"=>lang("experiments_used_it"));
		$allstat_headers=array_merge($extinfo_headers,$dossier_stat_headers,array("loans"=>lang("given_loans").'<br>('.lang('negative_for_borrowings',false).')'));
		$stat_info=statistics_info($_REQUEST["id"],$allstat_headers,$loanlines,"left_money");
		$line=orsee_query("SELECT * FROM ".table("payment_files")." WHERE `id`='".$_REQUEST["id"]."'");
		if(empty($line['nonliquid_funds_name'])) {
			unset($allstat_headers['spent_noncash']);
			$allstat_headers['spent_cash']=lang('spent');
		}
		else $allstat_headers['spent_noncash']=lang('spent').' ('.$line['nonliquid_funds_name'].')';
		echo '<BR> <table border=0 style="vertical-align:middle">';
		$shade=false;
		foreach($allstat_headers as $k=>$header) {
			if(($k=="left_money_loans" || $k=="loans") && count($loanlines)==0) continue;
			echo '<tr>';
			$tddeb= '	<td class="small" ';
            if ($shade) $tddeb.= ' bgcolor="'.$color['list_shade1'].'"'; 
			else $tddeb.= ' bgcolor="'.$color['list_shade2'].'"';
            $tddeb.= '>';
			echo $tddeb.$header.'</td>';
			echo $tddeb.$stat_info[$k].'</td>';
            echo '</tr>';
			$shade=!$shade;
		}
		echo '</table>';
		echo '<BR><BR><A href="'.thisdoc().'">'.icon('back').' '.lang('payment_files_main_page').'</A><BR>';
		if(!empty($_REQUEST['stat_only'])) {include_once ("footer.php"); exit;}

	}
	}
	
	if(!empty($_REQUEST["addit"])) {
		$new=!(isset($_REQUEST["id"]) && $_REQUEST["id"]!=="");
		if (!$new) $button_title=lang('change'); else $button_title=lang('add');
		$js_select_name=""; $form_name='payment_file';
		echo '<BR><FORM name='.$form_name.' action="'.thisdoc().'">
		<table border=0 style="align:center;vertical-align:middle">';
		$new_val=0;
		if ($new) {$new_val=1; $id=query_new_id("$dossier_tablename","`id`");}
		echo '<tr><td>'.lang('id').'</td><td>'.$id.'</td>';
		$initvals=array(); 
		for($i=0; $i<count($columns); $i++) //if($modifiable[$i]>0)
		{
			$val=''; $supcontrol='';
			if(!$new) {
				$query="SELECT ".$columns[$i]." FROM $dossier_tablename WHERE `id`='$id'";
				$forval=orsee_query($query);
				$val=$forval[$columns[$i]];
				if($id==1 && $columns[$i]=="year") {$modifiable[$i]=0; $colnames[$i]=""; $val=lang('mix_account');}
			}
			elseif($columns[$i]=="year") {
				$date14juil=mktime(0,0,0,7,14);
				$currdate=time();
				if($currdate>$date14juil) $val=date("Y")."/".date("Y",strtotime('+1 year'));
				else $val=date("Y",strtotime('-1 year'))."/".date("Y");
				$supcontrol=updown_control("updown",$columns[$i]);
			}
			elseif($columns[$i]=="number") {
				$supcontrol=updown_control("updown",$columns[$i]);
				$maxnumline=orsee_query("SELECT MAX(".$columns[$i].") as ".$columns[$i]." FROM $dossier_tablename WHERE `year`='".$initvals[$i-1]."'");
				$prevnumber=0;
				if(!empty($maxnumline[$columns[$i]])) $prevnumber=$maxnumline[$columns[$i]];
				$val=$prevnumber+1;
			}
			$initvals[$i]=$val;
			echo '<tr><td>'.$colnames[$i].'</td>';
			echo '<td>';
			$ntacols=50; $ntarows=1;
			if($columns[$i]=="remarks") {$ntarows=5;}
			if($modifiable[$i]==0) echo '<table><tr><td><textarea style="font-weight:bold" disabled name="'.$columns[$i].'_shown" rows='.$ntarows.' cols='.$ntacols.' >'.$val.'</textarea>'.'</td><td>'.$supcontrol.'</td></tr></table><input type=hidden name="'.$columns[$i].'" value="'.$val.'" />';
			if($modifiable[$i]==1) echo '<table><tr><td><textarea name="'.$columns[$i].'" rows='.$ntarows.' cols='.$ntacols.' >'.$val.'</textarea>'.'</td><td>'.$supcontrol.'</td></tr></table>';
			elseif($modifiable[$i]==2) {
				$checked=$val?"CHECKED":"";
				echo '<input type=checkbox name="'.$columns[$i].'" '.$checked.' value=1 >';
			}
			if($modifiable[$i]==3) {
				echo experiment__experimenters_checkbox_list($columns[$i],$val,true,"update_responsable");
				echo '<p align=center><a target=_blank href="admin_edit.php?new=true">'.lang('create_new').' '.lang('experimenter',false).'</a> ('.lang('then',false).' <a href="javascript:location.reload();">'.lang('reload_the_page',false).'</a>)</p>';
				// echo '<p>&nbsp;</p>';
			}
			if($modifiable[$i]==4) {
				$money_responsables=admin__load_names_by_rights('admin_edit');
				$responsables=array(""=>"-");
				if(!empty($initvals[$i-1])) $responsables+=admin__load_names_by_id($initvals[$i-1],true,"adminname");
				$responsables+=$money_responsables;
				$responsables+=admin__load_names_by_id($val,true,"adminname");;
				echo helpers__select_text($responsables,$columns[$i],$val);
				$js_select_name=$columns[$i];
				//echo '<p align=center><a target=_blank href="admin_edit?new=true">'.lang('create_new').' '.lang('experimenter',false).'</a> ('.lang('then',false).' <a href="javascript:location.reload();">'.lang('reload_the_page',false).'</a>)</p>';
			}
			echo '</td></tr>';
		}
		
		echo '</table>';
		echo '
			<TABLE>
				<TR>
					<TD COLSPAN=2 align=center>
						<INPUT name=addoredit type=submit value="'.$button_title.'">';
		if(true) echo '
						<INPUT type=hidden name="id" value="'.$id.'">
						<INPUT type=hidden name="new" value="'.$new_val.'">';

		echo '
					</TD>
				</TR>
			</table>
			</FORM>
			<BR>';
		script__update_responsable($form_name,$js_select_name);

		if (!$new && check_allow($allow_cat.'_delete')) {
			echo '<BR><BR><FORM action="'.thisdoc().'">
				<INPUT type=hidden name="id" value="'.$id.'">
				<INPUT type=hidden name="delete" id="delete_file" value="0">
				<INPUT type=button onclick="really_delete(this)" name="delete_button" value="'.lang('delete').'">
				</FORM>
				<SCRIPT> function really_delete(x){if(confirm("'.str_replace('?','',lang('do_you_really_want_to_delete')).lang('payment_file').dossier_name($id,1).'?")) {document.getElementById("delete_file").value=1; x.form.submit();}}</SCRIPT>';
			}
		echo '<BR><BR>
			<A href="'.thisdoc().'">'.icon('back').' '.lang('payment_files_main_page').'</A><BR><BR>
			</CENTER>';
		include_once ("footer.php");
		exit;
	}


	echo '<BR>
		<table border=1 style="border-collapse:collapse">
			<TR>
				<TD class="small">
					'.lang('id').'
            	</TD>
 				';
        		foreach ($colnames as $colkey=>$colname) if($columns[$colkey]!="closed" || !empty($_REQUEST['all']))
					if(empty($not_in_summary[$columns[$colkey]])) {
                	echo '<td class="small">
							'.$colname.'
					</td>';
				}
	foreach($dossier_stat_headers as $k=>$dsh) {
		echo '<td class="small">';
		echo $dsh;
		echo '</td>';
	}
	echo '			<TD></TD>
			</TR>';

	if ($show_part_stats) {
 		$num_p=array();
 		$query="SELECT ".$item." as type_p, 
 			count(*) as num_p 
 			FROM ".table('participants')." 
 			GROUP BY ".$item;
 		$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
 		while ($line=mysqli_fetch_assoc($result)) {
 			$num_p[$line['type_p']]=$line['num_p'];
 		}
 	}

	$query="SELECT *
      		FROM $dossier_tablename
		 ".$where."
      		ORDER BY ".$order;
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

	$shade=false;
	if (mysqli_num_rows($result)>0) {

	while ($line=mysqli_fetch_assoc($result)) {

   		echo '	<tr class="small"'; 
   		if ($shade) echo ' bgcolor="'.$color['list_shade1'].'"';
		else echo ' bgcolor="'.$color['list_shade2'].'"';
   		echo '>
				<td class="small" valign=top>
					<A HREF="'.thisdoc().'?addit=1&id='.$line['id'].'&show_statistics=1">
					<small>'.$line['id'].'</small>
					</A>
				</td>';
		foreach ($columns as $col) if($col!="closed" || !empty($_REQUEST['all'])) 
		if(empty($not_in_summary[$col])) {
			if($col=="money_responsible" || $col=="experimenter") $line[$col]=admin__load_name_by_val($line[$col],true); //,$col!="experimenter"
        	echo '	<td class="small" valign=top';
            if ($shade) echo ' bgcolor="'.$color['list_shade1'].'"'; 
			else echo ' bgcolor="'.$color['list_shade2'].'"';
            echo '>';
			if (isset($chnl2br) && $chnl2br) echo nl2br(stripslashes($line[$col]));
            else echo stripslashes($line[$col]);
            echo '</td>';
		}
		if ($show_part_stats) {
			if (isset($num_p[$line['content_name']])) $np=$num_p[$line['content_name']]; else $np=0;
			echo '<td class="small">'.$np.'</td>';
		}
        $query="SELECT COUNT(*) as num_p, SUM(CAST(payment AS DECIMAL(10,2))) as pay_cash, SUM(CAST(payment_nlf AS DECIMAL(10,2))) as pay_noncash FROM ".table('participate_at')." WHERE payment_file='".$line['id']."'";
		$stat_line=orsee_query($query);
        $query="SELECT COUNT(*) as num_p_part FROM ".table('participate_at')." WHERE payment_file='".$line['id']."' AND participated='y'";
		$stat_line_shownup=orsee_query($query);
		$stat_line["num_p_part"]=$stat_line_shownup["num_p_part"];
		foreach ($dossier_stat_headers as $k=>$dsh) {
        	echo '	<td class="small" valign=top';
            if ($shade) echo ' bgcolor="'.$color['list_shade1'].'"'; 
			else echo ' bgcolor="'.$color['list_shade2'].'"';
            echo '>';
			$keymap=explode('_',$k);
			$linktoreport=($keymap[0]=="spent" || $k=="left_money");
			if($linktoreport) echo '<a href="dossiers_paiement_report.php?dossier='.$line['id'].'"><small>';
			if($k=="n_participants") echo $stat_line["num_p_part"].'+'.($stat_line['num_p']-$stat_line["num_p_part"]);
			elseif($k=="spent_cash") echo $stat_line['pay_cash'];
			elseif($k=="spent_noncash") echo $stat_line['pay_noncash'];
			elseif($k=="left_money") echo $line['budget']-$stat_line['pay_cash']-$stat_line['pay_noncash'];
			elseif($k=="left_money_loans") {
				$reste=$line['budget']-$stat_line['pay_cash']-$stat_line['pay_noncash'];
				foreach ($loanlines as $ll) {
					if($ll['emprenteur']==$line['id']) $reste-=($ll['somme_cash']+$ll['somme_noncash']);
					if($ll['preteur']==$line['id']) $reste+=$ll['somme_cash']+$ll['somme_noncash'];
				}
				echo '<span style="color:#888">'.$reste.'</span>';
			}
			elseif($k=="available_money") {
				$available=0;
				operation::operation_tables();
				$accline=orsee_query("SELECT SUM(amount) as file_amount FROM ".table("pay_accounts")." WHERE shortname<>'bank' AND shortname<>'participants' AND payment_file='".$line['id']."'");
				if($accline!==false && !empty($accline['file_amount'])) $available=$accline['file_amount'];
				echo '<a target=_blank href="payment_accounts.php?limfile='.$line['id'].'"><small><span >'.$available.'&nbsp;'.lang('money_symbol').'</span></small></a>';
			}
			if($linktoreport) echo '</small></a>';
			//if($k>0 && $k<5) echo lang('money_symbol');
            echo '</td>';
		}
		// $dossier_stat_headers=array('N '.lang('participants'),
		// lang('spent'),
		// lang('left_money').' ('.mb_strtolower(lang('cash')).')',
		// lang('left_money').' ('.mb_strtolower(lang('non_cash')).')');
		// if(!empty($_REQUEST['show_loans'])) $dossier_stat_headers[]=lang('left_money').' ('.mb_strtolower(lang('non_cash')).')';
   		echo '		<TD>
					<A HREF="'.thisdoc().'?addit=1&id='.$line['id'].'">'.
						lang('edit').'</A>
				</TD>
   			</tr>';
   		if ($shade) $shade=false; else $shade=true;
		}
		
		}
	   else {
		echo '	<tr>
				<td style="text-align:center" colspan='.(count($columns)+count($dossier_stat_headers)+1).'>
					'.lang('no_payment_files').'
				</td>
			</tr>';
		}


	echo '</table>';
	if(true) {
		$query="SELECT *
				FROM $dossier_tablename
			 WHERE closed=1
				ORDER BY ".$order;
		$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
		$num_rows = mysqli_num_rows($result);

		if ($num_rows>0) {
			// echo '	<tr><td style="text-align:center" colspan='.(count($columns)+count($dossier_stat_headers)+1).'>';
			echo '<br><br><a href="'.thisdoc();
			if(empty($_REQUEST['all'])) {
				echo '?all=1';
				if(!empty($_REQUEST['show_loans'])) echo '&show_loans=1';
				echo '">';
				echo lang('show_also_closed_files');
				echo " ($num_rows)";
			}
			else {
				echo '">';
				echo lang('hide_closed_files');
			}
			echo '</a>';
			// echo '</td></tr>';
		}
	}	
	echo '

		</CENTER>';

include_once ("footer.php");

function statistics_info($id,$stat_headers,$loanlines=array(),$linktoreport=array()) {
		if(!is_array($linktoreport)) $linktoreport=explode(",",$linktoreport);
		$line=orsee_query("SELECT * FROM ".table("payment_files")." WHERE `id`='$id'");
	    $query="SELECT count(*) as num_p, SUM(CAST(payment AS DECIMAL(10,2))) as pay_cash, SUM(CAST(payment_nlf AS DECIMAL(10,2))) as pay_noncash FROM ".table('participate_at')." WHERE payment_file='".$id."'";
		$stat_line=orsee_query($query);
        $query="SELECT count(*) as num_p_part FROM ".table('participate_at')." WHERE payment_file='".$id."' AND participated='y'";
		$stat_line_shownup=orsee_query($query);
		$stat_line["num_p_part"]=$stat_line_shownup["num_p_part"];
		$res=array();
		foreach ($stat_headers as $k=>$dsh) {
			$res[$k]='';
			if(in_array($k,$linktoreport)) $res[$k].= '<a href="dossiers_paiement_report.php?dossier='.$id.'"><small>';
			if($k=="n_participants") $res[$k].= $stat_line["num_p_part"].'+'.($stat_line['num_p']-$stat_line["num_p_part"]);
			if($k=="spent_cash") $res[$k].= $stat_line['pay_cash'];
			if($k=="spent_noncash") $res[$k].= $stat_line['pay_noncash'];
			if($k=="left_money") $res[$k].= $line['budget']-$stat_line['pay_cash']-$stat_line['pay_noncash'];
			if($k=="available_money") {
				$available=0;
				operation::operation_tables();
				$accline=orsee_query("SELECT SUM(amount) as file_amount FROM ".table("pay_accounts")." WHERE shortname<>'bank' AND shortname<>'participants' AND payment_file='$id'");
				if($accline!==false && !empty($accline['file_amount'])) $available=$accline['file_amount'];
				$res[$k]= '<span style="color:#88B">'.$available.'&nbsp;'.lang('money_symbol').'</span> <small>(<a target=_blank href="payment_accounts.php?limfile='.$id.'"><small>'.lang('state_of_accounts',false).'</small></a>, <a target=_blank href="payment_operations.php?addit=1&dossier='.$id.'"><small>'.lang('new_operation',false).'</small></a>'.')</small>';
			}
			if($k=="left_money_loans") {
				$reste=$line['budget']-$stat_line['pay_cash']-$stat_line['pay_noncash'];
				foreach ($loanlines as $ll) {
					if($ll['emprenteur']==$id) $reste-=($ll['somme_cash']+$ll['somme_noncash']);
					if($ll['preteur']==$id) $reste+=$ll['somme_cash']+$ll['somme_noncash'];
				}
				$res[$k].= '<span style="color:#888">'.$reste.'</span>';
			}
			if($k=="loans") {
				$netloans=array();
				foreach ($loanlines as $ll) {
					if($ll['emprenteur']==$id) {
						if(!isset($netloans[$ll['preteur']]))$netloans[$ll['preteur']]=-($ll['somme_cash']+$ll['somme_noncash']);
						else $netloans[$ll['preteur']]-=($ll['somme_cash']+$ll['somme_noncash']);
					}
					if($ll['preteur']==$id) {
						if(!isset($netloans[$ll['emprenteur']]))$netloans[$ll['emprenteur']]=($ll['somme_cash']+$ll['somme_noncash']);
						else $netloans[$ll['emprenteur']]+=($ll['somme_cash']+$ll['somme_noncash']);
					}
				}
				$first=true;
				foreach($netloans as $file=>$loan)
				{
					if(!$first) $res[$k].= ", ";
					$color=($loan>0)?"#556B2F":"#CD5C5C";
					if($loan==0) $color="black";
					$res[$k].= lang('payment_file').'&nbsp;'.dossier_name($file,1).':&nbsp;<span style="color:'.$color.'">'.$loan.lang('money_symbol').'</span>';
					$first=false;
				}

			}
			if($k=="assigned_exp") {
				$query="SELECT experiment_id FROM ".table("experiments")." WHERE payment_file=$id";
				$epxs=array();
				if(query_column_exist(table("experiments"),"payment_file")) $epxs=orsee_query($query,"return_first_elem");
				if(!empty($epxs)) foreach($epxs as $i=>$exp_id){
					if($i>0) $res[$k].=', ';
					$res[$k].='<a target=_blank href="experiment_show.php?experiment_id='.$exp_id.'">'.experiment__get_title($exp_id).' ('.experiment__get_public_name($exp_id).')</a>';
				}
			}
			if($k=="used_exp") {
				$query="SELECT DISTINCT experiment_id FROM ".table("participate_at")." WHERE payment_file=$id";
				$epxs=array();
				if(query_column_exist(table("participate_at"),"payment_file")) $epxs=orsee_query($query,"return_first_elem");
				if(!empty($epxs)) foreach($epxs as $i=>$exp_id){
					if($i>0) $res[$k].=', ';
					$res[$k].='<a target=_blank href="experiment_show.php?experiment_id='.$exp_id.'">'.experiment__get_title($exp_id).' ('.experiment__get_public_name($exp_id).')</a>';
				}
			}
			if(in_array($k,$linktoreport)) $res[$k].= '</small></a>';
			if($k=="left_money" && check_allow('experiment_edit_participants') && $id>operation::$n_mix_accounts) {
				 $res[$k].=' (<a href="dossiers_paiement_report.php?dossier='.$id.'"><small>'.lang('payment_report',false).'</small></a>, <a href="dossiers_paiement_report.php?dossier='.$id.'&transfer_mode=1#transfer_mode_anchor"><small>'.lang('move_subjects',false).'</small></a>)';
			}
		}
		return $res;
}

?>

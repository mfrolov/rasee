<?php
// part of orsee. see orsee.org
ob_start();

$menu__area="statistics";
$title="participant statistics";

include("header.php");

	$allow=check_allow('statistics_participants_show','statistics_main.php');

	if (isset($_REQUEST['subpool_id']) && $_REQUEST['subpool_id']) 
		$subpool_id=$_REQUEST['subpool_id']; else $subpool_id='';
	if(preg_match("/\W/",$subpool_id)) $subpool_id="";
	$subscription_current=empty($_REQUEST["subscriptions"])?"":$_REQUEST["subscriptions"];
	if(preg_match("/\W/",$subscription_current)) $subscription_current="";

	echo '<center>
                <BR><BR>
                        <h4>'.$lang['participant_statistics'].'</h4>';
	$showallbutton="";
	if(!empty($_REQUEST['date_from']) || !empty($_REQUEST['date_till'])) $showallbutton='<button id=showall style="visibility:visible">'.lang('show all').'</button>';
	echo '<p align=right>'.lang('restrict_participant_subscriptions_dates').':  <input type="text" id="datepicker1" value="'.(empty($_REQUEST['date_from'])?'':$_REQUEST['date_from']).'" class="datepicker"> - <input type="text" value="'.(empty($_REQUEST['date_till'])?'':$_REQUEST['date_till']).'" id="datepicker2" class="datepicker">&nbsp;  <button id=sdbut style="visibility:hidden">'.lang('show').'</button> '.$showallbutton.' </p>
		  <link rel="stylesheet" href="../jcryption/jquery-ui.min.css">
		  <script src="../jcryption/jquery-ui.min.js"></script>';
	if ($settings['stats_type']=='text') {
		echo '<TABLE><TR><TD><pre>';
		echo stats__textstats_all();
		echo '</pre></TD></TR></TABLE>';
		}
	elseif ($settings['stats_type']=='plots') {
		echo stats__participant_graphstats_all();
		}
	elseif ($settings['stats_type']=='both') {
                echo stats__participant_htmlgraphstats_all();
                }
	else {
		echo stats__participant_htmlstats_all();
		}

	echo '<BR><BR><A href="statistics_main.php">'.icon('back').' '.$lang['back'].'</A><BR><BR>';
	echo '</center>';
	$dpreg='';
	if(lang('lang')=='fr') {
		echo '
		  <script src="../jcryption/datepicker-fr.js"></script>
		  ';
		$dpreg='fr';
	}
	echo '
	  <script>
	  $( function() {
		$.datepicker.setDefaults( $.datepicker.regional[ "'.$dpreg.'" ] );
		$( ".datepicker" ).datepicker({
		  dateFormat: "dd/mm/yy",
		  changeYear: true,
		  onSelect: function() {document.getElementById("sdbut").style.visibility="";}
		});
		$("#sdbut").click(function() {
			if($("#datepicker1").val().replace(/\s+/g,"")!="") {
					$("<input>").attr({
					type: "hidden",
					name: "date_from",
					value: $("#datepicker1").val().replace(/\s+/g,"")
				}).appendTo("#reportshowform");
			}
			if($("#datepicker2").val().replace(/\s+/g,"")!="") {
				$("<input>").attr({
					type: "hidden",
					name: "date_till",
					value: $("#datepicker2").val().replace(/\s+/g,"")
				}).appendTo("#reportshowform");
			}
			$("#reportshowform").submit();
		});
		$("#showall").click(function() {
			$("#reportshowform").submit();
		});
	  } );
	  
	  </script>
	
	<form method="get" id="reportshowform" action="'. basename(__FILE__) .'">
';
if(!empty($subpool_id)) echo '	<input name=subpool_id value="'.$subpool_id.'" />';
if(!empty($subscription_current)) echo '	<input name=subscriptions value="'.$subscription_current.'" />';
echo '
	</form>
';
include("footer.php");

<?php
// part of orsee. see orsee.org
ob_start();

$title="resubscribe participant";
include ("header.php");

	if (isset($_REQUEST['participant_id']) && $_REQUEST['participant_id']) 
			$participant_id=$_REQUEST['participant_id'];
                else redirect ($GLOBALS['settings__admin_folder']."/");

        if (isset($_REQUEST['betternot']) && $_REQUEST['betternot'])
                redirect ($GLOBALS['settings__admin_folder'].'/participants_edit.php?participant_id='.$participant_id);

        if (isset($_REQUEST['reallyresub']) && $_REQUEST['reallyresub']) $reallyresub=true;
                        else $reallyresub=false;

	$allow=check_allow('participants_resubscribe','participants_edit.php?participant_id='.$participant_id);

        $participant=orsee_db_load_array("participants",$participant_id,"participant_id");


	echo '<BR><BR>
		<center>
			<h4>'.$lang['resubscribe_participant'].'</h4>
		</center>';



	if ($reallyresub) {
				query_makecolumns(table('participants'),array("last_delete_time","last_delete_admin","can_undelete"),array("INT(20)","VARCHAR(20)","tinyint(1)"));
                $query="UPDATE ".table('participants')."
                        SET deleted='n', excluded='n', can_undelete=0
                        WHERE participant_id='".$participant_id."'";
                $result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

                if ($result) {
			log__admin("participant_resubscribe","participant_id:".$participant_id);
                        message ($lang['participant_resubscribed']);
                        redirect ($GLOBALS['settings__admin_folder']."/participants_edit.php?participant_id=".$participant_id);
			}
                   else message ($lang['database_error']);
                }

	echo '<CENTER>
		<FORM action="'.thisdoc().'">
		<INPUT type=hidden name="participant_id" value="'.$participant_id.'">
		<TABLE width=90%>
			<TR>
				<TD colspan=2 align=center>
					'.$lang['really_resubscribe_participant'].'<BR><BR>';
					dump_array($participant);
			echo '	</TD>
			</TR>
			<TR>
				<TD align=center>
					<INPUT type=submit name="reallyresub" 
						value="'.$lang['yes_resubscribe'].'">
				</TD>
				<TD align=center>
					<INPUT type=submit name=betternot 
						value="'.$lang['no_sorry'].'">
				</TD>
			</TR>
		</TABLE>
		</FORM>
	      </center>';

include ("footer.php");

?>

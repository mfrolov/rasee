<?php
// part of orsee. see orsee.org
ob_start();

$title="delete participant";
include ("header.php");

	if (isset($_REQUEST['participant_id']) && $_REQUEST['participant_id']) 
			$participant_id=$_REQUEST['participant_id'];
                else redirect ($GLOBALS['settings__admin_folder']."/");

        if (isset($_REQUEST['betternot']) && $_REQUEST['betternot'])
                redirect ($GLOBALS['settings__admin_folder'].'/participants_edit.php?participant_id='.$participant_id);

        if (isset($_REQUEST['reallydelete']) && $_REQUEST['reallydelete']) $reallydelete=true;
                        else $reallydelete=false;

        if (isset($_REQUEST['reallyexclude']) && $_REQUEST['reallyexclude']) $reallyexclude=true;
                        else $reallyexclude=false;

	$allow=check_allow('participants_unsubscribe','participants_edit.php?participant_id='.$participant_id);
	$allow=check_allow('subjectpool_delete','participants_edit.php?participant_id='.$participant_id);

    $participant=orsee_db_load_array("participants",$participant_id,"participant_id");


	echo '<BR><BR>
		<center>
			<h4>'.$lang['delete_participant_data'].'</h4>
		</center>';


        if ($reallydelete) {

			$query="DELETE FROM ".table('participate_at')." WHERE participant_id=$participant_id";
			$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
			$message = mysqli_affected_rows($GLOBALS['mysqli']) ." rows deleted from ".table('participate_at')." table";
			if(gettype($result)!="boolean") mysqli_free_result($result);
			$query="DELETE FROM ".table('participants')." WHERE participant_id=$participant_id";
			$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
			$message .= "<hr>". mysqli_affected_rows($GLOBALS['mysqli']) ." row deleted from ".table('participants')." table (participant_id $participant_id, ".$participant['fname']." ".$participant['lname'].", ".$participant['email'].")";
			if(gettype($result)!="boolean") mysqli_free_result($result);
			if ($result) { 
				message ($message);
				log__admin("participant_delete_from_base","participant_id:".$participant_id);
				redirect ($GLOBALS['settings__admin_folder']."/participants_main.php");
			}
		    else message ($lang['database_error']);
		}

        if ($reallyexclude) {
			
			$where=" WHERE tp.deleted='y' AND can_undelete=0 AND participant_id=$participant_id";
			$query="UPDATE ".table('participants')." as tp SET `email`='',`phone_number`='',`lname`='supprimé',`fname`='supprimé', remarks=REGEXP_REPLACE(remarks,'[a-zA-Z\-\.]+@([a-zA-Z-]+\.)+[\a-zA-Z\-]{2,}|((00)?33|0)\s*[1-9]([\s\.\-]*[0-9]{2}){4}',' X '), remarks=REGEXP_REPLACE(remarks,'(doublon de )([^(=]+)','doublon de X '), remarks=REGEXP_REPLACE(remarks,'(from participant )([^(=]+)','from participant X '), remarks=REGEXP_REPLACE(remarks,'(?-i)((-| )[A-Z]([a-zà-ÿ]|[A-Z])+){2,}',' X')".$where."";
			$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
			$message = mysqli_affected_rows($GLOBALS['mysqli']) ." row anonymised in the ".table('participants')." table (participant_id $participant_id, ".$participant['fname']." ".$participant['lname'].", ".$participant['email'].")";
			if(gettype($result)!="boolean") mysqli_free_result($result);
			if ($result) { 
				message ($message);
				log__admin("participant_anonymise","participant_id:".$participant_id);
				redirect ($GLOBALS['settings__admin_folder'].'/participants_edit.php?participant_id='.$participant_id);
			}
		    else message ($lang['database_error']);
		}


	echo '<CENTER>
		<FORM action="'.thisdoc().'">
		<INPUT type=hidden name="participant_id" value="'.$participant_id.'">
		<TABLE width=90%>
			<TR>
				<TD colspan=2 align=center>
					'.lang('delete_participant_from_database').'<BR><BR>';
					dump_array($participant);
			echo '	</TD>
			</TR>
			<TR>
				<TD align=center>
					<INPUT type=submit class="btn btn-danger" name="reallydelete" 
						value="'.lang('yes_delete').'">
				</TD>
			</TR>
			<TR>
				<TD align=center>
					<INPUT type=submit name=reallyexclude 
						value="'.lang('anonymise_only_without_deleting_participations').'">
				</TD>
			</TR>
			<TR>
				<TD align=center>
					<INPUT type=submit name=betternot 
						value="'.$lang['no_sorry'].'">
				</TD>
			</TR>
		</TABLE>
		</FORM>
	      </center>';

include ("footer.php");

?>




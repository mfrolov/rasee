<?php
// part of orsee. see orsee.org
ob_start();
$title="mail preview";

include("header.php");

	if (isset($_REQUEST['experiment_id']) && $_REQUEST['experiment_id']) $experiment_id=$_REQUEST['experiment_id'];
			else { $experiment_id=""; redirect ($GLOBALS['settings__admin_folder']."/"); }

	$allow=check_allow('experiment_invitation_edit','experiment_show.php?experiment_id='.$experiment_id);
	$experiment=orsee_db_load_array("experiments",$experiment_id,"experiment_id");
	if (!check_allow('experiment_restriction_override'))
		check_experiment_allowed($experiment,$GLOBALS['settings__admin_folder']."/experiment_show.php?experiment_id=".$experiment_id);

	$query="SELECT * from ".table('lang')."
                WHERE content_type='experiment_invitation_mail'
                AND content_name='".$experiment_id."'";
        $experiment_mail=orsee_query($query);

	$inv_langs=lang__get_part_langs();


	echo '	<BR><BR>
		<center>
		<h4>'.$lang['mail_preview'].'</h4>';

	echo '<TABLE border=0 width=90%>';

	foreach ($inv_langs as $inv_lang) {
        	// split in subject and text
        	$subject=str_replace(strstr($experiment_mail[$inv_lang],"\n"),"",$experiment_mail[$inv_lang]);
        	$body=substr($experiment_mail[$inv_lang],strpos($experiment_mail[$inv_lang],"\n")+1,strlen($experiment_mail[$inv_lang]));

        	$experimentmail['email']=load_language_symbol('recipients_email_address',$inv_lang);
        	$experimentmail['lname']=load_language_symbol('lastname',$inv_lang);
        	$experimentmail['fname']=load_language_symbol('firstname',$inv_lang);
        	$experimentmail['begin_of_studies']=load_language_symbol('begin_of_studies',$inv_lang);
        	$experimentmail['participant_id']=load_language_symbol('id',$inv_lang);
		$experimentmail['field_of_studies']=load_language_symbol('studies',$inv_lang);
		$experimentmail['profession']=load_language_symbol('profession',$inv_lang);
		$experimentmail['phone_number']=load_language_symbol('phone_number',$inv_lang);
		$experimentmail['sender']=$experiment['sender_mail'];

        	if ($experiment['experiment_type']=="laboratory") {
                	$experimentmail['sessionlist']=experimentmail__get_session_list($experiment_id,$inv_lang);
                	$experimentmail['link']=experimentmail__build_lab_registration_link(0);
                	}

		if (count($inv_langs) > 1) {
                        echo '<TR><TD colspan=2 bgcolor="'.$color['list_shade1'].'">'.$inv_lang.':</TD></TR>';
        }
		$message=process_mail_template(stripslashes($body),$experimentmail);
		$headers="";
		$footer="";
		$experimentmail['include_footer']='y';
		if (isset($experimentmail['include_footer']) && $experimentmail['include_footer']=="y") 
			$footer=stripslashes(experimentmail__get_mail_footer(0));
		if(preg_match("/^\s*#html#/",$subject)) {
			$subject=preg_replace("/^\s*#html#/","",$subject);
			$headers ='MIME-Version: 1.0' . "\r\n" . "Content-type: text/html; charset=$settings__charset" . "\r\n". $headers;
			$contains_a=preg_match('#\<a.*?\>(.*?)\<\/a\>#si',$message);
			$stripped_message=strip_tags($message, '<strong><b><em><i><u><span><font><hr><a><table><thead><tbody><tfoot><tr><td><ol><ul><li>');
			if($stripped_message!=$message) $message=str_replace('-------','<br>-------<br>',$message);
			else $message=str_replace("\n"," <br>\n",str_replace("\r\n","\n",$message));
			if(!$contains_a) {
				$url_pattern='%(?i)\b(((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’])))%';
				$url_replacement='<a href="$2" >$2</a>';
				$message=preg_replace($url_pattern,$url_replacement,$message);
			}
		}
		else {
			$message=nl2br(htmlentities($message));
			// var_dump($message);
			$headers .= "Content-Type: text/plain; charset=\"$settings__charset\"\r\n";
			$footer=nl2br(htmlentities($footer));
		}
		// $message=html_entity_decode($message,ENT_COMPAT,$settings__charset); //,'UTF-8' or 'ISO-8859-1'
		// $subject=html_entity_decode($subject,ENT_COMPAT,$settings__charset); //,'UTF-8' or 'ISO-8859-1'
		//$subject='=?'.$settings__charset.'?B?'.base64_encode($subject).'?=';

		echo '
              		<TR>
				<TD>'.$lang['from'].':</TD>
				<TD>'.$experimentmail['sender'].'</TD>
			</TR>
                	<TR>
				<TD>'.$lang['to'].':</TD>
				<TD>'.$experimentmail['email'].'</TD>
			</TR>
                	<TR>
				<TD>'.lang('headers').':</TD>
				<TD>'.$headers.'</TD>
			</TR>
                	<TR bgcolor="'.$color['list_shade2'].'">
				<TD>'.$lang['subject'].':</TD>
				<TD>'.stripslashes($subject).'</TD>
			</TR>
                	<TR>
				<TD valign=top bgcolor="'.$color['list_list_background'].'" colspan=2>
					'.$message;
				echo $footer;
		echo '		</TD>
			</TR>';
		echo '<TR><TD colspan=2>&nbsp;</TD></TR>';

	}

	echo '
              </TABLE>';

	echo '<BR><BR>
		<A HREF="experiment_mail_participants.php?experiment_id='.$experiment_id.'">'.
				$lang['back_to_mail_page'].'</A><BR><bR>
		</CENTER>';

include("footer.php");
?>

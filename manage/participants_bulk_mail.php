<?php
// part of orsee. see orsee.org
ob_start();
$title="bulk mail participants";

include ("header.php");

	$allow=check_allow('participants_bulk_mail','participants_main.php');

	if (isset($_REQUEST['send']) && $_REQUEST['send']) $send=true; else $send=false;

	// load invitation languages
	$inv_langs=lang__get_part_langs();

	if(isset($_SESSION['plist_ids'])) {
		$plist_ids=$_SESSION['plist_ids'];
		$number=count($plist_ids);
	}
	else {
		$number=0;
		if(!empty($_REQUEST['experiment_id']) && !empty($_REQUEST['session_id']) ) {
			$experiment_id=$_REQUEST['experiment_id'];
			$session_id=$_REQUEST['session_id'];
			$select_query=" SELECT *";
			if(!empty($_REQUEST['pay'])) $select_query.=", ".table('participate_at').".payment_file as dossier";
			$select_query.=" FROM ".table('participants').", ".table('participate_at').", 
							".table('sessions')."
					WHERE ".table('participants').".participant_id=".
							table('participate_at').".participant_id 
					AND ".table('sessions').".session_id=".table('participate_at').".session_id ";
			if(!empty($experiment_id)) $select_query.=" AND ".table('participate_at').".experiment_id='".$experiment_id."' ";
			if(empty($_REQUEST['payment_mode'])) $select_query.=" AND payghost=0 ";
			if ($session_id) $select_query.=" AND ".table('participate_at').".session_id='".$session_id."' "; 
			if(!empty($_REQUEST['restricted_plist']))  $select_query.=" AND ".table('participate_at').".participant_id IN (".$_REQUEST['restricted_plist'].") ";
			if(!empty($_REQUEST['payment_mode']) && $_REQUEST['payment_mode']=='view' && !empty($_REQUEST['pay']) && !isset($_REQUEST['only_empty']))  
						$select_query.=" AND (".table('participate_at').".payment<>'' OR ".table('participate_at').".payment_num<>'' OR ".table('participate_at').".payment_file<>'' OR ".table('participate_at').".payment_nlf<>'')";
			if(!empty($_REQUEST['pay']) && !empty($_REQUEST['only_empty'])) {
						$comp_sign=($_REQUEST['only_empty']>0)?'=':'<>';
						$select_query.=" AND (".table('participate_at').".payment".$comp_sign."''";
						if($_REQUEST['only_empty']<0) $select_query.=" OR ".table('participate_at').".payment_nlf".$comp_sign."''";
						$select_query.=")";
			}
			if(!empty($_REQUEST['payment_mode']) && $_REQUEST['payment_mode']!='view' && !empty($_REQUEST['pay']) && !empty($_REQUEST['dossier']))
						$select_query.=" AND (".table('participate_at').".payment_file='".$_REQUEST['dossier']."' OR ".table('participate_at').".payment_file='' OR ".table('participate_at').".payment_file IS NULL)";
			// if($pay_report_noexp) $select_query_init=$select_query;
			$select_query.="AND registered='y'";
			$result=mysqli_query($GLOBALS['mysqli'],$select_query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
			$plist_ids=array();
			while ($line=mysqli_fetch_assoc($result)) {
				$plist_ids[]=$line['participant_id'];
			}
			$number=count($plist_ids);
		}
	}
	
	if ($send) {

		if ((!is_array($plist_ids)) || count($plist_ids)<1) redirect ($GLOBALS['settings__admin_folder']."/");

		// checks
		$bulk=$_REQUEST;

		$continue=true;

		foreach ($inv_langs as $inv_lang) {
                        if (!$bulk[$inv_lang.'_subject']) {
				message ($lang['subject'].': '.$lang['missing_language'].": ".$inv_lang);
				$continue=false;
				}
			if (!$bulk[$inv_lang.'_body']) {
                                message ($lang['body_of_message'].': '.$lang['missing_language'].": ".$inv_lang);
                                $continue=false;
                                }
                        }

		if ($continue) {

			$bulk_id=time();
			foreach ($inv_langs as $inv_lang) {
				$query="INSERT INTO ".table('bulk_mail_texts')." 
					SET bulk_id='".$bulk_id."',
					lang='".$inv_lang."',
					bulk_subject='".mysqli_real_escape_string($GLOBALS['mysqli'],$bulk[$inv_lang.'_subject'])."',
					bulk_text='".mysqli_real_escape_string($GLOBALS['mysqli'],$bulk[$inv_lang.'_body'])."'";
                		$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
				}

					$starttime=0;
					if(!empty($_REQUEST['start_at'])) {
						$tempstarttime=strtotime($_REQUEST['start_at']);
						if($tempstarttime!==false) {
							$starttime=$tempstarttime;
							message (lang("the_earliest_sending_date").": ".date("Y-m-d H:i:s",$starttime));
						}
						else message (lang("error_in_start_date"));
					}
					$done=experimentmail__send_bulk_mail_to_queue($bulk_id,$plist_ids,$starttime);

                	message ($number.' '.$lang['xxx_bulk_mails_sent_to_mail_queue']);
			log__admin("bulk_mail","recipients:".$number);
			redirect ($GLOBALS['settings__admin_folder'].'/');
			}
	}


        echo '<BR><BR>
                <center>
                        <h4>'.$lang['send_bulk_mail'].'</h4>
			<h4>'.$number.' '.$lang['recipients'].'</h4>
                ';
        show_message();

	// form

        echo '<FORM action="'.thisdoc().'" method="post">';
		if(!empty($_GET["experiment_id"])) echo "
		<input type=hidden name=experiment_id value='".$_GET['experiment_id']."' />
		";
		if(!empty($_GET["session_id"])) echo "
		<input type=hidden name=session_id value='".$_GET['session_id']."' />
		";		
		echo '
        	<TABLE border=0 width=90%>';

	foreach ($inv_langs as $inv_lang) {

		if (count($inv_langs) > 1) {
			echo '<TR><TD colspan=2 bgcolor="'.$color['list_shade1'].'">'.$inv_lang.':</TD></TR>';
			}
		if (!isset($_REQUEST[$inv_lang.'_subject'])) $_REQUEST[$inv_lang.'_subject']="";
		if (!isset($_REQUEST[$inv_lang.'_body'])) $_REQUEST[$inv_lang.'_body']="";
		echo '
			<TR>
				<TD>
					'.$lang['subject'].':
				</TD>
				<TD>
					<INPUT type=text name="'.$inv_lang.'_subject" size=50 maxlength=180 value="'.
						$_REQUEST[$inv_lang.'_subject'].'">
				</TD>
			</TR>
                	<TR>
				<TD valign=top>
					'.$lang['body_of_message'].':
				</TD>
				<TD>
					<textarea name="'.$inv_lang.'_body" wrap=virtual rows=20 cols=50>'.
						$_REQUEST[$inv_lang.'_body'].'</textarea>
				</TD>
			</TR>
			<TR>
				<TD colspan=2 valign=top style="text-align:left">
					<br><br>Enter the start date (or leave empty for sending as soon as possible) : <br>- Date format : YYYY-MM-DD HH:MM:SS, or without seconds, and/or without date (for today) - <br>
					<INPUT type=text name="start_at" value="">
				</TD>
			</TR>';

		echo ' <TR><TD colspan=2>&nbsp;</TD></TR>';

		}

	echo '
                	<TR>
				<TD colspan=2 align=center>
                			<INPUT type=submit name="send" value="'.$lang['send'].'">
                		</TD>
			</TR>';

	echo '
        	</TABLE>
        	</FORM>';

	echo '<BR><BR>

		</CENTER>';

include ("footer.php");
?>

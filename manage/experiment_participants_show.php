<?php
// part of orsee. see orsee.org
ob_start();

$menu__area="experiments";
$title="show participants";

//"Rules signed?" disabled by Maxim Frolov;
$showrules=false; if(!empty($_REQUEST["showrules"])) $showrules=true;

include("header.php");

	if (isset($_REQUEST['experiment_id']) && $_REQUEST['experiment_id']) $experiment_id=$_REQUEST['experiment_id'];
                elseif((!empty($_REQUEST['pay']) && !empty($_REQUEST['payment_mode']) && $_REQUEST['payment_mode']=='view') || !empty($_REQUEST['disabled'])) $experiment_id='';
				elseif(!empty($_REQUEST['focus']) && $_REQUEST['focus']!="assigned") {$experiment_id=''; if(empty($_REQUEST["disabled"])) $_REQUEST["disabled"]=1;}
				else redirect($GLOBALS['settings__admin_folder']."/experiment_main.php"); //{var_dump($_REQUEST); var_dump($_POST); exit;}//
				

        if (isset($_REQUEST['session_id']) && $_REQUEST['session_id']) $session_id=$_REQUEST['session_id'];
                else $session_id='';

	if (isset($_REQUEST['remember']) && $_REQUEST['remember']) { 
		$_REQUEST=array_merge($_REQUEST,$_SESSION['save_posted']);
		$_SESSION['save_posted']=array();
		unset($_REQUEST['change']);
		}

	$allow=check_allow('experiment_show_participants','experiment_show.php?experiment_id='.$experiment_id);
	
	query_makecolumns(table('participate_at'),"payghost","TINYINT","0");

	$experiment=orsee_db_load_array("experiments",$experiment_id,"experiment_id");
	if (!check_allow('experiment_restriction_override'))
		check_experiment_allowed($experiment,$GLOBALS['settings__admin_folder']."/experiment_show.php?experiment_id=".$experiment_id);
		
	$payment_active=(!empty($_REQUEST['pay']) && !empty($_REQUEST['dossier']) && !empty($_REQUEST['payment_mode']) && !$_REQUEST['payment_mode']!='view');
	$pay_report_noexp=(!empty($_REQUEST['pay']) && !empty($_REQUEST['payment_mode']) && $_REQUEST['payment_mode']=='view'); // && $experiment_id==''

	if (isset($_REQUEST['change']) && $_REQUEST['change']) {

		//message(isset($_REQUEST['focus'])?$_REQUEST['focus']:"no_focus");
		// $x=isset($_REQUEST['focus'])?"_".$_REQUEST['focus']."_":"no_focus";
		// echo ($x); exit;
		if(empty($_REQUEST['focus'])) $_REQUEST['focus']='assigned';

		$allow=check_allow('experiment_edit_participants','experiment_participants_show.php?experiment_id='.
				$_REQUEST['experiment_id'].'&session_id='.$_REQUEST['session_id'].'&focus='.$_REQUEST['focus']);

		if (isset($_REQUEST['focus']) && $_REQUEST['focus']) $focus=$_REQUEST['focus'];
			else redirect($GLOBALS['settings__admin_folder'].'/'.thisdoc().'?experiment_id='.$_REQUEST['experiment_id'].
						'&session_id='.$_REQUEST['session_id']);

		$arr_focus=explode(":",$focus);
		$focus2part="";
		if(count($arr_focus)>1) { // && ($arr_focus[0]=="atdate" || $arr_focus[0]=="atmonth")
			$focus=$arr_focus[0];
			$focus2part=$arr_focus[1];
			if($focus=="athour") $focus="athours";
			if($focus=="atdates") $focus="atdate";
			if($focus=="absentatdates") $focus="absentatdate";
		}
		$focuses=array('assigned','invited','registered','shownup','participated');
		foreach($focuses as $f) { $$f=false; }
		$nonparticipated=false; $notpresent=false; $absent=false; $todayabsent=false; $absentold=false; $absentnew=false; $new=false; $today=false; $todaynew=false; $todayopen=false; $todayopenshownup=false; $todayshownup=false; $todayshownuponly=false; $todaystarted=false; $tomorrow=false; $atdate=false; $athours=false; $absentatdate=false; $shownupatdate=false; $participatedatdate=false; $atmonth=false; 
		$$focus=true;
		

		if (isset($_REQUEST['result_count']) && $_REQUEST['result_count']) $pcount=$_REQUEST['result_count']; else $pcount=0;

		if ($assigned || $invited) {
			$continue=true;

			if ($_REQUEST['to_session']) $to_session=$_REQUEST['to_session']; else $to_session=0;

			if ($to_session==0) {
				$continue=false;
				$_SESSION['save_posted']=$_REQUEST;
				message($lang['no_session_selected'],'message_error');
				redirect($GLOBALS['settings__admin_folder'].'/'.thisdoc().'?experiment_id='.$_REQUEST['experiment_id'].
                                                '&focus='.$_REQUEST['focus'].'&remember=true');
				}

			$tsession=orsee_db_load_array("sessions",$to_session,"session_id");

			$p_to_add=array();
			$i=0;
                        while ($i < $pcount) {
                                $i++;
				if (isset($_REQUEST['reg'.$i]) && $_REQUEST['reg'.$i]=="y") $p_to_add[]=$_REQUEST['pid'.$i];
				}

			$num_to_add=count($p_to_add);

			if (isset($_REQUEST['check_if_full']) && $_REQUEST['check_if_full']) {
				$alr_reg=experiment__count_participate_at($experiment_id,$to_session);
				$free_places=$tsession['part_needed']+$tsession['part_reserve']-$alr_reg;
				if ($free_places < 0) $free_places=0;
				
				$gender_stop=false;
				if(!empty($experiment['gender_equality']) && $experiment['gender_equality']!="n") {
					$alr_reg_f=experiment__count_participate_at($experiment_id,$to_session,"f");
					$alr_reg_m=experiment__count_participate_at($experiment_id,$to_session,"m");
					$free_places_gender=($free_places+$alr_reg)/2;
					$free_places_m=$free_places_gender-$alr_reg_m;
					$free_places_f=$free_places_gender-$alr_reg_f;
					$n_m=0; $n_f=0; $n_empty=0;
					foreach($p_to_add as $p_id){
						$current_pg=orsee_query("SELECT gender FROM ".table("participants")." WHERE participant_id='$p_id'");
						if($current_pg=="m") $n_m++;
						elseif($current_pg=="f") $n_f++;
						else $n_empty++;
					}
					if($n_empty>0 || $n_m>$free_places_m || $n_f>$free_places_f) $gender_stop=true;
				}
	
				if ($num_to_add > $free_places || $gender_stop) {
					$continue=false;
					$gender_info="";
					if($gender_stop) $gender_info=" ($free_places_m ".lang('gender_m')." ".lang('and')." $free_places_f ".lang('gender_f').")";
					message($lang['too_much_participants_to_register'].' '.
						$lang['free_places_in_session_xxx'].' '.
						session__build_name($tsession).': 
						<FONT color="green">'.$free_places.$gender_info.'</FONT><BR>'.
						$lang['please_change_your_selection'],'message_error');
						$_SESSION['save_posted']=$_REQUEST;
					redirect($GLOBALS['settings__admin_folder'].'/'.thisdoc().
						'?experiment_id='.$_REQUEST['experiment_id'].
                                                '&focus='.$_REQUEST['focus'].'&remember=true'.
						'&to_session='.$to_session);
					}
				}


			if ($continue) {
				if(experiment__check_option($experiment_id,'by_couples') && count($p_to_add)==2) {
					include_once("../".$GLOBALS['settings__public_folder']."/bycouples.php");
					$bchelper = new ByCouplesHelper();
					foreach($p_to_add as $cpk=>$cpid) {
						$other_k=count($p_to_add)-1-$cpk;
						$other_cpid=$p_to_add[$other_k];
						$partner=rasee_db_load_array('participants',array('participant_id'=>$other_cpid));
						$query="UPDATE ".table('participate_at')."
						SET session_id='".$to_session."',
							registered='y', ".$bchelper->mailtablefield."='".$partner['email']."' 
						WHERE experiment_id='".$experiment_id."'
						AND payghost=0
						AND participant_id = '".$cpid."'";
						$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']). ", query='$query'");
					}
				}
				else {
					$part_list=implode("','",$p_to_add);
							$query="UPDATE ".table('participate_at')."
						SET session_id='".$to_session."',
							registered='y'
						WHERE experiment_id='".$experiment_id."'
						AND payghost=0
						AND participant_id IN ('".$part_list."')";
					$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']). ", query='$query'");
				}

				message ($num_to_add.' '.$lang['xxx_subjects_registered_to_session_xxx'].' '.
					session__build_name($tsession).'.<BR>
					<A HREF="'.thisdoc().'?focus=registered&experiment_id='.$experiment_id.
					'&session_id='.$to_session.'">'.$lang['click_here_to_go_to_session_xxx'].
					' '.session__build_name($tsession).'</A>');
				redirect($GLOBALS['settings__admin_folder'].'/'.thisdoc().'?experiment_id='.$experiment_id.'&focus='.$_REQUEST['focus']);
				}

			}


		elseif ($registered || $shownup || $participated || $nonparticipated || $notpresent || $absent ||$todayabsent || $absentold || $absentnew || $new || $today || $todaynew || $todaystarted || $atdate || $athours || $absentatdate || $shownupatdate || $participatedatdate || $atmonth || $todayopen || $todayopenshownup || $todayshownup || $todayshownuponly || $tomorrow) {


			$p_shup=array(); $p_shup_not=array();
			$p_part=array(); $p_part_not=array();
			$p_rules=array(); $p_rules_not=array();
			$move=array();
			
			if($payment_active) {
				$op_amount_cash=0; $op_amount_noncash=0; $op_remarks=lang('automatic').' ('.lang('subjects_payment',false).')'; $op_nchages=0;
			}
			
			$dontChangeParticipationInfo=false;

		    $i=0;
            while ($i < $pcount) 
			{
				// $line=orsee_query("SELECT payghost FROM ".table('participate_at')." WHERE experiment_id='".$experiment_id."' AND participant_id='".$_REQUEST['pid'.$i]."'");
                $i++;
				if(!empty($_REQUEST['payghost'.$i])) continue;
				// if(empty($_REQUEST['pay'])) {
                        	if (isset($_REQUEST['shup'.$i]) ) {
								if ($_REQUEST['shup'.$i]=="y") $p_shup[]=$_REQUEST['pid'.$i];
								else $p_shup_not[]=$_REQUEST['pid'.$i];
							}
							elseif(!$dontChangeParticipationInfo) $p_shup_not[]=$_REQUEST['pid'.$i];

							if (isset($_REQUEST['part'.$i]) ) { 
								if( $_REQUEST['part'.$i]=="y") $p_part[]=$_REQUEST['pid'.$i];
													else $p_part_not[]=$_REQUEST['pid'.$i];
							}
							elseif(!$dontChangeParticipationInfo) $p_part_not[]=$_REQUEST['pid'.$i];

							if (isset($_REQUEST['rules'.$i]) ) { 
								if ($_REQUEST['rules'.$i]=="y") $p_rules[]=$_REQUEST['pid'.$i];
													else $p_rules_not[]=$_REQUEST['pid'.$i];
							}
							elseif($showrules && !$dontChangeParticipationInfo) $p_rules_not[]=$_REQUEST['pid'.$i];

				if (!isset($_REQUEST['session'.$i])) $_REQUEST['session'.$i]="";
				if (!isset($_REQUEST['csession'.$i])) $_REQUEST['csession'.$i]="";
				if ($_REQUEST['session'.$i]!=$session_id 
				    && $_REQUEST['session'.$i] != $_REQUEST['csession'.$i]) {
					$to_session=$_REQUEST['session'.$i];
					if (!isset($move[$to_session])) $move[$to_session]=array();
					$move[$to_session][]=$_REQUEST['pid'.$i];
					}
				// }
				if($payment_active) {
					$pid=$_REQUEST['pid'.$i]; $payment=$_REQUEST['payment_'.$i]; $paynum=$_REQUEST['payment_num_'.$i]; $dossier=$_REQUEST['dossier'];
					$nlfpay=""; $payment_nlf="";
					if(!empty($_REQUEST['payment_nlf_'.$i])) {$payment_nlf=$_REQUEST['payment_nlf_'.$i]; $nlfpay=", payment_nlf='".$payment_nlf."'";}
					$query="UPDATE ".table('participate_at')."
										SET payment = '$payment', payment_num='$paynum', payment_file='$dossier' $nlfpay
										WHERE experiment_id='".$experiment_id."'
						AND payghost=0
						AND participant_id = '$pid'";
					if($payment!=="" || $nlfpay!=="") {
						$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
					}
					// if($payment!=="") {var_dump($query,$done); exit;}
					$prevpayment=(empty($_REQUEST['payment_init_'.$i]))?0:$_REQUEST['payment_init_'.$i];
					$newpayment=(empty($payment))?0:$payment;
					$prevnlfpay=(empty($_REQUEST['payment_nlf_init_'.$i]))?0:$_REQUEST['payment_nlf_init_'.$i];
					$newnlfpay=(empty($payment_nlf))?0:$payment_nlf;
					$op_amount_cash+=$newpayment-$prevpayment;
					$op_amount_noncash+=$newnlfpay-$prevnlfpay;
					if(($prevpayment!=$newpayment || $prevnlfpay!=$newnlfpay) && ($prevpayment>0 || $prevnlfpay>0)) {
						if($op_nchages==0) $op_remarks.=". ".lang('changes').': ';
						$cpart=orsee_db_load_array("participants",$pid,"participant_id");
						$cpart_link='<a target=_blank href="participants_edit.php?participant_id='.$pid.'">'.$cpart['fname'].' '.$cpart['lname'].'</a>';
						if($prevpayment>0 && $prevpayment!=$newpayment) {
							if($op_nchages>0) $op_remarks.=", ";
							$op_remarks.= $cpart_link.' : '.$prevpayment.'<big>&rarr;</big>'.$newpayment;
							$op_nchages++;
						}
						if($prevnlfpay>0 && $prevnlfpay!=$newnlfpay) {
							if($op_nchages>0) $op_remarks.=", ";
							$nlfline=orsee_query("SELECT nonliquid_funds_name FROM ".table('payment_files')." WHERE id=$dossier");
							$op_remarks.= $cpart_link.'('.$nlfline['nonliquid_funds_name'].')'.': '.$prevnlfpay.'<big>&rarr;</big>'.$newnlfpay;
							$op_nchages++;
						}
						
					}
					
				}
			}
			if($payment_active) if($op_amount_cash!=0 || $op_amount_noncash!=0) {
				//var_dump($prevnlfpay,$newnlfpay,$op_amount_noncash); exit;
				$dossier=$_REQUEST['dossier'];
				$dossier_attr="";
				if($session_id!='') {
					$sessline=orsee_query("SELECT * FROM ".table("sessions")." WHERE session_id=$session_id");
					if(isset($sessline['payment_file'])) $dossier_attr=$sessline['payment_file'];
				}
				if(empty($dossier_attr) && $experiment_id!='') {
					$expline=orsee_query("SELECT * FROM ".table("experiments")." WHERE experiment_id=$experiment_id");
					if(isset($expline['payment_file'])) $dossier_attr=$expline['payment_file'];
				}
				$used_file=(empty($dossier_attr))?$dossier:$dossier_attr;
				$moneyrespline=orsee_query("SELECT money_responsible FROM ".table("payment_files")." WHERE id='$used_file'");
				$o= new operation($moneyrespline['money_responsible'],'participants',$dossier);
				if($op_amount_cash!=0) {
					$o->add("payment_cash",1,$op_amount_cash);
				}
				if($op_amount_noncash!=0) {
					$nlfline=orsee_query("SELECT nonliquid_funds_name FROM ".table('payment_files')." WHERE id=$dossier");
					$o->add("payment_".$nlfline['nonliquid_funds_name'],1,$op_amount_noncash);
				}
				$o->remarks=$op_remarks;
				if(!empty($dossier_attr) && $dossier!=$dossier_attr) $o->instead_of=$dossier_attr;
				$o->id=$_REQUEST['pay_operation_id'];
				$query="SELECT id FROM ".table("pay_operations")." WHERE id='".$o->id."' AND remarks='".$o->remarks."'";
				$make_operation=true;
				if(orsee_query($query)!==false) {
					message(lang('operation_already_exists').' (id='.$o->id.')');
					$make_operation=false;
					//redirect($GLOBALS['settings__admin_folder']."/".thisdoc());
					//exit;
				}
				$accline=orsee_query("SELECT SUM(amount) as file_amount FROM ".table("pay_accounts")." WHERE shortname<>'bank' AND shortname<>'participants' AND payment_file='$dossier'");
				if($accline===false || $accline['file_amount']==0) {
					$amount_lab_safe=helpers__regiedavance_amount_left();
					if($amount_lab_safe==0) {
						message(lang('accounting_record_not_made_as_no_money_available_for_this_file').'.<hr>');
						$make_operation=false;
					}
				}
				if($make_operation) $o->make();
			}

			// update shownup data
			$part_list=implode("','",$p_shup);
			$query="UPDATE ".table('participate_at')."
                                SET shownup = 'y'
                                WHERE experiment_id='".$experiment_id."'
								AND payghost=0
								AND participant_id IN ('".$part_list."')";
			$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
			
			$part_list=implode("','",$p_shup_not);
                        $query="UPDATE ".table('participate_at')."
                                SET shownup = 'n'
                                WHERE experiment_id='".$experiment_id."'
								AND payghost=0
								AND participant_id IN ('".$part_list."')";
                        $done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

			// update participation data
                        $part_list=implode("','",$p_part);
                        $query="UPDATE ".table('participate_at')."
                                SET participated = 'y'
                                WHERE experiment_id='".$experiment_id."'
								AND payghost=0
								AND participant_id IN ('".$part_list."')";
                        $done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

                        $part_list=implode("','",$p_part_not);
                        $query="UPDATE ".table('participate_at')."
                                SET participated = 'n'
                                WHERE experiment_id='".$experiment_id."'
								AND payghost=0
								AND participant_id IN ('".$part_list."')";
                        $done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

			// check for inconsitencies and clean
			$query="UPDATE ".table('participate_at')."
                                SET shownup = 'y'
                                WHERE participated='y'";
                        $done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

			// update rules signed data
                        $part_list=implode("','",$p_rules);
                        $query="UPDATE ".table('participants')."
                                SET rules_signed = 'y'
                                WHERE participant_id IN ('".$part_list."')";
                        $done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

                        $part_list=implode("','",$p_rules_not);
                        $query="UPDATE ".table('participants')."
                                SET rules_signed = 'n'
                                WHERE participant_id IN ('".$part_list."')";
                        $done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));



			// move participants to other sessions ...
			$moved_nr=array();
			foreach ($move as $msession => $mparts) {
				$part_list=implode("','",$mparts);
				$newshownup=(empty($_REQUEST['shup'.'sesschange']) || empty($msession))?'n':'y';
				$newparticipated=(empty($_REQUEST['part'.'sesschange']) || empty($msession))?'n':'y';
				$sessidpart="session_id = '".$msession."'"; $wheresup=""; $whatelsetoset=", shownup='$newshownup', participated='$newparticipated'";
				$wheresup="payment_num='' AND ";
				$query="UPDATE ".table('participate_at')."

					SET $sessidpart $whatelsetoset
					WHERE $wheresup participant_id IN ('".$part_list."')
					AND experiment_id='".$experiment_id."'";
				$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
				if($msession==0) {
					$sessidpart="";
					$wheresup="payment_num<>'' AND payghost=0 AND ";
					$apart_list=explode("','",$part_list);
					foreach($apart_list as $cp) {
						$q0="SELECT MAX(payghost) as maxpayghost FROM ".table('participate_at')." WHERE participant_id='".$cp."' AND experiment_id='".$experiment_id."'";
						$l0=orsee_query($q0);
						$newpayghost=$l0["maxpayghost"]+1;
						$whatelsetoset="payghost=$newpayghost";
						if($sessidpart!="") $whatelsetoset=", ".$whatelsetoset;
						$q1="UPDATE ".table('participate_at')."
							SET $sessidpart $whatelsetoset
							WHERE $wheresup participant_id='$cp'
							AND experiment_id='".$experiment_id."'";
						// echo($q1); message($q1);
						$done1=mysqli_query($GLOBALS['mysqli'],$q1) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));						
						// mysqli_free_result($done1);
					}
				}
				else {
					$whatelsetoset=""; $wheresup="payment_num<>'' AND payghost=0 AND ";
					$query="UPDATE ".table('participate_at')."
						SET $sessidpart $whatelsetoset
						WHERE $wheresup participant_id IN ('".$part_list."')
						AND experiment_id='".$experiment_id."'";
					$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']) ." query : $query");
				}
				if($msession==0 && mysqli_affected_rows($GLOBALS['mysqli'])>0) {
					// echo"356";
					$query="INSERT INTO ".table('participate_at')." (participant_id,experiment_id,invited) 
									SELECT participant_id, '".$experiment_id."', invited 
									FROM ".table('participate_at')." 
						WHERE payghost>0 AND experiment_id='".$experiment_id."' AND participant_id IN ('".$part_list."') ";
					$done=false; $ntry=0;
					while(!$done and $ntry<2)
					{
						$ntry++;
						$done=mysqli_query($GLOBALS['mysqli'],$query);
						if(!$done && $ntry==1) {
							$q2="ALTER TABLE ".table('participate_at')." 
								   DROP INDEX uindex2, 
								   ADD UNIQUE KEY `uindex2` (`participant_id`,`experiment_id`,`payghost`)";
							$done=mysqli_query($GLOBALS['mysqli'],$q2) or die("Error changing uindex2: " . mysqli_error($GLOBALS['mysqli']));
						}
						if(!$done && $ntry>1) die("Database error: " . mysqli_error($GLOBALS['mysqli']. "ntry=$ntry, $done=$done, query='$q2'" ));
					}
				}
				
			}

			// clean up 'no session's
                        $query="UPDATE ".table('participate_at')."
                                SET participated = 'n', shownup='n', registered='n'
                             	WHERE session_id='0'";
			$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));



			message($lang['changes_saved']);
			$m_message='<UL>';
			foreach ($move as $msession => $mparts) {
				$m_message.='<LI>'.count($mparts).' ';
				if ($msession==0) $m_message.=$lang['xxx_subjects_removed_from_registration'];
				   else {
					$tsession=orsee_db_load_array("sessions",$msession,"session_id");
					$m_message.=$lang['xxx_subjects_moved_to_session_xxx'].' 
						<A HREF="'.thisdoc().'?focus=registered&experiment_id='.
							$experiment_id.'&session_id='.$msession.'">'.
                					session__build_name($tsession).'</A>';
					$tpartnr=experiment__count_participate_at($experiment_id,$msession);
					if ($tsession['part_needed'] + $tsession['part_reserve'] < $tpartnr) 
						$mmessage.='<BLINK>'.
							$lang['subjects_number_exceeded'].'</BLINK>';
					}
				}
			$m_message.='</UL>';
			message($m_message);
			$target="experiment:".$experiment['experiment_name'];
			if ($session_id) $target.="\nsession_id:".$session_id;
			log__admin("experiment_edit_participant_list",$target);
			
			$redirectaddr=$GLOBALS['settings__admin_folder'].'/'.thisdoc().'?experiment_id='.$_REQUEST['experiment_id'].
                                                '&session_id='.$_REQUEST['session_id'].
						'&focus='.$_REQUEST['focus'];
			if(!empty($_REQUEST['pay'])) $redirectaddr.='&pay='.$_REQUEST['pay'];
			if(!empty($_REQUEST['payment_mode'])) $redirectaddr.='&payment_mode='.$_REQUEST['payment_mode'];
			if(!empty($_REQUEST['dossier'])) $redirectaddr.='&dossier='.$_REQUEST['dossier'];
			if(isset($_REQUEST['only_empty'])) $redirectaddr.='&only_empty='.$_REQUEST['only_empty'];

			redirect($redirectaddr);
		}

	}
	// list output


	$columns=participant__load_result_table_fields('session');
	$nlfpay_name="";
	if(!empty($_REQUEST['pay'])) {
		$payment_mode="entry"; $dossier="";
		if(!empty($_REQUEST['dossier'])) $dossier=$_REQUEST['dossier'];
		if(!empty($_REQUEST['payment_mode'])) $payment_mode=$_REQUEST['payment_mode'];
		$partemailpaymode=$_REQUEST['pay'];
		if($payment_mode=="view" && !empty($_REQUEST['show_emails'])) $partemailpaymode=2;
		$columns=participant__load_result_table_fields('session','',$partemailpaymode);
		// var_dump($columns);
		query_makecolumns(table('participate_at'),array("payment","payment_nlf","payment_num","payment_file"),array("varchar(20)","varchar(20)","varchar(20)","varchar(20)"), array("","","",""));
		$query="SELECT * FROM ".table('payment_files')." WHERE id=$dossier";
		if(!empty($dossier))$line=orsee_query($query);
		if(!empty($line['nonliquid_funds_name'])) $nlfpay_name=$line['nonliquid_funds_name'];

	}

	if ($session_id) $session=orsee_db_load_array("sessions",$session_id,"session_id");

	script__part_reg_show();

	$csorts=array(); 
	foreach($columns as $c) if (count($csorts)<2 && $c['allow_sort']) $csorts[]=$c['sort_order'];
	$csorts_string=implode(",",$csorts);

	if (isset($_REQUEST['sort']) && $_REQUEST['sort']) {
		$order=$_REQUEST['sort'];
		$ordercommaarr=explode(',',$order);
		$ordernamearr=explode('_',$order);
		if($ordernamearr[0]=="payment" && count($ordercommaarr)==1) {
			if(empty($ordernamearr[1]) || $ordernamearr[1]!="file") $order="CAST($order AS DECIMAL(10,2))";
			elseif($ordernamearr[1]=="file") $order="".table("participate_at").".$order, CAST(payment_num AS UNSIGNED)";
		}
		if(count($ordercommaarr)>1) $order=str_replace("payment_num","CAST(payment_num AS UNSIGNED)",$order);
		
	}
	else { 
		$order="session_start_year, session_start_month, session_start_day,
				session_start_hour, session_start_minute";
		if($pay_report_noexp) $order="session_start_year, session_start_month, session_start_day, dossier, CAST(payment_num AS UNSIGNED)";
		if ($csorts_string) $order.=",".$csorts_string;
    }

	if (isset($_REQUEST['focus']) && $_REQUEST['focus']) $focus=$_REQUEST['focus']; else $focus="assigned";
	$arr_focus=explode(":",$focus);
	$focus2part="";
	if(count($arr_focus)>1) {//&& ($arr_focus[0]=="atdate" || $arr_focus[0]=="atmonth")
		$focus=strtolower($arr_focus[0]);
		$focus2part=$arr_focus[1];
		if($focus=="athour") $focus="athours";
		if($focus=="atdates") $focus="atdate";
		if($focus=="absentatdates") $focus="absentatdate";
	}
	// message("focus=".$_REQUEST['focus'].", focus2part=".$focus2part);

	$assigned=false; $invited=false; $registered=false; $shownup=false; $participated=false; $nonparticipated=false; $notpresent=false; $absent=false; $todayabsent=false; $absentold=false; $absentnew=false; $new=false; $today=false; $todaynew=false; $todayopen=false; $todayopenshownup=false; $todayshownup=false; $todayshownuponly=false; $todaystarted=false; $tomorrow=false; $atdate=false; $athours=false; $absentatdate=false; $shownupatdate=false; $participatedatdate=false; $atmonth=false; 

	$$focus=true;
	
	$supsess_criteria="";

	if(!isset($lang['nonparticipated']))$lang['nonparticipated']=($lang['lang']=='fr')?"Enregistr&eacute;s mais pas particip&eacute;":"Registered but not participated";

	switch ($focus) {
		case "assigned":	$where_clause="AND registered='n'";
					$title=$lang['assigned_subjects_not_yet_registered'];
					break;
		case "invited":		$where_clause="AND invited='y' AND registered='n'";
                                        $title=$lang['invited_subjects_not_yet_registered'];
                                        break;
        	case "registered":	$where_clause="AND registered='y'";
                                        $title=$lang['registered_subjects'];
                                        break;
        	case "shownup":		$where_clause="AND shownup='y'";
                                        $title=$lang['shownup_subjects'];
                                        break;
        	case "participated":	$where_clause="AND participated='y'";
                                        $title=$lang['subjects_participated'];
                                        break;
        	case "nonparticipated":	$where_clause="AND participated='n'  AND registered='y'";
                                        $title=$lang['nonparticipated'];
                                        break;
				case "notpresent":						
                case "absent":    $where_clause="AND registered='y' AND shownup='n' AND participated='n'";
                                        $title=lang('subjects_absent');
                                        break;
                case "absentold":    $where_clause="AND registered='y' AND shownup='n' AND participated='n' AND session_finished='y'";
                                        $title=lang('absent_subjects_in_closed_sessions');
                                        break;
                case "absentnew":    $where_clause="AND registered='y' AND shownup='n' AND participated='n' AND session_finished='n'";
                                        $title=lang('absent_subjects_in_opened_sessions');
                                        break;
                case "new":    $where_clause="AND registered='y' AND shownup='n' AND participated='n' AND session_finished='n'";
                                        $title=lang('subjects_new');
                                        break;
                case "today":    
										$where_clause="AND registered='y' AND session_start_year='".date('Y')."' AND session_start_month='".date('n')."' AND session_start_day='".date('j')."'";
                                        $title=lang('subjects').' '.date('d/m/Y');
                                        break;
                case "todaystarted":    
										$where_clause="AND registered='y' AND session_start_year='".date('Y')."' AND session_start_month='".date('n')."' AND session_start_day='".date('j')."' AND CAST(session_start_hour AS UNSIGNED)<=".date('G')."";
                                        $title=lang('subjects').' '.date('d/m/Y').' '.lang('before',false).' '.date('G').lang('h',false);
                                        break;
                case "athours":    
										$cdate=empty($_REQUEST['date'])?date('H').".".date('j').".".date('n').".".date('Y'):$_REQUEST['date'];
										if(!empty($focus2part)) $cdate=$focus2part;
										$cdates=explode('-',$cdate);
										$where_clause="AND registered='y' ";
										$datetoshow="";
										for($i=0; $i<count($cdates); $i++)
										{
											$scdate=preg_replace("/\/|h\s*/",".",$cdates[$i],1);
											$acdate=explode('.',$scdate);
											// if(count($acdate)<2) $acdate=explode('/',$cdates[$i]);
											if(count($acdate)<2 || empty($acdate[1])) $acdate[1]=date('j');
											if(count($acdate)<3) $acdate[]=date('n');
											if(count($acdate)<4) $acdate[]=date('Y');
											$mysqlsessdate="STR_TO_DATE(CONCAT(session_start_year,'-',LPAD(session_start_month,2,'00'),'-',LPAD(session_start_day,2,'00'),'-',LPAD(session_start_hour,2,'00'),'-',LPAD(session_start_minute,2,'00')),'%Y-%m-%d-%H-%i')";
											$mysqldate1="STR_TO_DATE('".$acdate[3]."-".$acdate[2]."-".$acdate[1]."-".$acdate[0]."-00','%Y-%m-%d-%H-%i')";
											$mysqldate2="STR_TO_DATE('".$acdate[3]."-".$acdate[2]."-".$acdate[1]."-".$acdate[0]."-00','%Y-%m-%d-%H-%i')";
											if(count($cdates)==1) $where_clause.="AND session_start_year='".$acdate[3]."' AND session_start_month='".$acdate[2]."' AND session_start_day='".$acdate[1]."' AND session_start_hour='".$acdate[0]."'";
											elseif($i==0) {$supsess_criteria.="AND ".$mysqlsessdate.">=".$mysqldate1." "; $where_clause.=$supsess_criteria;}
											else {$supsess_criteria.="AND ".$mysqlsessdate."<=".$mysqldate2." "; $where_clause.=$supsess_criteria;}
											if($i>0) $datetoshow.=" - ";
											$cdtsh=implode("/",array_map("str_pad",$acdate,array_fill(0,count($acdate),2),array_fill(0,count($acdate),'0'),array_fill(0,count($acdate),STR_PAD_LEFT))); //str_pad($input, 10, "-=", STR_PAD_LEFT);
											$cdtsh=preg_replace("/\//","h ",$cdtsh,1);
											$datetoshow.=$cdtsh;
										}
										// "session_finished='n' AND NOW()<=STR_TO_DATE(CONCAT(session_start_year,'-',LPAD(session_start_month,2,'00'),'-',LPAD(session_start_day,2,'00'),'-',LPAD(session_start_hour,2,'00'),'-',LPAD(session_start_minute,2,'00')),'%Y-%m-%d-%H-%i')"
                                        $title=lang('subjects').' '.$datetoshow;
                                        break;
                case "atdate":    
										$cdate=empty($_REQUEST['date'])?date('j').".".date('n').".".date('Y'):$_REQUEST['date'];
										if(!empty($focus2part)) $cdate=$focus2part;
										$cdates=explode('-',$cdate);
										$where_clause="AND registered='y' ";
										$datetoshow="";
										for($i=0; $i<count($cdates); $i++)
										{
											$acdate=explode('.',$cdates[$i]);
											if(count($acdate)<2) $acdate=explode('/',$cdates[$i]);
											if(count($acdate)<2) $acdate[]=date('n');
											if(count($acdate)<3) $acdate[]=date('Y');
											$mysqlsessdate="STR_TO_DATE(CONCAT(session_start_year,'-',LPAD(session_start_month,2,'00'),'-',LPAD(session_start_day,2,'00'),'-',LPAD(session_start_hour,2,'00'),'-',LPAD(session_start_minute,2,'00')),'%Y-%m-%d-%H-%i')";
											$mysqldate1="STR_TO_DATE('".$acdate[2]."-".$acdate[1]."-".$acdate[0]."-00-01','%Y-%m-%d-%H-%i')";
											$mysqldate2="STR_TO_DATE('".$acdate[2]."-".$acdate[1]."-".$acdate[0]."-23-59','%Y-%m-%d-%H-%i')";
											if(count($cdates)==1) $where_clause.="AND session_start_year='".$acdate[2]."' AND session_start_month='".$acdate[1]."' AND session_start_day='".$acdate[0]."'";
											elseif($i==0) {$supsess_criteria.="AND ".$mysqlsessdate.">=".$mysqldate1." "; $where_clause.=$supsess_criteria;}
											else {$supsess_criteria.="AND ".$mysqlsessdate."<=".$mysqldate2." "; $where_clause.=$supsess_criteria;}
											if($i>0) $datetoshow.=" - ";
											$datetoshow.=implode("/",array_map("str_pad",$acdate,array_fill(0,count($acdate),2),array_fill(0,count($acdate),'0'),array_fill(0,count($acdate),STR_PAD_LEFT))); //str_pad($input, 10, "-=", STR_PAD_LEFT);
										}
										// "session_finished='n' AND NOW()<=STR_TO_DATE(CONCAT(session_start_year,'-',LPAD(session_start_month,2,'00'),'-',LPAD(session_start_day,2,'00'),'-',LPAD(session_start_hour,2,'00'),'-',LPAD(session_start_minute,2,'00')),'%Y-%m-%d-%H-%i')"
                                        $title=lang('subjects').' '.$datetoshow;
                                        break;
                case "absentatdate":    
										$cdate=empty($_REQUEST['date'])?date('j').".".date('n').".".date('Y'):$_REQUEST['date'];
										if(!empty($focus2part)) $cdate=$focus2part;
										$cdates=explode('-',$cdate);
										$where_clause="AND registered='y' AND shownup='n' AND participated='n' ";
										$datetoshow="";
										for($i=0; $i<count($cdates); $i++)
										{
											$acdate=explode('.',$cdates[$i]);
											if(count($acdate)<2) $acdate=explode('/',$cdates[$i]);
											if(count($acdate)<2) $acdate[]=date('n');
											if(count($acdate)<3) $acdate[]=date('Y');
											$mysqlsessdate="STR_TO_DATE(CONCAT(session_start_year,'-',LPAD(session_start_month,2,'00'),'-',LPAD(session_start_day,2,'00'),'-',LPAD(session_start_hour,2,'00'),'-',LPAD(session_start_minute,2,'00')),'%Y-%m-%d-%H-%i')";
											$mysqldate1="STR_TO_DATE('".$acdate[2]."-".$acdate[1]."-".$acdate[0]."-00-01','%Y-%m-%d-%H-%i')";
											$mysqldate2="STR_TO_DATE('".$acdate[2]."-".$acdate[1]."-".$acdate[0]."-23-59','%Y-%m-%d-%H-%i')";
											if(count($cdates)==1) $where_clause.="AND session_start_year='".$acdate[2]."' AND session_start_month='".$acdate[1]."' AND session_start_day='".$acdate[0]."'";
											elseif($i==0) {$supsess_criteria.="AND ".$mysqlsessdate.">=".$mysqldate1." "; $where_clause.=$supsess_criteria;}
											else {$supsess_criteria.="AND ".$mysqlsessdate."<=".$mysqldate2." "; $where_clause.=$supsess_criteria;}
											if($i>0) $datetoshow.=" - ";
											$datetoshow.=implode("/",array_map("str_pad",$acdate,array_fill(0,count($acdate),2),array_fill(0,count($acdate),'0'),array_fill(0,count($acdate),STR_PAD_LEFT))); //str_pad($input, 10, "-=", STR_PAD_LEFT);
										}
                                        $title=lang('subjects_absent').' '.$datetoshow;
                                        break;
                case "shownupatdate":    
										$cdate=empty($_REQUEST['date'])?date('j').".".date('n').".".date('Y'):$_REQUEST['date'];
										if(!empty($focus2part)) $cdate=$focus2part;
										$cdates=explode('-',$cdate);
										$where_clause="AND registered='y' AND shownup='y' ";
										$datetoshow="";
										for($i=0; $i<count($cdates); $i++)
										{
											$acdate=explode('.',$cdates[$i]);
											if(count($acdate)<2) $acdate=explode('/',$cdates[$i]);
											if(count($acdate)<2) $acdate[]=date('n');
											if(count($acdate)<3) $acdate[]=date('Y');
											$mysqlsessdate="STR_TO_DATE(CONCAT(session_start_year,'-',LPAD(session_start_month,2,'00'),'-',LPAD(session_start_day,2,'00'),'-',LPAD(session_start_hour,2,'00'),'-',LPAD(session_start_minute,2,'00')),'%Y-%m-%d-%H-%i')";
											$mysqldate1="STR_TO_DATE('".$acdate[2]."-".$acdate[1]."-".$acdate[0]."-00-01','%Y-%m-%d-%H-%i')";
											$mysqldate2="STR_TO_DATE('".$acdate[2]."-".$acdate[1]."-".$acdate[0]."-23-59','%Y-%m-%d-%H-%i')";
											if(count($cdates)==1) $where_clause.="AND session_start_year='".$acdate[2]."' AND session_start_month='".$acdate[1]."' AND session_start_day='".$acdate[0]."'";
											elseif($i==0) {$supsess_criteria.="AND ".$mysqlsessdate.">=".$mysqldate1." "; $where_clause.=$supsess_criteria;}
											else {$supsess_criteria.="AND ".$mysqlsessdate."<=".$mysqldate2." "; $where_clause.=$supsess_criteria;}
											if($i>0) $datetoshow.=" - ";
											$datetoshow.=implode("/",array_map("str_pad",$acdate,array_fill(0,count($acdate),2),array_fill(0,count($acdate),'0'),array_fill(0,count($acdate),STR_PAD_LEFT))); //str_pad($input, 10, "-=", STR_PAD_LEFT);
										}
                                        $title=lang('shownup').' '.$datetoshow;
                                        break;
                case "participatedatdate":    
										$cdate=empty($_REQUEST['date'])?date('j').".".date('n').".".date('Y'):$_REQUEST['date'];
										if(!empty($focus2part)) $cdate=$focus2part;
										$cdates=explode('-',$cdate);
										$where_clause="AND registered='y' AND participated='y' ";
										$datetoshow="";
										for($i=0; $i<count($cdates); $i++)
										{
											$acdate=explode('.',$cdates[$i]);
											if(count($acdate)<2) $acdate=explode('/',$cdates[$i]);
											if(count($acdate)<2) $acdate[]=date('n');
											if(count($acdate)<3) $acdate[]=date('Y');
											$mysqlsessdate="STR_TO_DATE(CONCAT(session_start_year,'-',LPAD(session_start_month,2,'00'),'-',LPAD(session_start_day,2,'00'),'-',LPAD(session_start_hour,2,'00'),'-',LPAD(session_start_minute,2,'00')),'%Y-%m-%d-%H-%i')";
											$mysqldate1="STR_TO_DATE('".$acdate[2]."-".$acdate[1]."-".$acdate[0]."-00-01','%Y-%m-%d-%H-%i')";
											$mysqldate2="STR_TO_DATE('".$acdate[2]."-".$acdate[1]."-".$acdate[0]."-23-59','%Y-%m-%d-%H-%i')";
											if(count($cdates)==1) $where_clause.="AND session_start_year='".$acdate[2]."' AND session_start_month='".$acdate[1]."' AND session_start_day='".$acdate[0]."'";
											elseif($i==0) {$supsess_criteria.="AND ".$mysqlsessdate.">=".$mysqldate1." "; $where_clause.=$supsess_criteria;}
											else {$supsess_criteria.="AND ".$mysqlsessdate."<=".$mysqldate2." "; $where_clause.=$supsess_criteria;}
											if($i>0) $datetoshow.=" - ";
											$datetoshow.=implode("/",array_map("str_pad",$acdate,array_fill(0,count($acdate),2),array_fill(0,count($acdate),'0'),array_fill(0,count($acdate),STR_PAD_LEFT))); //str_pad($input, 10, "-=", STR_PAD_LEFT);
										}
                                        $title=lang('shownup').' '.$datetoshow;
                                        break;
                case "atmonth":    
										$cdate=date('n').".".date('Y');
										if(!empty($_REQUEST['date'])) $cdate=$_REQUEST['date'];
										if(!empty($_REQUEST['month'])) $cdate=$_REQUEST['month'];
										if(!empty($focus2part)) $cdate=$focus2part;
										$acdate=explode('.',$cdate);
										if(count($acdate)<2) $acdate=explode('/',$cdate);
										if(count($acdate)<2) $acdate[]=date('Y');
										$where_clause="AND registered='y' AND session_start_year='".$acdate[1]."' AND session_start_month='".$acdate[0]."'";
                                        $title=lang('subjects').' '.implode("/",$acdate);
                                        break;
                case "tomorrow":    
										$where_clause="AND registered='y' AND session_start_year='".date('Y', strtotime('+1 day'))."' AND session_start_month='".date('n', strtotime('+1 day'))."' AND session_start_day='".date('j', strtotime('+1 day'))."'";
                                        $title=lang('subjects').' '.date('d/m/Y', strtotime('+1 day'));
                                        break;
				case "todaynew":    
										$where_clause="AND registered='y' AND shownup='n' AND participated='n'  AND session_finished='n' AND session_start_year='".date('Y')."' AND session_start_month='".date('n')."' AND session_start_day='".date('j')."'";
                                        $title=lang('subjects_new').' '.date('d/m/Y');
                                        break;
				case "todayopen":    
										$where_clause="AND registered='y' AND session_finished='n' AND session_start_year='".date('Y')."' AND session_start_month='".date('n')."' AND session_start_day='".date('j')."'";
                                        $title=lang('subjects_new').' '.date('d/m/Y');
                                        break;
				case "todayopenshownup":    
										$where_clause="AND registered='y' AND shownup='y' AND session_finished='n' AND session_start_year='".date('Y')."' AND session_start_month='".date('n')."' AND session_start_day='".date('j')."'";
                                        $title=lang('subjects_new').' '.date('d/m/Y');
                                        break;
				case "todayshownup":    
										$where_clause="AND registered='y' AND shownup='y' AND session_start_year='".date('Y')."' AND session_start_month='".date('n')."' AND session_start_day='".date('j')."'";
                                        $title=lang('subjects_new').' '.date('d/m/Y');
                                        break;
				case "todayshownuponly":    
										$where_clause="AND registered='y' AND shownup='y' AND participated='n' AND session_start_year='".date('Y')."' AND session_start_month='".date('n')."' AND session_start_day='".date('j')."'";
                                        $title=lang('subjects_new').' '.date('d/m/Y');
                                        break;
				case "todayabsent":    
										$where_clause="AND registered='y' AND shownup='n' AND participated='n' AND session_start_year='".date('Y')."' AND session_start_month='".date('n')."' AND session_start_day='".date('j')."'";
                                        $title=lang('subjects_absent').' '.date('d/m/Y');
                                        break;
		} 


	$select_query=" SELECT *";
	if(!empty($_REQUEST['pay'])) $select_query.=", ".table('participate_at').".payment_file as dossier";
	$select_query.=" FROM ".table('participants').", ".table('participate_at').", 
					".table('sessions')."
			WHERE ".table('participants').".participant_id=".
					table('participate_at').".participant_id 
			AND ".table('sessions').".session_id=".table('participate_at').".session_id ";
	if(!empty($experiment_id)) $select_query.=" AND ".table('participate_at').".experiment_id='".$experiment_id."' ";
	if(empty($_REQUEST['payment_mode'])) $select_query.=" AND payghost=0 ";
	if ($session_id) $select_query.=" AND ".table('participate_at').".session_id='".$session_id."' "; 
	if(!empty($_REQUEST['restricted_plist']))  $select_query.=" AND ".table('participate_at').".participant_id IN (".$_REQUEST['restricted_plist'].") ";
	if(!empty($_REQUEST['payment_mode']) && $_REQUEST['payment_mode']=='view' && !empty($_REQUEST['pay']) && !isset($_REQUEST['only_empty']))  
				$select_query.=" AND (".table('participate_at').".payment<>'' OR ".table('participate_at').".payment_num<>'' OR ".table('participate_at').".payment_file<>'' OR ".table('participate_at').".payment_nlf<>'')";
	if(!empty($_REQUEST['pay']) && !empty($_REQUEST['only_empty'])) {
				$comp_sign=($_REQUEST['only_empty']>0)?'=':'<>';
				$select_query.=" AND (".table('participate_at').".payment".$comp_sign."''";
				if($_REQUEST['only_empty']<0) $select_query.=" OR ".table('participate_at').".payment_nlf".$comp_sign."''";
				$select_query.=")";
	}
	if(!empty($_REQUEST['payment_mode']) && $_REQUEST['payment_mode']!='view' && !empty($_REQUEST['pay']) && !empty($_REQUEST['dossier']))
				$select_query.=" AND (".table('participate_at').".payment_file='".$_REQUEST['dossier']."' OR ".table('participate_at').".payment_file='' OR ".table('participate_at').".payment_file IS NULL)";
	if($pay_report_noexp) $select_query_init=$select_query;
    $select_query.=$where_clause." ORDER BY ".$order;
	


	echo '
		<center>
		<BR>
			<h4>'.$experiment['experiment_name'].'</h4>
			<h4 style="text-decoration:underline">'.$title.'</h4>

		<P align=right style="margin-right:5%">';
		if(!isset($_REQUEST['focus'])) $_REQUEST['focus']='';
		if(empty($_REQUEST['pay'])) echo '
			<A class="small" HREF="experiment_participants_show_pdf.php?experiment_id='.
				$experiment_id.'&session_id='.$session_id.'&focus='.$_REQUEST['focus'].
				'" target="_blank">'.$lang['print_version'].'</A>&nbsp;&nbsp;&nbsp;';
		if(empty($_REQUEST["disabled"]) && empty($_REQUEST['pay'])) echo '
				<br><br>
				<A class="small" HREF="'.$_SERVER['PHP_SELF'].'?experiment_id='.
				$experiment_id.'&session_id='.$session_id.'&focus='.$_REQUEST['focus'].
				'&disabled=1" ><small>disabled mode&nbsp;&nbsp;&nbsp;&nbsp;</small></A>';
		else echo '
				<br><br>
				<A class="small" HREF="'.$_SERVER['PHP_SELF'].'?experiment_id='.
				$experiment_id.'&session_id='.$session_id.'&focus='.$_REQUEST['focus'].
				'" ><small>normal mode&nbsp;&nbsp;&nbsp;&nbsp;</small></A>';
		$normal_link_start=$_SERVER['PHP_SELF'].'?experiment_id='.$experiment_id.'&session_id='.$session_id;
		$normal_link=$normal_link_start.'&focus='.$_REQUEST['focus'];
		$subjects_shortcuts=array();
		$subjects_shortcuts["shownup"]=$lang['shownup_subjects'];
		$subjects_shortcuts["participated"]=$lang['subjects_participated'];
		$subjects_shortcuts["absent"]=lang('absent_subjects');
		$subjects_shortcuts["registered"]=$lang['registered_subjects'];
		//$subjects_shortcuts["nonparticipated"]=$lang['nonparticipated'];
		unset($subjects_shortcuts[$focus]);
		if((empty($_REQUEST["disabled"]) || $experiment_id=='') && empty($_REQUEST['pay']) && !($assigned || $invited))  {
			echo '
				<br><br>'.lang('Other subpools').' : ';
			foreach ($subjects_shortcuts as $shk=>$sh) {
				$newfocus=$shk;
				if(count($arr_focus)>1) {
					if($shk=="registered") $newfocus="atdate:".$focus2part;
					else $newfocus=$shk."atdate:".$focus2part;
				}
				echo ' <A class="small" HREF="'.$normal_link_start.'&focus='.$newfocus.'" >'.$sh.'</A>&nbsp;&nbsp;&nbsp;&nbsp; ';
			}
		}
		$allow_payment_entry=check_allow("experiment_show_edit_payment","",true);
		$payment_links=array("entry/edit mode"=>$normal_link.'&pay=1&payment_mode=entry',
				"entry mode (only empty)"=>$normal_link.'&pay=1&payment_mode=entry&only_empty=1',
				"edit mode (only non empty)"=>$normal_link.'&pay=1&payment_mode=entry&only_empty=-1',
				"view mode"=>$normal_link.'&pay=1&payment_mode=view');
		if(!isset($lang['payment']))$lang['payment']=($lang['lang']=='fr')?"Paiement":"Payment";
		if(!($assigned || $invited) && $allow_payment_entry) {
			echo '
					<br><br>'.$lang['payment'].' : ';
			foreach ($payment_links as $lk=>$pl)
				if($experiment_id!='' || $lk=="view mode") echo ' <A class="small" HREF="'.$pl.'" >'.$lk.'</A>&nbsp;&nbsp;&nbsp;&nbsp; ';
		}
		if(!empty($_REQUEST['pay']) && $payment_mode=="view" && $allow_payment_entry) {
			echo '
					<br><br>View mode : ';
			$view_links=array();
			$veiwbase=$normal_link.'&pay=1&payment_mode=view';
			if(!isset($_REQUEST['only_empty']) || $_REQUEST['only_empty']<0) $view_links[]=array("show empty"=>$veiwbase.'&only_empty=0');
			if(isset($_REQUEST['only_empty']) && $_REQUEST['only_empty']>=0) $view_links[]=array("hide empty"=>$veiwbase);
			foreach($view_links[0] as &$ar) if(!empty($_REQUEST['show_emails'])) $ar.='&show_emails='.$_REQUEST['show_emails'];
			if(empty($_REQUEST['show_emails'])) $view_links[]=array("show emails"=>$veiwbase.'&show_emails=1');
			else $view_links[]=array("hide emails"=>$veiwbase.'');
			foreach($view_links[1] as &$ar) if(isset($_REQUEST['only_empty'])) $ar.='&only_empty='.$_REQUEST['only_empty'];
			foreach ($view_links as $view_links_inner) foreach ($view_links_inner as $lk=>$pl)
				echo ' <A class="small" HREF="'.$pl.'" >'.$lk.'</A>&nbsp;&nbsp;&nbsp;&nbsp; ';
		}
		echo '</P>';

	if(!empty($_REQUEST['pay']) && $allow_payment_entry) {
		$_REQUEST["disabled"]="1";
		if(empty($dossier) && $payment_mode=="entry") {
			echo '<CENTER>
				<FORM name="choisir_dossier" method=post action="'.thisdoc().'">
			
				<BR>';
			echo "<h2>".lang('please_choose_the_payment_file')."</h2>";
			echo '<INPUT name="payment_mode" type=hidden value="'.$payment_mode.'">';
			echo '
				<INPUT name="pay" type=hidden value="'.$_REQUEST['pay'].'">
				<INPUT type=hidden name="focus" value="'.$_REQUEST['focus'].'">
				<INPUT type=hidden name="experiment_id" value="'.$experiment_id.'">
				<INPUT type=hidden name="session_id" value="'.$session_id.'">';
			if(!empty($_REQUEST['only_empty'])) echo '
				<INPUT name="only_empty" type=hidden value="'.$_REQUEST['only_empty'].'">';
			if(!isset($lang['edit_payment_files'])) $lang['edit_payment_files']=($lang['lang']=='fr')?"Modifier les dossiers de paiement":"Edit payment files";
			echo '
				<table border=0 style="text-align:center"><tr>
				<td></td>
				</tr><tr>
				<td>';
			$select_payment_file=helpers__select_payment_file($experiment_id,$session_id);
			if($select_payment_file!==false) echo $select_payment_file;
			echo '
				</td>
				</tr></table>
				';
			if($select_payment_file!==false)  echo '<INPUT type=submit name="choose_payment_file" value="OK">';
			echo '<br><br><a target=_blank href="dossiers_paiement_main.php">'.$lang['edit_payment_files'].'</a>
				</FORM>
					';
		echo '<BR><BR>
			<A href="'.thisdoc().'?experiment_id='.$experiment_id.'&session_id='.$session_id.'&focus='.$_REQUEST['focus'].'">'.icon('back').' '.$lang['back'].'</A><BR><BR>
			';
			echo '  <A HREF="experiment_show.php?experiment_id='.$experiment_id.'">
							'.$lang['mainpage_of_this_experiment'].'</A><BR><BR>

					</CENTER>';
			include ("footer.php");
			exit;
		}
	}
	// show query
	echo '	<P class="small">Query: '.$select_query.'</P>';
	if(!empty($dossier) && $payment_mode=='entry') echo '<h3>Dossier : '.dossier_name($dossier,1).'</h3>';

	if($pay_report_noexp)
	{
		$gdquery=str_replace("SELECT *","SELECT SUM(CAST(payment AS DECIMAL(10,2))) as paysum, SUM(CAST(payment_nlf AS DECIMAL(10,2))) as nlfpaysum, ".table('participate_at').".payment_file as dossier",$select_query_init);
		$gdquery.=$where_clause." GROUP BY dossier ORDER BY session_start_year, session_start_month, session_start_day, dossier";
		$dossier_resarr=orsee_query($gdquery,"return_same");
		$nlf_tot=($dossier_resarr===false)?0:array_reduce($dossier_resarr,function($a,$b){return $a+$b["nlfpaysum"];},0);
		if($nlf_tot>0) $nlfpay_name=lang('nonliquid_value');
		if(isset($datetoshow)) {
			$cdates=explode(' - ',$datetoshow);
			$lastdate=end($cdates);
			$lastdate_dt=DateTime::createFromFormat("d/m/Y-H-i",$lastdate.'-00-01');
			$now=new DateTime('now');
			//echo date("d/m/Y-H-i",$now->getTimestamp());
			if($lastdate_dt>$now) {
				$datetoshow=str_replace($lastdate,$now->format("d/m/Y"),$datetoshow);
				$lastdate=$now->format("d/m/Y");
			}
			// var_dump(($lastdate_dt>$now),$lastdate);
			$mysqlsessdate="STR_TO_DATE(CONCAT(session_start_year,'-',LPAD(session_start_month,2,'00'),'-',LPAD(session_start_day,2,'00'),'-',LPAD(session_start_hour,2,'00'),'-',LPAD(session_start_minute,2,'00')),'%Y-%m-%d-%H-%i')";
			$mysqldatelast="STR_TO_DATE('".$lastdate."-23-59','%d/%m/%Y-%H-%i')";
			$dquery="SELECT d.id, d.budget, d.closed, d.year, d.number, d.experimenter, d.money_responsible, p.paysum, p.nlfpaysum FROM ".table('payment_files')." as d JOIN (SELECT pa.payment_file, SUM(CAST(payment AS DECIMAL(10,2))) as paysum, SUM(CAST(payment_nlf AS DECIMAL(10,2))) as nlfpaysum FROM ".table('participate_at')." as pa JOIN ".table('sessions')." as s ON s.session_id=pa.session_id  WHERE $mysqlsessdate<=$mysqldatelast GROUP BY pa.payment_file) as p ON p.payment_file=d.id";
		}
		else { $datetoshow=null; $lastdate=null;}
		// $dossier_ldarr0=orsee_query("SELECT pa.payment_file, session_start_year,session_start_month,session_start_day, SUM(CAST(payment AS DECIMAL(10,2))) as paysum, SUM(CAST(payment_nlf AS DECIMAL(10,2))) as nlfpaysum FROM ".table('participate_at')." as pa JOIN ".table('sessions')." as s ON s.session_id=pa.session_id WHERE $mysqlsessdate<=$mysqldatelast GROUP BY pa.payment_file","return_same");
		$dossier_ldarr=null; 
		if(isset($datetoshow)) $or_q_rkv=orsee_query($dquery,"return_key_vals"); 
		// var_dump($or_q_rkv);
		if(isset($datetoshow) && $or_q_rkv!==false) $dossier_ldarr=array_reduce(orsee_query($dquery,"return_key_vals"),function($a,$b){return $a+$b;}, array());
		// var_dump($dossier_ldarr);
		$d_info="<hr width=50%>".lang("used_payment_files")." ".$datetoshow." : ";
		$d_info.="<span id=budgetinfo_show style='display:none;'><a href='javascript:void(document.getElementById(\"budgetinfo_show\").style.display=\"none\"); var binfolist=document.getElementsByClassName(\"budgetinfo\"); for(var i = 0; i < binfolist.length; i++) {void(binfolist[i].style.display=\"\");}'><small> - ".lang('show budget_information')."</small></a></span> ";
		if($dossier_resarr!==false) $d_info.="<span id=toshow0 style='display:none;'><a href='javascript:void(document.getElementById(\"toshow0\").style.display=\"none\");for(var i=0; i<=".(count($dossier_resarr)+1)."; i++) {void(document.getElementById(\"tohide\"+i).style.display=\"\");}'><small> - ".lang('show links and comments')."</small></a></span> ";
		$d_info.="<br><table border=1 style='border-collapse:collapse'>";
		$d_info.="<tr>";
		$totalcashinfo="spent";
		if($nlf_tot>0) $totalcashinfo.=" :<br> liquid_value";
		$d_info.=sprintf("<th>%s</th><th>%s</th>",lang("payment_files"),lang($totalcashinfo));
		if($nlf_tot>0) $d_info.=sprintf("<th>%s</th>",lang("spent :<br> nonliquid_value"));
		$suphidebudget="<br> <a href='javascript:void(document.getElementById(\"budgetinfo_show\").style.display=\"inline\"); var binfolist=document.getElementsByClassName(\"budgetinfo\"); for(var i = 0; i < binfolist.length; i++) {void(binfolist[i].style.display=\"none\");}'><small>(".lang('hide').")</small></a>";
		$d_info.=sprintf("<th class=budgetinfo>%s</th><th class=budgetinfo>%s</th><th class=budgetinfo>%s</th>",lang("budget").$suphidebudget,lang("% spent <br> of_budget"),lang("left_at")."<br>$lastdate");
		if($dossier_resarr!==false) $d_info.=sprintf("<th id=tohide0 class=hiddable>%s</th>",lang("links and comments")."<br> <a href='javascript:void(document.getElementById(\"toshow0\").style.display=\"inline\"); for(var i=0; i<=".(count($dossier_resarr)+1)."; i++) {void(document.getElementById(\"tohide\"+i).style.display=\"none\");}'><small>(".lang('hide').")</small></a>");
		$d_info.="</tr>";
		$tot_cash=0; $tot_budget=0; $tot_left_lastdate=0;
		if($dossier_resarr!==false) foreach($dossier_resarr as $n=>$da)
		{
			$d_info.="<tr>";
			$d_info.=sprintf("<td>%s</td><td>%01.2f</td>",dossier_name($da['dossier'],1),$da['paysum']);
			if($nlf_tot>0) $d_info.=sprintf("<td>%01.2f</td>",$da['nlfpaysum']);
			$d_info.=sprintf("<td class=budgetinfo>%01.2f</td>",$dossier_ldarr[$da['dossier']]['budget']);
			$d_info.=sprintf("<td class=budgetinfo>%s</td>",($dossier_ldarr[$da['dossier']]['budget']==0)?"-":round(($da['paysum']+$da['nlfpaysum'])*100/$dossier_ldarr[$da['dossier']]['budget'])."<small>%</small>");
			$leftlastdate=$dossier_ldarr[$da['dossier']]['budget']-($dossier_ldarr[$da['dossier']]['paysum']+$dossier_ldarr[$da['dossier']]['nlfpaysum']);
			$d_info.=sprintf("<td class=budgetinfo>%01.2f</td>",$leftlastdate);
			$suplinks="<A class=\"small\" target=_blank href='dossiers_paiement_main.php?addit=1&id=".$da['dossier']."&show_statistics=1'><small>".lang("details",false)."</small></A>";
			$suplinks.=", <A class=\"small\" target=_blank href='dossiers_paiement_report.php?dossier=".$da['dossier']."&transfer_mode=1#transfer_mode_anchor'><small>".lang("move_subjects_to_another_payment_file",false)."</small></A>";
			$supinfo="";
			if($dossier_ldarr[$da['dossier']]['closed']==1) $supinfo=", <small>".str_replace(':','',lang("closed at ",false)).$now->format("d/m/Y")."</small>";
			$d_info.=sprintf("<TD class=\"small\" id=tohide".($n+1)." class=hiddable>%s</TD>",$suplinks.$supinfo);
			$d_info.="</tr>";
			$tot_cash+=$da['paysum'];
			$tot_budget+=$dossier_ldarr[$da['dossier']]['budget'];
			$tot_left_lastdate+=$leftlastdate;
		}
		$d_info.="<tr style='vertical-align:top;font-style:oblique;color:#55A'><td style='text-align:right'>".lang('total').":</td>";
		$d_info.=sprintf("<td>%01.2f</td>",$tot_cash);
		if($nlf_tot>0) $d_info.=sprintf("<td>%01.2f<hr align=left width=75%%>".lang('total').": %01.2f</td>",$nlf_tot,$tot_cash+$nlf_tot);
		$d_info.=sprintf("<td class=budgetinfo>%01.2f</td><td class=budgetinfo>%s</td><td class=budgetinfo>%01.2f</td>",$tot_budget,($tot_budget==0)?"-":round(($tot_cash+$nlf_tot)*100/$tot_budget)."<small>%</small>",$tot_left_lastdate);
		if($dossier_resarr!==false) $d_info.=sprintf("<td class=small id=tohide".(count($dossier_resarr)+1)." class=hiddable>%s</td>",'&nbsp;');
		$d_info.="</tr>";
		$d_info.="</table><hr width=50%>";
		// var_dump($dossier_resarr,$nlf_tot);
		echo $d_info;
	}
	// get result
	$result=mysqli_query($GLOBALS['mysqli'],$select_query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
	

        $participants=array(); $plist_ids=array(); $noncash_names=array(); $findnoncash=($nlfpay_name=="");
		if(!isset($lang['non_cash']))$lang['non_cash']=($lang['lang']=='fr')?"non cash":"non cash";
        while ($line=mysqli_fetch_assoc($result)) {
				if($findnoncash && !empty($line['payment_nlf']) && !empty($line['payment_file'])) {
					$nlfpay_name=$lang['non_cash'];
					$query="SELECT * FROM ".table('payment_files')." WHERE id=".$line['payment_file'];
					$nlfline=orsee_query($query);
					if(!empty($nlfline['nonliquid_funds_name']) && !in_array($nlfline['nonliquid_funds_name'],$noncash_names)) $noncash_names[]=$nlfline['nonliquid_funds_name'];
				}
                $participants[]=$line;
		$plist_ids[]=$line['participant_id'];
                }
		if($findnoncash && $nlfpay_name==$lang['non_cash']) $nlfpay_name=implode(', ',$noncash_names);
	$_SESSION['plist_ids']=$plist_ids;
	$result_count=count($participants);
	
	$critical_count=150;
	$atleast_registered=($registered || $shownup || $participated || $nonparticipated || $notpresent || $absent ||$todayabsent || $absentold || $absentnew || $new || $today || $todaynew || $todayopen || $todaystarted || $atdate || $athours || $absentatdate || $shownupatdate || $participatedatdate || $atmonth || $todayopen || $todayopenshownup || $todayshownup || $todayshownuponly || $tomorrow);
	$critical_count_apply=($atleast_registered && $result_count>$critical_count);

	$nr_normal_columns=2;
	if (check_allow('experiment_edit_participants') && empty($_REQUEST["disabled"])) $disabled=false; else $disabled=true;
	$disable_check=$disabled;
	echo '
	<script>
	var butch_but_event_attached=false;
	function activate_batch_session_apply(elem) {var bsa=document.getElementById("batch_session_apply"); if(elem.value!=-1) {bsa.disabled=false; if(!butch_but_event_attached) {bsa.addEventListener("click",function(event){
		var nall='.$result_count.';
		var batchtype=document.getElementById("batch_sess_type").value;
		var selectapsessvalue=document.getElementById("selectapsession").value;
		var n_applied=0; var n_checkrestr=0;
		for(var i = 1; i<=nall; i++) {
			var proceed=(batchtype==0);
			var checkrestr=(document.getElementById("selectapsession").value==-2);
			proceed= proceed || (batchtype==1 && document.getElementById("shup"+i).checked);
			proceed= proceed || (batchtype==2 && document.getElementById("part"+i).checked);
			proceed= proceed || (batchtype==3 && document.getElementById("shup"+i).checked && !document.getElementById("part"+i).checked);
			proceed= proceed || (batchtype==4 && !document.getElementById("part"+i).checked);
			proceed= proceed || (batchtype==5 && !document.getElementById("shup"+i).checked && !document.getElementById("part"+i).checked);
			var csp=document.getElementById("selectsession"+i);
			if(proceed) {
				if(selectapsessvalue>=0) {csp.value=selectapsessvalue; n_applied++}; 
				if(selectapsessvalue==-2) {document.getElementById("chkbx"+i).checked="CHECKED"; n_checkrestr++}
			}
		}
		if(n_applied>0) document.getElementById("apply_sess_batch_info").innerHTML="'.lang('press').' &laquo; '.lang('change').' &raquo;'.lang(' to_save_the_changes or reload the_page to_undo_the_modifications').'.";
		if(n_checkrestr>0) document.getElementById("apply_sess_batch_info").innerHTML="'.lang('press').' &laquo; '.lang('restrict_for_checked').' &raquo;'.lang(' to_save_the_changes or reload the_page to_undo_the_modifications').'.";
	});}; butch_but_event_attached=true;}
	else {bsa.disabled="disabled";}}
	</script>';
	
	if(!$disabled && $atleast_registered) echo '<table style="display:flex"><tr><td><select id=batch_sess_type><option value="0">'.lang('everyone').'</option><option value="1">'.lang('all shownup |s').'</option><option value="2">'.lang('all participated |s').'</option><option value="3">'.lang('all shownup not_participated |s').'</option><option value="4">'.lang('all not_participated |s').'</option><option value="5">'.lang('all absent |s').'</option></select> -> '.select__sessions('','apsession',$experiment_id,false,$supsess_criteria,'onchange="activate_batch_session_apply(this)"',true,$critical_count_apply).' <input disabled type=button name="batch_session_apply" id="batch_session_apply" value="'.lang('apply').'" /></td><tr><td id="apply_sess_batch_info" style="color:red"></td></tr></tr></table>';
	// form
	echo '
		<FORM name="part_list" method=post action="'.thisdoc().'">
	
		';
	if($payment_active) echo '<p align=right><input type=checkbox id="pay_also_check_participations" />'.lang("check_participations").'</p>';
	else echo '
		<BR>';
	echo '
		<table border=0>
			<TR>
				<TD class="small"></TD>';
				foreach($columns as $c) {
                   	if($c['allow_sort'] && !empty($_REQUEST['pay'])) 
					{
						$supvals="&pay=".$_REQUEST['pay']."&payment_mode=".$payment_mode;
						if($payment_mode=="entry") $supvals.="&dossier=".$_REQUEST['dossier'];
						if(!empty($_REQUEST['show_emails'])) $supvals.="&show_emails=".$_REQUEST['show_emails'];
						if(isset($_REQUEST['only_empty'])) $supvals.="&only_empty=".$_REQUEST['only_empty'];
						if ((empty($_REQUEST['sort']) || $_REQUEST['sort']=='payment_file' || $_REQUEST['sort']=='session_start_year,session_start_month,session_start_day,dossier,payment_num') && $experiment_id=='' && $c['mysql_column_name']=='lname') $c['sort_order']='session_start_year,session_start_month,session_start_day,dossier,'.$c['sort_order'];
						headcell($c['column_name'],$c['sort_order'],"",$supvals);
					}
					elseif($c['allow_sort']) headcell($c['column_name'],$c['sort_order']);
                   	else headcell($c['column_name']);
                   	$nr_normal_columns++;
                }
				if(empty($_REQUEST['pay'])) headcell($lang['noshowup'],"number_noshowup,number_reg");
				else {
					$sum_payment=0;
					if(!isset($lang['payment']))$lang['payment']=($lang['lang']=='fr')?"Paiement":"Payment";
					if(!isset($lang['payment_num']))$lang['payment_num']=($lang['lang']=='fr')?"N paiement":"N payment";
					if(!isset($lang['payment_file']))$lang['payment_file']=($lang['lang']=='fr')?"Dossier":"File";
					// "payment_num","payment_file"
					$supvals="&pay=".$_REQUEST['pay']."&payment_mode=".$payment_mode;
					if(!empty($_REQUEST['dossier'])) $supvals.="&dossier=".$_REQUEST['dossier'];
					if(!empty($_REQUEST['show_emails'])) $supvals.="&show_emails=".$_REQUEST['show_emails'];
					if(isset($_REQUEST['only_empty'])) $supvals.="&only_empty=".$_REQUEST['only_empty'];
					$payment_supname="";
					if($nlfpay_name!="") {
						$payment_supname.=" (".lang('cash',false).")";
						$sum_payment_nlf=0;
					}
					headcell($lang['payment'].$payment_supname,"payment","",$supvals);
					if($nlfpay_name!="") headcell($lang['payment'].' ('.$nlfpay_name.')',"payment_nlf","",$supvals);
					headcell($lang['payment_num'],"payment_num","",$supvals);
					if($payment_mode=="view") headcell($lang['payment_file'],"payment_file","",$supvals);
				}
			if ($assigned || $invited) {
				headcell($lang['invited'],"invited","invited");
				headcell($lang['register']);
				}
			if ($registered || $shownup || $participated || $nonparticipated || $notpresent || $absent ||$todayabsent || $absentold || $absentnew || $new || $today || $todaynew || $todayopen || $todaystarted || $atdate || $athours || $absentatdate || $shownupatdate || $participatedatdate || $atmonth || $todayopen || $todayopenshownup || $todayshownup || $todayshownuponly || $tomorrow) {
				if(!isset($supvals)) $supvals="";
				if(isset($payment_mode) && $payment_mode=="view") {
					$datesortorder=(!empty($_REQUEST['sort']) && $_REQUEST['sort']!='lname,fname' && $_REQUEST['sort']!='session_start_year,session_start_month,session_start_day,dossier,payment_num')?'session_start_year,session_start_month,session_start_day,dossier,payment_num':'session_start_year,session_start_month,session_start_day,dossier,lname,fname';
					headcell('Date',$datesortorder,'Date',$supvals);
				}
				$sess_sort_order=table("sessions").".session_id,lname,fname";
				if(empty($_REQUEST['sort']) || $_REQUEST['sort']=='payment_file' || substr($_REQUEST['sort'],0,65)=='session_start_year,session_start_month,session_start_day,dossier,')					$sess_sort_order="session_start_year,session_start_month,session_start_day,session_start_hour,session_start_minute,dossier,payment_num";
				headcell($lang['session'],$sess_sort_order,"session",$supvals);
				headcell($lang['shownup'],"shownup","shownup",$supvals);
				headcell($lang['participated'],"participated","participated",$supvals);
				if($showrules) headcell($lang['rules_signed']);
				if($critical_count_apply && !empty($experiment_id) && !$pay_report_noexp) headcell(lang('check_to_restrict'));
				}
	echo '		</TR>';



	$shade=false;

	$part_ids=array();
	$emails=array();

	$pnr=0;
	

        foreach ($participants as $p) {

		$emails[]=$p['email'];
		$pnr++;

		echo '<tr class="small"';
                        if ($shade) echo ' bgcolor="'.$color['list_shade1'].'"';
                               else echo ' bgcolor="'.$color['list_shade2'].'"';
                echo '>';

	        echo '	<td class="small">
				'.$pnr; 
				echo '<INPUT name="pid'.$pnr.'" id="pid'.$pnr.'" type=hidden value="'.$p['participant_id'].'">';
				echo '<INPUT name="payghost'.$pnr.'" id="payghost'.$pnr.'" type=hidden value="'.$p['payghost'].'">';
			echo '</td>';
			foreach($columns as $c) {
               	echo '<td class="small">';
               	if($c['link_as_email_in_lists']=='y') echo '<A class="small" HREF="mailto:'.
               		$p[$c['mysql_column_name']].'">';
               	if(preg_match("/(radioline|select_list|select_lang)/",$c['type']) && isset($c['lang'][$p[$c['mysql_column_name']]]))
               		echo $c['lang'][$p[$c['mysql_column_name']]];
               	else {
					$towrite=$p[$c['mysql_column_name']];
					if(!empty($_REQUEST['pay']) && $payment_mode=='view' && ($c['mysql_column_name']=='lname' || $c['mysql_column_name']=='fname')) $towrite=str_replace('?','',$towrite);
					echo $towrite;
				}
               	if($c['link_as_email_in_lists']=='y') '</A>';
               	echo '</td>';
            }
			if(empty($_REQUEST['pay']))  echo '
			<td class="small">'.$p['number_noshowup'].
                                                '/'.$p['number_reg'].'</td>';
			else {
				$disabled=" ";
				if(!empty($p['payghost'])) $disabled=" style='background:buttonface' ";
				if($payment_mode!="entry" && $payment_mode!="edit") echo '<td class="small"><input type=hidden name=payment_'.$pnr.' id=payment_'.$pnr.' value="'.$p['payment'].'">'.$p['payment'].'</td>';
				else echo '<td class="small"><input type=text'.$disabled.'onBlur="payment_num(this)" name=payment_'.$pnr.' id=payment_'.$pnr.' value="'.$p['payment'].'"><input type=hidden name=payment_num_'.$pnr.' id=payment_num_'.$pnr.' value="'.$p['payment_num'].'"><input type=hidden name=payment_init_'.$pnr.' value="'.$p['payment'].'"></td>';
				// var_dump($p);
				$sum_payment+=(float)$p['payment'];
				if($nlfpay_name!="") {
					if($payment_mode!="entry" && $payment_mode!="edit") echo '<td class="small"><input type=hidden name=payment_nlf_'.$pnr.' id=payment_nlf_'.$pnr.' value="'.$p['payment_nlf'].'">'.$p['payment_nlf'].'</td>';
					else echo '<td class="small"><input type=text onBlur="payment_num(this)" name=payment_nlf_'.$pnr.' id=payment_nlf_'.$pnr.' value="'.$p['payment_nlf'].'"><input type=hidden name=payment_nlf_init_'.$pnr.' value="'.$p['payment_nlf'].'"></td>';	
					$sum_payment_nlf+=$p['payment_nlf'];
				}
				echo '<td class="small" id="td_payment_num_'.$pnr.'">'.$p['payment_num'].'</td>';				
				if($payment_mode=="view") echo '<td class="small">'.dossier_name($p['dossier'],1).'</td>';				
			}

		if ($assigned || $invited) {
			echo '<td class="small">'.$p['invited'].'</td>
			      <td class="small">';
				  if(!$disabled ) {
					echo '
					<INPUT type=checkbox id="chkbx'.$pnr.'" name="reg'.$pnr.'" value="y"';
					if ((isset($_REQUEST['reg'.$pnr]) && $_REQUEST['reg'.$pnr]=="y") || !empty($_REQUEST['restricted_plist']) ) echo ' CHECKED';
					if ($disabled) echo ' DISABLED';
					echo '>';
					} elseif(isset($_REQUEST['reg'.$pnr])) echo $_REQUEST['reg'.$pnr]; else echo '?';
				echo '
				</td>';
				// Edit added by Maxim Frolov 
				if(check_allow("participants_edit")) if((empty($_REQUEST["disabled"]) && !(isset($_REQUEST["editlink"]) && empty($_REQUEST["editlink"])))  || !empty($_REQUEST["editlink"])) echo '<td><a target=_blank href="participants_edit.php?participant_id='.$p["participant_id"].'">Edit</a></td>';
				if((!empty($_REQUEST["sort"]) && $_REQUEST["sort"]=="participate_id") || !empty($_REQUEST["external_code"])) echo '<td>'.$p['participate_id'].'</td>';
				if(!empty($_REQUEST["external_code"]) && !empty($p["external_code"])) echo '<td><strong>'.$p['external_code'].'</strong></td>';
			}
	   	if ($registered || $shownup || $participated || $nonparticipated || $notpresent || $absent ||$todayabsent || $absentold || $absentnew || $new || $today || $todaynew || $todayopen || $todaystarted || $atdate || $athours || $absentatdate || $shownupatdate || $participatedatdate || $atmonth || $todayopen || $todayopenshownup || $todayshownup || $todayshownuponly || $tomorrow) {
	   		echo '<td class="small">';
				if(isset($payment_mode) && $payment_mode=="view") {
					echo time__format($lang['lang'],time__load_session_time($p),false,true,true,false);
					echo '
					</td>
					<td class="small">
					';
				}
				if (!$session_id) {
				echo '<INPUT type=hidden name="csession'.$pnr.'" value="'.$p['session_id'].'">';
				}
				if ($disabled) {if(isset($payment_mode) && $payment_mode=="view") echo session__build_name($p,"",true); else echo session__build_name($p);}
				   else echo select__sessions($p['session_id'],'session'.$pnr,$experiment_id,false,$supsess_criteria);
	   		echo '</td>
	   			<td class="small">';
			
			$disable_check=($disabled && !(!empty($_REQUEST['pay']) && !empty($payment_mode) && ($payment_mode=="entry" || $payment_mode=="edit") ) && ($registered || $nonparticipated || $notpresent || $absent ||$todayabsent || $absentold || $absentnew || $new || $today || $todaynew || $todayopen || $todaystarted || $atdate || $athours || $absentatdate || $shownupatdate || $participatedatdate || $atmonth || $todayopen || $todayopenshownup || $todayshownup || $todayshownuponly || $tomorrow));// !:  && (empty($_REQUEST['only_empty']) || $_REQUEST['only_empty']<0)
                if (!$disable_check && ($registered || $shownup || $nonparticipated || $notpresent || $absent ||$todayabsent || $absentold || $absentnew || $new || $today || $todaynew || $todayopen || $todaystarted || $atdate || $athours || $absentatdate || $shownupatdate || $participatedatdate || $atmonth || $todayopen || $todayopenshownup || $todayshownup || $todayshownuponly || $tomorrow)) {
                       		echo '<INPUT type=checkbox id="shup'.$pnr.'" name="shup'.$pnr.'" value="y" 
					onclick="checkshup('.$pnr.')"'; 
                               	if ($p['shownup']=="y") echo ' CHECKED';
				if ($disable_check) echo ' DISABLED';
				echo '>';
				}
                       	   else echo $p['shownup'];

	   		echo '</td>
	   			<td class="small">';
					
				  if(!$disable_check) {
					echo '
        	               		<INPUT type=checkbox id="part'.$pnr.'" name="part'.$pnr.'" value="y" 
						onclick="checkpart('.$pnr.')"'; 
        	                       	if ($p['participated']=="y") echo ' CHECKED';
					if ($disable_check) echo ' DISABLED';
                	        	echo '>';
					} elseif(!empty($p['participated'])) echo $p['participated']; else echo '?';
				echo '
	   			</td>';
			if($showrules) echo '
		   		<td class="small">';
			//if ($session_id) {
					if($showrules && !$disabled) {
						echo '<INPUT type=checkbox name="rules'.$pnr.'" value="y"';
                                	if ($p['rules_signed']=="y") echo ' CHECKED';
						if ($disabled) echo ' DISABLED';
						echo '>';
					}
					if($showrules && $disabled) echo $p['rules_signed'];
			//		}
			//	   else {
			//		if ($p['rules_signed']=="y") echo $lang['yes']; 
			//					else echo $lang['no_emp'];
			//		}
			if($showrules) echo '</td>';
				
			if($critical_count_apply && !($disable_check && empty($experiment_id)) && !$pay_report_noexp) {
				echo '
				<td class="small">
        	            <INPUT type=checkbox id="chkbx'.$pnr.'" name="restrict'.$pnr.'" value="y" 
						onclick="checkpart('.$pnr.')"'; 
					if ($disable_check) echo ' DISABLED';
                	echo '>
	   			</td>';
			}
			


				
				if(check_allow("participants_edit")) if((empty($_REQUEST["disabled"]) && !(isset($_REQUEST["editlink"]) && empty($_REQUEST["editlink"])))  || !empty($_REQUEST["editlink"])) echo '<td><a target=_blank href="participants_edit.php?participant_id='.$p["participant_id"].'">Edit</td>';
			}
			if(!($assigned || $invited)) {
				if((!empty($_REQUEST["sort"]) && $_REQUEST["sort"]=="participate_id") || !empty($_REQUEST["external_code"])) echo '<td>'.$p['participate_id'].'</td>';
				if(!empty($_REQUEST["external_code"]) && !empty($p["external_code"])) echo '<td><strong>'.$p['external_code'].'</strong></td>';
			}
		echo '</tr>';
		if ($shade) $shade=false; else $shade=true;
		}
		if(!empty($_REQUEST['pay']) && $payment_mode=="view" && $allow_payment_entry){
			$n_cols_before_pay=3;
			if(!empty($_REQUEST['show_emails'])) $n_cols_before_pay++;
			echo '<tr><td colspan='.$n_cols_before_pay.' style="text-align:right;border-top:2px solid black">Total :</td>';
			echo '<td style="border-top:2px solid black">'.$sum_payment.'</td>';
			if($nlfpay_name!="") echo '<td style="border-top:2px solid black">'.$sum_payment_nlf.'</td>';
			echo '</tr>';
			if($nlfpay_name!="") echo '<tr><td colspan='.$n_cols_before_pay.' style="text-align:right;border-top:2px solid black"><u>Total :</u></td>
								<td colspan=2 style="text-align:center;border-top:2px solid black"><u>'.($sum_payment+$sum_payment_nlf).'</u></td></tr>'; 
		}
		


function form__check_all($name,$count,$second_sel_name="",$second_un_name="") {
	global $lang;
	echo '<center>';
	echo '<input type=button value="'.$lang['select_all'].'" ';
	echo 'onClick="checkAll(\''.$name.'\','.$count.')';
	if ($second_sel_name) echo ';checkAll(\''.$second_sel_name.'\','.$count.')';
	echo '"><br>';
	echo '<input type=button value="'.$lang['select_none'].'" ';
	echo 'onClick="uncheckAll(\''.$name.'\','.$count.')';
	if ($second_un_name) echo ';uncheckAll(\''.$second_un_name.'\','.$count.')';
	echo '"></center>';
}

	if(!(!empty($_REQUEST['payment_mode']) && $_REQUEST['payment_mode']=='view' && !empty($_REQUEST['pay'])))
	if (check_allow('experiment_edit_participants')) {
	if(!$disable_check) { //empty($_REQUEST['disabled'])
	// table footer ...
	if($disabled) {$nr_normal_columns++; if(!empty($nlfpay_name)) $nr_normal_columns++; }
	echo '	<TR>
			<TD style="text-align:center" colspan='.$nr_normal_columns.'>';
	if(!empty($_REQUEST['payment_mode']) && !empty($_REQUEST['pay']) && ($registered || $shownup || $participated || $nonparticipated || $notpresent || $absent ||$todayabsent || $absentold || $absentnew || $new || $today || $todaynew || $todayopen || $todaystarted || $atdate || $athours || $absentatdate || $shownupatdate || $participatedatdate || $atmonth || $todayopen || $todayopenshownup || $todayshownup || $todayshownuponly || $tomorrow)) {
		echo lang('total'); if(!empty($nlfpay_name)) echo ' ('.lang('cash',false).')'; echo ' : <span id=total_cash></span>';
		echo '';
		if(!empty($nlfpay_name)) echo '&nbsp;&nbsp;&nbsp; '. lang('total').' ('.$nlfpay_name.')'.' : <span id=total_noncash></span>';
		$dossier_attr="";
		if($session_id!='') {
			$sessline=orsee_query("SELECT * FROM ".table("sessions")." WHERE session_id=$session_id");
			if(isset($sessline['payment_file'])) $dossier_attr=$sessline['payment_file'];
		}
		if(empty($dossier_attr) && $experiment_id!='') {
			$expline=orsee_query("SELECT * FROM ".table("experiments")." WHERE experiment_id=$experiment_id");
			if(isset($expline['payment_file'])) $dossier_attr=$expline['payment_file'];
		}
		$used_file=(empty($dossier_attr))?$dossier:$dossier_attr;
		$moneyrespline=orsee_query("SELECT money_responsible FROM ".table("payment_files")." WHERE id='$used_file'");
		if($moneyrespline!==false) {
			echo '<hr><small>'.lang('debit_account').' : '.helpers__pay_account_longname($moneyrespline['money_responsible']).' ';
			$accline0=orsee_query("SELECT SUM(amount) as file_amount FROM ".table("pay_accounts")." WHERE shortname='".$moneyrespline['money_responsible']."' AND payment_file='$dossier'");
			$amount0=0; if($accline0!==false && !empty($accline0['file_amount'])) $amount0=$accline0['file_amount'];
			echo "($amount0".lang('money_symbol')." ".lang('for_this_file',false).")";
			$accline=orsee_query("SELECT SUM(amount) as file_amount FROM ".table("pay_accounts")." WHERE shortname<>'bank' AND shortname<>'participants' AND payment_file='$dossier'");
			$amount1=0; if($accline!==false && !empty($accline['file_amount'])) $amount1=$accline['file_amount'];
			$amount2=helpers__regiedavance_amount_left(); //var_dump($amount2); 
			if($amount1>0 || $amount2==0) echo ", $amount1".lang('money_symbol').' '.lang('for_this_file',false).' '.lang('in_total',false);
			if($amount2>0) echo ", $amount2".lang('money_symbol').' '.lang('in_the lab_safe',false);
			if($amount1==0 && $amount2==0) echo " - <span style='color:brown'>".str_replace(" ","&nbsp;",lang('no_operation_will_be_made',false))."</span>";
			echo '.</small>';
		}

	}
	echo '</td>';

		if ($assigned || $invited) {
			echo '<TD></TD><TD>';
			form__check_all('reg',$result_count);
			echo '</TD>';
			}
	
		if ($registered || $shownup || $participated || $nonparticipated || $notpresent || $absent ||$todayabsent || $absentold || $absentnew || $new || $today || $todaynew || $todayopen || $todaystarted || $atdate || $athours || $absentatdate || $shownupatdate || $participatedatdate || $atmonth || $todayopen || $todayopenshownup || $todayshownup || $todayshownuponly || $tomorrow) {
			echo '<TD></TD>
				<TD>';
			if ($registered || $shownup || $nonparticipated || $notpresent || $absent ||$todayabsent || $absentold || $absentnew || $new || $today || $todaynew || $todayopen || $todaystarted || $atdate || $athours || $absentatdate || $shownupatdate || $participatedatdate || $atmonth || $todayopen || $todayopenshownup || $todayshownup || $todayshownuponly || $tomorrow) 
				form__check_all('shup',$result_count,'','part');
			echo '</TD>
				<TD>';
                		form__check_all('part',$result_count,'shup','');
			echo '</TD>
				<TD>';
			if ($showrules && $session_id)
				form__check_all('rules',$result_count);
			echo '</TD>';
			}
	echo '	</TR>';
	if(!$disabled && $result_count>2) if ($assigned || $invited) {
		echo '<TR><TD colspan='.($nr_normal_columns+2).' style="text-align:right" >';
		echo lang('select').'  <input type=number id="num_to_select" size=10 style="width:70px" />'.lang('participants',false).' <select name="custom_number_select" id="custom_number_select"><option value=0 selected>'.lang("randomly",false).'</option><option value=1>'.lang("from_top",false).'</option><option value=-1>'.lang("from_bottom", false).'</option></select><br><input type=button value="'.lang('select').'" id="ok_custom_number_select" onclick="checkCustomNumber(\'reg\','.$result_count.')"/>';
		// echo '<input type=button value="'.lang('select').'" id="ok_custom_number_select" onclick="checkCustomNumber(\'reg\','.$result_count.')"/>  <input type=number id="num_to_select" size=10 style="width:70px" />'.lang('participants',false).' <select name="custom_number_select" id="custom_number_select"><option value=0 selected>'.lang("randomly",false).'</option><option value=1>'.lang("from_top",false).'</option><option value=-1>'.lang("from_bottom", false).'</option></select> ';
		echo '</TD></TR>';
	}
	echo '
		</table>';
	


	echo '	<TABLE border=0>
			<TR>
				<TD>&nbsp;</TD>
			</TR>';
	if ($assigned || $invited) {
		if(!isset($_REQUEST['to_session'])) $_REQUEST['to_session']="";
		if(!isset($_REQUEST['check_if_full'])) $_REQUEST['check_if_full']="";
		if(!isset($_REQUEST['remember'])) $_REQUEST['remember']="";
		echo '	<TR>
				<TD>
					'.$lang['register_marked_for_session'].' ';
					echo select__sessions($_REQUEST['to_session'],'to_session',$experiment_id,false);
			echo '	</TD>
			</TR>
			<TR>
				<TD>
					'.$lang['check_for_free_places_in_session'].'
					<INPUT type=checkbox name="check_if_full" value="true"';
					if ($_REQUEST['check_if_full'] || ! $_REQUEST['remember']) 
						echo ' CHECKED';
					echo '>
				</TD>
			</TR>';
			}
		}
		else echo '	</table><br><TABLE border=0>';
	echo '	<TR>
			<TD align=center>';
	if(!empty($_REQUEST['pay'])) {
			echo '
			<INPUT name="payment_mode" type=hidden value="'.$payment_mode.'">
			<INPUT name="dossier" type=hidden value="'.$_REQUEST['dossier'].'">
			<INPUT name="pay" type=hidden value="'.$_REQUEST['pay'].'">';
		if(isset($_REQUEST['only_empty'])) echo '
			<INPUT name="only_empty" type=hidden value="'.$_REQUEST['only_empty'].'">';
	}
	elseif(!$disabled && ($registered || $shownup || $participated || $nonparticipated || $notpresent || $absent ||$todayabsent || $absentold || $absentnew || $new || $today || $todaynew || $todayopen || $todaystarted || $atdate || $athours || $absentatdate || $shownupatdate || $participatedatdate || $atmonth || $todayopen || $todayopenshownup || $todayshownup || $todayshownuponly || $tomorrow)) {
			echo '
			<table class="border border-light text-secondary" cellpadding=5><tr><td>'.lang('participants_who_change_session should_be_marked_as').'</td>
			';
			echo '<td class="small">';
			$pnr='sesschange';
			$disable_check=($disabled && !(!empty($_REQUEST['pay']) && !empty($payment_mode) && ($payment_mode=="entry" || $payment_mode=="edit") ) && ($registered || $nonparticipated || $notpresent || $absent ||$todayabsent || $absentold || $absentnew || $new || $today || $todaynew || $todayopen || $todaystarted || $atdate || $athours || $absentatdate || $shownupatdate || $participatedatdate || $atmonth || $todayopen || $todayopenshownup || $todayshownup || $todayshownuponly || $tomorrow));// !:  && (empty($_REQUEST['only_empty']) || $_REQUEST['only_empty']<0)
                if (!$disable_check && ($registered || $shownup || $nonparticipated || $notpresent || $absent ||$todayabsent || $absentold || $absentnew || $new || $today || $todaynew || $todayopen || $todaystarted || $atdate || $athours || $absentatdate || $shownupatdate || $participatedatdate || $atmonth || $todayopen || $todayopenshownup || $todayshownup || $todayshownuponly || $tomorrow)) {
                       		echo '<INPUT type=checkbox id="shup'.$pnr.'" name="shup'.$pnr.'" value="y" 
					onclick="checkshup(\''.$pnr.'\')"'; 
                               	// if ($p['shownup']=="y") echo ' CHECKED';
				if ($disable_check) echo ' DISABLED';
				echo '>';
				}
                       	   // else echo $p['shownup'];

			echo '<label for="shup'.$pnr.'">'.lang('shownup').'</label>';
	   		echo '</td>
	   			<td class="small">';
				  if(!$disable_check) {
					echo '
        	               		<INPUT type=checkbox id="part'.$pnr.'" name="part'.$pnr.'" value="y" 
						onclick="checkpart(\''.$pnr.'\')"'; 
        	                       	// if ($p['participated']=="y") echo ' CHECKED';
					if ($disable_check) echo ' DISABLED';
                	        	echo '>';
					} //elseif(!empty($p['participated'])) echo $p['participated']; else echo '?';
				echo '<label for="part'.$pnr.'">'.lang('participated').'</label>';
				echo '
	   			</td>';
		echo '<td>'.lang('in_the_new_session',false).'</td></tr></table><br>';
	}
	echo '
				<INPUT type=hidden name="result_count" value="'.$result_count.'">
				<INPUT type=hidden name="focus" value="'.$_REQUEST['focus'].'">
				<INPUT type=hidden name="experiment_id" value="'.$experiment_id.'">
				<INPUT type=hidden name="session_id" value="'.$session_id.'">
				<INPUT type=submit name="change" id="change" value="'.$lang['change'].'">
			</TD>
		</TR>';
	}

	echo '</table>';
	if($payment_active) echo '<br><br><center><input type=button id=autoinputbut onclick="showhideautoinput()" value="Show field for auto input" /><br><div id="autoinputfield" style="display:none">Please paste data, one line per subject, separated by tab or semicolon, the first line should be headers (including noms or ExternalInfo and gain)<br><textarea id=autoinputarea cols=50 rows=30></textarea><br><input type=button value="Proceed auto input" onclick="proceedautoinput()" /><br><span id=autoinputwarnings style="color:red"></span></div></center>';
	if($payment_active) echo '<INPUT type=hidden name="pay_operation_id" value="'.operation::get_new_id().'">';
	echo '
		</form>';

	if ($assigned || $invited || ($critical_count_apply && !empty($experiment_id) && !$pay_report_noexp)) {
		$alert_count="<br><br>"; $alert_count_sup="";
		if($critical_count_apply) $alert_count_sup="(by the last checkbox column)";
		if($result_count>$critical_count) $alert_count="<strong style='color:#F59'>More than $critical_count participants, restrict $alert_count_sup before changing!</strong><br>";
		echo $alert_count.'
		<form name="part_list_restricted" method=post  action="'.thisdoc().'">
			<INPUT type=hidden name="focus" value="'.$_REQUEST['focus'].'">
			<INPUT type=hidden name="experiment_id" value="'.$experiment_id.'">
			<INPUT type=hidden name="session_id" value="'.$session_id.'">
			<INPUT type=hidden name="restricted_plist" id="restricted_plist" value="">
			<INPUT type=submit name="restricted" value="'.lang('restrict_for_checked').'" onclick="restrict_checked()">
		</form>
		<script>
			function restrict_checked()
			{
				var res="";
				var ich=0;
				for(var i=1; i<='.$result_count.'; i++) {
					if(document.getElementById("chkbx"+i).checked) {
						if(ich>0) res+=", ";
						res+=document.getElementById("pid"+i).value;
						ich++;
					}
				}
				document.getElementById("restricted_plist").value=res;
				//alert(document.getElementById("restricted_plist").value);
			}
		</script>
		';
	}
	
	
	echo '
		<BR>
		<TABLE width="80%" border=0>
		<TR>
			<TD>';
				if(empty($_REQUEST['pay']))
	 			if ($session_id && $session['session_finished']!="y" && check_allow('session_send_reminder')) {
                                        if ($session['reminder_sent']=="y") {
                                                $state=$lang['session_reminder_state__sent'];
                                                $statecolor=$color['session_reminder_state_sent_text'];
						$explanation=$lang['session_reminder_sent_at_time_specified'];
						$send_button_title=$lang['session_reminder_send_again'];
                                                }
                                        elseif ($session['reminder_checked']=="y" && $session['reminder_sent']=="n") {
                                                $state=$lang['session_reminder_state__checked_but_not_sent'];
                                                $statecolor=$color['session_reminder_state_checked_text'];
						$explanation=$lang['session_reminder_not_sent_at_time_specified'];
                                                $send_button_title=$lang['session_reminder_send'];
                                                }
                                        else {
                                                $state=$lang['session_reminder_state__waiting'];
                                                $statecolor=$color['session_reminder_state_waiting_text'];
						$explanation=$lang['session_reminder_will_be_sent_at_time_specified'];
                                                $send_button_title=$lang['session_reminder_send_now'];
                                                }
                                        echo '<FONT color="'.$statecolor.'">'.$lang['session_reminder'].': '.$state.'</FONT><BR>';
					echo $explanation.'<BR><FORM action="session_send_reminder.php">'.
						'<INPUT type=hidden name="session_id" value="'.$session_id.'">'.
						'<INPUT type=submit name="submit" value="'.$send_button_title.'"></FORM>';
                                        }
					
	echo '		</TD><TD align=right>';
			if (check_allow('participants_bulk_mail')) 
                        	experimentmail__bulk_mail_form($experiment_id,$session_id);
	echo '		</TD>';

	echo '	</TR>
		</TABLE>';
		
		if(check_allow('session_edit')) {
			$whattocheck='experiment_id:'.$experiment_id;
			if(!empty($session_id)) {
				$whattocheck.="\n".'session_id:'.$session_id;
				echo '<BR><A class="small" HREF="session_edit.php?session_id='.$session_id.
									'">'.lang('edit session').'</A>'.(check_allow('log_file_participant_actions_show')?'&nbsp;&nbsp;&nbsp; / &nbsp;&nbsp;&nbsp;':'');
			}
			echo (check_allow('log_file_participant_actions_show')?'<a href="statistics_show_log.php?log=participant_actions&target='.urlencode($whattocheck).'&limit=0">'.lang('participant_actions').'</a>':'').'<BR><BR>';
		}

        if(!empty($experiment_id)) echo '  <A class="btn btn-primary" HREF="experiment_show.php?experiment_id='.$experiment_id.'">
                        '.$lang['mainpage_of_this_experiment'].'</A><BR><BR>

                </CENTER>';
if(!empty($_REQUEST['pay']) && !empty($dossier)) {
	$query="SELECT MAX(CAST(payment_num AS UNSIGNED)) as max_num FROM ".table('participate_at')." WHERE payment_file=$dossier";
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$query);
	$maxprev=0;
	if(mysqli_num_rows($result)>0) {
		$line=mysqli_fetch_assoc($result);
		if(is_numeric($line['max_num']))$maxprev=$line['max_num'];
	}
	// var_dump(mysqli_num_rows($result),$maxprev); exit;
	mysqli_free_result($result);
	$query="SELECT start_num_from as start_num FROM ".table('payment_files')." WHERE id=$dossier";
	$line=orsee_query($query);
	if($line['start_num']!='' && $line['start_num']>$maxprev) $maxprev=$line['start_num']-1;
	$idarray=array("payment");
	if($nlfpay_name!="") $idarray[]="payment_nlf";
	script__payment_num($maxprev,count($participants),$idarray);
}

if(empty($_REQUEST['pay']) && experiment__check_option($experiment_id,'by_couples') && ($registered || $shownup || $participated || $nonparticipated || $notpresent || $absent ||$todayabsent || $absentold || $absentnew || $new || $today || $todaynew || $todayopen || $todaystarted || $atdate || $athours || $absentatdate || $shownupatdate || $participatedatdate || $atmonth || $todayopen || $todayopenshownup || $todayshownup || $todayshownuponly || $tomorrow)) {
	include_once("../".$GLOBALS['settings__public_folder']."/bycouples.php");
	$bchelper = new ByCouplesHelper();
	if(!empty($_REQUEST['bcsubscribe']) && !empty($_REQUEST['bcpartner']) && !empty($session_id)) {
		$bchelper->subscribePartner($_REQUEST['bcsubscribe'],$_REQUEST['bcpartner'],$session_id,$experiment_id);
		unset($_REQUEST['bcsubscribe']); 
		unset($_REQUEST['bcpartner']);
		redirect($GLOBALS['settings__admin_folder'].'/'.thisdoc().'?'.http_build_query($_REQUEST));
	}
	$bchelper->writeCouplesTable();
	// var_dump($columns);
	
}


include ("footer.php");

?>

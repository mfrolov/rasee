<?php
// part of orsee. see orsee.org
ob_start();

$menu__area="participants";
$title="show participants";
$query_modules=array("field","field2","noshowups","nr_participations","subjectpool",
			"participant_form_fields",
			"experiment_classes","experiment_participated_or","experiment_participated_and","experiment_assigned_or","experiment_registered_future_or", "last_invitation", "last_activity");

include ("header.php");

	$allow=check_allow('participants_show','participants_main.php');
	$deleted= (isset($_REQUEST['deleted']) && $_REQUEST['deleted']) ? $_REQUEST['deleted'] : "n";

	echo '	<center>
		<BR><BR>
			<h4>'.$lang['edit_participants'].'</h4>
		';

	$query_modules=query__get_participant_form_modules($query_modules);

	if (isset($_REQUEST['show']) && $_REQUEST['show']) {

		$sort = (isset($_REQUEST['sort']) && $_REQUEST['sort']) ? $_REQUEST['sort']:"lname,fname,email";

		if (  (isset($_REQUEST['new_query']) && $_REQUEST['new_query']) || (!isset($_SESSION['assign_select_query']) || !$_SESSION['assign_select_query'])) {
			unset($_REQUEST['new_query']);
				if (!isset($_REQUEST['use'])) $_REQUEST['use']=array();
				if (!isset($_REQUEST['con'])) $_REQUEST['con']=array();
	       		$where_clause=query__where_clause($query_modules,
							  $_REQUEST['use'],
							  $_REQUEST['con']);

			if (!$where_clause) $where_clause=query__where_clause_module("all");

			$select_query="SELECT ".table('participants').".* 
                        	FROM ".table('participants')."  
							WHERE";
							if ($deleted=='b') $select_query.=" (deleted='y' OR deleted='n') ";
							else $select_query.=" deleted='".$deleted."' ";
                        	$select_query.=$where_clause;

            $_SESSION['assign_where_clause']=$where_clause;
			$_SESSION['assign_select_query']=$select_query;
			$_SESSION['assign_request']=$_REQUEST;
			$_SESSION['deleted']=$deleted;
		} else {
			$where_clause=$_SESSION['assign_where_clause'];
            $select_query=$_SESSION['assign_select_query'];
			$deleted=$_SESSION['deleted'];
		}

		//echo '<A HREF="participants_excel_import.php">'.$lang['excel_import'].'</A><BR>';
		
		$editword='edit';
		if($deleted=='b') $editword='editb';

		$assign_ids=query_show_result($select_query,$sort,$editword);
		$_SESSION['plist_ids']=$assign_ids;

		if (check_allow('participants_bulk_mail')) {
			echo '<BR><BR><TABLE width=80% border=0><TR><TD>';
				experimentmail__bulk_mail_form();
			echo '</TD></TR></TABLE>';
			}

		}

	else 	{
		if (!empty($_REQUEST['use_saved_request'])) {
			$jssr=rasee_db_load_value('experiment_queries',$_REQUEST['use_saved_request'],'id','json');
			$_SESSION['assign_request']=json_decode($jssr,true);
		}
		elseif((!empty($_SESSION['assign_request']) || !empty($_REQUEST['request_name'])) && empty($_REQUEST['new'])) {
			$unneeded_in_save=["show","request_name","save_request","delete_request","delete_request_id","new_query","use_saved_request"];
			if (!empty($_SESSION['assign_request'])) {
				$jsar=json_encode($_SESSION['assign_request']);
				$exp_queries=orsee_query("SELECT * FROM ".table('experiment_queries'). " WHERE experiment_id=0 ORDER BY id DESC", "return_same"); //(experiment_id=0 and `user`='".$expadmindata['adminname']."') 
				$used_request=[];
				if($exp_queries!==false) foreach($exp_queries as $eq) {
					$allequal=true;
					$prevsess=json_decode($eq["json"],true);
					foreach($_SESSION['assign_request'] as $kar=>$sar) if(!in_array($kar,$unneeded_in_save)) {
						if(!isset($prevsess[$kar]) || $prevsess[$kar]!=$sar) {
							$allequal=false;
							// var_dump("error:",$kar);
							break;
						}
					}
					if($allequal) {
						$_REQUEST['use_saved_request']=$eq["id"];
						$used_request=$eq;
						break;
					}
				}
			}
			if(!empty($_REQUEST['request_name']) ) {
				$arrtosave=$_REQUEST;
				foreach($unneeded_in_save as $us) {
					if(isset($arrtosave[$us])) unset($arrtosave[$us]);
				}
				$jsar=json_encode($arrtosave);
				// var_dump($_SESSION['assign_request']);
				rasee_db_save_array(array('json'=>$jsar),'experiment_queries',array("experiment_id"=>0,"name"=>trim($_REQUEST['request_name']),'user'=>$expadmindata['adminname']));
				message("<q>".trim($_REQUEST['request_name']). "</q> ". lang("saved",false));
				redirect($GLOBALS['settings__admin_folder'].'/'.thisdoc().'?deleted='.$_REQUEST['deleted']);
			}
			elseif(!empty($_REQUEST['delete_request']) && !empty($_REQUEST['delete_request_id']) && isset($used_request['id']) && $_REQUEST['delete_request_id'] == $used_request['id'] && isset($used_request['user']) && $expadmindata['adminname']==$used_request['user'] ) {
				orsee_query("DELETE FROM ".table('experiment_queries')." WHERE `id`='".$used_request['id']."'");
				message("<q>".$used_request['name']. "</q> ". lang("deleted",false));
				redirect($GLOBALS['settings__admin_folder'].'/'.thisdoc().'?deleted='.$_REQUEST['deleted']);
				
			}

		}
		if (!isset($_SESSION['assign_request']) || !empty($_REQUEST['new'])) $_SESSION['assign_request']=array();
			else {
				if(isset($_SESSION['assign_request']['use']) && isset($_REQUEST['use']) && empty($_REQUEST['use'])) unset($_REQUEST['use']);
				if(isset($_SESSION['assign_request']['con']) && isset($_REQUEST['con']) && empty($_REQUEST['con'])) unset($_REQUEST['con']);
				if(isset($_SESSION['assign_request']['new']) && isset($_REQUEST['new']) && empty($_REQUEST['new'])) unset($_REQUEST['new']);
				$new_req=array_merge($_SESSION['assign_request'],$_REQUEST);
				$_REQUEST=$new_req;
				$_SESSION['assign_request']=$_REQUEST;
				}
		$deleted= (isset($_REQUEST['deleted']) && $_REQUEST['deleted']) ? $_REQUEST['deleted'] : "n";

		echo participants__count_participants((($deleted!='b')?"deleted = '".$deleted."'":"deleted = 'n' OR deleted = 'y'"),true);
		echo ' '.$lang['xxx_participants_registered'].'
			<BR><BR>
	
        		<FORM action="'.thisdoc().'" method="POST">
			<INPUT type=hidden name="new_query" value="true">';
			// if ($deleted=='y') echo '<INPUT type=hidden name="deleted" value="y">';
			echo '<INPUT type=hidden name="deleted" value="'.$deleted.'">';
        		query__form($query_modules);


        	echo '	</FORM>';

		}


	echo '</CENTER>';

include ("footer.php");

?>

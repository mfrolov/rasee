<?php
// part of orsee. see orsee.org
ob_start();

$title="experiment mainpage";

include ("header.php");


	if (!$_REQUEST['experiment_id']) redirect ($GLOBALS['settings__admin_folder']."/");
		else $experiment_id=$_REQUEST['experiment_id'];

	$allow=check_allow('experiment_show','experiment_main.php');

	// load experiment data into array experiment
    	$experiment=orsee_db_load_array("experiments",$experiment_id,"experiment_id");

	echo '<BR>
		<center>
		<h4>'.$experiment['experiment_name'].'</h4>
		';

	show_message();

$sup_exp_part_show_link="";
if(experiment__check_option($experiment_id,'by_couples')) {
	include_once("../".$GLOBALS['settings__public_folder']."/bycouples.php");
	$bchelper = new ByCouplesHelper();
	$sup_exp_part_show_link.="&".$bchelper->mailtablefield."=1";
}
// show basic settings
	if(!isset($lang[$experiment['experiment_type']])) $lang[$experiment['experiment_type']]=$experiment['experiment_type'];
	// var_dump(experiment__get_experiment_class_names($experiment['experiment_class']));
	// var_dump($experiment['experiment_class']);
	echo '<BR>
	<table border=1 width=90%>
		<TR>
			<TD bgcolor="'.$color['list_header_background'].'" colspan=2><h3>'.$lang['basic_data'].'</h3></TD>
		</TR>
		<TR><TD>'.$lang['id'].':</TD><TD>'.$experiment['experiment_id'].'</TD></TR>
		<TR><TD>'.$lang['name'].':</TD><TD>'.$experiment['experiment_name'].'</TD></TR>
		<TR><TD>'.$lang['public_name'].':</TD><TD>'.$experiment['experiment_public_name'].'</TD></TR>
		<TR><TD>'.$lang['type'].':</TD>
		<TD>'.$lang[$experiment['experiment_type']].' ('.$experiment['experiment_ext_type'].')</TD></TR>
		<TR><TD>'.$lang['class'].':</TD>
			<TD>'.implode(", ",experiment__get_experiment_class_names($experiment['experiment_class'])).'</TD></TR>
		<TR><TD>'.$lang['description'].':</TD><TD>'.$experiment['experiment_description'].'</TD></TR>
		<TR><TD>'.$lang['experimenter'].':</TD><TD>'.experiment__list_experimenters($experiment['experimenter'],true,true).
			'</TD></TR>
		<TR><TD>'.$lang['get_emails'].':</TD><TD>'.experiment__list_experimenters($experiment['experimenter_mail'],true,true).
			'</TD></TR>
		<TR><TD>'.$lang['email_sender_address'].':</TD><TD>'.$experiment['sender_mail'].'</TD></TR>
		';
		$query="SELECT payment_file FROM ".table("experiments")." WHERE experiment_id=".$experiment['experiment_id'];
		$line=orsee_query($query);
		if($line!==false && !empty($line["payment_file"])) echo '<TR><TD>'.lang('payment_file_choosen').':</TD><TD>'.dossier_name($line["payment_file"],1);
		if($line!==false && !empty($line["payment_file"]) && check_allow('experimentclass_edit')) echo '
		 <small>(
		 <a target=_blank href="dossiers_paiement_main.php?addit=1&id='.$line['payment_file'].'&show_statistics=1">'.lang('details',false).'/'.lang('edit',false).'</a>...,
		 <a target=_blank href="payment_accounts.php?limfile='.$line['payment_file'].'">'.lang('state_of_accounts',false).'</a>...,
		 <a target=_blank href="dossiers_paiement_report.php?dossier='.$line['payment_file'].'">'.lang('payment_report',false).'</a>...,
		 <a target=_blank href="payment_operations.php?addit=1&dossier='.$line["payment_file"].'">'.lang('new_operation',false).'</a>...
		 )</small>';
		 echo '</TD></TR>';

	if ($experiment['experiment_type']=="laboratory") {
		echo '<TR><TD>'.$lang['from'].':</TD>
			<TD>'.sessions__get_first_date($experiment['experiment_id']).'</TD></TR>
			<TR><TD>'.$lang['to'].':</TD>
			<TD>'.sessions__get_last_date($experiment['experiment_id']).'</TD></TR>';
		}
	if (downloads__files_downloadable($experiment['experiment_id'])) {
			echo '<TR><TD valign="top">'.$lang['files'].':</TD>
				<TD>';
			downloads__list_files($experiment['experiment_id']);
			echo '</TD></TR>';
			}

	// does the following make sense? we order them on different pages! OK, but this is the
	// experiment show page. Maybe the user has forgotten ... ;-)
	if ($experiment['experiment_type']=="laboratory") {
		echo '<TR><TD colspan=2>';
		if ($experiment['experiment_finished']=="y")
			echo $lang['experiment_finished'];
		   else echo $lang['experiment_not_finished'];
		echo '</TD></TR>';
		}

	echo '<TR><TD bgcolor="'.$color['list_options_background'].'" colspan=2>
		'.$lang['options'].':<BR><BR></TD></TR>
		<TR><TD colspan=2>
			<TABLE width=100% border=0>
				<TR><TD>';
					if (check_allow('experiment_edit')) echo '
						<A HREF="experiment_edit.php?experiment_id='.
							$experiment['experiment_id'].'">'.
							$lang['edit_basic_data'].'</A>';
	echo '				</TD><TD>';
					if (check_allow('download_experiment_upload')) {
					echo '<A HREF="download_upload.php?experiment_id='.
						$experiment['experiment_id'].'">'.
						$lang['upload_file'].'</A> ';
 					echo help('upload_files');
					}
					
		$suppage=false; $suppageoption=""; $supquestoption=""; $supquestoption2=""; $experimentpublicname=$experiment['experiment_public_name'];
		$expepublicarr2=explode("_",$experimentpublicname);
		if($experimentpublicname=="expe39") {$suppage=true; $suppageoption="paypal";}
		if($experimentpublicname=="expe78 (sur 2 jours)") {$suppage=true; $suppageoption="paypal";}
		if($experimentpublicname=="expe59d") {$suppage=true; $suppageoption="documentaire";}
		if($experimentpublicname=="expe97") {$suppage=true; $suppageoption="FNAC";}
		if($experimentpublicname=="expe124_eeg") {$suppage=true; $suppageoption="supquest"; $suppageoption2="expe_eeg_log";}
		if($experimentpublicname=="expe110 (sur 2 jours)") {$suppage=true; $suppageoption="supquest";}
		if(count($expepublicarr2)>1)if($experimentpublicname=="expe129" || trim($expepublicarr2[1])=="anglais" || trim($expepublicarr2[1])=="english") {$suppage=true; $suppageoption="english"; /*$suppageoption2="expe_".$suppageoption."_log";*/}
		if(count($expepublicarr2)>1) if(trim($expepublicarr2[1])=="assas" || trim($expepublicarr2[1])=="jourdan") {$suppage=true;  $suppageoption="address"; /*$suppageoption2="expe_".$suppageoption."_log";*/}
		if(count($expepublicarr2)>1) if(trim($expepublicarr2[1])=="win" || trim($expepublicarr2[1])=="windows") {$suppage=true;  $suppageoption.=",forcewin";}
		if(count($expepublicarr2)>1) if(trim($expepublicarr2[1])=="lydia") {$suppage=true;  $suppageoption.=",lydia";}
		if(!empty(experiment__get_value($experiment['experiment_id'],'add_step_subscr_info'))) {$suppage=true; $suppageoption.=",info";}
		if(!empty(experiment__get_value($experiment['experiment_id'],'suppl_exp_description'))) {$suppage=true; $suppageoption.=",description"; if(str_replace(" via ","",$experiment['experiment_name'])!=$experiment['experiment_name']) {$suppageoption.=",redirect";}}
		if(experiment__check_option($experiment_id,'supquest')) {$suppage=true; $suppageoption="supquest";}
		if($suppage) {
					$supquesttable_arr=explode(" ",$experimentpublicname);
					echo' </TD><TD> ';
					$suppageoption_arr=explode(',',$suppageoption);
					$spo_k=0;
					foreach($suppageoption_arr as $spo) if(!empty($spo)) {$spo_k++; if($spo_k>1) echo ", "; echo'<A HREF="downloadpaypallog.php?option='.$spo.'">Download '.$spo.'_log_table</A>';}
					if($suppageoption=="supquest") {
						// if(empty($suppageoption2)) $suppageoption2="supquestlogtable";
						$supquestoption="supquest_".$supquesttable_arr[0];
						echo '&nbsp;&nbsp; <A HREF="downloadpaypallog.php?option='.$supquestoption.'">Download '.$supquestoption.'_table</A>';
						if(!empty($suppageoption2)) echo '&nbsp;&nbsp; <A HREF="downloadpaypallog.php?option='.$suppageoption2.'">Download '.$suppageoption2.'_table</A>';
					}
		}
					
	echo '			</TD></TR>
			</TABLE>
		</TD></TR>
	</TABLE>
	</center>';


	if ($experiment['experiment_type']=="laboratory") {
		
		if(!empty($_REQUEST['close_outdated_sessions'])) {
			if (!check_allow('experiment_restriction_override'))
				check_experiment_allowed($experiment,$GLOBALS['settings__admin_folder']."/experiment_show.php?experiment_id=".$experiment['experiment_id']);
			$open_sessions=experiment__list_session_id($experiment['experiment_id'],true);
			$n_s_done=0;
			if(!empty($open_sessions)) {
				$sess_to_close=array();
				$now=time();
				foreach($open_sessions as $session_id) {
					if($now > sessions__get_session_end_time(array(),$session_id)) $sess_to_close[]=$session_id;
				}
				
				if(!empty($sess_to_close)) {
					$q='UPDATE '.table('sessions').' SET session_finished="y" WHERE experiment_id="'.$experiment['experiment_id'].'" AND session_id IN ('.implode(',',$sess_to_close).')';
					orsee_query($q);
					$n_s_done=count($sess_to_close);
					// var_dump($n_s_done,$done); exit;
				}
			}
			message($n_s_done.' '.lang('sessions_closed',false));
			redirect($GLOBALS['settings__admin_folder']."/".thisdoc().'?experiment_id='.$experiment['experiment_id']);
		}
	// session summary

	echo '<center>
		<BR>
		<table border=1 width=90%>
		<TR>
			<TD bgcolor="'.$color['list_header_background'].'" colspan=2>
				<h3>'.$lang['sessions'].'</h3>
			</TD>
		</TR>
		<TR>
			<TD colspan=2>
				'.experiment__count_sessions($experiment['experiment_id']).' '.
				$lang['xxx_sessions_registered'].'<BR>
			</TD>
		</TR>

		<TR>
			<TD colspan=2 bgcolor="'.$color['list_list_background'].'">

			<TABLE border=0 width=100%>';

     	$query="SELECT *
      		FROM ".table('sessions')."
        	WHERE experiment_id='".$experiment['experiment_id']."'
      		ORDER BY session_start_year, session_start_month, session_start_day,
		 session_start_hour, session_start_minute";
	$done=orsee_query($query,"sessions__format_alist");

	echo '		</TABLE>

			</TD>
		</TR>
		<TR>
			<TD bgcolor="'.$color['list_options_background'].'" colspan=2>
				'.$lang['options'].':<BR><BR>
			</TD>
		</TR>
		<TR>
			<TD>';
				if (check_allow('session_edit')) echo '
					<A HREF="session_edit.php?experiment_id='.
						$experiment['experiment_id'].'">'.
					lang('create_new_session').'</A>';
	echo '		</TD>
			<TD style="text-align:right">';
				if (check_allow('session_edit')) echo '
					<A HREF="'.thisdoc().'?experiment_id='.
						$experiment['experiment_id'].'&close_outdated_sessions=1">'.
					lang('close_outdated_sessions').'</A>';
	echo '		</TD>
		</TR>
		</TABLE>
		</center>';

}




// participant summary for laboratory experiments
	if ($experiment['experiment_type']=="laboratory"  || $experiment['experiment_type']=="internet") {

		$allow_sp=check_allow('experiment_show_participants'); // show links to participant lists?
		$count_registered=experiment__count_registered($experiment['experiment_id']);
		$count_assigned=experiment__count_participate_at($experiment['experiment_id']);
		$count_available=$count_assigned-$count_registered;
		echo '	<center>
			<BR>
			<table border=1 width=90%>
			<TR>
				<TD bgcolor="'.$color['list_header_background'].'" colspan=2>
					<h3>'.$lang['participants'].'</h3>
				</TD>
			</TR>
			<TR>
				<TD>';
					if ($allow_sp) echo '<A HREF="experiment_participants_show.php?experiment_id='.
								$experiment['experiment_id'].$sup_exp_part_show_link.'">';
					echo $lang['assigned_subjects'];
					if ($allow_sp) echo '</A>';
					echo ':
					'.help('assigned_subjects').'
				</TD>
				<TD>
					'.$count_assigned.' ('.$count_available.' '.lang('not_yet_registered', false).')
				</TD>
			</TR>
			<TR>
				<TD>';
					if ($allow_sp) echo '&nbsp;&nbsp;<A HREF="experiment_participants_show.php?experiment_id='.
								$experiment['experiment_id'].$sup_exp_part_show_link.'&focus=invited">';
					echo $lang['invited_subjects'];
					if ($allow_sp) echo '</A>';
					echo ':
					'.help('invited_subjects').'
				</TD>
				<TD>
					'.experiment__count_invited($experiment['experiment_id']).'
				</TD>
			</TR>
			<TR>
				<TD>';
					if ($allow_sp) echo '<A HREF="experiment_participants_show.php?experiment_id='.
								$experiment['experiment_id'].$sup_exp_part_show_link.'&focus=registered">';
					echo $lang['registered_subjects'];
					if ($allow_sp) echo '</A>';
					echo ':
					'.help('registered_subjects').'
				</TD>
				<TD>
					'.$count_registered.'
				</TD>
			</TR>
			<TR>
				<TD>';
					if ($allow_sp) echo '&nbsp;&nbsp;<A HREF="experiment_participants_show.php?experiment_id='.
								$experiment['experiment_id'].$sup_exp_part_show_link.'&focus=shownup">';
					echo $lang['shownup_subjects'];
					if ($allow_sp) echo '</A>';
					echo ': 
					'.help('shownup_subjects').'
				</TD>
				<TD>
					'.experiment__count_shownup($experiment['experiment_id']).'
				</TD>
			</TR>
			<TR>
				<TD>';
					if ($allow_sp) echo '&nbsp;&nbsp;<A HREF="experiment_participants_show.php?experiment_id='.
								$experiment['experiment_id'].$sup_exp_part_show_link.'&focus=participated">';
					echo $lang['subjects_participated'];
					if ($allow_sp) echo '</A>';
					echo ': 
					'.help('subjects_participated').'
				</TD>
				<TD>
					'.experiment__count_participated($experiment['experiment_id']).'
				</TD>
			</TR>
			<TR>
				<TD>';
					if ($allow_sp) echo '&nbsp;&nbsp;<A HREF="experiment_participants_show.php?experiment_id='.
								$experiment['experiment_id'].$sup_exp_part_show_link.'&focus=nonparticipated">';
					if(!isset($lang['nonparticipated']))$lang['nonparticipated']=($lang['lang']=='fr')?"Participants enregistr&eacute;s mais pas particip&eacute;":"Participants registered but not participated";
					echo $lang['nonparticipated'];
					if ($allow_sp) echo '</A>';
					echo ': 
					'.help('nonparticipated').'
				</TD>
				<TD>
					'.experiment__count_nonparticipated($experiment['experiment_id']).'
				</TD>
			</TR>
			<TR>
				<TD>';
					if ($allow_sp) echo '&nbsp;&nbsp;<A HREF="experiment_participants_show.php?experiment_id='.
								$experiment['experiment_id'].$sup_exp_part_show_link.'&focus=absent"><small>';
					echo lang('participats_registered_but_not_participated_nor_present');
					if ($allow_sp) echo '</small></A>';
					echo ': 
					<small>'.help('subjects_absent').'</small>
				</TD>
				<TD>
					'.experiment__count_absent($experiment['experiment_id']).'
				</TD>
			</TR>';
			if ($allow_sp) {
				echo '
					<TR style="height:22px">
						<TD>';
							echo lang('participants_for_date(s)');
							echo ': <input type="text" id="datepicker1" style="height:15px;text-align:center;color:grey" value="'.lang('today').'" class="datepicker"> - <input type="text"  style="height:15px;text-align:center;color:grey" value="'.lang('today').'" id="datepicker2" class="datepicker">
						</TD>
						<TD>
							<button id=sdbutall style="">'.lang('registered_subjects').'</button> &nbsp;&nbsp; <button id=sdbutabs style="">'.lang('absent_subjects_only').'</button>
						</TD>
					</TR>
					<TR>
						<TD bgcolor="'.$color['list_options_background'].'" colspan=2>
							'.$lang['options'].':<BR><BR>
						</TD>
					</TR>';
			}
			if (check_allow('experiment_assign_participants')) echo '
			<TR>
				<TD>
					<A HREF="experiment_add_participants.php?experiment_id='.
					$experiment['experiment_id'].'">'.$lang['assign_subjects'].'</A><BR>
				</TD>
				<TD>
					<A HREF="experiment_drop_participants.php?experiment_id='.
					$experiment['experiment_id'].'">'.
					$lang['delete_assigned_subjects'].'</A><BR>
				</TD>
			</TR>';
		echo '	<TR>
				<TD>';
					if (check_allow('experiment_invitation_edit')) echo '
						<A HREF="experiment_mail_participants.php?experiment_id='.
						$experiment['experiment_id'].'">'.$lang['send_invitations'].'</A><BR>';
		echo '		</TD>';
		if($allow_sp) echo '
				<TD>
				<a href="experiment_participants_add_external_code.php?experiment_id='.$experiment['experiment_id'].'">'.lang('add_external_code').'</a>
				</TD>';
		echo '	
		</TR>
			</TABLE>
			</center>';
		if($allow_sp) {
		echo '
		  <br><br>
		  <link rel="stylesheet" href="../jcryption/jquery-ui.min.css">
		  <script src="../jcryption/jquery-ui.min.js"></script>
		  ';
		$dpreg='';
		if(lang('lang')=='fr') {
			echo '
			  <script src="../jcryption/datepicker-fr.js"></script>
			  ';
			$dpreg='fr';
		}
		echo '
		  <script>
		  $( function() {
			$.datepicker.setDefaults( $.datepicker.regional[ "'.$dpreg.'" ] );
			$( ".datepicker" ).datepicker({
			  dateFormat: "dd/mm/yy",
			  changeYear: true,
			  onSelect: function() {let myid=this.id; if(myid=="datepicker1" && $("#datepicker2").val()=="'.lang('today').'") $("#datepicker2").val(""); if(myid=="datepicker2" && $("#datepicker1").val()=="'.lang('today').'") $("#datepicker1").val("");  }
			});
			$("#sdbutall").click(function() {
				var atdate="atdate:";
				if($("#datepicker1").val()!="") {
					atdate+=$("#datepicker1").val()
					if($("#datepicker2").val()!="") atdate+="-";
				}
				if($("#datepicker2").val()!="") atdate+=$("#datepicker2").val()
				if($("#datepicker1").val()=="'.lang('today').'" || $("#datepicker12").val()=="'.lang('today').'") atdate="today";
				$("<input>").attr({
					type: "hidden",
					name: "focus",
					value: atdate
				}).appendTo("#reportshowform");
				$("#reportshowform").submit();
			});
			$("#sdbutabs").click(function() {
				var atdate="absentatdate:";
				if($("#datepicker1").val()!="") {
					atdate+=$("#datepicker1").val()
					if($("#datepicker2").val()!="") atdate+="-";
				}
				if($("#datepicker2").val()!="") atdate+=$("#datepicker2").val()
				if($("#datepicker1").val()=="'.lang('today').'" || $("#datepicker12").val()=="'.lang('today').'")  atdate="todayabsent";
				$("<input>").attr({
					type: "hidden",
					name: "focus",
					value: atdate
				}).appendTo("#reportshowform");
				$("#reportshowform").submit();
			});
		  } );
		  
		  </script>
		
		<form method="get" id="reportshowform" action="experiment_participants_show.php">
		<input type="hidden" name=experiment_id value="'.$experiment['experiment_id'].$sup_exp_part_show_link.'">
		</form>';
		
		}
	}



include ("footer.php");

?>

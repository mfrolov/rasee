<?php
// part of orsee. see orsee.org
ob_start();

$menu__area= (isset($_REQUEST['participant_id']) && $_REQUEST['participant_id']) ? "participant_edit" : "participant_create";
$title="edit participant";

/*
todos:
- check email exists
- internet experiment participation statistics
*/


include ("header.php");

	if (isset($_REQUEST['participant_id']) && $_REQUEST['participant_id']) $participant_id=$_REQUEST['participant_id'];
		else $participant_id="";

$maintable="participants";
if(!empty($_REQUEST['temptable'])) $maintable="participants_temp";

	$allow=check_allow('participants_edit','participants_main.php');

	$continue=true; $errors__dataform=array();
	
	$form_title=$lang['edit_participant'];
	if($maintable=="participants_temp") $form_title.=" ".lang("from_temp_table",false)."";
	if (isset($_REQUEST['add']) && $_REQUEST['add']) { 
	
		$nparticipants=1;
		if(!empty($_REQUEST['several']) && !empty($_REQUEST['email'])) {
			$emails=preg_split("/[\r\n]+/",trim($_REQUEST['email']));
			$nparticipants=count($emails);
			$_REQUEST_INIT=$_REQUEST;
		}
		
		$ncorrect=0; $nerrors=0;
		
		for($pindex=0; $pindex<$nparticipants; $pindex++) {
			if($nparticipants>1) {
				$new_request=[];
				foreach ($_REQUEST_INIT as $k=>$v) {
					if(!is_array($v)) {
						$clines=preg_split("/[\r\n]+/",trim($v));
						$new_request[$k]=(count($clines)>1)?$clines[$pindex]:end($clines);
					}
					else $new_request[$k]=$v;
				}
				$_REQUEST=$new_request;
			}
			// checks and errors
			foreach ($_REQUEST as $k=>$v) {
				if(!is_array($v)) $_REQUEST[$k]=trim($v);
			}
			$errors__dataform=participantform__check_fields($_REQUEST,true);		
			$error_count=count($errors__dataform);
			if ($error_count>0) $continue=false;

			if ($continue) {
				$participant=$_REQUEST;


				if (!$participant_id) {
					$participant['participant_id']=participant__create_participant_id();
					$participant['participant_id_crypt']=unix_crypt($participant['participant_id']);
					$participant['creation_time']=time();
					if (isset($_REQUEST['subpool_id']) && $_REQUEST['subpool_id']) $participant['subpool_id']=$_REQUEST['subpool_id'];
					else $participant['subpool_id']=$settings['subpool_default_registration_id'];
					if (!isset($participant['language']) || !$participant['language']) $participant['language']=$settings['public_standard_language'];
				}

				$prevparticipant=orsee_db_load_array($maintable,$participant['participant_id'],"participant_id");
				$updatedkeys=array(); $updatedkeys1=array();
				$pform=participantform__define();
				$checkboxkeys=array();
				foreach($pform as $pf) if(!empty($pf['checkbox_type']) && $pf['checkbox_type']!='n') $checkboxkeys[]=$pf['mysql_column_name'];
				foreach($prevparticipant as $ppk=>$ppv) {
					if(isset($participant[$ppk]) && $participant[$ppk]!=$ppv) {
						$updatedkeys[]=$ppk;
						$updatedkeys1[]=$ppk.'('.str_replace("'","�",$ppv).')';
					}
					if(!isset($participant[$ppk])) {
						$keyinform=false;
						if(in_array($ppk,$checkboxkeys)) $participant[$ppk]=null;
					}
				}
				$ignore_empty=($nparticipants>1);

				$done=rasee_db_save_array($participant,$maintable,["participant_id"=>$participant['participant_id']],$ignore_empty,true);
				if ($done) {
					$ncorrect++;
					$supmess=($nparticipants>1)?" ($ncorrect)":"";
					if($pindex == $nparticipants-1) message($lang['changes_saved'].$supmess);
				}
				else {
					$nerrors++;
					if($nparticipants>1) message(lang('error participant').' '.($pindex+1));
				}
				// var_dump($participant); exit;
				
				if (!$participant_id && isset($_REQUEST['register_session']) && $_REQUEST['register_session']=='y') {
					$session=orsee_db_load_array("sessions",$_REQUEST['session_id'],"session_id");
					if ($session['session_id']) {
						$query="INSERT INTO ".table('participate_at')." 
									SET participant_id='".$participant['participant_id']."',
										session_id='".$session['session_id']."', 
										experiment_id='".$session['experiment_id']."',
										registered='y'";
						$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error());
								message($lang['registered_participant_for'].' 
										<A HREF="experiment_participants_show.php?experiment_id='.
							$session['experiment_id'].'&session_id='.$session['session_id'].
							'&focus=registered">'.session__build_name($session).'</A>.');
					}
				}

				if ($done) {
					if (isset($_REQUEST['participant_id']) && $_REQUEST['participant_id'])
						log__admin("participant_edit","participant_id:".$participant['participant_id']);
					else log__admin("participant_create","participant_id:".$participant['participant_id']);
					$form=false;
					if($pindex == $nparticipants-1) redirect($GLOBALS['settings__admin_folder']."/participants_edit.php?participant_id=".$participant['participant_id']);
				} else {
					message($lang['database_error']);
				}
			}
		}
	}
	if(!empty($_REQUEST['submit_import_other'])) {
		if(empty($_REQUEST['other_id'])) message(lang('other_participant_id_not_entered_for_import'));
		else {
			$other_participant=orsee_db_load_array($maintable,$_REQUEST['other_id'],"participant_id");
			if($other_participant===false)  message(lang('other_participant_not_found_for_import'));
			elseif(!empty($participant_id))  {
				$me=orsee_db_load_array($maintable,$participant_id,"participant_id");
				$other_participations=participants__stat_laboratory($_REQUEST['other_id'],true,"participated");
				$my_participations=participants__stat_laboratory($participant_id,true,"registered");
				$n_imported=0; $newmessage=""; $newremarks="";
				foreach($other_participations as $nop=>$op)
				{
					if(!empty($op['payghost'])) continue;
					$already_imported=false; $doublon=false; $payghosted=false;
					// var_dump($my_participations); 
					if($my_participations!==false) foreach($my_participations as $mp) {
						if($mp['experiment_id']==$op['experiment_id'] && $mp['participated']=='y') {
							$doublon=true;
							if($mp['session_id']==$op['session_id'] && empty($mp['payghost'])) $already_imported=true;
						}
						if($mp['experiment_id']==$op['experiment_id'] && $mp['participated']=='n') {
							$payghosted=true;
						}
					}
					if($already_imported) $newmessage.="<br>".lang('experiment')." �".$op['experiment_name']."�, ".lang('session')." ".$op['session_start_day']."/".$op['session_start_month']."/".$op['session_start_year']." ".$op['session_start_hour'].":".str_pad($op['session_start_minute'], 2, "0", STR_PAD_LEFT)." ".lang('already_imported', false).".";
					else {
						if($payghosted) {
							$wheresup="payghost=0 AND "; //payment_num<>'' AND 
							$q0="SELECT MAX(payghost) as maxpayghost FROM ".table('participate_at')." WHERE participant_id='".$participant_id."' AND experiment_id='".$op['experiment_id']."'";
							$l0=orsee_query($q0);
							$newpayghost=$l0["maxpayghost"]+1;
							$whatelsetoset="payghost=$newpayghost";
							$q1="UPDATE ".table('participate_at')." SET $whatelsetoset WHERE $wheresup participant_id='$participant_id' AND experiment_id='".$op['experiment_id']."'";
							// echo($q1); message($q1); echo "<hr>";
							$done1=mysqli_query($GLOBALS['mysqli'],$q1) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));						
						}
						if($doublon) $newremarks.="\r\n".lang('duplicated_participation_found_in_experiment')." �".$op['experiment_name']."� ".lang('with_imported_participant',false)." ".$other_participant['fname']." ".$other_participant['lname'].", ".lang('session')." ".$op['session_start_day']."/".$op['session_start_month']."/".$op['session_start_year']." ".$op['session_start_hour'].":".str_pad($op['session_start_minute'], 2, "0", STR_PAD_LEFT);
						$q="INSERT INTO ".table("participate_at")." (participant_id,experiment_id,session_id,invited,registered,shownup,participated) VALUES ('".$participant_id."','".$op['experiment_id']."','".$op['session_id']."','".$op['invited']."','y','y','y')";
						if(!$doublon) {
							$result=mysqli_query($GLOBALS['mysqli'],$q) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$q);
							$n_imported++;
						}
						// var_dump($q);
					}
				}
				if($n_imported==0) message(lang('nothing_to_import').". ".$newmessage);
				else{
					$other_deleted=false;
					if($me['deleted']=='n' && $other_participant['deleted']=='n') {
						$other_deleted=true;
						$other_participant['deleted']='y';
						query_makecolumns(table($maintable),array("last_delete_time","last_delete_admin","can_undelete"),array("INT(20)","VARCHAR(20)","tinyint(1)"));
						$other_participant['last_delete_time']=time();
						$other_participant['last_delete_admin']=$expadmindata['adminname'];
						// var_dump($other_participant); exit;
					}
					message(lang('import_finished').". ".$n_imported." ".lang('lines_imported',false).".".$newmessage);
					if(!empty($me['remarks'])) $me['remarks'].="\r\n";
					$me['remarks'].=date("d/m/Y").": ".lang('imported_participation_data from_participant')." ".$other_participant['fname']." ".$other_participant['lname']." (email=".$other_participant['email'].", id=".$other_participant['participant_id'].")";
					$me['remarks'].=". ".$n_imported." ".lang('lines_imported',false).".";
					if($other_deleted) $me['remarks'].=" ".lang('imported_participant_unsubscribed').". ";
					$me['remarks'].=$newremarks;
					if(!empty($other_participant['remarks'])) $other_participant['remarks'].="\r\n";
					$other_participant['remarks'].=date("d/m/Y").": ".lang('exported_participation_data to_participant')." ".$me['fname']." ".$me['lname']." (email=".$me['email'].", id=".$me['participant_id'].")";
					if($other_deleted) {
						$other_participant['remarks'].=". ".lang('unsubscribed').".";
					}
					$new_other=array(); $new_me=array();
					$new_other["remarks"]=$other_participant['remarks'];
					$new_other["deleted"]=$other_participant['deleted'];
					if(!empty($other_participant['last_delete_time'])) $new_other["last_delete_time"]=$other_participant['last_delete_time'];
					if(!empty($other_participant['last_delete_admin'])) $new_other["last_delete_admin"]=$other_participant['last_delete_admin'];
					$new_me["remarks"]=$me['remarks'];
					orsee_db_save_array($new_me,$maintable,$me['participant_id'],"participant_id");
					orsee_db_save_array($new_other,$maintable,$other_participant['participant_id'],"participant_id");
					// var_dump($other_participant,$other_participations); exit;
				}
			}
		}
		if(!empty($participant_id)) redirect($GLOBALS['settings__admin_folder']."/participants_edit.php?participant_id=".$participant_id);
	}
	
	query_makecolumns(table($maintable),"twinto","TEXT NOT NULL");
    if ($participant_id && $continue) {
    	$_REQUEST=orsee_db_load_array($maintable,$participant_id,"participant_id");
	}

	$button_title = ($participant_id) ? $lang['save'] : $lang['add'];
	
	if(empty($participant_id)) {
		if(empty($_REQUEST['several'])) {
			$form_title=lang("create_participant");
			$form_title.=' (<a href="participants_edit.php?several=1">'.lang('create_several_participants', false).'</a>)';
		}
		else {
			$form_title=lang("create_participants");
			$form_title.='. '.lang('one_value_per_line').' '.lang('or_one_line_only_if_same_except_email',false).' (<a href="participants_edit.php">'.lang('create_just_one_participant',false).'</a>)';
		}
	}

	participant__show_form($_REQUEST,$button_title,$form_title,$errors__dataform,true,false,array(),!empty($_REQUEST['several']),$maintable);

	if ($participant_id) {

	echo '<CENTER><BR><BR>';

	if ((!isset($_REQUEST['deleted']) || $_REQUEST['deleted']=='n') && check_allow('participants_unsubscribe')) {

		echo '<FORM action="participants_delete.php">
			<INPUT type=hidden name="participant_id" value="'.$participant_id.'">
			<a href="'.$settings__root_url.'/'.$GLOBALS['settings__admin_folder'].'/checkdoublons.php?participant_id='.$participant_id.'">Trouver des doublons</a><br><br>
			<br><a target=_blank href="'.$settings__root_url.'/'.$GLOBALS['settings__public_folder'].'/participant_show.php?p='.url_cr_encode($participant_id).'">Participant\'s page (for test purposes)</a> &nbsp;&nbsp;&nbsp; / &nbsp;&nbsp;&nbsp; <a href="statistics_show_log.php?log=participant_actions&id='.$participant_id.'">'.lang("participant_actions").'</a><br><br><br>
			<TABLE>
				<TR>
					<TD>
						'.$lang['unsubscribe_participant'].'<BR>
					</TD>
				</TR>
				<TR>
					<TD align=center>
						<INPUT type=submit name=delete 
							value="'.$lang['unsubscribe'].'">
					</TD>
				</TR>
			</TABLE>
		      </FORM>';
		}
	elseif (isset($_REQUEST['deleted']) && $_REQUEST['deleted']=='y') {

		if (isset($_REQUEST['excluded']) && $_REQUEST['excluded']=="y")
			echo '<FONT color="red">'.$lang['was_excluded_by_experimenter'].'</FONT>';

		if (check_allow('participants_resubscribe')) {
		 echo '<FORM action="participants_resubscribe.php">
			<INPUT type=hidden name="participant_id" value="'.$participant_id.'">
			<a href="'.$settings__root_url.'/'.$GLOBALS['settings__admin_folder'].'/checkdoublons.php?participant_id='.$participant_id.'">Trouver des doublons</a><br><br>
			<br><a target=_blank href="'.$settings__root_url.'/'.$GLOBALS['settings__public_folder'].'/participant_show.php?p='.url_cr_encode($participant_id).'">Participant\'s page (for test purposes)</a><br><br><br>
			<TABLE>
				<TR>
					<TD>
						'.$lang['resubscribe_participant'].'<BR>
					</TD>
				</TR>
				<TR>
					<TD align=center>
						<INPUT type=submit name=resubscribe 
							value="'.$lang['resubscribe'].'">
					</TD>
				</TR>
			</TABLE>
		      </FORM>';
			if(check_allow('subjectpool_delete')) {
				echo '<FORM action="participants_delete_from_base.php">
				<INPUT type=hidden name="participant_id" value="'.$participant_id.'">
				<TABLE>
					<TR>
						<TD align=center>
							<button type=submit class="btn btn-danger" name=delete_from_base >
								'.lang('delete_from_base').'</button>
						</TD>
					</TR>
				</TABLE>
				  </FORM>';
			}
		}
	}

	echo '</CENTER>';

	}

	echo '<CENTER>';

	participants__get_statistics($participant_id);
	
	if(!empty($participant_id)) echo '<br> <FORM action="'.thisdoc().'" name=import_other_participant method="POST">
	<INPUT type="hidden" name="participant_id" value="'.$participant_id.'"/>
	'.lang('import_participation_data_from_another_participant').' ('.lang('enter id',false).'): <input type=text name=other_id /> <input type=submit name=submit_import_other value="'.lang('import').'" />
	';


	echo "</CENTER>";

include ("footer.php");
?>

<?php
// part of orsee. see orsee.org
ob_start();

$menu__area="experiments_new";
$title="edit experiment";
include ("header.php");
              
echo '<center><h4>'.$lang['edit_experiment'].'</h4></center>';


	if (isset($_REQUEST['experiment_id']) && $_REQUEST['experiment_id']) {
		$allow=check_allow('experiment_edit','experiment_show.php?experiment_id='.$_REQUEST['experiment_id']);
      		$edit=orsee_db_load_array("experiments",$_REQUEST['experiment_id'],"experiment_id");
		$edit['experiment_show_type']=$edit['experiment_type'].','.$edit['experiment_ext_type'];
		if (!check_allow('experiment_restriction_override')) 
			check_experiment_allowed($edit,$GLOBALS['settings__admin_folder']."/experiment_show.php?experiment_id=".$edit['experiment_id']);
		}
	   else {
		$allow=check_allow('experiment_edit','experiment_main.php');
		}

	$continue=true;

	if (isset($_REQUEST['edit']) && $_REQUEST['edit']) {
 
		if (isset($_REQUEST['experimenter_list'])) {
			$texperimenter=array();
			foreach ($_REQUEST['experimenter_list'] as $key=>$value) {
				if ($value) $texperimenter[]=$value;
				}
			$_REQUEST['experimenter']=implode(",",$texperimenter);
			}
		else $_REQUEST['experimenter']="";

		if (isset($_REQUEST['experimenter_mail_list'])) {
                        $texperimenter_mail=array();
                        foreach ($_REQUEST['experimenter_mail_list'] as $key=>$value) {
                                if ($key) $texperimenter_mail[]=$value;
                                }
                        $_REQUEST['experimenter_mail']=implode(",",$texperimenter_mail);
                        }
                else $_REQUEST['experimenter_mail']="";
				
		if (!isset($_REQUEST['experiment_class'])) $_REQUEST['experiment_class']="0";


  		if (!$_REQUEST['experiment_public_name']) {
  			message($lang['error_you_have_to_give_public_name']);
  			$continue=false;
			}

                if (!$_REQUEST['experiment_name']) {
                        message($lang['error_you_have_to_give_internal_name']);
                        $continue=false;
                        }

		// if (!preg_match("/^[^@ \t\r\n]+@[-_0-9a-zA-Z]+\.[^@ \t\r\n]+$/",$_REQUEST['sender_mail'])) {
		if (!preg_match("/^(([().!#$%&'*+\/=?^_`{|}~\\x00-\\x7E\w-]+[,\n#\s$&:\n\t]+){2,}){0,}(?:<){0,1}[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9]))(?:>){0,1}$/",$_REQUEST['sender_mail'])) {
			message($lang['error_no_valid_sender_mail']);
        		$continue=false;
			}

		if (!$_REQUEST['experimenter']) {
			message($lang['error_at_least_one_experimenter_required']);
			$continue=false;
			}

		if (!$_REQUEST['experimenter_mail']) {
                        message($lang['error_at_least_one_experimenter_mail_required']);
                        $continue=false;
                        }

		if (empty($_REQUEST['experiment_show_type'])) {
                        message(lang('you_have_to select_the experiment_type '));
                        $continue=false;
        }


  		if ($continue) {

        	if (!isset($_REQUEST['experiment_finished']) ||!$_REQUEST['experiment_finished']) $_REQUEST['experiment_finished']="n";

			if (!isset($_REQUEST['hide_in_stats']) ||!$_REQUEST['hide_in_stats']) $_REQUEST['hide_in_stats']="n";

			if (!isset($_REQUEST['hide_in_cal']) || !$_REQUEST['hide_in_cal']) $_REQUEST['hide_in_cal']="n";
			
			if (!isset($_REQUEST['gender_equality']) ||!$_REQUEST['gender_equality']) $_REQUEST['gender_equality']="n";
			if (!isset($_REQUEST['by_couples']) ||!$_REQUEST['by_couples']) $_REQUEST['by_couples']="n";

			if (!isset($_REQUEST['access_restricted']) ||!$_REQUEST['access_restricted']) $_REQUEST['access_restricted']="n";

			$exptypes=explode(",",$_REQUEST['experiment_show_type']);
			$_REQUEST['experiment_type']=$exptypes[0];
			$_REQUEST['experiment_ext_type']=$exptypes[1];

   			$edit=$_REQUEST; 
			// var_dump($edit); exit;

   			$done=orsee_db_save_array($edit,"experiments",$edit['experiment_id'],"experiment_id");
   	
			if ($done) {
       				message ($lang['changes_saved']);
					// exit;
				redirect ($GLOBALS['settings__admin_folder']."/experiment_edit.php?experiment_id=".$edit['experiment_id']);
				}
			   else {
   				message ($lang['database_error']);
				redirect ($GLOBALS['settings__admin_folder']."/experiment_edit.php?experiment_id=".$edit['experiment_id']);
   				}		

			}
			

		$edit=$_REQUEST;

		}
		if(!$continue && empty($_REQUEST["redirected"])) {
			unset($edit["experiment_id"]);
			// $edit["experiment_id"]="";
			$edit["redirected"]="true";
			$edit["addit"]="true";
			redirect($GLOBALS['settings__admin_folder']."/experiment_edit.php?".http_build_query($edit));
		}


	// form

	// initialize if empty
	if (!isset($edit)) $edit=array();
	$formvarnames=array('experiment_name','experiment_public_name','experiment_description','experiment_link_to_paper','experiment_class','experiment_id','sender_mail','experiment_show_type','access_restricted','experiment_finished','hide_in_stats','hide_in_cal','gender_equality','by_couples','confirm_mail_supinfo','reminder_mail_supinfo','confirm_message_supinfo','add_step_subscr_info','suppl_exp_description','by_couples_link','by_couples_exptype','supquest','supquest_qc');
	foreach ($formvarnames as $fvn) {
		if (!isset($edit[$fvn])) $edit[$fvn]="";
	}

	echo '<CENTER>';

	if(empty($_REQUEST["redirected"])) show_message();
	// var_dump($edit);


	if (!isset($edit['experiment_id']) || !$edit['experiment_id']) {
           $gibtsschon=true;
           srand ((double)microtime()*1000000);
           while ($gibtsschon) {
                $crypt_id = "/";
		while (preg_match("/(\/|\.)/",$crypt_id)) {
                        $exp_id = rand();
                        $crypt_id=unix_crypt($exp_id);
                        }

                $query="SELECT experiment_id FROM ".table('experiments')."
                        WHERE experiment_id=".$exp_id;
                $line=orsee_query($query);
                if (isset($line['experiment_id'])) $gibtsschon=true; else $gibtsschon=false;
                }
		$edit['experiment_id']=$exp_id;
    }

	echo '<FORM action="experiment_edit.php">
		<INPUT type=hidden name=experiment_id value="'.$edit['experiment_id'].'">
		<TABLE>';

	echo '		<TR>
				<TD>
					'.$lang['id'].'
				</TD>
				<TD>
					'.$edit['experiment_id'].'
				</TD>
			</TR>';

	echo '		<TR>
				<TD>
					'.$lang['internal_name'].':
				</TD>
				<TD>
					<INPUT name=experiment_name type=text size=40 maxlength=100 value="'.stripslashes($edit['experiment_name']).'"> 
					'.help("experiment_name").'
				</TD>
			</TR>';


	echo '		<TR>
				<TD>
					'.$lang['public_name'].':
				</TD>
				<TD>
					<INPUT name=experiment_public_name type=text size=40 maxlength=100 
					value="'.stripslashes($edit['experiment_public_name']).'">
					'.help("experiment_public_name").'
				</TD>
			</TR>';


	echo '		<TR>
				<TD>
					'.$lang['description'].':
				</TD>
				<TD>
					<textarea name=experiment_description rows=5 cols=30 
					wrap=virtual>'.stripslashes($edit['experiment_description']).'</textarea> 
					'.help("experiment_description").'
				</TD>
			</TR>';


	echo '		<TR>
				<TD>
					'.lang('experiment_type',true,true).':
				</TD>
				<TD>
					<SELECT name=experiment_show_type>';

				$experiment_types=array();
				$experiment_internal_types=$system__experiment_types;
				foreach ($experiment_internal_types as $inttype) {
					$expexttypes=load_external_experiment_types($inttype);
		if(empty($edit['experiment_show_type']) && count($expexttypes)>1) echo '<OPTION value="" SELECTED">-</OPTION>';	
					foreach ($expexttypes as $exttype) {
						$value=$inttype.','.$exttype;
						echo '<OPTION value="'.$value.'"';
                                        	   if ($value==$edit['experiment_show_type']) echo ' SELECTED';
                                        		echo '>'.$lang[$inttype].' ('.$exttype.')</OPTION>
                                                ';
						}
					}

			echo '		</SELECT> 
					'.help("experiment_type").'
				</TD>
			</TR>';

	 echo '          <TR>
                                <TD>
                                        '.$lang['class'].':
                                </TD>
                                <TD>
					'; 
					// experiment__experiment_class_select_field('experiment_class', $edit['experiment_class']);
					experiment__experiment_class_checkbox_list('experiment_class', $edit['experiment_class'],'-');
					echo '
                                </TD>
                        </TR>';

	echo '		<TR>
				<TD valign="top">
					'.$lang['experimenter'].':<BR>'.help("experimenter").'
				</TD>
				<TD>';
					if (!isset($_REQUEST['experiment_id']) || !$_REQUEST['experiment_id']) $edit['experimenter']=$expadmindata['adminname'];
					experiment__experimenters_checkbox_list("experimenter_list",$edit['experimenter']);
	echo '			</TD>
			</TR>';

	if ($settings['allow_experiment_restriction']=='y') {
	echo '          <TR>
                                <TD valign="top">
                                        '.$lang['experiment_access_restricted'].':
                                </TD>
                                <TD>
					<INPUT name="access_restricted" type=checkbox value="y"';
                                        if ($edit['access_restricted']=="y" || $edit['access_restricted']=="") echo " CHECKED";
                                        echo '>
                                        '.help("experiment_access_restricted").'
        			</TD>
                        </TR>';
		}

	echo '		<TR>
				<TD>
					'.$lang['get_emails'].':<BR>'.help("experimenters_email").'
				</TD>
				<TD>';
					if (!isset($_REQUEST['experiment_id']) || !$_REQUEST['experiment_id']) $edit['experimenter_mail']=$expadmindata['adminname'];
					experiment__experimenters_checkbox_list("experimenter_mail_list",$edit['experimenter_mail']);
	echo '			</TD>
			</TR>';

	echo '		<TR>
				<TD>
					'.$lang['email_sender_address'].':
				</TD>
				<TD>
					<INPUT name=sender_mail type=text size=40 maxlength=60
					value="';
					if ($edit['sender_mail']) echo stripslashes($edit['sender_mail']);
						else {
							if(!empty($settings['sender_name'])) echo $settings['sender_name']." <";
							echo $settings['support_mail'];
							if(!empty($settings['sender_name']))  echo ">";
						}
					echo '"> 
					'.help("email_sender_address").'
				</TD>
			</TR>';


	echo '		<TR>
				<TD>
					'.$lang['experiment_finished?'].'
				</TD>
				<TD>
					<INPUT name=experiment_finished type=checkbox value="y"';
					if ($edit['experiment_finished']=="y") echo " CHECKED";
					echo '> 
					'.help("experiment_finished").'
				</TD>
			</TR>';

	echo '		<TR>
				<TD>
					'.$lang['hide_in_stats?'].'
				</TD>
				<TD>
					<INPUT name=hide_in_stats type=checkbox value="y"'; 
					if ($edit['hide_in_stats']=="y") echo " CHECKED";
					echo '> 
					'.help("experiment_hide_in_stats").'
				</TD>
			</TR>';

	echo '		<TR>
				<TD>
					'.$lang['hide_in_cal?'].'
				</TD>
				<TD>
					<INPUT name=hide_in_cal type=checkbox value="y"';
					if ($edit['hide_in_cal']=="y" || $edit['hide_in_cal']=="") echo " CHECKED";
					echo '> 
					'.help("experiment_hide_in_cal").'
				</TD>
			</TR>';
			
	query_makecolumns(table('experiments'),"gender_equality","VARCHAR(1) NOT NULL");
	echo '		<TR>
				<TD>
					'.lang('gender_equality?').'
				</TD>
				<TD>
					<INPUT name=gender_equality type=checkbox value="y"';
					if (!empty($edit['gender_equality']) && $edit['gender_equality']!="n") echo " CHECKED";
					echo '> 
					'.help("gender_equality").'
				</TD>
			</TR>';
	query_makecolumns(table('experiments'),"by_couples,by_couples_link,by_couples_exptype","VARCHAR(1),VARCHAR(250),VARCHAR(16)","'n',,");
	echo '		<TR>
				<TD>
					'.lang('by_couples ?').'
				</TD>
				<TD>
					<INPUT name=by_couples type=checkbox value="y" onchange="document.getElementById(\'by_couples_link_tr\').style.display=(this.checked)?\'table-row\':\'none\'"';
					if (!empty($edit['by_couples']) && $edit['by_couples']!="n") echo " CHECKED";
					echo '> '.lang("(attention, this_will_force_participants_to_give_partner's_email_when_registering_to_a_session").')
					'.help("by_couples").'
				</TD>
			</TR>';
	echo '		<TR id=by_couples_link_tr style="display:'.((!empty($edit['by_couples']) && $edit['by_couples']!="n")?'table-row':'none').'">
				<TD style="vertical-align:top">
					<em>'.lang('subscribe_link_root').'</em>
				</TD>
				<TD>
					<INPUT name=by_couples_link type=text value="'.$edit['by_couples_link'].'"';
					echo '> ('.lang('relative_to',false).' <strong>'.$settings__root_url.'/</strong>, '.lang('default_is',false).' <em>'.$GLOBALS['settings__public_folder'].'</em>)
					'.help("by_couples_link");
					echo '<br><br><em>'.lang('limit_to_the_experiment_type(s)').' ('.lang('the_first_one_is_the_principal_type',false).')'.'</em> : ';
					echo '<INPUT name=by_couples_exptype type=text size=20 maxlength=60
					value="';
					if ($edit['by_couples_exptype']) echo stripslashes($edit['by_couples_exptype']);
					echo '"> <SELECT name=by_couples_exptype_helper onchange="by_couples_exptype_helper_func(this)">';

				$experiment_types=array();
				$experiment_internal_types=$system__experiment_types;
				// foreach ($experiment_internal_types as $inttype) {
					$inttype=""; $expexttypes=load_external_experiment_types($inttype);
					echo '<OPTION value="" SELECTED">-</OPTION>';	
					foreach ($expexttypes as $exttype) {
						$value=empty($inttype)?$exttype:$inttype.','.$exttype;
						echo '<OPTION value="'.$value.'"';
                                        	   if ($value==$edit['by_couples_exptype']) echo ' SELECTED';
                                        		echo '>';
												if(!empty($inttype)) echo $lang[$inttype].' (';
												echo $exttype;
												if(!empty($inttype)) echo ')';
												echo '</OPTION>
                                                ';
					
					}
				// }

			echo '		</SELECT> 
					'.help("experiment_type").'';
			echo '<script>
				function by_couples_exptype_helper_func(s) {
					var v = s.value;
					var cvals=document.forms[0]["by_couples_exptype"].value;
					var acvals=(cvals.replace(/\s*/g,"")=="")?[]:cvals.split(",");
					if(v!="" && acvals.indexOf(v)<0) acvals.push(v);
					document.forms[0]["by_couples_exptype"].value = acvals.join(",");
					s.value="";
					
				}
			</script>';
					echo '<hr>
				</TD>
			</TR>';
	query_makecolumns(table('experiments'),"supquest,supquest_qc","VARCHAR(1),VARCHAR(4086)","'n',");
	echo '		<TR>
				<TD>
					'.lang('selection_questionnaire').'
				</TD>
				<TD>
					<INPUT name=supquest type=checkbox value="y" onchange="document.getElementById(\'supquest_qc_tr\').style.display=(this.checked)?\'table-row\':\'none\'"';
					if (!empty($edit['supquest']) && $edit['supquest']!="n") echo " CHECKED";
					echo '>
					'.help("supquest").'
				</TD>
			</TR>';
	echo '		<TR id=supquest_qc_tr style="display:'.((!empty($edit['supquest']) && $edit['supquest']!="n")?'table-row':'none').'">
				<TD style="vertical-align:top">
					<em>'.lang('questions_criteria').'</em>
				</TD>
				<TD>
					<textarea name=supquest_qc cols=80 rows=3>'.stripslashes($edit['supquest_qc']).'</textarea>
					'.help("supquest_qc");
					echo ' <br><em>first line questions, second line criterea; for brackets in text use &amp;#40; &amp;#41;, dont use functions in criteria and questions (operators are allowed).</em><hr>';
				echo '
				</TD>
			</TR>';

		query_makecolumns(table('experiments'),"payment_file","INT(20) NOT NULL default 0");
		echo '		<TR>
				<TD>
					'.lang('assign_payment_file').' (<small><a target=_blank href="dossiers_paiement_main.php">'.lang('manage_payment_files').'</a></small>)
				</TD>
				<TD>
					';
					echo helpers__select_payment_file($edit['experiment_id'],'','payment_file');
					echo '
				</TD>
			</TR>';

	echo '		<TR>
				<TD>
					'.$lang['experiment_link_to_paper'].':
				</TD>
				<TD>
					<INPUT name=experiment_link_to_paper type=text size=40 maxlength=200
					value="'.stripslashes($edit['experiment_link_to_paper']).'">
					'.help("link_to_paper").'
				</TD>
			</TR>';
	query_makecolumns(table('experiments'),"confirm_mail_supinfo","TEXT NOT NULL");
	query_makecolumns(table('experiments'),"confirm_message_supinfo","TEXT NOT NULL");
	// var_dump($edit);
	echo '		<TR>
			<TD>
				'.lang('confirm_mail_suppl_text').'
			</TD>
			<TD>
			<table cellpadding=5 border=0 style="text-align:center;vertical-align:bottom"><tr><td>
				<textarea name=confirm_mail_supinfo cols=40 rows=4>'.stripslashes($edit['confirm_mail_supinfo']).'</textarea>
				</td>
			<td>'.lang('confirm_webmessage_suppl_text').'<br>
				<textarea name=confirm_message_supinfo cols=40 rows=3>'.stripslashes($edit['confirm_message_supinfo']).'</textarea>
				</td></tr></table>
			</TD>
		</TR>';

	query_makecolumns(table('experiments'),"reminder_mail_supinfo","TEXT NOT NULL");
	echo '		<TR>
			<TD>
				'.lang('reminder_mail_suppl_text').'
			</TD>
			<TD>
				<textarea name=reminder_mail_supinfo cols=40 rows=3>'.(empty($edit['reminder_mail_supinfo'])?lang('the_appointment_is_5_minutes_before_the_experiment'):stripslashes($edit['reminder_mail_supinfo'])).'</textarea>
				</TD>
		</TR>';
	query_makecolumns(table('experiments'),"add_step_subscr_info","TEXT NOT NULL");
	echo '		<TR>
			<TD>
				'.lang('additional_subscription_step_text').'
			</TD>
			<TD>
				<textarea name=add_step_subscr_info cols=40 rows=4>'.stripslashes($edit['add_step_subscr_info']).'</textarea>
				</TD>
		</TR>';

	query_makecolumns(table('experiments'),"suppl_exp_description","TEXT NOT NULL");
	echo '		<TR>
			<TD>
				<br>'.lang('supplementary_experiment_description_for_participants').'
			</TD>
			<TD><br>
				<textarea name=suppl_exp_description cols=80 rows=4>'.stripslashes($edit['suppl_exp_description']).'</textarea>
				</TD>
		</TR>';



	echo '		<TR>
				<TD COLSPAN=2 align=center>
					<INPUT name=edit type=submit 
					value="';
					if (!isset($_REQUEST['experiment_id']) || empty($_REQUEST['experiment_id'])) echo $lang['add'];
						else echo $lang['change'];
					echo '">
				</TD>
			</TR>';


	echo '	</TABLE>
		</FORM>
		<BR>';

	if (isset($_REQUEST['experiment_id']) && $_REQUEST['experiment_id'] && check_allow('experiment_delete')) {
		echo '	<FORM action="experiment_delete.php">
			<INPUT type=hidden name="experiment_id" value="'.$edit['experiment_id'].'">
			<table>
				<TR>
					<TD>
						<INPUT type=submit name=submit value="'.$lang['delete'].'">
					<TD>
				</TR>
			</table>
			</FORM>';
		}

	if (isset($_REQUEST['experiment_id']) && $_REQUEST['experiment_id']) 
		echo '	<BR><BR>
			<A HREF="experiment_show.php?experiment_id='.$_REQUEST['experiment_id'].'">'
			.$lang['mainpage_of_this_experiment'].'</A>';
	
	echo '</center>';


include ("footer.php");
?>

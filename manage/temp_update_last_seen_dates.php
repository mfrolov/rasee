<?php
// part of orsee. see orsee.org
ob_start();

$menu__area="participants";
$title="assign participants";

include ("header.php");

query_makecolumns(table('participants'),array("last_visit_time","last_visit_page"),array("int(20)","text"),array("0","null"));
query_makecolumns(table('participants_temp'),array("last_visit_time","last_visit_page"),array("int(20)","text"),array("0","null"));

$q="SELECT * FROM ".table('participants_log')." WHERE action NOT IN ('subscribe', 'confirm') ORDER BY timestamp DESC";
$lines=orsee_query($q,"return_same");
$parts=[]; $ids=[]; $parts_id=[];
foreach($lines as $line) {
	if(is_numeric($line["id"]) && !in_array($line["id"], $ids)) {
		$parts[]=$line;
		$ids[]=$line["id"];
		$parts_id[$line["id"]]=$line;
	}
}

$headers=array_keys($parts[0]);
unset($headers[array_search('target',$headers)]);

$participants=[];
foreach($parts as $line) {
	$q="SELECT * FROM ".table('participants')." WHERE participant_id=".$line['id']." AND last_visit_time=0";
	$p=orsee_query($q);
	if($p!==false) {
		$qu="UPDATE ".table('participants')." SET last_visit_time='".$line['timestamp']."',last_visit_page='".$line['action']."' WHERE participant_id=".$line['id']." AND last_visit_time=0";
		orsee_query($qu);
		$participants[]=$p;
	}
}
if(count($participants)>0) $headers=array_merge($headers,array_keys($participants[0]));

echo "<table class='table table-stripped' border=1>";
echo "<tr>";
echo "<th>".lang('edit')."</th>";
foreach($headers as $h) { echo "<th>".$h."</th>";}
echo "</tr>";
foreach($participants as $line) {
	echo '<tr>';
	echo '<td>'.'<a target=_blank href="participants_edit.php?participant_id='.$line['participant_id'].'">'.lang('edit').'</a>'.'</td>';
	foreach($headers as $h) {
		$cpid=$line['participant_id'];
		if(array_key_exists($h,$line)) echo "<td>".$line[$h]."</td>";
		elseif(!empty($parts_id[$cpid]) && array_key_exists($h,$parts_id[$cpid]))  echo "<td>".$parts_id[$cpid][$h]."</td>";
	}
	echo '</tr>';
}

echo "</table>";
echo "<p>".lang("participants_in_log").": ".count($ids)."."."</p>";
echo "<p>".lang("participants_updated").": ".count($participants)."."."</p>";
// var_dump(count($parts),count($ids),count($participants));
include ("footer.php");

?>

<?php
// part of orsee. see orsee.org
ob_start();

$title="login";

include("header.php");


	echo '<center>
		<BR><BR>';

	if (isset($_REQUEST['logout']) && $_REQUEST['logout']) message($lang['logout']);

	if (isset($_REQUEST['pw']) && $_REQUEST['pw']) {
		message($lang['logout']);
		message ($lang['password_changed_log_in_again']);
		}

	show_message();
	
	$php_gt533=version_compare(phpversion(), '5.3.3', '>=');
	require_once '../jcryption/include/sqAES.php';
	require_once '../jcryption/include/JCryption.php';
	if(!$php_gt533) include_once '../jcryption/include/Crypt/AES.php';
	if(!empty($_POST) && !isset($_POST[JCryption::POST_KEY])) exit(lang("an_error_is_occured or_you_are_not_authorized please_contact_the_site_support"));
	$postBefore = $_POST;
	if(!empty($_POST)) {
		JCryption::decrypt();
		$jcdebug=false;
		if($jcdebug) {
			print_r($postBefore);
			print_r($_POST);
			exit;
		}
	}
	
	echo '
		<script type="text/javascript">
			var jCryptionLocation="../jcryption/";
		</script>
		<script type="text/javascript" src="../jcryption/jquery.jcryption.3.1.0.js"></script>
		<script type="text/javascript">
			$(function() {
				$("form").jCryption();
			});
		</script>';
		



	echo '	<H4>'.$lang['admin_login_page'].'</H4>
		';
	if (isset($_GET['adminname']) && isset($_GET['password'])) {
		// redirect($GLOBALS['settings__admin_folder']."/");
		exit(lang("an_error_is_occured or_you_are_not_authorized"));
	}
	if (isset($_REQUEST['adminname']) && isset($_REQUEST['password'])) {
		$login_failure="login_failure";
		
		$securimage="";
		if(file_exists("../../securimage/securimage.php")) $securimage="../../securimage/securimage.php";
		if(file_exists("../securimage/securimage.php")) $securimage="../securimage/securimage.php";
		
		if(!empty($securimage)) {
			$audiopath=str_replace("securimage.php","audio",$securimage);
			require_once $securimage;
			$securimage = new Securimage();
			$ask_captcha=false;
			$logs=orsee_db_load_full_array('admin_log',$login_failure,'action');
			$ctime=time();
			$iaddr=get_client_ip();
			foreach($logs as $log) {
				$ctimediff=$ctime-$log['timestamp'];
				if(isset($log["ipaddr"]) && $log["ipaddr"]==$iaddr && $ctimediff<600) $ask_captcha=true;
			}
			// var_dump(get_client_ip(),$ask_captcha,$logs,$ctimediff); exit;
			if($ask_captcha) {
				echo "<h5>".lang('please_copy_the_image_code_to_continue')."</h5>";
				if(!empty($_REQUEST['rasee_captcha'])) {
					$_CURRENT_SESSION=$_SESSION;
					$_SESSION=$_DEFAULT_SESSION;
					if($securimage->check($_REQUEST['rasee_captcha'])) $ask_captcha=false;
					else echo "<h4>".lang('incorrect_code').', '.lang('please_retry',false)."</h4>";
					$_SESSION=$_CURRENT_SESSION;
				}
				if($ask_captcha) {
					$options = array();
					$options['input_name'] = 'rasee_captcha'; // change name of input element for form post
					$options['disable_flash_fallback'] = false; // allow flash fallback
					$options['input_text'] = lang("type_the_text").' : ';
					$options['show_text_input']=true;
					$options['show_audio_button']=file_exists($audiopath);

					$action=thisdoc();
					// var_dump($_REQUEST['rasee_captcha'],$securimage->check($_REQUEST['rasee_captcha']),$_SESSION,$_DEFAULT_SESSION);
					echo '<FORM method=POST action='.$action.'>';
					foreach($_REQUEST as $rk=>$rv) {
						if(!is_array($rv)) echo '<INPUT type=hidden name="'.$rk.'" value="'.$rv.'">'.PHP_EOL;
						else foreach($rv as $rvk=>$rvv) echo '<INPUT type=hidden name="'.$rk.'['.$rvk.']" value="'.$rvv.'">'.PHP_EOL;
					}
					echo "<table><tr><td><div style='white-space:nowrap' id='captcha_container_1'>";
					echo Securimage::getCaptchaHtml($options);
					echo "</div></td></tr></table>\n";
					echo "
					<p>
					<br>
					<input type='submit' value='".lang('submit')."'>
					</p>
					</FORM>";	
					// var_dump($_REQUEST);					
					exit;
				}
			}
		}
		$password=password_hash($_REQUEST['password'],PASSWORD_BCRYPT); //unix_crypt($_REQUEST['password']);
		$logged_in=admin__check_login($_REQUEST['adminname'],$_REQUEST['password']);
		if ($logged_in) {
			$expadmindata['admin_id']=$_SESSION['expadmindata']['admin_id'];
			log__admin("login");
			if (!empty($_REQUEST['redirect'])) redirect($_REQUEST['redirect']);
			elseif(!empty($_GET['document']) && preg_match("/^[a-z_]+\.[a-z]+$/",$_GET['document'])) {
				$redirectto=$GLOBALS['settings__admin_folder']."/".$_GET['document'];
				$other_vars=$_GET; unset($other_vars['document']);
				if(!empty($other_vars)) $redirectto.='?'.http_build_query($other_vars);
				redirect($redirectto);
			}
			else redirect($GLOBALS['settings__admin_folder']."/index.php");
		}
		else {
			$usedadmname=preg_replace("/\W/"," ",$_REQUEST['adminname']);
			$usedadmname=preg_replace("/^\s+|(?:\s)\s/","",$usedadmname);
			$usedadmname=substr($usedadmname,0,25);
			log__admin($login_failure,$usedadmname,get_client_ip());
			message($lang['error_password_or_username']);
			$supdata="";
			if(!empty($_REQUEST['document']) && preg_match("/^[a-z_]+\.[a-z]+$/",$_REQUEST['document'])) $supdata='?'.http_build_query($_REQUEST);
			if(!empty($_REQUEST['redirect'])) {
				$prev_redirect=urldecode($_REQUEST['redirect']);
				$prevurl=parse_url($prev_redirect);
				$prevdoc=basename($prevurl['path']);
				if(preg_match("/^[a-z_]+\.[a-z]+$/",$prevdoc)) {
					$supdata='?document='.$prevdoc;
					if(!empty($prevurl['query'])) $supdata.='&'.$prevurl['query'];
				}
				// var_dump(parse_url($prev_redirect),$_REQUEST['redirect']); exit;
			}
			redirect($GLOBALS['settings__admin_folder']."/admin_login.php".$supdata);
		}
	}

	check_options_exist();

	if(!empty($expadmindata['adminname'])) {
		if (!empty($_REQUEST['redirect'])) redirect($_REQUEST['redirect']);
		elseif(!empty($_GET['document']) && preg_match("/^[a-z_]+\.[a-z]+$/",$_GET['document'])) {
			$redirectto=$GLOBALS['settings__admin_folder']."/".$_GET['document'];
			$other_vars=$_GET; unset($other_vars['document']);
			if(!empty($other_vars)) $redirectto.='?'.http_build_query($other_vars);
			redirect($redirectto);
		}
		else redirect($GLOBALS['settings__admin_folder']."/index.php");
	}
	admin__login_form();

	echo '</center>';

include("footer.php");

?>

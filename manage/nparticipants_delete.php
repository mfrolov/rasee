<?php
ob_start();

$title="delete participants";
include ("header.php");

	/*if (isset($_REQUEST['participant_id']) && $_REQUEST['participant_id']) 
			$participant_id=$_REQUEST['participant_id'];
                else redirect ($GLOBALS['settings__admin_folder']."/");
				*/
/*
        if (isset($_REQUEST['betternot']) && $_REQUEST['betternot'])
                redirect ($GLOBALS['settings__admin_folder'].'/participants_edit.php?participant_id='.$participant_id);
*/
        if (isset($_REQUEST['reallydelete']) && $_REQUEST['reallydelete']) $reallydelete=true;
                        else $reallydelete=false;

        if (isset($_REQUEST['reallyexclude']) && $_REQUEST['reallyexclude']) $reallyexclude=true;
                        else $reallyexclude=false;

        if (isset($_REQUEST['reallyreactivate']) && $_REQUEST['reallyreactivate']) $reallyreactivate=true;
                        else $reallyreactivate=false;
						
        if (isset($_REQUEST['reallyaddremarksonly']) && $_REQUEST['reallyaddremarksonly']) $reallyaddremarksonly=true;
                        else $reallyaddremarksonly=false;

	$allow=check_allow('participants_unsubscribe','participants_main.php');
	
		echo '<BR><BR>
		<center>
			<h4>'.$lang['delete_participant_data'].'</h4>
		</center>';
		
	if (isset($_REQUEST['participantstodelete']) && $_REQUEST['participantstodelete']) 
	{
			$pstdt=$_REQUEST['participantstodelete'];
			$pstdt=trim($pstdt);
			$pstdt=str_replace("\r","",$pstdt);
			// $pstdt=str_replace(" ","",$pstdt);
			$pstdt=str_replace("\n",",",$pstdt);
			
	$pstd=explode(",",$pstdt);
	$pscount=count($pstd);
$pscount2=0;
for($i=0;$i<$pscount;$i++)
{
$clinetosplit=preg_replace("/(\d{1,2}\/\d{1,2}\/\d{2,4})[ ]+(\d{2}:\d{2}(:\d{2})*)/","$1($2)",trim($pstd[$i])); //(^|[\t])([\t]|$)
// echo $clinetosplit,"<br>";
$cline=preg_split("/[\s]+/",$clinetosplit);
$cpinput=$cline[0];
$ismail=(strpos($cpinput,"@")!==false && strpos($cpinput,"@")>0);
$whattolook="participant_id";
if($ismail) $whattolook="email";
$cpartquery="SELECT * FROM ".table('participants')." WHERE `$whattolook`='".$cpinput."'";
$cpartresult=mysqli_query($GLOBALS['mysqli'],$cpartquery)
					or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
// echo "<br>" . date('h:i:s') . ' and '. microtime() . "--";
// usleep(100000);
// echo date('h:i:s') . ' and '. microtime();
$cpartnumrows=mysqli_num_rows($cpartresult);
if($cpartresult===false || $cpartnumrows == 0)
{
	echo "<br>".$whattolook." <u>".$cpinput."</u> not found.";
	$pscount2++;
	continue;
}
$nnpi=0;

while ($cpartrow = mysqli_fetch_assoc($cpartresult))
{
$nnpi++;
$participantx=$cpartrow["participant_id"];
$fname=$cpartrow["fname"];
$lname=$cpartrow["lname"];
$email=$cpartrow["email"];
$nregistr=$cpartrow["number_reg"];
$nabsences=$cpartrow["number_noshowup"];
$deleted=$cpartrow["deleted"];
$excluded=$cpartrow["excluded"];
$remarks=$cpartrow["remarks"];
$remarkstoadd=!empty($_REQUEST['addtoremarks']);
$remarksOK=true;
$npi=$pscount2+1;
if($cpartnumrows>1) $npi=$npi.'.'.$nnpi;
if ($participantx!='') {

		$supecho='';
		if(!empty($cpartrow['last_visit_time'])) $supecho.=lang("last_visit").': '.'<em>'.(empty($cpartrow['last_visit_time'])?lang('unknown',false):time__format($lang['lang'],'',false,false,true,false,$cpartrow['last_visit_time'])).'</em>';
		if(!empty($cpartrow['last_visit_page'])) $supecho.='. <em>('.$cpartrow['last_visit_page'].')</em> ';
	
		if($reallydelete || $reallyexclude || $reallyreactivate) query_makecolumns(table('participants'),array("last_delete_time","last_delete_admin","can_undelete"),array("INT(20)","VARCHAR(20)","tinyint(1)"));


        if ($reallydelete) {
		
		if($deleted!="y") {

			$canund="0";
			if(!empty($_REQUEST['can_undelete'])) $canund="1";
                $query="UPDATE ".table('participants')."
                        SET deleted='y', last_delete_time=UNIX_TIMESTAMP(), last_delete_admin='".$expadmindata['adminname']."', can_undelete=$canund
			WHERE participant_id='".$participantx."'";
                $result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
				
		if ($result!== false) {
		echo '<br>Participant '.$npi.' de '.$pscount.' '.$fname.' '.$lname.' (absences: '.$nabsences.' de '.$nregistr.') <u>'.$email.'</u> id <u>'.$participantx.'</u> désabonné. '.$supecho.' <a target=_blank href="participants_edit.php?participant_id='.$participantx.'">Edit</a>'; 
		if($nnpi==$cpartnumrows) $pscount2++;
		/*
      			message ($lang['participant_unsubscribed']);
			log__admin("participant_unsubscribe","participant_id:".$participant_id);
			redirect ($GLOBALS['settings__admin_folder']."/participants_edit.php?participant_id=".$participant_id);
			*/
			}
		   else message ($lang['database_error']);
		}
		else {
			$remarksOK=false;
			$inforemarks="";
			if($remarkstoadd) $inforemarks="No remarks added";
			echo '<br>Participant '.$npi.' of '.$pscount.' '.$fname.' '.$lname.' (absences: '.$nabsences.' of '.$nregistr.') <u>'.$email.'</u> id <u>'.$participantx.'</u> was <b>already</b> unsubscribed. '.$inforemarks.' '.$supecho.' <a target=_blank href="participants_edit.php?participant_id='.$participantx.'">Edit</a>'; 
			if($nnpi==$cpartnumrows) $pscount2++;
		}
		}

	if ($reallyexclude) {
		if($excluded!="y") {
                $query="UPDATE ".table('participants')."
                        SET deleted='y', excluded='y', last_delete_time=UNIX_TIMESTAMP(), last_delete_admin='".$expadmindata['adminname']."', can_undelete=0
 			WHERE participant_id='".$participantx."'";
                $result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
				
		if ($result!== false) {
		echo '<br>Participant '.$npi.' de '.$pscount.' '.$fname.' '.$lname.' (absences: '.$nabsences.' of '.$nregistr.') <u>'.$email.'</u> id <u>'.$participantx.'</u> désabonné et exclu. '.$supecho.' <a target=_blank href="participants_edit.php?participant_id='.$participantx.'">Edit</a>'; 
		if($nnpi==$cpartnumrows) $pscount2++;
				/*
                        message ($lang['participant_unsubscribed_and_excluded']);
			log__admin("participant_exclude","participant_id:".$participant_id);
                        redirect ($GLOBALS['settings__admin_folder']."/participants_edit.php?participant_id=".$participant_id);
						*/
			}
                   else message ($lang['database_error']);
		}
		else {
			$remarksOK=false;
			$inforemarks="";
			if($remarkstoadd) $inforemarks="No remarks added";
			echo '<br>Participant '.$npi.' of '.$pscount.' '.$fname.' '.$lname.' (absences: '.$nabsences.' of '.$nregistr.') <u>'.$email.'</u> id <u>'.$participantx.'</u> was <b>already</b> excluded. '.$inforemarks.' <a target=_blank href="participants_edit.php?participant_id='.$participantx.'">Edit</a>'; 
			if($nnpi==$cpartnumrows) $pscount2++;
		}
    }
				
	if ($reallyreactivate) {
		if($deleted!="n") {
                $query="UPDATE ".table('participants')."
                        SET deleted='n', excluded='n', can_undelete=0
 			WHERE participant_id='".$participantx."'";
                $result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
				
		if ($result!== false) {
		echo '<br>Participant '.$npi.' de '.$pscount.' '.$fname.' '.$lname.' (absences: '.$nabsences.' of '.$nregistr.') <u>'.$email.'</u> id <u>'.$participantx.'</u> réabonné. ';
		echo $supecho;
		echo '<a target=_blank href="participants_edit.php?participant_id='.$participantx.'">Edit</a>'; 
		if($nnpi==$cpartnumrows) $pscount2++;
				/*
                        message ($lang['participant_unsubscribed_and_excluded']);
			log__admin("participant_exclude","participant_id:".$participant_id);
                        redirect ($GLOBALS['settings__admin_folder']."/participants_edit.php?participant_id=".$participant_id);
						*/
			}
                   else message ($lang['database_error']);
		}
		else {
			$remarksOK=false;
			$inforemarks="";
			if($remarkstoadd) $inforemarks="No remarks added";
			echo '<br>Participant '.$npi.' of '.$pscount.' '.$fname.' '.$lname.' (absences: '.$nabsences.' of '.$nregistr.') <u>'.$email.'</u> id <u>'.$participantx.'</u> <b>wasn\'t</b> unsubscribed. '.$inforemarks.' '.$supecho.' <a target=_blank href="participants_edit.php?participant_id='.$participantx.'">Edit</a>'; 
			if($nnpi==$cpartnumrows) $pscount2++;
		}
    }
	if(!empty($_REQUEST['addtoremarks']))
	{
		$addtoremarks=trim($_REQUEST['addtoremarks']);
		if(!empty($addtoremarks) && $remarksOK && ($reallyreactivate || $reallyexclude || $reallydelete || $reallyaddremarksonly))
		{
			for($ir=0; $ir<count($cline); $ir++) $addtoremarks=str_replace("#".$ir."#",$cline[$ir],$addtoremarks);
			$newremarks=(trim($remarks)=="")?$addtoremarks:$remarks."\r\n".$addtoremarks;
			$newremarks=str_replace("'"," ",$newremarks);
                $query="UPDATE ".table('participants')."
                        SET `remarks`='".$newremarks."' 
 			WHERE participant_id='".$participantx."'";
                $result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
				if($result===false) message ($lang['database_error']);
			if($reallyaddremarksonly) {
				$inforemarks="<i>Remarks added</i>.";
				echo '<br>Participant '.$npi.' of '.$pscount.' '.$fname.' '.$lname.' (absences: '.$nabsences.' of '.$nregistr.') <u>'.$email.'</u> id <u>'.$participantx.'</u>. '.$inforemarks.' '.$supecho.' <a target=_blank href="participants_edit.php?participant_id='.$participantx.'">Edit</a>'; 
				if($nnpi==$cpartnumrows) $pscount2++;			
			}
			
		}
	}
}
}
}
}
	echo '<CENTER>
		<FORM method="POST" action="'.thisdoc().'">
		'.lang('please_note_the_ids_or_emails_of_participants_to_unsubscribe_or_resubscribe |, one_per_line*_or_separated_by_comma').':<br>
		<TEXTAREA NAME="participantstodelete" COLS=40 ROWS=20></TEXTAREA>
		<br>
		<TABLE width=90%>
			<TR>
				<TD colspan=2 align=center>
					'.$lang['exclude_or_unsubscribe_participant'].'<BR><BR>';
					//dump_array($participant);
			echo '	</TD>
			</TR>
			<TR>
				<TD align=center>
					<INPUT type=checkbox name="can_undelete">'.lang('can_undelete').' 
					<INPUT type=submit name="reallydelete" 
						value="'.$lang['yes_unsubscribe'].'">
				</TD>
				<TD align=center>
					<INPUT type=submit name="reallyexclude" 
						value="'.$lang['yes_unsubscribe_and_exclude'].'">
				</TD>
			</TR>
			<TR>
				<TD colspan=2 align=center>
					<INPUT type=submit name="reallyreactivate"
						value="'.$lang['yes_resubscribe'].'">
						<br><br>
					<INPUT type=submit name="reallyaddremarksonly"
						value="Only add remarks">
				</TD>
			</TR>
		</TABLE>
		<br><br>
		'.lang('add_text_to_remarks').' :<br> 
		<script>function add_bounce_info(external){x=document.getElementById("addBounceInfoCheckbox"); if(external) x.checked=x.checked?false:"checked"; if(x.checked) {document.getElementById("addtoremarks").value="Unsubscribed for #1# bounce(s) with #6# user unknown, #5# disk space remark(s) and #7# permanent fatal error(s), last bounce on #3#, first bounce on #2#";} else document.getElementById("addtoremarks").value="";}</script>
		<TEXTAREA NAME="addtoremarks" id="addtoremarks" COLS=40 ROWS=3></TEXTAREA><br>
		<input type=checkbox id="addBounceInfoCheckbox" onclick="add_bounce_info(false)" value=1 /><span onclick="add_bounce_info(true)" style="cursor:default">&nbsp;'.lang('bounce_info_from_table').'</span>
		<br><br>
		*one line may contain several values separated by spaces or tabs, only the fist one will be taken as the Id or email, the others may be added to remarks via the "Add to remarks" field using the #n# mask where n is the index starting form 0 for the Id or email itself.
		<br><br>
		</FORM>
		
	      </CENTER>
		  ';

include ("footer.php");

?>




<?php
ob_start();

if (isset($_REQUEST['dossier'])) $dossier=$_REQUEST['dossier']; else $dossier="";

$menu__area="payment_operations";
$title="payment operations";
include ("header.php");


// if(empty($table)) {message(lang('no_table_to_edit')); redirect($GLOBALS['settings__admin_folder']."/options_main.php"); }//die(lang('no_table_to_edit'));
// if($table!="permanent_pay_accounts") {message(lang('wrong_table')); redirect($GLOBALS['settings__admin_folder']."/options_main.php"); } //redirect($GLOBALS['settings__admin_folder']."/".thisdoc()."?table=permanent_pay_accounts");//die(lang('wrong_table'));


	$done=false;
	$allow_cat="experimentclass";
    
	$allow=check_allow($allow_cat.'_edit','options_main.php');

	$header=lang('financial_operations');
	if(!empty($_REQUEST['addit'])) {
		$new=empty($_REQUEST["id"]) || empty($_REQUEST["details"]);
		// $new=true;
		if($new) $header=lang('new_operation'); else $header=lang('details_operation_id').''.$_REQUEST["id"];
	}
	// $where="";
	$where=" WHERE archived = 0 ";
	if(!empty($_REQUEST['all'])) $where="";
	
	echo '<BR>
		<center><h4>'.$header.'</h4>';

	if (check_allow($allow_cat.'_add') && empty($_REQUEST["addit"])) {
		echo '	<BR>
			<form action="'.thisdoc().'">
			<INPUT type=submit name="addit" value="'.lang('new_operation').'">
			<INPUT type=hidden name="dossier" value="'.$dossier.'">
			</FORM>';
		}
				
		
	// $columns=table_fields(table("pay_operations"));
	// $id_name=(in_array('id',$columns))?'id':$columns[0];
	// unset($columns[array_search($id_name,$columns)]);
	// $modifiable=array();
	// $not_in_summary=array();
	
	// $colnames=array();
	// foreach ($columns as $col) {$colnames[]=lang($col); } //$modifiable[]=($col!=$id_name)?1:0;
	operation::operation_tables();

	
	if(!empty($_REQUEST["addoperation"])) {
		
		// log__admin("operation_add","payment_file: $dossier");
		$account_names=array('debit','credit');//array_keys(helpers_pay_accounts($dossier));
		$accerrmess="";
		foreach($account_names as $accn) {
			$acc=$_REQUEST[$accn];
			$camount=0;
			$accline=orsee_query("SELECT SUM(amount) as amount FROM ".table("pay_accounts")." WHERE shortname='$acc' AND payment_file='$dossier'");
			// var_dump($accline);
			if($accline!==false) $camount=$accline["amount"];
			if($camount==null) $camount=0;
			$init_amount=$_REQUEST[$accn.'_init_amount'];
			if(empty($init_amount)) $init_amount=0;
			if ($camount!=$_REQUEST[$accn.'_init_amount']) $accerrmess.=lang('account')." `".helpers__pay_account_longname($acc).'`: '.lang('initial_amount',false)."=$init_amount, ".lang('new_amount',false)."=$camount. ";
			
		}
		if($accerrmess!="")
		{
			message(lang('account_values_were_modified_by_another_operation').'.<br>'.$accerrmess.' (<a target=_blank href="'.thisdoc().'">'.lang('see_operations').'</a>)<br>'.lang('changes_not_saved').'.');
			$_SESSION["restore_values"]=$_REQUEST;
			redirect($GLOBALS['settings__admin_folder']."/".thisdoc()."?dossier=$dossier&addit=1");
			exit;
		}
		$o=new operation($_REQUEST['debit'],$_REQUEST['credit'],$_REQUEST['dossier']);
		$o->remarks=str_replace("'","’",$_REQUEST['remarks']);
		$types=$_REQUEST['type']; $n_nominals=$_REQUEST['n_nominal'];
		$o->id=$_REQUEST['operation_id'];
		$query="SELECT id FROM ".table("pay_operations")." WHERE id='".$o->id."'";
		if(orsee_query($query)!==false) {
			message(lang('operation_already_exists').' (id='.$o->id.')');
			redirect($GLOBALS['settings__admin_folder']."/".thisdoc());
			exit;
		}
		foreach($_REQUEST['nominal'] as $k=>$nominal) {
			$cn_nominals=($types[$k]=="cash")?$n_nominals[$k]:$_REQUEST['total_noncash']; 
			if(!empty($cn_nominals)) $o->add($types[$k],$nominal,$cn_nominals);
		}
		if(!empty($_REQUEST['instead_of_file'])) $o->instead_of=$_REQUEST['instead_of_file'];
		$o->make();
		message (lang('changes_saved'));
		redirect($GLOBALS['settings__admin_folder']."/".thisdoc());
		// show_message();
	}
	

	if(!empty($_REQUEST["addit"])) {
		if(empty($dossier)) {
			echo '<CENTER>
				<FORM name="choisir_dossier" method=post action="'.thisdoc().'">
			
				<BR>';
			echo "<h2>".lang('please_choose_the_payment_file')."</h2>";
			echo '
				<INPUT name="addit" type=hidden value="'.$_REQUEST['addit'].'">';
			if(!empty($_REQUEST['only_empty'])) echo '
				<INPUT name="only_empty" type=hidden value="'.$_REQUEST['only_empty'].'">';
			echo '
				<table border=0 style="text-align:center"><tr>
				<td></td>
				</tr><tr>
				<td>';
			$select_payment_file=helpers__select_payment_file();
			if($select_payment_file!==false) echo $select_payment_file;
			echo '
				</td>
				</tr></table>
				';
			if($select_payment_file!==false)  echo '<INPUT type=submit name="choose_payment_file" value="OK">';
			echo '<br><br><a target=_blank href="dossiers_paiement_main.php">'.lang('edit_payment_files').'</a>
				</FORM>
					';
		echo '<BR><BR>
			<A href="'.thisdoc().'?dossier='.$dossier.'">'.icon('back').' '.lang('financial_operations_main_page').'</A><BR><BR>
			';
			echo '  </CENTER>';
			include ("footer.php");
			exit;
		}
		$restore_values=array();
		if(!empty($_SESSION["restore_values"])) {$restore_values=$_SESSION["restore_values"]; unset($_SESSION["restore_values"]);}
		$operation_id=operation::get_new_id();
		$prevdebit="";$prevcredit="";$disable=false;
		$supselectoption='onchange="account_info(this)"';
		if(!$new){
			$supselectoption="";
			$operation_id=$_REQUEST['id'];
			$disable=true;
			$prevbaseline=orsee_query("SELECT * FROM ".table('pay_operations')." WHERE id='$operation_id'");
			if($prevbaseline===false) {
				echo lang('this_operation_does_not_exist').'.';
				echo '<BR><BR>
				<A href="'.thisdoc().'">'.icon('back').' '.lang('financial_operations_main_page').'</A><BR><BR>
				</CENTER>';
				include_once ("footer.php");
				exit;
			}
			$prevbaselines=orsee_query("SELECT * FROM ".table('pay_operations')." WHERE id='$operation_id'","return_same");
			$prevdebit=$prevbaseline["debit_account"];
			$prevcredit=$prevbaseline["credit_account"];
			$dossier=$prevbaseline["payment_file"];
			echo "<h4 align=center>".lang('date_time').": ".date("d/m/Y H:i:s",$prevbaseline["datetime"])."</h4>";
			echo "<p align=center>".lang('registered_by').": ".admin__load_name_by_val($prevbaseline["admin"])." </p>";//(".$prevbaseline["admin"].")
		}
		else {
			echo '<BR>';
			$accounts=array_keys(helpers_pay_accounts($dossier));
			$acc_info=array();
			foreach($accounts as $acc) {
				$camount=0; $regiedavance_amount=0;
				$accline=orsee_query("SELECT SUM(amount) as amount FROM ".table("pay_accounts")." WHERE shortname='$acc' AND payment_file='$dossier'");
				$use_regiedavance=($acc=="lab" && (helpers__regiedavance_budget()>0 || helpers__regiedavance_amount_left()>0));
				if($use_regiedavance) $regiedavance_amount=helpers__regiedavance_amount_left();
				// var_dump($accline);
				if($accline!==false) $camount=$accline["amount"];
				if($camount==null) $camount=0;
				$supreg=$use_regiedavance?":".$regiedavance_amount:"";
				$acc_info[]=$acc.":".$camount.$supreg;
				
			}
			echo '<INPUT type=hidden name="acc_amounts" id="acc_amounts" value="'.implode(',',$acc_info).'">';
		}
		$dossier_info=orsee_query("SELECT * FROM ".table("payment_files")." WHERE `id`='$dossier'");
		echo '<center><h4>'.lang('payment_file').' '.dossier_name($dossier,1);
		if($new) echo ' <small>(<a target=_blank href="dossiers_paiement_main.php?addit=1&show_statistics=1&id='.$dossier.'">'.lang('edit',false).'</a>, <a href="'.thisdoc().'?addit=1">'.lang('choose_another',false).'</a>)</small>';
		else echo ' <small>(<a target=_blank href="'.thisdoc().'?addit=1&dossier='.$dossier.'">'.lang('make_a',false).' '.lang('new_operation',false).'</a>)</small>';
		echo '</h4>';
		if (!$new) $button_title=lang('change'); else $button_title=lang('make');
		$form_name='operation';
		echo '<BR><FORM name='.$form_name.' action="'.thisdoc().'">
		<INPUT type=hidden name="dossier" value="'.$dossier.'">
		<INPUT type=hidden name="operation_id" value="'.$operation_id.'">';
		echo '<table cellpadding=10 style="text-align:center;vertical-align:middle"><tr>
		<td>'.lang('debit_account_from').'</td><td>'.lang('credit_account_to').'</td></tr>
		<tr><td>'.helpers__select_pay_accounts("debit",$dossier,$prevdebit,$disable,$supselectoption).'</td>
		<td>'.helpers__select_pay_accounts("credit",$dossier,$prevcredit,$disable,$supselectoption).'</td>
		</tr><tr style="height:40px;">
		<td style="vertical-align:top;position:relative;top:-10px"><span id=debit_info></span><INPUT type=hidden id="debit_init_amount" name="debit_init_amount" value="" /></td>
		<td style="vertical-align:top;position:relative;top:-10px"><span id=credit_info></span><INPUT type=hidden id="credit_init_amount" name="credit_init_amount" value="" /></td>
		</tr></table>';
		echo '
		<table border=0 style="text-align:center;vertical-align:middle">';
		$theaders=array(lang('nominal'),lang('number_count'),lang('sum'));
		echo "<tr>";
		foreach($theaders as $h) echo "<td style='text-align:center'>$h</td>";
		echo" </tr>";
		$nominals=$dossier_info['nominal_values'];
		if(empty($nominals)) $nominals="20,10,5,2,1,0.5,0.2,0.1,0.05,0.01";
		//$nominals=preg_replace("/\s+|,|;|\.\s+/"," ",$nominals);
		$nominals=preg_replace("/[^0-9. ]+/"," ",$nominals);
		$nominals=preg_replace("/\s+/"," ",$nominals);
		$nominals=preg_replace("/\s+/"," ",$nominals);
		$nominalarr=explode(" ",$nominals);
		// var_dump($nominals);
		// print_r($nominalarr); echo "<hr>";
		$nominal_vals=array();
		$skip=false;
		for($i=0; $i<count($nominalarr); $i++) {
			if(!$skip) $nominal_vals[]=$nominalarr[$i];
			if($skip && $nominalarr[$i]!=0) {$nominal_vals[count($nominal_vals)-1].=".".$nominalarr[$i]; $skip=false;}
			if($nominalarr[$i]==0) $skip=true;
		}
		$nominal_vals=array_map("floatval",$nominal_vals);
		sort($nominal_vals);
		$money_vals=array();
		for($i=count($nominal_vals)-1; $i>=0; $i--) {
			$money_vals[]=array("nominal",$nominal_vals[$i]);
		}
		$noncash_present=false;
		if(!empty($dossier_info['nonliquid_funds_name'])) {$noncash_present=true; $money_vals[]=array("noncash",$dossier_info['nonliquid_funds_name']);}
		// $nominal_vals=array_reverse($nominal_vals);
		// print_r($money_vals); exit;
		$nn=0;
		$total_cash=""; $total_all="";
		if(!$new) {$total_cash=0; $total_all=0;}
		$remarks='';
		if(!$new) {
			$money_vals=array();
			foreach($prevbaselines as $pbl)
			{
				$ctype=($pbl["unit"]=="cash")?"nominal":$pbl["unit"];
				$money_vals[]=array($ctype,$pbl["nominal"]);
			}
		}
		foreach($money_vals as $k=>$mv) {
			if(!$new) {
				// $query="SELECT * FROM ".table('pay_operations')." WHERE id='$operation_id' AND unit='".(($mv[0]=="nominal")?"cash":$mv[1])."' AND nominal='".(($mv[0]=="nominal")?$mv[1]:"1")."'";
				// var_dump($query);
				$prevline=$prevbaselines[$k];//orsee_query($query);
			}
			echo '<tr><td>';
			if($mv[0]=="nominal") {$nn++; echo ((float)$mv[1]).''.lang('money_symbol');}
			else echo '';//$mv[0];
			echo '</td><td>';
			if($new) {
				if($mv[0]=="nominal") echo '<input type=hidden id="nn_'.$nn.'" name="nominal['.$nn.']" value="'.$mv[1].'"><input type=hidden name="type['.$nn.']" value="cash">
				<input type=text id="n_'.$nn.'" name="n_nominal['.$nn.']" onblur="calculate_sum(this)">';
				else echo '<input type=hidden id="nn_'.($nn+1).'" name="nominal['.($nn+1).']" value="1">'.$mv[1].' ('.lang('total_sum',false).')<span id=total_'.($nn+1).'></span><input type=hidden name="type['.($nn+1).']" value="'.$mv[1].'">';
				echo '</td><td>';
				if($mv[0]=="nominal") echo '<span id=total_'.$nn.'></span>';
				else echo '<input type=text size=7 id="n_'.($nn+1).'" name="total_'.$mv[0].'" onblur="calculate_sum(this)" >'.lang('money_symbol');
			} else {
				// var_dump($prevline);
				if($prevline===false) {if($mv[0]=="nominal") echo '0</td><td>0'; else echo $mv[1].' : </td><td>0';}
				else {
					if($mv[0]=="nominal") echo $prevline["n_units"]; else echo $prevline["unit"].' : '; //'<input type=text style="text-align:center" value="'.$prevline["n_units"].'" disabled/>'
					echo '</td><td>';
					echo $prevline["amount"].' '.lang('money_symbol');
					$mv0arr=explode("_",$mv[0]);
					if($mv[0]=="nominal" || $mv[0]=="cash" || (count($mv0arr)>1 && $mv0arr[1]=="cash")) $total_cash+=$prevline["amount"];
					$total_all+=$prevline["amount"];
					$remarks.=$prevline["remarks"].' ';
					if($k==count($money_vals)-1) {
						// var_dump($prevline["instead_of_file"]); 
						if(!empty($prevline["instead_of_file"])) {
							if(trim($remarks)!="") $remarks.="<br>";
							$remarks.=lang('replaces_the_file').' '.dossier_name($prevline["instead_of_file"],1);
						}
					}
				}
			}
			echo '</td></tr>';
		}
		if(!$new) {$total_cash.='&nbsp'.lang('money_symbol'); $total_all.='&nbsp'.lang('money_symbol');}
		echo "<tr><td colspan=3>&nbsp;</td></tr>";
		echo "<tr><td colspan=2 style='text-align:right'>".lang('total').":</td><td id=total_all  style='border-top:1px double black'>$total_all</td></tr>";
		if($noncash_present) echo "<tr><td colspan=2 style='text-align:right'>".lang('where_cash',false).":</td><td id=total_cash>$total_cash</td></tr>";
		echo '</table><br>';
		if($new)   {
			script__calculate_sum(count($nominal_vals),"n",$noncash_present?"true":"false");
			$remarks='<textarea rows=2 name=remarks></textarea>';
			$acc_files=array();
			foreach (helpers_pay_accounts($dossier) as $pa=>$pa_name) {
				$incl="$pa";
				$acquery="SELECT `id` FROM ".table("payment_files")." WHERE money_responsible='$pa'";
				$pfs_check=orsee_query($acquery);
				if($pfs_check!==false) {
					$incl.=":";
					$pfs=orsee_query($acquery,"return_first_elem");
					$me_n=array_search($dossier,$pfs);
					if($me_n!==false) {
						$incl.="y";
						unset($pfs[$me_n]);
					}
					else $incl.="n";
					if(count($pfs)>0) {
						// $dnfunc=create_function('$x','return str_replace(array(":","-",","),array("&#58;","&ndash;","&#44;"),dossier_name($x,1));');
						$dnfunc=function($x) {return str_replace(array(":","-",","),array("&#58;","&ndash;","&#44;"),dossier_name($x,1));};
						$incl.="-".implode("-",$pfs).":".implode("-",array_map($dnfunc,$pfs));
					}
					//var_dump($incl);
				}
				$acc_files[]=$incl;
			}
			echo "<INPUT id=instead_of_base type=hidden value='".implode(",",$acc_files)."'>";
			echo "<table align=center style='text-align:center'><tr>
				<td id='for_instead_of' style='height:40px;'></td>
				</tr></table><br>";
		}
		if(trim($remarks)=="") $remarks="-";
		echo '
			<TABLE>
				<TR>
					<TD COLSPAN=2 align=center>';
		if($new) echo '
						<INPUT name=addoperation type=hidden value=1>
						<INPUT name=change id=change type=button onclick="newoperation(this.form)" value="'.$button_title.'">';

		echo '
					</TD>
				</TR>
				<TR><TD COLSPAN=2 align=center>'.lang('remarks').':<br>'.$remarks.'</TD></TR>
			</table>
			</FORM>
			<BR>';
		//script__update_responsable($form_name,$js_select_name);


		echo '
			<A href="'.thisdoc().'">'.icon('back').' '.lang('financial_operations_main_page').'</A><BR><BR>
			</CENTER>';
		include_once ("footer.php");
		exit;
	}

	if (isset($_REQUEST['sort']) && $_REQUEST['sort']) {
		$order=$_REQUEST['sort'];
		$ordernamearr=explode('_',$order);
		if($ordernamearr[0]=="payment") {
			$order="CAST($order AS DECIMAL(10,2))";
		}
	}
	else { 
		$order="datetime DESC, archived";
    }

		// $tables["pay_operations"]=array("id"=>"int(20)","admin"=>"varchar(150)","datetime"=>"varchar(250)","payment_file"=>"varchar(250)","debit_account"=>"varchar(250)","credit_account"=>"varchar(250)","unit"=>"varchar(25)","nominal"=>"DECIMAL(4,2)","n_units"=>"INT","amount"=>"DECIMAL(5,2)","remarks"=>"text","archived"=>"tinyint");
	$supvars="";
	if(!empty($_REQUEST['all'])) $supvars=", archived";
	mysqli_query($GLOBALS['mysqli'],"SET sql_mode = ''") or die("Error setting sql_mode to '': " . mysqli_error($GLOBALS['mysqli']));
	$query="SELECT id, admin, datetime as date_time, SUM(amount) as amount, payment_file, debit_account, credit_account, GROUP_CONCAT(remarks) as remarks, MIN(instead_of_file) as replacement_file".$supvars."
      		FROM ".table('pay_operations')."
		 ".$where."
			GROUP BY id
			ORDER by ".$order;
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$query);
	//$exclfields=array("unit,","n_units","")

	$shade=false;
	$columns=array();
	if (mysqli_num_rows($result)>0) {
	
	$columns=array_keys(orsee_query($query));
	// var_dump($columns);
	$colnames=array();
	foreach ($columns as $col) {$colnames[]=lang($col); } 
	$not_in_summary=array("replacement_file"=>true);
	
	
	echo '<BR>
		<table border=1 style="border-collapse:collapse">
			<TR>
 				';
        		foreach ($colnames as $colkey=>$colname) if($columns[$colkey]!="archived" || !empty($_REQUEST['all']))
					if(empty($not_in_summary[$columns[$colkey]])) {
                	echo '<td class="small">
							'.$colname.'
					</td>';
				}

	echo '			<TD></TD>
			</TR>';

	while ($line=mysqli_fetch_assoc($result)) {

   		echo '	<tr class="small"'; 
   		if ($shade) echo ' bgcolor="'.$color['list_shade1'].'"';
		else echo ' bgcolor="'.$color['list_shade2'].'"';
   		echo '>';
		foreach ($columns as $col) if($col!="archived" || !empty($_REQUEST['all'])) 
		if(empty($not_in_summary[$col])) {
			$colval=$line[$col];
			if(substr($col,-8)=="_account") {$colval=helpers__pay_account_longname($colval);}
			$supstart=''; $supend='';
			if($col=='amount') {$supstart= '<A HREF="'.thisdoc().'?addit=1&id='.$line['id'].'&details=1&dossier='.$line['payment_file'].'"><small>'; $supend='</small></A>';}
			if($col=="amount") { $colval.='&nbsp;'.lang('money_symbol'); /*$supstart.="<span style='color:blue'>";  $supend="</span>". $supend;*/}
			if($col=="payment_file") $colval=dossier_name($colval,1);
			if($col=="remarks") { /*$colval=str_replace(",,","",$colval);*/ $colval=preg_replace("/^,+|,+$/","",$colval); if(!empty($line["replacement_file"])) {if(trim($colval)!="") $colval.="<br>"; $colval.=lang('replaces_the_file').' '.dossier_name($line["replacement_file"],1);} }
			if($col=="date_time") { $colval=date("d/m/Y H:i:s",$colval);}
			// if($col=="money_responsible" || $col=="experimenter") $line[$col]=admin__load_name_by_val($line[$col],true); //,$col!="experimenter"
        	echo '	<td class="small" valign=top';
            if ($shade) echo ' bgcolor="'.$color['list_shade1'].'"'; 
			else echo ' bgcolor="'.$color['list_shade2'].'"';
            echo '>';
			echo $supstart;
			if (isset($chnl2br) && $chnl2br) echo nl2br(stripslashes($colval));
            else echo stripslashes($colval);
			echo $supend;
            echo '</td>';
		}
   		echo '		<TD>
					<A HREF="'.thisdoc().'?addit=1&id='.$line['id'].'&details=1&dossier='.$line['payment_file'].'">'.
						lang('details').'</A>
				</TD>
   			</tr>';
   		if ($shade) $shade=false; else $shade=true;
		}
		
		}
	   else {
		echo '	<tr>
				<td style="text-align:center" colspan='.(count($columns)+1).'>
					'.lang('no_lines').'<BR><BR>
		<A href="javascript:history.back()">'.icon('back').' '.lang('back').'</A><BR><BR>
				</td>
			</tr>';
		}

	echo '</table>

		</CENTER>';

include_once ("footer.php");



?>

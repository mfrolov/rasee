<?php
// part of orsee. see orsee.org
ob_start();

$menu__area="payment files";
$title="receipts information";


//"Rules signed?" disabled by Maxim Frolov;
$showrules=false; if(!empty($_REQUEST["showrules"])) $showrules=true;

include("header.php");
$transfer_mode=false;
$disable_check=false;

if(!empty($_REQUEST["transfer_mode"]) && check_allow('experiment_edit_participants')) $transfer_mode=true;

	if (isset($_REQUEST['dossier']) && $_REQUEST['dossier']) $dossier=$_REQUEST['dossier'];
                else {redirect($GLOBALS['settings__admin_folder']."/dossiers_paiement_main.php"); exit;}// $dossier="";// {var_dump($_REQUEST); var_dump($_POST); exit;}//

        // if (isset($_REQUEST['session_id']) && $_REQUEST['session_id']) $session_id=$_REQUEST['session_id'];
                // else $session_id='';
				$session_id='';
// show_message();
if($transfer_mode  && !empty($_REQUEST['paynums_tomove'])  && !empty($_REQUEST['move_to'])) {
	if($_REQUEST['move_to']<=operation::$n_mix_accounts) {
		$message=lang("payment_file").' '.dossier_name($_REQUEST['move_to'],1).' (id '.$_REQUEST['move_to'].') '.lang('can_not accept_participants',false);
		message($message); //.'<br>'.print_r($tocorrect,true)
		redirect($GLOBALS['settings__admin_folder'].'/dossiers_paiement_report.php?dossier='.$dossier.'&transfer_mode=1');
		exit;
	}
	if(!empty(ini_get('max_execution_time')) && ini_get('max_execution_time')<720) ini_set('max_execution_time', '720');
	if(!empty(ini_get('max_input_time')) && ini_get('max_input_time')<600) ini_set('max_input_time', '600');
	$query="SELECT MAX(CAST(payment_num AS UNSIGNED)) as max_num FROM ".table('participate_at')." WHERE payment_file=".$_REQUEST['move_to'];
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$query);
	$maxprev=0;
	if(mysqli_num_rows($result)>0) {
		$line=mysqli_fetch_assoc($result);
		if(is_numeric($line['max_num']))$maxprev=$line['max_num'];
	}
	// var_dump(mysqli_num_rows($result),$maxprev); exit;
	mysqli_free_result($result);
	$paynums_tomove=explode(',',$_REQUEST['paynums_tomove']);
	sort($paynums_tomove,SORT_NUMERIC);
	$minnum=$maxprev+1; $maxnum=$minnum;
	foreach($paynums_tomove as $k=>$v) {
		$newnum=$maxprev+$k+1;
		$query="UPDATE ".table('participate_at')." SET payment_file='".$_REQUEST['move_to']."',payment_num='$newnum' WHERE payment_file='$dossier' AND payment_num='$v'";
		$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$query);
		$maxnum=$newnum;
	}
	$wrongnumbers=[]; $stillok=true; $prevnum=$paynums_tomove[0]-1;
	if($_REQUEST['orig_max_paynum']>$paynums_tomove[0]) {
		$query="SELECT payment_num,participate_id FROM ".table('participate_at')." WHERE payment_file=$dossier";
		$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$query);
		$cpaynums=[];
		if(mysqli_num_rows($result)>0) {
			while($line=mysqli_fetch_assoc($result)) {
				$cpaynums[$line["participate_id"]]=$line["payment_num"];
			}
			asort($cpaynums,SORT_NUMERIC);
			$n=0;
			$tocorrect=[];
			foreach ($cpaynums as $key => $val) {
				$n++;
				if($val!=$n) $tocorrect[$key]=$n;
			}
			foreach ($tocorrect as $key => $val) {
				$query="UPDATE ".table('participate_at')." SET payment_num='$val' WHERE payment_file='$dossier' AND participate_id='$key'";
				$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$query);
			}
		}
		mysqli_free_result($result);
		/*for($i=$paynums_tomove[0]+1; $i<=$_REQUEST['orig_max_paynum']; $i++) {
			if($stillok && !in_array($i,$paynums_tomove)) {$stillok=false; $prevnum=$i-1;}
			if(!$stillok) $wrongnumbers[]=$i;
		}
		$replacemets=[];
		foreach($wrongnumbers as $k=>$v) {
			$newnum=$prevnum+$k+1;
			$replacemets[]=$newnum;
			$query="UPDATE ".table('participate_at')." SET payment_num='$newnum' WHERE payment_file='$dossier' AND payment_num='$v'";
			$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$query);
		}*/
	}
	log__admin("move_subjects_payment_file","from:".dossier_name($dossier,1).", to:".dossier_name($_REQUEST['move_to'],1).", n:".$_REQUEST['n_tomove'].", sum:".$_REQUEST['sum_tomove'].lang('money_symbol'));
	$message=$_REQUEST['n_tomove'].' '.lang('participant(s)',false).' '.lang("moved_to payment_file", false).' <A target=_blank HREF="'.$_SERVER['PHP_SELF'].'?dossier='.$_REQUEST['move_to'].'" >'.dossier_name($_REQUEST['move_to'],1).'</A> ('.lang('cf_numbers',false).' '.$minnum.' - '.$maxnum.')';
	if($dossier==$_REQUEST['move_to']) $message=lang('the_destination_payment_file_was_the_same').'. '.$_REQUEST['n_tomove'].' '.lang('participant(s)',false).' '.lang("replaced_at_end", false);
	message($message); //.'<br>'.print_r($tocorrect,true)
	redirect($GLOBALS['settings__admin_folder'].'/dossiers_paiement_report.php?dossier='.$dossier.'&transfer_mode=1');
	exit;
}
	if (isset($_REQUEST['remember']) && $_REQUEST['remember']) { 
		$_REQUEST=array_merge($_REQUEST,$_SESSION['save_posted']);
		$_SESSION['save_posted']=array();
		unset($_REQUEST['change']);
		}
		
	$query="SELECT experiment_id FROM ".table('experiments')." WHERE payment_file=$dossier";
	$line=orsee_query($query);
	$experiment_id=$line['experiment_id'];
	$query="SELECT * FROM ".table('payment_files')." WHERE id=$dossier";
	$dossier_line=orsee_query($query);
	$query="SELECT count(*) as num_p, SUM(CAST(payment AS DECIMAL(10,2))) as pay_cash, SUM(CAST(payment_nlf AS DECIMAL(10,2))) as pay_noncash FROM ".table('participate_at')." WHERE payment_file='".$dossier."'";
	$dossier_stat_line=orsee_query($query);

	$allow=check_allow('experiment_show_participants','experiment_show.php?experiment_id='.$experiment_id);

	$experiment=orsee_db_load_array("experiments",$experiment_id,"experiment_id");
	if (!check_allow('experiment_restriction_override'))
		check_experiment_allowed($experiment,$GLOBALS['settings__admin_folder']."/experiment_show.php?experiment_id=".$experiment_id);

	if (!isset($_REQUEST['pay'])) $_REQUEST['pay']="1";
	if (!isset($_REQUEST['payment_mode'])) $_REQUEST['payment_mode']="view";
	if (!isset($_REQUEST['focus'])) $_REQUEST['focus']="all";
	if (!isset($_REQUEST['only_empty'])) $_REQUEST['only_empty']="0";
	$count_firstcol=false;
	


	$columns=participant__load_result_table_fields('session');
	$nlfpay_name="";
	if(!empty($_REQUEST['pay'])) {
		$payment_mode="entry"; $dossier="";
		if(!empty($_REQUEST['dossier'])) $dossier=$_REQUEST['dossier'];
		if(!empty($_REQUEST['payment_mode'])) $payment_mode=$_REQUEST['payment_mode'];
		$partemailpaymode=$_REQUEST['pay'];
		if($payment_mode=="view" && !empty($_REQUEST['show_emails'])) $partemailpaymode=2;
		$columns=participant__load_result_table_fields('session','',$partemailpaymode);
		query_makecolumns(table('participate_at'),array("payment","payment_nlf","payment_num","payment_file"),array("varchar(20)","varchar(20)","varchar(20)","varchar(20)"), array("","","",""));
		if($dossier!=="") {
			$query="SELECT * FROM ".table('payment_files')." WHERE id=$dossier";
			if(!empty($dossier))$line=orsee_query($query);
			if(!empty($line['nonliquid_funds_name'])) $nlfpay_name=$line['nonliquid_funds_name'];
		}

	}

	if ($session_id) $session=orsee_db_load_array("sessions",$session_id,"session_id");

	script__part_reg_show();

	$csorts=array(); 
	foreach($columns as $c) if (count($csorts)<2 && $c['allow_sort']) $csorts[]=$c['sort_order'];
	$csorts_string=implode(",",$csorts);

	if (isset($_REQUEST['sort']) && $_REQUEST['sort']) {
		$order=$_REQUEST['sort'];
		$ordernamearr=explode('_',$order);
		if($ordernamearr[0]=="payment") {
			if(empty($ordernamearr[1]) || $ordernamearr[1]!="dossier") $order="CAST($order AS DECIMAL(10,2))";
			elseif($ordernamearr[1]=="dossier") $order="$order, CAST(payment_num AS UNSIGNED)";
		}
		
	}
	else { 
		/*$order="session_start_year, session_start_month, session_start_day,
				session_start_hour, session_start_minute";*/
		$order=table('participate_at').".payment_file,CAST(payment_num AS UNSIGNED)";
		if ($csorts_string) $order.=",".$csorts_string;
    }

	if (isset($_REQUEST['focus']) && $_REQUEST['focus']) $focus=$_REQUEST['focus']; else $focus="assigned";
	$arr_focus=explode(":",$focus);
	$focus2part="";
	if(count($arr_focus)>1) {
		$focus=$arr_focus[0];
		$focus2part=$arr_focus[1];
	}
	// message("focus=".$_REQUEST['focus'].", focus2part=".$focus2part);

	$assigned=false; $invited=false; $registered=false; $shownup=false; $participated=false; $nonparticipated=false; $notpresent=false; $absent=false; $todayabsent=false; $absentold=false; $absentnew=false; $new=false; $today=false; $todaynew=false; $todayopen=false; $todayopenshownup=false; $todayshownup=false; $todaystarted=false; $tomorrow=false; $atdate=false; $absentatdate=false; $atmonth=false; 

	$$focus=true;


	switch ($focus) {
		case "assigned":	$where_clause="AND registered='n'";
					$title=lang('assigned_subjects_not_yet_registered');
					break;
		case "invited":		$where_clause="AND invited='y' AND registered='n'";
                                        $title=lang('invited_subjects_not_yet_registered');
                                        break;
        	case "registered":	$where_clause="AND registered='y'";
                                        $title=$lang['registered_subjects'];
                                        break;
        	case "shownup":		$where_clause="AND shownup='y'";
                                        $title=$lang['shownup_subjects'];
                                        break;
        	case "participated":	$where_clause="AND participated='y'";
                                        $title=$lang['subjects_participated'];
                                        break;
        	case "nonparticipated":	$where_clause="AND participated='n'  AND registered='y'";
                                        $title=$lang['nonparticipated'];
                                        break;
			case "notpresent":						
            case "absent":    $where_clause="AND registered='y' AND shownup='n' AND participated='n'";
                                        $title=lang('subjects_absent');
                                        break;
            case "absentold":    $where_clause="AND registered='y' AND shownup='n' AND participated='n' AND session_finished='y'";
                                        $title=lang('absent_subjects_in_closed_sessions');
                                        break;
            case "absentnew":    $where_clause="AND registered='y' AND shownup='n' AND participated='n' AND session_finished='n'";
                                        $title=lang('absent_subjects_in_opened_sessions');
                                        break;
            case "new":    $where_clause="AND registered='y' AND shownup='n' AND participated='n' AND session_finished='n'";
                                        $title=lang('subjects_new');
                                        break;
            case "today":    
										$where_clause="AND registered='y' AND session_start_year='".date('Y')."' AND session_start_month='".date('n')."' AND session_start_day='".date('j')."'";
                                        $title=lang('subjects').' '.date('d/m/Y');
                                        break;
            case "todaystarted":    
										$where_clause="AND registered='y' AND session_start_year='".date('Y')."' AND session_start_month='".date('n')."' AND session_start_day='".date('j')."' AND CAST(session_start_hour AS UNSIGNED)<=".date('G')."";
                                        $title=lang('subjects').' '.date('d/m/Y').' '.lang('before',false).' '.date('G').lang('h',false);
                                        break;
            case "atdate":    
										$cdate=empty($_REQUEST['date'])?date('j').".".date('n').".".date('Y'):$_REQUEST['date'];
										if(!empty($focus2part)) $cdate=$focus2part;
										$cdates=explode('-',$cdate);
										$where_clause="AND registered='y' ";
										$datetoshow="";
										for($i=0; $i<count($cdates); $i++)
										{
											$acdate=explode('.',$cdates[$i]);
											if(count($acdate)<2) $acdate=explode('/',$cdates[$i]);
											if(count($acdate)<2) $acdate[]=date('n');
											if(count($acdate)<3) $acdate[]=date('Y');
											$mysqlsessdate="STR_TO_DATE(CONCAT(session_start_year,'-',LPAD(session_start_month,2,'00'),'-',LPAD(session_start_day,2,'00'),'-',LPAD(session_start_hour,2,'00'),'-',LPAD(session_start_minute,2,'00')),'%Y-%m-%d-%H-%i')";
											$mysqldate1="STR_TO_DATE('".$acdate[2]."-".$acdate[1]."-".$acdate[0]."-00-01','%Y-%m-%d-%H-%i')";
											$mysqldate2="STR_TO_DATE('".$acdate[2]."-".$acdate[1]."-".$acdate[0]."-23-59','%Y-%m-%d-%H-%i')";
											if(count($cdates)==1) $where_clause.="AND session_start_year='".$acdate[2]."' AND session_start_month='".$acdate[1]."' AND session_start_day='".$acdate[0]."'";
											elseif($i==0) {$supsess_criteria.="AND ".$mysqlsessdate.">=".$mysqldate1." "; $where_clause.=$supsess_criteria;}
											else {$supsess_criteria.="AND ".$mysqlsessdate."<=".$mysqldate2." "; $where_clause.=$supsess_criteria;}
											if($i>0) $datetoshow.=" - ";
											$datetoshow.=implode("/",array_map("str_pad",$acdate,array_fill(0,count($acdate),2),array_fill(0,count($acdate),'0'),array_fill(0,count($acdate),STR_PAD_LEFT))); //str_pad($input, 10, "-=", STR_PAD_LEFT);
										}
										// "session_finished='n' AND NOW()<=STR_TO_DATE(CONCAT(session_start_year,'-',LPAD(session_start_month,2,'00'),'-',LPAD(session_start_day,2,'00'),'-',LPAD(session_start_hour,2,'00'),'-',LPAD(session_start_minute,2,'00')),'%Y-%m-%d-%H-%i')"
                                        $title=lang('subjects').' '.$datetoshow;
                                        break;
            case "absentatdate":    
										$cdate=empty($_REQUEST['date'])?date('j').".".date('n').".".date('Y'):$_REQUEST['date'];
										if(!empty($focus2part)) $cdate=$focus2part;
										$cdates=explode('-',$cdate);
										$where_clause="AND registered='y' AND shownup='n' AND participated='n' ";
										$datetoshow="";
										for($i=0; $i<count($cdates); $i++)
										{
											$acdate=explode('.',$cdates[$i]);
											if(count($acdate)<2) $acdate=explode('/',$cdates[$i]);
											if(count($acdate)<2) $acdate[]=date('n');
											if(count($acdate)<3) $acdate[]=date('Y');
											$mysqlsessdate="STR_TO_DATE(CONCAT(session_start_year,'-',LPAD(session_start_month,2,'00'),'-',LPAD(session_start_day,2,'00'),'-',LPAD(session_start_hour,2,'00'),'-',LPAD(session_start_minute,2,'00')),'%Y-%m-%d-%H-%i')";
											$mysqldate1="STR_TO_DATE('".$acdate[2]."-".$acdate[1]."-".$acdate[0]."-00-01','%Y-%m-%d-%H-%i')";
											$mysqldate2="STR_TO_DATE('".$acdate[2]."-".$acdate[1]."-".$acdate[0]."-23-59','%Y-%m-%d-%H-%i')";
											if(count($cdates)==1) $where_clause.="AND session_start_year='".$acdate[2]."' AND session_start_month='".$acdate[1]."' AND session_start_day='".$acdate[0]."'";
											elseif($i==0) {$supsess_criteria.="AND ".$mysqlsessdate.">=".$mysqldate1." "; $where_clause.=$supsess_criteria;}
											else {$supsess_criteria.="AND ".$mysqlsessdate."<=".$mysqldate2." "; $where_clause.=$supsess_criteria;}
											if($i>0) $datetoshow.=" - ";
											$datetoshow.=implode("/",array_map("str_pad",$acdate,array_fill(0,count($acdate),2),array_fill(0,count($acdate),'0'),array_fill(0,count($acdate),STR_PAD_LEFT))); //str_pad($input, 10, "-=", STR_PAD_LEFT);
										}
										$where_clause="AND registered='y' AND shownup='n' AND participated='n' AND session_start_year='".$acdate[2]."' AND session_start_month='".$acdate[1]."' AND session_start_day='".$acdate[0]."'";
                                        $title=lang('subjects_absent').' '.$datetoshow;
                                        break;
            case "atmonth":    
										$cdate=date('n').".".date('Y');
										if(!empty($_REQUEST['date'])) $cdate=$_REQUEST['date'];
										if(!empty($_REQUEST['month'])) $cdate=$_REQUEST['month'];
										if(!empty($focus2part)) $cdate=$focus2part;
										$acdate=explode('.',$cdate);
										if(count($acdate)<2) $acdate=explode('/',$cdate);
										if(count($acdate)<2) $acdate[]=date('Y');
										$where_clause="AND registered='y' AND session_start_year='".$acdate[1]."' AND session_start_month='".$acdate[0]."'";
                                        $title=lang('subjects').' '.implode("/",$acdate);
                                        break;
            case "tomorrow":    
										$where_clause="AND registered='y' AND session_start_year='".date('Y', strtotime('+1 day'))."' AND session_start_month='".date('n', strtotime('+1 day'))."' AND session_start_day='".date('j', strtotime('+1 day'))."'";
                                        $title=lang('subjects').' '.date('d/m/Y', strtotime('+1 day'));
                                        break;
			case "todaynew":    
										$where_clause="AND registered='y' AND shownup='n' AND participated='n'  AND session_finished='n' AND session_start_year='".date('Y')."' AND session_start_month='".date('n')."' AND session_start_day='".date('j')."'";
                                        $title=lang('subjects_new').' '.date('d/m/Y');
                                        break;
			case "todayopen":    
										$where_clause="AND registered='y' AND session_finished='n' AND session_start_year='".date('Y')."' AND session_start_month='".date('n')."' AND session_start_day='".date('j')."'";
                                        $title=lang('subjects_new').' '.date('d/m/Y');
                                        break;
			case "todayopenshownup":    
										$where_clause="AND registered='y' AND shownup='y' AND session_finished='n' AND session_start_year='".date('Y')."' AND session_start_month='".date('n')."' AND session_start_day='".date('j')."'";
                                        $title=lang('subjects_new').' '.date('d/m/Y');
                                        break;
			case "todayshownup":    
										$where_clause="AND registered='y' AND shownup='y' AND session_start_year='".date('Y')."' AND session_start_month='".date('n')."' AND session_start_day='".date('j')."'";
                                        $title=lang('subjects_new').' '.date('d/m/Y');
                                        break;
			case "todayabsent":    
										$where_clause="AND registered='y' AND shownup='n' AND participated='n' AND session_start_year='".date('Y')."' AND session_start_month='".date('n')."' AND session_start_day='".date('j')."'";
                                        $title=lang('subjects_absent').' '.date('d/m/Y');
                                        break;
			case "all":	$where_clause=""; //AND (registered='n' OR registered='y')
                                        $title=lang('assigned_subjects');
                                        break;
		} 
	$title=lang('payments');

	$select_query=" SELECT * FROM ".table('participants').", ".table('participate_at').", 
					".table('sessions')."
			WHERE ".table('participants').".participant_id=".
					table('participate_at').".participant_id 
			AND ".table('sessions').".session_id=".table('participate_at').".session_id 
			AND ".table('participate_at').".payment_file='".$dossier."' ";
	if ($session_id) $select_query.=" AND ".table('participate_at').".session_id='".$session_id."' "; 
	if(!empty($_REQUEST['restricted_plist']))  $select_query.=" AND ".table('participate_at').".participant_id IN (".$_REQUEST['restricted_plist'].") ";
	if(!empty($_REQUEST['payment_mode']) && $_REQUEST['payment_mode']=='view' && !empty($_REQUEST['pay']) && !isset($_REQUEST['only_empty']))  
				$select_query.=" AND (".table('participate_at').".payment<>'' OR ".table('participate_at').".payment_num<>'' OR ".table('participate_at').".payment_file<>'' OR ".table('participate_at').".payment_nlf<>'')";
	if(!empty($_REQUEST['pay']) && !empty($_REQUEST['only_empty'])) {
				$comp_sign=($_REQUEST['only_empty']>0)?'=':'<>';
				$select_query.=" AND (".table('participate_at').".payment".$comp_sign."''";
				if($_REQUEST['only_empty']<0) $select_query.=" OR ".table('participate_at').".payment_nlf".$comp_sign."''";
				$select_query.=")";
	}
	if(!empty($_REQUEST['payment_mode']) && $_REQUEST['payment_mode']!='view' && !empty($_REQUEST['pay']) && !empty($_REQUEST['dossier']))
				$select_query.=" AND (".table('participate_at').".payment_file='".$_REQUEST['dossier']."' OR ".table('participate_at').".payment_file='' OR ".table('participate_at').".payment_file IS NULL)";
    $select_query.=$where_clause." ORDER BY ".$order;


	echo '
		<center>
		<BR>
			<h4>'.lang('payment_file').' '.dossier_name($dossier,2).'</h4>
			<h4 style="text-decoration:underline">'.$title.'</h4>

		<P align=right style="margin-right:5%">';
		if(empty($_REQUEST['pay'])) echo '
			<A class="small" HREF="experiment_participants_show_pdf.php?experiment_id='.
				$experiment_id.'&session_id='.$session_id.'&focus='.$focus.
				'" target="_blank">'.lang('print_version').'</A>&nbsp;&nbsp;&nbsp;';
		/*if(empty($_REQUEST["disabled"]) && empty($_REQUEST['pay'])) echo '
				<br><br>
				<A class="small" HREF="'.$_SERVER['PHP_SELF'].'?experiment_id='.
				$experiment_id.'&session_id='.$session_id.'&focus='.$focus.
				'&disabled=1" ><small>disabled mode&nbsp;&nbsp;&nbsp;&nbsp;</small></A>';
		else echo '
				<br><br>
				<A class="small" HREF="'.$_SERVER['PHP_SELF'].'?experiment_id='.
				$experiment_id.'&session_id='.$session_id.'&focus='.$focus.
				'" ><small>normal mode&nbsp;&nbsp;&nbsp;&nbsp;</small></A>';*/
		$normal_link_start=$_SERVER['PHP_SELF'].'?experiment_id='.$experiment_id.'&session_id='.$session_id;
		$normal_link=$normal_link_start.'&focus='.$focus;
		$subjects_shortcuts=array();
		$subjects_shortcuts["shownup"]=lang('shownup_subjects');
		$subjects_shortcuts["participated"]=lang('subjects_participated');
		$subjects_shortcuts["registered"]=lang('registered_subjects');
		//$subjects_shortcuts["nonparticipated"]=lang('nonparticipated');
		unset($subjects_shortcuts[$focus]);
		if(empty($_REQUEST["disabled"]) && empty($_REQUEST['pay']))  {
			echo '
				<br><br>Other subpools : ';
			foreach ($subjects_shortcuts as $shk=>$sh) {
				echo ' <A class="small" HREF="'.$normal_link_start.'&focus='.$shk.'" >'.$sh.'</A>&nbsp;&nbsp;&nbsp;&nbsp; ';
			}
		}
		
		$payment_links=array("entry/edit mode"=>$normal_link.'&pay=1&payment_mode=entry',
				"entry mode (only empty)"=>$normal_link.'&pay=1&payment_mode=entry&only_empty=1',
				"edit mode (only non empty)"=>$normal_link.'&pay=1&payment_mode=entry&only_empty=-1',
				"view mode"=>$normal_link.'&pay=1&payment_mode=view');
		/*echo '
				<br><br>'.lang('payment').' : ';
		foreach ($payment_links as $lk=>$pl)
			echo ' <A class="small" HREF="'.$pl.'" >'.$lk.'</A>&nbsp;&nbsp;&nbsp;&nbsp; ';*/
		if(!empty($_REQUEST['pay']) && $payment_mode=="view") {
			echo '
					<br><br>View mode : ';
			$view_links=array();
			$veiwbase=$normal_link.'&dossier='.$dossier;
			// if(!isset($_REQUEST['only_empty']) || $_REQUEST['only_empty']<0) $view_links[]=array("show empty"=>$veiwbase.'&only_empty=0');
			// if(isset($_REQUEST['only_empty']) && $_REQUEST['only_empty']>=0) $view_links[]=array("hide empty"=>$veiwbase);
			// foreach($view_links[0] as &$ar) if(!empty($_REQUEST['show_emails'])) $ar.='&show_emails='.$_REQUEST['show_emails'];
			if(empty($_REQUEST['show_emails'])) $view_links[]=array("show emails"=>$veiwbase.'&show_emails=1');
			else $view_links[]=array("hide emails"=>$veiwbase.'');
			if(empty($_REQUEST['show_session_info'])) $view_links[]=array(lang("show_session_info",false)=>str_replace("focus=all","focus=registered",$veiwbase).'&show_session_info=1');
			else $view_links[]=array(lang("hide_session_info",false)=>str_replace("focus=registered","focus=all",$veiwbase).'');
			// foreach($view_links[1] as &$ar) if(isset($_REQUEST['only_empty'])) $ar.='&only_empty='.$_REQUEST['only_empty'];
			foreach($view_links[0] as &$ar) if(!empty($_REQUEST['show_session_info'])) $ar.='&show_session_info='.$_REQUEST['show_session_info'];
			foreach($view_links[1] as &$ar) if(!empty($_REQUEST['show_emails'])) $ar.='&show_emails='.$_REQUEST['show_emails'];
			foreach ($view_links as $view_links_inner) foreach ($view_links_inner as $lk=>$pl)
				echo ' <A class="small" HREF="'.$pl.'" >'.$lk.'</A>&nbsp;&nbsp;&nbsp;&nbsp; ';
			if(!$transfer_mode && check_allow('experiment_edit_participants')) echo '<A class="small" HREF="'.$_SERVER['PHP_SELF'].'?dossier='.$dossier.'&transfer_mode=1#transfer_mode_anchor" >'.lang('transfer_subjects_to_another_payment_file').'</A>&nbsp;&nbsp;&nbsp;&nbsp;';
			if($transfer_mode) echo '<A class="small" HREF="'.$_SERVER['PHP_SELF'].'?dossier='.$dossier.'" >'.lang('standard_mode').'</A>&nbsp;&nbsp;&nbsp;&nbsp;';
		}
		echo '</P>';

	if(!empty($_REQUEST['pay'])) {
		$_REQUEST["disabled"]="1";
		if(empty($dossier) && $payment_mode=="entry") {
			echo '<CENTER>
				<FORM name="choisir_dossier" method=post action="'.thisdoc().'">
			
				<BR>';
			if(lang('lang')=='fr') echo "<h2>Veuillez choisir le dossier de paiement :</h2>";
			else echo "<h2>Please choose the payment file:</h2>";
			echo '<INPUT name="payment_mode" type=hidden value="'.$payment_mode.'">';
			echo '
				<INPUT name="pay" type=hidden value="'.$_REQUEST['pay'].'">
				<INPUT type=hidden name="focus" value="'.$focus.'">
				<INPUT type=hidden name="experiment_id" value="'.$experiment_id.'">
				<INPUT type=hidden name="session_id" value="'.$session_id.'">';
			if(!empty($_REQUEST['only_empty'])) echo '
				<INPUT name="only_empty" type=hidden value="'.$_REQUEST['only_empty'].'">';
			// if(!isset(lang('edit_payment_files'))) lang('edit_payment_files')=(lang('lang')=='fr')?"Modifier les dossiers de paiement":"Edit payment files";
			echo '
				<table border=0 style="text-align:center"><tr>
				<td></td>
				</tr><tr>
				<td>';
			echo helpers__select_payment_file($experiment_id,$session_id);
			echo '
				</td>
				</tr></table>
			<INPUT type=submit name="choose_payment_file" value="OK">
				<br><br><a target=_blank href="dossiers_paiement_main.php">'.lang('edit_payment_files').'</a>
				</FORM>
					';
		echo '<BR><BR>
			<A href="'.thisdoc().'?experiment_id='.$experiment_id.'&session_id='.$session_id.'&focus='.$focus.'">'.icon('back').' '.lang('back').'</A><BR><BR>
			';
			echo '  <A HREF="experiment_show.php?experiment_id='.$experiment_id.'">
							'.lang('mainpage_of_this_experiment').'</A><BR><BR>

					</CENTER>';
			include ("footer.php");
			exit;
		}
	}
	// show query
	echo '	<P class="small">Query: '.$select_query.'</P>';
	if(!empty($dossier) && $payment_mode=='entry') echo '<h3>Dossier : '.dossier_name($dossier,1).'</h3>';

	// get result
	$result=mysqli_query($GLOBALS['mysqli'],$select_query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
	

        $participants=array(); $plist_ids=array(); $noncash_names=array(); $findnoncash=($nlfpay_name=="");
        while ($line=mysqli_fetch_assoc($result)) {
				if($findnoncash && !empty($line['payment_nlf']) && !empty($line['payment_file'])) {
					$nlfpay_name=lang('non_cash',false);
					$query="SELECT * FROM ".table('payment_files')." WHERE id=".$line['payment_file'];
					$nlfline=orsee_query($query);
					if(!empty($nlfline['nonliquid_funds_name']) && !in_array($nlfline['nonliquid_funds_name'],$noncash_names)) $noncash_names[]=$nlfline['nonliquid_funds_name'];
				}
                $participants[]=$line;
		$plist_ids[]=$line['participant_id'];
                }
		if($findnoncash && $nlfpay_name==lang('non_cash',false)) $nlfpay_name=implode(', ',$noncash_names);
	$_SESSION['plist_ids']=$plist_ids;
	$result_count=count($participants);
	
	$critical_count=300;
	$critical_count_apply=(($registered || $shownup || $participated || $nonparticipated || $notpresent || $absent || $todayabsent || $absentold || $absentnew || $new || $today || $todaynew || $todayopen || $todaystarted || $atdate || $absentatdate || $atmonth || $todayopen || $todayopenshownup || $todayshownup || $tomorrow) && $result_count>$critical_count);

	$nr_normal_columns=2;
	// form
	$spent=$dossier_stat_line['pay_cash']+$dossier_stat_line['pay_noncash'];
	$margin_left="10%";
	echo '<h5 align=left style="margin-left:'.$margin_left.'">'.lang('payment_file').' : '.dossier_name($dossier,2).'</h5>';
	echo '<p align=left style="margin-left:'.$margin_left.'">'.lang('initial_budget').': '.$dossier_line['budget'].' '.lang('money_symbol').'</p>';
	echo '<p align=left style="margin-left:'.$margin_left.'">'.lang('total_payment').': '.$spent.' '.lang('money_symbol').'</p>';
	echo '<p align=left style="margin-left:'.$margin_left.'">'.lang('residual').': '.($dossier_line['budget']-$spent).' '.lang('money_symbol').'</p>';
	echo '
		<FORM name="part_list" method=post action="'.thisdoc().'">
	
		<BR>
		<table border=1 style="border-collapse:collapse">
			<TR>';
				 echo '<TD class="small"></TD>';
				foreach($columns as $c) {
                   	if($c['allow_sort'] && !empty($_REQUEST['pay'])) 
					{
						$supvals="&pay=".$_REQUEST['pay']."&payment_mode=".$payment_mode;
						$supvals.="&dossier=".$_REQUEST['dossier']; //if($payment_mode=="entry") 
						if(!empty($_REQUEST['show_emails'])) $supvals.="&show_emails=".$_REQUEST['show_emails'];
						if(isset($_REQUEST['only_empty'])) $supvals.="&only_empty=".$_REQUEST['only_empty'];
						if(isset($_REQUEST['show_session_info'])) $supvals.="&show_session_info=".$_REQUEST['show_session_info'];
						headcell($c['column_name'],$c['sort_order'],"",$supvals);
					}
					elseif($c['allow_sort']) headcell($c['column_name'],$c['sort_order']);
                   	else headcell($c['column_name']);
                   	$nr_normal_columns++;
                }
				if(empty($_REQUEST['pay'])) headcell(lang('noshowup'),"number_noshowup,number_reg");
				else {
					$sum_payment=0;
					// "payment_num","payment_file"
					$supvals="&pay=".$_REQUEST['pay']."&payment_mode=".$payment_mode;
					if(!empty($_REQUEST['dossier'])) $supvals.="&dossier=".$_REQUEST['dossier'];
					if(!empty($_REQUEST['show_emails'])) $supvals.="&show_emails=".$_REQUEST['show_emails'];
					if(isset($_REQUEST['only_empty'])) $supvals.="&only_empty=".$_REQUEST['only_empty'];
					if(isset($_REQUEST['show_session_info'])) $supvals.="&show_session_info=".$_REQUEST['show_session_info'];
					$payment_supname="";
					if($nlfpay_name!="") {
						$payment_supname.=" (".lang('cash',false).")";
						$sum_payment_nlf=0;
					}
					headcell(lang('payment').$payment_supname,"payment","",$supvals);
					if($nlfpay_name!="") headcell(lang('payment').' ('.$nlfpay_name.')',"payment_nlf","",$supvals);
					headcell(lang('payment_num'),"payment_num","",$supvals);
					if($payment_mode=="view" && empty($dossier)) headcell(lang('payment_file'),"payment_file","",$supvals);
				}
			if ($assigned || $invited) {
				headcell(lang('invited'),"invited","invited");
				headcell(lang('register'));
				}
			
			if (true) { //if ($registered || $shownup || $participated || $nonparticipated || $notpresent || $absent || $todayabsent || $absentold || $absentnew || $new || $today || $todaynew || $todayopen || $todaystarted || $atdate || $absentatdate || $atmonth || $todayopen || $todayopenshownup || $todayshownup || $tomorrow)
				if(!isset($supvals)) $supvals="";
				// var_dump($payment_mode);
				if(isset($payment_mode) && $payment_mode=="view") headcell('Date');
				if(!empty($_REQUEST['show_session_info'])) {
					headcell(lang('session'),table("sessions").".session_id,lname,fname","session",$supvals);
					headcell(lang('shownup'),"shownup","shownup",$supvals);
					headcell(lang('participated'),"participated","participated",$supvals);
				}
				if($showrules) headcell(lang('rules_signed'));
				if($critical_count_apply && !$transfer_mode) headcell("Check to restrict");
				if($transfer_mode) {headcell(''); headcell(lang('check_for_transfer'));}
				}
	echo '		</TR>';



	$shade=false;

	$part_ids=array();
	$emails=array();

	$pnr=0;

	if (check_allow('experiment_edit_participants') && empty($_REQUEST["disabled"])) $disabled=false; else $disabled=true;

        foreach ($participants as $p) {

		$emails[]=$p['email'];
		$pnr++;

		echo '<tr class="small"';
                        if ($shade) echo ' bgcolor="'.$color['list_shade1'].'"';
                               else echo ' bgcolor="'.$color['list_shade2'].'"';
                echo '>';

	        echo '	<td class="small">';
				if($count_firstcol) echo $pnr; 
				echo '<INPUT name="pid'.$pnr.'" id="pid'.$pnr.'" type=hidden value="'.$p['participant_id'].'">';
			echo '</td>';
			foreach($columns as $c) {
               	echo '<td class="small">';
               	if($c['link_as_email_in_lists']=='y') echo '<A class="small" HREF="mailto:'.
               		$p[$c['mysql_column_name']].'">';
               	if(preg_match("/(radioline|select_list|select_lang)/",$c['type']) && isset($c['lang'][$p[$c['mysql_column_name']]]))
               		echo $c['lang'][$p[$c['mysql_column_name']]];
               	else {
					$towrite=$p[$c['mysql_column_name']];
					if($c['mysql_column_name']=='lname' || $c['mysql_column_name']=='fname') $towrite=str_replace('?','',$towrite);
					echo $towrite;
				}
               	if($c['link_as_email_in_lists']=='y') '</A>';
               	echo '</td>';
            }
			if(empty($_REQUEST['pay']))  echo '
			<td class="small">'.$p['number_noshowup'].
                                                '/'.$p['number_reg'].'</td>';
			else {
				if($payment_mode!="entry" && $payment_mode!="edit") echo '<td class="small" style="text-align:center"><input type=hidden name=payment_'.$pnr.' id=payment_'.$pnr.' value="'.$p['payment'].'">'.$p['payment'].'</td>';
				else echo '<td class="small"><input type=text onBlur="payment_num(this)" name=payment_'.$pnr.' id=payment_'.$pnr.' value="'.$p['payment'].'"><input type=hidden name=payment_num_'.$pnr.' id=payment_num_'.$pnr.' value="'.$p['payment_num'].'"></td>';
				$sum_payment+=$p['payment'];
				if($nlfpay_name!="") {
					if($payment_mode!="entry" && $payment_mode!="edit") echo '<td class="small" style="text-align:center"><input type=hidden name=payment_nlf_'.$pnr.' id=payment_nlf_'.$pnr.' value="'.$p['payment_nlf'].'">'.$p['payment_nlf'].'</td>';
					else echo '<td class="small"><input type=text onBlur="payment_num(this)" name=payment_nlf_'.$pnr.' id=payment_nlf_'.$pnr.' value="'.$p['payment_nlf'].'"></td>';	
					$sum_payment_nlf+=$p['payment_nlf'];
				}
				echo '<td class="small" style="text-align:center" id="td_payment_num_'.$pnr.'">'.$p['payment_num'].'</td>';				
				if($payment_mode=="view" && empty($dossier)) echo '<td class="small">'.dossier_name($p['payment_file'],1).'</td>';				
			}

		if ($assigned || $invited) {
			echo '<td class="small">'.$p['invited'].'</td>
			      <td class="small">';
				  if(!$disabled && empty($_REQUEST['restricted_plist'])) {
					echo '
					<INPUT type=checkbox id="chkbx'.$pnr.'" name="reg'.$pnr.'" value="y"';
					if ((isset($_REQUEST['reg'.$pnr]) && $_REQUEST['reg'.$pnr]=="y") || !empty($_REQUEST['restricted_plist']) ) echo ' CHECKED';
					if ($disabled) echo ' DISABLED';
					echo '>';
					} elseif(isset($_REQUEST['reg'.$pnr])) echo $_REQUEST['reg'.$pnr]; else echo '?';
				echo '
				</td>';
				// Edit added by Maxim Frolov 
				if((empty($_REQUEST["disabled"]) && !(isset($_REQUEST["editlink"]) && empty($_REQUEST["editlink"])))  || !empty($_REQUEST["editlink"])) echo '<td><a target=_blank href="participants_edit.php?participant_id='.$p["participant_id"].'">Edit</td>';
			}
	   	if (true) { //if ($registered || $shownup || $participated || $nonparticipated || $notpresent || $absent || $todayabsent || $absentold || $absentnew || $new || $today || $todaynew || $todayopen || $todaystarted || $atdate || $absentatdate || $atmonth || $todayopen || $todayopenshownup || $todayshownup || $tomorrow)
	   		echo '<td class="small">';
				if(isset($payment_mode) && $payment_mode=="view") {
					echo time__format(lang('lang'),time__load_session_time($p),false,true,true,false);
					echo '
					</td>
					<td class="small">
					';
				}
			if(!empty($_REQUEST['show_session_info'])) {
				if (!$session_id) {
					echo '<INPUT type=hidden name="csession'.$pnr.'" value="'.$p['session_id'].'">';
					}
					if ($disabled) {if(isset($payment_mode) && $payment_mode=="view") {
						echo '<a target=_blank href="experiment_show.php?experiment_id='.$p['experiment_id'].'">
							'.experiment__get_public_name($p['experiment_id']).'</a> :
							<a target=_blank href="experiment_participants_show.php?experiment_id='.$p['experiment_id'].'&focus=registered&session_id='.$p['session_id'].'">
							'.session__build_name($p,"",true).'</a>'; 
					}
					else echo session__build_name($p);
					}
					   else echo select__sessions($p['session_id'],'session'.$pnr,$experiment_id,false);
				echo '</td>
					<td class="small">';
				
				$disable_check=$disabled; //($disabled && !(!empty($_REQUEST['pay']) && !empty($payment_mode) && ($payment_mode=="entry" || $payment_mode=="edit") ) && ($registered || $nonparticipated || $notpresent || $absent || $todayabsent || $absentold || $absentnew || $new || $today || $todaynew || $todayopen || $todaystarted || $atdate || $absentatdate || $atmonth || $todayopen || $todayopenshownup || $todayshownup || $tomorrow));// !:  && (empty($_REQUEST['only_empty']) || $_REQUEST['only_empty']<0)
					if (!$disable_check && ($registered || $shownup || $nonparticipated || $notpresent || $absent || $todayabsent || $absentold || $absentnew || $new || $today || $todaynew || $todayopen || $todaystarted || $atdate || $absentatdate || $atmonth || $todayopen || $todayopenshownup || $todayshownup || $tomorrow)) {
								echo '<INPUT type=checkbox id="shup'.$pnr.'" name="shup'.$pnr.'" value="y" 
						onclick="checkshup('.$pnr.')"'; 
									if ($p['shownup']=="y") echo ' CHECKED';
					if ($disable_check) echo ' DISABLED';
					echo '>';
					}
							   else echo $p['shownup'];

				echo '</td>
					<td class="small">';
						
					  if(!$disable_check) {
						echo '
									<INPUT type=checkbox id="part'.$pnr.'" name="part'.$pnr.'" value="y" 
							onclick="checkpart('.$pnr.')"'; 
										if ($p['participated']=="y") echo ' CHECKED';
						if ($disable_check) echo ' DISABLED';
									echo '>';
						} elseif(!empty($p['participated'])) echo $p['participated']; else echo '?';
					echo '
					</td>';
				if($showrules) echo '
					<td class="small">';
				//if ($session_id) {
						if($showrules && !$disabled) {
							echo '<INPUT type=checkbox name="rules'.$pnr.'" value="y"';
										if ($p['rules_signed']=="y") echo ' CHECKED';
							if ($disabled) echo ' DISABLED';
							echo '>';
						}
						if($showrules && $disabled) echo $p['rules_signed'];
				//		}
				//	   else {
				//		if ($p['rules_signed']=="y") echo lang('yes'); 
				//					else echo lang('no_emp');
				//		}
				if($showrules) echo '</td>';
			}
				
			if($critical_count_apply && !$transfer_mode) {
				echo '
				<td class="small">
        	            <INPUT type=checkbox id="chkbx'.$pnr.'" name="restrict'.$pnr.'" value="y" 
						onclick="checkpart('.$pnr.')"'; 
					if ($disable_check) echo ' DISABLED';
                	echo '>
	   			</td>';
			}
			
			if($transfer_mode) {
				echo '
				<td class="small">
        	            <INPUT type=checkbox id="chkbx'.$pnr.'" name="restrict'.$pnr.'" value="y" 
						onclick="checkpart4transfer(event,'.$pnr.')"'; 
					if ($disable_check) echo ' DISABLED';
                	echo '>
	   			</td>';
			}
			

				
			// Edit added by Maxim Frolov 
				if((empty($_REQUEST["disabled"]) && !(isset($_REQUEST["editlink"]) && empty($_REQUEST["editlink"])))  || !empty($_REQUEST["editlink"])) echo '<td><a target=_blank href="participants_edit.php?participant_id='.$p["participant_id"].'">Edit</td>';
			}
		echo '</tr>';
		if ($shade) $shade=false; else $shade=true;
		}
		if(!empty($_REQUEST['pay']) && $payment_mode=="view"){
			$n_cols_before_pay=3;
			if(!empty($_REQUEST['show_emails'])) $n_cols_before_pay++;
			echo '<tr><td colspan='.$n_cols_before_pay.' style="text-align:right;border-top:2px solid black">Total :</td>';
			echo '<td style="border-top:2px solid black;text-align:center">'.$sum_payment.'</td>';
			if($nlfpay_name!="") echo '<td style="border-top:2px solid black;text-align:center">'.$sum_payment_nlf.'</td>';
			echo '</tr>';
			if($nlfpay_name!="") echo '<tr><td colspan='.$n_cols_before_pay.' style="text-align:right;border-top:2px solid black"><u>Total :</u></td>
								<td colspan=2 style="text-align:center;border-top:2px solid black"><u>'.($sum_payment+$sum_payment_nlf).'</u></td></tr>'; 
		}



function form__check_all($name,$count,$second_sel_name="",$second_un_name="") {
	global $lang;
	echo '<center>';
	echo '<input type=button value="'.lang('select_all').'" ';
	echo 'onClick="checkAll(\''.$name.'\','.$count.')';
	if ($second_sel_name) echo ';checkAll(\''.$second_sel_name.'\','.$count.')';
	echo '"><br>';
	echo '<input type=button value="'.lang('select_none').'" ';
	echo 'onClick="uncheckAll(\''.$name.'\','.$count.')';
	if ($second_un_name) echo ';uncheckAll(\''.$second_un_name.'\','.$count.')';
	echo '"></center>';
}

	if(!(!empty($_REQUEST['payment_mode']) && $_REQUEST['payment_mode']=='view' && !empty($_REQUEST['pay'])))
	if (check_allow('experiment_edit_participants')) {
	if(!$disable_check) { //empty($_REQUEST['disabled'])
	// table footer ...
	if($disabled) {$nr_normal_columns++; if(!empty($nlfpay_name)) $nr_normal_columns++; }
	echo '	<TR>
			<TD colspan='.$nr_normal_columns.'></td>';

		if ($assigned || $invited) {
			echo '<TD></TD><TD>';
			form__check_all('reg',$result_count);
			echo '</TD>';
			}
	
		if ($registered || $shownup || $participated || $nonparticipated || $notpresent || $absent || $todayabsent || $absentold || $absentnew || $new || $today || $todaynew || $todayopen || $todaystarted || $atdate || $absentatdate || $atmonth || $todayopen || $todayopenshownup || $todayshownup || $tomorrow) {
			echo '<TD></TD>
				<TD>';
			if ($registered || $shownup || $nonparticipated || $notpresent || $absent || $todayabsent || $absentold || $absentnew || $new || $today || $todaynew || $todayopen || $todaystarted || $atdate || $absentatdate || $atmonth || $todayopen || $todayopenshownup || $todayshownup || $tomorrow) 
				form__check_all('shup',$result_count,'','part');
			echo '</TD>
				<TD>';
                		form__check_all('part',$result_count,'shup','');
			echo '</TD>
				<TD>';
			if ($showrules && $session_id)
				form__check_all('rules',$result_count);
			echo '</TD>';
			}
	echo '	</TR>
		</table>';


	echo '	<TABLE border=0>
			<TR>
				<TD>&nbsp;</TD>
			</TR>';
	if ($assigned || $invited) {
		if(!isset($_REQUEST['to_session'])) $_REQUEST['to_session']="";
		if(!isset($_REQUEST['check_if_full'])) $_REQUEST['check_if_full']="";
		if(!isset($_REQUEST['remember'])) $_REQUEST['remember']="";
		echo '	<TR>
				<TD>
					'.lang('register_marked_for_session').' ';
					echo select__sessions($_REQUEST['to_session'],'to_session',$experiment_id,false);
			echo '	</TD>
			</TR>
			<TR>
				<TD>
					'.lang('check_for_free_places_in_session').'
					<INPUT type=checkbox name="check_if_full" value="true"';
					if ($_REQUEST['check_if_full'] || ! $_REQUEST['remember']) 
						echo ' CHECKED';
					echo '>
				</TD>
			</TR>';
			}
		}
		else echo '	</table><br><TABLE border=0>';
	echo '	<TR>
			<TD align=center>';
	if(!empty($_REQUEST['pay'])) {
			echo '
			<INPUT name="payment_mode" type=hidden value="'.$payment_mode.'">
			<INPUT name="dossier" type=hidden value="'.$_REQUEST['dossier'].'">
			<INPUT name="pay" type=hidden value="'.$_REQUEST['pay'].'">';
		if(isset($_REQUEST['only_empty'])) echo '
			<INPUT name="only_empty" type=hidden value="'.$_REQUEST['only_empty'].'">';
	}
	echo '
				<INPUT type=hidden name="result_count" value="'.$result_count.'">
				<INPUT type=hidden name="focus" value="'.$focus.'">
				<INPUT type=hidden name="experiment_id" value="'.$experiment_id.'">
				<INPUT type=hidden name="session_id" value="'.$session_id.'">
				<INPUT type=submit name="change" id="change" value="'.lang('change').'">
			</TD>
		</TR>';
	}

	echo '</table>';
	echo '<p align=left style="margin-left:'.$margin_left.'"> N '.lang('participants').' : '.$result_count.'</p>';
	echo '
		</form>';
	if($transfer_mode) {
		echo '<div id="transfer_bottom" style="border: 1px solid blue"><a name="transfer_mode_anchor"></a><div id="message_duplicate" style="display:none"></div><br><br><form name="move_selected" onsubmit="move_finilize()">
		<input id="n_tomove" name="n_tomove" type=hidden value=0/>
		<input id="transfer_mode" name="transfer_mode" type=hidden value=1/>
		<input id="orig_min_paynum" name="orig_min_paynum" type=hidden value=0/>
		<input id="orig_max_paynum" name="orig_max_paynum" type=hidden value=0/>
		<input id="sum_tomove" name="sum_tomove" type=hidden value=0/>
		<input id="paynums_tomove" name="paynums_tomove" type=hidden value="" />
		<input id="ids_tomove" name="ids_tomove" type=hidden value="" />
		<input id="dossier" name="dossier" type=hidden value="'.$dossier.'"/>
		<input id="move_to" name="move_to" type=hidden value="0"/>
		<div id=info_selected>'.lang('n_selected').' : <span id="info_nselected" style="color:blue">0</span>. '.lang('sum_selected').' : <span id="info_sumselected" style="color:blue;text-decoration:underline">0</span> '.lang('money_symbol').' </div>
		<hr width=10%/>
		<div id="select_action">'.lang('select_participants').' : <input type=button value="'.lang('select_all').'" onclick="select_all(1)"/><input type=button value="'.lang('select_none').'" onclick="select_all(-1)"/>,
		&nbsp;&nbsp;&nbsp;'.lang('reach_the_sum').'  <input type=number id="up_to_sum" size=10 style="width:70px" />'.lang('money_symbol').' <input type=button value="OK" id="ok_uptosum" onclick="select_up_to_sum()"/>, <select name="uptosum_sel" id="uptosum_sel"><option value=1>'.lang("go_forward_from_top",false).'</option><option value=-1 selected>'.lang("go_backward_from_bottom", false).'</option></select>
		</div>
		<hr width=10%/><br>
		<div id="transfer_dossier" style="display:block">'.lang('move_selected_participants_to').' '.lang('payment_file',false).' '.helpers__select_payment_file('','','dossier_new').' <input type=submit id=dossier_submit value="'.lang("make_transfer").'" disabled /></div>
		<br><div id="reminder_current_payment_file">'.lang('reminder').' : '.lang('the_current_payment_file_is', false).' '.dossier_name($dossier,1).'</div>
		</form><br><br></div>';
	}

	if ($assigned || $invited || $critical_count_apply) {
		$alert_count="<br><br>"; $alert_count_sup="";
		if($critical_count_apply) $alert_count_sup="(by the last checkbox column)";
		if($result_count>$critical_count) $alert_count="<strong style='color:#F59'>More than $critical_count participants, restrict $alert_count_sup before changing!</strong><br>";
		echo $alert_count.'
		<form name="part_list_restricted" method=post  action="'.thisdoc().'">
			<INPUT type=hidden name="focus" value="'.$focus.'">
			<INPUT type=hidden name="experiment_id" value="'.$experiment_id.'">
			<INPUT type=hidden name="session_id" value="'.$session_id.'">
			<INPUT type=hidden name="restricted_plist" id="restricted_plist" value="">
			<INPUT type=submit name="restricted" value="Restrict for checked" onclick="restrict_checked()">
		</form>
		<script>
			function restrict_checked()
			{
				var res="";
				var ich=0;
				for(var i=1; i<='.$result_count.'; i++) {
					if(document.getElementById("chkbx"+i).checked) {
						if(ich>0) res+=",";
						res+=document.getElementById("pid"+i).value;
						ich++;
					}
				}
				document.getElementById("restricted_plist").value=res;
				//alert(document.getElementById("restricted_plist").value);
			}
		</script>
		';
	}
	
	
	echo '
		<BR>
		<TABLE width="80%" border=0>
		<TR>
			<TD>';
				if(empty($_REQUEST['pay']))
	 			if ($session_id && $session['session_finished']!="y" && check_allow('session_send_reminder')) {
                                        if ($session['reminder_sent']=="y") {
                                                $state=lang('session_reminder_state__sent');
                                                $statecolor=$color['session_reminder_state_sent_text'];
						$explanation=lang('session_reminder_sent_at_time_specified');
						$send_button_title=lang('session_reminder_send_again');
                                                }
                                        elseif ($session['reminder_checked']=="y" && $session['reminder_sent']=="n") {
                                                $state=lang('session_reminder_state__checked_but_not_sent');
                                                $statecolor=$color['session_reminder_state_checked_text'];
						$explanation=lang('session_reminder_not_sent_at_time_specified');
                                                $send_button_title=lang('session_reminder_send');
                                                }
                                        else {
                                                $state=lang('session_reminder_state__waiting');
                                                $statecolor=$color['session_reminder_state_waiting_text'];
						$explanation=lang('session_reminder_will_be_sent_at_time_specified');
                                                $send_button_title=lang('session_reminder_send_now');
                                                }
                                        echo '<FONT color="'.$statecolor.'">'.lang('session_reminder').': '.$state.'</FONT><BR>';
					echo $explanation.'<BR><FORM action="session_send_reminder.php">'.
						'<INPUT type=hidden name="session_id" value="'.$session_id.'">'.
						'<INPUT type=submit name="submit" value="'.$send_button_title.'"></FORM>';
                                        }
	echo '		</TD><TD align=right>';
			if (!$transfer_mode && check_allow('participants_bulk_mail')) 
                        	experimentmail__bulk_mail_form();
	echo '		</TD>';

	echo '	</TR>
		</TABLE>';

        echo '  <A HREF="dossiers_paiement_main.php">
                        '.icon('back').' '.lang('back').'</A><BR><BR>

                </CENTER>';
if(false && !empty($_REQUEST['pay']) && !empty($dossier)) {
	$query="SELECT MAX(CAST(payment_num AS UNSIGNED)) as max_num FROM ".table('participate_at')." WHERE payment_file=$dossier";
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$query);
	$maxprev=0;
	if(mysqli_num_rows($result)>0) {
		$line=mysqli_fetch_assoc($result);
		if(is_numeric($line['max_num']))$maxprev=$line['max_num'];
	}
	// var_dump(mysqli_num_rows($result),$maxprev); exit;
	mysqli_free_result($result);
	$query="SELECT start_num_from as start_num FROM ".table('payment_files')." WHERE id=$dossier";
	$line=orsee_query($query);
	if($line['start_num']!='' && $line['start_num']>=$maxprev) $maxprev=$line['start_num']-1;
	$idarray=array("payment");
	if($nlfpay_name!="") $idarray[]="payment_nlf";
	script__payment_num($maxprev,count($participants),$idarray);
}
if($transfer_mode) echo '
<script type="text/javascript">
var sumselected=0,  nselected=0;
var lastchecked=0, lastunchecked=0;
var n_total='.$result_count.';
function select_all(m) {
	for(var h=1; h<=n_total; h++) checkpart4transfer(undefined,h,m);
	if(m>0) document.getElementById("up_to_sum").value="";
}
function checkpart4transfer(e,i,m) {
	if(m===undefined) m=0;
	var me=document.getElementById("chkbx"+i);
	// document.getElementById("test").innerHTML+="@ i="+i+", m="+m;
	var mypay=parseFloat(document.getElementById("payment_"+i).value);
	
	if(m==0){
		var evtobj=window.event? event : e;
		var mh=0; var lasttoconsider=0;
		if(me.checked) {
			lasttoconsider=lastchecked; lastchecked=i; lastunchecked=0; mh=1; nselected++; sumselected+=mypay; 
		}
		else {
			lasttoconsider=lastunchecked; lastchecked=0; lastunchecked=i; mh=-1; nselected--; sumselected-=mypay;
		}
		if (evtobj.shiftKey && lasttoconsider>0) {
			for(var h=Math.min(lasttoconsider,i); h<=Math.max(lasttoconsider,i); h++) if(h!=i) {
				checkpart4transfer(undefined,h,mh);
			}
			// document.getElementById("test").innerHTML+="| n="+nselected+", sum="+sumselected;
		}
		document.getElementById("up_to_sum").value="";
	}
	if(m!=0) {
		if((!me.checked && m>0) || (me.checked && m<0)) {
			me.checked=(me.checked)?false:"CHECKED";
			nselected+=m; sumselected+=m*mypay;
		}
	}
	document.getElementById("info_nselected").innerHTML=nselected;
	document.getElementById("info_sumselected").innerHTML=Math.round(sumselected*100)/100;
	if(nselected>0 && dossier_selected!=0) document.getElementById("dossier_submit").disabled=false;
	else document.getElementById("dossier_submit").disabled="DISABLED";
	// document.getElementById("test").innerHTML="n="+nselected+", sum="+Math.round(sumselected*100)/100;
}
var dossier_selected=0;
document.getElementsByTagName("select")["dossier_new"].addEventListener("change", function(){dossier_selected=this.value; if(this.value==0) document.getElementById("dossier_submit").disabled="DISABLED"; else if(nselected>0) document.getElementById("dossier_submit").disabled=false; });
function select_up_to_sum() {
	var csum=document.getElementById("up_to_sum").value;
	if(isNaN(parseFloat(csum)) || !isFinite(csum) || parseFloat(csum)<=0) {
		alert("'.lang('please_enter_a_correct_positive_number').'");
		return;
	}
	select_all(-1);
	var n=0;
	var dir=document.getElementById("uptosum_sel").value;
	var cpnr=(dir<0)?n_total:1;
	while(n<n_total && sumselected<csum) {
		checkpart4transfer(undefined,cpnr,1);
		cpnr=(dir<0)?cpnr-1:cpnr+1;
		n++;
	}
	
}
function move_finilize() {
	document.getElementById("move_to").value=dossier_selected;
	document.getElementById("sum_tomove").value=sumselected;
	document.getElementById("n_tomove").value=nselected;
	var paynums = new Array();
	var orig_min=n_total;
	var orig_max=0;
	for(var i=1; i<=n_total; i++) {
		var me=document.getElementById("chkbx"+i);
		var mynum=document.getElementById("td_payment_num_"+i).innerHTML;
		if(me.checked) paynums.push(mynum);
		if(Number(mynum)<orig_min) orig_min=Number(mynum);
		if(Number(mynum)>orig_max) orig_max=Number(mynum);
	}
	document.getElementById("paynums_tomove").value=paynums.join(",");
	document.getElementById("orig_max_paynum").value=orig_max;
	document.getElementById("orig_min_paynum").value=orig_min;
}
window.onload=function() {
	var message=document.querySelector(\'[bgcolor="coral"]\');
	if(message!=null && window.location.hash!="") {
		var duplicatemess=document.getElementById("message_duplicate");
		duplicatemess.innerHTML=\'<table bgcolor="blue" noshade="" width="400" cellpadding="2" cellspacing="0"><tbody><tr><td align="center">\'+message.outerHTML+"</td></tr></tbody></table>";
		duplicatemess.style.display="block";
	}
}
</script>
<div id="test"></div>
';

include ("footer.php");

?>

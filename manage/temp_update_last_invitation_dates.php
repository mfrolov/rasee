<?php
// part of orsee. see orsee.org
ob_start();

$menu__area="participants";
$title="assign participants";

include ("header.php");

query_makecolumns(table('participants'),array("last_invitation_time","last_invitation_exp"),array("int(20)","int(20)"),array("0","0"));
query_makecolumns(table('participants_temp'),array("last_invitation_time","last_invitation_exp"),array("int(20)","int(20)"),array("0","0"));

$n=0; $maxn=0;
$cexpid=0; $csessid=0;
$lastsession=[]; $session=[];
$parts=[];
$query="SELECT * FROM ".table('participate_at')." WHERE invited='y' ORDER BY experiment_id DESC,session_id DESC,participate_id DESC";
$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$query." <hr> ".lang('technical_information').":<br>".print_r(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS),true));
while ($result !==false && ($maxn == 0 || $n<$maxn) && $line = mysqli_fetch_assoc($result)) {
	$n++;
	if($line["experiment_id"]!=$cexpid) {
		$cexpid=$line["experiment_id"];
		$mysqlsessdate="STR_TO_DATE(CONCAT(session_start_year,'-',LPAD(session_start_month,2,'00'),'-',LPAD(session_start_day,2,'00'),'-',LPAD(session_start_hour,2,'00'),'-',LPAD(session_start_minute,2,'00')),'%Y-%m-%d-%H-%i')";
		$q="SELECT * FROM ".table('sessions')." WHERE experiment_id='$cexpid' AND part_needed>0 AND NOW()>$mysqlsessdate ORDER BY session_start_year DESC,session_start_month DESC,session_start_day DESC,session_start_hour DESC,session_start_minute DESC";
		$lastsession=orsee_query($q);
	}
	if(empty($line["session_id"])) {$session=$lastsession; $csessid=0;}
	if(!empty($line["session_id"]) && $line["session_id"]!=$csessid) {
		$csessid=$line["session_id"];
		$session=orsee_db_load_array("sessions",$csessid,"session_id");
		// $q="SELECT * FROM ".table('sessions')." WHERE session_id='$csessid' experiment_id='$cexpid'"; $session=orsee_query($q);
	}
	if(!empty($session)) {
		$ctime=sessions__get_session_time($session);
		if(!empty($session['session_reminder_hours']) && !empty($csessid)) $ctime=strtotime("- ".$session['session_reminder_hours']." hours",$ctime);
		if(empty($csessid)) $ctime=strtotime("-24 hours",$ctime);
		if(empty($parts[$line["participant_id"]][0]) || $parts[$line["participant_id"]][0]<$ctime) $parts[$line["participant_id"]]=[$ctime,$cexpid];
	}
	// echo "$n,$cexpid,$csessid,".$line["participant_id"]."<br>";
	// var_dump($line,$result);
	
}
mysqli_free_result($result);


$participants=[];
$n_undone=0;
foreach($parts as $pid=>$ltexp) {
	$notalreadydone=" AND last_invitation_time=0";
	// $notalreadydone="";
	$q="SELECT * FROM ".table('participants')." WHERE participant_id=".$pid.$notalreadydone;
	$p=orsee_query($q);
	if($p!==false) {
		if($p["last_delete_time"]>0 && $p["last_delete_time"]<$ltexp[0]) {$ltexp[0]=0; $ltexp[1]=0; $n_undone++;}
		$qu="UPDATE ".table('participants')." SET last_invitation_time='".$ltexp[0]."',last_invitation_exp='".$ltexp[1]."' WHERE participant_id=".$pid.$notalreadydone;
		orsee_query($qu);
		$participants[]=$p;
	}
}
$headers=[];
if(count($participants)>0) $headers=array_merge($headers,array_keys($participants[0]));

echo "<table class='table table-stripped' border=1>";
echo "<tr>";
echo "<th>".lang('edit')."</th>";
foreach($headers as $h) { echo "<th>".$h."</th>";}
echo "</tr>";
foreach($participants as $line) {
	echo '<tr>';
	echo '<td>'.'<a target=_blank href="participants_edit.php?participant_id='.$line['participant_id'].'">'.lang('edit').'</a>'.'</td>';
	foreach($headers as $h) {
		$cpid=$line['participant_id'];
		if(array_key_exists($h,$line)) echo "<td>".$line[$h]."</td>";
		elseif(!empty($parts_id[$cpid]) && array_key_exists($h,$parts_id[$cpid]))  echo "<td>".$parts_id[$cpid][$h]."</td>";
	}
	echo '</tr>';
}

echo "</table>";
echo "<p>".lang("participate_at_lines_proceeded").": ".$n."."."</p>";
echo "<p>".lang("participants_updated").": ".count($participants)."."."</p>";
if($n_undone>0) echo "<p>".lang("deleted_participants_updated").": ".$n_undone."."."</p>";
// var_dump($parts,$participants);
include ("footer.php");

?>

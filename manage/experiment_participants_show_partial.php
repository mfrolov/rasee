<?php
// part of orsee. see orsee.org
ob_start();

$menu__area="experiments";
$title="show participants";

//"Rules signed?" disabled by Maxim Frolov;
$showrules=false; if(!empty($_REQUEST["showrules"])) $showrules=true;

include("header.php");

	if (isset($_REQUEST['experiment_id']) && $_REQUEST['experiment_id']) $experiment_id=$_REQUEST['experiment_id'];
                else redirect($GLOBALS['settings__admin_folder']."/experiment_main.php"); //{var_dump($_REQUEST); var_dump($_POST); exit;}//

        if (isset($_REQUEST['session_id']) && $_REQUEST['session_id']) $session_id=$_REQUEST['session_id'];
                else $session_id='';

	if (isset($_REQUEST['remember']) && $_REQUEST['remember']) { 
		$_REQUEST=array_merge($_REQUEST,$_SESSION['save_posted']);
		$_SESSION['save_posted']=array();
		unset($_REQUEST['change']);
		}

	$allow=check_allow('experiment_show_participants','experiment_show.php?experiment_id='.$experiment_id);

	$experiment=orsee_db_load_array("experiments",$experiment_id,"experiment_id");
	if (!check_allow('experiment_restriction_override'))
		check_experiment_allowed($experiment,$GLOBALS['settings__admin_folder']."/experiment_show.php?experiment_id=".$experiment_id);
		
	$payment_active=(!empty($_REQUEST['pay']) && !empty($_REQUEST['dossier']) && !empty($_REQUEST['payment_mode']) && !$_REQUEST['payment_mode']!='view');

	if (isset($_REQUEST['change']) && $_REQUEST['change']) {

		$allow=check_allow('experiment_edit_participants','experiment_participants_show.php?experiment_id='.
				$_REQUEST['experiment_id'].'&session_id='.$_REQUEST['session_id'].'&focus='.$_REQUEST['focus']);

		if (isset($_REQUEST['focus']) && $_REQUEST['focus']) $focus=$_REQUEST['focus'];
			else redirect($GLOBALS['settings__admin_folder'].'/'.thisdoc().'?experiment_id='.$_REQUEST['experiment_id'].
						'&session_id='.$_REQUEST['session_id']);
		$focuses=array('assigned','invited','registered','shownup','participated');
		foreach($focuses as $f) { $$f=false; }
		$$focus=true;
		

		if (isset($_REQUEST['result_count']) && $_REQUEST['result_count']) $pcount=$_REQUEST['result_count']; else $pcount=0;

		if ($assigned || $invited) {
			$continue=true;

			if ($_REQUEST['to_session']) $to_session=$_REQUEST['to_session']; else $to_session=0;

			if ($to_session==0) {
				$continue=false;
				$_SESSION['save_posted']=$_REQUEST;
				message($lang['no_session_selected'],'message_error');
				redirect($GLOBALS['settings__admin_folder'].'/'.thisdoc().'?experiment_id='.$_REQUEST['experiment_id'].
                                                '&focus='.$focus.'&remember=true');
				}

			$tsession=orsee_db_load_array("sessions",$to_session,"session_id");

			$p_to_add=array();
			$i=0;
                        while ($i < $pcount) {
                                $i++;
				if (isset($_REQUEST['reg'.$i]) && $_REQUEST['reg'.$i]=="y") $p_to_add[]=$_REQUEST['pid'.$i];
				}

			$num_to_add=count($p_to_add);

			if (isset($_REQUEST['check_if_full']) && $_REQUEST['check_if_full']) {
				$alr_reg=experiment__count_participate_at($experiment_id,$to_session);
				$free_places=$tsession['part_needed']+$tsession['part_reserve']-$alr_reg;
				if ($free_places < 0) $free_places=0;
				
				$gender_stop=false;
				if(!empty($experiment['gender_equality']) && $experiment['gender_equality']!="n") {
					$alr_reg_f=experiment__count_participate_at($experiment_id,$to_session,"f");
					$alr_reg_m=experiment__count_participate_at($experiment_id,$to_session,"m");
					$free_places_gender=($free_places+$alr_reg)/2;
					$free_places_m=$free_places_gender-$alr_reg_m;
					$free_places_f=$free_places_gender-$alr_reg_f;
					$n_m=0; $n_f=0; $n_empty=0;
					foreach($p_to_add as $p_id){
						$current_pg=orsee_query("SELECT gender FROM ".table("participants")." WHERE participant_id='$p_id'");
						if($current_pg=="m") $n_m++;
						elseif($current_pg=="f") $n_f++;
						else $n_empty++;
					}
					if($n_empty>0 || $n_m>$free_places_m || $n_f>$free_places_f) $gender_stop=true;
				}
	
				if ($num_to_add > $free_places || $gender_stop) {
					$continue=false;
					$gender_info="";
					if($gender_stop) $gender_info=" ($free_places_m ".lang('gender_m')." ".lang('and')." $free_places_f ".lang('gender_f').")";
					message($lang['too_much_participants_to_register'].' '.
						$lang['free_places_in_session_xxx'].' '.
						session__build_name($tsession).': 
						<FONT color="green">'.$free_places.$gender_info.'</FONT><BR>'.
						$lang['please_change_your_selection'],'message_error');
						$_SESSION['save_posted']=$_REQUEST;
					redirect($GLOBALS['settings__admin_folder'].'/'.thisdoc().
						'?experiment_id='.$_REQUEST['experiment_id'].
                                                '&focus='.$focus.'&remember=true'.
						'&to_session='.$to_session);
					}
				}

			if ($continue) {
				$part_list=implode("','",$p_to_add);
               			$query="UPDATE ".table('participate_at')."
					SET session_id='".$to_session."',
						registered='y'
					WHERE experiment_id='".$experiment_id."'
					AND participant_id IN ('".$part_list."')";
				$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

				message ($num_to_add.' '.$lang['xxx_subjects_registered_to_session_xxx'].' '.
					session__build_name($tsession).'.<BR>
					<A HREF="'.thisdoc().'?focus=registered&experiment_id='.$experiment_id.
					'&session_id='.$to_session.'">'.$lang['click_here_to_go_to_session_xxx'].
					' '.session__build_name($tsession).'</A>');
				redirect($GLOBALS['settings__admin_folder'].'/'.thisdoc().'?experiment_id='.$experiment_id.'&focus='.$focus);
				}

			}


		elseif ($registered || $shownup || $participated || $nonparticipated || $absent) {


			$p_shup=array(); $p_shup_not=array();
			$p_part=array(); $p_part_not=array();
			$p_rules=array(); $p_rules_not=array();
			$move=array();
			
			if($payment_active) {
				$op_amount_cash=0; $op_amount_noncash=0; $op_remarks=lang('automatic').' ('.lang('subjects_payment',false).')'; $op_nchages=0;
			}
			
			$dontChangeParticipationInfo=false;

		    $i=0;
            while ($i < $pcount) 
			{
                $i++;
				// if(empty($_REQUEST['pay'])) {
                        	if (isset($_REQUEST['shup'.$i]) ) {
								if ($_REQUEST['shup'.$i]=="y") $p_shup[]=$_REQUEST['pid'.$i];
								else $p_shup_not[]=$_REQUEST['pid'.$i];
							}
							elseif(!$dontChangeParticipationInfo) $p_shup_not[]=$_REQUEST['pid'.$i];

							if (isset($_REQUEST['part'.$i]) ) { 
								if( $_REQUEST['part'.$i]=="y") $p_part[]=$_REQUEST['pid'.$i];
													else $p_part_not[]=$_REQUEST['pid'.$i];
							}
							elseif(!$dontChangeParticipationInfo) $p_part_not[]=$_REQUEST['pid'.$i];

							if (isset($_REQUEST['rules'.$i]) ) { 
								if ($_REQUEST['rules'.$i]=="y") $p_rules[]=$_REQUEST['pid'.$i];
													else $p_rules_not[]=$_REQUEST['pid'.$i];
							}
							elseif($showrules && !$dontChangeParticipationInfo) $p_rules_not[]=$_REQUEST['pid'.$i];

				if (!isset($_REQUEST['session'.$i])) $_REQUEST['session'.$i]="";
				if (!isset($_REQUEST['csession'.$i])) $_REQUEST['csession'.$i]="";
				if ($_REQUEST['session'.$i]!=$session_id 
				    && $_REQUEST['session'.$i] != $_REQUEST['csession'.$i]) {
					$to_session=$_REQUEST['session'.$i];
					if (!isset($move[$to_session])) $move[$to_session]=array();
					$move[$to_session][]=$_REQUEST['pid'.$i];
					}
				// }
				if($payment_active) {
					$pid=$_REQUEST['pid'.$i]; $payment=$_REQUEST['payment_'.$i]; $paynum=$_REQUEST['payment_num_'.$i]; $dossier=$_REQUEST['dossier'];
					$nlfpay=""; $payment_nlf="";
					if(!empty($_REQUEST['payment_nlf_'.$i])) {$payment_nlf=$_REQUEST['payment_nlf_'.$i]; $nlfpay=", payment_nlf='".$payment_nlf."'";}
					$query="UPDATE ".table('participate_at')."
										SET payment = '$payment', payment_num='$paynum', payment_file='$dossier' $nlfpay
										WHERE experiment_id='".$experiment_id."'
						AND participant_id = '$pid'";
					if($payment!=="" || $nlfpay!=="") {
						$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
					}
					// if($payment!=="") {var_dump($query,$done); exit;}
					$prevpayment=(empty($_REQUEST['payment_init_'.$i]))?0:$_REQUEST['payment_init_'.$i];
					$newpayment=(empty($payment))?0:$payment;
					$prevnlfpay=(empty($_REQUEST['payment_nlf_init_'.$i]))?0:$_REQUEST['payment_nlf_init_'.$i];
					$newnlfpay=(empty($payment_nlf))?0:$payment_nlf;
					$op_amount_cash+=$newpayment-$prevpayment;
					$op_amount_noncash+=$newnlfpay-$prevnlfpay;
					if(($prevpayment!=$newpayment || $prevnlfpay!=$newnlfpay) && ($prevpayment>0 || $prevnlfpay>0)) {
						if($op_nchages==0) $op_remarks.=". ".lang('changes').': ';
						$cpart=orsee_db_load_array("participants",$pid,"participant_id");
						$cpart_link='<a target=_blank href="participants_edit.php?participant_id='.$pid.'">'.$cpart['fname'].' '.$cpart['lname'].'</a>';
						if($prevpayment>0 && $prevpayment!=$newpayment) {
							if($op_nchages>0) $op_remarks.=", ";
							$op_remarks.= $cpart_link.' : '.$prevpayment.'<big>&rarr;</big>'.$newpayment;
							$op_nchages++;
						}
						if($prevnlfpay>0 && $prevnlfpay!=$newnlfpay) {
							if($op_nchages>0) $op_remarks.=", ";
							$nlfline=orsee_query("SELECT nonliquid_funds_name FROM ".table('payment_files')." WHERE id=$dossier");
							$op_remarks.= $cpart_link.'('.$nlfline['nonliquid_funds_name'].')'.': '.$prevnlfpay.'<big>&rarr;</big>'.$newnlfpay;
							$op_nchages++;
						}
						
					}
					
				}
			}
			if($payment_active) if($op_amount_cash!=0 || $op_amount_noncash!=0) {
				//var_dump($prevnlfpay,$newnlfpay,$op_amount_noncash); exit;
				$dossier=$_REQUEST['dossier'];
				$dossier_attr="";
				if($session_id!='') {
					$sessline=orsee_query("SELECT * FROM ".table("sessions")." WHERE session_id=$session_id");
					if(isset($sessline['payment_file'])) $dossier_attr=$sessline['payment_file'];
				}
				if(empty($dossier_attr) && $experiment_id!='') {
					$expline=orsee_query("SELECT * FROM ".table("experiments")." WHERE experiment_id=$experiment_id");
					if(isset($expline['payment_file'])) $dossier_attr=$expline['payment_file'];
				}
				$used_file=(empty($dossier_attr))?$dossier:$dossier_attr;
				$moneyrespline=orsee_query("SELECT money_responsible FROM ".table("payment_files")." WHERE id='$used_file'");
				$o= new operation($moneyrespline['money_responsible'],'participants',$dossier);
				if($op_amount_cash!=0) {
					$o->add("payment_cash",1,$op_amount_cash);
				}
				if($op_amount_noncash!=0) {
					$nlfline=orsee_query("SELECT nonliquid_funds_name FROM ".table('payment_files')." WHERE id=$dossier");
					$o->add("payment_".$nlfline['nonliquid_funds_name'],1,$op_amount_noncash);
				}
				$o->remarks=$op_remarks;
				if(!empty($dossier_attr) && $dossier!=$dossier_attr) $o->instead_of=$dossier_attr;
				$o->id=$_REQUEST['pay_operation_id'];
				$query="SELECT id FROM ".table("pay_operations")." WHERE id='".$o->id."' AND remarks='".$o->remarks."'";
				$make_operation=true;
				if(orsee_query($query)!==false) {
					message(lang('operation_already_exists').' (id='.$o->id.')');
					$make_operation=false;
					//redirect($GLOBALS['settings__admin_folder']."/".thisdoc());
					//exit;
				}
				$accline=orsee_query("SELECT SUM(amount) as file_amount FROM ".table("pay_accounts")." WHERE shortname<>'bank' AND shortname<>'participants' AND payment_file='$dossier'");
				if($accline===false || $accline['file_amount']==0) {
					message(lang('accounting_record_not_made_as_no_money_available_for_this_file').'.<hr>');
					$make_operation=false;
				}
				if($make_operation) $o->make();
			}

			// update shownup data
			$part_list=implode("','",$p_shup);
			$query="UPDATE ".table('participate_at')."
                                SET shownup = 'y'
                                WHERE experiment_id='".$experiment_id."'
				AND participant_id IN ('".$part_list."')";
			$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
			
			$part_list=implode("','",$p_shup_not);
                        $query="UPDATE ".table('participate_at')."
                                SET shownup = 'n'
                                WHERE experiment_id='".$experiment_id."'
				AND participant_id IN ('".$part_list."')";
                        $done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

			// update participation data
                        $part_list=implode("','",$p_part);
                        $query="UPDATE ".table('participate_at')."
                                SET participated = 'y'
                                WHERE experiment_id='".$experiment_id."'
                                AND participant_id IN ('".$part_list."')";
                        $done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

                        $part_list=implode("','",$p_part_not);
                        $query="UPDATE ".table('participate_at')."
                                SET participated = 'n'
                                WHERE experiment_id='".$experiment_id."'
                                AND participant_id IN ('".$part_list."')";
                        $done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

			// check for inconsitencies and clean
			$query="UPDATE ".table('participate_at')."
                                SET shownup = 'y'
                                WHERE participated='y'";
                        $done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

			// update rules signed data
                        $part_list=implode("','",$p_rules);
                        $query="UPDATE ".table('participants')."
                                SET rules_signed = 'y'
                                WHERE participant_id IN ('".$part_list."')";
                        $done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

                        $part_list=implode("','",$p_rules_not);
                        $query="UPDATE ".table('participants')."
                                SET rules_signed = 'n'
                                WHERE participant_id IN ('".$part_list."')";
                        $done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));



			// move participants to other sessions ...
			$moved_nr=array();
			foreach ($move as $msession => $mparts) {
				$part_list=implode("','",$mparts);
				$sessidpart="session_id = '".$msession."',"; $payghostpart=""; $wheresup="";
				if($msession==0) {
					$wheresup="payment_num='' AND ";
					$query="UPDATE ".table('participate_at')."
						SET $sessidpart shownup='n', participated='n'$payghostpart
						WHERE $wheresup participant_id IN ('".$part_list."')
						AND experiment_id='".$experiment_id."'";
					$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
					$sessidpart=""; $payghostpart=", payghost=1";
					$wheresup="payment_num<>'' AND ";
				}
				$query="UPDATE ".table('participate_at')."
					SET $sessidpart shownup='n', participated='n'$payghostpart
					WHERE $wheresup participant_id IN ('".$part_list."')
					AND experiment_id='".$experiment_id."'";
				$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
				if($msession==0 && mysqli_affected_rows($GLOBALS['mysqli'])>0) {
					$query="INSERT INTO ".table('participate_at')." (participant_id,experiment_id) 
									SELECT participant_id, '".$experiment_id."' 
									FROM ".table('participate_at')." 
						WHERE payghost=1 AND experiment_id='".$experiment_id."' AND participant_id IN ('".$part_list."') ";
					$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
				}
				
			}

			// clean up 'no session's
                        $query="UPDATE ".table('participate_at')."
                                SET participated = 'n', shownup='n', registered='n'
                             	WHERE session_id='0'";
			$done=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));



			message($lang['changes_saved']);
			$m_message='<UL>';
			foreach ($move as $msession => $mparts) {
				$m_message.='<LI>'.count($mparts).' ';
				if ($msession==0) $m_message.=$lang['xxx_subjects_removed_from_registration'];
				   else {
					$tsession=orsee_db_load_array("sessions",$msession,"session_id");
					$m_message.=$lang['xxx_subjects_moved_to_session_xxx'].' 
						<A HREF="'.thisdoc().'?focus=registered&experiment_id='.
							$experiment_id.'&session_id='.$msession.'">'.
                					session__build_name($tsession).'</A>';
					$tpartnr=experiment__count_participate_at($experiment_id,$msession);
					if ($tsession['part_needed'] + $tsession['part_reserve'] < $tpartnr) 
						$mmessage.='<BLINK>'.
							$lang['subjects_number_exceeded'].'</BLINK>';
					}
				}
			$m_message.='</UL>';
			message($m_message);
			$target="experiment:".$experiment['experiment_name'];
			if ($session_id) $target.="\nsession_id:".$session_id;
			log__admin("experiment_edit_participant_list",$target);
			
			$redirectaddr=$GLOBALS['settings__admin_folder'].'/'.thisdoc().'?experiment_id='.$_REQUEST['experiment_id'].
                                                '&session_id='.$_REQUEST['session_id'].
						'&focus='.$focus;
			if(!empty($_REQUEST['pay'])) $redirectaddr.='&pay='.$_REQUEST['pay'];
			if(!empty($_REQUEST['payment_mode'])) $redirectaddr.='&payment_mode='.$_REQUEST['payment_mode'];
			if(!empty($_REQUEST['dossier'])) $redirectaddr.='&dossier='.$_REQUEST['dossier'];
			if(isset($_REQUEST['only_empty'])) $redirectaddr.='&only_empty='.$_REQUEST['only_empty'];

			redirect($redirectaddr);
			}

		}
	// list output


	$columns=participant__load_result_table_fields('session');
	$nlfpay_name="";
	if(!empty($_REQUEST['pay'])) {
		$payment_mode="entry"; $dossier="";
		if(!empty($_REQUEST['dossier'])) $dossier=$_REQUEST['dossier'];
		if(!empty($_REQUEST['payment_mode'])) $payment_mode=$_REQUEST['payment_mode'];
		$partemailpaymode=$_REQUEST['pay'];
		if($payment_mode=="view" && !empty($_REQUEST['show_emails'])) $partemailpaymode=2;
		$columns=participant__load_result_table_fields('session','',$partemailpaymode);
		query_makecolumns(table('participate_at'),array("payment","payment_nlf","payment_num","payment_file"),array("varchar(20)","varchar(20)","varchar(20)","varchar(20)"), array("","","",""));
		$query="SELECT * FROM ".table('payment_files')." WHERE id=$dossier";
		if(!empty($dossier))$line=orsee_query($query);
		if(!empty($line['nonliquid_funds_name'])) $nlfpay_name=$line['nonliquid_funds_name'];

	}

	if ($session_id) $session=orsee_db_load_array("sessions",$session_id,"session_id");

	script__part_reg_show();

	$csorts=array(); 
	foreach($columns as $c) if (count($csorts)<2 && $c['allow_sort']) $csorts[]=$c['sort_order'];
	$csorts_string=implode(",",$csorts);

	if (isset($_REQUEST['sort']) && $_REQUEST['sort']) {
		$order=$_REQUEST['sort'];
		$ordernamearr=explode('_',$order);
		if($ordernamearr[0]=="payment") {
			if(empty($ordernamearr[1]) || $ordernamearr[1]!="file") $order="CAST($order AS DECIMAL(10,2))";
			elseif($ordernamearr[1]=="file") $order="".table("participate_at").".$order, CAST(payment_num AS UNSIGNED)";
		}
		
	}
	else { 
		$order="session_start_year, session_start_month, session_start_day,
				session_start_hour, session_start_minute";
		if ($csorts_string) $order.=",".$csorts_string;
    }

	if (isset($_REQUEST['focus']) && $_REQUEST['focus']) $focus=$_REQUEST['focus']; else $focus="assigned";

	$assigned=false; $invited=false; $registered=false; $shownup=false; $participated=false; $nonparticipated=false; $absent=false;

	$$focus=true;

	if(!isset($lang['nonparticipated']))$lang['nonparticipated']=($lang['lang']=='fr')?"Enregistr&eacute;s mais pas particip&eacute;":"Registered but not participated";

	switch ($focus) {
		case "assigned":	$where_clause="AND registered='n'";
					$title=$lang['assigned_subjects_not_yet_registered'];
					break;
		case "invited":		$where_clause="AND invited='y' AND registered='n'";
                                        $title=$lang['invited_subjects_not_yet_registered'];
                                        break;
        	case "registered":	$where_clause="AND registered='y'";
                                        $title=$lang['registered_subjects'];
                                        break;
        	case "shownup":		$where_clause="AND shownup='y'";
                                        $title=$lang['shownup_subjects'];
                                        break;
        	case "participated":	$where_clause="AND participated='y'";
                                        $title=$lang['subjects_participated'];
                                        break;
        	case "nonparticipated":	$where_clause="AND participated='n'  AND registered='y'";
                                        $title=$lang['nonparticipated'];
                                        break;
        	case "absent":	$where_clause="AND participated='n' AND shownup='n' AND registered='y'";
                                        $title=lang('subjects_absent');
                                        break;
		} 


	$select_query=" SELECT *";
	if(!empty($_REQUEST['pay'])) $select_query.=", ".table('participate_at').".payment_file as dossier";
	$select_query.=" FROM ".table('participants').", ".table('participate_at').", 
					".table('sessions')."
			WHERE ".table('participants').".participant_id=".
					table('participate_at').".participant_id 
			AND ".table('sessions').".session_id=".table('participate_at').".session_id 
			AND ".table('participate_at').".experiment_id='".$experiment_id."' ";
	if ($session_id) $select_query.=" AND ".table('participate_at').".session_id='".$session_id."' "; 
	if(!empty($_REQUEST['restricted_plist']))  $select_query.=" AND ".table('participate_at').".participant_id IN (".$_REQUEST['restricted_plist'].") ";
	if(!empty($_REQUEST['payment_mode']) && $_REQUEST['payment_mode']=='view' && !empty($_REQUEST['pay']) && !isset($_REQUEST['only_empty']))  
				$select_query.=" AND (".table('participate_at').".payment<>'' OR ".table('participate_at').".payment_num<>'' OR ".table('participate_at').".payment_file<>'' OR ".table('participate_at').".payment_nlf<>'')";
	if(!empty($_REQUEST['pay']) && !empty($_REQUEST['only_empty'])) {
				$comp_sign=($_REQUEST['only_empty']>0)?'=':'<>';
				$select_query.=" AND (".table('participate_at').".payment".$comp_sign."''";
				if($_REQUEST['only_empty']<0) $select_query.=" OR ".table('participate_at').".payment_nlf".$comp_sign."''";
				$select_query.=")";
	}
	if(!empty($_REQUEST['payment_mode']) && $_REQUEST['payment_mode']!='view' && !empty($_REQUEST['pay']) && !empty($_REQUEST['dossier']))
				$select_query.=" AND (".table('participate_at').".payment_file='".$_REQUEST['dossier']."' OR ".table('participate_at').".payment_file='' OR ".table('participate_at').".payment_file IS NULL)";
    $select_query.=$where_clause." ORDER BY ".$order;


	echo '
		<center>
		<BR>
			<h4>'.$experiment['experiment_name'].'</h4>
			<h4 style="text-decoration:underline">'.$title.'</h4>

		<P align=right style="margin-right:5%">';
		if(empty($_REQUEST['pay'])) echo '
			<A class="small" HREF="experiment_participants_show_pdf.php?experiment_id='.
				$experiment_id.'&session_id='.$session_id.'&focus='.$focus.
				'" target="_blank">'.$lang['print_version'].'</A>&nbsp;&nbsp;&nbsp;';
		if(empty($_REQUEST["disabled"]) && empty($_REQUEST['pay'])) echo '
				<br><br>
				<A class="small" HREF="'.$_SERVER['PHP_SELF'].'?experiment_id='.
				$experiment_id.'&session_id='.$session_id.'&focus='.$focus.
				'&disabled=1" ><small>disabled mode&nbsp;&nbsp;&nbsp;&nbsp;</small></A>';
		else echo '
				<br><br>
				<A class="small" HREF="'.$_SERVER['PHP_SELF'].'?experiment_id='.
				$experiment_id.'&session_id='.$session_id.'&focus='.$focus.
				'" ><small>normal mode&nbsp;&nbsp;&nbsp;&nbsp;</small></A>';
		$normal_link_start=$_SERVER['PHP_SELF'].'?experiment_id='.$experiment_id.'&session_id='.$session_id;
		$normal_link=$normal_link_start.'&focus='.$focus;
		$subjects_shortcuts=array();
		$subjects_shortcuts["shownup"]=$lang['shownup_subjects'];
		$subjects_shortcuts["participated"]=$lang['subjects_participated'];
		$subjects_shortcuts["registered"]=$lang['registered_subjects'];
		//$subjects_shortcuts["nonparticipated"]=$lang['nonparticipated'];
		unset($subjects_shortcuts[$focus]);
		if(empty($_REQUEST["disabled"]) && empty($_REQUEST['pay']) && !($assigned || $invited))  {
			echo '
				<br><br>Other subpools : ';
			foreach ($subjects_shortcuts as $shk=>$sh) {
				echo ' <A class="small" HREF="'.$normal_link_start.'&focus='.$shk.'" >'.$sh.'</A>&nbsp;&nbsp;&nbsp;&nbsp; ';
			}
		}
		
		$payment_links=array("entry/edit mode"=>$normal_link.'&pay=1&payment_mode=entry',
				"entry mode (only empty)"=>$normal_link.'&pay=1&payment_mode=entry&only_empty=1',
				"edit mode (only non empty)"=>$normal_link.'&pay=1&payment_mode=entry&only_empty=-1',
				"view mode"=>$normal_link.'&pay=1&payment_mode=view');
		if(!isset($lang['payment']))$lang['payment']=($lang['lang']=='fr')?"Paiement":"Payment";
		if(!($assigned || $invited)) {
			echo '
					<br><br>'.$lang['payment'].' : ';
			foreach ($payment_links as $lk=>$pl)
				echo ' <A class="small" HREF="'.$pl.'" >'.$lk.'</A>&nbsp;&nbsp;&nbsp;&nbsp; ';
		}
		if(!empty($_REQUEST['pay']) && $payment_mode=="view") {
			echo '
					<br><br>View mode : ';
			$view_links=array();
			$veiwbase=$normal_link.'&pay=1&payment_mode=view';
			if(!isset($_REQUEST['only_empty']) || $_REQUEST['only_empty']<0) $view_links[]=array("show empty"=>$veiwbase.'&only_empty=0');
			if(isset($_REQUEST['only_empty']) && $_REQUEST['only_empty']>=0) $view_links[]=array("hide empty"=>$veiwbase);
			foreach($view_links[0] as &$ar) if(!empty($_REQUEST['show_emails'])) $ar.='&show_emails='.$_REQUEST['show_emails'];
			if(empty($_REQUEST['show_emails'])) $view_links[]=array("show emails"=>$veiwbase.'&show_emails=1');
			else $view_links[]=array("hide emails"=>$veiwbase.'');
			foreach($view_links[1] as &$ar) if(isset($_REQUEST['only_empty'])) $ar.='&only_empty='.$_REQUEST['only_empty'];
			foreach ($view_links as $view_links_inner) foreach ($view_links_inner as $lk=>$pl)
				echo ' <A class="small" HREF="'.$pl.'" >'.$lk.'</A>&nbsp;&nbsp;&nbsp;&nbsp; ';
		}
		echo '</P>';

	if(!empty($_REQUEST['pay'])) {
		$_REQUEST["disabled"]="1";
		if(empty($dossier) && $payment_mode=="entry") {
			echo '<CENTER>
				<FORM name="choisir_dossier" method=post action="'.thisdoc().'">
			
				<BR>';
			echo "<h2>".lang('please_choose_the_payment_file')."</h2>";
			echo '<INPUT name="payment_mode" type=hidden value="'.$payment_mode.'">';
			echo '
				<INPUT name="pay" type=hidden value="'.$_REQUEST['pay'].'">
				<INPUT type=hidden name="focus" value="'.$focus.'">
				<INPUT type=hidden name="experiment_id" value="'.$experiment_id.'">
				<INPUT type=hidden name="session_id" value="'.$session_id.'">';
			if(!empty($_REQUEST['only_empty'])) echo '
				<INPUT name="only_empty" type=hidden value="'.$_REQUEST['only_empty'].'">';
			if(!isset($lang['edit_payment_files'])) $lang['edit_payment_files']=($lang['lang']=='fr')?"Modifier les dossiers de paiement":"Edit payment files";
			echo '
				<table border=0 style="text-align:center"><tr>
				<td></td>
				</tr><tr>
				<td>';
			$select_payment_file=helpers__select_payment_file($experiment_id,$session_id);
			if($select_payment_file!==false) echo $select_payment_file;
			echo '
				</td>
				</tr></table>
				';
			if($select_payment_file!==false)  echo '<INPUT type=submit name="choose_payment_file" value="OK">';
			echo '<br><br><a target=_blank href="dossiers_paiement_main.php">'.$lang['edit_payment_files'].'</a>
				</FORM>
					';
		echo '<BR><BR>
			<A href="'.thisdoc().'?experiment_id='.$experiment_id.'&session_id='.$session_id.'&focus='.$focus.'">'.icon('back').' '.$lang['back'].'</A><BR><BR>
			';
			echo '  <A HREF="experiment_show.php?experiment_id='.$experiment_id.'">
							'.$lang['mainpage_of_this_experiment'].'</A><BR><BR>

					</CENTER>';
			include ("footer.php");
			exit;
		}
	}
	// show query
	echo '	<P class="small">Query: '.$select_query.'</P>';
	if(!empty($dossier) && $payment_mode=='entry') echo '<h3>Dossier : '.dossier_name($dossier,1).'</h3>';

	// get result
	$result=mysqli_query($GLOBALS['mysqli'],$select_query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
	

        $participants=array(); $plist_ids=array(); $noncash_names=array(); $findnoncash=($nlfpay_name=="");
		if(!isset($lang['non_cash']))$lang['non_cash']=($lang['lang']=='fr')?"non cash":"non cash";
        while ($line=mysqli_fetch_assoc($result)) {
				if($findnoncash && !empty($line['payment_nlf']) && !empty($line['payment_file'])) {
					$nlfpay_name=$lang['non_cash'];
					$query="SELECT * FROM ".table('payment_files')." WHERE id=".$line['payment_file'];
					$nlfline=orsee_query($query);
					if(!empty($nlfline['nonliquid_funds_name']) && !in_array($nlfline['nonliquid_funds_name'],$noncash_names)) $noncash_names[]=$nlfline['nonliquid_funds_name'];
				}
                $participants[]=$line;
		$plist_ids[]=$line['participant_id'];
                }
		if($findnoncash && $nlfpay_name==$lang['non_cash']) $nlfpay_name=implode(', ',$noncash_names);
	$_SESSION['plist_ids']=$plist_ids;
	$result_count=count($participants);
	
	$critical_count=300;
	$critical_count_apply=(($registered || $shownup || $participated || $nonparticipated || $absent) && $result_count>$critical_count);

	$nr_normal_columns=2;
	// form
	echo '
		<FORM name="part_list" method=post action="'.thisdoc().'">
	
		<BR>
		<table border=0>
			<TR>
				<TD class="small"></TD>';
				foreach($columns as $c) {
                   	if($c['allow_sort'] && !empty($_REQUEST['pay'])) 
					{
						$supvals="&pay=".$_REQUEST['pay']."&payment_mode=".$payment_mode;
						if($payment_mode=="entry") $supvals.="&dossier=".$_REQUEST['dossier'];
						if(!empty($_REQUEST['show_emails'])) $supvals.="&show_emails=".$_REQUEST['show_emails'];
						if(isset($_REQUEST['only_empty'])) $supvals.="&only_empty=".$_REQUEST['only_empty'];
						headcell($c['column_name'],$c['sort_order'],"",$supvals);
					}
					elseif($c['allow_sort']) headcell($c['column_name'],$c['sort_order']);
                   	else headcell($c['column_name']);
                   	$nr_normal_columns++;
                }
				if(empty($_REQUEST['pay'])) headcell($lang['noshowup'],"number_noshowup,number_reg");
				else {
					$sum_payment=0;
					if(!isset($lang['payment']))$lang['payment']=($lang['lang']=='fr')?"Paiement":"Payment";
					if(!isset($lang['payment_num']))$lang['payment_num']=($lang['lang']=='fr')?"N paiement":"N payment";
					if(!isset($lang['payment_file']))$lang['payment_file']=($lang['lang']=='fr')?"Dossier":"File";
					// "payment_num","payment_file"
					$supvals="&pay=".$_REQUEST['pay']."&payment_mode=".$payment_mode;
					if(!empty($_REQUEST['dossier'])) $supvals.="&dossier=".$_REQUEST['dossier'];
					if(!empty($_REQUEST['show_emails'])) $supvals.="&show_emails=".$_REQUEST['show_emails'];
					if(isset($_REQUEST['only_empty'])) $supvals.="&only_empty=".$_REQUEST['only_empty'];
					$payment_supname="";
					if($nlfpay_name!="") {
						$payment_supname.=" (".lang('cash',false).")";
						$sum_payment_nlf=0;
					}
					headcell($lang['payment'].$payment_supname,"payment","",$supvals);
					if($nlfpay_name!="") headcell($lang['payment'].' ('.$nlfpay_name.')',"payment_nlf","",$supvals);
					headcell($lang['payment_num'],"payment_num","",$supvals);
					if($payment_mode=="view") headcell($lang['payment_file'],"payment_file","",$supvals);
				}
			if ($assigned || $invited) {
				headcell($lang['invited'],"invited","invited");
				headcell($lang['register']);
				}
			if ($registered || $shownup || $participated || $nonparticipated || $absent) {
				if(!isset($supvals)) $supvals="";
				if(isset($payment_mode) && $payment_mode=="view") headcell('Date');
				headcell($lang['session'],table("sessions").".session_id,lname,fname","session",$supvals);
				headcell($lang['shownup'],"shownup","shownup",$supvals);
				headcell($lang['participated'],"participated","participated",$supvals);
				if($showrules) headcell($lang['rules_signed']);
				if($critical_count_apply) headcell("Check to restrict");
				}
	echo '		</TR>';



	$shade=false;

	$part_ids=array();
	$emails=array();

	$pnr=0;

	if (check_allow('experiment_edit_participants') && empty($_REQUEST["disabled"])) $disabled=false; else $disabled=true;

        foreach ($participants as $p) {

		$emails[]=$p['email'];
		$pnr++;

		echo '<tr class="small"';
                        if ($shade) echo ' bgcolor="'.$color['list_shade1'].'"';
                               else echo ' bgcolor="'.$color['list_shade2'].'"';
                echo '>';

	        echo '	<td class="small">
				'.$pnr; 
				echo '<INPUT name="pid'.$pnr.'" id="pid'.$pnr.'" type=hidden value="'.$p['participant_id'].'">';
			echo '</td>';
			foreach($columns as $c) {
               	echo '<td class="small">';
               	if($c['link_as_email_in_lists']=='y') echo '<A class="small" HREF="mailto:'.
               		$p[$c['mysql_column_name']].'">';
               	if(preg_match("/(radioline|select_list|select_lang)/",$c['type']) && isset($c['lang'][$p[$c['mysql_column_name']]]))
               		echo $c['lang'][$p[$c['mysql_column_name']]];
               	else echo $p[$c['mysql_column_name']];
               	if($c['link_as_email_in_lists']=='y') '</A>';
               	echo '</td>';
            }
			if(empty($_REQUEST['pay']))  echo '
			<td class="small">'.$p['number_noshowup'].
                                                '/'.$p['number_reg'].'</td>';
			else {
				if($payment_mode!="entry" && $payment_mode!="edit") echo '<td class="small"><input type=hidden name=payment_'.$pnr.' id=payment_'.$pnr.' value="'.$p['payment'].'">'.$p['payment'].'</td>';
				else echo '<td class="small"><input type=text onBlur="payment_num(this)" name=payment_'.$pnr.' id=payment_'.$pnr.' value="'.$p['payment'].'"><input type=hidden name=payment_num_'.$pnr.' id=payment_num_'.$pnr.' value="'.$p['payment_num'].'"><input type=hidden name=payment_init_'.$pnr.' value="'.$p['payment'].'"></td>';
				$sum_payment+=$p['payment'];
				if($nlfpay_name!="") {
					if($payment_mode!="entry" && $payment_mode!="edit") echo '<td class="small"><input type=hidden name=payment_nlf_'.$pnr.' id=payment_nlf_'.$pnr.' value="'.$p['payment_nlf'].'">'.$p['payment_nlf'].'</td>';
					else echo '<td class="small"><input type=text onBlur="payment_num(this)" name=payment_nlf_'.$pnr.' id=payment_nlf_'.$pnr.' value="'.$p['payment_nlf'].'"><input type=hidden name=payment_nlf_init_'.$pnr.' value="'.$p['payment_nlf'].'"></td>';	
					$sum_payment_nlf+=$p['payment_nlf'];
				}
				echo '<td class="small" id="td_payment_num_'.$pnr.'">'.$p['payment_num'].'</td>';				
				if($payment_mode=="view") echo '<td class="small">'.dossier_name($p['dossier'],1).'</td>';				
			}

		if ($assigned || $invited) {
			echo '<td class="small">'.$p['invited'].'</td>
			      <td class="small">';
				  if(!$disabled ) {
					echo '
					<INPUT type=checkbox id="chkbx'.$pnr.'" name="reg'.$pnr.'" value="y"';
					if ((isset($_REQUEST['reg'.$pnr]) && $_REQUEST['reg'.$pnr]=="y") || !empty($_REQUEST['restricted_plist']) ) echo ' CHECKED';
					if ($disabled) echo ' DISABLED';
					echo '>';
					} elseif(isset($_REQUEST['reg'.$pnr])) echo $_REQUEST['reg'.$pnr]; else echo '?';
				echo '
				</td>';
				// Edit added by Maxim Frolov 
				if((empty($_REQUEST["disabled"]) && !(isset($_REQUEST["editlink"]) && empty($_REQUEST["editlink"])))  || !empty($_REQUEST["editlink"])) echo '<td><a target=_blank href="participants_edit.php?participant_id='.$p["participant_id"].'">Edit</td>';
			}
		$disable_check=$disabled;
	   	if ($registered || $shownup || $participated || $nonparticipated || $absent) {
	   		echo '<td class="small">';
				if(isset($payment_mode) && $payment_mode=="view") {
					echo time__format($lang['lang'],time__load_session_time($p),false,true,true,false);
					echo '
					</td>
					<td class="small">
					';
				}
				if (!$session_id) {
				echo '<INPUT type=hidden name="csession'.$pnr.'" value="'.$p['session_id'].'">';
				}
				if ($disabled) {if(isset($payment_mode) && $payment_mode=="view") echo session__build_name($p,"",true); else echo session__build_name($p);}
				   else echo select__sessions($p['session_id'],'session'.$pnr,$experiment_id,false);
	   		echo '</td>
	   			<td class="small">';
			
			$disable_check=($disabled && !(!empty($_REQUEST['pay']) && !empty($payment_mode) && ($payment_mode=="entry" || $payment_mode=="edit") ) && ($registered || $nonparticipated || $absent));// !:  && (empty($_REQUEST['only_empty']) || $_REQUEST['only_empty']<0)
                if (!$disable_check && ($registered || $shownup || $nonparticipated || $absent)) {
                       		echo '<INPUT type=checkbox id="shup'.$pnr.'" name="shup'.$pnr.'" value="y" 
					onclick="checkshup('.$pnr.')"'; 
                               	if ($p['shownup']=="y") echo ' CHECKED';
				if ($disable_check) echo ' DISABLED';
				echo '>';
				}
                       	   else echo $p['shownup'];

	   		echo '</td>
	   			<td class="small">';
					
				  if(!$disable_check) {
					echo '
        	               		<INPUT type=checkbox id="part'.$pnr.'" name="part'.$pnr.'" value="y" 
						onclick="checkpart('.$pnr.')"'; 
        	                       	if ($p['participated']=="y") echo ' CHECKED';
					if ($disable_check) echo ' DISABLED';
                	        	echo '>';
					} elseif(!empty($p['participated'])) echo $p['participated']; else echo '?';
				echo '
	   			</td>';
			if($showrules) echo '
		   		<td class="small">';
			//if ($session_id) {
					if($showrules && !$disabled) {
						echo '<INPUT type=checkbox name="rules'.$pnr.'" value="y"';
                                	if ($p['rules_signed']=="y") echo ' CHECKED';
						if ($disabled) echo ' DISABLED';
						echo '>';
					}
					if($showrules && $disabled) echo $p['rules_signed'];
			//		}
			//	   else {
			//		if ($p['rules_signed']=="y") echo $lang['yes']; 
			//					else echo $lang['no_emp'];
			//		}
			if($showrules) echo '</td>';
				
			if($critical_count_apply) {
				echo '
				<td class="small">
        	            <INPUT type=checkbox id="chkbx'.$pnr.'" name="restrict'.$pnr.'" value="y" 
						onclick="checkpart('.$pnr.')"'; 
					if ($disable_check) echo ' DISABLED';
                	echo '>
	   			</td>';
			}

				
			// Edit added by Maxim Frolov 
				if((empty($_REQUEST["disabled"]) && !(isset($_REQUEST["editlink"]) && empty($_REQUEST["editlink"])))  || !empty($_REQUEST["editlink"])) echo '<td><a target=_blank href="participants_edit.php?participant_id='.$p["participant_id"].'">Edit</td>';
			}
		echo '</tr>';
		if ($shade) $shade=false; else $shade=true;
		}
		if(!empty($_REQUEST['pay']) && $payment_mode=="view"){
			$n_cols_before_pay=3;
			if(!empty($_REQUEST['show_emails'])) $n_cols_before_pay++;
			echo '<tr><td colspan='.$n_cols_before_pay.' style="text-align:right;border-top:2px solid black">Total :</td>';
			echo '<td style="border-top:2px solid black">'.$sum_payment.'</td>';
			if($nlfpay_name!="") echo '<td style="border-top:2px solid black">'.$sum_payment_nlf.'</td>';
			echo '</tr>';
			if($nlfpay_name!="") echo '<tr><td colspan='.$n_cols_before_pay.' style="text-align:right;border-top:2px solid black"><u>Total :</u></td>
								<td colspan=2 style="text-align:center;border-top:2px solid black"><u>'.($sum_payment+$sum_payment_nlf).'</u></td></tr>'; 
		}



function form__check_all($name,$count,$second_sel_name="",$second_un_name="") {
	global $lang;
	echo '<center>';
	echo '<input type=button value="'.$lang['select_all'].'" ';
	echo 'onClick="checkAll(\''.$name.'\','.$count.')';
	if ($second_sel_name) echo ';checkAll(\''.$second_sel_name.'\','.$count.')';
	echo '"><br>';
	echo '<input type=button value="'.$lang['select_none'].'" ';
	echo 'onClick="uncheckAll(\''.$name.'\','.$count.')';
	if ($second_un_name) echo ';uncheckAll(\''.$second_un_name.'\','.$count.')';
	echo '"></center>';
}

	if(!(!empty($_REQUEST['payment_mode']) && $_REQUEST['payment_mode']=='view' && !empty($_REQUEST['pay'])))
	if (check_allow('experiment_edit_participants')) {
	if(!$disable_check) { //empty($_REQUEST['disabled'])
	// table footer ...
	if($disabled) {$nr_normal_columns++; if(!empty($nlfpay_name)) $nr_normal_columns++; }
	echo '	<TR>
			<TD style="text-align:center" colspan='.$nr_normal_columns.'>';
	if(!empty($_REQUEST['payment_mode']) && !empty($_REQUEST['pay']) && ($registered || $shownup || $participated || $nonparticipated || $absent)) {
		echo lang('total'); if(!empty($nlfpay_name)) echo ' ('.lang('cash',false).')'; echo ' : <span id=total_cash></span>';
		echo '';
		if(!empty($nlfpay_name)) echo '&nbsp;&nbsp;&nbsp; '. lang('total').' ('.$nlfpay_name.')'.' : <span id=total_noncash></span>';
		$dossier_attr="";
		if($session_id!='') {
			$sessline=orsee_query("SELECT * FROM ".table("sessions")." WHERE session_id=$session_id");
			if(isset($sessline['payment_file'])) $dossier_attr=$sessline['payment_file'];
		}
		if(empty($dossier_attr) && $experiment_id!='') {
			$expline=orsee_query("SELECT * FROM ".table("experiments")." WHERE experiment_id=$experiment_id");
			if(isset($expline['payment_file'])) $dossier_attr=$expline['payment_file'];
		}
		$used_file=(empty($dossier_attr))?$dossier:$dossier_attr;
		$moneyrespline=orsee_query("SELECT money_responsible FROM ".table("payment_files")." WHERE id='$used_file'");
		if($moneyrespline!==false) {
			echo '<hr><small>'.lang('debit_account').' : '.helpers__pay_account_longname($moneyrespline['money_responsible']).' ';
			$accline0=orsee_query("SELECT SUM(amount) as file_amount FROM ".table("pay_accounts")." WHERE shortname='".$moneyrespline['money_responsible']."' AND payment_file='$dossier'");
			$amount0=0; if($accline0!==false && !empty($accline0['file_amount'])) $amount0=$accline0['file_amount'];
			echo "($amount0".lang('money_symbol')." ".lang('for_this_file',false).")";
			$accline=orsee_query("SELECT SUM(amount) as file_amount FROM ".table("pay_accounts")." WHERE shortname<>'bank' AND shortname<>'participants' AND payment_file='$dossier'");
			$amount1=0; if($accline!==false && !empty($accline['file_amount'])) $amount1=$accline['file_amount'];
			echo ", $amount1".lang('money_symbol').' '.lang('for_this_file',false).' '.lang('in_total',false);
			if($amount1==0) echo " - <span style='color:brown'>".str_replace(" ","&nbsp;",lang('no_operation_will_be_made',false))."</span>";
			echo '.</small>';
		}

	}
	echo '</td>';

		if ($assigned || $invited) {
			echo '<TD></TD><TD>';
			form__check_all('reg',$result_count);
			echo '</TD>';
			}
	
		if ($registered || $shownup || $participated || $nonparticipated || $absent) {
			echo '<TD></TD>
				<TD>';
			if ($registered || $shownup || $nonparticipated || $absent) 
				form__check_all('shup',$result_count,'','part');
			echo '</TD>
				<TD>';
                		form__check_all('part',$result_count,'shup','');
			echo '</TD>
				<TD>';
			if ($showrules && $session_id)
				form__check_all('rules',$result_count);
			echo '</TD>';
			}
	echo '	</TR>
		</table>';


	echo '	<TABLE border=0>
			<TR>
				<TD>&nbsp;</TD>
			</TR>';
	if ($assigned || $invited) {
		if(!isset($_REQUEST['to_session'])) $_REQUEST['to_session']="";
		if(!isset($_REQUEST['check_if_full'])) $_REQUEST['check_if_full']="";
		if(!isset($_REQUEST['remember'])) $_REQUEST['remember']="";
		echo '	<TR>
				<TD>
					'.$lang['register_marked_for_session'].' ';
					echo select__sessions($_REQUEST['to_session'],'to_session',$experiment_id,false);
			echo '	</TD>
			</TR>
			<TR>
				<TD>
					'.$lang['check_for_free_places_in_session'].'
					<INPUT type=checkbox name="check_if_full" value="true"';
					if ($_REQUEST['check_if_full'] || ! $_REQUEST['remember']) 
						echo ' CHECKED';
					echo '>
				</TD>
			</TR>';
			}
		}
		else echo '	</table><br><TABLE border=0>';
	echo '	<TR>
			<TD align=center>';
	if(!empty($_REQUEST['pay'])) {
			echo '
			<INPUT name="payment_mode" type=hidden value="'.$payment_mode.'">
			<INPUT name="dossier" type=hidden value="'.$_REQUEST['dossier'].'">
			<INPUT name="pay" type=hidden value="'.$_REQUEST['pay'].'">';
		if(isset($_REQUEST['only_empty'])) echo '
			<INPUT name="only_empty" type=hidden value="'.$_REQUEST['only_empty'].'">';
	}
	echo '
				<INPUT type=hidden name="result_count" value="'.$result_count.'">
				<INPUT type=hidden name="focus" value="'.$focus.'">
				<INPUT type=hidden name="experiment_id" value="'.$experiment_id.'">
				<INPUT type=hidden name="session_id" value="'.$session_id.'">
				<INPUT type=submit name="change" id="change" value="'.$lang['change'].'">
			</TD>
		</TR>';
	}

	echo '</table>';
	if($payment_active) echo '<INPUT type=hidden name="pay_operation_id" value="'.operation::get_new_id().'">';
	echo '
		</form>';

	if ($assigned || $invited || $critical_count_apply) {
		$alert_count="<br><br>"; $alert_count_sup="";
		if($critical_count_apply) $alert_count_sup="(by the last checkbox column)";
		if($result_count>$critical_count) $alert_count="<strong style='color:#F59'>More than $critical_count participants, restrict $alert_count_sup before changing!</strong><br>";
		echo $alert_count.'
		<form name="part_list_restricted" method=post  action="'.thisdoc().'">
			<INPUT type=hidden name="focus" value="'.$focus.'">
			<INPUT type=hidden name="experiment_id" value="'.$experiment_id.'">
			<INPUT type=hidden name="session_id" value="'.$session_id.'">
			<INPUT type=hidden name="restricted_plist" id="restricted_plist" value="">
			<INPUT type=submit name="restricted" value="Restrict for checked" onclick="restrict_checked()">
		</form>
		<script>
			function restrict_checked()
			{
				var res="";
				var ich=0;
				for(var i=1; i<='.$result_count.'; i++) {
					if(document.getElementById("chkbx"+i).checked) {
						if(ich>0) res+=",";
						res+=document.getElementById("pid"+i).value;
						ich++;
					}
				}
				document.getElementById("restricted_plist").value=res;
				//alert(document.getElementById("restricted_plist").value);
			}
		</script>
		';
	}
	
	
	echo '
		<BR>
		<TABLE width="80%" border=0>
		<TR>
			<TD>';
				if(empty($_REQUEST['pay']))
	 			if ($session_id && $session['session_finished']!="y" && check_allow('session_send_reminder')) {
                                        if ($session['reminder_sent']=="y") {
                                                $state=$lang['session_reminder_state__sent'];
                                                $statecolor=$color['session_reminder_state_sent_text'];
						$explanation=$lang['session_reminder_sent_at_time_specified'];
						$send_button_title=$lang['session_reminder_send_again'];
                                                }
                                        elseif ($session['reminder_checked']=="y" && $session['reminder_sent']=="n") {
                                                $state=$lang['session_reminder_state__checked_but_not_sent'];
                                                $statecolor=$color['session_reminder_state_checked_text'];
						$explanation=$lang['session_reminder_not_sent_at_time_specified'];
                                                $send_button_title=$lang['session_reminder_send'];
                                                }
                                        else {
                                                $state=$lang['session_reminder_state__waiting'];
                                                $statecolor=$color['session_reminder_state_waiting_text'];
						$explanation=$lang['session_reminder_will_be_sent_at_time_specified'];
                                                $send_button_title=$lang['session_reminder_send_now'];
                                                }
                                        echo '<FONT color="'.$statecolor.'">'.$lang['session_reminder'].': '.$state.'</FONT><BR>';
					echo $explanation.'<BR><FORM action="session_send_reminder.php">'.
						'<INPUT type=hidden name="session_id" value="'.$session_id.'">'.
						'<INPUT type=submit name="submit" value="'.$send_button_title.'"></FORM>';
                                        }
	echo '		</TD><TD align=right>';
			if (check_allow('participants_bulk_mail')) 
                        	experimentmail__bulk_mail_form();
	echo '		</TD>';

	echo '	</TR>
		</TABLE>';

        echo '  <A HREF="experiment_show.php?experiment_id='.$experiment_id.'">
                        '.$lang['mainpage_of_this_experiment'].'</A><BR><BR>

                </CENTER>';
if(!empty($_REQUEST['pay']) && !empty($dossier)) {
	$query="SELECT MAX(CAST(payment_num AS UNSIGNED)) as max_num FROM ".table('participate_at')." WHERE payment_file=$dossier";
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']).", Query=".$query);
	$maxprev=0;
	if(mysqli_num_rows($result)>0) {
		$line=mysqli_fetch_assoc($result);
		if(is_numeric($line['max_num']))$maxprev=$line['max_num'];
	}
	// var_dump(mysqli_num_rows($result),$maxprev); exit;
	mysqli_free_result($result);
	$query="SELECT start_num_from as start_num FROM ".table('payment_files')." WHERE id=$dossier";
	$line=orsee_query($query);
	if($line['start_num']!='' && $line['start_num']>$maxprev) $maxprev=$line['start_num']-1;
	$idarray=array("payment");
	if($nlfpay_name!="") $idarray[]="payment_nlf";
	script__payment_num($maxprev,count($participants),$idarray);
}
include ("footer.php");

?>

<?php
// part of orsee. see orsee.org
ob_start();


// SELECT * FROM or_participants as tp WHERE tp.deleted='y' and (tp.remarks is NULL or tp.remarks='') AND can_undelete=0 AND (SELECT count(*) FROM or_participate_at as tpa WHERE tp.participant_id=tpa.participant_id AND registered='y')=0 
$title="delete participant";
include ("header.php");


	/*if (isset($_REQUEST['participant_id']) && $_REQUEST['participant_id']) 
			$participant_id=$_REQUEST['participant_id'];
                else redirect ($GLOBALS['settings__admin_folder']."/");
				*/

        if (isset($_REQUEST['betternot']) && $_REQUEST['betternot'])
                redirect ($GLOBALS['settings__admin_folder'].'/participants_main.php');


        if (isset($_REQUEST['reallydelete']) && $_REQUEST['reallydelete']) $reallydelete=true;
                        else $reallydelete=false;

        if (isset($_REQUEST['reallyexclude']) && $_REQUEST['reallyexclude']) $reallyexclude=true;
                        else $reallyexclude=false;

	$allow=check_allow('participants_unsubscribe','participants_main.php');



	echo '<BR><BR>
		<center>
			<h4>'.$lang['delete_participant_data'].'</h4>
		</center>';


		if($reallydelete || $reallyexclude) query_makecolumns(table('participants'),array("last_delete_time","last_delete_admin","can_undelete"),array("INT(20)","VARCHAR(20)","tinyint(1)"));

$years_old_delete=1;
$years_old_anonymise=4;

// $qpart="FROM ".table('participants')." as tp WHERE tp.deleted='y' AND can_undelete=0 AND (SELECT count(*) FROM ".table('participate_at')." as tpa WHERE tp.participant_id=tpa.participant_id AND registered='y')=0 AND last_delete_time < UNIX_TIMESTAMP()";
$qpart="FROM ".table('participants')." WHERE deleted='y' AND can_undelete=0 AND number_reg=0";
if ($reallydelete) {
	$reftime="UNIX_TIMESTAMP()-365*24*3600*".$years_old_delete;	
	$qpart.=" AND last_delete_time < $reftime AND creation_time < $reftime AND last_visit_time < $reftime AND last_invitation_time < $reftime";
	$qpart.=" AND (remarks is NULL OR remarks='' OR remarks='#from leep.univ-paris1.fr\n')";
	$query="DELETE FROM ".table('participate_at')." WHERE participant_id IN (SELECT participant_id ".$qpart.")";
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
	$message = mysqli_affected_rows($GLOBALS['mysqli']) ." rows deleted from ".table('participate_at')." table"; //, <br>$query
	if(gettype($result)!="boolean") mysqli_free_result($result);
	
	$query="DELETE ".$qpart."";
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
	$message .= "<hr>". mysqli_affected_rows($GLOBALS['mysqli']) ." rows deleted from ".table('participants')." table"; //, <br>$query
	if(gettype($result)!="boolean") mysqli_free_result($result);

	$query="DELETE FROM ".table('participants_temp')." WHERE creation_time < UNIX_TIMESTAMP()-365*24*3600*".$years_old_delete;
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
	$message .= "<hr>". mysqli_affected_rows($GLOBALS['mysqli']) ." rows deleted from ".table('participants_temp')." table";
	if(gettype($result)!="boolean") mysqli_free_result($result);
	message ($message);
	log__admin("participant_delete_ubsubscribed",$years_old_delete."_year_old without_participations");
	redirect($GLOBALS['settings__admin_folder'].'/'.thisdoc());
}
if ($reallyexclude) {
	$reftime="UNIX_TIMESTAMP()-365*24*3600*".$years_old_anonymise;
	$timewhere="last_delete_time < $reftime AND creation_time < $reftime AND last_visit_time < $reftime AND last_invitation_time < $reftime";
	$timewhere.=" AND remarks NOT LIKE '%faux compte probablement%'";
	$where=" WHERE tp.deleted='y' AND can_undelete=0 AND $timewhere";
	$qpart.=" AND $timewhere";
	$query="DELETE FROM ".table('participate_at')." WHERE participant_id IN (SELECT participant_id ".$qpart.")";
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
	$message = mysqli_affected_rows($GLOBALS['mysqli']) ." rows deleted from ".table('participate_at')." table";
	if(gettype($result)!="boolean") mysqli_free_result($result);
	$query="DELETE ".$qpart."";
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
	$message .= "<hr>". mysqli_affected_rows($GLOBALS['mysqli']) ." rows deleted from ".table('participants')." table";
	if(gettype($result)!="boolean") mysqli_free_result($result);

	$qpart=str_ireplace(" AND number_reg=0","",$qpart);
	$qpart.=" AND remarks LIKE '%Exported participation data to participant%'";
	$query="DELETE FROM ".table('participate_at')." WHERE participant_id IN (SELECT participant_id ".$qpart.")";
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
	$message .= "<hr>". mysqli_affected_rows($GLOBALS['mysqli']) ." rows deleted from ".table('participate_at')." table (Exported participation data)";
	if(gettype($result)!="boolean") mysqli_free_result($result);
	$query="DELETE ".$qpart."";
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
	$message .= "<hr>". mysqli_affected_rows($GLOBALS['mysqli']) ." rows deleted from ".table('participants')." table (Exported participation data)";
	if(gettype($result)!="boolean") mysqli_free_result($result);

	$query="UPDATE ".table('participants')." as tp SET `email`='',`phone_number`='',`lname`='supprimé',`fname`='supprimé', remarks=REGEXP_REPLACE(remarks,'[a-zA-Z\-\.]+@([a-zA-Z-]+\.)+[\a-zA-Z\-]{2,}|((00)?33|0)\s*[1-9]([\s\.\-]*[0-9]{2}){4}',' X '), remarks=REGEXP_REPLACE(remarks,'(doublon de )([^(=]+)','doublon de X '), remarks=REGEXP_REPLACE(remarks,'(from participant )([^(=]+)','from participant X '), remarks=REGEXP_REPLACE(remarks,'(?-i)((-| )[A-Z]([a-zà-ÿ]|[A-Z])+){2,}',' X')".$where."";
	//remarks=REGEXP_REPLACE(remarks,'[a-zA-Z\-\.]+@([a-zA-Z-]+\.)+[\a-zA-Z\-]{2,}','X'), 
	//((00)?33|0)\s*[1-9]([\s\.\-]*[0-9]{2}){4}
	//emarks=REGEXP_REPLACE(remarks,'[a-zA-Z\-\.]+@([a-zA-Z-]+\.)+[\a-zA-Z\-]{2,}|((00)?33|0)\s*[1-9]([\s\.\-]*[0-9]{2}){4}','X')
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));
	$message .= "<hr>". mysqli_affected_rows($GLOBALS['mysqli']) ." rows anonymised in the ".table('participants')." table";
	if(gettype($result)!="boolean") mysqli_free_result($result);
	message ($message);
	log__admin("participant_anonymise_ubsubscribed",$years_old_anonymise."_year_old");
	redirect($GLOBALS['settings__admin_folder'].'/'.thisdoc());
}

	echo '<CENTER>
		<FORM action="'.thisdoc().'">
		<TABLE width=90%>
			<TR>
				<TD colspan=2 align=center>
					'.lang('clear_the_database').'<BR><BR>';
					//dump_array($participant);
			echo '	</TD>
			</TR>
			<TR>
				<TD align=center> 
					<INPUT type=submit name="reallydelete" 
						value="'.lang("delete_unsubscribed_".$years_old_delete."_year_old_with_no_participantions").'">
				</TD>
				<TD align=center>
					<INPUT type=submit name="reallyexclude" 
						value="'.lang("delete_and/or_anonymise_unsubscribed_".$years_old_anonymise."_years_old").'">
				</TD>
			</TR>
			<TR>
				<TD colspan=2 align=center>
					<INPUT type=submit name=betternot 
						value="'.$lang['no_sorry'].'">
				</TD>
			</TR>
		</TABLE>
		</FORM>
	      </center>';

include ("footer.php");

?>




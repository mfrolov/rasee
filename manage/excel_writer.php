<?php

/**
 * Simple excel writer class with no external dependencies, drop it in and have fun
 * @author Matt Nowack
 * @license Unlicensed
 * @version 1.0
 */
class Excel {
  public $headers;
  private $col;
  private $row;
  private $data;
  private $title;
  private $mode;
  private $txtdata;
  private $lsep;
  private $csep;
  private $ext;
  
  /**
   * Safely encode a string for use as a filename
   * @param string $title The title to use for the file
   * @return string The file safe title
   */
  static function filename($title) {
    $result = strtolower(trim($title));
    $result = str_replace("'", '', $result);
    $result = preg_replace('#[^a-z0-9_]+#', '-', $result);
    $result = preg_replace('#\-{2,}#', '-', $result);
    return preg_replace('#(^\-+|\-+$)#D', '', $result);
  }
  
  /**
   * Builds a new Excel Spreadsheet object
   * @return Excel The Spreadsheet
   */
  function __construct($title,$mode = "bin") {
    $this->title = $title;
    $this->mode = $mode;
    $this->col = 0;
    $this->row = 0;
    $this->data = '';
	$this->txtdata = '';
	$this->ext = ($mode == "bin")?'.xls':'.xls';
	$this->headers = false;
	$this->csep = '</td><td>';
	$this->lsep = '</td></tr><tr><td>';
	if($this->mode=="txt") {
		$tab = (chr(9));
		$this->csep = "{$tab}";
		$this->lsep = PHP_EOL.PHP_EOL.PHP_EOL;
		$this->ext=".txt";
	}
    if($this->mode=="bin") $this->bofMarker();
  }
  
  /**
   * Transmits the proper headers to cause a download to occur and to identify the file properly
   * @return nothing
   */
  function headers() {
    //header("Content-Type: application/force-download");
    //header("Content-Type: application/octet-stream");
    //header("Content-Type: application/download");
	if($this->mode=="txt") header("Content-Type: text/plain; charset='utf-8'");
	if($this->mode!="txt") header("Content-type: application/vnd.ms-excel; charset='UTF-8'" );
	if($this->mode!="txt") header("Content-type:   application/x-msexcel; charset='utf-8'");
    header("Content-Disposition: attachment;filename=" . Excel::filename($this->title) . $this->ext);
    //header("Content-Transfer-Encoding: binary ");
  }
  
  function send() {
 // $this->storeBof();
	 if($this->mode=="bin") $this->storeEof();
   // $this->eofMarker();
	//echo chr(255).chr(254);
    $this->headers();
    //echo chr(255).chr(254).mb_convert_encoding($this->data, 'UTF-16LE', 'UTF-8');
	//echo pack('vv',chr(255),chr(254));
	//echo chr(255).chr(254)
	//$this->xlsCodepage(1200);
	$utf8_prefix=chr(239).chr(187).chr(191);
	if(defined("no_utf8") && no_utf8==1) $utf8_prefix="";
	if($this->mode=="bin") echo $this->data;
	if($this->mode=="html") echo $utf8_prefix."<table border=1><tr><td>".$this->txtdata."</td></tr></table>";
	if($this->mode=="txt") echo $utf8_prefix.$this->txtdata;
  }
  
  function xlsCodepage($codepage) {
    $record    = 0x0042;    // Codepage Record identifier
    $length    = 0x0002;    // Number of bytes to follow

    $header    = pack('vv', $record, $length);
    $data      = pack('v',  $codepage);

    echo $header , $data;
  }
  function storeBof()
    {
        $record  = 0x0809;        // Record identifier

        // According to the SDK $build and $year should be set to zero.
        // However, this throws a warning in Excel 5. So, use magic numbers.
            $length  = 0x0008;
            $unknown = '';
            $build   = 0x096C;
            $year    = 0x07C9;
			$type=0x06;//0x0005;

        $version = 0x0800;

        $header  = pack("vv",   $record, $length);
        $data    = pack("vvvv", $version, $type, $build, $year);
        $this->data.=$header . $data . $unknown;
    }
  /**
   * Writes the Excel Beginning of File marker
   * @see pack()
   * @return nothing
   */
  private function bofMarker() {

    //$this->data .= pack("vvvvvv", 0x809, 0x8, 0x0, 0x10, 0x0, 0x0); 
	$this->storeBof();
	  	
 
  }
  function storeEof()
    {
        $record    = 0x000A;   // Record identifier
        $length    = 0x0000;   // Number of bytes to follow
        $header    = pack("vv", $record, $length);
        $this->data .= $header;
    }
  
  /**
   * Writes the Excel End of File marker
   * @see pack()
   * @return nothing
   */
  private function eofMarker() { 
  
    $this->data .= pack("vv", 0x0A, 0x00); 
	$this->data .=chr(255).chr(254);
  }
  
  /**
   * Moves internal cursor left by the amount specified
   * @param optional integer $amount The amount to move left by, defaults to 1
   * @return integer The current column after the move
   */
  function left($amount = 1) {
    $this->col -= $amount;
    if($this->col < 0) {
      $this->col = 0;
    }
    return $this->col;
  }
  
  /**
   * Moves internal cursor right by the amount specified
   * @param optional integer $amount The amount to move right by, defaults to 1
   * @return integer The current column after the move
   */
  function right($amount = 1) {
    $this->col += $amount;
    return $this->col;
  }
  
  /**
   * Moves internal cursor up by amount
   * @param optional integer $amount The amount to move up by, defaults to 1
   * @return integer The current row after the move
   */  
  function up($amount = 1) {
    $this->row -= $amount;
    if($this->row < 0) {
      $this->row = 0;
    }
    return $this->row;
  }
  
  /**
   * Moves internal cursor down by amount
   * @param optional integer $amount The amount to move down by, defaults to 1
   * @return integer The current row after the move
   */
  function down($amount = 1) {
    $this->row += $amount;
    return $this->row;
  }
  
  /**
   * Moves internal cursor to the top of the page, row = 0
   * @return nothing
   */
  function top() {
    $this->row = 0;
  }
  
  /**
   * Moves internal cursor all the way left, col = 0
   * @return nothing
   */
  function home() {
    $this->col = 0;
  }
  
  /**
   * Writes a number to the Excel Spreadsheet
   * @see pack()
   * @param integer $value The value to write out
   * @return nothing
   */
  function number($value) { 
    $this->data .= pack("vvvvv", 0x203, 14, $this->row, $this->col, 0x0); 
    $this->data .= pack("d", $value); 
  }
  
  /**
   * Writes a string (or label) to the Excel Spreadsheet
   * @see pack()
   * @param string $value The value to write out
   * @return nothing
   */
   
   
  function label($value) { 
    $length = strlen($value);
    $this->data .= pack("vvvvvv", 0x204, 8 + $length, $this->row, $this->col, 0x0, $length);	
    $this->data .= $value; 
  }
  
  function writeTxtData($value) { 
  /*
    $length = strlen($value);
    $tdata=$this->data;
	$tarr=explode("$lsep",$tdata);
	$lkeyexists=0;
	foreach($tarr as $lkey=>$tl) {
		if($lkey==$this->row){
			$lkeyexists=1;
			$tlarr=explode("$csep",$tl)
			$ckeyexists=0;
			foreach($tlarr as $ckey=>$tv) {
			if($ckey==$this->col){
			$ckeyexists=1;
			}
			}
		}
	}
    $this->data .= $value; 
*/
  }
    
  function writeLine($arr) {
	if($this->mode!="bin" && $this->row>0){$this->txtdata.=$this->lsep;}
	foreach($arr as  $value) {
	//$value= iconv('UTF-8', 'UTF-16LE', $value);
	//$value=utf8_decode($value);
	if($this->mode=="bin"){
		if(is_numeric($value)) {$this->number($value);}
		else {$this->label($value);}
		}
	else {
		if($this->mode=="html" && $this->row==0 && $this->headers) $value="<strong>".$value."</strong>";
		$this->txtdata.=($this->col==0)?$value:$this->csep.$value;
	}
	$this->right();
	}
	$this->down(); 
	$this->home();
  }
}
<?php
ob_start();

if (isset($_REQUEST['table'])) $table=$_REQUEST['table']; else $table="";

$menu__area="view/edit table";
$title="view/edit table ".$table;
include ("header.php");

if(empty($table)) {message(lang('no_table_to_edit')); redirect($GLOBALS['settings__admin_folder']."/options_main.php"); }//die(lang('no_table_to_edit'));
if($table!="permanent_pay_accounts") {message(lang('wrong_table')); redirect($GLOBALS['settings__admin_folder']."/options_main.php"); } //redirect($GLOBALS['settings__admin_folder']."/".thisdoc()."?table=permanent_pay_accounts");//die(lang('wrong_table'));


	$done=false;
	$allow_cat="experimentclass";
    
	$allow=check_allow($allow_cat.'_edit','options_main.php');

	$header=lang('table').' "'.lang($table).'"';
	$where="";
	// $where=" WHERE closed = 0 ";
	if(!empty($_REQUEST['all'])) $where="";
	
	echo '<BR>
		<center><h4>'.$header.'</h4>';

	if (check_allow($allow_cat.'_add') && empty($_REQUEST["addit"])) {
		echo '	<BR>
			<form action="'.thisdoc().'">
			<INPUT type=submit name="addit" value="'.lang('create_new').'">
			<INPUT type=hidden name="table" value="'.$table.'">
			</FORM>';
		}
		
	$query="CREATE TABLE IF NOT EXISTS ".table($table)." (`id` INT(20) NOT NULL default 0)";
 	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysql_error($GLOBALS['mysqli']));
		
		
	// load languages
	$columns=table_fields(table($table));
	$id_name=(in_array('id',$columns))?'id':$columns[0];
	$modifiable=array();
	$not_in_summary=array();
	
	$colnames=array();
	foreach ($columns as $col) {$colnames[]=lang($col); $modifiable[]=($col!=$id_name)?1:0;}
	
	if(!empty($_REQUEST["delete"]) && isset($_REQUEST["id"]) && $_REQUEST["id"]!=="") {

		log__admin("delete_from_table","table $table, $id_name: $id");
		$query="DELETE FROM ".table($table)." WHERE `$id_name`='$id'";
		$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysql_error($GLOBALS['mysqli']));
		message (lang('changes_saved'));
		redirect($GLOBALS['settings__admin_folder']."/".thisdoc());
		// show_message();
	}
	
	if(!empty($_REQUEST["addoredit"])) {
		$expr=array();
		$new=!empty($_REQUEST['new']);
		if($new) {
			$query="SELECT `$id_name` FROM ".table($table)."
							WHERE `$id_name`=".$_REQUEST['id'];
			$checkline=orsee_query($query);
			$new=!isset($checkline['id_name']);
		}
		// var_dump($new);
		for($i=0; $i<count($columns); $i++) if($modifiable[$i]>0)
		{
			// $valnames[]=$columns[$i]; $values[]=$_REQUEST[$columns[$i]];
			$val=$_REQUEST[$columns[$i]];
			if($modifiable[$i]==2) $val=(int)$val;
			if(is_array($val)) $val=implode(",",$val);
			$expr[]="`".$columns[$i]."`='".$val."'";
		}
		// var_dump($_REQUEST,$expr); exit;
		$query=($new)?"INSERT INTO ":"UPDATE ";
			// $valnames=array(); $values=array(); 
		$query.="$table SET ";
		if($new) $query.="`$id_name`='".$_REQUEST['id']."', ";
		$query.=implode(", ",$expr);
		if(!$new) $query.="WHERE `$id_name`='$id'";
		// var_dump($_REQUEST,$query);
		$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysql_error($GLOBALS['mysqli']));
		log__admin("modify_table","table: $table, $id_name: ".$_REQUEST['id']."");
		message (lang('changes_saved'));
		redirect($GLOBALS['settings__admin_folder']."/".thisdoc());
		// show_message();
	}
	

	if(!empty($_REQUEST["addit"])) {
		$new=!(isset($_REQUEST["id"]) && $_REQUEST["id"]!=="");
		if (!$new) $button_title=lang('change'); else $button_title=lang('add');
		$js_select_name=""; $form_name='payment_file';
		echo '<BR><FORM name='.$form_name.' action="'.thisdoc().'">
		<INPUT type=hidden name="table" value="'.$table.'">
		<table border=0 style="align:center;vertical-align:middle">';
		$new_val=0;
		if ($new) {$new_val=1; $id=query_new_id(table($table),"`$id_name`");}
		$initvals=array(); 
		for($i=0; $i<count($columns); $i++) if($modifiable[$i]>=0)
		{
			$val='';
			if(!$new) {
				$query="SELECT ".$columns[$i]." FROM ".table($table)." WHERE `$id_name`='$id'";
				$forval=orsee_query($query);
				$val=$forval[$columns[$i]];
			}
			if($columns[$i]==$id_name) $val=$id;
			$initvals[$i]=$val;
			echo '<tr><td>'.$colnames[$i].'</td>';
			echo '<td>';
			$ntacols=50; $ntarows=1;
			if($columns[$i]=="remarks") {$ntarows=5;}
			if($modifiable[$i]==0) echo $val; 
			elseif($modifiable[$i]==1) echo '<textarea name="'.$columns[$i].'" rows='.$ntarows.' cols='.$ntacols.' >'.$val.'</textarea>';
			elseif($modifiable[$i]==2) {
				$checked=$val?"CHECKED":"";
				echo '<input type=checkbox name="'.$columns[$i].'" '.$checked.' value=1 >';
			}
			if($modifiable[$i]==3) {
				echo experiment__experimenters_checkbox_list($columns[$i],$val,true,"update_responsable");
				echo '<p align=center><a target=_blank href="admin_edit?new=true">'.lang('create_new').' '.lang('experimenter',false).'</a> ('.lang('then',false).' <a href="javascript:location.reload();">'.lang('reload_the_page',false).'</a>)</p>';
				// echo '<p>&nbsp;</p>';
			}
			echo '</td></tr>';
		}
		
		echo '</table>';
		echo '
			<TABLE>
				<TR>
					<TD COLSPAN=2 align=center>
						<INPUT name=addoredit type=submit value="'.$button_title.'">';
		if(true) echo '
						<INPUT type=hidden name="id" value="'.$id.'">
						<INPUT type=hidden name="new" value="'.$new_val.'">';

		echo '
					</TD>
				</TR>
			</table>
			</FORM>
			<BR>';
		script__update_responsable($form_name,$js_select_name);

		if (!$new && check_allow($allow_cat.'_delete')) {
			echo '<BR><BR><FORM action="'.thisdoc().'">
				<INPUT type=hidden name="table" value="'.$table.'">
				<INPUT type=hidden name="id" value="'.$id.'">
				<INPUT type=hidden name="delete" id="delete_file" value="0">
				<INPUT type=button onclick="really_delete(this)" name="delete_button" value="'.lang('delete').'">
				</FORM>
				<SCRIPT> function really_delete(x){if(confirm("'.str_replace('?','',lang('do_you_really_want_to_delete')).lang('payment_file').dossier_name($id,1).'?")) {document.getElementById("delete_file").value=1; x.form.submit();}}</SCRIPT>';
			}
		echo '<BR><BR>
			<A href="'.thisdoc().'?table='.$table.'">'.icon('back').' '.lang('back').'</A><BR><BR>
			</CENTER>';
		include_once ("footer.php");
		exit;
	}


	echo '<BR>
		<table border=1 style="border-collapse:collapse">
			<TR>
 				';
        		foreach ($colnames as $colkey=>$colname) if($columns[$colkey]!="closed" || !empty($_REQUEST['all']))
					if(empty($not_in_summary[$columns[$colkey]])) {
                	echo '<td class="small">
							'.$colname.'
					</td>';
				}

	echo '			<TD></TD>
			</TR>';


	$query="SELECT *
      		FROM ".table($table)."
		 ".$where;
	$result=mysqli_query($GLOBALS['mysqli'],$query) or die("Database error: " . mysqli_error($GLOBALS['mysqli']));

	$shade=false;
	if (mysqli_num_rows($result)>0) {

	while ($line=mysqli_fetch_assoc($result)) {

   		echo '	<tr class="small"'; 
   		if ($shade) echo ' bgcolor="'.$color['list_shade1'].'"';
		else echo ' bgcolor="'.$color['list_shade2'].'"';
   		echo '>
				<td class="small" valign=top>
					<A HREF="'.thisdoc().'?addit=1&id='.$line['id'].'&show_statistics=1">
					<small>'.$line['id'].'</small>
					</A>
				</td>';
		foreach ($columns as $col) if($col!="closed" || !empty($_REQUEST['all'])) 
		if(empty($not_in_summary[$col])) {
			if($col=="money_responsible" || $col=="experimenter") $line[$col]=admin__load_name_by_val($line[$col],true); //,$col!="experimenter"
        	echo '	<td class="small" valign=top';
            if ($shade) echo ' bgcolor="'.$color['list_shade1'].'"'; 
			else echo ' bgcolor="'.$color['list_shade2'].'"';
            echo '>';
			if (isset($chnl2br) && $chnl2br) echo nl2br(stripslashes($line[$col]));
            else echo stripslashes($line[$col]);
            echo '</td>';
		}
   		echo '		<TD>
					<A HREF="'.thisdoc().'?addit=1&id='.$line['id'].'">'.
						lang('edit').'</A>
				</TD>
   			</tr>';
   		if ($shade) $shade=false; else $shade=true;
		}
		
		}
	   else {
		echo '	<tr>
				<td style="text-align:center" colspan='.(count($columns)+1).'>
					'.lang('no_lines').'
				</td>
			</tr>';
		}

	echo '</table>

		</CENTER>';

include_once ("footer.php");



?>

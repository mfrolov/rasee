<?php
// part of orsee. see orsee.org
ob_start();
$title="mail participants";

include ("header.php");
// var_dump($_REQUEST);
	if ($_REQUEST['experiment_id']) $experiment_id=$_REQUEST['experiment_id'];
			else redirect ($GLOBALS['settings__admin_folder']."/");

	$allow=check_allow('experiment_invitation_edit','experiment_show.php?experiment_id='.$experiment_id);

	if (isset($_REQUEST['id'])) $id=$_REQUEST['id']; else $id="";

	if (isset($_REQUEST['preview']) && $_REQUEST['preview']) $preview=true; else $preview=false;
	if (isset($_REQUEST['save']) && $_REQUEST['save']) $save=true; else $save=false;
	if (isset($_REQUEST['send']) && $_REQUEST['send']) $send=true; else $send=false;
	if (isset($_REQUEST['sendall']) && $_REQUEST['sendall']) $sendall=true; else $sendall=false;
	if (isset($_REQUEST['remove_mails_from_queue']) && $_REQUEST['remove_mails_from_queue']) $removemails=true; else $removemails=false;

	if ($preview || $save || $send || $sendall || $removemails) $action=true; else $action=false;


	$experiment=orsee_db_load_array("experiments",$experiment_id,"experiment_id");
	if (!check_allow('experiment_restriction_override'))
		check_experiment_allowed($experiment,$GLOBALS['settings__admin_folder']."/experiment_show.php?experiment_id=".$experiment_id);

	// load invitation languages
	$inv_langs=lang__get_part_langs();
	$installed_langs=get_languages();


	echo '<BR><BR>
		<center>
			<h4>'.$experiment['experiment_name'].'</h4>
			<h4>'.$lang['send_invitations'].'</h4>
		';


	if ($action) {

		$sitem=$_REQUEST;
                $sitem['content_type']='experiment_invitation_mail';
		$sitem['content_name']=$experiment_id;

		// prepare lang stuff
		foreach ($inv_langs as $inv_lang) {
			$sitem[$inv_lang]=$sitem[$inv_lang.'_subject']."\n".$sitem[$inv_lang.'_body'];
			}	

		// well: just to be sure: for all other languages, copy the public default lang
		foreach ($installed_langs as $inst_lang) {
			if (!in_array($inst_lang,$inv_langs)) $sitem[$inst_lang]=$sitem[$settings['public_standard_language']];
			}	


		// is unknown or known?
                if (!$id) $done=lang__insert_to_lang($sitem);
                	else $done=orsee_db_save_array($sitem,"lang",$id,"lang_id");

                if ($done) message ($lang['changes_saved']);
                        else message ($lang['database_error']);

		if ($preview) {
			redirect ($GLOBALS['settings__admin_folder'].'/experiment_mail_preview.php?experiment_id='.$experiment_id);
		}
		elseif ($send || $sendall) {
			// send mails!

			$allow=check_allow('experiment_invite_participants','experiment_mail_participants.php?experiment_id='.$experiment_id);

			if ($allow) {

						$whom= ($sendall) ? "all" : "not-invited";
						$measure_start=getmicrotime();
						$starttime=0;
						if(!empty($_REQUEST['start_at'])) {
							$tempstarttime=strtotime($_REQUEST['start_at']);
							if($tempstarttime!==false) {
								$starttime=$tempstarttime;
								message (lang("the_earliest_sending_date").": ".date("Y-m-d H:i:s",$starttime));
							}
							else message (lang("error_in_start_date"));
						}
						$sended=experimentmail__send_invitations_to_queue($experiment_id,$whom,$starttime);

						message ($sended.' '.$lang['xxx_inv_mails_added_to_mail_queue']);

				//$sended.' '.$lang['xxx_e-mail_messages_sent']

				$measure_end=getmicrotime();	
						message($lang['time_needed_in_seconds'].': '.round(($measure_end-$measure_start),5));
				log__admin("experiment_send_invitations","experiment:".$experiment['experiment_name']);
						redirect ($GLOBALS['settings__admin_folder']."/experiment_mail_participants.php?experiment_id=".$experiment_id);
			}
		}
		elseif($removemails) {
			$qmails=experimentmail__remove_from_queue("invitation",$experiment_id);
			message($qmails." ".lang("mails_removed_from_queue",false));
			redirect ($GLOBALS['settings__admin_folder'].'/'.thisdoc().'?experiment_id='.$experiment_id);
		}
		else {
			message($lang['mail_text_saved']);
			log__admin("experiment_edit_invitation_mail","experiment:".$experiment['experiment_name']);
			redirect ($GLOBALS['settings__admin_folder'].'/'.thisdoc().'?experiment_id='.$experiment_id);
		}
	}

	$query="SELECT * from ".table('lang')." 
		WHERE content_type='experiment_invitation_mail' 
		AND content_name='".$experiment_id."'";
	$experiment_mail=orsee_query($query);

	// form

        echo '<FORM action="'.thisdoc().'" method="post">
        	<INPUT type=hidden name="experiment_id" value="'.$experiment_id.'">
		<INPUT type=hidden name="id" value="'.$experiment_mail['lang_id'].'">

        	<TABLE border=0 width=90%>';

	foreach ($inv_langs as $inv_lang) {
		// split in subject and text
		$subject=str_replace(strstr($experiment_mail[$inv_lang],"\n"),"",$experiment_mail[$inv_lang]);
		$body=substr($experiment_mail[$inv_lang],strpos($experiment_mail[$inv_lang],"\n")+1,strlen($experiment_mail[$inv_lang]));

        	// set defaults if not existent
        	if (!$subject) {
                	$subject=load_language_symbol('def_expmail_subject',$inv_lang);
                	}

        	if (!$body) {
                	$body=load_mail('default_invitation_'.$experiment['experiment_type'],$inv_lang);
			}

		if (count($inv_langs) > 1) {
			echo '<TR><TD colspan=2 bgcolor="'.$color['list_shade1'].'">'.$inv_lang.':</TD></TR>';
			}

		echo '
			<TR>
				<TD>
					'.$lang['subject'].':
				</TD>
				<TD>
					<INPUT type=text class="form-control" name="'.$inv_lang.'_subject" size=55 maxlength=150 value="'.
						stripslashes($subject).'">
				</TD>
			</TR>
			<TR><TD colspan=2>
				<span class="small form-text">add #html# at the beginning of the subject to send as HTML (so tags like &lt;em&gt;/&lt;<i>i</i>&gt; or &lt;strong&gt;/&lt;<b>b</b>&gt; may be used inside the body)</span>
			</TD></TR>
                	<TR>
				<TD valign=top colspan=2>
					'.$lang['body_of_message'].':<BR>
					<span class="small form-text">'.$lang['experimentmail_how_to_rebuild_default'].'</span>
					<BR>

					<center>
					<textarea class="form-control" name="'.$inv_lang.'_body" wrap=virtual rows=20 cols=67>'.
						stripslashes($body).'</textarea>
					</center>
				</TD>
			</TR>
			<TR>
					<TD valign=top colspan=2 style="text-align:left">
						<br><br>Enter the start date (or leave empty for sending as soon as possible) : <br>- Date format : YYYY-MM-DD HH:MM:SS, or without seconds, and/or without date (for today) - <br>
						<INPUT type=text name="start_at" value="">
                			</TD>
				</TR>';

		echo ' <TR><TD colspan=2>&nbsp;</TD></TR>';

		}

	echo '
                	<TR bgcolor="'.$color['list_options_background'].'">
				<TD colspan=2>
					1. '.$lang['save_mail_text_only'].'
				</TD>
			</TR>
                	<TR bgcolor="'.$color['list_options_background'].'">
                		<TD align=left>
                			<INPUT type=submit name="preview" class="small" value="'.$lang['mail_preview'].'">
                		</TD>
                		<TD align=right>
					<INPUT type=submit name="save" value="'.$lang['save'].'">
                		</TD>
			</TR>
			<TR>
				<TD>
					'.$lang['assigned_subjects'].':
				</TD>
				<TD>
					'.experiment__count_participate_at($experiment_id).'
				</TD>
			</TR>
        		<TR>
				<TD>
					'.$lang['invited_subjects'].':
				</TD>
				<TD>
					'.experiment__count_invited($experiment_id).'
				</TD>
			</TR>
			<TR>
            			<TD>
					'.$lang['registered_subjects'].':
				</TD>
				<TD>
					'.experiment__count_registered($experiment_id).'
				</TD>
        		</TR>
			<TR>
				<TD>
					'.$lang['inv_mails_in_mail_queue'].':
				</TD>
				<TD>';
					$qmails=experimentmail__mails_in_queue("invitation",$experiment_id);
					echo $qmails.'
				</TD>
			</TR>';


		if ($qmails>0) {
			echo '	<TR>
					<TD colspan=2>
						<BR><FONT color="red">
						'.$qmails.' '.$lang['xxx_inv_mails_for_this_exp_still_in_queue'].'
						<button type="submit" name="remove_mails_from_queue" class="btn btn-sm btn-danger" value="1" >'.lang('remove_them_from_queue').'</button>
						</FONT><BR>
					</TD>
				</TR>';
			}
		   elseif (check_allow('experiment_invite_participants')) {
                	echo '	<TR bgcolor="'.$color['list_options_background'].'">
					<TD colspan=2>
                				2. '.$lang['mail_to_not_got_one'].'
					</TD>
				</TR>
	        		<TR bgcolor="'.$color['list_options_background'].'">
					<TD colspan=2 align=right>
						<INPUT type=submit name="send" value="'.$lang['send'].'">
                			</TD>
				</TR>
                		<TR bgcolor="'.$color['list_options_background'].'">
					<TD colspan=2>
                				3. '.$lang['mail_have_got_it_already'].'
					</TD>
				</TR>
                		<TR bgcolor="'.$color['list_options_background'].'">
					<TD colspan=2 align=right>
						<INPUT type=submit name="sendall" value="'.$lang['send_to_all'].'">
                			</TD>
				</TR>';
			}
	echo '
        	</TABLE>
        	</FORM>';

	echo '<BR><A HREF="experiment_show.php?experiment_id='.$experiment_id.'">'.
			$lang['mainpage_of_this_experiment'].'</A><BR><BR>

		</CENTER>';

include ("footer.php");
?>

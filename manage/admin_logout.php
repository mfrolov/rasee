<?php
// part of orsee. see orsee.org
ob_start();

include ("nonoutputheader.php");

log__admin("logout");
admin__logout();

redirect($GLOBALS['settings__admin_folder']."/admin_login.php?logout=true");

include ("footer.php");

?>

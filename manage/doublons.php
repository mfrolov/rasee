<?php
ob_start();

$menu__area="experiments";
$title="show participants";
include("header.php");
if(!isset($expadmindata)) die('$expadmindata not defined! May be wrong ORSEE version.');
if ((!$expadmindata['adminname']))  die('You are not authorized!');


$partlist=$_REQUEST['participants'];

	if (!$partlist) {
		message("incorrect parameter");
		redirect($GLOBALS['settings__admin_folder']."/");
		exit;
		}

include("doublonfunction.php");

$justlist=false;
if(!empty($_REQUEST['just_show'])) $justlist=true;

$lite=false;
if(!empty($_REQUEST['lite'])) $lite=true;

if(strtoupper($partlist)=="ALL") echo "<br><br>Started at ".date("d/m/Y à H:i").".<br><br>";

$whereclause=(strtoupper($partlist)=="ALL")?"":" WHERE participant_id='".str_replace(",","' OR participant_id='",$partlist)."'";

$query="SELECT * FROM ".table('participants').$whereclause;
//echo $query."<br>";							
$result = mysqli_query($GLOBALS['mysqli'],$query);
$ftable=array();
$lnarr=array();
$fnarr=array();
$gender=array();
$emails=array();
$narr=0;
$nfields=0;
$fnames=array();
$marked=array(); 
if($result!="") {
while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
$ftable[$narr]=$row;
$ln[$narr]=$row["lname"];
$fn[$narr]=$row["fname"];
$tel[$narr]=$row["phone_number"];
$del[$narr]=$row["deleted"];
$exl[$narr]=$row["excluded"];
$nreg[$narr]=$row["number_reg"];
$nnoshow[$narr]=$row["number_noshowup"];
$gender[$narr]=$row["gender"];
$emails[$narr]=$row["email"];
$marked[$narr]=0;
$narr++;
}
$nfields = mysqli_num_fields($result);

for ($i = 0; $i < $nfields; $i++){
    $fname=mysqli_fetch_field($result)->name;
	$fnames[$i]=$fname;
}
}
mysqli_free_result($result);
$doublons=array(); $groupd=array(); $nactives=array(); $gd=0; $nd=0; 
$nparticipants=count($ftable);
for($i=0; $i<$nparticipants; $i++)
{ //strtoupper
$cln=supac(strtoupper($ln[$i])); $cfn=supac(strtoupper($fn[$i])); $ctel=preg_replace('/^\+33/','0',$tel[$i]); $ctel=supac(strtoupper(preg_replace('/[\s\W]+/','',$ctel))); $cgender=strtoupper($gender[$i]); $cemail=strtoupper($emails[$i]); 
$idoubl=0; $nexti=$i+1; $gddeb=$gd; $nactive=0;
if(!$justlist) {
for($h=$nexti; $h<$nparticipants; $h++)
{ //strtoupper
$chln=supac(strtoupper($ln[$h])); $chfn=supac(strtoupper($fn[$h]));  $chtel=preg_replace('/^\+33/','0',$tel[$h]); $chtel=supac(strtoupper(preg_replace('/[\s\W]+/','',$chtel))); $chgender=strtoupper($gender[$h]);  $chemail=strtoupper($emails[$h]);
// var_dump(strtoupper($ln[$h]),strtoupper($ln[$h]));
//($cln==$chln && $cfn==$chfn) || ($cln==$chfn && $cfn==$chln) || ($chtel==$ctel && $chtel!="" )
//doubloncondition($cln,$chln,$cfn,$chfn,$ctel,$chtel,$cgender,$chgender)
// var_dump(doubloncondition(array($cln,$chln,$cfn,$chfn,$ctel,$chtel,$cgender,$chgender)));
// var_dump($cln); var_dump($chln);
if($marked[$i]==0 && (doubloncondition(array($cln,$chln,$cfn,$chfn,$ctel,$chtel,$cgender,$chgender,$cemail,$chemail))))
{
//echo $cln.".".$cfn."-".$chln.".".$chfn."|";


if($idoubl==0) {$gd++; $doublons[$nd]=$ftable[$i]; $groupd[$nd]=$gd; $nd++; $idoubl=1;  if($lite && $del[$i]!="y") $nactive++;};
$doublons[$nd]=$ftable[$h]; $groupd[$nd]=$gd; $marked[$h]=1;
if($lite && $del[$h]!="y") $nactive++;
$nd++;
}
}
if($gd>$gddeb) {$marked[$i]=1; if($lite) $nactives[$gd]=$nactive;};
}
}
$doubleaff=array(); for($i=0; $i<=$gd; $i++) { $doubleaff[$i]=false; };
$ndaff=0;
if($lite) 
{
for($i=0; $i<count($doublons); $i++)
{
	$cgd=$groupd[$i];
	$cline=$doublons[$i];
	$nshow=$cline["number_reg"]-$cline["number_noshowup"];
	if($cline["deleted"]=="y" && $nshow>0 && $nactives[$cgd]>0) {$doubleaff[$cgd]=true;}
	if($nactives[$cgd]>1) {$doubleaff[$cgd]=true;}
	}
	for($g=0; $g<$gd; $g++) {if($doubleaff[$g]) $ndaff++;}
}

echo "<br><br>Nombre de participants : ".$nparticipants.".";
if(!$justlist) echo " Nombre de doublons : ".$gd;
if($lite) echo " . Nombre de doublons affichés: ".$ndaff;
echo "<br>";
echo "<table border=1>";
$headerline='<tr>';
// foreach($fnames as $fname) $headerline.='<td>'.$fname.'</td>';
// var_dump($fnames);
$headerline.='<td>'.implode("</td><td>",$fnames).'</td>';
$headerline.='<td>des</td><td>desExc</td><td>edit</td><td>Rap: NOM</td><td>Rap: Prénom</td><td>Rap: Tél</td><td>Créé le</td>';
$headerline.='</tr>';
echo $headerline;
//<td>participant_id </td><td>participant_id_crypt </td><td>creation_time </td><td>subpool_id </td><td>email </td><td>phone_number </td><td>lname </td><td>fname </td><td>begin_of_studies </td><td>subscriptions </td><td>field_of_studies </td><td>profession </td><td>address_street </td><td>address_zip </td><td>address_city </td><td>address_country </td><td>deleted </td><td>excluded </td><td>gender </td><td>number_reg </td><td>number_noshowup </td><td>language </td><td>remarks </td><td>rules_signed</td><td>des</td><td>desExc</td><td>edit</td><td>Rap: NOM</td><td>Rap: Prénom</td><td>Rap: Tél</td><td>Créé le</td></tr>";
if($justlist) $doublons=$ftable;
$cgdprec=0; $ngaff=0;
$doublons_id=array();
$doublons_id_aff=array();
for($i=0; $i<count($doublons); $i++)
{
$cgd=0;
$cline=$doublons[$i];
$doublons_id[]=$cline["participant_id"];
if($i<count($groupd)) $cgd=$groupd[$i];
if(!$lite || $doubleaff[$cgd]) 
{
if($cgd>$cgdprec) {$ngaff++;}
$doublons_id_aff[]=$cline["participant_id"];
$clinet=implode("</td><td>",$cline);
if($ngaff%2==0) {$ccolor="#CCA"; $ccolor=($cline["deleted"]=="y")?"#BB9":$ccolor; $ccolor=($cline["excluded"]=="y")?"#CB9":$ccolor;}
if($ngaff%2!=0) {$ccolor="#ACC"; $ccolor=($cline["deleted"]=="y")?"#9BB":$ccolor; $ccolor=($cline["excluded"]=="y")?"#ABB":$ccolor;}
echo "<tr style='background-color:".$ccolor."'><td>".$clinet."</td> 
<td> <a target=_blank href='nparticipants_delete.php?participantstodelete=".$cline["participant_id"]."&reallydelete=1&reallyexclude=0'>Désabonner</td>
<td> <a target=_blank href='nparticipants_delete.php?participantstodelete=".$cline["participant_id"]."&reallydelete=0&reallyexclude=1'>DésExc</td>
<td> <a target=_blank href='participants_edit.php?participant_id=".$cline["participant_id"]."'>Edit</td>
<td> ".$cline["lname"]."</td>
<td> ".$cline["fname"]."</td>
<td> ".$cline["phone_number"]."</td>
<td> ".date("d/m/Y à H:i",$cline["creation_time"])."</td>
</tr>";
$cgdprec=$cgd;
}
}
echo "</table><br><br>";
// echo implode(";",$groupd)."<br><br><br><br>";
// echo implode(";",$doublons);
echo "<a href='doublons.php?participants=".implode(',',$doublons_id)."'>Short link to all duplicated accouts</a>, <a href='doublons.php?participants=".implode(',',$doublons_id)."&lite=1'>lite version</a>";
if($lite) echo ", <a href='doublons.php?lite=1&participants=".implode(',',$doublons_id_aff)."''>short link to shown duplicated accouts</a>.";
if(strtoupper($partlist)=="ALL") echo "<br><br>Finished at ".date("d/m/Y à H:i").".<br><br>";
include ("footer.php");

?>
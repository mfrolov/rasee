<?php
// part of orsee. see orsee.org
ob_start();

$menu__area="experiments";
$title="show participants";

//"Rules signed?" disabled by Maxim Frolov;
$showrules=false; if(!empty($_REQUEST["showrules"])) $showrules=true;

include("header.php");

	if (isset($_REQUEST['experiment_id']) && $_REQUEST['experiment_id']) $experiment_id=$_REQUEST['experiment_id'];
                elseif((!empty($_REQUEST['pay']) && !empty($_REQUEST['payment_mode']) && $_REQUEST['payment_mode']=='view') || !empty($_REQUEST['disabled'])) $experiment_id='';
				elseif(!empty($_REQUEST['focus']) && $_REQUEST['focus']!="assigned") {$experiment_id=''; if(empty($_REQUEST["disabled"])) $_REQUEST["disabled"]=1;}
				else redirect($GLOBALS['settings__admin_folder']."/experiment_main.php"); //{var_dump($_REQUEST); var_dump($_POST); exit;}//
				

        if (isset($_REQUEST['session_id']) && $_REQUEST['session_id']) $session_id=$_REQUEST['session_id'];
                else $session_id='';

	if (isset($_REQUEST['remember']) && $_REQUEST['remember']) { 
		$_REQUEST=array_merge($_REQUEST,$_SESSION['save_posted']);
		$_SESSION['save_posted']=array();
		unset($_REQUEST['change']);
		}

	$allow=check_allow('experiment_show_participants','experiment_show.php?experiment_id='.$experiment_id);
	
	

	$experiment=orsee_db_load_array("experiments",$experiment_id,"experiment_id");
	if (!check_allow('experiment_restriction_override'))
		check_experiment_allowed($experiment,$GLOBALS['settings__admin_folder']."/experiment_show.php?experiment_id=".$experiment_id);
	
	query_makecolumns(table('participate_at'),"external_code","varchar(255)","");
	$w=" WHERE `experiment_id`='$experiment_id'";
	if(!empty($session_id)) $w.=" AND session_id='$session_id'";
	$corder=" ORDER BY participate_id";
	if(!empty($_REQUEST["apply"]) && check_allow('experiment_edit_participants'))
	{
		if(empty($_REQUEST["replace"])) $w.=" AND external_code=''";
		if(!empty($_REQUEST["exclude_registered"])) $w.=" AND registered='n'";
		if(!empty($_REQUEST["exclude_invited"])) $w.=" AND invited='n'";
		$firstquery="SELECT * FROM ".table('participate_at').$w.$corder;
		$firstlines=orsee_query($firstquery,"return_same");
		$pstdt=$_REQUEST['codestoadd'];
		$pstdt=trim($pstdt);
		$pstdt=str_replace("\r","",$pstdt);
		// $pstdt=str_replace(" ","",$pstdt);
		$pstdt=str_replace("\n",",",$pstdt);
		$pstd=explode(",",$pstdt);
		$ncodes=count($pstd); $nplaces=($firstlines === false)?0:count($firstlines);
		$ntoadd=min($ncodes,$nplaces);
		// var_dump($firstquery,$firstlines,$ncodes,$nplaces); exit;
		$mess=$ncodes." ".lang("codes_provided_for",false)." ".$nplaces." ".lang("lines", false).". ";
		
		for($i=0; $i<$ntoadd; $i++) {
			$cw=" AND participate_id='".$firstlines[$i]["participate_id"]."'";
			$q="UPDATE ".table('participate_at')." ";
			$q.="SET `external_code`='".$pstd[$i]."'";
			$q.=$w.$cw;
			orsee_query($q);
		}
		
		$mess.=" ".$ntoadd." ".lang("entries_proceeded", false).". ";
		message($mess);
		redirect($GLOBALS['settings__admin_folder'].'/'.thisdoc().'?experiment_id='.$experiment_id);
		exit;
		
	}
	$checkquery="SELECT * FROM ".table('participate_at').$w.$corder;
	$alllines=orsee_query($checkquery,"return_same");
	if($alllines===false) $alllines=array();
	$filledlines=array_filter($alllines,function($val){return !empty($val['external_code']);});
	$unregisteredlines=array_filter($alllines,function($val){return $val["registered"]=="n";});
	$unregisteredemptylines=array_filter($alllines,function($val){return ($val["registered"]=="n" && empty($val['external_code']));});
	$uninvitedlines=array_filter($alllines,function($val){return $val["invited"]=="n";});
	$uninvitedemptylines=array_filter($alllines,function($val){return ($val["invited"]=="n" && empty($val['external_code']));});
	$nempty=count($alllines)-count($filledlines);
	echo '<CENTER>
		<FORM method="POST" action="'.thisdoc().'"><TABLE align=center>
			<TR>
				<TD align=left>
					<ul>
					<li>'.count($alllines).' '.lang("lines_in_total", false).'. '.$nempty.' '.lang("lines_with_empty_external_codes",false).';</li>
					<li>'.count($unregisteredlines).' '.lang("lines_without_session", false).'	('.count($unregisteredemptylines).' '.lang("with_empty_external_codes",false).'); </li>
					<li>'.count($uninvitedlines).' '.lang("participants_non_invited", false).'	('.count($uninvitedemptylines).' '.lang("with_empty_external_codes",false).'). </li>
					</ul>
					<INPUT type=hidden name="experiment_id" value="'.$experiment_id.'">
					<INPUT type=checkbox name="replace">'.lang('replace_existing').' <br>
					<INPUT type=checkbox name="exclude_registered">'.lang('exclude_participants_registered_for_a_session').' <br>
					<INPUT type=checkbox name="exclude_invited">'.lang('exclude_already_invited_participants').' <br>
					<br>
				</TD>
			</TR>
			<tr><td align=center><INPUT type=submit name="apply" 
						value="'.lang('apply').'"></td></tr>
		</TABLE>
		<br>
		'.lang("please_enter_external_codes").", ".lang("one_per_line",false).', '. lang("or_separated_by_comma",false).':<br>
		<TEXTAREA NAME="codestoadd" COLS=40 ROWS=30></TEXTAREA>
		<br>
		</FORM>';
        if(!empty($experiment_id)) echo '<BR><BR><A HREF="experiment_show.php?experiment_id='.$experiment_id.'">
                        '.$lang['mainpage_of_this_experiment'].'</A><BR><BR>
		
	      </CENTER>
		  ';
include ("footer.php");

?>

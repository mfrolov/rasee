

<?php
chdir("../");
include ("nonoutputheader.php");
ini_set('default_charset', "");
$charset='UTF-8';
if (isset($settings__charset) && $settings__charset=='ISO-8859-1') $charset='ISO-8859-1';
?>
<html>
<head>
<meta charset="<?php echo $charset; ?>">
<meta http-equiv="content-type" content="text/html; charset=<?php echo $charset; ?>">
<meta http-equiv="expires" content="0">
<title>Session's creation tutorial</title>
<style>
ol {margin-left:-5px; }
li {
 margin-top: 0.6em;
}
li.sub {
 margin-top: 0.2em; type:a
}
</style>
<?php
$tuto_title=lang('some_days_before_the_experiment');
$jscr_please_choose_exp='javascript:void(alert(\''.lang('pelase_select_experiment_first').'\'))';
?>
<script>
function resizeIframe(obj) {
// alert(obj.contentWindow.document.documentElement.scrollHeight);
// obj.style.height="10px";
// obj.style.width="10px";
obj.style.height = obj.contentWindow.document.documentElement.scrollHeight + 'px';
obj.style.width = obj.contentWindow.document.documentElement.scrollWidth + 'px';
//if(obj.parentElement!=null) obj.parentElement.style.height = obj.style.height;
//alert(id("mainframe").src+"\r\n"+window.frames[0].location);
id("pleasewait").style.display="none";
obj.contentWindow.onbeforeunload = function () {
        // console.log(obj.contentWindow.document.documentElement.scrollHeight);
		id("pleasewait").style.display="block";
    };
}
function alertIframeUnload(obj) {
	alert(obj.contentWindow.document.documentElement.scrollHeight);
}
function id(elem) {return document.getElementById(elem);}
var t=setInterval(proceedUpdate,700);
var pelase_select_experiment_first="<?php echo lang('pelase_select_experiment_first'); ?>";
var jscr_please_choose_exp="javascript:void(alert('"+pelase_select_experiment_first+"'))";
var last_exp_id="", last_sess_id=""; var page_before_dossier="../";
var hints=["experiment_main|experiment_my:Find the experiment you need in the list and click on its name. If it doesn't exist, <a target=mainframe href='../experiment_edit.php?addit=true'>create a new experiment</a>."];
hints.push("experiment_edit:Look carefully at all parameters and fill if necessary. Don't forget to attribute a payment file (look at '<?php echo lang('assign_payment_file')?>'). If the corresponding payment file doesn't exist, <a target=mainframe href='../dossiers_paiement_main.php?addit=1'>create a new payment file</a>.");
hints.push("dossiers_paiement_main:after creating/modifying the payment file, <a target=mainframe href='#page_before_dossier#'>return back to the previous page</a>");
hints.push("experiment_drop_participants:uncheck all criteria and click on “<?php echo lang('search_and_show');?>”|click on “<?php echo lang('remove_all_participants_in_list'); ?>” at the bottom");
hints.push("experiment_add_participants:check and modify the criteria you need and then click on “<?php echo lang('search_and_show');?>” For the first criterion, it is possible to enter several values separated by a &#124; (vertical line)|check subjects and click on “<?php echo lang('assign_all_participants_in_list'); ?>” at the bottom");
hints.push("experiment_show:you have finished selecting or creating the experiment, check the substeps of the first step and proceed to the folloing steps|you seem to have already created a session. Create more sessions if you need (step 2), check the substep of the step 2 and proceed to the following steps.");
hints.push("experiment_mail_participants:check the subject and the body of the mail and change it if necessary, click on “<?php echo lang('mail_preview'); ?>” to test the result, and then click on “<?php echo lang('send'); ?>” or “<?php echo lang('send_to_all'); ?>” according to your choice.");
hints.push("experiment_mail_preview:after checking the mail details, click on “<?php echo lang('back_to_mail_page'); ?>” in order to return back to the mail composition/sending page");
hints.push("session_edit:Check all parameters and click “<?php echo lang('add'); ?>”. If “<?php echo lang('gender_equality'); ?>” is checked in the <a target=mainframe href='../experiment_edit.php?experiment_id=#experiment_id#'>experiment's configuration</a>, you may change the number of males and females allowed for the session in the “<?php echo lang('remarks'); ?>” in the form f:N,m:K (example: f:15,m:5).|You may click at “<?php echo lang('copy_as_new_session'); ?>” in order to create a new session with same parameters and save time. ");
function get_var_val(q,varname)
{
	var aq1 = q.toString().split("?");
	if(aq1.length<2) return "";
	if(varname=="#allline#") return aq1[1];
	var aq2=aq1[1].split('&');
	res="";
	for(i=0; i<aq2.length; i++) {
		aq3=aq2[i].split('=');
		if(aq3[0]==varname) {
			res=(aq3.length>1)?aq3[1]:"";
			break;
		}
	}
	return res;
}
function get_page(q)
{
	var aq1 = q.toString().split("?");
	var aq2=aq1[0].split('/');
	var page=aq2[aq2.length-1];
	if(page=="") return "index";
	var apage=page.split(".");
	apage.pop();
	if(apage.length>0) return apage.join(".");
	return page;
}
function get_hint(pagename,location)
{
	if(location==undefined) location="";
	var cloc=location.toString();
	var reshint="";
	for(var i=0; i<hints.length; i++) {
		var cah=hints[i].split(":");
		var hbase=cah.shift();
		var ahbase=hbase.split("|");
		if(ahbase.indexOf(pagename)>=0) {
			reshint=cah.join(":");
			reshint=reshint.replace(/#page_before_dossier#/g,page_before_dossier);
			reshint=reshint.replace(/#experiment_id#/g,last_exp_id);
			break;
		}
	}
	var areshint=reshint.split("|");
	if(areshint.length>1) {
		// console.log(get_var_val(location,"#allline#"));
		if(pagename=="experiment_show" && last_sess_id!="") return areshint[1];
		if(pagename=="session_edit" && get_var_val(location,"experiment_id")=="") return areshint[1];
		if(cloc!="" && get_var_val(location,"#allline#")=="") return areshint[1];
		reshint=areshint[0];
	}
	return reshint;
}
function set_href(cid,newhref)
{
	if(id(cid)!=null) id(cid).href=newhref;
}
function proceedUpdate()
{
	var currLoc=id("mainframe").contentWindow.location;
	id("currentPageLoc").innerHTML=currLoc.toString();//+"<br>"+id("mainframe").contentWindow.document.documentElement.scrollHeight+'px';
	var expid=get_var_val(currLoc,"experiment_id");
	var sessid=get_var_val(currLoc,"session_id");
	var pelase_select_experiment_first="<?php  echo lang('pelase_select_experiment_first'); ?>";
	var cpage=get_page(currLoc);
	if(expid!="") {
		set_href("new_session","../session_edit.php?experiment_id="+expid);
		set_href("select_subjects","../experiment_add_participants.php?experiment_id="+expid);
		set_href("send_invitations","../experiment_mail_participants.php?experiment_id="+expid);
		set_href("check_subjects","../experiment_participants_show.php?experiment_id="+expid);
		set_href("delete_subjects","../experiment_drop_participants.php?experiment_id="+expid);
		set_href("exp_details","../experiment_edit.php?experiment_id="+expid);
		last_exp_id=expid; set_href("return_back","../experiment_show.php?experiment_id="+last_exp_id);
	}
	else {
		if(cpage!="session_edit" || sessid=="") {
			set_href("new_session",jscr_please_choose_exp);
			set_href("select_subjects",jscr_please_choose_exp);
			set_href("send_invitations",jscr_please_choose_exp);
			set_href("check_subjects",jscr_please_choose_exp);
			set_href("delete_subjects",jscr_please_choose_exp);
			set_href("exp_details",jscr_please_choose_exp);
		}
		if(cpage!="calendar_main" && cpage!="session_edit") set_href("return_back",jscr_please_choose_exp);
	}
	if(sessid!="") {last_sess_id=sessid;}
	if(cpage!="dossiers_paiement_main") page_before_dossier=currLoc.toString();
	var chint=get_hint(cpage,currLoc);
	// console.log(chint);
	if(chint!="") id("hintdiv").innerHTML="<u>Advice</u>:<br><br>"+"<i>"+chint+"</i>"+"<br><br>";
	else id("hintdiv").innerHTML="";
}
</script>
</head>

<body>
<div id=pleasewait style="position:fixed; left:55%;top:45%; display:none; border:1px solid blue; text-align:center; vertical-align:middle; background-color:yellow; font-weight:bold; min-height:50px; min-width:150px; color:blue; opacity:0.5"><br><?php echo lang('please_wait'); ?>...</div>
<table border=1 style="width:100%; border-color:blue; border-collapse:collapse">
<tr><td colspan=2 style="text-align:right;vertical-align:top;border-color:blue;"><table style='width:100%'><tr><td style="width:25%;text-align:left"><a href="../"><?php echo lang('back_to_introduction');?></a></td><td style="text-align:right;vertical-align:top; max-width:500px"><?php echo lang("current_page_address"); ?>: &nbsp;&nbsp;<span  id=currentPageLoc style="text-align:right;vertical-align:bottom;max-width:250px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td></tr></table> </td></tr>
<tr>
<td style="width:21%;min-width:250px;vertical-align:top;border-color:blue;">
<div style="position:sticky;top:7%;width:20%;min-width:240px;overflow:auto">
<strong><?php echo $tuto_title; ?>:</strong>
<ol>
<?php echo '<li>'.'<a target=mainframe href="../experiment_main.php">'.lang("select_experiment").'</a>'.lang(" or ").'<a target=mainframe href="../experiment_edit.php?addit=true">'.lang("create_new_experiment").'</a>'.' ('.lang("option").': <a target=mainframe href="../experiment_my.php">'.lang("select_only_from my_experiments").'</a> '.lang('or find_it',false).' <a target=mainframe href="../calendar_main.php">'.lang('in_the_calendar',false).'</a> '.lang('and click_on_its_name',false).')'.'
	<ol type=a>
		<li class=sub>'.lang('optionally ').'<a target=mainframe id=exp_details href="'.$jscr_please_choose_exp.'">'.lang('check/modify experiment_details').'</a></li>
		<li class=sub>'.lang('optionally ').'<a target=mainframe id=check_subjects href="'.$jscr_please_choose_exp.'">'.lang('check_the assigned_subjects').'</a> '.lang('and_then',false).'</li>
		<li class=sub>'.lang('optionally ').'<a target=mainframe id=delete_subjects href="'.$jscr_please_choose_exp.'">'.lang('delete_assigned_subjects').'</a> '.lang('if_there_are_any',false).'</li>
	</ol>
</li>
<li>'.'<a id=new_session target=mainframe href="'.$jscr_please_choose_exp.'">'.lang("create_new_session").'</a> <br>('.lang('repeat_for_several_sessions').')'.'
	<ol type=a><li class=sub>'.lang('optionally ').'<a target=mainframe href="../calendar_main.php">'.lang('check in_the_calendar').'</a> '.lang('and_then',false).' <a target=mainframe id=return_back href="'.$jscr_please_choose_exp.'">'.lang('return_back_to_the_experiment').'</a> '.lang('or click_on_its_name in_the_calendar').'</li></ol>
</li>
<li>'.'<a id=select_subjects target=mainframe href="'.$jscr_please_choose_exp.'">'.lang("assign_subjects").'</a>'.'</li>
<li>'.'<a id=send_invitations target=mainframe href="'.$jscr_please_choose_exp.'">'.lang("send_invitations").'</a>'.'</li>';
?>
</ol>
	<center>
	<div id=hintdiv style='min-height:100px;width:95%; border:2px solid black; text-align:justify' >
	</div>
	</center>
</div>
</td>
<td  id=tdforiframe><div  class="embed-responsive embed-responsive-16by9">
<iframe class="embed-responsive-item" name=mainframe id=mainframe src="../" frameborder="0" style="width:100%" scrolling="auto" onload="resizeIframe(this)" />
</div></td>
</tr></table>
</body>

</html> 
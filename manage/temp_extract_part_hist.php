<?php
ob_start();

$menu__area="experiments";
$title="show participants";
include("header.php");
$exps="77995098,1685488630";
$sesscond="(session_start_year='2013' AND session_start_day>27) OR (session_start_year='2010' AND session_start_month=10 AND session_start_hour<15) OR (session_start_year='2011' AND part_needed>10)";
//$sesscond="session_start_year='2010' AND session_start_month=10 AND session_start_hour<15";
$q="SELECT * FROM or_participants, or_participate_at, or_sessions WHERE or_participants.participant_id=or_participate_at.participant_id AND or_sessions.session_id=or_participate_at.session_id AND or_participate_at.experiment_id IN ($exps) AND (payghost=0 OR payghost is NULL) AND registered='y' AND participated='y' AND ($sesscond) ORDER BY session_start_year, session_start_month, session_start_day, session_start_hour, session_start_minute,lname,fname,fname";
$lines=orsee_query($q,"return_same");
$res="<table border=1 cellpadding=5 style='text-align:center'>";
$res.="<tr><th>N</th><th>participant_id</th><th>experiment_id</th><th>session_id</th><th>Experiment</th><th>Session</th>";
$res.="<th>N Prev Experiments</th>";
$res.="<th>Prev Experiment Classes</th>";
$res.="<th>Prev Experiment Names</th>";
$res.="</tr>";
$experimentclasses=experiment__get_experiment_class_names();
$prev_n=array(); $perclass=array(); $pername=array();
foreach($lines as $k=>$line) {
	$res.="<tr>";
	$res.="<td>".($k+1)."</td><td>".$line["participant_id"]."</td><td>".$line["experiment_id"]."</td><td>".$line["session_id"]."</td>";
	$res.="<td>".experiment__get_title($line["experiment_id"])."</td><td>".session__build_name($line)."</td>";
	//$res.="<td>".implode(",",experiment__get_experiment_class_names())."</td>";
	// $mysqlsessdate="STR_TO_DATE(CONCAT(session_start_year,'-',LPAD(session_start_month,2,'00'),'-',LPAD(session_start_day,2,'00'),'-',LPAD(session_start_hour,2,'00'),'-',LPAD(session_start_minute,2,'00')),'%Y-%m-%d-%H-%i')";
	// $mysqldate1="STR_TO_DATE('".$acdate[3]."-".$acdate[2]."-".$acdate[1]."-".$acdate[0]."-00','%Y-%m-%d-%H-%i')";
	// $mysqldate2="STR_TO_DATE('".$acdate[3]."-".$acdate[2]."-".$acdate[1]."-".$acdate[0]."-00','%Y-%m-%d-%H-%i')";
	// if(count($cdates)==1) $where_clause.="AND session_start_year='".$acdate[3]."' AND session_start_month='".$acdate[2]."' AND session_start_day='".$acdate[1]."' AND session_start_hour='".$acdate[0]."'";
	// elseif($i==0) {$supsess_criteria.="AND ".$mysqlsessdate.">=".$mysqldate1." "; $where_clause.=$supsess_criteria;}
	$mysqlsessdate="STR_TO_DATE(CONCAT(session_start_year,'-',LPAD(session_start_month,2,'00'),'-',LPAD(session_start_day,2,'00'),'-',LPAD(session_start_hour,2,'00'),'-',LPAD(session_start_minute,2,'00')),'%Y-%m-%d-%H-%i')";
	$mysqldate1="STR_TO_DATE(CONCAT(".$line["session_start_year"].",'-',LPAD(".$line["session_start_month"].",2,'00'),'-',LPAD(".$line["session_start_day"].",2,'00'),'-',LPAD(".$line["session_start_hour"].",2,'00'),'-',LPAD(".$line["session_start_minute"].",2,'00')),'%Y-%m-%d-%H-%i')";
	$exps=$line["experiment_id"];
	$sesscond="$mysqlsessdate<$mysqldate1";	
	$cq="SELECT * FROM or_participants, or_participate_at, or_sessions, or_experiments WHERE or_participants.participant_id=or_participate_at.participant_id AND or_sessions.session_id=or_participate_at.session_id AND or_experiments.experiment_id=or_participate_at.experiment_id AND or_participants.participant_id='".$line["participant_id"]."' AND or_participate_at.experiment_id <> $exps AND (payghost=0 OR payghost is NULL) AND registered='y' AND participated='y' AND ($sesscond) ORDER BY session_start_year, session_start_month, session_start_day, session_start_hour, session_start_minute,lname,fname,fname";
	$clines=orsee_query($cq,"return_same");
	$nprev=($clines===false)?0:count($clines);
	$prev_n[]=$nprev;
	$res.="<td>".$nprev."</td>";
	$cexp_classes=array();
	$cexp_names=array();
	if($nprev>0) foreach($clines as $cline) {
		$experiment_class_arr=explode(",",$cline["experiment_class"]);
		foreach($experiment_class_arr as $expclass) {
			if(!in_array($experimentclasses[$expclass],$cexp_classes)) {
				$cexp_classes[]=$experimentclasses[$expclass];
				$perclass[]=$experimentclasses[$expclass];
			}
		}
		if(!in_array($cline["experiment_name"],$cexp_names)) {
			$cexp_names[]=$cline["experiment_name"];
			$pername[]=$cline["experiment_name"];
		}
	}
	$res.="<td>".implode("; ",$cexp_classes)."</td>";
	$res.="<td>".implode("; ",$cexp_names)."</td>";
	$res.="</tr>";
}
$res.="</table>";
echo $res;
function show_arr_c_vals($acv,$elemname="Element")
{
	$cres="<table border=1 cellpadding=5 style='text-align:center'>";
	$cres.="<tr><th>$elemname</th><th>N</th></tr>";
	foreach($acv as $v=>$n) {
		$cres.="<tr>";
		$cres.="<td>".$v."</td><td>".$n."</td>";
		$cres.="</tr>";
	}
	$cres.="</table>";
	echo $cres;
	
}
//$prev_n=array(); $perclass=array(); $pername=array();
echo "<hr>";  show_arr_c_vals(array_count_values($prev_n),"Prev Experiments");
echo "<hr>";  show_arr_c_vals(array_count_values($perclass),"Experiment classe");
echo "<hr>";  show_arr_c_vals(array_count_values($pername),"Experiment name");
include ("footer.php");

?>
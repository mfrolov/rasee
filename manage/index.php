<?php
// part of orsee. see orsee.org
ob_start();

$menu__area="mainpage";
$title="Welcome";

include("header.php");

	echo '<BR><BR><center>
		';

	show_message();

	echo content__get_content("admin_mainpage");

	if (check_allow('experiment_edit')) {
		echo '
		<br><br>'.lang('organise_a_session').':
		<A target=_top HREF="session_tutorial/">'.lang('step_by_step_tutorial').'</A>';
	}
	if (check_allow('experimentclass_edit')) {
		echo '
		<br><br>'.lang('accounting').':
        <A HREF="dossiers_paiement_main.php">'.lang('payment_files').'</A>, 
        <A HREF="payment_operations.php">'.lang('financial_operations').'</A>,
		<A HREF="payment_accounts.php">'.lang('state_of_accounts').'</A>';
		echo '
		  <br><br>
		  <link rel="stylesheet" href="../jcryption/jquery-ui.min.css">
		  <script src="../jcryption/jquery-ui.min.js"></script>
		  ';
		$dpreg='';
		if(lang('lang')=='fr') {
			echo '
			  <script src="../jcryption/datepicker-fr.js"></script>
			  ';
			$dpreg='fr';
		}
		echo '
		  <script>
		  $( function() {
			$.datepicker.setDefaults( $.datepicker.regional[ "'.$dpreg.'" ] );
			$( ".datepicker" ).datepicker({
			  dateFormat: "dd/mm/yy",
			  changeYear: true,
			  onSelect: function() {document.getElementById("sdbut").style.visibility="";}
			});
			$("#sdbut").click(function() {
				var atdate="atdate:";
				if($("#datepicker1").val()!="") {
					atdate+=$("#datepicker1").val()
					if($("#datepicker2").val()!="") atdate+="-";
				}
				if($("#datepicker2").val()!="") atdate+=$("#datepicker2").val()
				$("<input>").attr({
					type: "hidden",
					name: "focus",
					value: atdate
				}).appendTo("#reportshowform");
				$("#reportshowform").submit();
			});
		  } );
		  
		  </script>
		
		<form method="get" id="reportshowform" action="experiment_participants_show.php">
		<input type="hidden" name=pay value=1>
		<input type="hidden" name=payment_mode value="view">
		</form>
		<p>'.lang('payment_report_all_files_for_dates').':  <input type="text" id="datepicker1" class="datepicker"> - <input type="text" id="datepicker2" class="datepicker">&nbsp;  <button id=sdbut style="visibility:hidden">'.lang('show').'</button></p>
		';
	}

	echo '</center><BR><BR>';

include("footer.php");

?>
